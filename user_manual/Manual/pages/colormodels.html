<html>
<head>
<title>LodePaint User Manual</title>
<link rel="stylesheet" href="../stylesheet.css" type="text/css" />
</head>
<body>
<h1>LodePaint User Manual</h1>
<p><a href="../manual.html">Back to main page</a></p>
<h1>Color Models</h1>

This document describes the color models supported by LodePaint.
<p/>
NOTE: a color <u>model</u> is not the same as a color <u>mode</u>. LodePaint also supports multiple color modes for image, but the color modes are the way the data per pixel is structured in the computer memory. This document is about color models, which are mathematical descriptions of colors.
<p/>
<img src="../images/colordialoga.png"/>
<p/>
LodePaint supports multiple color models. The image itself is always in RGBA color (or single channel greyscale), but the support for multiple color models is there in three ways:
<br/>
<br/>-The color chooser dialog allows choosing colors in all color models in the "acvanced" tab
<br/>-The "Channel Switcher" operation under "Colors" in the "Image" menu, allows converting channels of any of these color models, to the R, G, B or A channel of the image.
<br/>-Some other filters use HSV or HSL to affect colors in intuitive ways.
<p/>
This document gives some explanation on what each color model means. The alpha channel (A) isn't mentioned here because it's not really part of the color data, it has the same meaning in every color model.
<p/>
First of all it should be mentioned that every color model (except some exceptions like CMYK which add an extra channel for better reproduction with ink) is 3-dimensional, because 3 numerical values can be used to reproduce the colors seen by the human eye which has 3 types of color receptors.
<p/>
In 32-bit images, the range of R, G and B must be 0-255 for each pixel. Values out of this range can be represented by some other color models, but are invalid: they cannot be displayed on a computer monitor or saved in an image format such as PNG. Such invalid values are indicated in the color chooser dialog with a yellow and red line below the color (see the screenshot above, note the yellow/red horizontal lines in the sliders of YPbPr, YCbCr, Lab and XYZ).
<p/>
However, when using floating point images (with 128-bit pixels), then colors are never out of range (unless you'd use a value such as 42 gazillion) and the red/yellow lines can then be safely ignored. Gamut (the available range of colors a color model represents) is no longer relevant on floating point images, because with floating point images a simple RGB can already represent every color that exist on the planet, the hottest part of the brightest star, the darkest part of the blackest black hole, and negative colors.
<p/>
If you need to enter values outside the range of the color models in the color dialog, you can type them manually in the input boxes, the sliders can't be used for this.

<h2>RGB</h2>

Red, Green, Blue. The additive color model. This color model is used by the human eye, which has recepters sensitive to red, green and blue. Combinations of these colors can make any color. For example, red, green and blue together form white (the maximum brightness). No red, no green, and no blue, form black. Red and green light together form yellow light. If red, green and blue are equal, you get grey colors.
<p/>
Apart from the human eye, this color model is used by computer screens, most digital images (on the internet, in computer games), by TV screens, by the receptors of most digital cameras, and so on.
<p/>
The classic digital RGB model is limited to representing the values in a certain range (e.g. 0-255), but by using "floating point" color in LodePaint, it's also possible to support any higher value or negative values, for HDR image operations.

<h2>CMY(K)</h2>

CMY is the subtractive color model. Mathematically speaking this is just the inverse of RGB. It's based on the colors cyan, magenta and yellow. This color model represents that what you do with paint on white paper. If you mix cyan and yellow paint, you get green, for example. CMYK adds an extra "K" component which represents black. Mathematically speaking the black component isn't needed, but practically speaking it adds higher quality to ink. This color model is used by printers: printers have cyan, magenta, yellow and black ink.

<h2>HSL</h2>

Hue, Saturation, Lightness. This is a very intuitive color model to use, it's much easier to choose a certain color with this than with RGB. The hue is often represented in a circle. The channels have the following meaning:
<br/>
<br/>-Hue (H): the colors of the rainbow
<br/>-Saturation (S): how colorful or greyish the color is. E.g. if you choose a red hue, with high saturation it'll be very red, with half saturation it'll be more greyish or pink-like red, with the lowest saturation it's grey (colorless)
<br/>-Lightness (L): how dark or light the color is. In HSL, the darkest is black, the lightest is white.

<h2>HSV</h2>

This color model is very similar to HSL, except that the lightness component is replaced by "value". In HSV, the lowest value gives a black color, while the highest value can give the brighest possible red, green, yellow, white, etc... depending on the hue and saturation. Compare this with HSL, where the brightest lightness is always white.

<h2>YPbPr</h2>

This is a color model used in some telivision standards. Y represents the lightness, Pb and Pr determine the color. The Pb and Pr components can have negative values.
<p/>
The meaning of the L, Pb and Pr components is roughly the following:
<br/>
<br/>-Y: luma (brightness)
<br/>-Pb: difference between blue and luma
<br/>-Pr: difference between red and luma

<h2>YCbCr</h2>

YCbCr is a version of YPbPr that is scaled to be more suited for digital applications: the values are scaled in the range 0-255, while in YPbPr you'll notice that some channels can have negative values. This color model is used in the encoding JPEG images.

<h2>Lab</h2>

Lab refers to CIE L*a*b* color in LodePaint. This color model is often used in professional printing and color calibration, because it has the highest gamut of all standardized models - at least if you're limited to the ranges of values in 8-bit per channel storage. Lab can represent many colors that are out of range for 8-bit per channel RGB.
<p/>
LodePaint just offers this color model to be able to use its values in the color chooser or for the "channel switcher" filter, but doesn't actually allow exporting images in Lab color for professional printing purposes (that's not the purpose of LodePaint).
<p/>
In LodePaint, the Lab values may be scaled in a range from 0-255 and -128-+127, or, 0-1 and -0.5-+0.5, instead of 0-100 for L and free to choose for a and b in the standard definition of Lab. Note that here too you can choose other values than the sliders in the color dialog make appear possible by typing it manually, and these values will still mathematically work.
<p/>
The meaning of the L, a and b components is roughly the following:
<br/>
<br/>-L: lightness: 0 is black, 255 is white,  and on floating images higher values can represent brighter white (such as sunlight) and negative values are also supported to represent blacker than black or to be a "sink" for blur filtes.
<br/>-a: chroma position between red and green. Negative values indicate green, positive values indicate red.
<br/>-b: chroma position between yellow and blue. Negative values indicate blue, positive values indicate yellow.

<h2>XYZ</h2>

XYZ refers to CIE XYZ in LodePaint. XYZ is a linear transformation of RGB: since color is 3-dimensional for humans, XYZ defines 3 standardized values to represent it. It's not possible to describe the meaning of X, Y and Z here like is possible for HSL for example, because X, Y and Z simply have no intuitive meaning other than that they can be mathematically transformed to other color models given the right matrix or formula.


<h2>Legal Stuff</h2>

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages
arising from the use of this software.
<p/>
Copyright (c) 2009-2010 by Lode Vandevenne.
<p/>
Note: No images? Get the full manual at <a href="http://sourceforge.net/projects/lodepaint/files/LodePaint_Manual_Full.zip/download">http://sourceforge.net/projects/lodepaint/files/LodePaint_Manual_Full.zip/download/</a>. The full manual with images is released separately because the images filesize is larger than that of the program!
</body>
</html>