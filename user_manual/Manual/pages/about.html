<html>
<head>
<title>LodePaint User Manual</title>
<link rel="stylesheet" href="../stylesheet.css" type="text/css" />
</head>
<body>
<h1>LodePaint User Manual</h1>
<p><a href="../manual.html">Back to main page</a></p>
<h1>About LodePaint</h1>

<h2>About</h2>

LodePaint is a painting program that tries to fill in the gap between simple painting
programs for pixel editing, and professional image editors. LodePaint
has an interface that is more similar to the simple painting programs, but adds a lot more
tools and advanced filters.
<p/>
The focus of this painting program is on drawing
pixel-based paintings, textures, sprites or icons for games, programs or websites. Pixel-precision is considered more
important than anti-aliased effects, for example.
<p/>
The focus of the program is <b>not</b> on publishing, printing, illustrations, posters or professional graphics.
<p/>
The user interface of LodePaint uses a game engine, rather than a classical GUI, in an attempt
to have a simple but flexible GUI that works on multiple platforms. A side effect of this is that
LodePaint requires OpenGL 3D (even for its 2D graphics) hardware accelerated graphics to run, but that should not be
a problem on most of todays computers.
<p/>
And finally, this painting program has some special features that are different than most painting programs. For
example, the alpha channel is treated as an equal channel to R, G and B: opacity is always a different setting.
Zooming and panning on the image can be done even while in the middle of using any tool. HDR images can be edited as well,
and this using 128 bits per pixel.

<p/>
This is what the program looks like as of 2010/07/31:
<p/>
<img src="../images/screenshot.png">

<h2>Design Principles (Philosophy of this program)</h2>

These points describe what design choices are made for this program and what
the purpose mainly is.

<ul>
<li>
  <b>Main Purpose: pixel art, icons, sprites, textures, and computer graphics effects.</b>
</li>
<li>
  <b>Pixel Precision</b>
  It must be easy to edit per-pixel, so the following features are important:
  <ul>
  <li>The arrow keys move the mouse cursor exactly one image pixel.</li>
  <li>There must be a way to easily see the numerical RGBA color value of any pixel without many mouse clicks.</li>
  <li>Always see the numerical coordinates of the pixel where the mouse is somewhere</li>
  <li>See exact edges of the pixel that will be edited by rectangular or single-pixel tools.</li>
  <li>Multiple mouse buttons must be able to draw a separate color.</li>
  <li>It must be easy to switch between color picker and other tool without moving the mouse outside the image, to be able to use the image as a giant palette.</li>
  <li>When doing something over a distance larger than the screen, it must be possible to choose the exact pixel where you start
      and end. It's made possible by being able to zoom and pan while in the middle of using any tool.</li>
  </ul>
</li>
<li><b>Sharp pixels are preferred over anti-aliasing</b>
  This doesn't mean that anti-aliasing features are completely excluded from this program, but where a choice has to be
  made it's made in favor of pixel precision.
</li>
<li>
  <b>Alpha channel</b>
    The alpha channel is important for textures in modern games, and for images on websites, so there must be easy and powerful ways to edit it.
    <ul>
    <li>The alpha channel is always enabled and always there. It's only lost when saving to a file format that doesn't support it.</li>
    <li>The alpha channel can be treated exactly the same as the R, G and B channels, and can be edited by any tool</li>
    <li>Alpha channel and opacity are two different things in LodePaint. Opacity is always a separate setting than the alpha channel of the current tool color.</li>
    </ul>
<li><b>No confusing layers</b> If layers are implemented (they are not yet) then they shouldn't cause confusing behaviour
  when you're not actually using them. That means:
  <ul>
    <li>If you didn't explicitely make multiple layers, there won't appear multiple layers. Floating selections aren't layers, nor are geometric primitives or texts.</li>
    <li>Cropping and resizing should work on the entire image by default, independent of layers, unless a command to do it only on the current layer is explicitely enabled.</li>
    <li>There should never be drawn something that looks like a selection rectangle around a layer or an image. A white/black dotted line is reserved only for selections to avoid confusion.</li>
  </ul>
</li>
<li><b>Handy zooming</b>
  Zooming should work around the mouse cursor instead of around the center of the image, should work at any time
and be easy with the scrollwheel of the mouse. Toolbar buttons and zoom tools might be available for those unconventional mice without scrollwheel, but a user with scrollwheel would rarely need these tools.</li>
<li><b>Tileable images</b>
  There should be features to edit tileable images and textures. For example, the "wrapping" option in various filters, and such operations as "Wrapping shift 50%" and "Repeat"</li>
<li><b>Independent</b> This program is as independent as possible from large programming libraries. No GUI library like GTK+, Qt or MFC is used, nor imaging libraries such as ImageMagick
or compression libraries like zlib or libPNG. Many features are programmed from scratch instead. The libraries that are used are: SDL, OpenGL, optionally Boost, in Windows a small amount of Win32 calls, in Linux a small amount of X11 calls.
</ul>

<h3>Features that aren't the focus of this program.</h3>

The following features are not part of the design goals of this program, or are not important enough for this design to counter their difficulty of implementation.
Some of those missing features have a convenient alternative that is noted in the points below.

<ul>
  <li>
  <b>Drawing Tablets, Touch pads, Multi-Touch and non-conventional mice.</b>
  Sorry, this program supports only a conventional mouse for now. Preferably a mouse with at least a left button, right button, scrollwheel and option to press the scrollwheel (= third button).
  Alternative devices might be supported later though, if portable libraries (preferably for SDL) are found. For now these aren't the focus of the programming effort though.
  </li>
  <li>
  <b>Print option</b>
  It's not really worth it to try to find a portable way to have a Print dialog in this program and send the image to the printer. You can save
  the image to a PNG file with the program, and then print it with the tools available by your operating system.
  </li>
  <li>
  <b>Import Screenshot option</b>
  Not portable enough. But on most operating systems it is already possible to import a screenshot anyway: hit the print screen button of the keyboard, or
  ctrl+print screen or alt+print screen, and then press "CTRL+V" or the "Paste" button in LodePaint and if your operating system
  supports it, the screenshot will be available in LodePaint.
  </li>
  <li>
  <b>Import from scanner</b>
  Not portable enough. But it's easy to edit a scanned image. Use the software that came with the scanner to scan the image, save it to
  a file, and then open this file with LodePaint to edit it.
  </li>
  <li>
  <b>Thumbnail browsing of images</b>
  It's quite a lot of work to implement. Most operating systems have a file manager that supports thumbnail browsing. So it's not really
  the purpose of this painting program to also do that.
  </li>
  <li>
  <b>One-click retouch functions</b>
  This program focuses on "doing the math yourself" and a combination of various general purpose filters instead of specialized single-purpose filters.
  </li>
  <li>
  <b>Clip art data</b>
  A user can use their own favorite preferred clipart database. Effects like a brush that adds coins to the image
  or add a predefined wooden photoframe around it aren't included because these rely on a predefined image data and
  it's not good if everyone creates the same thing...
  </li>
  <li>
  <b>Features such as "Send picture through email", "Upload image to ..."</b>
  LodePaint is an image editor, not an image sender. You can save the image to a file and then use a web browser
  or email client to mail/upload the image, or do anything else with it.
  </li>
  <li>
  <b>Color callibration, device independent colors, ...</b>
  Since the main purpose of this program is to create textures and images for the computer screen,
  plain RGB(A) color is used, to get the RGB(A) color values needed for web images and computer game textures. This program isn't meant to be used for publishing and printing.
  </li>
  <li>
  <b>Dots Per Inch units</b>
  The focus of LodePaint is on editing digital raster images made out of pixels. What the physical size of a pixel is, has no influence
  or meaning in LodePaint, since LodePaint isn't made for publishing or printing. All sizes and distances are measured in pixels. There are no dpi options or units.
  </li>
  <li>
  <b>Editing indexed color images with palette</b>
  For now, when an indexed color image is opened with LodePaint, it is immediatly converted to full color 32-bit RGBA. Editing images in indexed color
  mode, or editing the palette, isn't supported. Most filters and tools couldn't possibly operate on them anyway. LodePaint does have a few
  filters that can reduce the amount of colors in the image (such as dithering, posterization, ...), but they'll still be saved as 32-bit (or 128-bit).
  If palettized images are needed for your purpose, save them as 32-bit with LodePaint, then convert them to indexed color with another program.
  Note that LodePaint will automatically create a PNG with a palette if the image has less than 256 colors, but that is only how the PNG image is encoded,
  when opening it in LodePaint it'll be 32-bit again. Image encoding, and raw pixel format, are two independent things in LodePaint.
  </li>
</ul>

<h3>Acknowledgements</h3>

Portions of LodePaint use open source code or other resources with a liberal license. Here are the acknowledgements and copyright messages for these resources.
<p/>
<b>Radix FFT</b>
<p/>
The code in sing.cpp and sing.h is created by:
<p/>
Javier Soley, Ph. D,   FJSOLEY AT UCRVM2 DOT BITNET
<br/>
Escuela de Fisica y Centro de Investigaciones Geof sicas
<br/>
Universidad de Costa Rica
<p/>
<b>JPEG and TGA support</b>
<p/>
This uses stb_image, public domain library to load different image formats, original source: http://nothings.org/stb_image.c
<p/>
<b>PNG Support</b>
<p/>
This uses the LodePNG library, http://www.lodev.org/lodepng
<p/>
<b>GIF Loader</b>
<p/>
Code to decode GIF files.
<p/>
The code in winimage.cpp and winimage.h is a modified version of a library called WINIMAGE, Copyright (c) 2000, Juan Soulie (jsoulie at cplusplus dot com)
<p/>
<b>LED Icon Set</b>
<p/>
Many icons for the toolbar and paint tools are based on the LED Icon Set: http://led24.de/iconset/


<h2>Legal Stuff</h2>

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages
arising from the use of this software.
<p/>
Copyright (c) 2009-2010 by Lode Vandevenne.
<p/>
Note: No images? Get the full manual at <a href="http://sourceforge.net/projects/lodepaint/files/LodePaint_Manual_Full.zip/download">http://sourceforge.net/projects/lodepaint/files/LodePaint_Manual_Full.zip/download/</a>. The full manual with images is released separately because the images filesize is larger than that of the program!
</body>
</html>