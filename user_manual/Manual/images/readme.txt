This folder contains the images of the LodePaint manual.

If this folder is empty, then you can download the images of the manual from http://sourceforge.net/projects/lodepaint/.

There go to to the download or view all files page, and download LodePaint_Manual_Full.zip to get the full manual with images.

The images of the manual are released separately because these images take more space in disk than the complete LodePaint program itself!