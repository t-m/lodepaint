<h1>Plugins</h1>

This document describes the plugin framework of LodePaint.
<p/>

<h2>Using Plugins</h2>

It is possible to have plugins for LodePaint. There are currently two types of plugins:
<p/>
*) A file format plugin: allows decoding and/or encoding a certain image file type. There are two sub-types of file format plugins: 32-bit (for 32-bit RGBA color images) and 128-bit (for floating point aka HDR images).
<p/>
*) A filter plugin: this type of plugin adds a filter in the "Plugins" menu of LodePaint. Such a filter can operate on all the pixels of the image. Optionally the filter can have a GUI with named parameters. Currently it can't resize the image though but that is planned for the future. So basically a plugin filter can do almost as much as the built-in LodePaint filters, except less flexibility with the GUI of the filter and no resizing yet.
<p/>
To install a plugin, put its DLL file (for Windows) or .so file (for Linux) in the plugins folder of LodePaint. This plugins folder is inside the folder where the executable of LodePaint is. You may require admin rights to be able to place files there.
<p/>
When LodePaint starts up, it scans all files in this "plugins" folder, checks for each file what type of plugin it is, if any, and adds the file types to the file dialog or filters to the plugins menu.

<h2>Programming Plugins</h2>

This section is for developers who wish to make a plugin for LodePaint or add a file format or filter to LodePaint without modifying the original source code of LodePaint.
<p/>
To program the plugins, you need to write the plugins in C or C++ (or another programming language that can create compatible libraries), and create a dynamic link library (preferably both for Linux (*.so) and for Windows (*.dll)). For a file format plugin, you need to write functions to convert from/to the binary file data and an image buffer and some functions to give the names of the file format and extensions and detect if a file is of this format. For a filter plugin, you need to write a function that operates on an image buffer, as well as some optional functions to define the names and types of parameters that the user can see in the GUI of the filter.
<p/>
The dynamic library must have certain exported functions implemented in it with the exact correct name and signature (to be able to load the dynamic library and let LodePaint find these functions in them). Depending on the type of plugin (file format or filter plugin), different functions are required. To find out which function signatures that are: check out the LodePaint source code (currently obtainable from sourceforge.net), and find the file "paint_plugin.h". That file gives a list of the exact function signatures you need to implement. Not every function must be implemented, some are obligatory, others are optional.
<p/>
Other than viewing these functions, the source code of LodePaint isn't required to write such external plugins. Plugins need no dependency at all to LodePaint source code or any of the libraries LodePaint uses. The plugins need to implement only a simple C interface where images are represented as simple unsigned char or float buffers, etc...
<p/>
It's not possible to write multiple file formats or filters in a single plugin, and also not adviced to combine 32-bit and 128-bit files in a single plugin. So every file format and every filter must be a separate plugin.
<p/>
Since the plugin system is quite new in LodePaint, the interface (the required function signatures) might change in the future if some big blooper turns out to be in the current interface, so that might make LodePaint incompatible with older plugins.
<p/>
The LodePaint source code is also supplied with a few example plugins (currently an example 32-bit and 128-bit file format and an example filter). These will easily get you started. The samples are in the folder src/plugin_examples. There's also a readme.txt file there with some extra build-tips, and .dev project files for compiling them with Dev-C++ in Windows (and in Linux you don't need project files for them, a single gcc command is enough there).
<p/>
Some ideas for plugins to develop:
<br/>
*) Image formats I don't want to implement in the core code due to huge library size or legal issues: JPEG2000 reading/writing. GIF saving and writing. JPEG writing. JPEG reading is already supported natively but no progressive ones, so those. OpenEXR reading/writing.

<h2>Example Plugins</h2>

There are 3 example plugins delivered with LodePaint. These are programming examples. The plugins don't do anything that is very useful, but they do appear in the user interface of LodePaint. Here's what they do:
<p/>
*) Example 32-bit file format: this plugin implements a file format with extension ".example32". This is not a file format that exists in real life, but the plugin can save to and load from it. These files are uncompressed so very big. The specification of this file format is very simple, namely: first 7 bytes that make up the word "example". Then 4 bytes that make up the image width. Then 4 bytes that make up the image height. Then 4 * width * height bytes that represent all the RGBA pixels. Big endian.
<p/>
*) Example 128-bit file format: this plugin implements a file format with extension ".example128". This is not a file format that exists in real life, but the plugin can save to and load from it. These files are uncompressed and 16-bit per pixel so they're HUGE. The specification of this file format is very simple, namely: first 7 bytes that make up the word "example". Then 4 bytes that make up the image width. Then 4 bytes that make up the image height. Then 16 * width * height bytes that represent all the RGBA pixels. Big endian. Every pixel exists out of 4 floating point values, and each floating point value is 4 bytes. This file format is only a plugin sample for LodePaint, don't use it, the built-in "float" file format is better because it's compressed.
<p/>
*) Example filter: this is the only filter appearing in the "Plugins" menu by default. This filter generates a 2D sine pattern over the image (so it's a very simplified plasma) with customizable X and Y period. Rarely useful, but nice as demonstration of a plugin.

 
