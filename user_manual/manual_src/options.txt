<h1>Options</h1>

<h2>Settings Dialog</h2>
<p/>
<img src="../images/options.png"/>
<p/>
The settings on the Options dialog under the Settings menu are the following:

<h3>Undo Size (MB)</h3>

LodePaint uses an "unlimited" amount of undo and redo steps, where "unlimited" actually means limited by the memory available in your computer. The amount of memory of your RAM that you want to allocate to LodePaint can be changed with this option. A good value is 200MB, but if you're working on very large images and still want many undo steps even after doing many filters that change every pixel, and have enough RAM, you may want to increase the size with this option.
<p/>
LodePaint uses the RAM of your computer for undo, it doesn't use disk caching.
<p/>
This setting is per image, not for the total of all images together.
<p/>
Here's how LodePaint's undo system consumes memory:
<br/>
<br/>-When using interactive tools, such as the brush, then it saves the pixels in the rectangular area affected by the movement. So the larger the brush stroke or geometric shape, the more undo memory is used.
<br/>-When using filters that are perfectly invertible without information loss, such as Negate RGB, Flip, Rotate 90 degrees, Repeat, etc..., then LodePaint saves no pixels at all, this consumes no memory. It just recalculates the operation in the inverted direction when you press undo.
<br/>-When using filters that affect all pixels of the image in a non invertible way (this are most filters, such as Blur, FFT, Greyscale, Mosaic, ...), it stores the complete image before the operation in RAM. This consumes the most memory.
<br/>-When using such filters on a selection, only the selection is stored.
<p/>
The amount of undo memory currently used can be seen in the bottom left of the main window of LodePaint. When the amount is full, then the oldest undo steps are forgotten.

<h3>Screen BG Color</h3>

The background color of the main window of LodePaint. Change according to your own taste. To change the color, press the small colored square next to the name of this setting and a color chooser dialog will pop up.

<h3>Drawing Window Color</h3>

The background color of the drawing window of LodePaint. Change according to your own taste. To change the color, press the small colored square next to the name of this setting and a color chooser dialog will pop up.

<h3>Window Top Color</h3>

Another GUI stule setting, this changes the color of the top bar of all floating windows inside the LodePaint program.

<h3>Override Plugin Dir</h3>

If this is empty, the directory where LodePaint searches for Plugins, is the Plugins directory where the executable of the program is. In this setting you can enter a different path of a directory to search in for plugins. Use this only in a case where the default Plugins directory can't be used by LodePaint. NOTE: this setting exists only temporarily here, while I investigate a clean way to use plugin and data directories on multiple operating systems.

<h3>Use Native OS File Dialog</h3>

If enabled and supported, the file dialog known by your operating system is used when saving and loading images. If disabled or not supported, a built-in file dialog is used, which may differ from what you know and might be more limited. Currently in Linux a native file dialog isn't supported so the setting has no effect there.

<h3>Brush Dynamics</h3>

This allows changing parameters of the brush while drawing, by using the CTRL key + the scrollwheel while drawing. You can change size, opacity or softness of the brush, hue, saturation, lightness or alpha of the foreground color and/or hue, saturation, lightness or alpha of the background color. There are two changes at the same time possible. Set the second one to "Nothing" if you want only one thing to change while using these dynamics. The numerical value indicates how much effect the scrollwheel should have. This value can be made negative for the opposite effect.

<h2>Configuration file</h2>

This program leaves one settings.txt file with settings on your hard disk once it's been used for the first time. This file is located here:

<ul>
<li>In Windows: %APPDATA%\LodePaint (type this path in Explorer, this brings you to the Application Data folder, where it is depends on your version of Windows and your Windows username). If I try this on a Windows XP coputer, the path is "C:\Documents and Settings\myusername\Application Data\LodePaint". NOTE: the file may be shown simply as "settings" instead of "settings.txt" in Windows.</li>
<li>In Linux: ~/.config/LodePaint</li>
</ul>

In this settings file there are two experimental options that aren't availble in the normal options dialog. Those are:

<ul>
<li>options.screenResizable: default is "1". Set to "0" if for some reason the program has garbled graphics. With some luck, it works if the screen isn't resizable.</li>
<li>options.fullScreen: default is "0". If "1", the program will go in full screen mode when run. This setting can cause problems if "options.screenSizeX" and "options.screenSizeY" have bad values. If you set this to "1", make sure that those two resolution parameters have a resolution that your computer supports for full screen computer games, such as 1280*1024. If you don't know what values to use, better don't touch this setting. In fullscreen mode, the rest of your desktop isn't available as long as LodePaint runs. The fullscreen mode is NOT a way to show the drawing full screen, because the menu bar and tool windows of the painting window will still be visible.</li>
</ul>

