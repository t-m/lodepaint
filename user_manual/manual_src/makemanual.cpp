/*
LodePaint


Copyright (c) 2009-2010 Lode Vandevenne

All rights reserved.



This file is part of LodePaint.



LodePaint is free software: you can redistribute it and/or modify

it under the terms of the GNU General Public License as published by

the Free Software Foundation, either version 3 of the License, or

(at your option) any later version.



LodePaint is distributed in the hope that it will be useful,

but WITHOUT ANY WARRANTY; without even the implied warranty of

MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the

GNU General Public License for more details.



You should have received a copy of the GNU General Public License

along with LodePaint.  If not, see <http://www.gnu.org/licenses/>.

*/

/*
This is the build system for the manual.
It generates all the HTML pages that are in the subfolder "pages" of the manual.
Each HTML page should be created as a .txt file that contains everything between the <body> tags of the HTML.
This "script" attaches a fixed header and footer to the file and saves it as a .html in the correct Manual/pages folder.

To indicate the pages to include, list the names of their .txt files in "list.txt"
For each indicated file, it sticks the files "header.txt", the file, and "footer.txt" together to form one HTML file.
The HTML files are generated in the following directory relative to this one: "../Manual/pages/"
The HTML files expect the images to be in the following directory, relative to the HTML files: "../images/"

Linux compile command (from main LodePaint directory):
g++ manual_src/makemanual.cpp -o makemanual
*/

#include <vector>
#include <string>
#include <fstream>
#include <iostream>

#if defined(_WIN32)
#include <windows.h>
#endif

//returns the size of the file
int getFilesize(const std::string& filename)
{
  using namespace std;
  ifstream file(filename.c_str(), ios::in|ios::binary|ios::ate);

  //get filesize
  file.seekg(0, ios::end);
  int size = file.tellg();
  file.seekg(0, ios::beg);
  size -= file.tellg();

  file.close();

  return size;
}

//write given buffer to the file, overwriting the file, it doesn't append to it.
void saveFile(const std::string& buffer, const std::string& filename)
{
  using namespace std;
  ofstream file(filename.c_str(), ios::out|ios::binary);
  file.write((char*)&buffer[0], buffer.size());
  file.close();
}

/*
read file into string
*/
int loadFile(std::string& buffer, const std::string& filename)
{
  using namespace std;
  buffer.clear();
  int size = getFilesize(filename.c_str());
  buffer.resize(size);

  ifstream file(filename.c_str(), ios::in|ios::binary|ios::ate);
  file.seekg(0, ios::beg);
  file.read((char*)&buffer[0], size);
  file.close();
  return size;
}

void splitLines(std::vector<std::string>& result, const std::string& text)
{
  result.push_back("");
  for(size_t i = 0; i < text.size(); i++)
  {
    if(text[i] == 10) result.push_back("");
    else if(text[i] < 32);
    else result.back() += text[i];
  }
}

std::string getFileNameFilePart(const std::string& filename)
{
  //find last backwards or forwards slash
  int lastslash = -1;
  for(size_t i = 0; i < filename.size(); i++)
  {
    if(filename[i] == '/' || filename[i] == '\\') lastslash = i;
  }

  //find the last dot
  int lastdot = -1;
  for(size_t i = 0; i < filename.size(); i++)
  {
    if(filename[i] == '.') lastdot = i;
  }

  std::string result = filename;
  if(lastdot >= 0 && !(lastslash >= 0 && lastdot < lastslash)) result = result.substr(0, lastdot);
  if(lastslash == (int)result.size() - 1) result = "";
  else if(lastslash >= 0) result = result.substr(lastslash + 1, result.size() - lastslash);

  return result;
}

int main(int argc, char* argv[])
{
  std::string exe_dir = "";
  
#if defined(_WIN32)
  {
    char c[MAX_PATH];
    GetModuleFileName(GetModuleHandle(NULL),c,MAX_PATH);
    std::string s = c;
    exe_dir = s.substr(0, s.find_last_of("\\/")) + "\\";
  }
#else
  if(argc > 0)
  {
    std::string s = argv[0];
    exe_dir = s.substr(0, s.find_last_of("\\/")) + "/";
  }
#endif

  std::string header;
  std::string footer;
  std::string list;
  loadFile(header, "manual_src/header.txt");
  loadFile(footer, "manual_src/footer.txt");
  loadFile(list, "manual_src/list.txt");

  std::vector<std::string> files;
  splitLines(files, list);
  
  for(size_t i = 0; i < files.size(); i++)
  {
    if(files[i].size() < 5) continue; //ignore empty lines or spaces and such. File name is at least 5 chars (something and .txt)
    std::string file;
    loadFile(file, "manual_src/" + files[i]);
    std::string name = getFileNameFilePart(files[i]);
    std::string result = header + "\n" + file + "\n" + footer;
    saveFile(result, "Manual/pages/" + name + ".html");
  }
  
  std::string manual_html;
  loadFile(manual_html, "manual_src/manual.html");
  saveFile(manual_html, "Manual/manual.html");

  std::string css;
  loadFile(css, "manual_src/stylesheet.css");
  saveFile(css, "Manual/stylesheet.css");
}
