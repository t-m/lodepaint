/*
LodePaint

Copyright (c) 2009-2010 Lode Vandevenne
All rights reserved.

This file is part of LodePaint.

LodePaint is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LodePaint is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LodePaint.  If not, see <http://www.gnu.org/licenses/>.
*/


/*
OpenEXR reading and writing support for LodePaint.


To build from within folder in which this .cpp file is:

g++ -shared format_openexr.cpp -o ../../../main_program/Plugins/format_openexr.so -pthread -I/usr/include/OpenEXR -lIlmImf -lz -lImath -lHalf -lIex -lIlmThread

This requires OpenEXR libraries and headers on your system.

For Windows, download OpenEXR from http://www.openexr.com/downloads.html and make the libraries
work with your compiler.

In Archlinux, this requires the packages "openexr" and "ilmbase". If some headers are missing, reinstall ilmbase with pacman.

On other Linux distros, it requires packages called "openexr" and "openexr-devel".

pkg-config --cflags OpenEXR
pkg-config --libs OpenEXR


"The OpenEXR Library compiles fine on the Windows platform with gcc in the Cygwin or MinGW environment.
As far as I remember the only thing I had to change was:
../configure CXXFLAGS=-mno-cygwin -DPLATFORM_WINDOWS=1
On this way I could create a DLL extension for tcl/tk without buying an MS VC7 compiler."
*/
#include <cmath>
#include <cstdlib>
#include <string>
#include <sstream>
#include <stdexcept>
#include <vector>

//OpenEXR headers
#include <ImfHeader.h>
#include <ImfIO.h>
#include <ImfRgbaFile.h>
#include <ImfRgbaFile.h>
#include <ImfArray.h>

#if defined(_WIN32) //windows needs something special
#define DLLEXPORT __declspec(dllexport)
#else
#define DLLEXPORT
#endif

static std::string lastError;

extern "C"
{

DLLEXPORT int getLodePaintPluginType()
{
  return 0;
}

static void strcopy(char* out, int maxstrlen, const char* in)
{
  int i = 0;
  while(in[i] != 0 && i < maxstrlen - 2)
  {
    out[i] = in[i];
    i++;
  }
  out[i] = 0;
}

DLLEXPORT void fileFormatGetDescription(char* name, int maxstrlen)
{
  strcopy(name, maxstrlen, "OpenEXR");
}

DLLEXPORT int fileFormatGetNumExt()
{
  return 1;
}

DLLEXPORT void fileFormatGetExt(char* ext, int maxstrlen, int index)
{
  (void)index;
  strcopy(ext, maxstrlen, "exr");
}

DLLEXPORT int fileFormatSupportsAlpha()
{
  return 0;
}

DLLEXPORT int fileFormatIsLosless()
{
  return 1;
}

DLLEXPORT void getLastError(char* err, int maxstrlen)
{
  strcopy(err, maxstrlen, lastError.c_str());
}

/*
Each file format for LodePaint must have some identification in the file to ensure that the contents of a file
are of that type. The extension cannot be used to identify the file type. The identification should be so that it's
not possible to confuse this file type with other file types.
The rule for this "example" file format is that it must begin with "example", and, the file size must be exactly correct according to the
width and height of the image.
Real-life examples of such identification headers in a file is e.g. the magic bytes at the start of a PNG or JPEG2000 file.
*/
DLLEXPORT int fileFormatIsOfFormat(unsigned char* buffer, unsigned long size)
{
  if(size < 4) return 0;
  if(buffer[0] != 0x76 || buffer[1] != 0x2f
  || buffer[2] != 0x31 || buffer[3] != 0x01)
    return 0;

  return 1;
}


//IStream for in-memory decoding
class MyIStream : public Imf::IStream
{
  private:
  
    unsigned char* buffer;
    unsigned long size;
    Imf::Int64 pos;
  
  public:
  
    MyIStream(unsigned char* buffer, long size)
    : IStream("inmemory")
    , buffer(buffer)
    , size(size)
    , pos(0)
    {
    }

    virtual ~MyIStream ()
    {
    }
    
    virtual bool isMemoryMapped () const
    {
      return true;
    }
    
    virtual bool read (char c[/*n*/], int n)
    {
      if(pos + n > size) throw new std::runtime_error("reading past size");
      
      for(int i = 0; i < n ; i++)
      {
        c[i] = buffer[pos++];
      }
      
      if(pos >= size) return false;
      else return true;
    }
    
    virtual char* readMemoryMapped (int n)
    {
      if(pos + n > size) throw new std::runtime_error("reading past size");
      
      int index = pos;
      pos += n;
      return (char*)&buffer[index];
    }
    
    virtual Imf::Int64 tellg()
    {
      return pos;
    }
    
    virtual void seekg(Imf::Int64 pos)
    {
      this->pos = pos;
    }
};

DLLEXPORT int fileFormatDecodeRGBA32F(float** image, int* width, int* height, unsigned char* buffer, unsigned long size, int* parameters)
{
  using namespace Imf;
  using namespace Imath;
  
  if(!fileFormatIsOfFormat(buffer, size)) return 1;
  
  int debug = 0;
  
  try
  {
    Array2D<Rgba> pixels;
    
    MyIStream istream(buffer, size);
    
    RgbaInputFile file(istream);
    
    Box2i dw = file.dataWindow();
    int u = dw.max.x - dw.min.x + 1;
    int v = dw.max.y - dw.min.y + 1;
    *width = u;
    *height = v;
    pixels.resizeErase(v, u);
    file.setFrameBuffer(&pixels[0][0] - dw.min.x - dw.min.y * u, 1, u);
    file.readPixels (dw.min.y, dw.max.y);
    
    
    (*image) = (float*)malloc(u * v * 4 * sizeof(float));
    
    for(size_t y = 0; y < v; y++)
    for(size_t x = 0; x < u; x++)
    {
      int index = 4 * u * y + 4 * x;
      Rgba* rgba = &pixels[y][x];
      (*image)[index + 0] = rgba->r;
      (*image)[index + 1] = rgba->g;
      (*image)[index + 2] = rgba->b;
      (*image)[index + 3] = rgba->a;
    }

  }
  catch (const std::exception &exc)
  {
    lastError = std::string("exception occured: ") + exc.what();
    return 2;
  }
  
  return 0;
}

//OStream for in-memory decoding
class MyOStream : public Imf::OStream
{
  private:
  
    unsigned long allocsize;
    Imf::Int64 pos;
    
    unsigned char** buffer;
    unsigned long* size;
  
  public:
  
    MyOStream(unsigned char** buffer, unsigned long* size)
    : OStream("inmemory")
    , buffer(buffer)
    , size(size)
    , allocsize(0)
    , pos(0)
    {
      *size = 0;
      *buffer = 0;
    }
    
    virtual ~MyOStream()
    {
    }
    
    void reallocIfNeeded(int totalsize)
    {
      if(totalsize > allocsize)
      {
        allocsize = totalsize * 2;
        *buffer = (unsigned char*)realloc(*buffer, allocsize);
      }
    }

    virtual void write (const char c[/*n*/], int n)
    {
      if(pos + n > *size)
      {
        *size = pos + n;
        reallocIfNeeded(*size);
      }
      
      for(int i = 0; i < n; i++)
      {
        (*buffer)[pos++] = c[i];
      }
    }

    virtual Imf::Int64 tellp()
    {
      return pos;
    }

    virtual void seekp(Imf::Int64 pos)
    {
      this->pos = pos;
      
      if(pos > *size)
      {
        *size = pos;
        reallocIfNeeded(*size);
      }
    }
};

DLLEXPORT int fileFormatEncodeRGBA32F(unsigned char** buffer, unsigned long* size, float* image, int width, int height, int* parameters)
{
  using namespace Imf;
  using namespace Imath;
  
  try
  {
    std::vector<Rgba> pixels;
    pixels.resize(height * width);
    
    for(int y = 0; y < height; y++)
    for(int x = 0; x < width; x++)
    {
      int index = 4 * width * y + 4 * x;
      Rgba* rgba = &pixels[y * width + x];
      rgba->r = image[index + 0];
      rgba->g = image[index + 1];
      rgba->b = image[index + 2];
      rgba->a = image[index + 3];
    }
  
    MyOStream ostream(buffer, size);
    Header header(width, height);
    RgbaOutputFile file (ostream, header, WRITE_RGBA);
    file.setFrameBuffer(&pixels[0], 1, width);
    file.writePixels (height);
  }
  catch (const std::exception &exc)
  {
    lastError = std::string("exception occured: ") + exc.what();
    return 1;
  }

  return 0; //ok
}

DLLEXPORT void pluginFreeUnsignedChar(unsigned char* buffer)
{
  free(buffer);
}

DLLEXPORT void pluginFreeFloat(float* buffer)
{
  free(buffer);
}

} // extern "C"
