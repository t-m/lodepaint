Compilation instructions for DNG reader Plugin for LodePaint

This uses the official DNG SDK from Adobe. Almost nothing changed for it. Expad and a few other dependencies have been added as source directories as well. There are no other external dependenies.

This will be used for a DNG (Digital Negative) plugin for LodePaint.

Here are the compile instructions for Linux.

First the compile commands for separate directories are given, just to provide information about which package needs what flags. At the end everything is put together in one big compile command.

*** Only dng_sdk ***

g++ dng_sdk/*.cpp -DqMacOS=0 -DqWinOS=0 -DqDNGUseStdInt=1 -DqDNGLittleEndian=1 -DUNIX_ENV=1 -Ixmp_sdk/include

*** Only xmp_sdk & md5 ***

g++ xmp_sdk/XMPCore/*.cpp xmp_sdk/common/*.cpp md5/*.cpp -Ixmp_sdk/include -Ixmp_sdk/common -Imd5 -DUNIX_ENV=1 

*** Only expat ***

g++ expat/*.c -DHAVE_MEMMOVE

*** everything (without main function demo program) ***

g++ xmp_sdk/XMPCore/*.cpp xmp_sdk/common/*.cpp md5/*.cpp expat/*.c dng_sdk/*.cpp -DqMacOS=0 -DqWinOS=0 -DqDNGUseStdInt=1 -DqDNGLittleEndian=1 -DHAVE_MEMMOVE -Ixmp_sdk/include -Ixmp_sdk/common -Imd5 -DUNIX_ENV=1

*** everything (with Adobe's Validate example program) ***

g++ xmp_sdk/XMPCore/*.cpp xmp_sdk/common/*.cpp md5/*.cpp expat/*.c dng_sdk/*.cpp -DqMacOS=0 -DqWinOS=0 -DqDNGUseStdInt=1 -DqDNGLittleEndian=1 -DHAVE_MEMMOVE -Ixmp_sdk/include -Ixmp_sdk/common -Imd5 -DUNIX_ENV=1 -DqDNGValidateTarget

*** And finally, compiling the dng plugin for LodePaint, this command is for when you're in the plugins/dng/src folder ***

g++ xmp_sdk/XMPCore/*.cpp xmp_sdk/common/*.cpp md5/*.cpp expat/*.c dng_sdk/*.cpp -DqMacOS=0 -DqWinOS=0 -DqDNGUseStdInt=1 -DqDNGLittleEndian=1 -DHAVE_MEMMOVE -Ixmp_sdk/include -Ixmp_sdk/common -Imd5 -DUNIX_ENV=1 -DqDNGValidateTarget *.cpp -shared -o ../../../main_program/Plugins/format_dng.so

Copyright (c) 2010 by Lode Vandevenne. All rights reserved.