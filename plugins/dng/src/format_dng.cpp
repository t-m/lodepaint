/*
LodePaint

Copyright (c) 2009-2010 Lode Vandevenne
All rights reserved.

This file is part of LodePaint.

LodePaint is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LodePaint is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LodePaint.  If not, see <http://www.gnu.org/licenses/>.
*/


/*
DNG reader Plugin for LodePaint. Uses Adobe DNG SDK. Also includes Expat.



g++ xmp_sdk/XMPCore/*.cpp xmp_sdk/common/*.cpp md5/*.cpp expat/*.c dng_sdk/*.cpp -DqMacOS=0 -DqWinOS=0 -DqDNGUseStdInt=1 -DqDNGLittleEndian=1 -DHAVE_MEMMOVE -Ixmp_sdk/include -Ixmp_sdk/common -Imd5 -DUNIX_ENV=1 -DqDNGValidateTarget *.cpp -shared -o ../../../main_program/Plugins/format_dng.so

*/



#include <cmath>
#include <cstdlib>
#include <string>
#include <sstream>
#include <stdexcept>
#include <vector>
#include <iostream>

//DNG headers
#include "dng_sdk/dng_color_space.h"
#include "dng_sdk/dng_date_time.h"
#include "dng_sdk/dng_exceptions.h"
#include "dng_sdk/dng_memory.h"
#include "dng_sdk/dng_memory_stream.h"
#include "dng_sdk/dng_globals.h"
#include "dng_sdk/dng_host.h"
#include "dng_sdk/dng_ifd.h"
#include "dng_sdk/dng_image_writer.h"
#include "dng_sdk/dng_info.h"
#include "dng_sdk/dng_linearization_info.h"
#include "dng_sdk/dng_mosaic_info.h"
#include "dng_sdk/dng_negative.h"
#include "dng_sdk/dng_preview.h"
#include "dng_sdk/dng_render.h"
#include "dng_sdk/dng_simple_image.h"
#include "dng_sdk/dng_tag_codes.h"
#include "dng_sdk/dng_tag_types.h"
#include "dng_sdk/dng_tag_values.h"
#include "dng_sdk/dng_xmp.h"
#include "dng_sdk/dng_xmp_sdk.h"

#if defined(_WIN32) //windows needs something special
#define DLLEXPORT __declspec(dllexport)
#else
#define DLLEXPORT
#endif


class dng_buffer_stream: public dng_stream
{
  
  private:
  
    unsigned char *fFile;
    uint64 size;
  
  public:
  
    dng_buffer_stream (unsigned char* buffer, uint64 size)
    : dng_stream ((dng_abort_sniffer *) NULL,
            size,
            0)
    , fFile(buffer)
    , size(size)
    {
    }
    
    virtual ~dng_buffer_stream ()
    {
    }
  
  protected:
  
    virtual uint64 DoGetLength ()
    {
      return size;
    }
  
    virtual void DoRead (void *data,uint32 count, uint64 offset)
    {
      for(uint32 i = 0; i < count; i++)
      {
        ((unsigned char*)(data))[i] = fFile[offset + i];
      }
    }
    
    virtual void DoWrite (const void *data, uint32 count, uint64 offset)
    {
      for(uint32 i = 0; i < count; i++)
      {
        fFile[offset + i] = ((unsigned char*)(data))[i];
      }
    }
    
  private:
  
    // Hidden copy constructor and assignment operator.
  
    dng_buffer_stream (const dng_buffer_stream &stream) { (void)stream; }
    dng_buffer_stream & operator= (const dng_buffer_stream &stream) { (void) stream; }
};




static std::string lastError;

extern "C"
{

DLLEXPORT int getLodePaintPluginType()
{
  return 0;
}

static void strcopy(char* out, int maxstrlen, const char* in)
{
  int i = 0;
  while(in[i] != 0 && i < maxstrlen - 2)
  {
    out[i] = in[i];
    i++;
  }
  out[i] = 0;
}

DLLEXPORT void fileFormatGetDescription(char* name, int maxstrlen)
{
  strcopy(name, maxstrlen, "Digital Negative");
}

DLLEXPORT int fileFormatGetNumExt()
{
  return 1;
}

DLLEXPORT void fileFormatGetExt(char* ext, int maxstrlen, int index)
{
  (void)index;
  strcopy(ext, maxstrlen, "dng");
}

DLLEXPORT int fileFormatSupportsAlpha()
{
  return false;
}

DLLEXPORT int fileFormatIsLosless()
{
  return true;
}

DLLEXPORT void getLastError(char* err, int maxstrlen)
{
  strcopy(err, maxstrlen, lastError.c_str());
}



/*
Each file format for LodePaint must have some identification in the file to ensure that the contents of a file
are of that type. The extension cannot be used to identify the file type. The identification should be so that it's
not possible to confuse this file type with other file types.
The rule for this "example" file format is that it must begin with "example", and, the file size must be exactly correct according to the
width and height of the image.
Real-life examples of such identification headers in a file is e.g. the magic bytes at the start of a PNG or JPEG2000 file.
*/
DLLEXPORT int fileFormatIsOfFormat(unsigned char* buffer, unsigned long size)
{
  //before doing the heavy stuff, first of all check for the TIFF magic bytes
  
  if(size < 4) return 0;
  
  if(!(( buffer[0] == 0x49 && buffer[1] == 0x49 && buffer[2] == 0x2a && buffer[3] == 0x00)
   || ( buffer[0] == 0x4d && buffer[1] == 0x4d && buffer[2] == 0x00 && buffer[3] == 0x2a))) return 0;
  
  
  try
  {
    dng_buffer_stream stream (buffer, size);
    dng_host host;
    dng_info info;
    
    info.Parse (host, stream);
    info.PostParse (host);

    if (!info.IsValidDNG ())
    {
      return 0;
    }
    return 1;
  }
  catch (const std::exception &exc)
  {
    lastError = exc.what();
    return 0;
  }
  catch(...)
  {
    return 0;
  }
}

DLLEXPORT int fileFormatDecodeRGBA32F(float** image, int* width, int* height, unsigned char* buffer, unsigned long size, int* parameters)
{

  if(!fileFormatIsOfFormat(buffer, size))
  {
    lastError = "File is not a DNG file";
    return 1;
  }
  static uint32 gMathDataType = ttShort;
  
  static bool gFourColorBayer = false;
      
  static int32 gMosaicPlane = -1;
  
  static uint32 gPreferredSize = 0;
  static uint32 gMinimumSize   = 0;
  static uint32 gMaximumSize   = 0;
  static const dng_color_space *gFinalSpace = &dng_space_sRGB::Get ();
  static uint32 gFinalPixelType = ttByte;
  static dng_string gDumpStage1;
  static dng_string gDumpStage2;
  static dng_string gDumpStage3;
  static dng_string gDumpTIF;
  static dng_string gDumpDNG;
  
  
  try
  {
    dng_buffer_stream stream (buffer, size);
    
    
     dng_host host;
     dng_info info;

    host.SetPreferredSize (gPreferredSize);
    host.SetMinimumSize   (gMinimumSize  );
    host.SetMaximumSize   (gMaximumSize  );
    
    host.ValidateSizes ();
    
    if (host.MinimumSize ())
    {
      host.SetForPreview (true);
      gDumpDNG.Clear ();
    }
      
    if (gDumpDNG.NotEmpty ())
    {
      host.SetSaveDNGVersion (dngVersion_SaveDefault);
      host.SetSaveLinearDNG (false);
      host.SetKeepOriginalFile (true);
    }
      
    // Read into the negative.
    
    AutoPtr<dng_negative> negative;
    
      {
      
      dng_info info;
      
      info.Parse (host, stream);
      
      info.PostParse (host);
      
      if (!info.IsValidDNG ())
      {
        lastError = "Not a valid DNG file";
        return 1;
      }
        
      negative.Reset (host.Make_dng_negative ());
      
      negative->Parse (host, stream, info);
      
      negative->PostParse (host, stream, info);
      
      {
        //dng_timer timer ("Raw image read time");

        negative->ReadStage1Image (host, stream, info);
      }
        
      negative->ValidateRawImageDigest (host);
        
      }
      
    // Metadata.
      
    negative->SynchronizeMetadata ();
    
    // Four color Bayer option.
    
    if (gFourColorBayer)
    {
      negative->SetFourColorBayer ();
    }
      
    // Build stage 2 image.
    
      {
      
      //dng_timer timer ("Linearization time");
      
      negative->BuildStage2Image (host,
                      gMathDataType);
      }
      
    // Build stage 3 image.
      
      {
      
      //dng_timer timer ("Interpolate time");
    
      negative->BuildStage3Image (host,
                      gMosaicPlane);
              
      }


    dng_render render(host, *negative.Get());
    render.SetFinalPixelType(ttFloat);
    dng_image& img = *render.Render();

    size_t u = img.Width();
    size_t v = img.Height();

    
    *width = u;
    *height = v;
  
  
  
     (*image) = (float*)malloc(u * v * 4 * sizeof(float));
     
     dng_const_tile_buffer b(img, img.Size());
     float* c = (float*)b.ConstPixel (0,0);
    
    
    for(size_t y = 0; y < v; y++)
    for(size_t x = 0; x < u; x++)
    {
      int index = 4 * u * y + 4 * x;
      float* rgba = &c[(index/4) * 3];
      (*image)[index + 0] = rgba[0];
      (*image)[index + 1] = rgba[1];
      (*image)[index + 2] = rgba[2];
      (*image)[index + 3] = 1;//rgba[3];
    }

  }
  catch (const std::exception &exc)
  {
    lastError = exc.what();
    return 2;
  }

  return 0;
}

DLLEXPORT void pluginFreeUnsignedChar(unsigned char* buffer)
{
  free(buffer);
}

DLLEXPORT void pluginFreeFloat(float* buffer)
{
  free(buffer);
}

} // extern "C"

