/*
LodePaint

Copyright (c) 2009-2010 Lode Vandevenne
All rights reserved.

This file is part of LodePaint.

LodePaint is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LodePaint is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LodePaint.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <jpeglib.h>
#include <setjmp.h>
#include <jerror.h>
#include <stdio.h>
#include <string.h>

/*
Plugin for libjpeg support for LodePaint.

LodePaint has some built-in jpeg support in the core code, but that doesn't support
progressive jpeg files and no encoding.

If this plugin is loaded by LodePaint, it overrides the built-in JPEG support to
have more complete JPEG support.

NOTE: currently it can only decode, encoding is todo.

gcc -shared format_libjpeg.c -o ../../../main_program/Plugins/format_libjpeg.so -ljpeg -O3

*/

#if defined(_WIN32) /*windows needs something special*/
#define DLLEXPORT __declspec(dllexport)
#else
#define DLLEXPORT
#endif

/*
There's a function "jpeg_mem_src" in libjpeg. This is needed to load a JPEG file
from memory instead of using a path to a file on disk.

However this "jpeg_mem_src" isn't available in older versions of jpeglib or
can't be found by the linker on windows or whatever.

Anyway, in conclusion, here's a copy of "jpeg_mem_src", called "my_jpeg_mem_src"
that is available directly in here so it can find it on any system.
*/
void my_init_mem_source (j_decompress_ptr cinfo)
{
  (void)cinfo;
}
boolean my_fill_mem_input_buffer (j_decompress_ptr cinfo)
{
  static JOCTET mybuffer[4];
  mybuffer[0] = (JOCTET) 0xFF;
  mybuffer[1] = (JOCTET) JPEG_EOI;

  cinfo->src->next_input_byte = mybuffer;
  cinfo->src->bytes_in_buffer = 2;

  return 1;
}
void my_skip_input_data (j_decompress_ptr cinfo, long num_bytes)
{
  struct jpeg_source_mgr * src = cinfo->src;

  if (num_bytes > 0) {
    while (num_bytes > (long) src->bytes_in_buffer) {
      num_bytes -= (long) src->bytes_in_buffer;
      (void) (*src->fill_input_buffer) (cinfo);
    }
    src->next_input_byte += (size_t) num_bytes;
    src->bytes_in_buffer -= (size_t) num_bytes;
  }
}
void my_term_source (j_decompress_ptr cinfo)
{
  (void)cinfo;
}
/*returns 0 if no error, other value if error*/
int my_jpeg_mem_src (j_decompress_ptr cinfo, unsigned char * inbuffer, unsigned long insize)
{
  struct jpeg_source_mgr * src;

  if (inbuffer == NULL || insize == 0)  /* Treat empty input as fatal error */
    return 1;

  if (cinfo->src == NULL) {  /* first time for this JPEG object? */
    cinfo->src = (struct jpeg_source_mgr *)
      (*cinfo->mem->alloc_small) ((j_common_ptr) cinfo, JPOOL_PERMANENT,
        sizeof(struct jpeg_source_mgr));
  }

  src = cinfo->src;
  src->init_source = my_init_mem_source;
  src->fill_input_buffer = my_fill_mem_input_buffer;
  src->skip_input_data = my_skip_input_data;
  src->resync_to_restart = jpeg_resync_to_restart; /* use default method */
  src->term_source = my_term_source;
  src->bytes_in_buffer = (size_t) insize;
  src->next_input_byte = (JOCTET *) inbuffer;

  return 0;
}

/* ////////////////////////////////////////////////////////////////////////// */


#define OUTPUT_BUF_SIZE 4096 /* choose an efficiently fwrite’able size */

/* Expanded data destination object for memory output */

typedef struct
{
  struct jpeg_destination_mgr pub; /* public fields */
  unsigned char ** outbuffer; /* target buffer */
  unsigned long * outsize;
  unsigned char * newbuffer; /* newly allocated buffer */
  JOCTET * buffer; /* start of buffer */
  size_t bufsize;
} my_mem_destination_mgr;

typedef my_mem_destination_mgr * my_mem_dest_ptr;

void init_mem_destination (j_compress_ptr cinfo) { /* no work necessary here */ }

boolean empty_mem_output_buffer (j_compress_ptr cinfo)
{
  size_t nextsize;
  JOCTET * nextbuffer;
  my_mem_dest_ptr dest = (my_mem_dest_ptr) cinfo->dest;
  /* Try to allocate new buffer with double size */
  nextsize = dest->bufsize * 2;
  nextbuffer = (JOCTET *)malloc(nextsize);

  if (nextbuffer == NULL) ERREXIT1(cinfo, JERR_OUT_OF_MEMORY, 10);
  memcpy(nextbuffer, dest->buffer, dest->bufsize);
  if (dest->newbuffer != NULL) free(dest->newbuffer);
  dest->newbuffer = nextbuffer;
  dest->pub.next_output_byte = nextbuffer + dest->bufsize; dest->pub.free_in_buffer = dest->bufsize;
  dest->buffer = nextbuffer; dest->bufsize = nextsize;
  return TRUE;
}

void term_mem_destination (j_compress_ptr cinfo)
{
  my_mem_dest_ptr dest = (my_mem_dest_ptr) cinfo->dest;
  *dest->outbuffer = dest->buffer;
  *dest->outsize = dest->bufsize - dest->pub.free_in_buffer;
}

void my_jpeg_mem_dest (struct jpeg_compress_struct* cinfo, unsigned char ** outbuffer, unsigned long * outsize)
{
  my_mem_dest_ptr dest;
  if (outbuffer == NULL || outsize == NULL) /* sanity check */ ERREXIT(cinfo, JERR_BUFFER_SIZE);

  /* The destination object is made permanent so that multiple JPEG images * can be written to the same buffer without re-executing jpeg_mem_dest. */
  if (cinfo->dest == NULL)
  { /* first time for this JPEG object? */
    cinfo->dest = (struct jpeg_destination_mgr *) (*cinfo->mem->alloc_small) ((j_common_ptr) cinfo, JPOOL_PERMANENT, sizeof(my_mem_destination_mgr));
  }
  dest = (my_mem_dest_ptr) cinfo->dest;
  dest->pub.init_destination = init_mem_destination;
  dest->pub.empty_output_buffer = empty_mem_output_buffer;
  dest->pub.term_destination = term_mem_destination;
  dest->outbuffer = outbuffer; dest->outsize = outsize;
  dest->newbuffer = NULL;
  if (*outbuffer == NULL || outsize == 0)
  {
    /* Allocate initial buffer */
    dest->newbuffer = *outbuffer = (unsigned char*)malloc(OUTPUT_BUF_SIZE);
    if (dest->newbuffer == NULL) ERREXIT1(cinfo, JERR_OUT_OF_MEMORY, 10);
    *outsize = OUTPUT_BUF_SIZE;
  }
  dest->pub.next_output_byte = dest->buffer = *outbuffer;
  dest->pub.free_in_buffer = dest->bufsize = *outsize;
}


/* ////////////////////////////////////////////////////////////////////////// */

static char lastError[1024];

DLLEXPORT int getLodePaintPluginType()
{
  return 0;
}

static void strcopy(char* out, int maxstrlen, const char* in)
{
  int i = 0;
  while(in[i] != 0 && i < maxstrlen - 2)
  {
    out[i] = in[i];
    i++;
  }
  out[i] = 0;
}

DLLEXPORT void fileFormatGetDescription(char* name, int maxstrlen)
{
  strcopy(name, maxstrlen, "JPEG with libjpeg plugin");
}

DLLEXPORT int fileFormatGetNumExt()
{
  return 2;
}

DLLEXPORT void fileFormatGetExt(char* ext, int maxstrlen, int index)
{
  if(index == 0) strcopy(ext, maxstrlen, "jpg");
  else strcopy(ext, maxstrlen, "jpeg");
}

DLLEXPORT int fileFormatSupportsAlpha()
{
  return 0;
}

DLLEXPORT int fileFormatIsLosless()
{
  return 0;
}

DLLEXPORT void getLastError(char* err, int maxstrlen)
{
  int maxlen = maxstrlen;
  if(maxstrlen > 1024) maxlen = 1024;
  strcopy(err, maxlen, lastError);
}

/*
Each file format for LodePaint must have some identification in the file to ensure that the contents of a file
are of that type. The extension cannot be used to identify the file type. The identification should be so that it's
not possible to confuse this file type with other file types.
The rule for this "example" file format is that it must begin with "example", and, the file size must be exactly correct according to the
width and height of the image.
Real-life examples of such identification headers in a file is e.g. the magic bytes at the start of a PNG or JPEG2000 file.
*/
DLLEXPORT int fileFormatIsOfFormat(unsigned char* buffer, unsigned long size)
{
  if(size < 4) return 0;
  if(buffer[0] != 0xff || buffer[1] != 0xd8 || buffer[size - 2] != 0xff || buffer[size - 1] != 0xd9) return 0;
  return 1;
}

struct my_error_mgr {
  struct jpeg_error_mgr pub;  /* "public" fields */

  jmp_buf setjmp_buffer;  /* for return to caller */
};

typedef struct my_error_mgr * my_error_ptr;

METHODDEF(void)
my_error_exit (j_common_ptr cinfo)
{
  /* cinfo->err really points to a my_error_mgr struct, so coerce pointer */
  my_error_ptr myerr = (my_error_ptr) cinfo->err;

  /* Always display the message. */
  /* We could postpone this until after returning, if we chose. */
  (*cinfo->err->format_message) (cinfo, lastError);

  /* Return control to the setjmp point */
  longjmp(myerr->setjmp_buffer, 1);
}

/* ************************************************************************** */

DLLEXPORT int fileFormatDecodeRGBA8(unsigned char** image, int* width, int* height, unsigned char* ibuffer, unsigned long size, int* parameters)
{
  struct jpeg_decompress_struct cinfo;  /*the jpeg decompress info*/
  struct my_error_mgr jerr;           /*the error handler*/
  
  int x, y, u, v, i, j, numchannels;
  unsigned char* data;
  unsigned char* pdata;
  unsigned char** ppdata;
  JSAMPARRAY samparray;

  

  cinfo.err = jpeg_std_error(&jerr.pub);
  jerr.pub.error_exit = my_error_exit;
  
  if (setjmp(jerr.setjmp_buffer)) {
    /* If we get here, the JPEG code has signaled an error.
     * We need to clean up the JPEG object, close the input file, and return.
     */
    jpeg_destroy_decompress(&cinfo);
    return 1;
  }
  
  jpeg_create_decompress(&cinfo);       /*sets info to all the default stuff*/

  if(my_jpeg_mem_src(&cinfo, ibuffer, size))
  {
    strcopy(lastError, 1024, "JPEG input empty");
    return 1;
  }

  jpeg_read_header(&cinfo, TRUE);   /*tell it to start reading it*/

  jpeg_start_decompress(&cinfo);    /*decompress the file*/

  /*set the x and y*/
  u = cinfo.output_width;
  v = cinfo.output_height;
  *width = u;
  *height = v;
  numchannels = cinfo.output_components;

  /*read turn the uncompressed data into something ogl can read*/
  data = (unsigned char*)malloc(u * v * numchannels);      /*setup data for the data its going to be handling*/
  pdata = data;
  ppdata = &pdata;
  
  /* Make a one-row-high sample array that will go away when done with image */
  samparray = (*cinfo.mem->alloc_sarray)((j_common_ptr) &cinfo, JPOOL_IMAGE, u * numchannels, 1);

  while(cinfo.output_scanline < cinfo.output_height)
  {
    int numlines = jpeg_read_scanlines(&cinfo, samparray, 1);

    for(i = 0; i < u * numchannels; i++) (*ppdata)[i] = samparray[0][i];
    
    *ppdata += numchannels * cinfo.output_width;
  }
  
  (*image) = (unsigned char*)malloc(u * v * 4);
  
  if(numchannels == 1)
  {
    for(y = 0; y < v; y++)
    for(x = 0; x < u; x++)
    {
      int index = 4 * u * y + 4 * x;
      int index2 = numchannels * u * y + numchannels * x;
      (*image)[index + 0] = data[index2];
      (*image)[index + 1] = data[index2];
      (*image)[index + 2] = data[index2];
      (*image)[index + 3] = 255;
    }
  }
  else if(numchannels == 2)
  {
    for(y = 0; y < v; y++)
    for(x = 0; x < u; x++)
    {
      int index = 4 * u * y + 4 * x;
      int index2 = numchannels * u * y + numchannels * x;
      (*image)[index + 0] = data[index2 + 0];
      (*image)[index + 1] = data[index2 + 0];
      (*image)[index + 2] = data[index2 + 0];
      (*image)[index + 3] = data[index2 + 1];;
    }
  }
  else if(numchannels == 3)
  {
    for(y = 0; y < v; y++)
    for(x = 0; x < u; x++)
    {
      int index = 4 * u * y + 4 * x;
      int index2 = numchannels * u * y + numchannels * x;
      (*image)[index + 0] = data[index2 + 0];
      (*image)[index + 1] = data[index2 + 1];
      (*image)[index + 2] = data[index2 + 2];
      (*image)[index + 3] = 255;
    }
  }
  else
  {
    for(y = 0; y < v; y++)
    for(x = 0; x < u; x++)
    {
      int index = 4 * u * y + 4 * x;
      int index2 = numchannels * u * y + numchannels * x;
      (*image)[index + 0] = data[index2 + 0];
      (*image)[index + 1] = data[index2 + 1];
      (*image)[index + 2] = data[index2 + 2];
      (*image)[index + 3] = data[index2 + 3];
    }
  }
  
  free(data);


  jpeg_finish_decompress(&cinfo);   /*finish decompressing this file*/

  return 0; /*ok*/
}

/* ////////////////////////////////////////////////////////////////////////// */

DLLEXPORT int fileFormatEncodeRGBA8(unsigned char** buffer, unsigned long* size, unsigned char* image, int width, int height, int* parameters)
{
  struct jpeg_compress_struct cinfo;
  struct my_error_mgr jerr;
  
  
  /* this is a pointer to one row of image data */
  JSAMPROW row_pointer[1];
  
  *buffer = 0;
  *size = 0;
  
  
  cinfo.err = jpeg_std_error(&jerr.pub);
  jerr.pub.error_exit = my_error_exit;
  if (setjmp(jerr.setjmp_buffer)) {
    return 1;
  }
  
  jpeg_create_compress(&cinfo);

  my_jpeg_mem_dest(&cinfo, buffer, size);

  /* Setting the parameters of the output file here */
  cinfo.image_width = width;  
  cinfo.image_height = height;
  cinfo.input_components = 4;
  /*cinfo.in_color_space = JCS_RGB;*/ /*this gives "Bogus input colorspace" error...*/
  
  /* default compression parameters, we shouldn't be worried about these */

  jpeg_set_defaults( &cinfo );
  jpeg_set_quality(&cinfo, 80, TRUE);

  /* Now do the compression .. */
  jpeg_start_compress( &cinfo, TRUE );
  /* like reading a file, this time write one row at a time */

  while( cinfo.next_scanline < cinfo.image_height )
  {
    row_pointer[0] = &image[ cinfo.next_scanline * cinfo.image_width *  cinfo.input_components];
    jpeg_write_scanlines( &cinfo, row_pointer, 1 );
  }

  /* similar to read file, clean up after we're done compressing */
  jpeg_finish_compress( &cinfo );

  jpeg_destroy_compress( &cinfo );


  return 0; /*ok*/
}

DLLEXPORT void pluginFreeUnsignedChar(unsigned char* buffer)
{
  free(buffer);
}

DLLEXPORT void pluginFreeFloat(float* buffer)
{
  free(buffer);
}
