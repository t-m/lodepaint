/*
LodePaint

Copyright (c) 2009 Lode Vandevenne
All rights reserved.

This file is part of LodePaint.

LodePaint is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LodePaint is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LodePaint.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "paint_clipboard.h"

#include "lpi/lpi_os.h"

#if defined(LPI_OS_WINDOWS)

#include "Windows.h"
#include "imfmt/imageformats.h"
#include <iostream>
#include <SDL/SDL.h> //for Uint32

#ifndef CF_DIBV5
#define CF_DIBV5 17
#endif

struct PrivateHeader
{
  static const Uint32 ID = 5849684; //random keypad combination
  Uint32 size; //size of the clipboard data, including this header
  Uint32 id; //recognise LodePaint
  Uint32 colorMode; //0=RGBA8, 1=RGBA32F, 2=GREY8, 3=GREY32F
  Uint32 w;
  Uint32 h;
};

bool getClipboardImageRGBA8(std::vector<unsigned char>& image, int& w, int& h)
{
  if(IsClipboardFormatAvailable(CF_PRIVATEFIRST))
  {
    OpenClipboard(NULL);
    unsigned char* data = (unsigned char*)GetClipboardData(CF_PRIVATEFIRST);
    PrivateHeader& header = (PrivateHeader&)(*data);
    unsigned char* pixels = data + sizeof(PrivateHeader);
    if(header.id != PrivateHeader::ID) return false; //not from us

    w = header.w;
    h = header.h;
    std::vector<unsigned char> v(w * h * 4);

    CloseClipboard();
    image.resize(w * h * 4);
    for(size_t i = 0; i < w * h; i++)
    {
      image[i * 4 + 0] = pixels[i * 4 + 0];
      image[i * 4 + 1] = pixels[i * 4 + 1];
      image[i * 4 + 2] = pixels[i * 4 + 2];
      image[i * 4 + 3] = pixels[i * 4 + 3];
    }

    return true;
  }
  else if(IsClipboardFormatAvailable(CF_BITMAP))
  {
    OpenClipboard(NULL);
    HBITMAP hbitmap = (HBITMAP)GetClipboardData(CF_BITMAP);

    BITMAPINFO info;
    ZeroMemory(&info,sizeof(BITMAPINFO));
    info.bmiHeader.biSize=sizeof(BITMAPINFOHEADER);
    HDC hdc = CreateCompatibleDC(0);
    //HDC hdc = GetDC(0);

    GetDIBits(hdc, hbitmap, 0, 0, 0, &info, DIB_RGB_COLORS);

    w = info.bmiHeader.biWidth;
    h = info.bmiHeader.biHeight;
    std::vector<unsigned char> v(w * h * 4);

    info.bmiHeader.biBitCount = 32;
    if(info.bmiHeader.biHeight > 0) info.bmiHeader.biHeight = -info.bmiHeader.biHeight;

    int result = GetDIBits(hdc, hbitmap, 0, h, &v[0], &info, DIB_RGB_COLORS);
    CloseClipboard();
    image.resize(w * h * 4);
    for(size_t i = 0; i < w * h; i++)
    {
      image[i * 4 + 0] = v[i * 4 + 2];
      image[i * 4 + 1] = v[i * 4 + 1];
      image[i * 4 + 2] = v[i * 4 + 0];
      //The alpha channel in the BMP does not contain the information you want! 0 when copypasting from GIMP, something strange but intersting when doing "print screen" in Windows. TODO: use PNG support if supported and BMP here only as fallback (see http://www.microsoft.com/communities/newsgroups/list/en-us/default.aspx?pg=2&p=1&tid=64a90f25-9dd5-4d27-8d69-02993d0a35ed&dg=microsoft.public.dotnet.framework.drawing&cat=en_us_1ec4e19a-545f-4aca-8a90-871ca6a0d724&lang=en&cr=us&sloc=)
      image[i * 4 + 3] = 255; //v[i * 4 + 3];
    }

    return result != 0;
  }
  return false;
}

bool setClipboardImageRGBA8(const unsigned char* image, int w, int h, int w2)
{
  if (OpenClipboard(NULL))
  {
    HGLOBAL hClipboardData = GlobalAlloc(GMEM_DDESHARE, w * h * 4 + sizeof(PrivateHeader));

    unsigned char* data = (unsigned char*)GlobalLock(hClipboardData);

    PrivateHeader& header = (PrivateHeader&)(*data);
    unsigned char* pixels = data + sizeof(PrivateHeader);

    header.w = w;
    header.h = h;
    header.id = PrivateHeader::ID;
    header.size = w * h * 4 + sizeof(PrivateHeader);
    header.colorMode = 0;
    for(int y = 0; y < h; y++)
    for(int x = 0; x < w; x++)
    {
      size_t index = 4 * w * y + 4 * x;
      size_t index2 = 4 * w2 * y + 4 * x;
      pixels[index + 0] = image[index2 + 0];
      pixels[index + 1] = image[index2 + 1];
      pixels[index + 2] = image[index2 + 2];
      pixels[index + 3] = image[index2 + 3];
    }

    GlobalUnlock(hClipboardData);

    EmptyClipboard();
    SetClipboardData(CF_PRIVATEFIRST, hClipboardData);
    //TODO: also make a RGB CF_BITMAP image here for other apps. Note that alpha channel doesn't work in the CF_BITMAP format.
    CloseClipboard();

    return true;
  }



//  if (OpenClipboard(NULL))
//  {
//    std::vector<unsigned char> vec(w * h * 4 + sizeof(PrivateHeader));
//    unsigned char* data = &vec[0];
//
//    PrivateHeader& header = (PrivateHeader&)(*data);
//    unsigned char* pixels = data + sizeof(PrivateHeader);
//
//    header.w = w;
//    header.h = h;
//    header.id = PrivateHeader::ID;
//    for(int y = 0; y < h; y++)
//    for(int x = 0; x < w; x++)
//    {
//      size_t index = 4 * w * y + 4 * x;
//      size_t index2 = 4 * w2 * y + 4 * x;
//      pixels[index + 0] = image[index2 + 0];
//      pixels[index + 1] = image[index2 + 1];
//      pixels[index + 2] = image[index2 + 2];
//      pixels[index + 3] = image[index2 + 3];
//    }
//
//    EmptyClipboard();
//    SetClipboardData(CF_PRIVATEFIRST, data);
//    CloseClipboard();
//
//    return true;
//  }


//  if (OpenClipboard(NULL))
//  {
//    HBITMAP bitmap = CreateBitmap(w, h, 4, 32, (void*)(&image[0]));
//
//    EmptyClipboard();
//    SetClipboardData(CF_BITMAP, bitmap);
//    CloseClipboard();
//
//    //DeleteObject(bitmap);
//
//    return true;
//  }

  return false;
}

//bool getClipboardImageRGBA8(std::vector<unsigned char>& image, int& w, int& h)
//{
//  if (IsClipboardFormatAvailable(CF_DIB))
//  {
//    BOOL result = OpenClipboard(NULL);
//std::cout<<"sizeof(BITMAPINFO)"<<sizeof(BITMAPINFO)<<std::endl;
//    GLOBALHANDLE handle = GetClipboardData(CF_DIB);
//    LPBITMAPINFO lpBI = (LPBITMAPINFO)GlobalLock(handle);
//    BITMAPINFOHEADER& bih = lpBI->bmiHeader;
//
//    size_t numpal = 0;
//    if(bih.biBitCount < 8) numpal = 1 << bih.biBitCount;
//    if(bih.biClrUsed != 0) numpal = bih.biClrUsed;
//    size_t rowsize = 4 * ((bih.biWidth * bih.biBitCount + 31) / 32);
//    size_t imagesize = rowsize * bih.biHeight;
//    if(bih.biCompression != BI_RGB) imagesize = bih.biSizeImage;
//    size_t dibsize = sizeof(BITMAPINFOHEADER) + 4 * numpal + imagesize;
//std::cout<<dibsize<<" "<<sizeof(BITMAPINFOHEADER)<<" "<<numpal<<" "<<rowsize<<" "<<" "<<bih.biBitCount<<" "<<bih.biSizeImage<<std::endl;
//std::cout<<"dimensions: "<<bih.biWidth<<" "<<bih.biHeight<<std::endl;
//std::cout<<"going to make vector of size "<<dibsize/1024<<" K"<<std::endl;
//    std::vector<unsigned char> dib(dibsize);
//std::cout<<"vectormade"<<std::endl;
//    for(size_t i = 0; i < dibsize; i++) dib[i] = ((unsigned char*)(&bih))[i];
//std::cout<<"data copied"<<std::endl;
//
//    std::string error;
//    lpi::decodeImageFile(error, image, w, h, dib, IF_DIB);
//std::cout<<"image decoded"<<" "<<w<<" "<<h<<std::endl;
//    CloseClipboard();
//std::cout<<"clipboard closed"<<std::endl;
//
//    //windows clipboard image can't have translucent pixels. Yet the image data received is 32-bit and has some data in the alpha channel that we can't use (greyscale version of image or so???). Remove that now.
//    for(size_t i = 3; i < image.size(); i+=4) image[i] = 255;
//    return true;
//  }
//  return false;
//}

void processClipboardEvents()
{
}

#elif defined(LPI_OS_LINUX)

#define CLIPBOARDDEBUG true//gives debug messages in std::cout

#include <X11/Xlib.h>
#include <X11/Xatom.h>
#include <cstdio>
#include <climits>
#include <cstring>
#include <iostream>
#include "lpi/sdl/lpi_time.h"
#include "imfmt/imageformats.h"

namespace
{

/*
This code is created thanks to the "ad-hoc" tutorials from http://mi.eng.cam.ac.uk/~er258/code/x11.html,
but written from scratch.
Also used for documentation:
http://tronche.com/gui/x/icccm/sec-2.html
xsel.c (search for a recent enough version of it that has INCR related code in it)
*/

//the return values of XGetWindowProperty
struct WindowProperty
{
  unsigned char* prop;
  int actual_format;
  int nitems;
  Atom actual_type;

  WindowProperty()
  : prop(0)
  {
  }

  ~WindowProperty()
  {
    if(prop) XFree(prop);
  }
};

std::string GetAtomName(Display* display, Atom atom)
{
  if(atom == None) return "None";
  char* name = XGetAtomName(display, atom);
  std::string result(name);
  XFree(name);
  return result;
}

void getWindowProperty(WindowProperty& property, Display* display, const Window& window, Atom atom, Bool del)
{
  Atom actual_type;
  int actual_format;
  unsigned long nitems;
  unsigned long bytes_after;
  unsigned char* prop = 0;

  int length = 1024;

  //read all bytes
  do
  {
    if(prop) XFree(prop);
    XGetWindowProperty(display, window, atom, del, length, False, AnyPropertyType, &actual_type, &actual_format, &nitems, &bytes_after, &prop);
    length *= 2;
  } while(bytes_after != 0);

  property.prop = prop;
  property.actual_format = actual_format;
  property.nitems = nitems;
  property.actual_type = actual_type;
}

void printAtomsDebugMessage(Display* display, const WindowProperty& property)
{
  Atom* atoms = (Atom*)property.prop;
  for(int i = 0; i < property.nitems; i++) std::cout << "available target type: " << GetAtomName(display, atoms[i]) << std::endl;
}

Atom findAtomOfType(Display* display, const WindowProperty& property, const std::string& datatype)
{
  Atom* atoms = (Atom*)property.prop;

  for(int i = 0; i < property.nitems; i++)
  {
    if(GetAtomName(display, atoms[i]) == datatype) return atoms[i];
  }

  return None;
}

bool getNextEvent(Display *display, XEvent *event_return)
{
#if 1
//This one doesn't always get the event
  double t = lpi::getSeconds();
  while(!XPending(display))
  {
    if(lpi::getSeconds() - t > 5.0)
    {
if(CLIPBOARDDEBUG) std::cout << "Error: The XNextEvent never came... :(" << std::endl;
      return false; //he's probably never going to send us an event :(
    }
  }
  XNextEvent(display, event_return);
  return true;
#else
//This one blocks forever when seeing complex visuals fullscreen (so > 300K data), pressing ctrl + printscreen, and then pasting it in the program, causing INCR transfer
//ACTUALLY, good news, Gimp also hangs in this scenario. KDE 3.5 is the bug!
  XNextEvent(display, event_return);
  return true;
#endif
}

void getIncrData(std::vector<unsigned char>& data, const XSelectionEvent& selevent)
{
if(CLIPBOARDDEBUG) std::cout << "Incremental transfer starting due to INCR property" << std::endl;
  XEvent event;
  XSelectInput(selevent.display, selevent.requestor, PropertyChangeMask);
  XDeleteProperty(selevent.display, selevent.requestor, selevent.property); //this SHOULD start the INCR mechanism (but in KDE 3.5 I don't get any events after this???)

  for(;;)
  {
    if(!getNextEvent(selevent.display, &event)) break;
    if(event.type == PropertyNotify)
    {
      if (event.xproperty.state != PropertyNewValue) continue;
      WindowProperty property;
      getWindowProperty(property, selevent.display, selevent.requestor, selevent.property, False);
      size_t num_bytes = property.nitems * property.actual_format / 8;
if(CLIPBOARDDEBUG) std::cout<<"INCR data size: " << num_bytes << std::endl;
      for(size_t i = 0; i < num_bytes; i++) data.push_back(property.prop[i]);
      XDeleteProperty(selevent.display, selevent.requestor, selevent.property);
      if(num_bytes == 0) break;
    }
    else break;
  }
}

static Display* display = 0;

void clearClipboard()
{
  if(!display) display = XOpenDisplay(0);
  Atom ATOM_CLIPBOARD = XInternAtom(display, "CLIPBOARD", 0);

  XSetSelectionOwner(display, ATOM_CLIPBOARD, None, CurrentTime);
  XSync(display, False);
}

static int REMEMBER_W;
static int REMEMBER_H;
static std::vector<unsigned char> REMEMBER_IMAGE;

}

//stores the image as RGBA in image, and its width and height in w and h. So image.size() is w * h * 4. Returns true if there was an image on the clipboard, false if not (in that case the output parameters should not be used)
bool getClipboardImageRGBA8(std::vector<unsigned char>& image, int& w, int& h)
{
  if(!display) display = XOpenDisplay(0);
  int screen = DefaultScreen(display);
  Window root = RootWindow(display, screen);

  //dummy window
  Window window = XCreateSimpleWindow(display, root, 0, 0, 100, 100, 0, BlackPixel(display, screen), BlackPixel(display, screen));

  Atom ATOM_TARGETS = XInternAtom(display, "TARGETS", False); //possible formats in which source program can output the data
  Atom ATOM_CLIPBOARD = XInternAtom(display, "CLIPBOARD", 0);

  XConvertSelection(display, ATOM_CLIPBOARD, ATOM_TARGETS, ATOM_CLIPBOARD, window, CurrentTime);
  XFlush(display);

  std::vector<unsigned char> data;

  XEvent event;
  bool sent_request = false;
  Atom image_png_atom = None;
  Atom image_bmp_atom = None;
  for(;;)
  {
    if(!getNextEvent(display, &event)) break;
    if(event.type == SelectionNotify)
    {
      Atom target = event.xselection.target;

if(CLIPBOARDDEBUG) std::cout << "target atom name: " << GetAtomName(display, target) << std::endl;

      if(event.xselection.property != None)
      {
        WindowProperty property;
        getWindowProperty(property, display, window, ATOM_CLIPBOARD, False);

if(CLIPBOARDDEBUG) std::cout << "property atom name: " << GetAtomName(display, property.actual_type) << std::endl;

        if(target == ATOM_TARGETS && !sent_request)
        {
          //property.prop now contains a list of Atoms, and each atom has a datatype associated with it
          sent_request = true;
if(CLIPBOARDDEBUG) printAtomsDebugMessage(display, property);
          image_png_atom = findAtomOfType(display, property, "image/png");
          image_bmp_atom = findAtomOfType(display, property, "image/bmp");

          if(image_png_atom != None) XConvertSelection(display, ATOM_CLIPBOARD, image_png_atom, ATOM_CLIPBOARD, window, CurrentTime);
          else if(image_bmp_atom != None) XConvertSelection(display, ATOM_CLIPBOARD, image_bmp_atom, ATOM_CLIPBOARD, window, CurrentTime);
          else break;
        }
        else if(target == image_png_atom || target == image_bmp_atom)
        {
          std::string atomname = GetAtomName(display, property.actual_type);
          if(atomname == "image/png" || atomname == "image/bmp")
          {
            //property.prop now contains actual data bytes, the image
            size_t num_bytes = property.nitems * property.actual_format / 8;
if(CLIPBOARDDEBUG) std::cout<<"data size: " << num_bytes << std::endl;
            data.resize(num_bytes);
            for(size_t i = 0; i < data.size(); i++) data[i] = property.prop[i];
            break;
          }
          else if(atomname == "INCR")
          {
            //XConvertSelection(display, ATOM_CLIPBOARD, ATOM_TARGETS, ATOM_CLIPBOARD, window, CurrentTime);
            //XFlush(display);
            getIncrData(data, event.xselection);
            break;
          }
        }
        else break;
      }
      else break;
    }
  }

  //XCloseDisplay(display);
  XDestroyWindow(display, window);

  if(!data.empty())
  {
    std::string error;
    if(decodeImageFile(error, image, w, h, &data[0], data.size(), (image_png_atom != None) ? IF_PNG : IF_BMP))
    {
if(CLIPBOARDDEBUG) std::cout<<"image size: " << w << " " << h << std::endl;
      return true;
    }
if(CLIPBOARDDEBUG) std::cout<<"image from clipboard decoding error: " << error << std::endl;
    return false;
  }

  if(REMEMBER_IMAGE.size() == (size_t)(REMEMBER_W * REMEMBER_H * 4) && !REMEMBER_IMAGE.empty())
  {
if(CLIPBOARDDEBUG) std::cout<<"getting image internally (w: " << REMEMBER_W << " h: " << REMEMBER_H << ")" << std::endl;
    image = REMEMBER_IMAGE;
    w = REMEMBER_W;
    h = REMEMBER_H;
    return true;
  }


if(CLIPBOARDDEBUG) std::cout<<"no image on clipboard (or error)" << std::endl;
  return false;
}

bool setClipboardImageRGBA8(const unsigned char* image, int w, int h, int w2)
{
  /*
  This is pretty unfriendly towards other applications, but for now, for simplicity, I don't actually
  properly put the image on the X clipboard. Instead, I clear the clipboard, so LodePaint won't see
  any outside clipboard data, and remember the image internally here (in REMEMBER_IMAGE & co).
  */

  clearClipboard();

  REMEMBER_W = w;
  REMEMBER_H = h;
  REMEMBER_IMAGE.resize(w * h * 4);
  for(int y = 0; y < h; y++)
  for(int x = 0; x < w; x++)
  for(int c = 0; c < 4; c++)
  {
    size_t index = y * 4 * w + x * 4 + c;
    size_t index2 = y * 4 * w2 + x * 4 + c;
    REMEMBER_IMAGE[index] = image[index2];
  }

  return true;
}

void processClipboardEvents()
{
}

//bool setClipboardImageRGBA8(const unsigned char* image, int w, int h, int w2)
//{
  //(void)image;
  //(void)w;
  //(void)h;
  //(void)w2;

  //if(!display) display = XOpenDisplay(0);
  //int screen = DefaultScreen(display);
  //Window root = RootWindow(display, screen);

  //Atom ATOM_CLIPBOARD = XInternAtom(display, "CLIPBOARD", 0);

  //XSetSelectionOwner(display, ATOM_CLIPBOARD, root, CurrentTime);
  //Window owner = XGetSelectionOwner(display, ATOM_CLIPBOARD);

  ////XCloseDisplay(display);

  //if (owner != root) return false;

  //return true;
//}


//void processClipboardEvents()
//{

  //if(!display) display = XOpenDisplay(0);
  //if(XPending(display))
  //{
    //XEvent getevent;
    //XNextEvent(display, &getevent);
    //std::cout << getevent.type << " " << SelectionRequest << std::endl;

    //if(getevent.type == SelectionRequest)
    //{

////typedef struct {
  ////int type;	  /* SelectionNotify */
  ////unsigned long serial; /* # of last request processed by server */
  ////Bool send_event;  /* true if this came from a SendEvent request */
  ////Display *display; /* Display the event was read from */
  ////Window requestor;
  ////Atom selection;
  ////Atom target;
  ////Atom property;    /* atom or None */
  ////Time time;
////} XSelectionEvent;



      //Atom ATOM_CLIPBOARD = XInternAtom(display, "CLIPBOARD", 0);

      //XEvent event;
      //event.type = SelectionNotify;
      //static unsigned long serial = 0;
      //event.xselection.serial = serial++;
      //event.xselection.send_event = false;
      //event.xselection.display = display;
      //event.xselection.requestor = getevent.xselectionrequest.requestor;
      //event.xselection.selection = ATOM_CLIPBOARD;
      //event.xselection.target = XInternAtom(display, "LodePaint", 0);
      //event.xselection.property = XInternAtom(display, "LodePaint", 0);
      //event.xselection.time = CurrentTime;

      //XSendEvent(display, getevent.xselectionrequest.requestor, False, 0, &event);
    //}

  //}
  ////XCloseDisplay(display);
//}

#elif defined(LPI_OS_AMIGA) || defined(LPI_OS_AROS) //end of linux section

static int REMEMBER_W;
static int REMEMBER_H;
static std::vector<unsigned char> REMEMBER_IMAGE;

bool getClipboardImageRGBA8(std::vector<unsigned char>& image, int& w, int& h)
{
  if(REMEMBER_IMAGE.size() == (size_t)(REMEMBER_W * REMEMBER_H * 4) && !REMEMBER_IMAGE.empty())
  {
    image = REMEMBER_IMAGE;
    w = REMEMBER_W;
    h = REMEMBER_H;
    return true;
  }

  return false;
}

bool setClipboardImageRGBA8(const unsigned char* image, int w, int h, int w2) //RGBA. w2 is memory width of the lines in the buffer (also in pixels)
{
  REMEMBER_W = w;
  REMEMBER_H = h;
  REMEMBER_IMAGE.resize(w * h * 4);
  for(int y = 0; y < h; y++)
  for(int x = 0; x < w; x++)
  for(int c = 0; c < 4; c++)
  {
    size_t index = y * 4 * w + x * 4 + c;
    size_t index2 = y * 4 * w2 + x * 4 + c;
    REMEMBER_IMAGE[index] = image[index2];
  }

  return true;
}

void processClipboardEvents() //call once per frame to process events related to clipboard if required by the Operating System.
{
}

#endif // end of amiga section


/*int main()
{
  std::vector<unsigned char> image;
  int w, h;
  getClipboardImageRGBA8(image, w, h);
}*/




