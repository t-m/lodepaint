/*
LodePaint

Copyright (c) 2009 Lode Vandevenne
All rights reserved.

This file is part of LodePaint.

LodePaint is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LodePaint is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LodePaint.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "paint_filter_alpha.h"

#include "lpi/lpi_math.h"
#include "lpi/lpi_math2d.h"
#include "lpi/lpi_math3d.h" //for 3D RGB-space calculations
#include "lpi/gui/lpi_gui_color.h"

#include "imalg/alpha.h"

#include <iostream>
#include <limits>

////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////

void OperationOpaque::doit(TextureRGBA8& texture, Progress&)
{
  alphaOpaqueRGBA8(texture.getBuffer(), texture.getU(), texture.getV(), texture.getU2());
}

void OperationOpaque::doit(TextureRGBA32F& texture, Progress&)
{
  alphaOpaqueRGBA32F(texture.getBuffer(), texture.getU(), texture.getV(), texture.getU2());
}



////////////////////////////////////////////////////////////////////////////////

OperationSetAlphaTo::OperationSetAlphaTo(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom)
: AOperationFilterUtil(settings, true, true)
, value(128.0)
, dialog(this)
{
  params.addDouble("Value", &value, 0.0, 255.0, 1.0);

  paramsToDialog(dialog.page, params, geom);
}

void OperationSetAlphaTo::doit(TextureRGBA8& texture, Progress&)
{
  unsigned char* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();

  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  {
    size_t index = y * u2 * 4 + x * 4;
    buffer[index + 3] = (int)value;
  }
}

void OperationSetAlphaTo::doit(TextureRGBA32F& texture, Progress&)
{
  float* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();

  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  {
    size_t index = y * u2 * 4 + x * 4;
    buffer[index + 3] = value / 255.0;
  }
}

////////////////////////////////////////////////////////////////////////////////

void OperationTranslucent::doit(TextureRGBA8& texture, Progress&)
{
  alphaTranslucentRGBA8(texture.getBuffer(), texture.getU(), texture.getV(), texture.getU2());
}

void OperationTranslucent::doit(TextureRGBA32F& texture, Progress&)
{
  alphaTranslucentRGBA32F(texture.getBuffer(), texture.getU(), texture.getV(), texture.getU2());
}

////////////////////////////////////////////////////////////////////////////////

void OperationAlphaModulated::doit(TextureRGBA8& texture, Progress&)
{
  alphaModulatedRGBA8(texture.getBuffer(), texture.getU(), texture.getV(), texture.getU2());
}

void OperationAlphaModulated::doit(TextureRGBA32F& texture, Progress&)
{
  alphaModulatedRGBA32F(texture.getBuffer(), texture.getU(), texture.getV(), texture.getU2());
}

////////////////////////////////////////////////////////////////////////////////

void OperationAlphaSaturation::doit(TextureRGBA8& texture, Progress&)
{
  alphaSaturationRGBA8(texture.getBuffer(), texture.getU(), texture.getV(), texture.getU2());
}

void OperationAlphaSaturation::doit(TextureRGBA32F& texture, Progress&)
{
  alphaSaturationRGBA32F(texture.getBuffer(), texture.getU(), texture.getV(), texture.getU2());
}

////////////////////////////////////////////////////////////////////////////////

void OperationAlphaColorKey::doit(TextureRGBA8& texture, const GlobalToolSettings& settings, Progress&)
{
  alphaColorKeyRGBA8(texture.getBuffer(), texture.getU(), texture.getV(), texture.getU2(), settings.rightColor255());
}

void OperationAlphaColorKey::doit(TextureRGBA32F& texture, const GlobalToolSettings& settings, Progress&)
{
  alphaColorKeyRGBA32F(texture.getBuffer(), texture.getU(), texture.getV(), texture.getU2(), settings.rightColor);
}

////////////////////////////////////////////////////////////////////////////////

void OperationApplyAlpha::doit(TextureRGBA8& texture, const GlobalToolSettings& settings, Progress&)
{
  lpi::ColorRGB color = bg ? settings.rightColor255() : settings.leftColor255();
  unsigned char* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();
  
  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  {
    size_t index = y * u2 * 4 + x * 4;
    
    int r2 = buffer[index + 0];
    int r = color.r;
    int g2 = buffer[index + 1];
    int g = color.g;
    int b2 = buffer[index + 2];
    int b = color.b;
    int a2 = buffer[index + 3];
    int a = color.a;

    buffer[index + 0] = (r2 * a2 + (r * (255 - a2) * a) / 255) / 255;
    buffer[index + 1] = (g2 * a2 + (g * (255 - a2) * a) / 255) / 255;
    buffer[index + 2] = (b2 * a2 + (b * (255 - a2) * a) / 255) / 255;
    buffer[index + 3] = a2 + ((255 - a2) * a) / 255;
  }
}

////////////////////////////////////////////////////////////////////////////////

void OperationAlphaSoftKey::doit(TextureRGBA8& texture, const GlobalToolSettings& settings, Progress&)
{
  alphaSoftKeyRGBA8(texture.getBuffer(), texture.getU(), texture.getV(), texture.getU2(), settings.rightColor255());
}

void OperationAlphaSoftKey::doit(TextureRGBA32F& texture, const GlobalToolSettings& settings, Progress&)
{
  alphaSoftKeyRGBA32F(texture.getBuffer(), texture.getU(), texture.getV(), texture.getU2(), settings.rightColor);
}

////////////////////////////////////////////////////////////////////////////////

void OperationOnlyAlpha::doit(TextureRGBA8& texture, const GlobalToolSettings& settings, Progress&)
{
  keepOnlyAlphaRGBA8(texture.getBuffer(), texture.getU(), texture.getV(), texture.getU2(), settings.rightColor255());
}

void OperationOnlyAlpha::doit(TextureRGBA32F& texture, const GlobalToolSettings& settings, Progress&)
{
  keepOnlyAlphaRGBA32F(texture.getBuffer(), texture.getU(), texture.getV(), texture.getU2(), settings.rightColor);
}

////////////////////////////////////////////////////////////////////////////////

void OperationAlphaToGray::doit(TextureRGBA8& texture, Progress&)
{
  alphaToGreyRGBA8(texture.getBuffer(), texture.getU(), texture.getV(), texture.getU2());
}

void OperationAlphaToGray::doit(TextureRGBA32F& texture, Progress&)
{
  alphaToGreyRGBA32F(texture.getBuffer(), texture.getU(), texture.getV(), texture.getU2());
}

////////////////////////////////////////////////////////////////////////////////

void OperationAlphaExtractCloud::doit(TextureRGBA8& texture, Progress&)
{
  alphaExtractCloudRGBA8(texture.getBuffer(), texture.getU(), texture.getV(), texture.getU2(), settings.leftColor255(), settings.rightColor255());
}

void OperationAlphaExtractCloud::doit(TextureRGBA32F& texture, Progress&)
{
  alphaExtractCloudRGBA32F(texture.getBuffer(), texture.getU(), texture.getV(), texture.getU2(), settings.leftColor, settings.rightColor);
}

////////////////////////////////////////////////////////////////////////////////

namespace
{
  unsigned char clamp0255(int value)
  {
    return value > 255 ? 255 : (value < 0 ? 0 : value);
  }
  void addAndClampAlpha(unsigned char* color, int add)
  {
    color[3] = clamp0255(color[3] + add);
  }
}

void OperationDitherMonochromeAlpha::doit(TextureRGBA8& texture, Progress&)
{
  unsigned char* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();

  //Floyd-Steinberg
  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  {
    size_t index = y * u2 * 4 + x * 4;
    size_t index10 = y * u2 * 4 + (x+1) * 4;
    size_t index11 = (y+1) * u2 * 4 + (x+1) * 4;
    size_t index01 = (y+1) * u2 * 4 + x * 4;
    size_t index91 = (y+1) * u2 * 4 + (x-1) * 4;
    
    int value = buffer[index + 3];
    
    if(value <= 128) buffer[index + 3] = 0;
    else buffer[index + 3] = 255;
    
    int error = value - buffer[index + 3];
    
    if(x + 1 < u) addAndClampAlpha(&buffer[index10], (7 * error) / 16);
    if(y + 1 < v)
    {
      if(x >= 1) addAndClampAlpha(&buffer[index91], (3 * error) / 16);
      addAndClampAlpha(&buffer[index01], (5 * error) / 16);
      if(x + 1 < u) addAndClampAlpha(&buffer[index11], (1 * error) / 16);
    }
  }
}

////////////////////////////////////////////////////////////////////////////////

OperationThresholdAlpha::OperationThresholdAlpha(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom)
: AOperationFilterUtil(settings, true, true)
, threshold(128.0)
, dialog(this)
{
  params.addDouble("Threshold", &threshold, 0.0, 255.0, 1.0);

  paramsToDialog(dialog.page, params, geom);
}

void OperationThresholdAlpha::doit(TextureRGBA8& texture, Progress&)
{
  unsigned char* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();

  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  {
    size_t index = y * u2 * 4 + x * 4;
    int value = buffer[index + 3];
    if(value < (int)threshold) buffer[index + 3] = 0;
    else buffer[index + 3] = 255;
  }
}

void OperationThresholdAlpha::doit(TextureRGBA32F& texture, Progress&)
{
  float* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();

  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  {
    size_t index = y * u2 * 4 + x * 4;
    float value = buffer[index + 3];
    if(value < (float)threshold) buffer[index + 3] = 0.0f;
    else buffer[index + 3] = 1.0f;
  }
}

////////////////////////////////////////////////////////////////////////////////

//Marks transparent pixels black, opaque pixels white, and anything translucent in between as red
//this allows visually easily finding places that are "almost opaque" in an image that's supposed to be fully opaque, etc...
void OperationIdentifyAlpha::doit(TextureRGBA8& texture, Progress&)
{
  unsigned char* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();

  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  {
    size_t index = y * u2 * 4 + x * 4;
    int value = buffer[index + 3];
    buffer[index + 3] = 255;
    if(value == 0) buffer[index + 0] = buffer[index + 1] = buffer[index + 2] = 0;
    else if(value == 255) buffer[index + 0] = buffer[index + 1] = buffer[index + 2] = 255;
    else
    {
      buffer[index + 0] = 255;
      buffer[index + 1] = 0;
      buffer[index + 2] = 0;
    }
  }
}

////////////////////////////////////////////////////////////////////////////////

void OperationPremultiplyAlpha::doit(TextureRGBA8& texture, Progress&)
{
  unsigned char* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();

  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  {
    size_t index = y * u2 * 4 + x * 4;
    int a = buffer[index + 3];
    buffer[index + 0] = buffer[index + 0] * a / 255;
    buffer[index + 1] = buffer[index + 1] * a / 255;
    buffer[index + 2] = buffer[index + 2] * a / 255;
    buffer[index + 3] = 255;
  }
}


////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
