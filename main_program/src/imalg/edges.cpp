/*
LodePaint

Copyright (c) 2009-2012 Lode Vandevenne
All rights reserved.

This file is part of LodePaint.

LodePaint is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LodePaint is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LodePaint.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "edges.h"

#include "blur.h"
#include "dsp.h"
#include <cmath>

static void copyBuffer(std::vector<float>& result, unsigned char* buffer, size_t u, size_t v, size_t u2)
{
  (void)u;
  result.resize(u * v * 4);
  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  for(size_t c = 0; c < 4; c++)
  {
    result[y * u * 4 + x * 4 + c] = buffer[y * u2 * 4 + x * 4 + c];
  }
}

void cannyEdgeDetectRGBA8(unsigned char* buffer, size_t u, size_t v, size_t u2, bool affect_alpha, Progress& progress,
                          double radius, int lt, int ht, int steps)
{
  if(radius > 0) gaussianBlurRGBA8(buffer, u, v, u2, radius, false /*wrapping*/, affect_alpha, progress);

  if(steps <= 0) return;

  static const double sobelxmatrix[9] = {-1, 0, 1, -2, 0, 2, -1, 0, 1};
  std::vector<float> vectora; /*first sobelx, later angle*/
  copyBuffer(vectora, buffer, u, v, u2);
  convoluteRGBA32F<3, 3>(&vectora[0], u, v, u, sobelxmatrix, false, affect_alpha, 0, 1);

  static const double sobelymatrix[9] = {1, 2, 1, 0, 0, 0, -1, -2, -1};
  std::vector<float> vectorb; /*first sobely, later magnitude*/
  copyBuffer(vectorb, buffer, u, v, u2);
  convoluteRGBA32F<3, 3>(&vectorb[0], u, v, u, sobelymatrix, false, affect_alpha, 0, 1);

  for(size_t i = 0; i < u * v * 4; i++)
  {
    double a = vectora[i];
    double b = vectorb[i];
    vectora[i] = std::atan2(b, a); //gradient angle
    vectorb[i] = std::sqrt(a * a + b * b); //gradient magnitude
  }

  //Gradient angle is super awesome looking!!
  if(steps <= 2)
  {
    for(size_t y = 0; y < v; y++)
    for(size_t x = 0; x < u; x++)
    for(size_t c = 0; c < (affect_alpha ? 4 : 3); c++)
    {
      int result = steps <= 1 ? vectorb[y * u * 4 + x * 4 + c] / 4 : 128 + vectora[y * u * 4 + x * 4 + c] * 20;
      if(result < 0) result = 0;
      if(result > 255) result = 255;
      buffer[y * u2 * 4 + x * 4 + c] = result;
    }
    return;
  }

  //quantize angle
  for(size_t i = 0; i < u * v * 4; i++)
  {
    double deg = vectora[i] / 6.28318531 * 360 + 180;
    if((deg > 0 && deg <= 22.5) || (deg > 157.5 && deg <= 202.5) || (deg > 337.5 && deg < 360)) vectora[i] = 0;
    else if((deg > 22.5 && deg <= 67.5) || (deg > 202.5 && deg <= 247.5)) vectora[i] = 1;
    else if((deg > 67.5 && deg <= 112.5) || (deg > 247.5 && deg <= 292.5)) vectora[i] = 2;
    else vectora[i] = 3;
  }
  
  if(steps <= 3)
  {
    for(size_t y = 0; y < v; y++)
    for(size_t x = 0; x < u; x++)
    for(size_t c = 0; c < (affect_alpha ? 4 : 3); c++)
    {
      int result = (int)(128 + (vectora[y * u * 4 + x * 4 + c] - 1.5) * 30);
      buffer[y * u2 * 4 + x * 4 + c] = result;
    }
    return;
  }

  //Non Maxima Suppression
  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  for(size_t c = 0; c < (affect_alpha ? 4 : 3); c++)
  {
    if(x == 0 || y == 0 || x == u - 1 || y == v - 1)
    {
      buffer[y * u2 * 4 + x * 4 + c] = 0;
      continue;
    }

    size_t i = y * u * 4 + x * 4 + c;
    size_t x0, y0, x1, y1;
    if(vectora[i] == 0)
    {
      x0 = x - 1;
      y0 = y;
      x1 = x + 1;
      y1 = y;
    }
    else if(vectora[i] == 1)
    {
      x0 = x - 1;
      y0 = y + 1;
      x1 = x + 1;
      y1 = y - 1;
    }
    else if(vectora[i] == 2)
    {
      x0 = x;
      y0 = y - 1;
      x1 = x;
      y1 = y + 1;
    }
    else
    {
      x0 = x - 1;
      y0 = y - 1;
      x1 = x + 1;
      y1 = y + 1;
    }

    size_t i0 = y0 * u * 4 + x0 * 4 + c;
    size_t i1 = y1 * u * 4 + x1 * 4 + c;
    size_t i2 = y * u2 * 4 + x * 4 + c;

    if(vectorb[i] >= vectorb[i0] && vectorb[i] >= vectorb[i1]) buffer[i2] = vectorb[i];
    else buffer[i2] = 0;
  }

  //At this point there's also a pretty good looking result in buffer. Stopping here could also be useful.
  if(steps <= 4) return;

  //Hysteresis Thresholding
  //Start at points higher than high threshold, trace edges using the 8 neighbouring points
  //higher than the low threshold with a stack, and mark used points as visited.
  std::vector<bool> visited(u2 * v * 4, false);

  std::vector<size_t> stackx;
  std::vector<size_t> stacky;
  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  for(size_t c = 0; c < 4; c++)
  {
    if(!affect_alpha && c == 3) continue;
    size_t i = y * u2 * 4 + x * 4 + c;
    if(visited[i]) continue;
    visited[i] = true;
    if(buffer[i] >= ht)
    {
      stackx.push_back(x);
      stacky.push_back(y);
      while(!stackx.empty())
      {
        size_t x2 = stackx.back();
        stackx.pop_back();
        size_t y2 = stacky.back();
        stacky.pop_back();
        size_t i2 = y2 * u2 * 4 + x2 * 4 + c;
        buffer[i2] = 255;
        for(int a = 0; a < 8; a++)
        {
          size_t x3 = (a == 0 || a == 3 || a == 5) ? x2 - 1 : (a == 1 || a == 6 ? x2 : x2 + 1);
          size_t y3 = (a == 0 || a == 1 || a == 2) ? y2 - 1 : (a == 3 || a == 4 ? y2 : y2 + 1);
          if(x3 >= u || y3 >= v) continue;
          size_t i3 = y3 * u2 * 4 + x3 * 4 + c;
          if(visited[i3]) continue;
          visited[i3] = true;
          if(buffer[i3] >= lt)
          {
            stackx.push_back(x3);
            stacky.push_back(y3);
            buffer[i3] = 255;
          }
          else buffer[i3] = 0;
        }
      }
    }
    else buffer[i] = 0;
  }
}




