/*
LodePaint

Copyright (c) 2009-2010 Lode Vandevenne
All rights reserved.

This file is part of LodePaint.

LodePaint is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LodePaint is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LodePaint.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "tolerance.h"

#include <vector>

using std::size_t;


void floodFillRGBA8(int& ox0, int& oy0, int& ox1, int& oy1
                  , RGBA8* buffer
                  , size_t u, size_t v, size_t u2
                  , int x, int y
                  , int x0, int y0, int x1, int y1
                  , const RGBA8& newColor, const RGBA8& oldColor
                  , int tolerance, bool tolerance_ignores_alpha);
void floodFillRGBA8RGBA8(int& ox0, int& oy0, int& ox1, int& oy1
                       , RGBA8* obuffer, const RGBA8* ibuffer
                       , size_t u, size_t v, size_t ou2, size_t iu2
                       , int x, int y
                       , int x0, int y0, int x1, int y1
                       , const RGBA8& newColor, const RGBA8& oldColor
                       , int tolerance, bool tolerance_ignores_alpha);
void floodFillRGBA8Grey8(int& ox0, int& oy0, int& ox1, int& oy1
                       , unsigned char* obuffer, const RGBA8* ibuffer
                       , size_t u, size_t v, size_t ou2, size_t iu2
                       , int x, int y
                       , int x0, int y0, int x1, int y1
                       , unsigned char newColor, const RGBA8& oldColor
                       , int tolerance, bool tolerance_ignores_alpha);


void floodFillRGBA32F(int& ox0, int& oy0, int& ox1, int& oy1
                    , RGBA32F* buffer
                    , size_t u, size_t v, size_t u2
                    , int x, int y
                    , int x0, int y0, int x1, int y1
                    , const RGBA32F& newColor, const RGBA32F& oldColor
                    , float tolerance, bool tolerance_ignores_alpha);




