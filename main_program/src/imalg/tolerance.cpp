/*
LodePaint

Copyright (c) 2009-2010 Lode Vandevenne
All rights reserved.

This file is part of LodePaint.

LodePaint is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LodePaint is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LodePaint.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "tolerance.h"
#include "../lpi/lpi_math.h"



int toldist(const lpi::ColorRGB& a, const lpi::ColorRGB& b, bool tolerance_ignores_alpha)
{
  if(tolerance_ignores_alpha)
  {
    return lpi::template_max(lpi::template_max(lpi::template_abs(a.r - b.r), lpi::template_abs(a.g - b.g))
                           , lpi::template_abs(a.b - b.b));
  }
  else
  {
    return lpi::template_max(lpi::template_max(lpi::template_abs(a.r - b.r), lpi::template_abs(a.g - b.g))
                           , lpi::template_max(lpi::template_abs(a.b - b.b), lpi::template_abs(a.a - b.a)));
  }
}

int toldist(const RGBA8& a, const RGBA8& b, bool tolerance_ignores_alpha)
{
  unsigned char* aa = (unsigned char*)(&a);
  unsigned char* bb = (unsigned char*)(&b);
  if(tolerance_ignores_alpha)
  {
    return lpi::template_max(lpi::template_max(lpi::template_abs(aa[0] - bb[0]), lpi::template_abs(aa[1] - bb[1]))
                           , lpi::template_abs(aa[2] - bb[2]));
  }
  else
  {
    return lpi::template_max(lpi::template_max(lpi::template_abs(aa[0] - bb[0]), lpi::template_abs(aa[1] - bb[1]))
                           , lpi::template_max(lpi::template_abs(aa[2] - bb[2]), lpi::template_abs(aa[3] - bb[3])));
  }
}

float toldist(const RGBA32F& a, const RGBA32F& b, bool tolerance_ignores_alpha)
{
  float* aa = (float*)(&a);
  float* bb = (float*)(&b);
  if(tolerance_ignores_alpha)
  {
    return lpi::template_max(lpi::template_max(lpi::template_abs(aa[0] - bb[0]), lpi::template_abs(aa[1] - bb[1]))
                           , lpi::template_abs(aa[2] - bb[2]));
  }
  else
  {
    return lpi::template_max(lpi::template_max(lpi::template_abs(aa[0] - bb[0]), lpi::template_abs(aa[1] - bb[1]))
                           , lpi::template_max(lpi::template_abs(aa[2] - bb[2]), lpi::template_abs(aa[3] - bb[3])));
  }
}
