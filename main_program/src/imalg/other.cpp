/*
LodePaint

Copyright (c) 2009-2010 Lode Vandevenne
All rights reserved.

This file is part of LodePaint.

LodePaint is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LodePaint is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LodePaint.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "other.h"

#include "../lpi/lpi_color.h"

void mosaicRGBA8(unsigned char* buffer, size_t u, size_t v, size_t u2, int tilesizex, int tilesizey, bool antialiasing)
{
  if(antialiasing)
  {
    size_t numx = (u + tilesizex - 1) / tilesizex;
    size_t numy = (v + tilesizey - 1) / tilesizey;
    for(size_t tx = 0; tx < numx; tx++)
    for(size_t ty = 0; ty < numy; ty++)
    {
      lpi::ColorRGB average(0, 0, 0, 0);
      size_t num = 0;
      for(size_t sx = 0; sx < (size_t)tilesizex && tx * (size_t)tilesizex + sx < u; sx++)
      for(size_t sy = 0; sy < (size_t)tilesizey && ty * (size_t)tilesizey + sy < v; sy++)
      {
        size_t x = tx * tilesizex + sx;
        size_t y = ty * tilesizey + sy;
        size_t index = y * u2 * 4 + x * 4;
        average.r += buffer[index + 0];
        average.g += buffer[index + 1];
        average.b += buffer[index + 2];
        average.a += buffer[index + 3];
        num++;
      }
      average.r /= num;
      average.g /= num;
      average.b /= num;
      average.a /= num;
      for(size_t sx = 0; sx < (size_t)tilesizex && tx * (size_t)tilesizex + sx < u; sx++)
      for(size_t sy = 0; sy < (size_t)tilesizey && ty * (size_t)tilesizey + sy < v; sy++)
      {
        size_t x = tx * tilesizex + sx;
        size_t y = ty * tilesizey + sy;
        size_t index = y * u2 * 4 + x * 4;
        buffer[index + 0] = average.r;
        buffer[index + 1] = average.g;
        buffer[index + 2] = average.b;
        buffer[index + 3] = average.a;
      }
    }
  }
  else
  {
    size_t x2, y2;
    for(size_t y = 0; y < v; y++)
    {
      y2 = (y / tilesizey) * tilesizey;
      for(size_t x = 0; x < u; x++)
      {
        x2 = (x / tilesizex) * tilesizex;
        for(size_t c = 0; c < 4; c++)
        {
          size_t index = y * u2 * 4 + x * 4 + c;
          size_t index2 = y2 * u2 * 4 + x2 * 4 + c;
          buffer[index] = buffer[index2];
        }
      }
    }
  }
}


