/*
LodePaint

Copyright (c) 2009-2010 Lode Vandevenne
All rights reserved.

This file is part of LodePaint.

LodePaint is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LodePaint is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LodePaint.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <vector>

using std::size_t;

//link_rgb and link_rgba may not be both true at the same time
void normalizeRGBA8(unsigned char* buffer, size_t u, size_t v, size_t u2, bool link_rgb, bool link_rgba, bool affect_alpha);
void normalizeRGBA32F(float* buffer, size_t u, size_t v, size_t u2, bool link_rgb, bool link_rgba, bool affect_alpha);
void normalizeGrey8(unsigned char* buffer, size_t u, size_t v, size_t u2);
void normalizeGrey32F(float* buffer, size_t u, size_t v, size_t u2);

//brightness:-1.0->1.0, contrast: -1.0->1.0, lgamma: log of gamma: -1.0->1.0
void adjustBrightnessContrastGammaRGBA8(unsigned char* buffer, size_t u, size_t v, size_t u2, double brightness, double contrast, double lgamma, bool affect_alpha);
void adjustBrightnessContrastGammaRGBA32F(float* buffer, size_t u, size_t v, size_t u2, float brightness, float contrast, float lgamma, bool affect_alpha);


