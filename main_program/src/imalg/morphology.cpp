/*
LodePaint

Copyright (c) 2009-2010 Lode Vandevenne
All rights reserved.

This file is part of LodePaint.

LodePaint is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LodePaint is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LodePaint.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "blur.h"

#include "util.h"

#include <cmath>
#include <vector>

/*
Dilate
Dilate is commonly known as "fill", "expand", or "grow." It can be used to fill
"holes" of a size equal to or smaller than the structuring element.

Erode
Erode does to the background what dilation does to the foreground. Given an
image and a structuring element, erode can be used to remove islands smaller
than the structuring element.

Morph open
Morph open is simply an erosion operation followed by a dilation operation.
Applying morph open more than once produces no further effect.

Morph close
Morph close is simply a dilation operation followed by an erosion operation.
Applying morph close more than once produces no further effect.

Morph gradient
Morph gradient is the subtraction of an eroded version of the original image
from a dilated version of the original image.

Morph tophat
Morph tophat is implemented by first applying the opening operator to the
original image, then subtracting the result from the original image. Applying
tophat shows the bright peaks within the image. 
*/

void erodeRGBA8(unsigned char* buffer, size_t u, size_t v, size_t u2, bool wrapping, bool affect_alpha, Progress& progress)
{
  static const int diameterx = 3;
  static const int diametery = 3;
  
  std::vector<unsigned char> old;
  getAlignedBufferRGBA8(old, buffer, u, v, u2);
  
  int dd = diameterx * diametery;
  
  std::vector<int> values(dd);
  std::vector<int> xs(dd);
  std::vector<int> ys(dd);
  
  //apply the filter
  for(size_t y = 0; y < v; y++)
  {
    if(progress.handle(y, v)) return;
    for(size_t x = 0; x < u; x++)
    {
      size_t index = y * 4 * u2 + x * 4;
      
      int n = 0;
      //set the color values in the arrays
      for(int filterY = 0; filterY < diametery; filterY++)
      for(int filterX = 0; filterX < diameterx; filterX++)
      {
        int x2 = x + filterX - diameterx / 2;
        int y2 = y + filterY - diametery / 2;
        if(wrapping)
        {
          while(x2 < 0) x2 += u;
          while(x2 >= (int)u) x2 -= u;
          while(y2 < 0) y2 += v;
          while(y2 >= (int)v) y2 -= v;
        }
        else
        {
          if(x2 < 0) x2 = 0;
          if(x2 >= (int)u) x2 = u - 1;
          if(y2 < 0) y2 = 0;
          if(y2 >= (int)v) y2 = v - 1;
        }
        size_t index2 = y2 * 4 * u + x2 * 4;

        values[n] = old[index2 + 0] + old[index2 + 1] + old[index2 + 2];
        xs[n] = x2;
        ys[n] = y2;
        n++;
      }

      combsort(&values[0], &xs[0], &ys[0], values.size());
      
      size_t index2 = ys[0] * 4 * u + xs[0] * 4;

      buffer[index + 0] = old[index2 + 0];
      buffer[index + 1] = old[index2 + 1];
      buffer[index + 2] = old[index2 + 2];
      if(affect_alpha) buffer[index + 3] = old[index2 + 3];
    }
  }
}

void dilateRGBA8(unsigned char* buffer, size_t u, size_t v, size_t u2, bool wrapping, bool affect_alpha, Progress& progress)
{
  static const int diameterx = 3;
  static const int diametery = 3;
  
  std::vector<unsigned char> old;
  getAlignedBufferRGBA8(old, buffer, u, v, u2);
  
  int dd = diameterx * diametery;
  
  std::vector<int> values(dd);
  std::vector<int> xs(dd);
  std::vector<int> ys(dd);
  
  //apply the filter
  for(size_t y = 0; y < v; y++)
  {
    if(progress.handle(y, v)) return;
    for(size_t x = 0; x < u; x++)
    {
      size_t index = y * 4 * u2 + x * 4;
      
      int n = 0;
      //set the color values in the arrays
      for(int filterY = 0; filterY < diametery; filterY++)
      for(int filterX = 0; filterX < diameterx; filterX++)
      {
        int x2 = x + filterX - diameterx / 2;
        int y2 = y + filterY - diametery / 2;
        if(wrapping)
        {
          while(x2 < 0) x2 += u;
          while(x2 >= (int)u) x2 -= u;
          while(y2 < 0) y2 += v;
          while(y2 >= (int)v) y2 -= v;
        }
        else
        {
          if(x2 < 0) x2 = 0;
          if(x2 >= (int)u) x2 = u - 1;
          if(y2 < 0) y2 = 0;
          if(y2 >= (int)v) y2 = v - 1;
        }
        size_t index2 = y2 * 4 * u + x2 * 4;

        values[n] = old[index2 + 0] + old[index2 + 1] + old[index2 + 2];
        xs[n] = x2;
        ys[n] = y2;
        n++;
      }

      combsort(&values[0], &xs[0], &ys[0], values.size());
      
      size_t index2 = ys[dd - 1] * 4 * u + xs[dd - 1] * 4;

      buffer[index + 0] = old[index2 + 0];
      buffer[index + 1] = old[index2 + 1];
      buffer[index + 2] = old[index2 + 2];
      if(affect_alpha) buffer[index + 3] = old[index2 + 3];
    }
  }
}


