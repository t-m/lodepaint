/*
LodePaint

Copyright (c) 2009-2010 Lode Vandevenne
All rights reserved.

This file is part of LodePaint.

LodePaint is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LodePaint is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LodePaint.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <cstring> //for size_t

/*
Does something similar to hq2x algorithm, but on RGBA data (it also uses the alpha channel in
its calculations), and the focus isn't as much on speed because it's not for a realtime game
emulator but for image processing.
For simplicity reasons, it doesn't use YUV like the real hq2x but simple RGBA comparison.
The alpha component was needed so the YUV of the real hq2x couldn't even be used.
It also doesn't use lookup tables like the official hq2x code, but does for the algorithm choice data.
This implementation is created from scratch.

Parameters:
out: output RGBA buffer with size ou * ov * 4
ou: width of output image in pixels, must be >= iu * 2
ov: height of output image in pixels, must be >= iv * 2
in: input RGBA buffer with size iu * iv * 4
iu: width of input image in pixels
iv: height of input image in pixels
*/
void hq2x(unsigned char* out, size_t ou, size_t ov, const unsigned char* in, size_t iu, size_t iv, bool wrapping);
