/*
LodePaint

Copyright (c) 2009-2010 Lode Vandevenne
All rights reserved.

This file is part of LodePaint.

LodePaint is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LodePaint is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LodePaint.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "alpha.h"

#include "../lpi/lpi_math.h"
#include "../lpi/lpi_math3d.h"

void alphaOpaqueRGBA8(unsigned char* buffer, size_t u, size_t v, size_t u2)
{
  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  {
    size_t index = y * u2 * 4 + x * 4;
    
    buffer[index + 3] = 255;
  }
}

void alphaOpaqueRGBA32F(float* buffer, size_t u, size_t v, size_t u2)
{
  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  {
    size_t index = y * u2 * 4 + x * 4;
    
    buffer[index + 3] = 1.0f;
  }
}

////////////////////////////////////////////////////////////////////////////////

void alphaTranslucentRGBA8(unsigned char* buffer, size_t u, size_t v, size_t u2)
{
  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  {
    size_t index = y * u2 * 4 + x * 4;
    
    buffer[index + 3] = (buffer[index + 0] + buffer[index + 1] + buffer[index + 2] ) / 3;
  }
}

void alphaTranslucentRGBA32F(float* buffer, size_t u, size_t v, size_t u2)
{
  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  {
    size_t index = y * u2 * 4 + x * 4;
    
    buffer[index + 3] = (buffer[index + 0] + buffer[index + 1] + buffer[index + 2] ) / 3.0f;
  }
}

////////////////////////////////////////////////////////////////////////////////

void alphaModulatedRGBA8(unsigned char* buffer, size_t u, size_t v, size_t u2)
{
  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  {
    size_t index = y * u2 * 4 + x * 4;
    int alpha = (2 * lpi::template_abs(buffer[index + 0] - 128)
          + 2 * lpi::template_abs(buffer[index + 1] - 128)
          + 2 * lpi::template_abs(buffer[index + 2] - 128)) / 3;
    if(alpha < 0) alpha = 0; if(alpha > 255) alpha = 255;
    buffer[index + 3] = alpha;
  }
}

void alphaModulatedRGBA32F(float* buffer, size_t u, size_t v, size_t u2)
{
  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  {
    size_t index = y * u2 * 4 + x * 4;
    float alpha = (2 * lpi::template_abs(buffer[index + 0] - 0.5)
                 + 2 * lpi::template_abs(buffer[index + 1] - 0.5)
                 + 2 * lpi::template_abs(buffer[index + 2] - 0.5)) / 3;
    if(alpha < 0) alpha = 0; if(alpha > 1.0) alpha = 1.0;
    buffer[index + 3] = alpha;
  }
}

////////////////////////////////////////////////////////////////////////////////

void alphaSaturationRGBA8(unsigned char* buffer, size_t u, size_t v, size_t u2)
{
  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  {
    size_t index = y * u2 * 4 + x * 4;
    int r = buffer[index + 0];
    int g = buffer[index + 1];
    int b = buffer[index + 2];
    int minc = r; if(g < minc) minc = g; if(b < minc) minc = b;
    int maxc = r; if(g > maxc) maxc = g; if(b > maxc) maxc = b;
    if(maxc == 0) buffer[index + 3] = 0;
    else buffer[index + 3] = (255 * (maxc - minc)) / maxc;
  }
}

void alphaSaturationRGBA32F(float* buffer, size_t u, size_t v, size_t u2)
{
  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  {
    size_t index = y * u2 * 4 + x * 4;
    float r = buffer[index + 0];
    float g = buffer[index + 1];
    float b = buffer[index + 2];
    float minc = r; if(g < minc) minc = g; if(b < minc) minc = b;
    float maxc = r; if(g > maxc) maxc = g; if(b > maxc) maxc = b;
    if(maxc == 0) buffer[index + 3] = 0;
    else buffer[index + 3] = ((maxc - minc)) / maxc;
  }
}

////////////////////////////////////////////////////////////////////////////////

void alphaColorKeyRGBA8(unsigned char* buffer, size_t u, size_t v, size_t u2, const lpi::ColorRGB& key)
{
  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  {
    size_t index = y * u2 * 4 + x * 4;
    
    if(buffer[index + 0] == key.r && buffer[index + 1] == key.g && buffer[index + 2] == key.b) buffer[index + 3] = 0;
  }
}

void alphaColorKeyRGBA32F(float* buffer, size_t u, size_t v, size_t u2, const lpi::ColorRGBd& key)
{
  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  {
    size_t index = y * u2 * 4 + x * 4;
    
    if(buffer[index + 0] == key.r && buffer[index + 1] == key.g && buffer[index + 2] == key.b) buffer[index + 3] = 0;
  }
}

void alphaSoftKeyRGBA8(unsigned char* buffer, size_t u, size_t v, size_t u2, const lpi::ColorRGB& key)
{
  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  {
    size_t index = y * u2 * 4 + x * 4;
    
    int r2 = buffer[index + 0];
    int r = key.r;
    int g2 = buffer[index + 1];
    int g = key.g;
    int b2 = buffer[index + 2];
    int b = key.b;

    int distance = std::abs(r2 - r) + std::abs(g2 - g) + std::abs(b2 - b);
    buffer[index + 3] = distance > 255 ? 255 : distance;
  }
}

void alphaSoftKeyRGBA32F(float* buffer, size_t u, size_t v, size_t u2, const lpi::ColorRGBd& key)
{
  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  {
    size_t index = y * u2 * 4 + x * 4;
    
    double r2 = buffer[index + 0];
    double r = key.r;
    double g2 = buffer[index + 1];
    double g = key.g;
    double b2 = buffer[index + 2];
    double b = key.b;

    double distance = std::abs(r2 - r) + std::abs(g2 - g) + std::abs(b2 - b);
    buffer[index + 3] = distance > 1.0 ? 1.0 : distance;
  }
}

////////////////////////////////////////////////////////////////////////////////

void keepOnlyAlphaRGBA8(unsigned char* buffer, size_t u, size_t v, size_t u2, const lpi::ColorRGB& color)
{
  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  {
    size_t index = y * u2 * 4 + x * 4;
    
    buffer[index + 0] = color.r;
    buffer[index + 1] = color.g;
    buffer[index + 2] = color.b;
  }
}

void keepOnlyAlphaRGBA32F(float* buffer, size_t u, size_t v, size_t u2, const lpi::ColorRGBd& color)
{
  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  {
    size_t index = y * u2 * 4 + x * 4;
    
    buffer[index + 0] = color.r;
    buffer[index + 1] = color.g;
    buffer[index + 2] = color.b;
  }
}

////////////////////////////////////////////////////////////////////////////////

void alphaToGreyRGBA8(unsigned char* buffer, size_t u, size_t v, size_t u2)
{
  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  {
    size_t index = y * u2 * 4 + x * 4;
    
    buffer[index + 0] = buffer[index + 3];
    buffer[index + 1] = buffer[index + 3];
    buffer[index + 2] = buffer[index + 3];
    buffer[index + 3] = 255;
  }
}

void alphaToGreyRGBA32F(float* buffer, size_t u, size_t v, size_t u2)
{
  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  {
    size_t index = y * u2 * 4 + x * 4;
    
    buffer[index + 0] = buffer[index + 3];
    buffer[index + 1] = buffer[index + 3];
    buffer[index + 2] = buffer[index + 3];
    buffer[index + 3] = 255;
  }
}

////////////////////////////////////////////////////////////////////////////////



void alphaExtractCloudRGBA8(unsigned char* buffer, size_t u, size_t v, size_t u2, const lpi::ColorRGB& leftColor, const lpi::ColorRGB& rightColor)
{
  //r (right color) will be treated as the origin
  lpi::Vector3 l(leftColor.r, leftColor.g, leftColor.b);
  lpi::Vector3 r(rightColor.r, rightColor.g, rightColor.b);
  lpi::Vector3 n = lpi::normalize(l - r);
  
  double distrl = lpi::distance(r, l);

  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  {
    size_t index = y * u2 * 4 + x * 4;
    lpi::Vector3 c(buffer[index + 0], buffer[index + 1], buffer[index + 2]);
    lpi::Vector3 rc = c - r;
    
    //distance of point (rc) to plane
    double distance = std::abs(n.x * rc.x + n.y * rc.y + n.z * rc.z);
    
    unsigned char alpha = 0;
    
    if(distance > 0 && lpi::sideOfPlaneGivenByNormal(rc, n))
    {
      double reldist = distance / distrl;
      if(reldist > 1) reldist = 1;
      if(reldist < 0) reldist = 0;
      alpha = (unsigned char)(reldist * 255);
    }
    
    buffer[index + 3] = alpha;
  }
}

void alphaExtractCloudRGBA32F(float* buffer, size_t u, size_t v, size_t u2, const lpi::ColorRGBd& leftColor, const lpi::ColorRGBd& rightColor)
{
  //r (right color) will be treated as the origin
  lpi::Vector3 l(leftColor.r, leftColor.g, leftColor.b);
  lpi::Vector3 r(rightColor.r, rightColor.g, rightColor.b);
  lpi::Vector3 n = lpi::normalize(l - r);
  
  double distrl = lpi::distance(r, l);

  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  {
    size_t index = y * u2 * 4 + x * 4;
    lpi::Vector3 c(buffer[index + 0], buffer[index + 1], buffer[index + 2]);
    lpi::Vector3 rc = c - r;
    
    //distance of point (rc) to plane
    float distance = std::abs(n.x * rc.x + n.y * rc.y + n.z * rc.z);
    
    float alpha = 0;
    
    if(distance > 0 && lpi::sideOfPlaneGivenByNormal(rc, n))
    {
      float reldist = distance / distrl;
      if(reldist > 1) reldist = 1;
      if(reldist < 0) reldist = 0;
      alpha = reldist;
    }
    
    buffer[index + 3] = alpha;
  }
}



////////////////////////////////////////////////////////////////////////////////

