/*
LodePaint

Copyright (c) 2009-2010 Lode Vandevenne
All rights reserved.

This file is part of LodePaint.

LodePaint is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LodePaint is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LodePaint.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <vector>

using std::size_t;

void getAlignedBufferRGBA8(std::vector<unsigned char>& out, unsigned char* buffer, size_t u, size_t v, size_t u2);
void getAlignedBufferRGBA32F(std::vector<float>& out, float* buffer, size_t u, size_t v, size_t u2);

//combsort: bubble sort made faster by using gaps to eliminate turtles
template<typename T>
void combsort(T* data, int amount)
{
  int gap = amount;
  bool swapped = false;
  while(gap > 1 || swapped)
  {
    //shrink factor 1.3
    gap = (gap * 10) / 13;
    if(gap == 9 || gap == 10) gap = 11;
    if (gap < 1) gap = 1;
    swapped = false;
    for (int i = 0; i < amount - gap; i++)
    {
      int j = i + gap;
      if (data[i] > data[j])
      {
        std::swap(data[i], data[j]); 
        swapped = true;
      }
    }
  }
}

//combsort: bubble sort made faster by using gaps to eliminate turtles
//indices: start by setting at 0-N, at the end its values are swapped together with the data so you know which was which
template<typename T>
void combsort(T* data, int* indices, int amount)
{
  int gap = amount;
  bool swapped = false;
  while(gap > 1 || swapped)
  {
    //shrink factor 1.3
    gap = (gap * 10) / 13;
    if(gap == 9 || gap == 10) gap = 11;
    if (gap < 1) gap = 1;
    swapped = false;
    for (int i = 0; i < amount - gap; i++)
    {
      int j = i + gap;
      if (data[i] > data[j])
      {
        std::swap(data[i], data[j]);
        std::swap(indices[i], indices[j]);
        swapped = true;
      }
    }
  }
}

//combsort: bubble sort made faster by using gaps to eliminate turtles
//xs and ys: both swapped together with the data, so you can track x and y pixel coordinates where the data came from
template<typename T>
void combsort(T* data, int* xs, int* ys, int amount)
{
  int gap = amount;
  bool swapped = false;
  while(gap > 1 || swapped)
  {
    //shrink factor 1.3
    gap = (gap * 10) / 13;
    if(gap == 9 || gap == 10) gap = 11;
    if (gap < 1) gap = 1;
    swapped = false;
    for (int i = 0; i < amount - gap; i++)
    {
      int j = i + gap;
      if (data[i] > data[j])
      {
        std::swap(data[i], data[j]);
        std::swap(xs[i], xs[j]);
        std::swap(ys[i], ys[j]);
        swapped = true;
      }
    }
  }
}

size_t countColorsRGBA8(const unsigned char* buffer, size_t u, size_t v, size_t u2);
size_t countColorsRGBA32F(const float* buffer, size_t u, size_t v, size_t u2);
size_t countColorsGrey8(const unsigned char* buffer, size_t u, size_t v, size_t u2);
size_t countColorsGrey32F(const float* buffer, size_t u, size_t v, size_t u2);
