/*
LodePaint

Copyright (c) 2009-2010 Lode Vandevenne
All rights reserved.

This file is part of LodePaint.

LodePaint is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LodePaint is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LodePaint.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <vector>
#include "progress.h"

using std::size_t;

/*
radius: for the initial blur
lt: low threshold
ht: high threshold
steps: which steps of the algorithm to perform:
  0: only do the blurring
  1: only return gradient magnitude
  2: only return gradient angle
  3: only return quantized gradient angle
  4: do up to non maxima suppression
  5: do the full canny edge detection
*/
void cannyEdgeDetectRGBA8(unsigned char* buffer, size_t u, size_t v, size_t u2, bool affect_alpha, Progress& progress,
                          double radius, int lt, int ht, int steps = 5);




