/*
LodePaint

Copyright (c) 2009-2010 Lode Vandevenne
All rights reserved.

This file is part of LodePaint.

LodePaint is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LodePaint is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LodePaint.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "transform.h"

#include "../lpi/lpi_math.h"

//antialiazing downscale. ov must be <= iv, ou <= iu
template<typename T, int N, int I>
void rescaleAAT(T* out, size_t ou2, size_t ou, size_t ov, const T* in, size_t iu2, size_t iu, size_t iv, bool wrapping)
{
  for(size_t y = 0; y < ov; y++)
  {
    double rely = ((double)y / ov) * iv; //where y is in the in image
    double fractY = rely - int(rely);

    for(size_t x = 0; x < ou; x++)
    {
      double relx = ((double)x / ou) * iu; //where x is in the in image
      double fractX = relx - int(relx);
      size_t index = y * ou2 * N + x * N;
      size_t x0 = (int)relx;
      size_t x1 = x0 + 1; if(x1 > iu - 1) x1 = wrapping ? 0 : iu - 1;

      double relx1 = ((double)(x + 1) / ou) * iu;
      double fractX1 = relx1 - int(relx1);
      int xb = (int)relx + 1;
      int xe = (int)relx1;
      int ixe = xe;  if(ixe > (int)iu - 1) ixe = wrapping ? 0 : iu - 1;
      double distx = relx1 - relx;

      double color[N]; //rgba
      for(int c = 0; c < N; c++) color[c] = 0;
      {
        double rely1 = ((double)(y + 1) / ov) * iv;
        double fractY1 = rely1 - int(rely1);
        int yb = (int)rely + 1;
        int ye = (int)rely1;
        int iye = ye; if(iye > (int)iv - 1) iye = wrapping ? 0 : iv - 1;
        double disty = rely1 - rely;
        for(size_t c = 0; c < N; c++)
        {
          for(int yy = yb; yy < ye; yy++)
          {
            int iy = yy;  if(iy > (int)iv - 1) iy = wrapping ? 0 : iv - 1;
            color[c] += (1.0 - fractX) * in[iy * iu2 * N + (int)relx * N + c];
            color[c] += fractX1 * in[iy * iu2 * N + ixe * N + c];
          }

          for(int xx = xb; xx < xe; xx++)
          {
            int ix = xx;  if(ix > (int)iu - 1) ix = wrapping ? 0 : iu - 1;
            color[c] += (1.0 - fractY) * in[(int)rely * iu2 * N + ix * N + c];
            color[c] += fractY1 * in[iye * iu2 * N + ix * N + c];
          }

          for(int yy = yb; yy < ye; yy++)
          for(int xx = xb; xx < xe; xx++)
          {
            int ix = xx;  if(ix > (int)iu - 1) ix = wrapping ? 0 : iu - 1;
            int iy = yy;  if(iy > (int)iv - 1) iy = wrapping ? 0 : iv - 1;
            color[c] += in[iy * iu2 * N + ix * N + c];
          }

          //the N corners
          color[c] += (1.0 - fractX) * (1.0 - fractY) * in[(int)rely * iu2 * N + (int)relx * N + c];
          color[c] += (1.0 - fractX) * fractY1 * in[iye * iu2 * N + (int)relx * N + c];
          color[c] += fractX1 * (1.0 - fractY) * in[(int)rely * iu2 * N + ixe * N + c];
          color[c] += fractX1 * fractY1 * in[iye * iu2 * N + ixe * N + c];

          color[c] /= (distx * disty);
        }
      }

      // On integers, the + 0.5 is done to round such that e.g. no alpha channels of 254 instead of 255 appear due to imprecisions.
      for(int c = 0; c < N; c++) out[index + c] = (T)(color[c] + (I == 0 ? 0.5 : 0));
    }
  }
}

//N = number of color channels (4 for RGBA, 1 for greyscale)
template<typename T, int N>
void rescaleNearestT(T* out, size_t ou2, size_t ou, size_t ov, const T* in, size_t iu2, size_t iu, size_t iv)
{
  for(size_t y = 0; y < ov; y++)
  {
    size_t iy = (y * iv) / ov;
    for(size_t x = 0; x < ou; x++)
    {
      size_t ix = (x * iu) / ou;
      size_t oindex = y * ou2 * N + x * N;
      size_t iindex = iy * iu2 * N + ix * N;

      for(int n = 0; n < N; n++) out[oindex + n] = in[iindex + n];
    }
  }
}

//N = number of color channels (4 for RGBA, 1 for greyscale)
//I = 0 for integer, 1 for floating point
template<typename T, int N, int I>
void rescaleBilinearT(T* out, size_t ou2, size_t ou, size_t ov, const T* in, size_t iu2, size_t iu, size_t iv, bool wrapping)
{
  if(ou <= iu && ov <= iv)
  {
    rescaleAAT<T, N, I>(out, ou2, ou, ov, in, iu2, iu, iv, wrapping);
    return;
  }

  for(size_t y = 0; y < ov; y++)
  {
    double rely = ((double)y / ov) * iv; //where y is in the in image
    double fractY = rely - int(rely);

    for(size_t x = 0; x < ou; x++)
    {
      double relx = ((double)x / ou) * iu; //where x is in the in image
      double fractX = relx - int(relx);
      size_t index = y * ou2 * N + x * N;
      size_t x0 = (int)relx;
      size_t x1 = x0 + 1; if(x1 > iu - 1) x1 = wrapping ? 0 : iu - 1;

      double color[N]; //rgba
      for(int c = 0; c < N; c++) color[c] = 0;

      if(iu < ou)
      {
        if(iv < ov) //increase width and height, so bilinear in both directions
        {
          size_t y0 = (int)rely;
          size_t y1 = y0 + 1; if(y1 > iv - 1) y1 = wrapping ? 0 : iv - 1;

          for(size_t c = 0; c < N; c++)
          {
            color[c] += fractX         * fractY         * in[y1 * iu2 * N + x1 * N + c];
            color[c] += fractX         * (1.0 - fractY) * in[y0 * iu2 * N + x1 * N + c];
            color[c] += (1.0 - fractX) * fractY         * in[y1 * iu2 * N + x0 * N + c];
            color[c] += (1.0 - fractX) * (1.0 - fractY) * in[y0 * iu2 * N + x0 * N + c];
          }
        }
        else //increase width (bilinear interpolation), decrease height (antialias)
        {
          double rely1 = ((double)(y + 1) / ov) * iv;
          double fractY1 = rely1 - int(rely1);
          int yb = (int)rely + 1;
          int ye = (int)rely1;
          int iye = ye; if(iye > (int)iv - 1) iye = wrapping ? 0 : iv - 1;
          double disty = rely1 - rely;

          for(size_t c = 0; c < N; c++)
          {
            for(int yy = yb; yy < ye; yy++)
            {
              int iy = yy; if(iy > (int)iv - 1) iy = wrapping ? 0 : iv - 1;
              color[c] += (1.0 - fractX) * in[iy * iu2 * N + x0 * N + c];
              color[c] += fractX * in[iy * iu2 * N + x1 * N + c];
            }

            //the 4 corners
            color[c] += (1.0 - fractX) * (1.0 - fractY) * in[(int)rely * iu2 * N + x0 * N + c];
            color[c] += (1.0 - fractX) * fractY1 * in[iye * iu2 * N + x0 * N + c];
            color[c] += fractX * (1.0 - fractY) * in[(int)rely * iu2 * N + x1 * N + c];
            color[c] += fractX * fractY1 * in[iye * iu2 * N + x1 * N + c];

            color[c] /= disty;
          }
        }
      }
      else //decrease width, so antialias
      {
        double relx1 = ((double)(x + 1) / ou) * iu;
        double fractX1 = relx1 - int(relx1);
        int xb = (int)relx + 1;
        int xe = (int)relx1;
        int ixe = xe;  if(ixe > (int)iu - 1) ixe = wrapping ? 0 : iu - 1;
        double distx = relx1 - relx;

        if(iv < ov) //decrease width (antialias), increase height (bilinear interpolation)
        {
          size_t y0 = (int)rely;
          size_t y1 = y0 + 1; if(y1 > iv - 1) y1 = wrapping ? 0 : iv - 1;

          for(size_t c = 0; c < N; c++)
          {
            for(int xx = xb; xx < xe; xx++)
            {
              int ix = xx;  if(ix > (int)iu - 1) ix = wrapping ? 0 : iu - 1;
              color[c] += (1.0 - fractY) * in[y0 * iu2 * N + ix * N + c];
              color[c] += fractY * in[y1 * iu2 * N + ix * N + c];
            }

            color[c] += (1.0 - fractX) * (1.0 - fractY) * in[y0 * iu2 * N + (int)relx * N + c];
            color[c] += (1.0 - fractX) * fractY * in[y1 * iu2 * N + (int)relx * N + c];
            color[c] += fractX1 * (1.0 - fractY) * in[y0 * iu2 * N + ixe * N + c];
            color[c] += fractX1 * fractY * in[y1 * iu2 * N + ixe * N + c];

            color[c] /= distx;
          }
        }
        else //decrease width and height
        {
          //Nothing to do, rescaleAAT is called in this case.
        }
      }

      // On integers, the + 0.5 is done to round such that e.g. no alpha channels of 254 instead of 255 appear due to imprecisions.
      for(int c = 0; c < N; c++) out[index + c] = (T)(color[c] + (I == 0 ? 0.5 : 0));
    }
  }
}



void rescaleBilinearRGBA8(unsigned char* out, size_t ou2, size_t ou, size_t ov, const unsigned char* in, size_t iu2, size_t iu, size_t iv, bool wrapping)
{
  rescaleBilinearT<unsigned char, 4, 0>(out, ou2, ou, ov, in, iu2, iu, iv, wrapping);
}

void rescaleBilinearRGBA32F(float* out, size_t ou2, size_t ou, size_t ov, const float* in, size_t iu2, size_t iu, size_t iv, bool wrapping)
{
  rescaleBilinearT<float, 4, 1>(out, ou2, ou, ov, in, iu2, iu, iv, wrapping);
}

void rescaleBilinearGrey8(unsigned char* out, size_t ou2, size_t ou, size_t ov, const unsigned char* in, size_t iu2, size_t iu, size_t iv, bool wrapping)
{
  rescaleBilinearT<unsigned char, 1, 0>(out, ou2, ou, ov, in, iu2, iu, iv, wrapping);
}

void rescaleBilinearGrey32F(float* out, size_t ou2, size_t ou, size_t ov, const float* in, size_t iu2, size_t iu, size_t iv, bool wrapping)
{
  rescaleBilinearT<float, 1, 1>(out, ou2, ou, ov, in, iu2, iu, iv, wrapping);
}

/*
If larger, just do "nearest"
If smaller, take the color that is the rarest in the area of pixels that is resized into one pixel.
This rescale is nice to make line drawing smaller, but bad for everything else.
*/
template<typename T, int N>
void rescaleRarestT(T* out, size_t ou2, size_t ou, size_t ov, const T* in, size_t iu2, size_t iu, size_t iv, bool wrapping)
{
  (void)wrapping;
  if(iu >= ou && iv >= ov)
  {
    for(size_t y = 0; y < ov; y++)
    {
      size_t iy0 = (y * iv) / ov;
      size_t iy1 = ((y + 1) * iv) / ov;
      for(size_t x = 0; x < ou; x++)
      {
        size_t ix0 = (x * iu) / ou;
        size_t ix1 = ((x + 1) * iu) / ou;
        double c[N];
        for(int n = 0; n < N; n++) c[n] = 0;
        size_t num = (iy1 - iy0 + 1) * (ix1 - ix0 + 1);

        for(size_t y2 = iy0 - 1; y2 <= iy1; y2++)
        for(size_t x2 = ix0 - 1; x2 <= ix1; x2++)
        {
          if(x2 >= iu || y2 >= iv) continue;
          size_t iindex = y2 * iu2 * N + x2 * N;
          for(int n = 0; n < N; n++) c[n] += in[iindex + n] / num;
        }

        double bestdist = 0.0;
        double best[N] = {0};

        for(size_t y2 = iy0; y2 < iy1; y2++)
        for(size_t x2 = ix0; x2 < ix1; x2++)
        {
          double dist = 0;
          size_t iindex = y2 * iu2 * N + x2 * N;
          for(int n = 0; n < N; n++) dist += (c[n] - in[iindex + n]) * (c[n] - in[iindex + n]);
          if(dist > bestdist)
          {
            for(int n = 0; n < N; n++) best[n] = in[iindex + n];
            bestdist = dist;
          }
        }

        size_t oindex = y * ou2 * N + x * N;
        for(int n = 0; n < N; n++) out[oindex + n] = (T)(best[n]);
      }
    }
  }
  else
  {
    rescaleNearestT<T, N>(out, ou2, ou, ov, in, iu2, iu, iv);
  }
  //TODO: also make somewhat work if it is smaller in one direction but larger in the other
}

void rescaleNearestRGBA8(unsigned char* out, size_t ou2, size_t ou, size_t ov, const unsigned char* in, size_t iu2, size_t iu, size_t iv)
{
  rescaleNearestT<unsigned char, 4>(out, ou2, ou, ov, in, iu2, iu, iv);
}

void rescaleNearestRGBA32F(float* out, size_t ou2, size_t ou, size_t ov, const float* in, size_t iu2, size_t iu, size_t iv)
{
  rescaleNearestT<float, 4>(out, ou2, ou, ov, in, iu2, iu, iv);
}

void rescaleNearestGrey8(unsigned char* out, size_t ou2, size_t ou, size_t ov, const unsigned char* in, size_t iu2, size_t iu, size_t iv)
{
  rescaleNearestT<unsigned char, 1>(out, ou2, ou, ov, in, iu2, iu, iv);
}

void rescaleNearestGrey32F(float* out, size_t ou2, size_t ou, size_t ov, const float* in, size_t iu2, size_t iu, size_t iv)
{
  rescaleNearestT<float, 1>(out, ou2, ou, ov, in, iu2, iu, iv);
}

void rescaleRarestRGBA8(unsigned char* out, size_t ou2, size_t ou, size_t ov, const unsigned char* in, size_t iu2, size_t iu, size_t iv, bool wrapping)
{
  rescaleRarestT<unsigned char, 4>(out, ou2, ou, ov, in, iu2, iu, iv, wrapping);
}

void rescaleRarestRGBA32F(float* out, size_t ou2, size_t ou, size_t ov, const float* in, size_t iu2, size_t iu, size_t iv, bool wrapping)
{
  rescaleRarestT<float, 4>(out, ou2, ou, ov, in, iu2, iu, iv, wrapping);
}

void rescaleRarestGrey8(unsigned char* out, size_t ou2, size_t ou, size_t ov, const unsigned char* in, size_t iu2, size_t iu, size_t iv, bool wrapping)
{
  rescaleRarestT<unsigned char, 1>(out, ou2, ou, ov, in, iu2, iu, iv, wrapping);
}

void rescaleRarestGrey32F(float* out, size_t ou2, size_t ou, size_t ov, const float* in, size_t iu2, size_t iu, size_t iv, bool wrapping)
{
  rescaleRarestT<float, 1>(out, ou2, ou, ov, in, iu2, iu, iv, wrapping);
}

double cubic(double v0, double v1, double v2, double v3, double t)
{
  double p = (v3 - v2) - (v0 - v1);
  double q = (v0 - v1) - p;
  double r = v2 - v0;
  double s = v1;
  double tt = t * t;

  return (p * (tt * t)) + (q * tt) + (r * t) + s;
}


//N = number of color channels (4 for RGBA, 1 for greyscale)
template<typename T, int N>
void addRemoveBordersT(T* out, size_t ou2, size_t ou, size_t ov
                     , const T* in, size_t iu2, size_t iu, size_t iv
                     , int top, int left, int bottom, int right
                     , const T* defaultColor)
{
  (void)bottom;
  (void)right;
  for(int y = 0; y < (int)ov; y++)
  for(int x = 0; x < (int)ou; x++)
  for(int c = 0; c < N; c++)
  {
    size_t oindex = y * ou2 * N + x * N + c; //output index

    int x2 = x - left;
    int y2 = y - top;

    if(x2 < (int)iu && x2 >= 0 && y2 < (int)iv && y2 >= 0)
    {
      size_t iindex = y2 * iu2 * N + x2 * N + c; //input index
      out[oindex] = in[iindex];
    }
    else
    {
      out[oindex] = defaultColor[c];
    }
  }
}

void addRemoveBordersRGBA8(unsigned char* out, size_t ou2, size_t ou, size_t ov
                     , const unsigned char* in, size_t iu2, size_t iu, size_t iv
                     , int top, int left, int bottom, int right
                     , const lpi::ColorRGB& defaultColor)
{
  unsigned char c[4] = { (unsigned char)defaultColor.r, (unsigned char)defaultColor.g,
                         (unsigned char)defaultColor.b, (unsigned char)defaultColor.a };
  addRemoveBordersT<unsigned char, 4>(out, ou2 ,ou, ov, in, iu2, iu, iv, top, left, bottom, right, c);
}


void addRemoveBordersRGBA32F(float* out, size_t ou2, size_t ou, size_t ov
                     , const float* in, size_t iu2, size_t iu, size_t iv
                     , int top, int left, int bottom, int right
                     , const lpi::ColorRGBd& defaultColor)
{
  float c[4] = { (float)defaultColor.r, (float)defaultColor.g, (float)defaultColor.b, (float)defaultColor.a };
  addRemoveBordersT<float, 4>(out, ou2 ,ou, ov, in, iu2, iu, iv, top, left, bottom, right, c);
}


void addRemoveBordersGrey8(unsigned char* out, size_t ou2, size_t ou, size_t ov
                     , const unsigned char* in, size_t iu2, size_t iu, size_t iv
                     , int top, int left, int bottom, int right
                     , const lpi::ColorRGB& defaultColor)
{
  unsigned char c = (defaultColor.r + defaultColor.g + defaultColor.b) / 3;
  addRemoveBordersT<unsigned char, 1>(out, ou2 ,ou, ov, in, iu2, iu, iv, top, left, bottom, right, &c);
}


void addRemoveBordersGrey32F(float* out, size_t ou2, size_t ou, size_t ov
                     , const float* in, size_t iu2, size_t iu, size_t iv
                     , int top, int left, int bottom, int right
                     , const lpi::ColorRGBd& defaultColor)
{
  float c = (defaultColor.r + defaultColor.g + defaultColor.b) / 3;
  addRemoveBordersT<float, 1>(out, ou2 ,ou, ov, in, iu2, iu, iv, top, left, bottom, right, &c);
}



//N = number of color channels (4 for RGBA, 1 for greyscale)
template<typename T, int N>
void rescaleSkewedT(T* out, size_t ou2, size_t ou, size_t ov, const T* in, size_t iu2, size_t iu, size_t iv, const T* defaultColor)
{
  size_t iindex = 0;
  size_t iindex2 = 0;
  size_t oindex = 0;
  for(size_t y = 0; y < ov; y++)
  {
    for(size_t x = 0; x < ou; x++)
    {
      if(iindex < iu * iv * N)
      {
        for(int c = 0; c < N; c++) out[oindex + c] = in[iindex2 + c];
      }
      else
      {
        for(int c = 0; c < N; c++) out[oindex + c] = defaultColor[c];
      }
      iindex+=N;
      iindex2+=N;
      oindex+=N;
      if(iindex % iu == 0) iindex2 += (iu2 - iu) * N;
    }
    oindex += (ou2 - ou) * N;
  }
}



void rescaleSkewedRGBA8(unsigned char* out, size_t ou2, size_t ou, size_t ov
                     , const unsigned char* in, size_t iu2, size_t iu, size_t iv
                     , const lpi::ColorRGB& defaultColor)
{
  unsigned char c[4] = { (unsigned char)defaultColor.r, (unsigned char)defaultColor.g,
                         (unsigned char)defaultColor.b, (unsigned char)defaultColor.a };
  rescaleSkewedT<unsigned char, 4>(out, ou2 ,ou, ov, in, iu2, iu, iv, c);
}


void rescaleSkewedRGBA32F(float* out, size_t ou2, size_t ou, size_t ov
                     , const float* in, size_t iu2, size_t iu, size_t iv
                     , const lpi::ColorRGBd& defaultColor)
{
  float c[4] = { (float)defaultColor.r, (float)defaultColor.g, (float)defaultColor.b, (float)defaultColor.a };
  rescaleSkewedT<float, 4>(out, ou2 ,ou, ov, in, iu2, iu, iv, c);
}


void rescaleSkewedGrey8(unsigned char* out, size_t ou2, size_t ou, size_t ov
                     , const unsigned char* in, size_t iu2, size_t iu, size_t iv
                     , const lpi::ColorRGB& defaultColor)
{
  unsigned char c = (defaultColor.r + defaultColor.g + defaultColor.b) / 3;
  rescaleSkewedT<unsigned char, 1>(out, ou2 ,ou, ov, in, iu2, iu, iv, &c);
}


void rescaleSkewedGrey32F(float* out, size_t ou2, size_t ou, size_t ov
                     , const float* in, size_t iu2, size_t iu, size_t iv
                     , const lpi::ColorRGBd& defaultColor)
{
  float c = (defaultColor.r + defaultColor.g + defaultColor.b) / 3;
  rescaleSkewedT<float, 1>(out, ou2 ,ou, ov, in, iu2, iu, iv, &c);
}


////////////////////////////////////////////////////////////////////////////////

//Copied from the website of Paul Breeuwsma ("Anything at this page may be copied and modified.")
template<typename U, typename T>
U cubicInterpolate(T p[4], double x)
{
  return p[1] + x*((U)p[2] - p[0] + x*(2*p[0] - 5*p[1] + 4*p[2] - p[3] + x*(3*((U)p[1] - p[2]) + p[3] - p[0]))) / 2;
}

template<typename U, typename T>
U bicubicInterpolate(T p[][4], double x, double y)
{
  U arr[4];
  arr[0] = cubicInterpolate<U>(p[0], y);
  arr[1] = cubicInterpolate<U>(p[1], y);
  arr[2] = cubicInterpolate<U>(p[2], y);
  arr[3] = cubicInterpolate<U>(p[3], y);
  return cubicInterpolate<U>(arr, x);
}

//Special treatment for unsigned chars (bicubic rescale can overflow, no problem for floating point images, but needs to be clamped for chars)
unsigned char bicubicInterpolateU(unsigned char p[][4], double x, double y)
{
  short result = bicubicInterpolate<short, unsigned char>(p, x, y);
  if(result < 0) result = 0;
  if(result > 255) result = 255;
  return (unsigned char)result;
}

/*I: is integer?*/
template<typename T, int N, int I>
void rescaleBicubicT(T* out, size_t ou2, size_t ou, size_t ov, const T* in, size_t iu2, size_t iu, size_t iv, bool wrapping, T ipol(T[][4], double, double))
{
  T p[4][4];

  if(ou <= iu && ov <= iv)
  {
    rescaleAAT<T, N, I>(out, ou2, ou, ov, in, iu2, iu, iv, wrapping);
    return;
  }

  for(size_t y = 0; y < ov; y++)
  {
    double rely = ((double)y / ov) * iv; //where y is in the in image
    double fractY = rely - int(rely);
    int y0 = (int)rely;

    for(size_t x = 0; x < ou; x++)
    {
      double relx = ((double)x / ou) * iu; //where x is in the in image
      double fractX = relx - int(relx);
      int x0 = (int)relx;

      for(size_t c = 0; c < N; c++)
      {
        for(size_t j = 0; j < 4; j++)
        for(size_t i = 0; i < 4; i++)
        {
          int xi = (int)(x0 + i) - 1;
          int yj = (int)(y0 + j) - 1;
          if(wrapping) { xi = lpi::wrap(xi, 0, iu); yj = lpi::wrap(yj, 0, iv); }
          else { xi = lpi::clamp(xi, 0, (int)iu - 1); yj = lpi::clamp(yj, 0, (int)iv - 1); }

          p[i][j] = in[N * iu2 * yj + N * xi + c];
        }

        T d = (T)(ipol(p, fractX, fractY) + (I == 0 ? 0.5 : 0));
        out[N * ou2 * y + N * x + c] = d;
      }
    }
  }
}

void rescaleBicubicRGBA8(unsigned char* out, size_t ou2, size_t ou, size_t ov, const unsigned char* in, size_t iu2, size_t iu, size_t iv, bool wrapping)
{
  rescaleBicubicT<unsigned char, 4, 0>(out, ou2, ou, ov, in, iu2, iu, iv, wrapping, bicubicInterpolateU);
}

void rescaleBicubicRGBA32F(float* out, size_t ou2, size_t ou, size_t ov, const float* in, size_t iu2, size_t iu, size_t iv, bool wrapping)
{
  rescaleBicubicT<float, 4, 1>(out, ou2, ou, ov, in, iu2, iu, iv, wrapping, bicubicInterpolate<float, float>);
}

void rescaleBicubicGrey8(unsigned char* out, size_t ou2, size_t ou, size_t ov, const unsigned char* in, size_t iu2, size_t iu, size_t iv, bool wrapping)
{
  rescaleBicubicT<unsigned char, 1, 0>(out, ou2, ou, ov, in, iu2, iu, iv, wrapping, bicubicInterpolateU);
}

void rescaleBicubicGrey32F(float* out, size_t ou2, size_t ou, size_t ov, const float* in, size_t iu2, size_t iu, size_t iv, bool wrapping)
{
  rescaleBicubicT<float, 1, 1>(out, ou2, ou, ov, in, iu2, iu, iv, wrapping, bicubicInterpolate<float, float>);
}

////////////////////////////////////////////////////////////////////////////////
