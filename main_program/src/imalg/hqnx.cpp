/*
LodePaint

Copyright (c) 2009-2010 Lode Vandevenne
All rights reserved.

This file is part of LodePaint.

LodePaint is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LodePaint is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LodePaint.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "hqnx.h"

#include <cstdlib>
#include <iostream>

namespace
{
  /*
  RGBA struct is made so that it can nicely match the memory structure
  of an incoming unsigned char buffer.
  This is much more convenient to work and calculate with.
  */
  struct RGBA
  {
    unsigned char r;
    unsigned char g;
    unsigned char b;
    unsigned char a;
  };
  
  bool operator==(const RGBA& a, const RGBA& b)
  {
    //TODO: use a way that works no matter how many bits int is
    return (*(int*)(&a)) == (*(int*)(&b));
  }


  /*
  Returns whether or not the two colors are different enough. In the original
  hq2x this was done using YUV color format, here not but the result looks
  similar enough...
  This also includes the alpha channel, unlike the original hq2x.
  */
  bool different(RGBA a, RGBA b)
  {
    if(a == b) return false;
    
    int diffr = std::abs((int)a.r - (int)b.r);
    int diffg = std::abs((int)a.g - (int)b.g);
    int diffb = std::abs((int)a.b - (int)b.b);
    int diffa = std::abs((int)a.a - (int)b.a);
    int diffbrightness = std::abs((int)(a.r+a.g+a.b) - (int)(b.r+b.g+b.b));
    (void)diffbrightness;
    
    static const int A = 16;
    static const int B = 16 * 3;
    static const int C = 16;
    
    if(diffa > A) return true;
    if(diffbrightness > B) return true;
    if(diffr > C || diffg > C || diffb > C) return true;
    return false;
  }

}


template<int Fa, int Fb>
void interpolate(RGBA& o, RGBA a, RGBA b)
{
  o.r = (a.r * Fa + b.r * Fb) / (Fa + Fb);
  o.g = (a.g * Fa + b.g * Fb) / (Fa + Fb);
  o.b = (a.b * Fa + b.b * Fb) / (Fa + Fb);
  o.a = (a.a * Fa + b.a * Fb) / (Fa + Fb);
}

template<int Fa, int Fb, int Fc>
void interpolate(RGBA& o, RGBA a, RGBA b, RGBA c)
{
  o.r = (a.r * Fa + b.r * Fb + c.r * Fc) / (Fa + Fb + Fc);
  o.g = (a.g * Fa + b.g * Fb + c.g * Fc) / (Fa + Fb + Fc);
  o.b = (a.b * Fa + b.b * Fb + c.b * Fc) / (Fa + Fb + Fc);
  o.a = (a.a * Fa + b.a * Fb + c.a * Fc) / (Fa + Fb + Fc);
}

/*
This data defines the original hq2x algorithm, the value in the table
indicates which interpolation algorithm is called. This table is
applied 4 times, for 4 rotations, for 4 pixels in the result per
one pixel of the original image.
*/

static const int DATA_HQ2X[256] = {
//-0--1--2--3--4--5--6--7--8--9- 9-10-12-13-14-15
   3, 3, 5, 1, 3, 3, 5, 1, 4, 2,12,13, 4, 2,11, 9, //000-015
   3, 3, 5, 6, 3, 3, 5, 6, 4, 2, 9, 9, 4, 2, 0, 9, //016-031 
   3, 3, 5, 1, 3, 3, 5, 1, 4, 2,11, 9, 4, 2, 8,10, //032-047
   3, 3, 5, 6, 3, 3, 5, 6, 4, 2, 8,13, 4, 2, 0,10, //048-063
   3, 3, 5, 1, 3, 3, 5, 1, 4, 7, 9,13, 4, 7, 8,13, //064-079
   3, 3, 5, 1, 3, 3, 5, 1, 4, 2, 8,13, 4, 2, 8,13, //080-095
   3, 3, 5, 1, 3, 3, 5, 1, 4, 7, 0, 9, 4, 7, 0,10, //096-111
   3, 3, 5, 1, 3, 3, 5, 6, 4, 2, 8,13, 4, 7, 0,10, //112-127
   3, 3, 5, 1, 3, 3, 5, 1, 4, 2,12,13, 4, 2,11, 9, //128-143
   3, 3, 5, 1, 3, 3, 5, 1, 4, 2, 8,13, 4, 2, 8,13, //144-159
   3, 3, 5, 1, 3, 3, 5, 1, 4, 2,11, 9, 4, 2, 8,10, //160-175
   3, 3, 5, 1, 3, 3, 5, 1, 4, 2, 8, 9, 4, 2, 0,10, //176-191
   3, 3, 5, 1, 3, 3, 5, 1, 4, 2, 8,13, 4, 2, 8, 9, //192-207
   3, 3, 5, 1, 3, 3, 5, 1, 4, 2, 8,13, 4, 2, 0,13, //208-223
   3, 3, 5, 1, 3, 3, 5, 1, 4, 2, 8,13, 4, 2, 0,10, //224-239
   3, 3, 5, 1, 3, 3, 5, 1, 4, 2, 0,13, 4, 2, 0,10, //240-255
};

static int rotatePattern(int pattern)
{
  /*
  Turns   Into
   012     530
   3 4     6 1
   567     742
  The numbers are the index of the bit in the pattern integer of the compared neighbor pixel.
  there's probably a shorter way with shift and boolean operators but this works too...
  */
  bool b0 = pattern & 128;
  bool b1 = pattern & 64;
  bool b2 = pattern & 32;
  bool b3 = pattern & 16;
  bool b4 = pattern & 8;
  bool b5 = pattern & 4;
  bool b6 = pattern & 2;
  bool b7 = pattern & 1;
  pattern = 0;
  pattern |= (b5 * 128);
  pattern |= (b3 * 64);
  pattern |= (b0 * 32);
  pattern |= (b6 * 16);
  pattern |= (b1 * 8);
  pattern |= (b7 * 4);
  pattern |= (b4 * 2);
  pattern |= (b2 * 1);
  return pattern;
}

void applyHQ2XTable(RGBA& out0, RGBA& out1, RGBA& out2, RGBA& out3 //the 4 output pixels that take place of one pixel of the original image
                  , const RGBA* pixel //the 9 neighbor pixels from the original image
                  , int pattern) //the pattern that says in what way the neighbors differ or are equal.
{
  switch(DATA_HQ2X[pattern])
  {
    case 0: interpolate<3, 1>(out0, pixel[4], pixel[0]); break;
    case 1: interpolate<3, 1>(out0, pixel[4], pixel[3]); break;
    case 2: interpolate<3, 1>(out0, pixel[4], pixel[1]); break;
    case 3: interpolate<2, 1, 1>(out0, pixel[4], pixel[3], pixel[1]); break;
    case 4: interpolate<2, 1, 1>(out0, pixel[4], pixel[0], pixel[1]); break;
    case 5: interpolate<2, 1, 1>(out0, pixel[4], pixel[0], pixel[3]); break;
    case 6: different(pixel[1], pixel[5]) ? interpolate<3, 1>(out0, pixel[4], pixel[3]) : interpolate<5, 2, 1>(out0, pixel[4], pixel[1], pixel[3]); break;
    case 7: different(pixel[7], pixel[3]) ? interpolate<3, 1>(out0, pixel[4], pixel[1]) : interpolate<5, 2, 1>(out0, pixel[4], pixel[3], pixel[1]); break;
    case 8: different(pixel[3], pixel[1]) ? interpolate<3, 1>(out0, pixel[4], pixel[0]) :  interpolate<6, 1, 1>(out0, pixel[4], pixel[3], pixel[1]); break;
    case 9: different(pixel[3], pixel[1]) ? (void)(out0 = pixel[4]) : interpolate<2, 3, 3>(out0, pixel[4], pixel[3], pixel[1]); break;
    case 10: different(pixel[3], pixel[1]) ? (void)(out0 = pixel[4]) : interpolate<14, 1, 1>(out0, pixel[4], pixel[3], pixel[1]); break;
    case 11: different(pixel[3], pixel[1]) ? interpolate<3, 1>(out0, pixel[4], pixel[0]) : interpolate<2, 3, 3>(out0, pixel[4], pixel[3], pixel[1]); break;
    case 12: different(pixel[3], pixel[1]) ? interpolate<3, 1>(out0, pixel[4], pixel[0]) : interpolate<2, 1, 1>(out0, pixel[4], pixel[3], pixel[1]); break;
    case 13: different(pixel[3], pixel[1]) ? (void)(out0 = pixel[4]) : interpolate<2, 1, 1>(out0, pixel[4], pixel[3], pixel[1]); break;
  }
  
  pattern = rotatePattern(pattern);
  

  switch(DATA_HQ2X[pattern])
  {
    case 0: interpolate<3, 1>(out2, pixel[4], pixel[6]); break;
    case 1: interpolate<3, 1>(out2, pixel[4], pixel[7]); break;
    case 2: interpolate<3, 1>(out2, pixel[4], pixel[3]); break;
    case 3: interpolate<2, 1, 1>(out2, pixel[4], pixel[7], pixel[3]); break;
    case 4: interpolate<2, 1, 1>(out2, pixel[4], pixel[6], pixel[3]); break;
    case 5: interpolate<2, 1, 1>(out2, pixel[4], pixel[6], pixel[7]); break;
    case 6: different(pixel[3], pixel[1]) ? interpolate<3, 1>(out2, pixel[4], pixel[7]) : interpolate<5, 2, 1>(out2, pixel[4], pixel[3], pixel[7]); break;
    case 7: different(pixel[5], pixel[7]) ? interpolate<3, 1>(out2, pixel[4], pixel[3]) : interpolate<5, 2, 1>(out2, pixel[4], pixel[7], pixel[3]); break;
    case 8: different(pixel[7], pixel[3]) ? interpolate<3, 1>(out2, pixel[4], pixel[6]) :  interpolate<6, 1, 1>(out2, pixel[4], pixel[7], pixel[3]); break;
    case 9: different(pixel[7], pixel[3]) ? (void)(out2 = pixel[4]) : interpolate<2, 3, 3>(out2, pixel[4], pixel[7], pixel[3]); break;
    case 10: different(pixel[7], pixel[3]) ? (void)(out2 = pixel[4]) : interpolate<14, 1, 1>(out2, pixel[4], pixel[7], pixel[3]); break;
    case 11: different(pixel[7], pixel[3]) ? interpolate<3, 1>(out2, pixel[4], pixel[6]) : interpolate<2, 3, 3>(out2, pixel[4], pixel[7], pixel[3]); break;
    case 12: different(pixel[7], pixel[3]) ? interpolate<3, 1>(out2, pixel[4], pixel[6]) : interpolate<2, 1, 1>(out2, pixel[4], pixel[7], pixel[3]); break;
    case 13: different(pixel[7], pixel[3]) ? (void)(out2 = pixel[4]) : interpolate<2, 1, 1>(out2, pixel[4], pixel[7], pixel[3]); break;
  }
  
  pattern = rotatePattern(pattern);

  switch(DATA_HQ2X[pattern])
  {
    case 0: interpolate<3, 1>(out3, pixel[4], pixel[8]); break;
    case 1: interpolate<3, 1>(out3, pixel[4], pixel[5]); break;
    case 2: interpolate<3, 1>(out3, pixel[4], pixel[7]); break;
    case 3: interpolate<2, 1, 1>(out3, pixel[4], pixel[5], pixel[7]); break;
    case 4: interpolate<2, 1, 1>(out3, pixel[4], pixel[8], pixel[7]); break;
    case 5: interpolate<2, 1, 1>(out3, pixel[4], pixel[8], pixel[5]); break;
    case 6: different(pixel[7], pixel[3]) ? interpolate<3, 1>(out3, pixel[4], pixel[5]) : interpolate<5, 2, 1>(out3, pixel[4], pixel[7], pixel[5]); break;
    case 7: different(pixel[1], pixel[5]) ? interpolate<3, 1>(out3, pixel[4], pixel[7]) : interpolate<5, 2, 1>(out3, pixel[4], pixel[5], pixel[7]); break;
    case 8: different(pixel[5], pixel[7]) ? interpolate<3, 1>(out3, pixel[4], pixel[8]) :  interpolate<6, 1, 1>(out3, pixel[4], pixel[5], pixel[7]); break;
    case 9: different(pixel[5], pixel[7]) ? (void)(out3 = pixel[4]) : interpolate<2, 3, 3>(out3, pixel[4], pixel[5], pixel[7]); break;
    case 10: different(pixel[5], pixel[7]) ? (void)(out3 = pixel[4]) : interpolate<14, 1, 1>(out3, pixel[4], pixel[5], pixel[7]); break;
    case 11: different(pixel[5], pixel[7]) ? interpolate<3, 1>(out3, pixel[4], pixel[8]) : interpolate<2, 3, 3>(out3, pixel[4], pixel[5], pixel[7]); break;
    case 12: different(pixel[5], pixel[7]) ? interpolate<3, 1>(out3, pixel[4], pixel[8]) : interpolate<2, 1, 1>(out3, pixel[4], pixel[5], pixel[7]); break;
    case 13: different(pixel[5], pixel[7]) ? (void)(out3 = pixel[4]) : interpolate<2, 1, 1>(out3, pixel[4], pixel[5], pixel[7]); break;
  }
  
  pattern = rotatePattern(pattern);

  switch(DATA_HQ2X[pattern])
  {
    case 0: interpolate<3, 1>(out1, pixel[4], pixel[2]); break;
    case 1: interpolate<3, 1>(out1, pixel[4], pixel[1]); break;
    case 2: interpolate<3, 1>(out1, pixel[4], pixel[5]); break;
    case 3: interpolate<2, 1, 1>(out1, pixel[4], pixel[1], pixel[5]); break;
    case 4: interpolate<2, 1, 1>(out1, pixel[4], pixel[2], pixel[5]); break;
    case 5: interpolate<2, 1, 1>(out1, pixel[4], pixel[2], pixel[1]); break;
    case 6: different(pixel[5], pixel[7]) ? interpolate<3, 1>(out1, pixel[4], pixel[1]) : interpolate<5, 2, 1>(out1, pixel[4], pixel[5], pixel[1]); break;
    case 7: different(pixel[3], pixel[1]) ? interpolate<3, 1>(out1, pixel[4], pixel[5]) : interpolate<5, 2, 1>(out1, pixel[4], pixel[1], pixel[5]); break;
    case 8: different(pixel[1], pixel[5]) ? interpolate<3, 1>(out1, pixel[4], pixel[2]) :  interpolate<6, 1, 1>(out1, pixel[4], pixel[1], pixel[5]); break;
    case 9: different(pixel[1], pixel[5]) ? (void)(out1 = pixel[4]) : interpolate<2, 3, 3>(out1, pixel[4], pixel[1], pixel[5]); break;
    case 10: different(pixel[1], pixel[5]) ? (void)(out1 = pixel[4]) : interpolate<14, 1, 1>(out1, pixel[4], pixel[1], pixel[5]); break;
    case 11: different(pixel[1], pixel[5]) ? interpolate<3, 1>(out1, pixel[4], pixel[2]) : interpolate<2, 3, 3>(out1, pixel[4], pixel[1], pixel[5]); break;
    case 12: different(pixel[1], pixel[5]) ? interpolate<3, 1>(out1, pixel[4], pixel[2]) : interpolate<2, 1, 1>(out1, pixel[4], pixel[1], pixel[5]); break;
    case 13: different(pixel[1], pixel[5]) ? (void)(out1 = pixel[4]) : interpolate<2, 1, 1>(out1, pixel[4], pixel[1], pixel[5]); break;
  }
}

void hq2x(unsigned char* out, size_t ou, size_t ov, const unsigned char* in, size_t iu, size_t iv, bool wrapping)
{
  (void)ov;
  
  RGBA pixel[9]; //9 pixels of the input
  //pixel order:
  // 012
  // 345
  // 678
  
  for(size_t y = 0; y < iv; y++)
  {
    size_t ym = y == 0 ? (wrapping ? iv - 1 : 0) : y - 1; //y minus 1
    size_t y0 = y;
    size_t y1 = y == iv - 1 ? (wrapping ? 0 : iv - 1) : y + 1;

    for(size_t x = 0; x < iu; x++)
    {
      size_t xm = x == 0 ? (wrapping ? iu - 1 : 0) : x - 1; //x minus 1
      size_t x0 = x;
      size_t x1 = x == iu - 1 ? (wrapping ? 0 : iu - 1) : x + 1;

      pixel[0] = ((const RGBA*)in)[ym * iu + xm];
      pixel[1] = ((const RGBA*)in)[ym * iu + x0];
      pixel[2] = ((const RGBA*)in)[ym * iu + x1];
      pixel[3] = ((const RGBA*)in)[y0 * iu + xm];
      pixel[4] = ((const RGBA*)in)[y0 * iu + x0];
      pixel[5] = ((const RGBA*)in)[y0 * iu + x1];
      pixel[6] = ((const RGBA*)in)[y1 * iu + xm];
      pixel[7] = ((const RGBA*)in)[y1 * iu + x0];
      pixel[8] = ((const RGBA*)in)[y1 * iu + x1];

      int pattern = 0;
      int bit = 1;
      for(size_t i = 0; i < 9; i++)
      {
        if(i == 4) continue; //center pixel
        if(different(pixel[i], pixel[4])) pattern |= bit;
        bit *= 2;
      }
        
      RGBA& out0 = ((RGBA*)(out))[y * 2 * ou + x * 2];
      RGBA& out1 = ((RGBA*)(out))[y * 2* ou + (x * 2 + 1)];
      RGBA& out2 = ((RGBA*)(out))[(y * 2 + 1) * ou + x * 2];
      RGBA& out3 = ((RGBA*)(out))[(y * 2 + 1) * ou + (x * 2 + 1)];

      applyHQ2XTable(out0, out1, out2, out3, pixel, pattern);
    }
  }
}
