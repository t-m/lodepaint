/*
LodePaint

Copyright (c) 2009-2012 Lode Vandevenne
All rights reserved.

This file is part of LodePaint.

LodePaint is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LodePaint is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LodePaint.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "reduction.h"

#include "../lpi/lpi_color.h"
#include "../lpi/lpi_math.h"

#include <algorithm>
#include <cassert>

/*
reduceer naar N kleuren met median cut

maybe first sort the colors and group the same ones together in one struct containing rgb and count?

genereer box met erin alle punten (elk punt heeft rgba coordinaten)
N keer:
  -zoek tussen alle boxen, de langste zijde (dus 3 zijden per box checken)
  -in richting van die zijde, box in 2 doen op de mediaan

then apply palette (using octree?)
*/

struct Color
{
  unsigned color;
  unsigned count;
};

bool sortfun_r(const Color& a, const Color& b)
{
  return (a.color & 0xFF000000U) < (b.color & 0xFF000000U);
}

bool sortfun_g(const Color& a, const Color& b)
{
  return (a.color & 0x00FF0000U) < (b.color & 0x00FF0000U);
}

bool sortfun_b(const Color& a, const Color& b)
{
  return (a.color & 0x0000FF00U) < (b.color & 0x0000FF00U);
}

bool sortfun_a(const Color& a, const Color& b)
{
  return (a.color & 0x000000FFU) < (b.color & 0x000000FFU);
}

//for median cut
struct Box
{
  std::vector<Color> colors;
  double sider;
  double sideg;
  double sideb;
  double sidea;
  int longestindex; //0=r, 1=g, 2=b, 3=a
  double longest;

  int r;
  int g;
  int b;
  int a;
  

  void calculateSize()
  {
    int minr = 255, maxr = 0, ming = 255, maxg = 0, minb = 255, maxb = 0, mina = 255, maxa = 0;
    for(size_t i = 0; i < colors.size(); i++)
    {
      int r = (colors[i].color >> 24) & 255;
      int g = (colors[i].color >> 16) & 255;
      int b = (colors[i].color >> 8) & 255;
      int a = (colors[i].color >> 0) & 255;

      //smaller alpha makes R, G, B closer
      if(a < 255)
      {
        if(r < minr) r += (minr - r) * (255 - a) / 255;
        if(r > maxr) r -= (r - maxr) * (255 - a) / 255;
        if(g < ming) g += (ming - g) * (255 - a) / 255;
        if(g > maxg) g -= (g - maxg) * (255 - a) / 255;
        if(b < minb) b += (minb - b) * (255 - a) / 255;
        if(b > maxb) b -= (b - maxb) * (255 - a) / 255;
      }
      
      minr = std::min(minr, r);
      maxr = std::max(maxr, r);
      ming = std::min(ming, g);
      maxg = std::max(maxg, g);
      minb = std::min(minb, b);
      maxb = std::max(maxb, b);
      mina = std::min(mina, a);
      maxa = std::max(maxa, a);
    }
    sider = maxr - minr;
    sideg = maxg - ming;
    sideb = maxb - minb;
    // alpha difference is way larger than R, G or B difference. Prevent opaque pixels getting too translucent.
    sidea = (maxa - mina) * 3;

    if(sider > sideg && sider > sideb && sider > sidea)
    {
      longestindex = 0;
      longest = sider;
    }
    else if(sideg > sideb && sideg > sidea)
    {
      longestindex = 1;
      longest = sideg;
    }
    else if(sideb > sidea)
    {
      longestindex = 2;
      longest = sideb;
    }
    else
    {
      longestindex = 3;
      longest = sidea;
    }
  }

  void calculateColor()
  {
    double rd = 0, gd = 0, bd = 0, ad = 0;
    for(size_t i = 0; i < colors.size(); i++)
    {
      unsigned color = colors[0].color;
      rd += ((color >> 24) & 255) / (double)(colors.size());
      gd += ((color >> 16) & 255) / (double)(colors.size());
      bd += ((color >>  8) & 255) / (double)(colors.size());
      ad += ((color >>  0) & 255) / (double)(colors.size());
    }
    r = lpi::round(rd);
    g = lpi::round(gd);
    b = lpi::round(bd);
    a = lpi::round(ad);
  }

  void split(Box*& out1, Box*& out2)
  {
    if(colors.size() <= 1)
    {
      out1 = 0;
      out2 = 0;
      return;
    }
    
    if(longestindex == 0) std::sort(colors.begin(), colors.end(), sortfun_r);
    else if(longestindex == 1) std::sort(colors.begin(), colors.end(), sortfun_g);
    else if(longestindex == 2) std::sort(colors.begin(), colors.end(), sortfun_b);
    else if(longestindex == 3) std::sort(colors.begin(), colors.end(), sortfun_a);

    size_t total = 0;
    for(size_t i = 0; i < colors.size(); i++) total += colors[i].count;

    // Get median. The color at j'th index is the median.
    size_t count = 0;
    size_t j = 0;
    while(count <= total / 2)
    {
      count += colors[j].count;
      j++;
    }
    // ensure every box gets at least something
    if(j == 0) j++;
    if(j == colors.size()) j--;

    out1 = new Box();
    out2 = new Box();

    out1->colors = std::vector<Color>(colors.begin(), colors.begin() + j);
    out2->colors = std::vector<Color>(colors.begin() + j, colors.end());

    assert(out1->colors.size());
    assert(out2->colors.size());

    out1->calculateSize();
    out2->calculateSize();
  }
};

void mediancutRGBA8(unsigned char* buffer, size_t u, size_t v, size_t u2, size_t amount, Progress& progress)
{
  // The std::map is used to group colors so that there's less to sort
  // they key is the color, the value the amount of it
  std::map<unsigned, unsigned> m;
  for(size_t y = 0; y < v; y++)
  {
    if(progress.handle(y / 2, v)) return;
    for(size_t x = 0; x < u; x++)
    {
      size_t index = y * u2 * 4 + x * 4;
      unsigned rgba = lpi::RGBAtoINT(&buffer[index]);
      m[rgba]++;
    }
  }

  std::vector<Box*> boxes;
  boxes.push_back(new Box());
  std::vector<Color>& colors = boxes.back()->colors;

  
  for(std::map<unsigned, unsigned>::const_iterator it = m.begin(); it != m.end(); it++)
  {
    Color color;
    color.color = it->first;
    color.count = it->second;
    colors.push_back(color);
  }

  boxes.back()->calculateSize();

  for(size_t i = 1; i < amount; i++)
  {
    double biggest = -1;
    size_t biggestbox = 0;
    for(size_t j = 0; j < boxes.size(); j++)
    {
      if(boxes[j]->longest > biggest)
      {
        biggest = boxes[j]->longest;
        biggestbox = j;
      }
    }
    Box* a;
    Box* b;
    boxes[biggestbox]->split(a, b);
    if(!a || !b) break;
    delete boxes[biggestbox];
    boxes[biggestbox] = a;
    boxes.push_back(b);
  }

  //key = color RGBA, value = palette index
  std::map<unsigned, unsigned> colorpalette;
  for(size_t i = 0; i < boxes.size(); i++)
  {
    Box& b = *boxes[i];
    b.calculateColor();
    for(size_t j = 0; j < b.colors.size(); j++)
    {
      colorpalette[b.colors[j].color] = i;
    }
  }

  for(size_t y = 0; y < v; y++)
  {
    if(progress.handle( v / 2 + y / 2, v)) return;
    for(size_t x = 0; x < u; x++)
    {
      size_t index = y * u2 * 4 + x * 4;
      unsigned p = colorpalette[lpi::RGBAtoINT(&buffer[index])];
      Box& box = *boxes[p];

      // TODO: take average color of the box instead of first (or average snapped to sides so palette remains more opaque)
      buffer[index + 0] = box.r;
      buffer[index + 1] = box.g;
      buffer[index + 2] = box.b;
      buffer[index + 3] = box.a;
    }
  }

  for(size_t i = 0; i < boxes.size(); i++) delete boxes[i];
}
