/*
LodePaint

Copyright (c) 2009-2010 Lode Vandevenne
All rights reserved.

This file is part of LodePaint.

LodePaint is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LodePaint is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LodePaint.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "blur.h"

#include "util.h"

#include <cmath>
#include <limits>
#include <vector>
#include <algorithm>

#include "../lpi/lpi_color.h"
#include "../lpi/lpi_math.h"


namespace {

template<typename T>
static inline void m9swap(T* a, T* b) {
  T temp = std::min(*a, *b);
  *b = std::max(*a, *b); // faster than with if
  *a = temp;
}

template<typename T>
static inline void m9min(T* a, T* b) {
  *a = std::min(*a, *b);
}

template<typename T>
static inline void m9max(T* a, T* b) {
  *b = std::max(*a, *b);
}

// sorting network for median of 9 values
template<typename T>
static inline void med9(T* values) {
  m9swap(values + 0, values + 1);
  m9swap(values + 3, values + 4);
  m9swap(values + 6, values + 7);
  m9swap(values + 1, values + 2);
  m9swap(values + 4, values + 5);
  m9swap(values + 7, values + 8);
  m9swap(values + 0, values + 1);
  m9swap(values + 3, values + 4);
  m9swap(values + 6, values + 7);
  m9max(values + 0, values + 3);
  m9max(values + 3, values + 6);
  m9swap(values + 1, values + 4);
  m9min(values + 4, values + 7);
  m9max(values + 1, values + 4);
  m9min(values + 5, values + 8);
  m9min(values + 2, values + 5);
  m9swap(values + 2, values + 4);
  m9min(values + 4, values + 6);
  m9max(values + 2, values + 4);
}

} // namespace

template<typename T>
static T fastroundcast(float v) {
  return v;
}

template<>
int fastroundcast(float v) {
  return v + 0.5; // using + 0.5 because std::round is visibly slower
}

template<>
unsigned char fastroundcast(float v) {
  return v + 0.5; // using + 0.5 because std::round is visibly slower
}

// TODO: support wrapping (wrap-around) option
// numc = num channels to affect, numc2 = num channels in image
template<typename T>
void boxBlur(T* image, size_t w, size_t h, size_t w2, size_t r, int numc, int numc2) {
  double iarr = 1.0 / (r + r + 1.0);

  std::vector<T> temp(w2 * h * numc2, 0);

  for(size_t y = 0; y < h; y++) {
    double val[numc];
    for(int c = 0; c < numc; c++) {
      val[c] = (r + 1) * image[y * w2 * numc2 + c];
    }
    for(size_t x = 0; x < r; x++) {
      for(int c = 0; c < numc; c++) {
        val[c] += image[(x + y * w2) * numc2 + c];
      }
    }
    for(size_t x = 0; x < w; x++) {
      int xp = std::min<int>(w - 1, x + r);
      int xm = std::max<int>(0, x - r - 1);
      for(int c = 0; c < numc; c++) {
        val[c] += image[(xp + y * w2) * numc2 + c] - (double)image[(xm + y * w2) * numc2 + c];
        temp[(x + y * w2) * numc2 + c] = fastroundcast<T>(val[c] * iarr);
      }
    }
  }

  // faster with remembering whole row thanks to using cache line order
  std::vector<double> val(w * numc2);
  for(size_t x = 0; x < w; x++) {
    for(int c = 0; c < numc; c++) {
      val[x * numc2 + c] = (r + 1) * temp[x * numc2 + c];
    }
  }
  for(size_t y = 0; y < r; y++) {
    for(size_t x = 0; x < w; x++) {
      for(int c = 0; c < numc; c++) {
        val[x * numc2 + c] += temp[(x + y * w2) * numc2 + c];
      }
    }
  }
  for(size_t y = 0; y < h; y++) {
    int yp = std::min<int>(h - 1, y + r);
    int ym = std::max<int>(0, y - r - 1);
    for(size_t x = 0; x < w; x++) {
      for(int c = 0; c < numc; c++) {
        val[x * numc2 + c] += temp[(x + yp * w2) * numc2 + c] - (double)temp[(x + ym * w2) * numc2 + c];
        image[(x + y * w2) * numc2 + c] = fastroundcast<T>(val[x * numc2 + c] * iarr);
      }
    }
  }
}

//1D kernel (apply in both directions to get 2D gaussian blur)
template<typename T>
void makeGaussianBlurKernel(std::vector<T>& kernel, double radius)
{
  int r = (int)std::ceil(radius);
  int rows = r * 2 + 1;
  kernel.resize(rows);
  T sigma = radius / 3;
  T sigma22 = 2 * sigma * sigma;
  T sigmaPi2 = 2 * 3.14159 * sigma;
  T total = 0;
  int index = 0;
  for (int row = -r; row <= r; row++)
  {
    T distance = row*row;
    if (distance > radius*radius)
      kernel[index] = 0;
    else
      kernel[index] = std::exp(-(distance) / sigma22) / std::sqrt(sigmaPi2);
    total += kernel[index];
    index++;
  }
  for (int i = 0; i < rows; i++)
    kernel[i] /= total;
}

void gaussianBlurRGBA8Slow(unsigned char* buffer, size_t u, size_t v, size_t u2, double radius, bool wrapping, bool affect_alpha, Progress& progress)
{
  std::vector<double> kernel;
  makeGaussianBlurKernel(kernel, radius);
  int s = kernel.size() / 2;
  double* k = &kernel[0];

  std::vector<unsigned char> temp(u * v * 4);

  //horizontal pass
  for(size_t y = 0; y < v; y++)
  {
    if(progress.handle(y, v, 0, 2)) return;
    for(size_t x = 0; x < u; x++)
    for(size_t c = 0; c < 4; c++)
    {
      if(c == 3 && !affect_alpha) continue;
      double value = 0.0;
      for(int x2 = -s; x2 < s + 1; x2++)
      {
        int x3 = x + x2;
        if(x3 < 0) x3 = wrapping ? u + x3 : 0;
        else if(x3 >= (int)u) x3 = wrapping ? x3 % u : u - 1;
        //for in case the image is so small the coordinate still doesn't fit...:
        if(x3 < 0) x3 = 0;
        if(x3 >= (int)u) x3 = wrapping ? 0 : u - 1;

        size_t index = y * u2 * 4 + x3 * 4 + c;
        value += k[x2 + s] * (double)buffer[index];
      }
      size_t index = y * u * 4 + x * 4 + c;
      if(value < 0) value = 0;
      if(value > 255) value = 255;
      temp[index] = (unsigned char)(value + 0.5); //small correction factor to keep fully 255 image at fully 255
    }
  }

  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  for(size_t c = 0; c < 4; c++)
  {
    if(c == 3 && !affect_alpha) continue;
    size_t index = y * u * 4 + x * 4 + c;
    size_t index2 = y * u2 * 4 + x * 4 + c;
    buffer[index2] = temp[index];
  }

  //vertical pass
  for(size_t y = 0; y < v; y++)
  {
    if(progress.handle(y, v, 1, 2)) return;
    for(size_t x = 0; x < u; x++)
    for(size_t c = 0; c < 4; c++)
    {
      if(c == 3 && !affect_alpha) continue;
      double value = 0.0;
      for(int y2 = -s; y2 < s + 1; y2++)
      {
        int y3 = y + y2;
        if(y3 < 0) y3 = wrapping ? v + y3 : 0;
        else if(y3 >= (int)v) y3 = wrapping ? y3 % v : v - 1;
        //for in case the image is so small the coordinate still doesn't fit...:
        if(y3 < 0) y3 = 0;
        if(y3 >= (int)v) y3 = wrapping ? 0 : v - 1;

        size_t index = y3 * u2 * 4 + x * 4 + c;
        value += k[y2 + s] * (double)buffer[index];
      }
      size_t index = y * u * 4 + x * 4 + c;
      if(value < 0) value = 0;
      if(value > 255) value = 255;
      temp[index] = (unsigned char)(value + 0.5); //small correction factor to keep fully 255 image at fully 255
    }
  }

  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  for(size_t c = 0; c < 4; c++)
  {
    if(c == 3 && !affect_alpha) continue;
    size_t index = y * u * 4 + x * 4 + c;
    size_t index2 = y * u2 * 4 + x * 4 + c;
    buffer[index2] = temp[index];
  }
}

void gaussianBlurRGBA8(unsigned char* buffer, size_t w, size_t h, size_t w2, double radius, bool wrapping, bool affect_alpha, Progress& progress)
{
  if(wrapping || radius < 3) {
    // TODO: support wrapping in boxBlur instead to make this case much faster
    gaussianBlurRGBA8Slow(buffer, w, h, w2, radius, wrapping, affect_alpha, progress);
    return;
  }

  radius /= 3;
  // three such boxblurs approximate gauss blur very well
  boxBlur(buffer, w, h, w2, radius, 4, affect_alpha ? 4 : 3);
  progress.handle(1, 3);
  boxBlur(buffer, w, h, w2, radius, 4, affect_alpha ? 4 : 3);
  progress.handle(2, 3);
  boxBlur(buffer, w, h, w2, radius, 4, affect_alpha ? 4 : 3);
}

void gaussianBlurRGBA32FSlow(float* buffer, size_t u, size_t v, size_t u2, double radius, bool wrapping, bool affect_alpha, Progress& progress)
{
  std::vector<float> kernel;
  makeGaussianBlurKernel(kernel, radius);
  int s = kernel.size() / 2;
  float* k = &kernel[0];

  std::vector<float> temp(u * v * 4);


  //horizontal pass
  for(size_t y = 0; y < v; y++)
  {
    if(progress.handle(y, v, 0, 2)) return;
    for(size_t x = 0; x < u; x++)
    for(size_t c = 0; c < 4; c++)
    {
      if(c == 3 && !affect_alpha) continue;
      float value = 0.0;
      for(int x2 = -s; x2 < s + 1; x2++)
      {
        int x3 = x + x2;
        if(x3 < 0) x3 = wrapping ? u + x3 : 0;
        else if(x3 >= (int)u) x3 = wrapping ? x3 % u : u - 1;
        //for in case the image is so small the coordinate still doesn't fit...:
        if(x3 < 0) x3 = 0;
        if(x3 >= (int)u) x3 = wrapping ? 0 : u - 1;

        size_t index = y * u2 * 4 + x3 * 4 + c;
        value += k[x2 + s] * buffer[index];
      }
      size_t index = y * u * 4 + x * 4 + c;
      temp[index] = value;
    }
  }

  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  for(size_t c = 0; c < 4; c++)
  {
    if(c == 3 && !affect_alpha) continue;
    size_t index = y * u * 4 + x * 4 + c;
    size_t index2 = y * u2 * 4 + x * 4 + c;
    buffer[index2] = temp[index];
  }

  //vertical pass
  for(size_t y = 0; y < v; y++)
  {
    if(progress.handle(y, v, 1, 2)) return;
    for(size_t x = 0; x < u; x++)
    for(size_t c = 0; c < 4; c++)
    {
      if(c == 3 && !affect_alpha) continue;
      float value = 0.0;
      for(int y2 = -s; y2 < s + 1; y2++)
      {
        int y3 = y + y2;
        if(y3 < 0) y3 = wrapping ? v + y3 : 0;
        else if(y3 >= (int)v) y3 = wrapping ? y3 % v : v - 1;
        //for in case the image is so small the coordinate still doesn't fit...:
        if(y3 < 0) y3 = 0;
        if(y3 >= (int)v) y3 = wrapping ? 0 : v - 1;

        size_t index = y3 * u2 * 4 + x * 4 + c;
        value += k[y2 + s] * buffer[index];
      }
      size_t index = y * u * 4 + x * 4 + c;
      temp[index] = value;
    }
  }

  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  for(size_t c = 0; c < 4; c++)
  {
    if(c == 3 && !affect_alpha) continue;
    size_t index = y * u * 4 + x * 4 + c;
    size_t index2 = y * u2 * 4 + x * 4 + c;
    buffer[index2] = temp[index];
  }
}

void gaussianBlurRGBA32F(float* buffer, size_t w, size_t h, size_t w2, double radius, bool wrapping, bool affect_alpha, Progress& progress)
{
  if(wrapping || radius < 3) {
    // TODO: support wrapping in boxBlur instead to make this case much faster
    gaussianBlurRGBA32FSlow(buffer, w, h, w2, radius, wrapping, affect_alpha, progress);
    return;
  }

  radius /= 3;
  // three such boxblurs approximate gauss blur very well
  boxBlur(buffer, w, h, w2, radius, 4, affect_alpha ? 4 : 3);
  progress.handle(1, 3);
  boxBlur(buffer, w, h, w2, radius, 4, affect_alpha ? 4 : 3);
  progress.handle(2, 3);
  boxBlur(buffer, w, h, w2, radius, 4, affect_alpha ? 4 : 3);
}

void gaussianBlurGrey8Slow(unsigned char* buffer, size_t u, size_t v, size_t u2, double radius, bool wrapping, Progress& progress)
{
  std::vector<double> kernel;
  makeGaussianBlurKernel(kernel, radius);
  int s = kernel.size() / 2;
  double* k = &kernel[0];

  std::vector<unsigned char> temp(u * v);

  //horizontal pass
  for(size_t y = 0; y < v; y++)
  {
    if(progress.handle(y, v, 0, 2)) return;
    for(size_t x = 0; x < u; x++)
    {
      double value = 0.0;
      for(int x2 = -s; x2 < s + 1; x2++)
      {
        int x3 = x + x2;
        if(x3 < 0) x3 = wrapping ? u + x3 : 0;
        else if(x3 >= (int)u) x3 = wrapping ? x3 % u : u - 1;
        //for in case the image is so small the coordinate still doesn't fit...:
        if(x3 < 0) x3 = 0;
        if(x3 >= (int)u) x3 = wrapping ? 0 : u - 1;

        size_t index = y * u2 + x3;
        value += k[x2 + s] * (double)buffer[index];
      }
      size_t index = y * u + x;
      if(value < 0) value = 0;
      if(value > 255) value = 255;
      temp[index] = (unsigned char)(value + 0.5); //small correction factor to keep fully 255 image at fully 255
    }
  }

  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  {
    size_t index = y * u + x;
    size_t index2 = y * u2 + x;
    buffer[index2] = temp[index];
  }

  //vertical pass
  for(size_t y = 0; y < v; y++)
  {
    if(progress.handle(y, v, 1, 2)) return;
    for(size_t x = 0; x < u; x++)
    {
      double value = 0.0;
      for(int y2 = -s; y2 < s + 1; y2++)
      {
        int y3 = y + y2;
        if(y3 < 0) y3 = wrapping ? v + y3 : 0;
        else if(y3 >= (int)v) y3 = wrapping ? y3 % v : v - 1;
        //for in case the image is so small the coordinate still doesn't fit...:
        if(y3 < 0) y3 = 0;
        if(y3 >= (int)v) y3 = wrapping ? 0 : v - 1;

        size_t index = y3 * u2 + x;
        value += k[y2 + s] * (double)buffer[index];
      }
      size_t index = y * u + x;
      if(value < 0) value = 0;
      if(value > 255) value = 255;
      temp[index] = (unsigned char)(value + 0.5); //small correction factor to keep fully 255 image at fully 255
    }
  }

  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  {
    size_t index = y * u + x;
    size_t index2 = y * u2 + x;
    buffer[index2] = temp[index];
  }
}

void gaussianBlurGrey8(unsigned char* buffer, size_t w, size_t h, size_t w2, double radius, bool wrapping, Progress& progress)
{
  if(wrapping || radius < 3) {
    // TODO: support wrapping in boxBlur instead to make this case much faster
    gaussianBlurGrey8Slow(buffer, w, h, w2, radius, wrapping, progress);
    return;
  }

  radius /= 3;
  // three such boxblurs approximate gauss blur very well
  boxBlur(buffer, w, h, w2, radius, 1, 1);
  progress.handle(1, 3);
  boxBlur(buffer, w, h, w2, radius, 1, 1);
  progress.handle(2, 3);
  boxBlur(buffer, w, h, w2, radius, 1, 1);
}

void gaussianBlurGrey32FSlow(float* buffer, size_t u, size_t v, size_t u2, double radius, bool wrapping, Progress& progress)
{
  std::vector<float> kernel;
  makeGaussianBlurKernel(kernel, radius);
  int s = kernel.size() / 2;
  float* k = &kernel[0];

  std::vector<float> temp(u * v);


  //horizontal pass
  for(size_t y = 0; y < v; y++)
  {
    if(progress.handle(y, v, 0, 2)) return;
    for(size_t x = 0; x < u; x++)
    {
      float value = 0.0;
      for(int x2 = -s; x2 < s + 1; x2++)
      {
        int x3 = x + x2;
        if(x3 < 0) x3 = wrapping ? u + x3 : 0;
        else if(x3 >= (int)u) x3 = wrapping ? x3 % u : u - 1;
        //for in case the image is so small the coordinate still doesn't fit...:
        if(x3 < 0) x3 = 0;
        if(x3 >= (int)u) x3 = wrapping ? 0 : u - 1;

        size_t index = y * u2 + x3;
        value += k[x2 + s] * buffer[index];
      }
      size_t index = y * u + x;
      temp[index] = value;
    }
  }

  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  {
    size_t index = y * u + x;
    size_t index2 = y * u2 + x;
    buffer[index2] = temp[index];
  }

  //vertical pass
  for(size_t y = 0; y < v; y++)
  {
    if(progress.handle(y, v, 1, 2)) return;
    for(size_t x = 0; x < u; x++)
    {
      float value = 0.0;
      for(int y2 = -s; y2 < s + 1; y2++)
      {
        int y3 = y + y2;
        if(y3 < 0) y3 = wrapping ? v + y3 : 0;
        else if(y3 >= (int)v) y3 = wrapping ? y3 % v : v - 1;
        //for in case the image is so small the coordinate still doesn't fit...:
        if(y3 < 0) y3 = 0;
        if(y3 >= (int)v) y3 = wrapping ? 0 : v - 1;

        size_t index = y3 * u2 + x;
        value += k[y2 + s] * buffer[index];
      }
      size_t index = y * u + x;
      temp[index] = value;
    }
  }

  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  {
    size_t index = y * u + x;
    size_t index2 = y * u2 + x;
    buffer[index2] = temp[index];
  }
}

void gaussianBlurGrey32F(float* buffer, size_t w, size_t h, size_t w2, double radius, bool wrapping, Progress& progress)
{
  if(wrapping || radius < 3) {
    // TODO: support wrapping in boxBlur instead to make this case much faster
    gaussianBlurGrey32FSlow(buffer, w, h, w2, radius, wrapping, progress);
    return;
  }

  radius /= 3;
  // three such boxblurs approximate gauss blur very well
  boxBlur(buffer, w, h, w2, radius, 1, 1);
  progress.handle(1, 3);
  boxBlur(buffer, w, h, w2, radius, 1, 1);
  progress.handle(2, 3);
  boxBlur(buffer, w, h, w2, radius, 1, 1);
}


////////////////////////////////////////////////////////////////////////////////

void gaussianBlurSelectiveRGBA8(unsigned char* buffer, size_t u, size_t v, size_t u2, double radius, double threshold, bool wrapping, bool affect_alpha, Progress& progress)
{
  std::vector<double> kernel;
  makeGaussianBlurKernel(kernel, radius);
  int s = kernel.size() / 2;
  double* k = &kernel[0];

  std::vector<unsigned char> temp(u * v * 4);

  //horizontal pass
  for(size_t y = 0; y < v; y++)
  {
    if(progress.handle(y, v, 0, 2)) return;
    for(size_t x = 0; x < u; x++)
    for(size_t c = 0; c < 4; c++)
    {
      if(c == 3 && !affect_alpha) continue;
      size_t index = y * u * 4 + x * 4 + c;
      size_t index2 = y * u2 * 4 + x * 4 + c;
      double val = buffer[index2];
      double value = 0.0;
      double sum = 0.0;
      for(int x2 = -s; x2 < s + 1; x2++)
      {
        int x3 = x + x2;
        if(x3 < 0) x3 = wrapping ? u + x3 : 0;
        else if(x3 >= (int)u) x3 = wrapping ? x3 % u : u - 1;
        //for in case the image is so small the coordinate still doesn't fit...:
        if(x3 < 0) x3 = 0;
        if(x3 >= (int)u) x3 = wrapping ? 0 : u - 1;

        size_t index3 = y * u2 * 4 + x3 * 4 + c;
        if(std::abs(buffer[index3] - val) < threshold)
        {
          value += k[x2 + s] * (double)buffer[index3];
          sum += k[x2 + s];
        }
      }
      value /= sum;
      if(value < 0) value = 0;
      if(value > 255) value = 255;
      temp[index] = (unsigned char)(value + 0.5); //small correction factor to keep fully 255 image at fully 255
    }
  }

  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  for(size_t c = 0; c < 4; c++)
  {
    if(c == 3 && !affect_alpha) continue;
    size_t index = y * u * 4 + x * 4 + c;
    size_t index2 = y * u2 * 4 + x * 4 + c;
    buffer[index2] = temp[index];
  }

  //vertical pass
  for(size_t y = 0; y < v; y++)
  {
    if(progress.handle(y, v, 1, 2)) return;
    for(size_t x = 0; x < u; x++)
    for(size_t c = 0; c < 4; c++)
    {
      if(c == 3 && !affect_alpha) continue;
      size_t index = y * u * 4 + x * 4 + c;
      size_t index2 = y * u2 * 4 + x * 4 + c;
      double val = buffer[index2];
      double value = 0.0;
      double sum = 0.0;
      for(int y2 = -s; y2 < s + 1; y2++)
      {
        int y3 = y + y2;
        if(y3 < 0) y3 = wrapping ? v + y3 : 0;
        else if(y3 >= (int)v) y3 = wrapping ? y3 % v : v - 1;
        //for in case the image is so small the coordinate still doesn't fit...:
        if(y3 < 0) y3 = 0;
        if(y3 >= (int)v) y3 = wrapping ? 0 : v - 1;

        size_t index3 = y3 * u2 * 4 + x * 4 + c;
        if(std::abs(buffer[index3] - val) < threshold)
        {
          value += k[y2 + s] * (double)buffer[index3];
          sum += k[y2 + s];
        }
      }
      value /= sum;
      if(value < 0) value = 0;
      if(value > 255) value = 255;
      temp[index] = (unsigned char)(value + 0.5); //small correction factor to keep fully 255 image at fully 255
    }
  }

  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  for(size_t c = 0; c < 4; c++)
  {
    if(c == 3 && !affect_alpha) continue;
    size_t index = y * u * 4 + x * 4 + c;
    size_t index2 = y * u2 * 4 + x * 4 + c;
    buffer[index2] = temp[index];
  }
}

void gaussianBlurSelectiveRGBA32F(float* buffer, size_t u, size_t v, size_t u2, double radius, double threshold, bool wrapping, bool affect_alpha, Progress& progress)
{
  std::vector<float> kernel;
  makeGaussianBlurKernel(kernel, radius);
  int s = kernel.size() / 2;
  float* k = &kernel[0];

  std::vector<float> temp(u * v * 4);


  //horizontal pass
  for(size_t y = 0; y < v; y++)
  {
    if(progress.handle(y, v, 0, 2)) return;
    for(size_t x = 0; x < u; x++)
    for(size_t c = 0; c < 4; c++)
    {
      if(c == 3 && !affect_alpha) continue;
      size_t index = y * u * 4 + x * 4 + c;
      size_t index2 = y * u2 * 4 + x * 4 + c;
      float val = buffer[index2];
      float value = 0.0;
      float sum = 0.0;
      for(int x2 = -s; x2 < s + 1; x2++)
      {
        int x3 = x + x2;
        if(x3 < 0) x3 = wrapping ? u + x3 : 0;
        else if(x3 >= (int)u) x3 = wrapping ? x3 % u : u - 1;
        //for in case the image is so small the coordinate still doesn't fit...:
        if(x3 < 0) x3 = 0;
        if(x3 >= (int)u) x3 = wrapping ? 0 : u - 1;

        size_t index3 = y * u2 * 4 + x3 * 4 + c;
        if(std::abs(buffer[index3] - val) < threshold / 255.0)
        {
          value += k[x2 + s] * buffer[index3];
          sum += k[x2 + s];
        }
      }
      value /= sum;
      temp[index] = value;
    }
  }

  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  for(size_t c = 0; c < 4; c++)
  {
    if(c == 3 && !affect_alpha) continue;
    size_t index = y * u * 4 + x * 4 + c;
    size_t index2 = y * u2 * 4 + x * 4 + c;
    buffer[index2] = temp[index];
  }

  //vertical pass
  for(size_t y = 0; y < v; y++)
  {
    if(progress.handle(y, v, 1, 2)) return;
    for(size_t x = 0; x < u; x++)
    for(size_t c = 0; c < 4; c++)
    {
      if(c == 3 && !affect_alpha) continue;
      size_t index = y * u * 4 + x * 4 + c;
      size_t index2 = y * u2 * 4 + x * 4 + c;
      float val = buffer[index2];
      float value = 0.0;
      float sum = 0.0;
      for(int y2 = -s; y2 < s + 1; y2++)
      {
        int y3 = y + y2;
        if(y3 < 0) y3 = wrapping ? v + y3 : 0;
        else if(y3 >= (int)v) y3 = wrapping ? y3 % v : v - 1;
        //for in case the image is so small the coordinate still doesn't fit...:
        if(y3 < 0) y3 = 0;
        if(y3 >= (int)v) y3 = wrapping ? 0 : v - 1;

        size_t index3 = y3 * u2 * 4 + x * 4 + c;
        if(std::abs(buffer[index3] - val) < threshold / 255.0)
        {
          value += k[y2 + s] * buffer[index3];
          sum += k[y2 + s];
        }
      }
      value /= sum;
      temp[index] = value;
    }
  }

  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  for(size_t c = 0; c < 4; c++)
  {
    if(c == 3 && !affect_alpha) continue;
    size_t index = y * u * 4 + x * 4 + c;
    size_t index2 = y * u2 * 4 + x * 4 + c;
    buffer[index2] = temp[index];
  }
}


void gaussianBlurSelectiveGrey8(unsigned char* buffer, size_t u, size_t v, size_t u2, double radius, double threshold, bool wrapping, Progress& progress)
{
  std::vector<double> kernel;
  makeGaussianBlurKernel(kernel, radius);
  int s = kernel.size() / 2;
  double* k = &kernel[0];

  std::vector<unsigned char> temp(u * v);

  //horizontal pass
  for(size_t y = 0; y < v; y++)
  {
  if(progress.handle(y, v, 0, 2)) return;
    for(size_t x = 0; x < u; x++)
    {
      size_t index = y * u + x;
      size_t index2 = y * u2 + x;
      double val = buffer[index2];
      double value = 0.0;
      double sum = 0.0;
      for(int x2 = -s; x2 < s + 1; x2++)
      {
        int x3 = x + x2;
        if(x3 < 0) x3 = wrapping ? u + x3 : 0;
        else if(x3 >= (int)u) x3 = wrapping ? x3 % u : u - 1;
        //for in case the image is so small the coordinate still doesn't fit...:
        if(x3 < 0) x3 = 0;
        if(x3 >= (int)u) x3 = wrapping ? 0 : u - 1;

        size_t index3 = y * u2 + x3;
        if(std::abs(buffer[index3] - val) < threshold)
        {
          value += k[x2 + s] * (double)buffer[index3];
          sum += k[x2 + s];
        }
      }
      value /= sum;
      if(value < 0) value = 0;
      if(value > 255) value = 255;
      temp[index] = (unsigned char)(value + 0.5); //small correction factor to keep fully 255 image at fully 255
    }
  }

  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  {
    size_t index = y * u + x;
    size_t index2 = y * u2 + x;
    buffer[index2] = temp[index];
  }

  //vertical pass
  for(size_t y = 0; y < v; y++)
  {
    if(progress.handle(y, v, 1, 2)) return;
    for(size_t x = 0; x < u; x++)
    {
      size_t index = y * u + x;
      size_t index2 = y * u2 + x;
      double val = buffer[index2];
      double value = 0.0;
      double sum = 0.0;
      for(int y2 = -s; y2 < s + 1; y2++)
      {
        int y3 = y + y2;
        if(y3 < 0) y3 = wrapping ? v + y3 : 0;
        else if(y3 >= (int)v) y3 = wrapping ? y3 % v : v - 1;
        //for in case the image is so small the coordinate still doesn't fit...:
        if(y3 < 0) y3 = 0;
        if(y3 >= (int)v) y3 = wrapping ? 0 : v - 1;

        size_t index3 = y3 * u2 + x;
        if(std::abs(buffer[index3] - val) < threshold)
        {
          value += k[y2 + s] * (double)buffer[index3];
          sum += k[y2 + s];
        }
      }
      value /= sum;
      if(value < 0) value = 0;
      if(value > 255) value = 255;
      temp[index] = (unsigned char)(value + 0.5); //small correction factor to keep fully 255 image at fully 255
    }
  }

  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  {
    size_t index = y * u + x;
    size_t index2 = y * u2 + x;
    buffer[index2] = temp[index];
  }
}

void gaussianBlurSelectiveGrey32F(float* buffer, size_t u, size_t v, size_t u2, double radius, double threshold, bool wrapping, Progress& progress)
{
  std::vector<float> kernel;
  makeGaussianBlurKernel(kernel, radius);
  int s = kernel.size() / 2;
  float* k = &kernel[0];

  std::vector<float> temp(u * v);


  //horizontal pass
  for(size_t y = 0; y < v; y++)
  {
    if(progress.handle(y, v, 0, 2)) return;
    for(size_t x = 0; x < u; x++)
    {
      size_t index = y * u + x;
      size_t index2 = y * u2 + x;
      float val = buffer[index2];
      float value = 0.0;
      float sum = 0.0;
      for(int x2 = -s; x2 < s + 1; x2++)
      {
        int x3 = x + x2;
        if(x3 < 0) x3 = wrapping ? u + x3 : 0;
        else if(x3 >= (int)u) x3 = wrapping ? x3 % u : u - 1;
        //for in case the image is so small the coordinate still doesn't fit...:
        if(x3 < 0) x3 = 0;
        if(x3 >= (int)u) x3 = wrapping ? 0 : u - 1;

        size_t index3 = y * u2 + x3;
        if(std::abs(buffer[index3] - val) < threshold / 255.0)
        {
          value += k[x2 + s] * buffer[index3];
          sum += k[x2 + s];
        }
      }
      value /= sum;
      temp[index] = value;
    }
  }

  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  {
    size_t index = y * u + x;
    size_t index2 = y * u2 + x;
    buffer[index2] = temp[index];
  }

  //vertical pass
  for(size_t y = 0; y < v; y++)
  {
    if(progress.handle(y, v, 1, 2)) return;
    for(size_t x = 0; x < u; x++)
    {
      size_t index = y * u + x;
      size_t index2 = y * u2 + x;
      float val = buffer[index2];
      float value = 0.0;
      float sum = 0.0;
      for(int y2 = -s; y2 < s + 1; y2++)
      {
        int y3 = y + y2;
        if(y3 < 0) y3 = wrapping ? v + y3 : 0;
        else if(y3 >= (int)v) y3 = wrapping ? y3 % v : v - 1;
        //for in case the image is so small the coordinate still doesn't fit...:
        if(y3 < 0) y3 = 0;
        if(y3 >= (int)v) y3 = wrapping ? 0 : v - 1;

        size_t index3 = y3 * u2 + x;
        if(std::abs(buffer[index3] - val) < threshold / 255.0)
        {
          value += k[y2 + s] * buffer[index3];
          sum += k[y2 + s];
        }
      }
      value /= sum;
      temp[index] = value;
    }
  }

  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  {
    size_t index = y * u + x;
    size_t index2 = y * u2 + x;
    buffer[index2] = temp[index];
  }
}

////////////////////////////////////////////////////////////////////////////////

template<typename T>
static T median(T* values, int num)
{
  //NOT taking average of two values if length is even: always use original image pixel channel value
  std::nth_element(values, values + num / 2, values + num);
  return values[num / 2];
}

template<typename T>
static T getmax(T* values, int num)
{
  T result = std::numeric_limits<T>().min();
  for(int i = 0; i < num; i++) result = std::max(result, values[i]);
  return result;
}

template<typename T>
static T getmin(T* values, int num)
{
  T result = std::numeric_limits<T>().max();
  for(int i = 0; i < num; i++) result = std::min(result, values[i]);
  return result;
}

template<typename T, typename U>
static void getPixelRect(T* values, size_t sizex, size_t sizey, size_t x, size_t y, const U* buffer, size_t u, size_t v, size_t u2, int step, bool wrapping)
{
  int n = 0;
  for(size_t filterY = 0; filterY < sizex; filterY++)
  for(size_t filterX = 0; filterX < sizey; filterX++)
  {
    int x2 = x + filterX - sizex / 2;
    int y2 = y + filterY - sizey / 2;
    if(wrapping)
    {
      x2 = lpi::wrap(x2, 0, (int)u);
      y2 = lpi::wrap(y2, 0, (int)v);
    }
    else
    {
      x2 = lpi::clamp(x2, 0, (int)u - 1);
      y2 = lpi::clamp(y2, 0, (int)v - 1);
    }
    size_t index2 = y2 * step * u2 + x2 * step;

    values[n] = buffer[index2];
    n++;
  }
}

template<typename T, typename U>
static void getPixelIntensityRect(T* values, size_t sizex, size_t sizey, size_t x, size_t y, const U* buffer, size_t u, size_t v, size_t u2, bool wrapping)
{
  std::vector<T> p(sizex * sizey * 4);
  getPixelRect(&p[0], sizex, sizey, x, y, buffer, u, v, u2, 1, wrapping);
  // The 1 * is to ensure it's not adding unsigned chars with overflow
  for(size_t i = 0; i < sizex * sizey; i++) values[i] = (1 * p[i] + p[i + 1] + p[i + 2]) / 3;
}

// faster than generic case
void median9FilterRGBA8(unsigned char* buffer, size_t u, size_t v, size_t u2, bool wrapping, bool affect_alpha, Progress& progress)
{
  std::vector<unsigned char> old;
  getAlignedBufferRGBA8(old, buffer, u, v, u2);

  std::vector<int> valuesv(9);
  int* values = &valuesv[0];

  //apply the filter
  for(size_t y = 0; y < v; y++)
  {
    if(progress.handle(y, v)) return;
    for(size_t x = 0; x < u; x++)
    for(size_t c = 0; c < 4; c++)
    {
      if(!affect_alpha && c == 3) continue;
      size_t index = y * 4 * u2 + x * 4 + c;
      getPixelRect(values, 3, 3, x, y, &old[0] + c, u, v, u /*NOT u2, using 'old'!*/, 4, wrapping);
      med9(values);
      buffer[index] = values[4];
    }
  }
}

void medianFilterRGBA8(unsigned char* buffer, size_t u, size_t v, size_t u2, int diameterx, int diametery, bool wrapping, bool affect_alpha, Progress& progress)
{
  if (diameterx == 3 && diametery == 3) {
    median9FilterRGBA8(buffer, u, v, u2, wrapping, affect_alpha, progress);
    return;
  }
  std::vector<unsigned char> old;
  getAlignedBufferRGBA8(old, buffer, u, v, u2);

  int dd = diameterx * diametery;

  std::vector<int> valuesv(dd);
  int* values = &valuesv[0];

  //apply the filter
  for(size_t y = 0; y < v; y++)
  {
    if(progress.handle(y, v)) return;
    for(size_t x = 0; x < u; x++)
    for(size_t c = 0; c < 4; c++)
    {
      if(!affect_alpha && c == 3) continue;

      size_t index = y * 4 * u2 + x * 4 + c;

      getPixelRect(values, diameterx, diametery, x, y, &old[0] + c, u, v, u /*NOT u2, using 'old'!*/, 4, wrapping);

      buffer[index] = median(&values[0], dd);
    }
  }
}

// faster than generic case
void median9FilterRGBA32F(float* buffer, size_t u, size_t v, size_t u2, bool wrapping, bool affect_alpha, Progress& progress)
{
  std::vector<float> old;
  getAlignedBufferRGBA32F(old, buffer, u, v, u2);

  std::vector<int> valuesv(9);
  int* values = &valuesv[0];

  //apply the filter
  for(size_t y = 0; y < v; y++)
  {
    if(progress.handle(y, v)) return;
    for(size_t x = 0; x < u; x++)
    for(size_t c = 0; c < 4; c++)
    {
      if(!affect_alpha && c == 3) continue;
      size_t index = y * 4 * u2 + x * 4 + c;
      getPixelRect(values, 3, 3, x, y, &old[0] + c, u, v, u /*NOT u2, using 'old'!*/, 4, wrapping);
      med9(values);
      buffer[index] = values[4];
    }
  }
}

void medianFilterRGBA32F(float* buffer, size_t u, size_t v, size_t u2, int diameterx, int diametery, bool wrapping, bool affect_alpha, Progress& progress)
{
  if (diameterx == 3 && diametery == 3) {
    median9FilterRGBA32F(buffer, u, v, u2, wrapping, affect_alpha, progress);
    return;
  }
  std::vector<float> old;
  getAlignedBufferRGBA32F(old, buffer, u, v, u2);

  int dd = diameterx * diametery;

  std::vector<float> valuesv(dd);
  float* values = &valuesv[0];

  //apply the filter
  for(size_t y = 0; y < v; y++)
  {
    if(progress.handle(y, v)) return;
    for(size_t x = 0; x < u; x++)
    for(size_t c = 0; c < 4; c++)
    {
      if(!affect_alpha && c == 3) continue;

      size_t index = y * 4 * u2 + x * 4 + c;
      getPixelRect(values, diameterx, diametery, x, y, &old[0] + c, u, v, u /*NOT u2, using 'old'!*/, 4, wrapping);
      buffer[index] = median(&values[0], dd);
    }
  }
}

////////////////////////////////////////////////////////////////////////////////

void adaptiveMedianFilterRGBA8(unsigned char* buffer, size_t u, size_t v, size_t u2, int maxdiameter, bool wrapping, bool affect_alpha, Progress& progress)
{
  std::vector<unsigned char> old;
  getAlignedBufferRGBA8(old, buffer, u, v, u2);

  std::vector<int> valuesv(maxdiameter * maxdiameter);
  int* values = &valuesv[0];

  for(size_t y = 0; y < v; y++)
  {
    if(progress.handle(y, v)) return;
    for(size_t x = 0; x < u; x++)
    {
      size_t index = y * 4 * u2 + x * 4;

      for(size_t c = 0; c < 4; c++)
      {
        if(c == 3 && !affect_alpha) continue;
        int diameter = 3; // Only use odd diameters! Combining even and odd diameters in one is really ugly.
        // Do the adaptive median algorithm on an intensity based version of the image, doing R,G,B independently looks ugly.
        for(;;)
        {
          int dd = diameter * diameter;
          getPixelRect(values, diameter, diameter, x, y, &old[0] + c, u, v, u /*NOT u2, using 'old'!*/, 4, wrapping);

          int zmin = getmin(&values[0], dd);
          int zmed = median(&values[0], dd);
          int zmax = getmax(&values[0], dd);
          int zxy = buffer[index + c];

          int a1 = zmed - zmin;
          int a2 = zmed - zmax;
          if(a1 > 0 && a2 < 0)
          {
            int b1 = zxy - zmin;
            int b2 = zxy - zmax;
            if(b1 > 0 && b2 < 0) buffer[index + c] = zxy;
            else buffer[index + c] = zmed;
            break;
          }
          else
          {
            if(diameter >= maxdiameter)
            {
              buffer[index + c] = zmed;
              break;
            }
            else diameter += 2;
          }
        }

      }
    }
  }
}

////////////////////////////////////////////////////////////////////////////////

/*
bilinear interpolation has some ugly "cross/diamond shaped" artefacts that the human eye is sensitive to.
This function converts the straight line from 0-1, into an S-curve from 0-1.
*/
static double scurve(double v)
{
  double w = 2 * (v - 0.5);
  if(v < 0.5) w = -w;
  w = 1.0 - w;
  w = w * w;
  w = 1.0 - w;
  if(v < 0.5) w = -w;
  return 0.5 + w / 2;

  //slower version below

  //double result = 1.0 / (1.0 + std::exp(-(v-0.5) * 2 * 6));
  //return (result + v * 3) / 4; //the S curve is too essy, so mix it a bit with the linear origin again
}

void interpolatingBlurRGBA8(unsigned char* buffer, size_t u, size_t v, size_t u2, int radius, bool affect_alpha, Progress& progress)
{
  if(radius < 1) return; //no integer division through zero

  for(size_t y = 0; y < v; y++)
  {
    for(size_t x = 0; x < u; x++)
    {
      size_t index = y * u2 * 4 + x * 4;

      size_t x0 = (x / radius) * radius;
      size_t x1 = (x / radius + 1) * radius;
      if(x1 >= u) x1 = u - 1;
      size_t y0 = (y / radius) * radius;
      size_t y1 = (y / radius + 1) * radius;
      if(y1 >= v) y1 = v - 1;

      if(x0 == x1 || y0 == y1) continue; //avoid wrongly colored pixels at bottom row and right column

      size_t index00 = y0 * u2 * 4 + x0 * 4;
      size_t index01 = y1 * u2 * 4 + x0 * 4;
      size_t index10 = y0 * u2 * 4 + x1 * 4;
      size_t index11 = y1 * u2 * 4 + x1 * 4;

      lpi::ColorRGBd c00(buffer[index00 + 0], buffer[index00 + 1], buffer[index00 + 2], buffer[index00 + 3]);
      lpi::ColorRGBd c01(buffer[index01 + 0], buffer[index01 + 1], buffer[index01 + 2], buffer[index01 + 3]);
      lpi::ColorRGBd c10(buffer[index10 + 0], buffer[index10 + 1], buffer[index10 + 2], buffer[index10 + 3]);
      lpi::ColorRGBd c11(buffer[index11 + 0], buffer[index11 + 1], buffer[index11 + 2], buffer[index11 + 3]);

      double vx = (double)(x - x0) / (x1 - x0);
      double vy = (double)(y - y0) / (y1 - y0);
      /*vx = 0.5 + (vx - 0.5) * (vx - 0.5) * 2 * (vx < 0.5 ? -1 : +1);
      vy = 0.5 + (vy - 0.5) * (vy - 0.5) * 2 * (vy < 0.5 ? -1 : +1);*/
      vx = scurve(vx);
      vy = scurve(vy);

      lpi::ColorRGBd c = (((c11 & vx) | (c01 & (1.0 - vx))) & vy)
                       | (((c10 & vx) | (c00 & (1.0 - vx))) & (1.0 - vy));

      buffer[index + 0] = (int)(c.r + 0.5);
      buffer[index + 1] = (int)(c.g + 0.5);
      buffer[index + 2] = (int)(c.b + 0.5);
      if(affect_alpha) buffer[index + 3] = (int)(c.a + 0.5);
    }
    if(progress.handle(y, v)) return;
  }
}


void softestBlurRGBA8(unsigned char* buffer, size_t u, size_t v, size_t u2, int amount, bool wrapping, bool affect_alpha, Progress& progress)
{
  std::vector<unsigned char> old;
  getAlignedBufferRGBA8(old, buffer, u, v, u2);

  std::vector<int> valuesv(9);
  int* values = &valuesv[0];

  //apply the filter
  for(size_t y = 0; y < v; y++)
  {
    if(progress.handle(y, v)) return;
    for(size_t x = 0; x < u; x++)
    for(size_t c = 0; c < 4; c++)
    {
      if(!affect_alpha && c == 3) continue;

      size_t index = y * 4 * u2 + x * 4 + c;

      getPixelRect(values, 3, 3, x, y, &old[0] + c, u, v, u /*NOT u2, using 'old'!*/, 4, wrapping);

      int value = (values[0] + values[1] + values[2] + values[3] + values[5] + values[6] + values[7] + values[8]) / 8;

      /*// The + 1 and - 1 make the effect stop after a while. But without this, after many repetitions there are not so good looking patterns.
      if(value > buffer[index] + 1) buffer[index]++;
      else if(value < buffer[index] - 1) buffer[index]--;*/

      if(value > buffer[index] + amount) buffer[index] += amount;
      else if(value < buffer[index] - amount) buffer[index] -= amount;
      // The + 1 and - 1 make the effect stop after a while. But without this, after many repetitions there are not so good looking patterns.
      if(value > buffer[index] + 1) buffer[index] = (buffer[index] + value) / 2;
      else if(value < buffer[index] - 1) buffer[index] = (buffer[index] + value) / 2;
    }
  }
}
