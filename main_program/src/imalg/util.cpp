/*
LodePaint

Copyright (c) 2009-2010 Lode Vandevenne
All rights reserved.

This file is part of LodePaint.

LodePaint is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LodePaint is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LodePaint.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "util.h"

#include <iostream>
#include <map>
#include <cassert>

void getAlignedBufferRGBA8(std::vector<unsigned char>& out, unsigned char* buffer, size_t u, size_t v, size_t u2)
{
  out.resize(u * v * 4);

  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  for(size_t c = 0; c < 4; c++)
  {
    size_t index = u * 4 * y + 4 * x + c;
    size_t index2 = u2 * 4 * y + 4 * x + c;

    out[index] = buffer[index2];
  }
}

void getAlignedBufferRGBA32F(std::vector<float>& out, float* buffer, size_t u, size_t v, size_t u2)
{
  out.resize(u * v * 4);

  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  for(size_t c = 0; c < 4; c++)
  {
    size_t index = u * 4 * y + 4 * x + c;
    size_t index2 = u2 * 4 * y + 4 * x + c;

    out[index] = buffer[index2];
  }
}

////////////////////////////////////////////////////////////////////////////////

/*
It's actually not an octree but a tree with 16 children per node instead of 8,
because we use RGBA instead of just RGB. But the name octree is so widespread
for color quantization that it makes sense to call it that.

The struct is a node of the tree.
*/
struct OctreeRGBA8
{
  OctreeRGBA8* children[16];
  //TODO: If I'm going to use this for quantization and palette generation, add count here (sum of all child counts)

  OctreeRGBA8()
  {
    for(size_t i = 0; i < 16; i++) children[i] = 0;
  }

  ~OctreeRGBA8()
  {
    for(size_t i = 0; i < 16; i++) delete children[i];
  }

  //should be called with bit 0 on the root
  bool hasColor(unsigned char r, unsigned char g, unsigned char b, unsigned char a, int bit = 0)
  {
    int index = 8 * ((r >> bit) & 1) + 4 * ((g >> bit) & 1) + 2 * ((b >> bit) & 1) + 1 * ((a >> bit) & 1);
    OctreeRGBA8* child = children[index];

    if(!child) return false;
    if(bit >= 7) return true;
    return child->hasColor(r, g, b, a, bit + 1);
  }

  //should be called with bit 0 on the root
  void addColor(unsigned char r, unsigned char g, unsigned char b, unsigned char a, int bit = 0)
  {
    int index = 8 * ((r >> bit) & 1) + 4 * ((g >> bit) & 1) + 2 * ((b >> bit) & 1) + 1 * ((a >> bit) & 1);
    OctreeRGBA8*& child = children[index];

    if(!child) child = new OctreeRGBA8();

    if(bit >= 7) return;
    child->addColor(r, g, b, a, bit + 1);
  }
};

//unsigned is required to have exactly as many bits as float, which is usually true in C++
inline unsigned getFloatBits(float f)
{
  assert(sizeof(float) == sizeof(unsigned)); //otherwise the bits are wrongly aligned. If this assert triggers, make this more portable.
  union {unsigned i; float f;} u;
  u.f = f;
  return u.i;
}

//Idem as OctreeRGBA8, but then for 32-bit floating point RGBA colors (bits get extracted from the floats)
struct OctreeRGBA32F
{
  OctreeRGBA32F* children[16];
  //TODO: If I'm going to use this for quantization and palette generation, add count here (sum of all child counts)

  OctreeRGBA32F()
  {
    for(size_t i = 0; i < 16; i++) children[i] = 0;
  }

  ~OctreeRGBA32F()
  {
    for(size_t i = 0; i < 16; i++) delete children[i];
  }

  //should be called with bit 0 on the root
  bool hasColor(float rf, float gf, float bf, float af, int bit = 0)
  {
    unsigned r = getFloatBits(rf), g = getFloatBits(gf), b = getFloatBits(bf), a = getFloatBits(af);
    int index = 8 * ((r >> bit) & 1) + 4 * ((g >> bit) & 1) + 2 * ((b >> bit) & 1) + 1 * ((a >> bit) & 1);
    OctreeRGBA32F* child = children[index];

    if(!child) return false;
    if(bit >= 31) return true;
    return child->hasColor(rf, gf, bf, af, bit + 1);
  }

  //should be called with bit 0 on the root
  void addColor(float rf, float gf, float bf, float af, int bit = 0)
  {
    unsigned r = getFloatBits(rf), g = getFloatBits(gf), b = getFloatBits(bf), a = getFloatBits(af);
    int index = 8 * ((r >> bit) & 1) + 4 * ((g >> bit) & 1) + 2 * ((b >> bit) & 1) + 1 * ((a >> bit) & 1);
    OctreeRGBA32F*& child = children[index];

    if(!child) child = new OctreeRGBA32F();

    if(bit >= 31) return;
    child->addColor(rf, gf, bf, af, bit + 1);
  }
};

size_t countColorsRGBA8(const unsigned char* buffer, size_t u, size_t v, size_t u2)
{
  OctreeRGBA8 m;
  size_t count = 0;
  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  {
    size_t index = y * u2 * 4 + x * 4;
    if(!m.hasColor(buffer[index], buffer[index + 1], buffer[index + 2], buffer[index + 3]))
    {
      count++;
      m.addColor(buffer[index], buffer[index + 1], buffer[index + 2], buffer[index + 3]);
    }
  }
  return count;
}


size_t countColorsRGBA32F(const float* buffer, size_t u, size_t v, size_t u2)
{
  OctreeRGBA32F m;
  size_t count = 0;
  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  {
    size_t index = y * u2 * 4 + x * 4;
    if(!m.hasColor(buffer[index], buffer[index + 1], buffer[index + 2], buffer[index + 3]))
    {
      count++;
      m.addColor(buffer[index], buffer[index + 1], buffer[index + 2], buffer[index + 3]);
    }
  }
  return count;
}

size_t countColorsGrey8(const unsigned char* buffer, size_t u, size_t v, size_t u2)
{
  std::map<unsigned char, unsigned> m;
  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  {
    m[buffer[y * u2 + x]]++;
  }
  return m.size();
}

size_t countColorsGrey32F(const float* buffer, size_t u, size_t v, size_t u2)
{
  std::map<float, unsigned> m;
  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  {
    m[buffer[y * u2 + x]]++;
  }
  return m.size();
}
