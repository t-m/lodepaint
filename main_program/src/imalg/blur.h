/*
LodePaint

Copyright (c) 2009-2010 Lode Vandevenne
All rights reserved.

This file is part of LodePaint.

LodePaint is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LodePaint is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LodePaint.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <cstring>

#include "progress.h"

void gaussianBlurRGBA8(unsigned char* buffer, size_t u, size_t v, size_t u2, double radius, bool wrapping, bool affect_alpha, Progress& progress);
void gaussianBlurRGBA32F(float* buffer, size_t u, size_t v, size_t u2, double radius, bool wrapping, bool affect_alpha, Progress& progress);
void gaussianBlurGrey8(unsigned char* buffer, size_t u, size_t v, size_t u2, double radius, bool wrapping, Progress& progress);
void gaussianBlurGrey32F(float* buffer, size_t u, size_t v, size_t u2, double radius, bool wrapping, Progress& progress);

void gaussianBlurSelectiveRGBA8(unsigned char* buffer, size_t u, size_t v, size_t u2, double radius, double threshold, bool wrapping, bool affect_alpha, Progress& progress);
void gaussianBlurSelectiveRGBA32F(float* buffer, size_t u, size_t v, size_t u2, double radius, double threshold, bool wrapping, bool affect_alpha, Progress& progress);
void gaussianBlurSelectiveGrey8(unsigned char* buffer, size_t u, size_t v, size_t u2, double radius, double threshold, bool wrapping, Progress& progress);
void gaussianBlurSelectiveGrey32F(float* buffer, size_t u, size_t v, size_t u2, double radius, double threshold, bool wrapping, Progress& progress);

void medianFilterRGBA8(unsigned char* buffer, size_t u, size_t v, size_t u2, int diameterx, int diametery, bool wrapping, bool affect_alpha, Progress& progress);
void medianFilterRGBA32F(float* buffer, size_t u, size_t v, size_t u2, int diameterx, int diametery, bool wrapping, bool affect_alpha, Progress& progress);

void adaptiveMedianFilterRGBA8(unsigned char* buffer, size_t u, size_t v, size_t u2, int maxdiameter, bool wrapping, bool affect_alpha, Progress& progress);

//interpolating blur: a very cheap way to blur (same result as making size of image smaller, then rescaling it to original size again with interpolation).
//Much faster than gaussian blur, but ugly, used as background behind some other filters (such as pointillism)
void interpolatingBlurRGBA8(unsigned char* buffer, size_t u, size_t v, size_t u2, int radius, bool affect_alpha, Progress& progress);

//softest possible blur (hardly noticable)
void softestBlurRGBA8(unsigned char* buffer, size_t u, size_t v, size_t u2, int amount, bool wrapping, bool affect_alpha, Progress& progress);
