/*
LodePaint

Copyright (c) 2009-2010 Lode Vandevenne
All rights reserved.

This file is part of LodePaint.

LodePaint is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LodePaint is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LodePaint.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "floodfill.h"
#include "../lpi/lpi_math.h"



/*
w, h is the size of the buffer
x0, y0, x1, y1 is the size of the box in which the floodfill is performed
ox0, oy0, ox1, oy1 is the bounding box around the zone where changes happened (for memory efficient undo and texture partial upload zone)
*/
void floodFillRGBA8(int& ox0, int& oy0, int& ox1, int& oy1
                  , RGBA8* buffer
                  , size_t u, size_t v, size_t u2
                  , int x, int y
                  , int x0, int y0, int x1, int y1
                  , const RGBA8& newColor, const RGBA8& oldColor
                  , int tolerance, bool tolerance_ignores_alpha)
{
  //Scanline Floodfill Algorithm With Stack
  
  
  ox0 = x;
  oy0 = y;
  ox1 = x + 1;
  oy1 = y + 1;
  
  if(tolerance == 0 && oldColor == newColor) return;
  
  std::vector<size_t> stack_x;
  std::vector<size_t> stack_y;
  std::vector<bool> been(u * v, false); //where we've been so far

  int xt;
  
  stack_x.push_back(x);
  stack_y.push_back(y);

  while(stack_x.size() > 0)
  {
    x = stack_x.back(); stack_x.pop_back();
    y = stack_y.back(); stack_y.pop_back();
    
    xt = x;
    while(xt >= x0 && !been[u * y + xt] && tolcomp(buffer[u2 * y + xt], oldColor, tolerance, tolerance_ignores_alpha)) xt--; xt++;
    while(xt < x1 && !been[u * y + xt] && tolcomp(buffer[u2 * y + xt], oldColor, tolerance, tolerance_ignores_alpha))
    {
      if(xt < ox0) ox0 = xt;
      if(y < oy0) oy0 = y;
      if(xt >= ox1) ox1 = xt + 1;
      if(y >= oy1) oy1 = y + 1;
      if(tolerance_ignores_alpha)
      {
        unsigned char* aa = (unsigned char*)(&buffer[u2 * y + xt]);
        unsigned char* bb = (unsigned char*)(&newColor);
        unsigned char a = (aa[3] * bb[3]) / 255;
        aa[0] = bb[0];
        aa[1] = bb[1];
        aa[2] = bb[2];
        aa[3] = a;
      }
      else buffer[u2 * y + xt] = newColor;
      been[u * y + xt] = true;
      if(y > y0 && !been[u * (y - 1) + xt] && tolcomp(buffer[u2 * (y - 1) + xt], oldColor, tolerance, tolerance_ignores_alpha))
      {
        stack_x.push_back(xt);
        stack_y.push_back(y - 1);
      }
      if(y < y1 - 1 && !been[u * (y + 1) + xt] && tolcomp(buffer[u2 * (y + 1) + xt], oldColor, tolerance, tolerance_ignores_alpha))
      {
        stack_x.push_back(xt);
        stack_y.push_back(y + 1);
      }
      xt++;
    }
  }
}


void floodFillRGBA8RGBA8(int& ox0, int& oy0, int& ox1, int& oy1
                       , RGBA8* obuffer, const RGBA8* ibuffer
                       , size_t u, size_t v, size_t ou2, size_t iu2
                       , int x, int y
                       , int x0, int y0, int x1, int y1
                       , const RGBA8& newColor, const RGBA8& oldColor
                       , int tolerance, bool tolerance_ignores_alpha)
{
  //Scanline Floodfill Algorithm With Stack

  ox0 = x;
  oy0 = y;
  ox1 = x + 1;
  oy1 = y + 1;

  if(tolerance == 0 && oldColor == newColor) return;

  std::vector<size_t> stack_x;
  std::vector<size_t> stack_y;
  std::vector<bool> been(u * v, false); //where we've been so far

  int xt;

  stack_x.push_back(x);
  stack_y.push_back(y);

  while(stack_x.size() > 0)
  {
    x = stack_x.back(); stack_x.pop_back();
    y = stack_y.back(); stack_y.pop_back();
    xt = x;
    while(xt >= x0 && !been[u * y + xt] && tolcomp(ibuffer[iu2 * y + xt], oldColor, tolerance, tolerance_ignores_alpha)) xt--; xt++;
    while(xt < x1 && !been[u * y + xt] && tolcomp(ibuffer[iu2 * y + xt], oldColor, tolerance, tolerance_ignores_alpha))
    {
      if(xt < ox0) ox0 = xt;
      if(y < oy0) oy0 = y;
      if(xt >= ox1) ox1 = xt + 1;
      if(y >= oy1) oy1 = y + 1;
      if(tolerance_ignores_alpha)
      {
        unsigned char* aa = (unsigned char*)(&obuffer[ou2 * y + xt]);
        unsigned char* bb = (unsigned char*)(&newColor);
        unsigned char a = (aa[3] * bb[3]) / 255;
        aa[0] = bb[0];
        aa[1] = bb[1];
        aa[2] = bb[2];
        aa[3] = a;
      }
      else obuffer[ou2 * y + xt] = newColor;
      been[u * y + xt] = true;
      if(y > y0 && !been[u * (y - 1) + xt] && tolcomp(ibuffer[iu2 * (y - 1) + xt], oldColor, tolerance, tolerance_ignores_alpha))
      {
        stack_x.push_back(xt);
        stack_y.push_back(y - 1);
      }
      if(y < y1 - 1 && !been[u * (y + 1) + xt] && tolcomp(ibuffer[iu2 * (y + 1) + xt], oldColor, tolerance, tolerance_ignores_alpha))
      {
        stack_x.push_back(xt);
        stack_y.push_back(y + 1);
      }
      xt++;
    }
  }
}

void floodFillRGBA8Grey8(int& ox0, int& oy0, int& ox1, int& oy1
                       , unsigned char* obuffer, const RGBA8* ibuffer
                       , size_t u, size_t v, size_t ou2, size_t iu2
                       , int x, int y
                       , int x0, int y0, int x1, int y1
                       , unsigned char newColor, const RGBA8& oldColor
                       , int tolerance, bool tolerance_ignores_alpha)
{
  //Scanline Floodfill Algorithm With Stack

  ox0 = x;
  oy0 = y;
  ox1 = x + 1;
  oy1 = y + 1;

  std::vector<size_t> stack_x;
  std::vector<size_t> stack_y;
  std::vector<bool> been(u * v, false); //where we've been so far

  int xt;

  stack_x.push_back(x);
  stack_y.push_back(y);

  while(stack_x.size() > 0)
  {
    x = stack_x.back(); stack_x.pop_back();
    y = stack_y.back(); stack_y.pop_back();
    xt = x;
    while(xt >= x0 && !been[u * y + xt] && tolcomp(ibuffer[iu2 * y + xt], oldColor, tolerance, tolerance_ignores_alpha)) xt--; xt++;
    while(xt < x1 && !been[u * y + xt] && tolcomp(ibuffer[iu2 * y + xt], oldColor, tolerance, tolerance_ignores_alpha))
    {
      if(xt < ox0) ox0 = xt;
      if(y < oy0) oy0 = y;
      if(xt >= ox1) ox1 = xt + 1;
      if(y >= oy1) oy1 = y + 1;
      obuffer[ou2 * y + xt] = newColor;
      been[u * y + xt] = true;
      if(y > y0 && !been[u * (y - 1) + xt] && tolcomp(ibuffer[iu2 * (y - 1) + xt], oldColor, tolerance, tolerance_ignores_alpha))
      {
        stack_x.push_back(xt);
        stack_y.push_back(y - 1);
      }
      if(y < y1 - 1 && !been[u * (y + 1) + xt] && tolcomp(ibuffer[iu2 * (y + 1) + xt], oldColor, tolerance, tolerance_ignores_alpha))
      {
        stack_x.push_back(xt);
        stack_y.push_back(y + 1);
      }
      xt++;
    }
  }
}

void floodFillRGBA32F(int& ox0, int& oy0, int& ox1, int& oy1
                    , RGBA32F* buffer
                    , size_t u, size_t v, size_t u2
                    , int x, int y
                    , int x0, int y0, int x1, int y1
                    , const RGBA32F& newColor, const RGBA32F& oldColor
                    , float tolerance, bool tolerance_ignores_alpha)
{
  //Scanline Floodfill Algorithm With Stack
  
  
  ox0 = x;
  oy0 = y;
  ox1 = x + 1;
  oy1 = y + 1;
  
  if(tolerance == 0 && oldColor == newColor) return;
  
  std::vector<size_t> stack_x;
  std::vector<size_t> stack_y;
  std::vector<bool> been(u * v, false); //where we've been so far

  int xt;

  stack_x.push_back(x);
  stack_y.push_back(y);

  while(stack_x.size() > 0)
  {
    x = stack_x.back(); stack_x.pop_back();
    y = stack_y.back(); stack_y.pop_back();
    
    xt = x;
    while(xt >= x0 && !been[u * y + xt] && tolcomp(buffer[u2 * y + xt], oldColor, tolerance, tolerance_ignores_alpha)) xt--; xt++;
    while(xt < x1 && !been[u * y + xt] && tolcomp(buffer[u2 * y + xt], oldColor, tolerance, tolerance_ignores_alpha))
    {
      if(xt < ox0) ox0 = xt;
      if(y < oy0) oy0 = y;
      if(xt >= ox1) ox1 = xt + 1;
      if(y >= oy1) oy1 = y + 1;
      if(tolerance_ignores_alpha)
      {
        float* aa = (float*)(&buffer[u2 * y + xt]);
        float* bb = (float*)(&newColor);
        float a = aa[3] * bb[3];
        aa[0] = bb[0];
        aa[1] = bb[1];
        aa[2] = bb[2];
        aa[3] = a;
      }
      else buffer[u2 * y + xt] = newColor;
      been[u * y + xt] = true;
      if(y > y0 && !been[u * (y - 1) + xt] && tolcomp(buffer[u2 * (y - 1) + xt], oldColor, tolerance, tolerance_ignores_alpha))
      {
        stack_x.push_back(xt);
        stack_y.push_back(y - 1);
      }
      if(y < y1 - 1 && !been[u * (y + 1) + xt] && tolcomp(buffer[u2 * (y + 1) + xt], oldColor, tolerance, tolerance_ignores_alpha))
      {
        stack_x.push_back(xt);
        stack_y.push_back(y + 1);
      }
      xt++;
    }
  }
}


