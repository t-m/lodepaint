/*
LodePaint

Copyright (c) 2009-2010 Lode Vandevenne
All rights reserved.

This file is part of LodePaint.

LodePaint is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LodePaint is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LodePaint.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <vector>

using std::size_t;

//matrix must have size N*M
template<int N, int M>
void convoluteRGBA8(unsigned char* buffer, size_t u, size_t v, size_t u2, const double* matrix, bool wrapping, bool affect_alpha, double extra_term, double mul)
{
  const int N2 = (N - 1) / 2;
  const int M2 = (M - 1) / 2;
  
  //detect if matrix has many zeroes
  int startx = 0;
  int starty = 0;
  int endx = 1;
  int endy = 1;
  for(int y = -M2; y <= M2; y++)
  for(int x = -N2; x <= N2; x++)
  {
    if(matrix[(M2 + y) * N + (N2 + x)] != 0.0)
    {
      if(x < startx) startx = x;
      if(y < starty) starty = y;
      if(x + 1 > endx) endx = x + 1;
      if(y + 1 > endy) endy = y + 1;
    }
  }

  std::vector<unsigned char> temp(u * v * 4);

  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  for(size_t c = 0; c < 4; c++)
  {
    if(c == 3 && !affect_alpha) continue;
    double value = 0.0;
    for(int y2 = starty; y2 < endy; y2++)
    {
      int y3 = y + y2;
      if(y3 < 0) y3 = wrapping ? v + y3 : 0;
      else if(y3 >= (int)v) y3 = wrapping ? v - y3 : v - 1;
      //for in case the image is so small the coordinate still doesn't fit...:
      if(y3 < 0) y3 = 0;
      if(y3 >= (int)v) y3 = wrapping ? 0 : v - 1;
      for(int x2 = startx; x2 < endx; x2++)
      {
        int x3 = x + x2;
        if(x3 < 0) x3 = wrapping ? u + x3 : 0;
        else if(x3 >= (int)u) x3 = wrapping ? u - x3 : u - 1;
        //for in case the image is so small the coordinate still doesn't fit...:
        if(x3 < 0) x3 = 0;
        if(x3 >= (int)u) x3 = wrapping ? 0 : u - 1;

        size_t index = y3 * u2 * 4 + x3 * 4 + c;
        value += mul * matrix[(M2 + y2) * N + (N2 + x2)] * (double)buffer[index];
      } //for x2
    } //for y2
    size_t index = y * u * 4 + x * 4 + c;
    value += extra_term;
    if(value < 0) value = 0;
    if(value > 255) value = 255;
    temp[index] = (unsigned char)(value);
  } //for x, y, c

  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  for(size_t c = 0; c < 4; c++)
  {
    if(c == 3 && !affect_alpha) continue;
    size_t index = y * u * 4 + x * 4 + c;
    size_t index2 = y * u2 * 4 + x * 4 + c;
    buffer[index2] = temp[index];
  }
}

//matrix must have size N*M
template<int N, int M>
void convoluteRGBA32F(float* buffer, size_t u, size_t v, size_t u2, const double* matrix, bool wrapping, bool affect_alpha, double extra_term, double mul)
{
  const int N2 = (N - 1) / 2;
  const int M2 = (M - 1) / 2;
  
  //detect if matrix has many zeroes
  int startx = 0;
  int starty = 0;
  int endx = 1;
  int endy = 1;
  for(int y = -M2; y <= M2; y++)
  for(int x = -N2; x <= N2; x++)
  {
    if(matrix[(M2 + y) * N + (N2 + x)] != 0.0)
    {
      if(x < startx) startx = x;
      if(y < starty) starty = y;
      if(x + 1 > endx) endx = x + 1;
      if(y + 1 > endy) endy = y + 1;
    }
  }

  std::vector<float> temp(u * v * 4);

  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  for(size_t c = 0; c < 4; c++)
  {
    if(c == 3 && !affect_alpha) continue;
    double value = 0.0;
    for(int y2 = starty; y2 < endy; y2++)
    {
      int y3 = y + y2;
      if(y3 < 0) y3 = wrapping ? v + y3 : 0;
      else if(y3 >= (int)v) y3 = wrapping ? v - y3 : v - 1;
      //for in case the image is so small the coordinate still doesn't fit...:
      if(y3 < 0) y3 = 0;
      if(y3 >= (int)v) y3 = wrapping ? 0 : v - 1;
      for(int x2 = startx; x2 < endx; x2++)
      {
        int x3 = x + x2;
        if(x3 < 0) x3 = wrapping ? u + x3 : 0;
        else if(x3 >= (int)u) x3 = wrapping ? u - x3 : u - 1;
        //for in case the image is so small the coordinate still doesn't fit...:
        if(x3 < 0) x3 = 0;
        if(x3 >= (int)u) x3 = wrapping ? 0 : u - 1;

        size_t index = y3 * u2 * 4 + x3 * 4 + c;
        value += mul * matrix[(M2 + y2) * N + (N2 + x2)] * (double)buffer[index];
      } //for x2
    } //for y2
    size_t index = y * u * 4 + x * 4 + c;
    value += extra_term;
    temp[index] = (float)(value);
  } //for x, y, c

  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  for(size_t c = 0; c < 4; c++)
  {
    if(c == 3 && !affect_alpha) continue;
    size_t index = y * u * 4 + x * 4 + c;
    size_t index2 = y * u2 * 4 + x * 4 + c;
    buffer[index2] = temp[index];
  }
}




