/*
LodePaint

Copyright (c) 2009-2010 Lode Vandevenne
All rights reserved.

This file is part of LodePaint.
filters
LodePaint is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LodePaint is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LodePaint.  If not, see <http://www.gnu.org/licenses/>.
*/ 

#include "progress.h"

Progress::Progress()
: progress(0.0)
, cancel(false)
, status(S_PRE)
{
}

void Progress::setProgress(double val)
{
  progress = val;
}

double Progress::getProgress() const
{
  return progress;
}

//set to true if user wants to cancel. Algorithm should then stop.
void Progress::setUserCancel(bool cancel)
{
  this->cancel = cancel;
}

bool Progress::isUserCancel() const
{
  return cancel;
}

bool Progress::handle(double progress)
{
  setProgress(progress);
  if(isUserCancel())
  {
    setStatus(S_KILLED);
    return true;
  }
  return false;
}

bool Progress::handle(int i, int n)
{
  return handle((double)i / (double)n);
}

bool Progress::handle(int i, int n, int j, int m)
{
  return handle( (double)j / (double)m + ((double)i / (double)n) / (double)m);
}

void Progress::setStatus(Status status)
{
  this->status = status;
}

Status Progress::getStatus() const
{
  return status;
}

////////////////////////////////////////////////////////////////////////////////

NestedProgress::NestedProgress(Progress* p, double p0, double p1)
: p(p)
, p0(p0)
, p1(p1)
{
}

void NestedProgress::setProgress(double val)
{
  progress = val;
  double masterProgress = p0 + progress * (p1 - p0);
  p->setProgress(masterProgress);
}

void NestedProgress::setInterval(double p0, double p1)
{
  this->p0 = p0;
  this->p1 = p1;
}

double NestedProgress::getProgress() const
{
  return progress;
}

//set to true if user wants to cancel. Algorithm should then stop.
void NestedProgress::setUserCancel(bool cancel)
{
  p->setUserCancel(cancel);
}

bool NestedProgress::isUserCancel() const
{
  return p->isUserCancel() ;
}

void NestedProgress::setStatus(Status status)
{
  p->setStatus(status);
}

Status NestedProgress::getStatus() const
{
  return p->getStatus();
}
