/*
LodePaint

Copyright (c) 2009-2010 Lode Vandevenne
All rights reserved.

This file is part of LodePaint.

LodePaint is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LodePaint is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LodePaint.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <vector>

#include "../lpi/lpi_color.h"

using std::size_t;

//sets alpha channel to 255
void alphaOpaqueRGBA8(unsigned char* buffer, size_t u, size_t v, size_t u2);
void alphaOpaqueRGBA32F(float* buffer, size_t u, size_t v, size_t u2);

//sets alpha channel to brightness
void alphaTranslucentRGBA8(unsigned char* buffer, size_t u, size_t v, size_t u2);
void alphaTranslucentRGBA32F(float* buffer, size_t u, size_t v, size_t u2);

void alphaModulatedRGBA8(unsigned char* buffer, size_t u, size_t v, size_t u2);
void alphaModulatedRGBA32F(float* buffer, size_t u, size_t v, size_t u2);

void alphaSaturationRGBA8(unsigned char* buffer, size_t u, size_t v, size_t u2);
void alphaSaturationRGBA32F(float* buffer, size_t u, size_t v, size_t u2);

void alphaColorKeyRGBA8(unsigned char* buffer, size_t u, size_t v, size_t u2, const lpi::ColorRGB& key);
void alphaColorKeyRGBA32F(float* buffer, size_t u, size_t v, size_t u2, const lpi::ColorRGBd& key);

void alphaSoftKeyRGBA8(unsigned char* buffer, size_t u, size_t v, size_t u2, const lpi::ColorRGB& key);
void alphaSoftKeyRGBA32F(float* buffer, size_t u, size_t v, size_t u2, const lpi::ColorRGBd& key);

//sets the complete image to the given color, while keeping the alpha channel intact
void keepOnlyAlphaRGBA8(unsigned char* buffer, size_t u, size_t v, size_t u2, const lpi::ColorRGB& color);
void keepOnlyAlphaRGBA32F(float* buffer, size_t u, size_t v, size_t u2, const lpi::ColorRGBd& color);

void alphaToGreyRGBA8(unsigned char* buffer, size_t u, size_t v, size_t u2);
void alphaToGreyRGBA32F(float* buffer, size_t u, size_t v, size_t u2);


void alphaExtractCloudRGBA8(unsigned char* buffer, size_t u, size_t v, size_t u2, const lpi::ColorRGB& leftColor, const lpi::ColorRGB& rightColor);
void alphaExtractCloudRGBA32F(float* buffer, size_t u, size_t v, size_t u2, const lpi::ColorRGBd& leftColor, const lpi::ColorRGBd& rightColor);



