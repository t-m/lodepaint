/*
LodePaint

Copyright (c) 2009-2010 Lode Vandevenne
All rights reserved.

This file is part of LodePaint.

LodePaint is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LodePaint is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LodePaint.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "generate.h"

#include <cmath>

static const double TWOPI = 6.283185307179586477;

PlasmaSinParam::PlasmaSinParam()
: period_x(16.0)
, period_y(16.0)
, shift_x(0.0)
, shift_y(0.0)
, power(1.0)
, circular(false)
{
}

void generatePlasmaRGBA8(unsigned char* buffer, size_t u, size_t v, size_t u2, const PlasmaParam& param, double opacity)
{
  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  {
    size_t index = y * u2 * 4 + x * 4;
    
    double v = 0;
    
    for(size_t i = 0; i < param.sins.size(); i++)
    {
      const PlasmaSinParam& s = param.sins[i];
      
      double d = 0;
      
      if(s.circular)
      {
        double dx = x - s.shift_x;
        double dy = y - s.shift_y;
        d = s.power * std::sin(TWOPI * std::sqrt((dx * dx + dy * dy) / (s.period_x * s.period_x) + 1)) / 2.0;
      }
      else
      {
        d = s.power * (std::sin((double)x * TWOPI / s.period_x + s.shift_x)
                     + std::sin((double)y * TWOPI / s.period_y + s.shift_y)
                     + 2) / 4.0;
      }
                          
      v += d;
                        
      /*int r = (int)(buffer[index + 0] * (1.0 - d) + s.color.r * d * 255); if(r > 255) r = 255; if(r < 0) r = 0;
      int g = (int)(buffer[index + 1] * (1.0 - d) + s.color.g * d * 255); if(g > 255) g = 255; if(g < 0) g = 0;
      int b = (int)(buffer[index + 2] * (1.0 - d) + s.color.b * d * 255); if(b > 255) b = 255; if(b < 0) b = 0;
      int a = (int)(buffer[index + 3] * (1.0 - d) + s.color.a * d * 255); if(a > 255) a = 255; if(a < 0) a = 0;
      
      buffer[index + 0] = r;
      buffer[index + 1] = g;
      buffer[index + 2] = b;
      buffer[index + 3] = a;*/
    }
    
    //v /= param.sins.size();
    
    if(v > 1.0) v = 1.0;
    if(v < 0.0) v = 0.0;
    
    buffer[index + 0] = (int)(buffer[index + 0] * (1.0 - opacity) + v * 255 * opacity);
    buffer[index + 1] = (int)(buffer[index + 1] * (1.0 - opacity) + v * 255 * opacity);
    buffer[index + 2] = (int)(buffer[index + 2] * (1.0 - opacity) + v * 255 * opacity);
  }
}




