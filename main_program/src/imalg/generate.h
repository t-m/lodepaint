/*
LodePaint

Copyright (c) 2009-2010 Lode Vandevenne
All rights reserved.

This file is part of LodePaint.

LodePaint is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LodePaint is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LodePaint.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "../lpi/lpi_color.h"

#include <vector>

using std::size_t;

struct PlasmaSinParam
{
  double period_x;
  double period_y;
  double shift_x;
  double shift_y;
  double power; //range 0.0-1.0 (for non HDR images)
  bool circular;
  
  PlasmaSinParam();
};

struct PlasmaParam
{
  std::vector<PlasmaSinParam> sins;
};

void generatePlasmaRGBA8(unsigned char* buffer, size_t u, size_t v, size_t u2, const PlasmaParam& param, double opacity);


