/*
LodePaint

Copyright (c) 2009-2010 Lode Vandevenne
All rights reserved.

This file is part of LodePaint.

LodePaint is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LodePaint is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LodePaint.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "colorop.h"
#include <algorithm>
#include <cmath>
#include <limits>

#include "../lpi/lpi_color.h"
#include "../lpi/lpi_math.h"

void normalizeRGBA8(unsigned char* buffer, size_t u, size_t v, size_t u2, bool link_rgb, bool link_rgba, bool affect_alpha)
{
  lpi::ColorRGB maxColor(0, 0, 0, 0);
  lpi::ColorRGB minColor(255, 255, 255, 255);

  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  {
    size_t index = y * u2 * 4 + x * 4;
    maxColor.r = std::max(maxColor.r, (int)buffer[index + 0]);
    maxColor.g = std::max(maxColor.g, (int)buffer[index + 1]);
    maxColor.b = std::max(maxColor.b, (int)buffer[index + 2]);
    maxColor.a = std::max(maxColor.a, (int)buffer[index + 3]);
    minColor.r = std::min(minColor.r, (int)buffer[index + 0]);
    minColor.g = std::min(minColor.g, (int)buffer[index + 1]);
    minColor.b = std::min(minColor.b, (int)buffer[index + 2]);
    minColor.a = std::min(minColor.a, (int)buffer[index + 3]);
  }
  
  if(link_rgba)
  {
    maxColor.r = std::max(maxColor.r, maxColor.g);
    maxColor.r = std::max(maxColor.r, maxColor.b);
    if(affect_alpha) maxColor.r = std::max(maxColor.r, maxColor.a);
    minColor.r = std::min(minColor.r, minColor.g);
    minColor.r = std::min(minColor.r, minColor.b);
    if(affect_alpha) minColor.r = std::min(minColor.r, minColor.a);
    double shift = minColor.r;
    double factor = 1.0 / ((maxColor.r - minColor.r) / 255.0);
    if(maxColor.r == minColor.r) { shift = 0.0f; factor = 1.0f; }

    for(size_t y = 0; y < v; y++)
    for(size_t x = 0; x < u; x++)
    for(size_t c = 0; c < 4; c++)
    {
      if(c == 3 && !affect_alpha) continue;
      size_t index = y * u2 * 4 + x * 4 + c;
      buffer[index] = (unsigned char)((buffer[index] - shift) * factor);
    }
  }
  else if(link_rgb)
  {
    maxColor.r = std::max(maxColor.r, maxColor.g);
    maxColor.r = std::max(maxColor.r, maxColor.b);
    minColor.r = std::min(minColor.r, minColor.g);
    minColor.r = std::min(minColor.r, minColor.b);
    double shift = minColor.r;
    double factor = 1.0 / ((maxColor.r - minColor.r) / 255.0);
    if(maxColor.r == minColor.r) { shift = 0.0f; factor = 1.0f; }
    double shift_a = minColor.a;
    double factor_a = 1.0 / ((maxColor.a - minColor.a) / 255.0);
    if(maxColor.a == minColor.a) { shift_a = 0.0f; factor_a = 1.0f; }

    for(size_t y = 0; y < v; y++)
    for(size_t x = 0; x < u; x++)
    for(size_t c = 0; c < 4; c++)
    {
      if(c == 3 && !affect_alpha) continue;
      size_t index = y * u2 * 4 + x * 4 + c;
      if(c == 3) buffer[index] = (unsigned char)((buffer[index] - shift_a) * factor_a);
      else buffer[index] = (unsigned char)((buffer[index] - shift) * factor);
    }
  }
  else
  {
    double shift_r = minColor.r;
    double factor_r = 1.0 / ((maxColor.r - minColor.r) / 255.0);
    if(maxColor.r == minColor.r) { shift_r = 0.0f; factor_r = 1.0f; }
    double shift_g = minColor.g;
    double factor_g = 1.0 / ((maxColor.g - minColor.g) / 255.0);
    if(maxColor.g == minColor.g) { shift_g = 0.0f; factor_g = 1.0f; }
    double shift_b = minColor.b;
    double factor_b = 1.0 / ((maxColor.b - minColor.b) / 255.0);
    if(maxColor.b == minColor.b) { shift_b = 0.0f; factor_b = 1.0f; }
    double shift_a = minColor.a;
    double factor_a = 1.0 / ((maxColor.a - minColor.a) / 255.0);
    if(maxColor.a == minColor.a) { shift_a = 0.0f; factor_a = 1.0f; }
    for(size_t y = 0; y < v; y++)
    for(size_t x = 0; x < u; x++)
    {
      size_t index = y * u2 * 4 + x * 4;
      buffer[index + 0] = (unsigned char)((buffer[index + 0] - shift_r) * factor_r);
      buffer[index + 1] = (unsigned char)((buffer[index + 1] - shift_g) * factor_g);
      buffer[index + 2] = (unsigned char)((buffer[index + 2] - shift_b) * factor_b);
      if(affect_alpha) buffer[index + 3] = (unsigned char)((buffer[index + 3] - shift_a) * factor_a);
    }
  }
}

void normalizeRGBA32F(float* buffer, size_t u, size_t v, size_t u2, bool link_rgb, bool link_rgba, bool affect_alpha)
{
  float limit = std::numeric_limits<float>::max();
  lpi::ColorRGBf maxColor(-limit, -limit, -limit, -limit);
  lpi::ColorRGBf minColor(limit, limit, limit, limit);

  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  {
    size_t index = y * u2 * 4 + x * 4;
    maxColor.r = std::max(maxColor.r, buffer[index + 0]);
    maxColor.g = std::max(maxColor.g, buffer[index + 1]);
    maxColor.b = std::max(maxColor.b, buffer[index + 2]);
    maxColor.a = std::max(maxColor.a, buffer[index + 3]);
    minColor.r = std::min(minColor.r, buffer[index + 0]);
    minColor.g = std::min(minColor.g, buffer[index + 1]);
    minColor.b = std::min(minColor.b, buffer[index + 2]);
    minColor.a = std::min(minColor.a, buffer[index + 3]);
  }

  if(link_rgba)
  {
    maxColor.r = std::max(maxColor.r, maxColor.g);
    maxColor.r = std::max(maxColor.r, maxColor.b);
    if(affect_alpha) maxColor.r = std::max(maxColor.r, maxColor.a);
    minColor.r = std::min(minColor.r, minColor.g);
    minColor.r = std::min(minColor.r, minColor.b);
    if(affect_alpha) minColor.r = std::min(minColor.r, minColor.a);
    float shift = minColor.r;
    float factor = 1.0f / (maxColor.r - minColor.r);
    if(maxColor.r == minColor.r) { shift = 0.0f; factor = 1.0f; }

    for(size_t y = 0; y < v; y++)
    for(size_t x = 0; x < u; x++)
    for(size_t c = 0; c < 4; c++)
    {
      if(c == 3 && !affect_alpha) continue;
      size_t index = y * u2 * 4 + x * 4 + c;
      buffer[index] = (buffer[index] - shift) * factor;
    }
  }
  else if(link_rgb)
  {
    maxColor.r = std::max(maxColor.r, maxColor.g);
    maxColor.r = std::max(maxColor.r, maxColor.b);
    minColor.r = std::min(minColor.r, minColor.g);
    minColor.r = std::min(minColor.r, minColor.b);
    double shift = minColor.r;
    double factor = 1.0 / (maxColor.r - minColor.r);
    if(maxColor.r == minColor.r) { shift = 0.0f; factor = 1.0f; }
    double shift_a = minColor.a;
    double factor_a = 1.0 / (maxColor.a - minColor.a);
    if(maxColor.a == minColor.a) { shift_a = 0.0f; factor_a = 1.0f; }

    for(size_t y = 0; y < v; y++)
    for(size_t x = 0; x < u; x++)
    for(size_t c = 0; c < 4; c++)
    {
      if(c == 3 && !affect_alpha) continue;
      size_t index = y * u2 * 4 + x * 4 + c;
      if(c == 3) buffer[index] = (buffer[index] - shift_a) * factor_a;
      else buffer[index] = (buffer[index] - shift) * factor;
    }
  }
  else
  {
    float shift_r = minColor.r;
    float factor_r = 1.0f / (maxColor.r - minColor.r);
    if(maxColor.r == minColor.r) { shift_r = 0.0f; factor_r = 1.0f; }
    float shift_g = minColor.g;
    float factor_g = 1.0f / (maxColor.g - minColor.g);
    if(maxColor.g == minColor.g) { shift_g = 0.0f; factor_g = 1.0f; }
    float shift_b = minColor.b;
    float factor_b = 1.0f / (maxColor.b - minColor.b);
    if(maxColor.b == minColor.b) { shift_b = 0.0f; factor_b = 1.0f; }
    float shift_a = minColor.a;
    float factor_a = 1.0f / (maxColor.a - minColor.a);
    if(maxColor.a == minColor.a) { shift_a = 0.0f; factor_a = 1.0f; }
    for(size_t y = 0; y < v; y++)
    for(size_t x = 0; x < u; x++)
    {
      size_t index = y * u2 * 4 + x * 4;
      buffer[index + 0] = (buffer[index + 0] - shift_r) * factor_r;
      buffer[index + 1] = (buffer[index + 1] - shift_g) * factor_g;
      buffer[index + 2] = (buffer[index + 2] - shift_b) * factor_b;
      if(affect_alpha) buffer[index + 3] = (buffer[index + 3] - shift_a) * factor_a;
    }
  }
}

void normalizeGrey8(unsigned char* buffer, size_t u, size_t v, size_t u2)
{
  int maxColor = 0;
  int minColor = 255;

  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  {
    size_t index = y * u2 + x;
    maxColor = std::max(maxColor, (int)buffer[index]);
    minColor = std::min(minColor, (int)buffer[index]);
  }
  

  double shift = minColor;
  double factor = 1.0 / ((maxColor - minColor) / 255.0);
  if(maxColor == minColor) { shift = 0.0f; factor = 1.0f; }
  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  {
    size_t index = y * u2 + x;
    buffer[index] = (unsigned char)((buffer[index] - shift) * factor);
  }
}

void normalizeGrey32F(float* buffer, size_t u, size_t v, size_t u2)
{
  float limit = std::numeric_limits<float>::max();
  float maxColor = -limit;
  float minColor = limit;

  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  {
    size_t index = y * u2 + x;
    maxColor = std::max(maxColor, buffer[index]);
    minColor = std::min(minColor, buffer[index]);
  }
  

  float shift = minColor;
  float factor = 1.0 / (maxColor - minColor);
  if(maxColor == minColor) { shift = 0.0f; factor = 1.0f; }
  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  {
    size_t index = y * u2 + x;
    buffer[index] = (buffer[index] - shift) * factor;
  }
}




void adjustBrightnessContrastGammaRGBA8(unsigned char* buffer, size_t u, size_t v, size_t u2, double brightness, double contrast, double lgamma, bool affect_alpha)
{
  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  for(size_t c = 0; c < 4; c++)
  {
    if(!affect_alpha && c == 3) continue; //this isn't useful on alpha
    
    size_t index = y * u2 * 4 + x * 4 + c;
    
    double value = buffer[index];
    value += brightness * 255.0;
    
    value -= 128;
    bool neg = value < 0;
    if(neg) value = -value;
    if(contrast < 0) value *= (contrast + 1.0);
    else value += ((127.0 - value) * contrast);
    if(neg) value = -value;
    value += 128;
    
    if(lgamma != 0.0)
    {
      double gamma = std::pow(10.0, -lgamma);
      value /= 255.0;
      value += 0.01; //slight adjustment to make zero values also scale
      value = std::pow(value, gamma);
      value -= 0.01;
      value *= 255.0;
    }
    
    value = lpi::clamp(value, 0.0, 255.0);
    
    buffer[index] = (int)value;
  }
}

void adjustBrightnessContrastGammaRGBA32F(float* buffer, size_t u, size_t v, size_t u2, float brightness, float contrast, float lgamma, bool affect_alpha)
{
  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  for(size_t c = 0; c < 4; c++)
  {
    if(!affect_alpha && c == 3) continue; //this isn't useful on alpha
    
    size_t index = y * u2 * 4 + x * 4 + c;
    
    float value = buffer[index];
    value += brightness;
    
    value -= 0.5;
    bool neg = value < 0;
    if(neg) value = -value;
    if(contrast < 0) value *= (contrast + 1.0);
    else value += ((0.5 - value) * contrast);
    if(neg) value = -value;
    value += 0.5;
    
    if(lgamma != 0.0)
    {
      float gamma = std::pow(10.0f, -lgamma);
      value += 0.01; //slight adjustment to make zero values also scale
      value = std::pow(value, gamma);
      value -= 0.01;
    }
    
    buffer[index] = value;
  }
}
