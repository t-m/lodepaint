/*
LodePaint

Copyright (c) 2009-2010 Lode Vandevenne
All rights reserved.

This file is part of LodePaint.
filters
LodePaint is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LodePaint is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LodePaint.  If not, see <http://www.gnu.org/licenses/>.
*/ 

#pragma once

enum Status
{
  S_PRE,
  S_BUSY,
  S_DONE,
  S_KILLED
};

class Progress
{
  protected:
    double progress; //range 0-1
    bool cancel;
    Status status;
  
  public:
    Progress();
  
    virtual void setProgress(double val); //range 0-1
    virtual double getProgress() const;
    
    //set to true if user wants to cancel. Algorithm should then stop.
    virtual void setUserCancel(bool cancel);
    virtual bool isUserCancel() const;
    
    bool handle(double progress); //sets progress to given progress, sets status to killed and returns true if user pressed cancel, returns false if operation should continue
    bool handle(int i, int n); //calls handle((double)i / (double)n)
    bool handle(int i, int n, int j, int m); //for multipass operations
    
    
    virtual void setStatus(Status status);
    virtual Status getStatus() const;
};

//a NestedProgress represents a small part of its master progress p.
class NestedProgress : public Progress
{
  private:
    Progress* p;
    double p0;
    double p1;
    
  public:
  
    NestedProgress(Progress* p, double p0, double p1);
    
    void setInterval(double p0, double p1);
  
    virtual void setProgress(double val); //range 0-1
    virtual double getProgress() const;
    
    //set to true if user wants to cancel. Algorithm should then stop.
    virtual void setUserCancel(bool cancel);
    virtual bool isUserCancel() const;
    
    virtual void setStatus(Status status);
    virtual Status getStatus() const;
};
