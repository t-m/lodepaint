/*
LodePaint

Copyright (c) 2009-2010 Lode Vandevenne
All rights reserved.

This file is part of LodePaint.

LodePaint is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LodePaint is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LodePaint.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "../lpi/lpi_color.h"

//used by floodfill. Defined here because floodfill uses tolerance and tolerance uses these structs too.
struct RGBA8 { unsigned char f[4]; };
struct RGBA32F { float f[4]; };

inline bool operator==(const RGBA8& a, const RGBA8& b) { return a.f[0]==b.f[0]&&a.f[1]==b.f[1]&&a.f[2]==b.f[2]&&a.f[3]==b.f[3];}
inline bool operator!=(const RGBA8& a, const RGBA8& b) { return a.f[0]!=b.f[0]||a.f[1]!=b.f[1]||a.f[2]!=b.f[2]||a.f[3]!=b.f[3];}
inline bool operator==(const RGBA32F& a, const RGBA32F& b) { return a.f[0]==b.f[0]&&a.f[1]==b.f[1]&&a.f[2]==b.f[2]&&a.f[3]==b.f[3];}
inline bool operator!=(const RGBA32F& a, const RGBA32F& b) { return a.f[0]!=b.f[0]||a.f[1]!=b.f[1]||a.f[2]!=b.f[2]||a.f[3]!=b.f[3];}

 
//the distance formula used for tolerance of floodfill, color replacer, ...
int toldist(const lpi::ColorRGB& a, const lpi::ColorRGB& b, bool tolerance_ignores_alpha);
int toldist(const RGBA8& a, const RGBA8& b, bool tolerance_ignores_alpha);
float toldist(const RGBA32F& a, const RGBA32F& b, bool tolerance_ignores_alpha);


//compare if tolerance of colors is smaller than given tolerance (and avoid calculation of tolerance is 0)
inline bool tolcomp(const lpi::ColorRGB& a, const lpi::ColorRGB& b, int tolerance, bool tolerance_ignores_alpha)
{
  if(tolerance == 0) return a == b;
  return toldist(a, b, tolerance_ignores_alpha) <= tolerance;
}

//compare if tolerance of colors is smaller than given tolerance (and avoid calculation of tolerance is 0)
inline bool tolcomp(const RGBA8& a, const RGBA8& b, int tolerance, bool tolerance_ignores_alpha)
{
  if(tolerance == 0) return a == b;
  return toldist(a, b, tolerance_ignores_alpha) <= tolerance;
}

//compare if tolerance of colors is smaller than given tolerance (and avoid calculation of tolerance is 0)
inline bool tolcomp(const RGBA32F& a, const RGBA32F& b, float tolerance, bool tolerance_ignores_alpha)
{
  if(tolerance == 0) return a == b;
  return toldist(a, b, tolerance_ignores_alpha) <= tolerance;
}
