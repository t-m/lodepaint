/*
LodePaint

Copyright (c) 2009-2010 Lode Vandevenne
All rights reserved.

This file is part of LodePaint.

LodePaint is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LodePaint is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LodePaint.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <vector>

#include "../lpi/lpi_color.h"

using std::size_t;

/*
out = RGBA output buffer (4 unsigned chars per pixel) (must have enough memory allocated already)
ou2 = linewidth of output buffer (can be bigger than output image width)
ou: width of output image in pixels
ov: height of output image in pixels
in = RGBA input buffer (4 unsigned chars per pixel)
iu2 = linewidth of output buffer (can be bigger than input image width)
iu: width of input image in pixels
iv: height of input image in pixels
*/
void rescaleNearestRGBA8(unsigned char* out, size_t ou2, size_t ou, size_t ov, const unsigned char* in, size_t iu2, size_t iu, size_t iv);
void rescaleNearestRGBA32F(float* out, size_t ou2, size_t ou, size_t ov, const float* in, size_t iu2, size_t iu, size_t iv);
void rescaleNearestGrey8(unsigned char* out, size_t ou2, size_t ou, size_t ov, const unsigned char* in, size_t iu2, size_t iu, size_t iv);
void rescaleNearestGrey32F(float* out, size_t ou2, size_t ou, size_t ov, const float* in, size_t iu2, size_t iu, size_t iv);

void rescaleBilinearRGBA8(unsigned char* out, size_t ou2, size_t ou, size_t ov, const unsigned char* in, size_t iu2, size_t iu, size_t iv, bool wrapping);
void rescaleBilinearRGBA32F(float* out, size_t ou2, size_t ou, size_t ov, const float* in, size_t iu2, size_t iu, size_t iv, bool wrapping);
void rescaleBilinearGrey8(unsigned char* out, size_t ou2, size_t ou, size_t ov, const unsigned char* in, size_t iu2, size_t iu, size_t iv, bool wrapping);
void rescaleBilinearGrey32F(float* out, size_t ou2, size_t ou, size_t ov, const float* in, size_t iu2, size_t iu, size_t iv, bool wrapping);

void rescaleRarestRGBA8(unsigned char* out, size_t ou2, size_t ou, size_t ov, const unsigned char* in, size_t iu2, size_t iu, size_t iv, bool wrapping);
void rescaleRarestRGBA32F(float* out, size_t ou2, size_t ou, size_t ov, const float* in, size_t iu2, size_t iu, size_t iv, bool wrapping);
void rescaleRarestGrey8(unsigned char* out, size_t ou2, size_t ou, size_t ov, const unsigned char* in, size_t iu2, size_t iu, size_t iv, bool wrapping);
void rescaleRarestGrey32F(float* out, size_t ou2, size_t ou, size_t ov, const float* in, size_t iu2, size_t iu, size_t iv, bool wrapping);

void rescaleRandomRGBA8(unsigned char* out, size_t ou2, size_t ou, size_t ov, const unsigned char* in, size_t iu2, size_t iu, size_t iv, bool wrapping);

double cubic(double v0, double v1, double v2, double v3, double t);

void rescaleBicubicRGBA8(unsigned char* out, size_t ou2, size_t ou, size_t ov, const unsigned char* in, size_t iu2, size_t iu, size_t iv, bool wrapping);
void rescaleBicubicRGBA32F(float* out, size_t ou2, size_t ou, size_t ov, const float* in, size_t iu2, size_t iu, size_t iv, bool wrapping);
void rescaleBicubicGrey8(unsigned char* out, size_t ou2, size_t ou, size_t ov, const unsigned char* in, size_t iu2, size_t iu, size_t iv, bool wrapping);
void rescaleBicubicGrey32F(float* out, size_t ou2, size_t ou, size_t ov, const float* in, size_t iu2, size_t iu, size_t iv, bool wrapping);


void rescaleSkewedRGBA8(unsigned char* out, size_t ou2, size_t ou, size_t ov, const unsigned char* in, size_t iu2, size_t iu, size_t iv, const lpi::ColorRGB& defaultColor);
void rescaleSkewedRGBA32F(float* out, size_t ou2, size_t ou, size_t ov, const float* in, size_t iu2, size_t iu, size_t iv, const lpi::ColorRGBd& defaultColor);
void rescaleSkewedGrey8(unsigned char* out, size_t ou2, size_t ou, size_t ov, const unsigned char* in, size_t iu2, size_t iu, size_t iv, const lpi::ColorRGB& defaultColor);
void rescaleSkewedGrey32F(float* out, size_t ou2, size_t ou, size_t ov, const float* in, size_t iu2, size_t iu, size_t iv, const lpi::ColorRGBd& defaultColor);


void addRemoveBordersRGBA8(unsigned char* out, size_t ou2, size_t ou, size_t ov
                         , const unsigned char* in, size_t iu2, size_t iu, size_t iv
                         , int top, int left, int bottom, int right
                         , const lpi::ColorRGB& defaultColor);
void addRemoveBordersRGBA32F(float* out, size_t ou2, size_t ou, size_t ov
                         , const float* in, size_t iu2, size_t iu, size_t iv
                         , int top, int left, int bottom, int right
                         , const lpi::ColorRGBd& defaultColor);
void addRemoveBordersGrey8(unsigned char* out, size_t ou2, size_t ou, size_t ov
                         , const unsigned char* in, size_t iu2, size_t iu, size_t iv
                         , int top, int left, int bottom, int right
                         , const lpi::ColorRGB& defaultColor);
void addRemoveBordersGrey32F(float* out, size_t ou2, size_t ou, size_t ov
                         , const float* in, size_t iu2, size_t iu, size_t iv
                         , int top, int left, int bottom, int right
                         , const lpi::ColorRGBd& defaultColor);

