/*
LodePaint

Copyright (c) 2009-2010 Lode Vandevenne
All rights reserved.

This file is part of LodePaint.

LodePaint is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LodePaint is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LodePaint.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <vector>
#include <list>

#include "../lpi/lpi_math2d.h"

/*
Algorithms and data structures for LodePaint for polygons.

The following general remarks hold:

-a contour can have a clockwise or counterclockwise direction. In general when drawing
polygons, the direction does NOT matter, but in some algorithms the direction is used
to detect positive and negative parts. I'm not saying any convention like "CCW is positive".
Instead, you need to find the direction of an original contour, and then after doing some
algorithms on it, use the direction(s) of the new contour(s) and compare them with the
original to decide if the new contours are negative or positive if the original was
interpreted as being positive.

-PolyContour is the main shape that has the intention of being worked with. A single contour
is not good enough because many algorithms on a single contour can give multiple contours
as a result (e.g. after removing self intersections...)

-For most algorithms, contours and polycontours may not have self-intersections. For some algorithms
this is due to limitations and simplifications, for others this is because self intersections don't make sense
to them.

-The mathematical carthesian axes are those of 2D computer graphics: 0,0 is at the top left instead of the bottom left

-A closed contour has as many edges as points. The index of an edge is the index of the first point of it. The edge connecting last and first point, has index of the last point.

-CW means clockwise ON SCREEN, same for CCW. This is the opposite of mathematics, since the Y axis is inverted.
*/


//a contour is a single polygon
struct Contour : public std::vector<lpi::Vector2>
{
  bool closed; //closed means the first and last point are also connected. Not checked by all algorithms, some always work with it as if it's closed (e.g. drawing a polygon)

  Contour() : closed(true){}
};

//a polycontour is multiple contours, so this can be multiple polygons, or a polygon with a hole in it, etc...
struct PolyContour : public std::vector<Contour*>
{
  void cleanup(); //deletes all contours, then clears. This is also called by the destructor. If you don't want the destructor to delete anything, then use clear() before the dtor.
  ~PolyContour();
};


struct IntersectInfo
{
  //first segment
  int contour_index_0; //index of the contour of which the touched line is part
  int line_index_0; //index of the line (= index of the first point of this contour line) that was touched

  //second segment (not always used, e.g. in the sweep functions the second segment is not part of the polygons but another horizontal or vertical line)
  int contour_index_1; //index of the contour of which the touched line is part
  int line_index_1; //index of the line (= index of the first point of this contour line) that was touched

  //exact position
  lpi::Vector2 p; //position of where the line was touched
};

struct SubIntersectInfo : public IntersectInfo
{
  /*

  p0 o----------------o p1 //original edge

       e0   / e1  | e2
  p0 o----/-------|---o p1 //three parts due to two intersections (and the 2 intersections have sub-index 0 and 1)
         /        |
        i0       i1

  */
  int sub_index_0; //index of this intersection point in the segment. That is, if there are e.g. 3 intersection points in this one segment, the one closest to the edge with lowest index gets index 0, the next closest index 1, the farthest index 2
  int sub_index_1; //index of this intersection point in the other segment.
};

bool intersectHLineLineSegment(double& x, double y, double x0, double y0, double x1, double y1);
bool intersectVLineLineSegment(double x, double& y, double x0, double y0, double x1, double y1);

//sweep = drag line through polygon and see which line segments of the polygon are touched and where
void sweepH(std::vector<IntersectInfo>& result, const Contour& contour, double y); //sweep using horizontal line
void sweepV(std::vector<IntersectInfo>& result, const Contour& contour, double x); //sweep using vertical line

void sortSweepH(std::vector<IntersectInfo>& sweep);
void sortSweepV(std::vector<IntersectInfo>& sweep);

/*
Calculates the direction (clockwise or counterclockwise) of a contour.
Returns true for CCW, false for CW
This makes sense mostly on polygons without self intersections, because when there are self intersections some parts in the polygon have another direction than other parts!
If you stand on the line between the first and second point of the shape, and look from the first to the second point, then, the inside part of the polygon is:
-to your left if this funciton returns CCW
-to your right if this function returns CW
The above is true even for self-intersecting polygons: it's the line segment between the first two points that is tested.
*/
bool calcDirection(const Contour& contour);

/*
findSelfIntersections:
finds all edges of this contour that intersect the contour itself.
If two successive edges touch each other in an endpoint, this endpoint is NEVER returned because it's simply the connection of two edges.
If endpoints of edges touch somewhere exactly in another endpoint or exactly on a side of an edge, the behaviour is undefined.
So endpoints aren't really treated by this function, but clear mathematical intersections of two edges are.
If edges share a complete line-part, that is also not treated by this function. It only gives intersections that result in a point.
*/
void findSelfIntersections(std::vector<IntersectInfo>& result, const Contour& contour);

/*
Turn one self intersecting polygon into multiple non self-intersecting polygons. The input contour is considered to be closed (even if bool closed is false)
*/
void removeSelfIntersections(PolyContour& result, const Contour& contour);

/*
groupSubContoursByDirection: first removes self intersections.
In the resulting sub-contours, it splits them in two groups: those that are ccw and those that are cw.

This is useful for two things:
  -to have a different way of drawing a contour with self intersections than the alternating way
  -for using after doing offsetBasic, to remove unwanted parts
*/
void groupSubContoursByDirection(PolyContour& resultcw, PolyContour& resultccw, const Contour& contour);

/*
offsetBasic: offsets, but doesn't create 100% the result you want if the offset contour has self intersections.
After offsetBasic is done, self intersections should be removed from the result and parts with wrong ccw direction after that discarded.
This offset method only supports input contours that have no self-intersections.
*/
void offsetBasic(Contour& result, const Contour& contour, double amount);

/*
Other names for "offset operation":
-in GIS: buffer (possible with negative distance)
-in CAD/CAM: offset curves
-erosion and dilation
-mathematical: Minkowski sum with disk of radius equal to the buffer distance
*/
void offset(PolyContour& result, const Contour& contour, double amount);

//removeDuplicates removes all points from the contour where two points in a row have the same coordinates
void removeDuplicates(Contour& result, const Contour& contour);


void getContourBounds(double& x0, double& y0, double& x1, double& y1, const Contour& contour);
void getContourBounds(int& x0, int& y0, int& x1, int& y1, const int* xs, const int* ys, size_t num);
void getContourBounds(double& x0, double& y0, double& x1, double& y1, const PolyContour& poly);

void intsToContour(Contour& contour, const int* xs, const int* ys, size_t num);
void contourToInts(std::vector<int>& xs, std::vector<int>& ys, const Contour& contour);

#define ENABLE_CONTOUR_UNITTEST

#if defined(ENABLE_CONTOUR_UNITTEST)
void doContourUnitTest();
#endif





