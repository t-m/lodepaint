/*
LodePaint

Copyright (c) 2009-2010 Lode Vandevenne
All rights reserved.

This file is part of LodePaint.

LodePaint is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LodePaint is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LodePaint.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "contour.h"


#include <algorithm>
#include <iostream>

#include "../lpi/lpi_math.h"

void PolyContour::cleanup()
{
  for(size_t i = 0; i < size(); i++) delete operator[](i);
  clear();
}

PolyContour::~PolyContour()
{
  cleanup();
}

bool intersectHLineLineSegment(double& x, double y, double x0, double y0, double x1, double y1)
{
  if(y0 > y1) { std::swap(x0, x1); std::swap(y0, y1); } //this is needed for having always the same behaviour, meant for semi translucent triangles that touch each other (having no overlap and no 1-pixel gaps between two triangles)
  if(y < std::min(y0, y1)) return false;
  if(y >= std::max(y0, y1)) return false;
  if(y1 == y0) return false;
  x = ((y - y0) * (x1 - x0)) / (y1 - y0) + x0;
  return true;
}

bool intersectVLineLineSegment(double x, double& y, double x0, double y0, double x1, double y1)
{
  if(x0 > x1) { std::swap(x0, x1); std::swap(y0, y1); } //this is needed for having always the same behaviour, meant for semi translucent triangles that touch each other (having no overlap and no 1-pixel gaps between two triangles)
  if(x < std::min(x0, x1)) return false;
  if(x >= std::max(x0, x1)) return false;
  if(x1 == x0) return false;
  y = ((x - x0) * (y1 - y0)) / (x1 - x0) + y0;
  return true;
}

void sweepH(std::vector<IntersectInfo>& result, const Contour& contour, double y)
{
  for(size_t i = 0; i < contour.size(); i++)
  {
    size_t i1 = i + 1;
    if(i1 == contour.size())
    {
      if(contour.closed) i1 = 0;
      else break;
    }
    double x = 0;
    if(intersectHLineLineSegment(x, y, contour[i].x, contour[i].y, contour[i1].x, contour[i1].y))
    {
      result.resize(result.size() + 1);
      result.back().contour_index_0 = 0;
      result.back().line_index_0 = i;
      result.back().p = lpi::Vector2(x, y);
    }
  }
}

void sweepV(std::vector<IntersectInfo>& result, const Contour& contour, double x)
{
  for(size_t i = 0; i < contour.size(); i++)
  {
    size_t i1 = i + 1;
    if(i1 == contour.size())
    {
      if(contour.closed) i1 = 0;
      else break;
    }
    double y = 0;
    if(intersectVLineLineSegment(x, y, contour[i].x, contour[i].y, contour[i1].x, contour[i1].y))
    {
      result.resize(result.size() + 1);
      result.back().contour_index_0 = 0;
      result.back().line_index_0 = i;
      result.back().p = lpi::Vector2(x, y);
    }
  }
}

static bool mySortSweepH(const IntersectInfo& i, const IntersectInfo& j) { return (i.p.x < j.p.x); }
static bool mySortSweepV(const IntersectInfo& i, const IntersectInfo& j) { return (i.p.y < j.p.y); }


void sortSweepH(std::vector<IntersectInfo>& sweep)
{
  std::sort(sweep.begin(), sweep.end(), mySortSweepH);
}

void sortSweepV(std::vector<IntersectInfo>& sweep)
{
  std::sort(sweep.begin(), sweep.end(), mySortSweepV);
}

static bool mySortSweepH2(const SubIntersectInfo& i, const SubIntersectInfo& j) { return (i.p.x < j.p.x); }
static bool mySortSweepV2(const SubIntersectInfo& i, const SubIntersectInfo& j) { return (i.p.y < j.p.y); }


void sortSweepH(std::vector<SubIntersectInfo>& sweep)
{
  std::sort(sweep.begin(), sweep.end(), mySortSweepH2);
}

void sortSweepV(std::vector<SubIntersectInfo>& sweep)
{
  std::sort(sweep.begin(), sweep.end(), mySortSweepV2);
}

template<typename T>
void revertVectorOrder(std::vector<T>& v)
{
  for(size_t i = 0; i < v.size() / 2; i++)
  {
    std::swap(v[i], v[v.size() - i - 1]);
  }
}

/*
returns whether this edge is more horizontal, or more vertical. This is for usage
in cases where you want the highest precision (horizontal if the x's are farthest
apart, vertical if the y's are farthest apart.)
Returns true if the edge is more horizontal, false if it's more vertical.
*/
bool isMoreHorizontal(const lpi::Vector2& p0, const lpi::Vector2& p1)
{
  double xdist = lpi::template_abs(p0.x - p1.x);
  double ydist = lpi::template_abs(p0.y - p1.y);
  return xdist > ydist;
}

bool calcDirection(const Contour& contour)
{
  if(contour.size() < 3) return false;
  std::vector<IntersectInfo> sweep;

  const lpi::Vector2& p0 = contour[0];
  const lpi::Vector2& p1 = contour[1];

  //for best precision, take direction with highest precision result (X or Y)
  bool hor = isMoreHorizontal(p0, p1); //If hor is true, the segment is more horizontal, so the sweep will be done vertically
  if(hor)
  {
    double xav = (p0.x + p1.x) / 2;
    sweepV(sweep, contour, xav);
    sortSweepV(sweep);
  }
  else
  {
    double yav = (p0.y + p1.y) / 2;
    sweepH(sweep, contour, yav);
    sortSweepH(sweep);
  }
  if(sweep.size() > 1)
  {
    for(size_t i = 0; i < sweep.size(); i++)
    {
      if(sweep[i].line_index_0 == 0)
      {
        /*
        if our line segment (the one with index 0, p0-p1), is at even position in the sweep, the filled part is to the left of us (if standing in p0 and looking at p1) so the direction is CCW, otherwise the direction is CW.


    -------------------------------------> X
    |
    |       ******************
    |       *                *
    |       *                *
    |       *    *******     *
    |       *    *     *     *
    |       0    0     *     *
    |  .....*....*.....*.....*......> "sweepH"
    |       1    1     *     *
    |       *    *     *     *
    |       ******     *******
    |
    v
    Y
         Check the two "0 1" cases in the picture above. The left case is CCW, the right case is CW.
         The ...... is the sweep line (going horizontally from left to right and intersecting the polygon 4 times)
         The left 01 case is an even index (index 0) of intersection of sweep line,
         the right 01 case is an odd index (index 1) of an intersection of the sweep line
         If 1 is above 0, the opposite is true of course.
        */
        bool above = hor ? (p0.x > p1.x) : (p0.y < p1.y);
        if(above) return (i % 2) == 0;
        else return (i % 2) == 1;
      }
    }
  }


  return false; //invalid case, just return something...
}


//Only works on closed contours for now.
void findSelfIntersections(std::vector<IntersectInfo>& result, const Contour& contour)
{
  //TODO: optimizations to avoid the quadratic algorithm, e.g. allocating lines in certain regions, ...

  lpi::Vector2 p;

  for(size_t i = 0; i < contour.size(); i++)
  for(size_t j = i + 1; j < contour.size(); j++)
  {
    size_t i1 = i + 1; if(i1 == contour.size()) i1 = 0;
    size_t j1 = j + 1; if(j1 == contour.size()) j1 = 0;

    //check that the edges aren't neighbors. Neighbor edges may not be treated because otherwise the point they share is often returned as an intersection, which is NOT so!!
    if(i == j + 1 || i + 1 == j || (j == 0 && i == contour.size() - 1) || (i == 0 && j == contour.size() - 1)) continue;

    bool yes = !lpi::linesParallel(contour[i], contour[i1], contour[j], contour[j1]) && lpi::intersectLineSegmentLineSegment(p, contour[i], contour[i1], contour[j], contour[j1]);
    if(yes)
    {
      result.resize(result.size() + 1);
      result.back().contour_index_0 = 0;
      result.back().line_index_0 = i;
      result.back().contour_index_1 = 0;
      result.back().line_index_1 = j;
      result.back().p = p;
    }
  }
}

/*
This creates a different (and more usable for some algorithms) datastructure with the self intersections of the contour.
Input parameters are the contour and the result from findSelfIntersections.
Output parameter is a vector of the same size as the contour, with in it for each edge an std::vector of SubIntersectInfos.
And the SubIntersectInfos of each edge are sorted from closest to start point of edge to closest to end point of it, and that are also the index values of sub_index_0 and sub_index_1 values in SubIntersectInfo.
In the SubIntersectInfos, it's also so that each index_0 will refer to this edge, index_1 to the other edge (and all SubIntersectInfos are actually there twice, but with these indices swapped)

Only works on closed contours for now.
*/
void postprocesSelfIntersections(std::vector<std::vector<SubIntersectInfo> >& result, const std::vector<IntersectInfo>& intersections, const Contour& contour)
{
  (void)result; (void)intersections; (void)contour;

  result.resize(contour.size());
  for(size_t i = 0; i < intersections.size(); i++)
  {
    std::vector<SubIntersectInfo>& va = result[intersections[i].line_index_0];
    std::vector<SubIntersectInfo>& vb = result[intersections[i].line_index_1];

    va.resize(va.size() + 1);
    vb.resize(vb.size() + 1);

    SubIntersectInfo& a = va.back();
    SubIntersectInfo& b = vb.back();


    const IntersectInfo& r = intersections[i];

    a.line_index_0 = r.line_index_0;
    a.line_index_1 = r.line_index_1;
    a.p = r.p;

    b.line_index_0 = r.line_index_1;
    b.line_index_1 = r.line_index_0;
    b.p = r.p;
  }
  //and now the sub-indices
  for(size_t i = 0; i < contour.size(); i++)
  {
    size_t i1 = i + 1;
    if(i1 == contour.size())
    {
      if(contour.closed) i1 = 0;
      else break;
    }

    const lpi::Vector2& p0 = contour[i];
    const lpi::Vector2& p1 = contour[i1];

    bool hor = isMoreHorizontal(p0, p1);

    std::vector<SubIntersectInfo>& subs = result[i];

    if(hor)
    {
      sortSweepH(subs);
      if(p1.x < p0.x) revertVectorOrder(subs);
    }
    else
    {
      sortSweepV(subs);
      if(p1.y < p0.y) revertVectorOrder(subs);
    }

    for(size_t j = 0; j < subs.size(); j++) //fill in the values for sub_index_0 and sub_index_1
    {
      subs[j].sub_index_0 = j;
      int other_line_index = subs[j].line_index_1;
      std::vector<SubIntersectInfo>& subs2 = result[other_line_index];
      for(size_t k = 0; k < subs2.size(); k++)
      {
        if(subs2[k].line_index_1 == (int)i) subs2[k].sub_index_1 = j;
      }
    }
  }
}


namespace
{
  //used purely in the removeSelfIntersections function, but C++ doesn't want to use a local type as template argument...
  struct SubSegment
  {
    int line_index; //index of the segment in the input contour
    int sub_index; //index of the part of the segment (it's divided in parts by intersections) (index 0 is the subsegment touching the first point of the segment. index 1 is that of sub-intersection 0, index 2 of sub-intersection 1, etc...)
  };
}

void removeDuplicates(Contour& result, const Contour& contour)
{
  if(contour.empty()) return;

  result.push_back(contour[0]);
  for(size_t i = 1; i < contour.size(); i++)
  {
    if(contour[i] != result.back()) result.push_back(contour[i]);
  }
}


/*
removeSelfIntersections

This function gets a closed contour as input, and at the output can have one or more contours (a polycontour)

This function removeSelfIntersections has a special rule. The other function removeSelfIntersections2
has a different special rule.

The special rule of removeSelfIntersections is: All segments of the returned contours, have the same direction
as they had in the input contour. This rule allows detecting from returned sub-contours whether they are clockwise
or counterclockwise, allowing eliminating some in certain scenarios (such as in a result in the offset method), but
it also means the returned contours don't go nicely around "filled" areas of the self-intersecting input contour.

Example:

 0-------1
  \    /
   \  /
    \/
    /\ A
   /  \
  /    \
 2-------3

  In the example above, the input is one polygon with 4 points 0,1,2,3), the output is two polygons with each 3 points (they both have point A)

  More complex example:


       9************************8
        *                      *
        *     2         3      *
        *     ***********      *
        *     *         *      *
       C*     *B        *      *
 5***********************      *
  *     *     *         4      *
  *     *     *                *
 6******************************7
       D*     *A
        *     *
        *******
        0     1

  In the example above, the input is a self intersecting polygon with 10 points, the output is 3 polygons.
  The output is NOT 4 or 5 polygons. The output is the following polygons:

  01A789C56D, ABCD, B234

  Algorithm:

  1) find all the self intersections

  2) put all the edges of the polygon in an extra data structure where each edge gets the following information:
     a list of intersections in which the edge is involved, can be 0, 1 or more, with each intersection being a IntersectInfo

  3) Split up the one closed contour into multiple open and/or closed contours, with new points added to some
     of the contours, namely the intersection points.

  4) From the result of 3, or while doing 3 (e.g. by working recursively during it), find the new sub-contours.
*/
void removeSelfIntersections(PolyContour& result, const Contour& contour)
{
  if(contour.empty()) return;

  ///1)

  std::vector<IntersectInfo> intersections;
  findSelfIntersections(intersections, contour);

  ///2)

  std::vector<std::vector<SubIntersectInfo> > post;
  postprocesSelfIntersections(post, intersections, contour);

  std::vector<std::vector<int> > used(contour.size());
  for(size_t i = 0; i < contour.size(); i++)
  {
    used[i].resize(post[i].size() + 1);
    for(size_t j = 0; j < used[i].size(); j++)
      used[i][j] = 0; //not yet used
  }


  ///3)

  std::vector<SubSegment> todos; //stack of sub-segments to-do

  /*
  Go throug the segments in order. If one with intersection is encountered, there are 3 possible sub-segments to take,
  of which one should NOT be taken (the one in our direction), but left or right can be taken (free to choose which...).
  Put the other two on the sub-segments-to-do stack.
  When start of our new contour is reached, look in the todo stack, and start creating another new subcontour with it, etc...
  Also use the "used" flags to mark segments as done, these are not added to the stack anymore.
  --> When given a choice of paths from a certain intersection point, pick one such that the indexes of the edges go up, not down. This to keep the cw or ccw directions of new subcontours intact.
  */

  /*
  Example traverse of the while-loop below

p3*********************p2
  *                   *s2
  *             s1    *B   s0
  *     p7*********************p6
  *       *           *       *
  *       *s0         *s1     *
  *   s0  *    s1     *   s2  *
p4*****************************p5
         C*           *A
          *s1         *s0
          *           *
          *************
         p0           p1

  The picture above shows a polygon of the points p0-p7
  There are 3 intersection points found: A, B and C
  Some segments are divided into subsegments. These subsegments are numbered s0, s1, ...
  E.G. the edge 1 (the one going from p1 to p2) has 3 subsegments s0, s1, s2.
  The algorithm will traverse this as follows:
    p0
    p1s0
    Save p1s1 on TODO list
    p4s2
    p5
    p6s0
    Save p6s1 on TODO list
    p1s2
    p2
    p3
    p4s0
    Save p4s1 on TODO list
    p7s1
    And we're back at p0 so this contour is complete

    Read p4s1 from TODO list
    p4s1
    Don't save p4s2 on TODO list, it's already marked as used
    p1s1
    Don't save p1s2 on TODO list, it's already marked as used
    p6s1
    p7s0
    Don't save p7s1 on TODO list, it's already marked as used
    And we're back at p4s1 so this contour is complete

    Read p6s1 from TODO list, but it's already used so nothing to do
    Read p1s1 from TODO list, but it's already used so nothing to do

    Done! The parts are two contours

    In the case below, it *must* return 3 contours. And two of them are CCW, the center one CW.

   4*********3
     *     *
      *   *
       * *
        *B
       * *
      *   *
    2*     *5
      *   *
       * *
        *A
       * *
      *   *
     *     *
   0*********1

  */
  PolyContour parts;
  parts.push_back(new Contour);
  int line_index = 0;
  int sub_index = 0; //index of sub SEGMENT. There are one less sub INTERSECTIONS. If sub_index is 0, there is no matching intersection index. For sub_index > 0, the intersection index is sub_index - 1. Intersection index refers to the sub_index members of SubIntersectInfo.
  bool done = false;

  while(!done) //each time the loop starts, there must be a valid line_index and sub_index
  {
    if(used[line_index][sub_index] == 0)
    {
      if(sub_index == 0) parts.back()->push_back(contour[line_index]);
      else parts.back()->push_back(post[line_index][sub_index - 1].p);

       used[line_index][sub_index] = 1;

      if(sub_index >= (int)post[line_index].size()) //this edge has no intersections OR we are on the last sub-edge of an edge with intersections
      {
        line_index++;
        if(line_index >= (int)contour.size()) line_index = 0;
        sub_index = 0;
      }
      else //we're working on an edge that has intersections
      {
        todos.resize(todos.size() + 1);

        todos.back().line_index = line_index;
        todos.back().sub_index = sub_index + 1;
        if(todos.back().sub_index >= (int)post[line_index].size()) //we may be on an edge with intersections currently, but, the next point for the todo isn't an intersection point
        {
          todos.back().sub_index = 0;
          todos.back().line_index++;
          if(todos.back().line_index >= (int)contour.size()) todos.back().line_index = 0;
        }
        SubIntersectInfo& subint = post[line_index][sub_index];
        line_index = subint.line_index_1;
        sub_index = subint.sub_index_1 + 1; //plus one because sub-segment index is one higher than sub-intersection-point index
      }
    }
    else //we reached a line segment that is used. This means we finished a contour.
    {
      while(!todos.empty() && used[todos.back().line_index][todos.back().sub_index] != 0)
      {
        todos.pop_back(); //that todo is already processed in the meantime, discard it
      }
      if(todos.empty())
      {
        done = true;
        break;
      }
      else
      {
        parts.push_back(new Contour);

        SubSegment subsegment = todos.back();
        todos.pop_back();

        line_index = subsegment.line_index;
        sub_index = subsegment.sub_index;
      }
    }
  }

  result.swap(parts);
}


/*
This function removeSelfIntersections2 has a special rule. The other function removeSelfIntersections
has a different special rule.

First read the explanation of removeSelfIntersections. Then replace its special rule by the special
rule of this function described below.

Rule:

If you have a polygon like this:

       9************************8
        *######################*
        *#####2#########3######*
        *#####***********######*
        *#####*         *######*
       C*#####*B        *######*
 5***********************######*
  *#####*     * ########4######*
  *#####*     *################*
 6******************************7
       D*#####*A
        *#####*
        *******
        0     1

And you'd paint it on screen with a sweep algo, then the areas filled with #,
become filled pixels on screen, while white areas are "empty".

The rule says that the returned contours by removeSelfIntersections2, are
all contours around a single such filled area. There are no contours returned around
an empty area and no contours around multiple filled areas at once. So the 3 contours
returned for the above shape are:

01DA, A789CB, 56DC

However the order (cw or ccw) isn't specified, and some edges may get the opposite order.

This also means that the following self-intersecting polygon will have one single polygon
as a result, instead of two!


6*************5
*             *
*             *
*    2****1   *
*    *    *   *
*    *    *   *
*    3****A***4
*         *
*         *
7*********0

The result is 0A321A4567 (the fact that point A is included twice is NOT a self-intersection, it merely touches its own border)

(for the other removeSelfIntersections2- function, the result would be two contours, 0A4567 and A123)

*/
void removeSelfIntersections2(PolyContour& result, const Contour& contour)
{
  (void)result; (void)contour;
  //TODO! This version of the function is needed to process the input of the offset function, but NOT to process the output of the offsetBasic function, that one needs the other removeSelfIntersections function.
  //What there is todo: there where an intersection is enountered in the algo below, there is a choice between two segments to follow.
  //Instead of choosing the one with incrementing index, now choose the one that follows the "filled" side of the current sub-polygon we're following.

//  ///1)
//
//  std::vector<IntersectInfo> intersections;
//  findSelfIntersections(intersections, contour);
//
//  ///2)
//
//  std::vector<std::vector<SubIntersectInfo> > post;
//  postprocesSelfIntersections(post, intersections, contour);
//
//  std::vector<std::vector<int> > used(contour.size());
//  for(size_t i = 0; i < contour.size(); i++)
//  {
//    used[i].resize(post[i].size() + 1);
//    for(size_t j = 0; j < used[i].size(); j++)
//      used[i][j] = 0; //not yet used
//  }
//
//
//  ///3)
//
//  std::vector<SubSegment> todos; //stack of sub-segments to-do
//
//  PolyContour parts;
//  parts.push_back(new Contour);
//  int line_index = 0;
//  int sub_index = 0; //index of sub SEGMENT. There are one less sub INTERSECTIONS. If sub_index is 0, there is no matching intersection index. For sub_index > 0, the intersection index is sub_index - 1. Intersection index refers to the sub_index members of SubIntersectInfo.
//  bool done = false;
//
//  while(!done) //each time the loop starts, there must be a valid line_index and sub_index
//  {
//    if(used[line_index][sub_index] == 0)
//    {
//      if(sub_index == 0) parts.back()->push_back(contour[line_index]);
//      else parts.back()->push_back(post[line_index][sub_index - 1].p);
//
//       used[line_index][sub_index] = 1;
//
//      if(sub_index >= (int)post[line_index].size()) //this edge has no intersections OR we are on the last sub-edge of an edge with intersections
//      {
//        line_index++;
//        if(line_index >= (int)contour.size()) line_index = 0;
//        sub_index = 0;
//      }
//      else //we're working on an edge that has intersections
//      {
//        todos.resize(todos.size() + 1);
//
//        todos.back().line_index = line_index;
//        todos.back().sub_index = sub_index + 1;
//        if(todos.back().sub_index >= (int)post[line_index].size()) //we may be on an edge with intersections currently, but, the next point for the todo isn't an intersection point
//        {
//          todos.back().sub_index = 0;
//          todos.back().line_index++;
//          if(todos.back().line_index >= (int)contour.size()) todos.back().line_index = 0;
//        }
//        SubIntersectInfo& subint = post[line_index][sub_index];
//        line_index = subint.line_index_1;
//        sub_index = subint.sub_index_1 + 1; //plus one because sub-segment index is one higher than sub-intersection-point index
//      }
//    }
//    else //we reached a line segment that is used. This means we finished a contour.
//    {
//      while(!todos.empty() && used[todos.back().line_index][todos.back().sub_index] != 0)
//      {
//        todos.pop_back(); //that todo is already processed in the meantime, discard it
//      }
//      if(todos.empty())
//      {
//        done = true;
//        break;
//      }
//      else
//      {
//        parts.push_back(new Contour);
//
//        SubSegment subsegment = todos.back();
//        todos.pop_back();
//
//        line_index = subsegment.line_index;
//        sub_index = subsegment.sub_index;
//      }
//    }
//  }
//
//  result.swap(parts);
}

void groupSubContoursByDirection(PolyContour& resultcw, PolyContour& resultccw, const Contour& contour)
{
  PolyContour poly;
  removeSelfIntersections(poly, contour);
  for(size_t i = 0; i < poly.size(); i++)
  {
    if(calcDirection(*poly[i])) resultccw.push_back(poly[i]);
    else resultcw.push_back(poly[i]);
  }
  poly.clear(); //clear it, otherwise its destructor deletes all members while they're now in the other two PolyContours!
}

void getContourBounds(double& x0, double& y0, double& x1, double& y1, const Contour& contour)
{
  if(contour.empty()) return;

  x0 = contour[0].x;
  y0 = contour[0].y;
  x1 = contour[0].x;
  y1 = contour[0].y;

  for(size_t i = 1; i < contour.size(); i++)
  {
    x0 = std::min(x0, contour[i].x);
    y0 = std::min(y0, contour[i].y);
    x1 = std::max(x1, contour[i].x);
    y1 = std::max(y1, contour[i].y);
  }
}

void getContourBounds(double& x0, double& y0, double& x1, double& y1, const PolyContour& poly)
{
  if(poly.empty()) return;
  //if(poly[0]->empty()) return;

  x0 = 1e308;//(*poly[0])[0].x;
  y0 = 1e308;//(*poly[0])[0].y;
  x1 = -1e308;//(*poly[0])[0].x;
  y1 = -1e308;//(*poly[0])[0].y;

  for(size_t j = 0; j < poly.size(); j++)
  {
    const Contour& contour = *poly[j];
    for(size_t i = 0; i < contour.size(); i++)
    {
      x0 = std::min(x0, contour[i].x);
      y0 = std::min(y0, contour[i].y);
      x1 = std::max(x1, contour[i].x);
      y1 = std::max(y1, contour[i].y);
    }
  }
}

void getContourBounds(int& x0, int& y0, int& x1, int& y1, const int* xs, const int* ys, size_t num)
{
  if(num <= 0) return;

  x0 = xs[0];
  y0 = ys[0];
  x1 = xs[0];
  y1 = ys[0];

  for(size_t i = 1; i < num; i++)
  {
    x0 = std::min(x0, xs[i]);
    y0 = std::min(y0, ys[i]);
    x1 = std::max(x1, xs[i]);
    y1 = std::max(y1, ys[i]);
  }
}

void intsToContour(Contour& contour, const int* xs, const int* ys, size_t num)
{
  contour.resize(num);

  for(size_t i = 0; i < num; i++)
  {
    contour[i].x = xs[i] + 0.5;
    contour[i].y = ys[i] + 0.5;
  }
}

void contourToInts(std::vector<int>& xs, std::vector<int>& ys, const Contour& contour)
{
  xs.resize(contour.size());
  ys.resize(contour.size());

  for(size_t i = 0; i < contour.size(); i++)
  {
    xs[i] = (int)(contour[i].x);
    ys[i] = (int)(contour[i].y);
    //TODO: avoid doubles (coordinats that are in the same pixel)
  }
}

/*
offsetBasic: offsets, but doesn't create 100% the result you want if the offset contour has self intersections.
After offsetBasic is done, self intersections should be removed and parts with wrong ccw direction after that discarded.
This offset method only supports input contours that have no self-intersections.
*/
void offsetBasic(Contour& result, const Contour& contour, double amount)
{
  double x0 = 0, y0 = 0, x1 = 0, y1 = 0;
  getContourBounds(x0, y0, x1, y1, contour);
  //maximum bounds of the offset contour
  double bx0 = x0 + (amount);
  double by0 = y0 + (amount);
  double bx1 = x1 - (amount);
  double by1 = y1 - (amount);
  if(bx0 >= bx1 || by0 >= by1) return; //offset as big as whole polygon
  //bounds outside polygon for error detection (see below)
  /*double ax0 = x0 - (amount - 1);
  double ay0 = y0 - (amount - 1);
  double ax1 = x1 + (amount - 1);
  double ay1 = y1 + (amount - 1);*/

  bool ccw = calcDirection(contour);

  std::vector<lpi::Vector2> normals;//normals towards the inside of each segment

  for(int i = 0; i < (int)contour.size(); i++)
  {
    int j = lpi::wrap(i + 1, 0, contour.size());
    lpi::Vector2 d = contour[j] - contour[i];
    normals.push_back(ccw ? lpi::Vector2(d.y, -d.x) : lpi::Vector2(-d.y, d.x));
    normals.back().normalize();
  }

  for(int i = 0; i < (int)contour.size(); i++)
  {
    int j = lpi::wrap(i + 1, 0, contour.size());
    int h = lpi::wrap(i - 1, 0, contour.size());

    //segment a
    lpi::Vector2 pa0 = contour[i] + amount * normals[i];
    lpi::Vector2 pa1 = contour[j] + amount * normals[i];
    //segment b
    lpi::Vector2 pb0 = contour[h] + amount * normals[h];
    lpi::Vector2 pb1 = contour[i] + amount * normals[h];

    lpi::Vector2 intersection;

    if(lpi::linesParallel(pa0, pa1, pb0, pb1))
    {
      intersection = pa0;
    }
    else
    {
      lpi::intersectLineLine(intersection, pa0, pa1, pb0, pb1);
    }

    //disallow points that are clearly wrong (the algorithm has a few problems still)
    /*bool allow = true;
    if(intersection.x < ax0 || intersection.x > ax1 || intersection.y < ay0 || intersection.y > ay1) allow = false;*/

    /*if(allow) */result.push_back(intersection);
  }
}

void offset(PolyContour& result, const Contour& contour, double amount)
{
  Contour contour2;
  removeDuplicates(contour2, contour);
  PolyContour poly;
  removeSelfIntersections(poly, contour2);

  for(size_t i = 0; i < poly.size(); i++)
  {
    const Contour& outer = *poly[i];
    //result.push_back(new Contour);
    //Contour& inner = *result.back();

    bool out_ccw = calcDirection(outer);

    Contour inner; //"inner" in case of offset towards the inside, if the offset is to the outside then this name is wrong of course...
    offsetBasic(inner, outer, amount);

    PolyContour inners;
    removeSelfIntersections(inners, inner);

    for(size_t j = 0; j < inners.size(); j++)
    {
      Contour& inn = *inners[j];
      bool inn_ccw = calcDirection(inn);
      if(inn_ccw == out_ccw && !inn.empty()) result.push_back(&inn);
    }

    inners.clear();
  }
}

#if defined(ENABLE_CONTOUR_UNITTEST)

#define LUT_MY_RESET \
{\
}

#include "../lpi/lpi_unittest.h"


bool pointsNear(const lpi::Vector2& a, const lpi::Vector2& b, double delta = 0.01)
{
  return std::fabs(a.x - b.x) < delta && std::fabs(a.y - b.y) < delta;
}

bool onePointNear(const lpi::Vector2& a, const std::vector<lpi::Vector2>& b, double delta = 0.01)
{
  for(size_t i = 0; i < b.size(); i++) if(pointsNear(a, b[i], delta)) return true;
  return false;
}

void doContourUnitTest()
{
  Contour contour0; //simple square without intersections
  contour0.push_back(lpi::Vector2(0,0));
  contour0.push_back(lpi::Vector2(0,1));
  contour0.push_back(lpi::Vector2(1,1));
  contour0.push_back(lpi::Vector2(1,0));

  Contour contour1; //two intersections: edge 0 with edge 2 and edge 4 with edge 2
  contour1.push_back(lpi::Vector2(0,0));   //        0/\           .
  contour1.push_back(lpi::Vector2(1,4));   //  3     /  \     2    .
  contour1.push_back(lpi::Vector2(2,2));   //   \---/----\---/     .
  contour1.push_back(lpi::Vector2(-2,2));  //    \ /      \ /      .
  contour1.push_back(lpi::Vector2(-1,4));  //     4        1       .

  Contour contour2; //special contour with 4 intersections (A, B, C and D in the picture below)
  contour2.push_back(lpi::Vector2(0,0));   //
  contour2.push_back(lpi::Vector2(1,0));   //      0---1
  contour2.push_back(lpi::Vector2(1,3));   //      |   |
  contour2.push_back(lpi::Vector2(2,3));   //  6---A---B-------7
  contour2.push_back(lpi::Vector2(2,2));   //  |   |   |       |
  contour2.push_back(lpi::Vector2(-1,2));  //  5---D---C---4   |
  contour2.push_back(lpi::Vector2(-1,1));  //      |   |   |   |
  contour2.push_back(lpi::Vector2(3,1));   //      |   2---3   |
  contour2.push_back(lpi::Vector2(3,4));   //      |           |
  contour2.push_back(lpi::Vector2(0,4));   //      9-----------8


  LUT_START_UNIT_TEST

  LUT_CASE("calcDirection")
  {
    using namespace lpi;

    /*the test polygons are a square in both direction and with the 0 edge once in every corner, and a triangle in both directions and with the 0 edge once in every corner
      the indices in the name are: first digit: 0 square, 1 triangle. Second digit: direction: 0 cw, 1 ccq. Third digit: which corner is the 0.
      square:                triangle:
        (0,0)---(1,0)           (0,0)
         |        |             /  \
         |        |            /  _(1,1)
        (0,1)---(1,1)       (-1,2)
    */
    Contour c000; c000.push_back(Vector2(0,0)); c000.push_back(Vector2(1,0)); c000.push_back(Vector2(1,1)); c000.push_back(Vector2(0,1));
    Contour c001; c001.push_back(Vector2(1,0)); c001.push_back(Vector2(1,1)); c001.push_back(Vector2(0,1)); c001.push_back(Vector2(0,0));
    Contour c002; c002.push_back(Vector2(1,1)); c002.push_back(Vector2(0,1)); c002.push_back(Vector2(0,0)); c002.push_back(Vector2(1,0));
    Contour c003; c003.push_back(Vector2(0,1)); c003.push_back(Vector2(0,0)); c003.push_back(Vector2(1,0)); c003.push_back(Vector2(1,1));
    Contour c010; c010.push_back(Vector2(0,0)); c010.push_back(Vector2(0,1)); c010.push_back(Vector2(1,1)); c010.push_back(Vector2(1,0));
    Contour c011; c011.push_back(Vector2(0,1)); c011.push_back(Vector2(1,1)); c011.push_back(Vector2(1,0)); c011.push_back(Vector2(0,0));
    Contour c012; c012.push_back(Vector2(1,1)); c012.push_back(Vector2(1,0)); c012.push_back(Vector2(0,0)); c012.push_back(Vector2(0,1));
    Contour c013; c013.push_back(Vector2(1,0)); c013.push_back(Vector2(0,0)); c013.push_back(Vector2(0,1)); c013.push_back(Vector2(1,1));

    Contour c100; c100.push_back(Vector2(0,0)); c100.push_back(Vector2(1,1)); c100.push_back(Vector2(-1,2));
    Contour c101; c101.push_back(Vector2(1,1)); c101.push_back(Vector2(-1,2)); c101.push_back(Vector2(0,0));
    Contour c102; c102.push_back(Vector2(-1,2)); c102.push_back(Vector2(0,0)); c102.push_back(Vector2(1,1));
    Contour c110; c110.push_back(Vector2(1,1)); c110.push_back(Vector2(0,0)); c110.push_back(Vector2(-1,2));
    Contour c111; c111.push_back(Vector2(0,0)); c111.push_back(Vector2(-1,2)); c111.push_back(Vector2(1,1));
    Contour c112; c112.push_back(Vector2(-1,2)); c112.push_back(Vector2(1,1)); c112.push_back(Vector2(0,0));

    LUT_SUB_ASSERT_FALSE(calcDirection(c000));
    LUT_SUB_ASSERT_FALSE(calcDirection(c001));
    LUT_SUB_ASSERT_FALSE(calcDirection(c002));
    LUT_SUB_ASSERT_FALSE(calcDirection(c003));
    LUT_SUB_ASSERT_TRUE(calcDirection(c010));
    LUT_SUB_ASSERT_TRUE(calcDirection(c011));
    LUT_SUB_ASSERT_TRUE(calcDirection(c012));
    LUT_SUB_ASSERT_TRUE(calcDirection(c013));
    LUT_SUB_ASSERT_FALSE(calcDirection(c100));
    LUT_SUB_ASSERT_FALSE(calcDirection(c101));
    LUT_SUB_ASSERT_FALSE(calcDirection(c102));
    LUT_SUB_ASSERT_TRUE(calcDirection(c110));
    LUT_SUB_ASSERT_TRUE(calcDirection(c111));
    LUT_SUB_ASSERT_TRUE(calcDirection(c112));
  }
  LUT_CASE_END

  LUT_CASE("findSelfIntersections 1")
  {

    std::vector<IntersectInfo> intersections;

    std::vector<lpi::Vector2> wanted;
    wanted.push_back(lpi::Vector2(-0.5,2));
    wanted.push_back(lpi::Vector2(0.5,2));

    findSelfIntersections(intersections, contour1);
    //std::cout<<"num intersections found: " << intersections.size()<<std::endl; for(size_t i = 0; i < intersections.size(); i++) std::cout<<intersections[i].p<<std::endl;
    LUT_SUB_ASSERT_TRUE(intersections.size() == 2)
    LUT_SUB_ASSERT_TRUE(onePointNear(intersections.at(0).p, wanted))
    LUT_SUB_ASSERT_TRUE(onePointNear(intersections.at(1).p, wanted))
  }
  LUT_CASE_END

  LUT_CASE("findSelfIntersections 2")
  {

    std::vector<IntersectInfo> intersections;

    std::vector<lpi::Vector2> wanted;
    wanted.push_back(lpi::Vector2(1,1));
    wanted.push_back(lpi::Vector2(1,2));
    wanted.push_back(lpi::Vector2(0,2));
    wanted.push_back(lpi::Vector2(0,1));

    findSelfIntersections(intersections, contour2);
    //std::cout<<"num intersections found: " << intersections.size()<<std::endl; for(size_t i = 0; i < intersections.size(); i++) std::cout<<intersections[i].p<<std::endl;
    LUT_SUB_ASSERT_TRUE(intersections.size() == 4)
    LUT_SUB_ASSERT_TRUE(onePointNear(intersections.at(0).p, wanted))
    LUT_SUB_ASSERT_TRUE(onePointNear(intersections.at(1).p, wanted))
    LUT_SUB_ASSERT_TRUE(onePointNear(intersections.at(2).p, wanted))
    LUT_SUB_ASSERT_TRUE(onePointNear(intersections.at(3).p, wanted))
  }
  LUT_CASE_END

  LUT_CASE("postProcessSelfIntersections")
  {
    std::vector<IntersectInfo> intersections;
    findSelfIntersections(intersections, contour1);
    std::vector<std::vector<SubIntersectInfo> > post;
    postprocesSelfIntersections(post, intersections, contour1);

    LUT_SUB_ASSERT_TRUE(post.size() == 5)
    LUT_SUB_ASSERT_TRUE(post.at(2).size() == 2) //2 intersections
    LUT_SUB_ASSERT_TRUE(pointsNear(post.at(2).at(0).p, lpi::Vector2(0.5,2)))
    LUT_SUB_ASSERT_TRUE(pointsNear(post.at(2).at(1).p, lpi::Vector2(-0.5,2)))
    LUT_SUB_ASSERT_TRUE(post.at(2).at(0).line_index_0 == 2)
    LUT_SUB_ASSERT_TRUE(post.at(2).at(0).line_index_1 == 0)
    LUT_SUB_ASSERT_TRUE(post.at(2).at(1).line_index_0 == 2)
    LUT_SUB_ASSERT_TRUE(post.at(2).at(1).line_index_1 == 4)
    LUT_SUB_ASSERT_TRUE(post.at(2).at(0).sub_index_0 == 0)
    LUT_SUB_ASSERT_TRUE(post.at(2).at(0).sub_index_1 == 0)
    LUT_SUB_ASSERT_TRUE(post.at(2).at(1).sub_index_0 == 1)
    LUT_SUB_ASSERT_TRUE(post.at(2).at(1).sub_index_1 == 0)
    LUT_SUB_ASSERT_TRUE(post.at(0).size() == 1)
    LUT_SUB_ASSERT_TRUE(post.at(0).at(0).line_index_0 == 0)
    LUT_SUB_ASSERT_TRUE(post.at(0).at(0).line_index_1 == 2)
    LUT_SUB_ASSERT_TRUE(post.at(0).at(0).sub_index_0 == 0)
    LUT_SUB_ASSERT_TRUE(post.at(0).at(0).sub_index_1 == 0)
    LUT_SUB_ASSERT_TRUE(post.at(4).size() == 1)
    LUT_SUB_ASSERT_TRUE(post.at(4).at(0).line_index_0 == 4)
    LUT_SUB_ASSERT_TRUE(post.at(4).at(0).line_index_1 == 2)
    LUT_SUB_ASSERT_TRUE(post.at(4).at(0).sub_index_0 == 0)
    LUT_SUB_ASSERT_TRUE(post.at(4).at(0).sub_index_1 == 1)
  }
  LUT_CASE_END

  LUT_CASE("postProcessSelfIntersections 2")
  {
    std::vector<IntersectInfo> intersections;
    findSelfIntersections(intersections, contour2);
    std::vector<std::vector<SubIntersectInfo> > post;
    postprocesSelfIntersections(post, intersections, contour2);

    LUT_SUB_ASSERT_TRUE(post.size() == 10)
    LUT_SUB_ASSERT_TRUE(post.at(1).size() == 2)
    LUT_SUB_ASSERT_TRUE(pointsNear(post.at(1).at(0).p, lpi::Vector2(1,1)))
    LUT_SUB_ASSERT_TRUE(pointsNear(post.at(1).at(1).p, lpi::Vector2(1,2)))
    LUT_SUB_ASSERT_TRUE(post.at(1).at(0).line_index_0 == 1)
    LUT_SUB_ASSERT_TRUE(post.at(1).at(0).line_index_1 == 6)
    LUT_SUB_ASSERT_TRUE(post.at(1).at(1).line_index_0 == 1)
    LUT_SUB_ASSERT_TRUE(post.at(1).at(1).line_index_1 == 4)
    LUT_SUB_ASSERT_TRUE(post.at(1).at(0).sub_index_0 == 0)
    LUT_SUB_ASSERT_TRUE(post.at(1).at(0).sub_index_1 == 1)
    LUT_SUB_ASSERT_TRUE(post.at(1).at(1).sub_index_0 == 1)
    LUT_SUB_ASSERT_TRUE(post.at(1).at(1).sub_index_1 == 0)
  }
  LUT_CASE_END

  LUT_CASE("removeSelfIntersections on simple non intersecting contour")
  {
    PolyContour p;

    removeSelfIntersections(p, contour0);

    LUT_SUB_ASSERT_TRUE(p.size() == 1)

    Contour& c = *(p.at(0));

    //std::cout<<"result contour size: " << c.size()<<std::endl; for(size_t i = 0; i < c.size(); i++) std::cout<<c[i]<<std::endl;

    LUT_SUB_ASSERT_TRUE(c.size() == 4)

    LUT_SUB_ASSERT_TRUE(pointsNear(contour0.at(0), c[0]))
    LUT_SUB_ASSERT_TRUE(pointsNear(contour0.at(1), c[1]))
    LUT_SUB_ASSERT_TRUE(pointsNear(contour0.at(2), c[2]))
    LUT_SUB_ASSERT_TRUE(pointsNear(contour0.at(3), c[3]))

    p.cleanup();
  }
  LUT_CASE_END

  LUT_CASE("removeSelfIntersections on contour with two intersections")
  {
    PolyContour p;

    removeSelfIntersections(p, contour1);

    //std::cout<<"num result contours: " << p.size()<<std::endl;
    //for(size_t i = 0; i < p.size(); i++)
    //{
    //  Contour& c = *(p[i]);
    //  std::cout<<"c size: " << c.size()<<std::endl; for(size_t i = 0; i < c.size(); i++) std::cout<<c[i]<<std::endl;
    //}

    LUT_SUB_ASSERT_TRUE(p.size() == 3)

    Contour& c0 = *(p.at(0));
    LUT_SUB_ASSERT_TRUE(c0.size() == 3)

    Contour& c1 = *(p.at(1));
    LUT_SUB_ASSERT_TRUE(c1.size() == 3)

    Contour& c2 = *(p.at(2));
    LUT_SUB_ASSERT_TRUE(c2.size() == 3)

    p.cleanup();
  }
  LUT_CASE_END

  LUT_CASE("removeSelfIntersections on special contour with 4 intersections")
  {
    PolyContour p;

    removeSelfIntersections(p, contour2);

    //std::cout<<"num result contours: " << p.size()<<std::endl;
    //for(size_t i = 0; i < p.size(); i++)
    //{
    //  Contour& c = *(p[i]);
    //  std::cout<<"c size: " << c.size()<<std::endl; for(size_t i = 0; i < c.size(); i++) std::cout<<c[i]<<std::endl;
    //}

    LUT_SUB_ASSERT_TRUE(p.size() == 3)

    Contour& c0 = *(p.at(0));
    Contour& c1 = *(p.at(1));
    Contour& c2 = *(p.at(2));

    LUT_SUB_ASSERT_TRUE(c0.size() == 4 || c1.size() == 4 || c2.size() == 4)
    LUT_SUB_ASSERT_TRUE(c0.size() == 10 || c1.size() == 10 || c2.size() == 10)
    LUT_SUB_ASSERT_TRUE(c0.size() == 4 || c1.size() == 4 || c2.size() == 4)


    p.cleanup();
  }
  LUT_CASE_END


  LUT_END_UNIT_TEST
}

#endif







