/*
LodePaint

Copyright (c) 2009-2010 Lode Vandevenne
All rights reserved.

This file is part of LodePaint.

LodePaint is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LodePaint is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LodePaint.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once 

#include "lpi/os/lpi_filebrowse.h"
#include "lpi/lpi_os.h"

#include "paint_filter.h" 
#include "paint_window.h"

#include <string>
#include <vector>
#include <deque>

#if defined(LPI_OS_WINDOWS)
#include "windows.h"
#endif

class FilterPlugin : public AOperationFilterUtil
{
  private:
#if defined(LPI_OS_LINUX) || defined(LPI_OS_AMIGA)
    void* dl; //the pointer to the dynamic or shared library
#elif defined(LPI_OS_WINDOWS)
    HINSTANCE  dl; //the pointer to the dynamic or shared library
#endif
    std::string label;
    std::string submenu;
    bool isvalid; //true if all required functions are in the dll (not ALL functions are required, but some are). For example if 128-bit is supported, the fileFormatFreeFloat function must be there, the fileFormatFreeUnsignedChar function must ALWAYS be there.
    bool hasgui;
    lpi::gui::Element* dialog;
    
    //for possible resize controls in the dialog
    int origwidth;
    int origheight;
    
    std::vector<int> enums;
    std::vector<int> ints;
    std::vector<double> doubles;
    std::deque<bool> bools;
    std::vector<lpi::ColorRGBd> colors;
    
    typedef void (*T_pluginFreeUnsignedChar)(unsigned char*);
    T_pluginFreeUnsignedChar p_pluginFreeUnsignedChar;
    typedef void (*T_pluginFreeFloat)(float*);
    T_pluginFreeFloat p_pluginFreeFloat;

    typedef void (*T_filterGetName)(char*, int, int);
    T_filterGetName p_filterGetName;
    typedef void (*T_filterGetSubMenu)(char*, int, int);
    T_filterGetSubMenu p_filterGetSubMenu;
    
    typedef void (*T_filterExecuteRGBA8)(unsigned char**, int*, int*, unsigned char*, int, int, int, CParams*, CParams*, int);
    T_filterExecuteRGBA8 p_filterExecuteRGBA8;
    typedef void (*T_filterExecuteRGBA32F)(float**, int*, int*, float*, int, int, int, CParams*, CParams*, int);
    T_filterExecuteRGBA32F p_filterExecuteRGBA32F;
    typedef void (*T_filterExecuteGrey8)(unsigned char**, int*, int*, unsigned char*, int, int, int, CParams*, CParams*, int);
    T_filterExecuteGrey8 p_filterExecuteGrey8;
    typedef void (*T_filterExecuteGrey32F)(float**, int*, int*, float*, int, int, int, CParams*, CParams*, int);
    T_filterExecuteGrey32F p_filterExecuteGrey32F;
    
    typedef int (*T_filterGetParametersXMLSize)(int);
    T_filterGetParametersXMLSize p_filterGetParametersXMLSize;
    typedef void (*T_filterGetParametersXML)(char*, int);
    T_filterGetParametersXML p_filterGetParametersXML;
    

  protected:
  
    virtual bool operate(Paint& paint, UndoState& state, Progress& progress);
    virtual bool operateGL(Paint& paint, UndoState& state);
    virtual void doit(TextureRGBA8& texture, Progress& progress);
    virtual void doit(TextureRGBA32F& texture, Progress& progress);
    virtual void doit(TextureGrey8& texture, Progress& progress);
    virtual void doit(TextureGrey32F& texture, Progress& progress);
    virtual bool supportsColorMode(ColorMode mode) const;
    
    void initGUI(const lpi::gui::IGUIDrawer& geom);
    
  public:
  
    FilterPlugin(const std::string& dynamicLib, GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom);
    ~FilterPlugin();
    
    virtual lpi::gui::Element* getDialog();
    virtual std::string getLabel() const;
    virtual std::string getSubMenu() const; //returns empty string for root
    
    bool isValidFilterPlugin() const;
    
    void init(PaintWindow* pw);
};





