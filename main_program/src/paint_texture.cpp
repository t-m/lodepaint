/*
LodePaint

Copyright (c) 2009-2010 Lode Vandevenne
All rights reserved.

This file is part of LodePaint.

LodePaint is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LodePaint is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LodePaint.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "paint_texture.h"

#include <iostream>
#include <sstream>
#include <iterator>

//has OpenGL got the given extension?
bool checkOpenGLExtension(const std::string& extension)
{
  std::stringstream ss;
  ss << (const char*)glGetString(GL_EXTENSIONS);

  //split stringstream into multiple strings with space as delimiter
  std::vector<std::string> extensions;
  std::copy(std::istream_iterator<std::string>(ss),
            std::istream_iterator<std::string>(),
            std::back_inserter<std::vector<std::string> >(extensions));

  for(size_t i = 0; i < extensions.size(); i++)
  {
    if(extensions[i] == extension) return true;
  }

  return false;
}


static bool ALWAYS_USE_GL_UNSIGNED_BYTE = true; //enable this for systems that don't support GL_FLOAT

lpi::GLContext* contextForPaintTextures = 0;

static int getNumChannels(GLenum format)
{
  if(format == GL_LUMINANCE || format == GL_ALPHA) return 1; //TODO: take possible other formats into account
  return 4;
}

TextureGeneric::Part::Part(lpi::GLContext* context)
: generated_id(-1)
, context(context)
{
}

TextureGeneric::Part::~Part()
{
  if(context->isActive() && context->getID() == generated_id)
  {
    glDeleteTextures(1, &texture);
  }
}


TextureGeneric::TextureGeneric(GLint internalFormat, GLenum format, GLenum type, size_t bytesPerPixel)
: internalFormat(internalFormat)
, format(format)
, type(type)
, bytesPerPixel(bytesPerPixel)
, size_valid_gl(false)
, u(0)
, v(0)
, u2(0)
, v2(0)
, partsx(1)
, partsy(1)
, context(contextForPaintTextures)
{
  static bool static_init = false;
  if(!static_init)
  {
    static_init = true;
    ALWAYS_USE_GL_UNSIGNED_BYTE = !checkOpenGLExtension("GL_ARB_texture_float");
  }
}

void TextureGeneric::setSize(size_t u, size_t v)
{
  this->u = u;
  this->v = v;
  makeBuffer();
  size_valid_gl = false;
}


TextureGeneric::~TextureGeneric()
{
}

//make memory for the buffer of the texture
void TextureGeneric::makeBuffer()
{
  size_valid_gl = true;

  if(u == 0 && v == 0)
  {
    u2 = v2 = 0;
  }
  if(u <= MAXX && v <= MAXY)
  {
    partsx = partsy = 1;

    //find first larger power of two of width and store it in u2
    u2 = 1;
    while(u2 < u) u2 *= 2;

    //find first larger power of two of height and store it in v2
    v2 = 1;
    while(v2 < v) v2 *= 2;
  }
  else
  {
    partsx = (u + MAXX - 1) / MAXX;
    partsy = (v + MAXY - 1) / MAXY;

    u2 = partsx * MAXX;
    v2 = partsy * MAXY;
  }

  buffer.resize(bytesPerPixel * u2 * v2);

  if(ALWAYS_USE_GL_UNSIGNED_BYTE && type != GL_UNSIGNED_BYTE)
  {
    ubyte_buffer.resize(u2 * v2 * getNumChannels(format));
  }
}

//make memory for the buffer of the texture
void TextureGeneric::makeBufferGL() const
{
  size_valid_gl = true;

  if(u == 0 && v == 0)
  {
    parts.clear();
  }
  if(u <= MAXX && v <= MAXY)
  {
    parts.clear(); //always clear before resizing, a Part can't be correctly copied
    parts.push_back(Part(context));
    Part& part = parts[0];
    part.shiftx = 0;
    part.shifty = 0;
    part.u = u;
    part.v = v;
    part.u2 = u2;
    part.v2 = v2;
  }
  else
  {
    parts.clear(); //always clear before resizing, a Part can't be correctly copied
    for(size_t i = 0; i < partsx * partsy; i++) parts.push_back(Part(context));
    for(size_t y = 0; y < partsy; y++)
    for(size_t x = 0; x < partsx; x++)
    {
      size_t index = y * partsx + x;
      Part& part = parts[index];
      part.shiftx = x * MAXX;
      part.shifty = y * MAXY;
      part.u = MAXX;
      if(x == partsx - 1) part.u = (u % MAXX == 0) ? MAXX : u % MAXX;
      part.v = MAXY;
      if(y == partsy - 1) part.v = (v % MAXY == 0) ? MAXY : v % MAXY;
      part.u2 = MAXX;
      part.v2 = MAXY;
    }
  }
}

void TextureGeneric::uploadPartial(int x0, int y0, int x1, int y1)
{
  if(!context->isActive()) return;

  if(!size_valid_gl) makeBufferGL();

  if(parts.empty()) return;
  //if(x0 < 0 || x0 > (int)u || y0 < 0 || y0 > (int)v || x1 < 0 || x1 > (int)u || y1 < 0 || y1 > (int)v) std::cout<<"panic!"<<std::endl;

  if(x0 < 0) x0 = 0; if(x0 > (int)u) x0 = u;
  if(y0 < 0) y0 = 0; if(y0 > (int)v) y0 = v;
  if(x1 < 0) x1 = 0; if(x1 > (int)u) x1 = u;
  if(y1 < 0) y1 = 0; if(y1 > (int)v) y1 = v;

  if(x0 == x1 || y0 == y1) return;

  for(size_t i = 0; i < parts.size(); i++)
  {
    if(parts[i].generated_id < 0)
    {
      upload();
      return;
    }
  }

  //the type used for the GL commands, can be changed if ALWAYS_USE_GL_UNSIGNED_BYTE is true
  GLenum actualType = type;
  const unsigned char* actualBuffer = &buffer[0];

  if(ALWAYS_USE_GL_UNSIGNED_BYTE && type != GL_UNSIGNED_BYTE)
  {
    actualType = GL_UNSIGNED_BYTE;
    actualBuffer = &ubyte_buffer[0];

    if(type == GL_FLOAT)
    {
      float* f = (float*)getBufferGeneric();
      int n = getNumChannels(format);

      for(int y = y0; y < y1; y++)
      for(int x = x0; x < x1; x++)
      for(int c = 0; c < n; c++)
      {
        float d = f[u2 * n * y + n * x + c];
        if(d > 1.0) d = 1.0;
        if(d < 0.0) d = 0.0;
        ubyte_buffer[u2 * n * y + n * x + c] = (unsigned char)(d * 255.0);
      }
    }
  }


  if(u <= MAXX && v <= MAXY)
  {
    bind(false, 0);

    //indicate how we'll read from buffer
    glPixelStorei( GL_UNPACK_ROW_LENGTH, u2);
    glPixelStorei( GL_UNPACK_SKIP_PIXELS, x0);
    glPixelStorei( GL_UNPACK_SKIP_ROWS, y0);

    glTexSubImage2D(GL_TEXTURE_2D, 0, x0, y0, x1 - x0, y1 - y0, format, actualType, actualBuffer );

  }
  else
  {
    for(size_t i = 0; i < parts.size(); i++)
    {
      Part& part = parts[i];

      bool xoverlap = true;
      if(x1 <= part.shiftx || x0 >= (int)(part.shiftx + part.u)) xoverlap = false;
      bool yoverlap = true;
      if(y1 <= part.shifty || y0 >= (int)(part.shifty + part.v)) yoverlap = false;
      if(xoverlap && yoverlap)
      {
        bind(false, i);

        int px0 = x0 - part.shiftx; if(px0 < 0) px0 = 0; /*if(px0 > (int)part.u) px0 = part.u;*/
        int py0 = y0 - part.shifty; if(py0 < 0) py0 = 0; /*if(py0 > (int)part.v) py0 = part.v;*/
        int px1 = x1 - part.shiftx; /*if(px1 < 0) px1 = 0;*/ if(px1 > (int)part.u) px1 = part.u;
        int py1 = y1 - part.shifty; /*if(py1 < 0) py1 = 0;*/ if(py1 > (int)part.v) py1 = part.v;

        //indicate how we'll read from buffer
        glPixelStorei( GL_UNPACK_ROW_LENGTH, u2);
        glPixelStorei( GL_UNPACK_SKIP_PIXELS, part.shiftx + px0);
        glPixelStorei( GL_UNPACK_SKIP_ROWS, part.shifty + py0);

        glTexSubImage2D(GL_TEXTURE_2D, 0, px0, py0, px1 - px0, py1 - py0, format, actualType, actualBuffer );

      }
    }
  }

  //set back to default so that the rest behaves normally
  glPixelStorei( GL_UNPACK_ROW_LENGTH, 0);
  glPixelStorei( GL_UNPACK_SKIP_PIXELS, 0);
  glPixelStorei( GL_UNPACK_SKIP_ROWS, 0);

}

//This generates the OpenGL texture so that OpenGL can use it, also use after changing the texture buffer
void TextureGeneric::upload() const
{
  if(!context->isActive()) return;

  if(!size_valid_gl) makeBufferGL();

  if(parts.empty()) return;

  for(size_t i = 0; i < parts.size(); i++)
  {
    Part& part = parts[i];
    if(part.generated_id < 0)
    {
      glGenTextures(1, &part.texture);
      part.generated_id = context->getID();
    }
  }

  //the type used for the GL commands, can be changed if ALWAYS_USE_GL_UNSIGNED_BYTE is true
  GLenum actualType = type;
  const unsigned char* actualBuffer = &buffer[0];

  if(ALWAYS_USE_GL_UNSIGNED_BYTE && type != GL_UNSIGNED_BYTE)
  {
    actualType = GL_UNSIGNED_BYTE;
    actualBuffer = &ubyte_buffer[0];

    if(type == GL_FLOAT)
    {
      float* f = (float*)(&buffer[0]);
      int n = getNumChannels(format);

      for(size_t y = 0; y < v; y++)
      for(size_t x = 0; x < u; x++)
      for(int c = 0; c < n; c++)
      {
        float d = f[u2 * n * y + n * x + c];
        if(d > 1.0) d = 1.0;
        if(d < 0.0) d = 0.0;
        ubyte_buffer[u2 * n * y + n * x + c] = (unsigned char)(d * 255.0);
      }
    }
  }

  if(u <= MAXX && v <= MAXY)
  {
    bind(false, 0);
    glTexImage2D(GL_TEXTURE_2D, 0, internalFormat, u2, v2, 0, format, actualType, actualBuffer);
  }
  else
  {
    for(size_t i = 0; i < parts.size(); i++)
    {
      Part& part = parts[i];
      bind(false, i);
      glPixelStorei( GL_UNPACK_ROW_LENGTH, u2);
      glPixelStorei( GL_UNPACK_SKIP_PIXELS, part.shiftx);
      glPixelStorei( GL_UNPACK_SKIP_ROWS, part.shifty);
      glTexImage2D(GL_TEXTURE_2D, 0, internalFormat, part.u2, part.v2, 0, format, actualType, actualBuffer);
    }
    //set back to default so that the rest behaves normally
    glPixelStorei( GL_UNPACK_ROW_LENGTH, 0);
    glPixelStorei( GL_UNPACK_SKIP_PIXELS, 0);
    glPixelStorei( GL_UNPACK_SKIP_ROWS, 0);
  }

}

void TextureGeneric::reupload() const
{
  parts.clear();
  upload();
}

//make this the selected one for drawing
void TextureGeneric::bind(bool smoothing, size_t index) const
{
  if(parts.empty()) return;

  glBindTexture(GL_TEXTURE_2D, parts[index].texture);

  if(smoothing) //todo: remove this from the "bind" function, do it in generate or separatly instead
  {
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    //avoid wrapping artefacts, this is for paintings, we're never repeating them, and when wrapping
    //there appear ugly lines between texture-pieces that form an image if it's drawn with smoothing
    //enabled
#ifndef GL_CLAMP_TO_EDGE
#define GL_CLAMP_TO_EDGE 33071 //Code::Blocks in Windows has rather outdated OpenGL library, let's define the value ourselves
#endif
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
  }
  else
  {
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  }
}

bool TextureGeneric::updateForNewOpenGLContextIfNeeded() const
{
  if(parts.empty()) return false;

  if(!context->isActive()) return false;

  if(parts[0].generated_id != context->getID())
  {
    reupload();
    return true;
  }

  return false;
}



////////////////////////////////////////////////////////////////////////////////

void prepareDrawTextured(lpi::ScreenGL* screen)
{
  screen->set2DScreen(true);
  glEnable(GL_TEXTURE_2D);
  //screen->setOpenGLScissor(); //everything that draws something must always do this //TODO: investigate that statement
}

void drawTexture(lpi::ScreenGL* screen, const TextureGeneric* texture, int x, int y, const lpi::ColorRGB& colorMod)
{
  drawTextureSized(screen, texture, x, y, texture->getU(), texture->getV(), colorMod);
}

void drawTextureSized(lpi::ScreenGL* screen, const TextureGeneric* texture, int x, int y, size_t sizex, size_t sizey, const lpi::ColorRGB& colorMod)
{
  if(sizex == 0 || sizey == 0) return;

  prepareDrawTextured(screen);
  texture->updateForNewOpenGLContextIfNeeded();
  glColor4ub(colorMod.r, colorMod.g, colorMod.b, colorMod.a);

  //Enable smoothing always for certain size ratios. For some reason I need to do this now while I didn't need
  //to do this on a previous computer. On the previous computer, 1.5X zoomed in textures without smoothing looked
  //perfectly fine (and pixely as intended). But on this new computer, (same OS, same graphics card brand but
  // newer model), there's something ugly going on where you can see the two triangles forming the quad.
  // Especially on an image existing out of horizontal alternating stripes this is ugly.
  // Anyway, smoothing for this special case seems to fix this. But don't smooth when zooming in with nice
  //factors like 2! Because to paint on the images when zoomed in, you need to see its pixels exactly as-is.
  bool useSmoothing = screen->isSmoothingEnabled();
  if(sizex == texture->getU() * 3 / 2 && sizey == texture->getV() * 3 / 2)
  {
    useSmoothing = true;
  }

  if(texture->getNumParts() == 1)
  {
    texture->bind(useSmoothing, 0);

    double u3 = (double)texture->getU() / (double)texture->getU2();
    double v3 = (double)texture->getV() / (double)texture->getV2();

    //note how in the texture coordinates x and y are swapped because the texture buffers are 90 degrees rotated
    glBegin(GL_QUADS);
      glTexCoord2d(0.0, 0.0); glVertex2d(x +          0, y +          0);
      glTexCoord2d(+u3, 0.0); glVertex2d(x + (int)sizex, y +          0);
      glTexCoord2d(+u3, +v3); glVertex2d(x + (int)sizex, y + (int)sizey);
      glTexCoord2d(0.0, +v3); glVertex2d(x +          0, y + (int)sizey);
    glEnd();
  }
  else
  {
    for(size_t i = 0; i < texture->getNumParts(); i++)
    {
      texture->bind(useSmoothing, i);
      const TextureGeneric::Part& part = texture->getPart(i);
      double u3 = (double)part.u / part.u2;
      double v3 = (double)part.v / part.v2;

      double zoomx = (double)sizex / (double)texture->getU();
      double zoomy = (double)sizey / (double)texture->getV();

      double px = x + zoomx * part.shiftx;
      double py = y + zoomy * part.shifty;
      double psizex = sizex * ((double)part.u / (double)texture->getU());
      double psizey = sizey * ((double)part.v / (double)texture->getV());

      //note how in the texture coordinates x and y are swapped because the texture buffers are 90 degrees rotated
      glBegin(GL_QUADS);
        glTexCoord2d(0.0, 0.0); glVertex2d(px +      0, py +      0);
        glTexCoord2d(+u3, 0.0); glVertex2d(px + psizex, py +      0);
        glTexCoord2d(+u3, +v3); glVertex2d(px + psizex, py + psizey);
        glTexCoord2d(0.0, +v3); glVertex2d(px +      0, py + psizey);
      glEnd();

    }
  }
}

void drawTextureSizedCentered(lpi::ScreenGL* screen, const TextureGeneric* texture, int x, int y, size_t sizex, size_t sizey, const lpi::ColorRGB& colorMod)
{
  drawTextureSized(screen, texture, x - sizex / 2, y - sizey / 2, sizex, sizey, colorMod);
}

void getAlignedBuffer(std::vector<unsigned char>& buffer, const TextureRGBA8* texture)
{
  size_t u = texture->getU();
  size_t v = texture->getV();
  size_t u2 = texture->getU2();
  buffer.resize(u * v * 4);
  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  for(size_t c = 0; c < 4; c++)
  {
    buffer[y * u * 4 + x * 4 + c] = texture->getBuffer()[y * u2 * 4 + x * 4 + c];
  }
}

void setAlignedBuffer(TextureRGBA8* texture, const unsigned char* buffer, bool update)
{
  size_t u = texture->getU();
  size_t v = texture->getV();
  size_t u2 = texture->getU2();
  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  for(size_t c = 0; c < 4; c++)
  {
    texture->getBuffer()[y * u2 * 4 + x * 4 + c] = buffer[y * u * 4 + x * 4 + c];
  }
  if(update) texture->update();
}


void getAlignedBuffer(std::vector<float>& buffer, const TextureRGBA32F* texture)
{
  size_t u = texture->getU();
  size_t v = texture->getV();
  size_t u2 = texture->getU2();
  buffer.resize(u * v * 4);
  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  for(size_t c = 0; c < 4; c++)
  {
    buffer[y * u * 4 + x * 4 + c] = texture->getBuffer()[y * u2 * 4 + x * 4 + c];
  }
}

void setAlignedBuffer(TextureRGBA32F* texture, const float* buffer, bool update)
{
  size_t u = texture->getU();
  size_t v = texture->getV();
  size_t u2 = texture->getU2();
  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  for(size_t c = 0; c < 4; c++)
  {
    texture->getBuffer()[y * u2 * 4 + x * 4 + c] = buffer[y * u * 4 + x * 4 + c];
  }
  if(update) texture->update();
}


void getAlignedBuffer(std::vector<unsigned char>& buffer, const TextureGrey8* texture)
{
  size_t u = texture->getU();
  size_t v = texture->getV();
  size_t u2 = texture->getU2();
  buffer.resize(u * v);
  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  {
    buffer[y * u + x] = texture->getBuffer()[y * u2 + x];
  }
}

void setAlignedBuffer(TextureGrey8* texture, const unsigned char* buffer, bool update)
{
  size_t u = texture->getU();
  size_t v = texture->getV();
  size_t u2 = texture->getU2();
  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  {
    texture->getBuffer()[y * u2 + x] = buffer[y * u + x];
  }
  if(update) texture->update();
}

void getAlignedBuffer(std::vector<float>& buffer, const TextureGrey32F* texture)
{
  size_t u = texture->getU();
  size_t v = texture->getV();
  size_t u2 = texture->getU2();
  buffer.resize(u * v);
  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  {
    buffer[y * u + x] = texture->getBuffer()[y * u2 + x];
  }
}

void setAlignedBuffer(TextureGrey32F* texture, const float* buffer, bool update)
{
  size_t u = texture->getU();
  size_t v = texture->getV();
  size_t u2 = texture->getU2();
  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  {
    texture->getBuffer()[y * u2 + x] = buffer[y * u + x];
  }
  if(update) texture->update();
}


void copyTexture(TextureRGBA8* dest, const TextureRGBA8* source)
{
  size_t u = source->getU();
  size_t v = source->getV();
  size_t u2 = source->getU2();
  dest->setSize(u, v);
  size_t u2b = dest->getU2();
  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  for(size_t c = 0; c < 4; c++)
  {
    size_t index = y * u2 * 4 + x * 4 + c;
    size_t indexb = y * u2b * 4 + x * 4 + c;
    dest->getBuffer()[indexb] = source->getBuffer()[index];
  }
  dest->update();
}

void copyTexture(TextureRGBA32F* dest, const TextureRGBA32F* source)
{
  size_t u = source->getU();
  size_t v = source->getV();
  size_t u2 = source->getU2();
  dest->setSize(u, v);
  size_t u2b = dest->getU2();
  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  for(size_t c = 0; c < 4; c++)
  {
    size_t index = y * u2 * 4 + x * 4 + c;
    size_t indexb = y * u2b * 4 + x * 4 + c;
    dest->getBuffer()[indexb] = source->getBuffer()[index];
  }
  dest->update();
}

void copyTexture(TextureGrey8* dest, const TextureGrey8* source)
{
  size_t u = source->getU();
  size_t v = source->getV();
  size_t u2 = source->getU2();
  dest->setSize(u, v);
  size_t u2b = dest->getU2();
  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  {
    size_t index = y * u2 + x;
    size_t indexb = y * u2b + x;
    dest->getBuffer()[indexb] = source->getBuffer()[index];
  }
  dest->update();
}

void copyTexture(TextureGrey32F* dest, const TextureGrey32F* source)
{
  size_t u = source->getU();
  size_t v = source->getV();
  size_t u2 = source->getU2();
  dest->setSize(u, v);
  size_t u2b = dest->getU2();
  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  {
    size_t index = y * u2 + x;
    size_t indexb = y * u2b + x;
    dest->getBuffer()[indexb] = source->getBuffer()[index];
  }
  dest->update();
}

void setPixel(TextureRGBA8* texture, int x, int y, const lpi::ColorRGB& color)
{
  unsigned char* buffer = texture->getBuffer();
  size_t u2 = texture->getU2();
  buffer[y * u2 * 4 + x * 4 + 0] = color.r;
  buffer[y * u2 * 4 + x * 4 + 1] = color.g;
  buffer[y * u2 * 4 + x * 4 + 2] = color.b;
  buffer[y * u2 * 4 + x * 4 + 3] = color.a;
}

lpi::ColorRGB getPixel(const TextureRGBA8* texture, int x, int y)
{
  lpi::ColorRGB result;
  const unsigned char* buffer = texture->getBuffer();
  size_t u2 = texture->getU2();
  result.r = buffer[y * u2 * 4 + x * 4 + 0];
  result.g = buffer[y * u2 * 4 + x * 4 + 1];
  result.b = buffer[y * u2 * 4 + x * 4 + 2];
  result.a = buffer[y * u2 * 4 + x * 4 + 3];
  return result;
}

void setPixel(TextureRGBA32F* texture, int x, int y, const lpi::ColorRGBd& color)
{
  float* buffer = texture->getBuffer();
  size_t u2 = texture->getU2();
  buffer[y * u2 * 4 + x * 4 + 0] = color.r;
  buffer[y * u2 * 4 + x * 4 + 1] = color.g;
  buffer[y * u2 * 4 + x * 4 + 2] = color.b;
  buffer[y * u2 * 4 + x * 4 + 3] = color.a;
}

lpi::ColorRGBd getPixel(const TextureRGBA32F* texture, int x, int y)
{
  lpi::ColorRGBd result;
  const float* buffer = texture->getBuffer();
  size_t u2 = texture->getU2();
  result.r = buffer[y * u2 * 4 + x * 4 + 0];
  result.g = buffer[y * u2 * 4 + x * 4 + 1];
  result.b = buffer[y * u2 * 4 + x * 4 + 2];
  result.a = buffer[y * u2 * 4 + x * 4 + 3];
  return result;
}

void setPixel(TextureGrey8* texture, int x, int y, int color)
{
  unsigned char* buffer = texture->getBuffer();
  size_t u2 = texture->getU2();
  buffer[y * u2 + x] = color;
}

int getPixel(const TextureGrey8* texture, int x, int y)
{
  const unsigned char* buffer = texture->getBuffer();
  size_t u2 = texture->getU2();
  return buffer[y * u2 + x];
}

void setPixel(TextureGrey32F* texture, int x, int y, float color)
{
  float* buffer = texture->getBuffer();
  size_t u2 = texture->getU2();
  buffer[y * u2 + x] = color;
}

float getPixel(const TextureGrey32F* texture, int x, int y)
{
  const float* buffer = texture->getBuffer();
  size_t u2 = texture->getU2();
  return buffer[y * u2 + x];
}
