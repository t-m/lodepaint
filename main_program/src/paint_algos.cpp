/*
LodePaint

Copyright (c) 2009-2010 Lode Vandevenne
All rights reserved.

This file is part of LodePaint.

LodePaint is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LodePaint is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LodePaint.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "paint_algos.h"

#include "lpi/gio/lpi_draw2d.h"
#include "lpi/lpi_math.h"
#include "lpi/lpi_math2d.h"

#include "imalg/tolerance.h"

#include "paint_window.h"

#include <algorithm>


////////////////////////////////////////////////////////////////////////////////

void pset(unsigned char* buffer, int u, int v, int u2, int x, int y, const lpi::ColorRGB& color, double opacity, bool affect_alpha, bool wrapping)
{
  if(wrapping)
  {
    x = lpi::wrap(x, 0, u);
    y = lpi::wrap(y, 0, v);
  }
  
  if(x < 0 || y < 0 || x >= u || y >= v) return;
  
  int bufferPos = 4 * u2 * y + 4 * x;
  
  lpi::ColorRGB result = color;
  
  if(opacity < 1.0)
  {
    lpi::ColorRGB old;
    old.r = buffer[bufferPos + 0];
    old.g = buffer[bufferPos + 1];
    old.b = buffer[bufferPos + 2];
    old.a = buffer[bufferPos + 3];
    
    //result = (old & (1.0 - opacity)) | (color & opacity);
    
    int o = (int)(opacity * 255);
    result.r = (old.r * (255 - o) + color.r * o) / 255;
    result.g = (old.g * (255 - o) + color.g * o) / 255;
    result.b = (old.b * (255 - o) + color.b * o) / 255;
    result.a = (old.a * (255 - o) + color.a * o) / 255;
  }
  
  buffer[bufferPos + 0] = result.r;
  buffer[bufferPos + 1] = result.g;
  buffer[bufferPos + 2] = result.b;
  if(affect_alpha) buffer[bufferPos + 3] = result.a;
}

void psetGrey8(unsigned char* buffer, int u, int v, int u2, int x, int y, int color, double opacity, bool wrapping)
{
  if(wrapping)
  {
    x = lpi::wrap(x, 0, u);
    y = lpi::wrap(y, 0, v);
  }

  if(x < 0 || y < 0 || x >= u || y >= v) return;
  
  int bufferPos = u2 * y + x;
  
  int result = color;
  
  if(opacity < 1.0)
  {
    int old = buffer[bufferPos];
    
    //result = (old & (1.0 - opacity)) | (color & opacity);
    
    int o = (int)(opacity * 255);
    result = (old * (255 - o) + color * o) / 255;
  }
  
  buffer[bufferPos] = result;
}

void pset(float* buffer, int u, int v, int u2, int x, int y, const lpi::ColorRGBd& color, double opacity, bool affect_alpha, bool wrapping)
{
  if(wrapping)
  {
    x = lpi::wrap(x, 0, u);
    y = lpi::wrap(y, 0, v);
  }

  if(x < 0 || y < 0 || x >= u || y >= v) return;
  
  int bufferPos = 4 * u2 * y + 4 * x;
  
  lpi::ColorRGBd result = color;
  
  if(opacity < 1.0)
  {
    lpi::ColorRGBd old;
    old.r = buffer[bufferPos + 0];
    old.g = buffer[bufferPos + 1];
    old.b = buffer[bufferPos + 2];
    old.a = buffer[bufferPos + 3];
    
    //result = (old & (1.0 - opacity)) | (color & opacity);
    
    result.r = old.r * (1.0 - opacity) + color.r * opacity;
    result.g = old.g * (1.0 - opacity) + color.g * opacity;
    result.b = old.b * (1.0 - opacity) + color.b * opacity;
    result.a = old.a * (1.0 - opacity) + color.a * opacity;
  }
  
  buffer[bufferPos + 0] = result.r;
  buffer[bufferPos + 1] = result.g;
  buffer[bufferPos + 2] = result.b;
  if(affect_alpha) buffer[bufferPos + 3] = result.a;
}

void psetGrey32f(float* buffer, int u, int v, int u2, int x, int y, float color, double opacity, bool wrapping)
{
  if(wrapping)
  {
    x = lpi::wrap(x, 0, u);
    y = lpi::wrap(y, 0, v);
  }

  if(x < 0 || y < 0 || x >= u || y >= v) return;
  
  int bufferPos = u2 * y + x;
  
  float result = color;
  
  if(opacity < 1.0)
  {
    float old = buffer[bufferPos];
    result = old * (1.0 - opacity) + color * opacity;
  }
  
  buffer[bufferPos] = result;
}


void horLine(unsigned char* buffer, int u, int v, int u2, int y, int x1, int x2, const lpi::ColorRGB& color, double opacity, bool affect_alpha, bool wrapping)
{
  if(wrapping)
  {
    y = lpi::wrap(y, 0, v);
    x1 = lpi::wrap(x1, 0, u);
    x2 = lpi::wrap(x2, 0, u);
    if(x1 > x2) x2 += u;

    if(x1 < 0 && x2 > 0)
    {
      horLine(buffer, u, v, u2, y, u + x1, u, color, opacity, affect_alpha, false);
      horLine(buffer, u, v, u2, y, 0, x2, color, opacity, affect_alpha, false);
    }
    else if(x1 < u && x2 > u)
    {
      horLine(buffer, u, v, u2, y, 0, x2 - u, color, opacity, affect_alpha, false);
      horLine(buffer, u, v, u2, y, x1, u, color, opacity, affect_alpha, false);
    }
    else if(x2 - x1 >= u)
    {
      horLine(buffer, u, v, u2, y, 0, u, color, opacity, affect_alpha, false);
    }
    else
    {
      horLine(buffer, u, v, u2, y, x1, x2, color, opacity, affect_alpha, false);
    }
    
    return;
  }

  if(x2 < x1) {x1 += x2; x2 = x1 - x2; x1 -= x2;} //swap x1 and x2, x1 must be the leftmost endpoint   
  if(x2 < 0 || x1 > u - 1 || y < 0 || y > v - 1) return; //no single point of the line is on screen
  
  if(x1 < 0) x1 = 0;
  if(x2 > u) x2 = u;
  
  for(int x = x1; x < x2; x++)
  {
    pset(buffer, u, v, u2, x, y, color, opacity, affect_alpha);
  }
}

void verLine(unsigned char* buffer, int u, int v, int u2, int x, int y1, int y2, const lpi::ColorRGB& color, double opacity, bool affect_alpha, bool wrapping)
{
  if(wrapping)
  {
    x = lpi::wrap(x, 0, u);
    y1 = lpi::wrap(y1, 0, v);
    y2 = lpi::wrap(y2, 0, v);
    if(y1 > y2) y2 += v;

    if(y1 < 0 && y2 > 0)
    {
      verLine(buffer, u, v, u2, x, v + y1, v, color, opacity, affect_alpha, false);
      verLine(buffer, u, v, u2, x, 0, y2, color, opacity, affect_alpha, false);
    }
    else if(y1 < v && y2 > v)
    {
      verLine(buffer, u, v, u2, x, 0, y2 - v, color, opacity, affect_alpha, false);
      verLine(buffer, u, v, u2, x, y1, v, color, opacity, affect_alpha, false);
    }
    else if(y2 - y1 >= v)
    {
      verLine(buffer, u, v, u2, x, 0, v, color, opacity, affect_alpha, false);
    }
    else
    {
      verLine(buffer, u, v, u2, x, y1, y2, color, opacity, affect_alpha, false);
    }
    
    return;
  }

  if(y2 < y1) {y1 += y2; y2 = y1 - y2; y1 -= y2;} //swap y1 and y2, y1 must be the leftmost endpoint   
  if(y2 < 0 || y1 > v - 1 || x < 0 || x > u - 1) return; //no single point of the line is on screen
  
  if(y1 < 0) y1 = 0;
  if(y2 > v) y2 = v;
  
  for(int y = y1; y < y2; y++)
  {
    pset(buffer, u, v, u2, x, y, color, opacity, affect_alpha);
  }
}


////////////////////////////////////////////////////////////////////////////////

//this one does NOT check if the line is inside the buffer! (hence only width, not height, as parameter)
void bresenhamLine(unsigned char* buffer, int buffer_w, int buffer_h, int x0, int y0, int x1, int y1, const lpi::ColorRGB& color, double opacity, bool affect_alpha, bool include_last_pixel)
{
  /*if(x0 == y0 && x1 == y1 )
  {
    if(include_last_pixel) pset(buffer, buffer_w, buffer_h, x0, y0, color, opacity);
    return;
  }*/
  
  //draw the line with bresenham
  int deltax = (int)std::abs((double)(x1 - x0));    // The difference between the x's
  int deltay = (int)std::abs((double)(y1 - y0));    // The difference between the y's
  int x = x0;           // Start x off at the first pixel
  int y = y0;           // Start y off at the first pixel
  int xinc0, xinc1, yinc0, yinc1, den, num, numadd, numpixels, curpixel;

  if(x1 >= x0)         // The x-values are increasing
  {
    xinc0 = 1;
    xinc1 = 1;
  }
  else              // The x-values are decreasing
  {
    xinc0 = -1;
    xinc1 = -1;
  }
  if (y1 >= y0)         // The y-values are increasing
  {
    yinc0 = 1;
    yinc1 = 1;
  }
  else              // The y-values are decreasing
  {
    yinc0 = -1;
    yinc1 = -1;
  }
  if(deltax >= deltay)     // There is at least one x-value for every y-value
  {
    xinc0 = 0;          // Don't change the x when numerator >= denominator
    yinc1 = 0;          // Don't change the y for every iteration
    den = deltax;
    num = deltax / 2;
    numadd = deltay;
    numpixels = deltax;     // There are more x-values than y-values
  }
  else              // There is at least one y-value for every x-value
  {
    xinc1 = 0;          // Don't change the x for every iteration
    yinc0 = 0;          // Don't change the y when numerator >= denominator
    den = deltay;
    num = deltay / 2;
    numadd = deltax;
    numpixels = deltay;     // There are more y-values than x-values
  }
  
  if(!include_last_pixel) numpixels--;
  
  for(curpixel = 0; curpixel <= numpixels; curpixel++)
  {
    pset(buffer, buffer_w, buffer_h, buffer_w, x, y, color, opacity, affect_alpha);  // Draw the current pixel
    num += numadd;        // Increase the numerator by the top of the fraction
    if (num >= den)       // Check if numerator >= denominator
    {
      num -= den;       // Calculate the new numerator value
      x += xinc0;       // Change the x as appropriate
      y += yinc0;       // Change the y as appropriate
    }
    x += xinc1;         // Change the x as appropriate
    y += yinc1;         // Change the y as appropriate
  }
}

void clippedBresenhamLine(unsigned char* buffer, int buffer_w, int buffer_h, int x0, int y0, int x1, int y1, const lpi::ColorRGB& color, double opacity, bool affect_alpha, bool include_last_pixel)
{
  //clip if some point is outside the clipping area
  if(x0 < 0 || x0 >= buffer_w || x1 < 0 || x1 >= buffer_w || y0 < 0 || y0 >= buffer_h || y1 < 0 || y1 >= buffer_h)
  {
    int x2 = x0, y2 = y0, x3 = x1, y3 = y1;
    if(!include_last_pixel && (x1 < 0 || x1 > buffer_w || y1 < 0 || y1 > buffer_h)) include_last_pixel = true; //if include_last_pixel is false, don't clip too much...
    if(!lpi::clipLine(x0, y0, x1, y1, x2, y2, x3, y3, 0, 0, buffer_w, buffer_h)) return;
    
  }
  bresenhamLine(buffer, buffer_w, buffer_h, x0, y0, x1, y1, color, opacity, affect_alpha, include_last_pixel);
}

//#define UGLYLINEENDINGS

void drawThickLine(unsigned char* buffer, int u, int v, int u2, int x0, int y0, int x1, int y1, int thickness, const lpi::ColorRGB& color, double opacity, bool affect_alpha, bool wrapping)
{
  int radius = thickness / 2;
  
  if(lpi::template_abs(x1 - x0) < lpi::template_abs(y1 - y0))
  {
    double alpha = std::atan2(x1 - x0, y1 - y0);
    int pixnum = (int)(thickness / std::cos(alpha));
    if(pixnum < 0) pixnum = -pixnum;
    if(y0 > y1)
    {
      std::swap(y0, y1);
      std::swap(x0, x1);
    }
    
    int ystart = y0 - radius; if(!wrapping && ystart < 0) ystart = 0;
    int yend = y1 + radius; if(!wrapping && yend >v) yend = v;
    
    for(double y = ystart; y < yend; y++)
    {
      int xc = (int)(((y - y0) * (x1 - x0)) / (y1 - y0)) + x0;
      int xstart = xc - pixnum / 2; if(!wrapping && xstart < 0) xstart = 0;
      int xend = xc - pixnum / 2 + pixnum; if(!wrapping && xend > u) xend = u;
#ifdef UGLYLINEENDINGS
      if(y > y0 + radius && y < y1 - radius)
#else
      if(y > y0 && y < y1)
#endif
      {
        horLine(buffer, u, v, u2, (int)y, xstart, xend, color, opacity, affect_alpha, wrapping);
      }
#ifdef UGLYLINEENDINGS
      else
      {
        for(int x = xstart; x < xend; x++)
        {
          lpi::Vector2 p(x + 0.25, y + 0.25), a(x0, y0), b(x1, y1);
          if(lpi::distancePointLineSegmentSq(p, a, b) <= (radius * radius))
          {
            pset(buffer, u, v, u2, x, (int)y, color, opacity, affect_alpha, wrapping);
          }
        }
      }
#endif
    }
  }
  else
  {
    double alpha = std::atan2(y1 - y0, x1 - x0);
    int pixnum = (int)(thickness / std::cos(alpha));
    if(pixnum < 0) pixnum = -pixnum;
    if(x0 > x1)
    {
      std::swap(x0, x1);
      std::swap(y0, y1);
    }
    
    int xstart = x0 - radius; if(!wrapping && xstart < 0) xstart = 0;
    int xend = x1 + radius; if(!wrapping && xend > u) xend = u;
    
    for(double x = xstart; x < xend; x++)
    {
      int yc = (int)(((x - x0) * (y1 - y0)) / (x1 - x0)) + y0;
      
      int ystart = yc - pixnum / 2; if(!wrapping && ystart < 0) ystart = 0;
      int yend = yc - pixnum / 2 + pixnum; if(!wrapping && yend > v) yend = v;

#ifdef UGLYLINEENDINGS
      if(x > x0 + radius && x < x1 - radius)
#else
      if(x > x0 && x < x1)
#endif
      {
        verLine(buffer, u, v, u2, (int)x, ystart, yend, color, opacity, affect_alpha, wrapping);
      }
#ifdef UGLYLINEENDINGS
      else
      {
        for(int y = ystart; y < yend; y++)
        {
          lpi::Vector2 p(x + 0.25, y + 0.25), a(x0, y0), b(x1, y1);
          if(lpi::distancePointLineSegmentSq(p, a, b) <= (radius * radius))
          {
            pset(buffer, u, v, u2, (int)x, y, color, opacity, affect_alpha, wrapping);
          }
        }
      }
#endif
    }
  }
}

/*
about include_last_pixel:
 If true, the line is drawn including the end coordinates.
 If false, the end pixel isn't drawn. In that case, a next line which begins at that pixel, draws it instead. That is important for multi-lines with opacity. Only used for lines of one pixel thick!!
 If the begin and end pixel are the same however, then it IS drawn.
*/
void drawLine(unsigned char* buffer, int buffer_w, int buffer_h, int x0, int y0, int x1, int y1, int thickness, const lpi::ColorRGB& color, double opacity, bool affect_alpha, bool include_last_pixel)
{
  if(thickness <= 1)
  {
    clippedBresenhamLine(buffer, buffer_w, buffer_h, x0, y0, x1, y1, color, opacity, affect_alpha, include_last_pixel);
  }
  else if(thickness <= 3)
  {
    if(lpi::template_abs(x1 - x0) < lpi::template_abs(y1 - y0))
    {
      for(int i = -thickness / 2; i < -thickness / 2 + thickness; i++) clippedBresenhamLine(buffer, buffer_w, buffer_h, x0 + i, y0, x1 + i, y1, color, opacity, affect_alpha, include_last_pixel);
    }
    else
    {
      for(int i = -thickness / 2; i < -thickness / 2 + thickness; i++) clippedBresenhamLine(buffer, buffer_w, buffer_h, x0, y0 + i, x1, y1 + i, color, opacity, affect_alpha, include_last_pixel);
    }
  }
  else
  {
    drawThickLine(buffer, buffer_w, buffer_h, buffer_w, x0, y0, x1, y1, thickness, color, opacity, affect_alpha, false);
  }
}

////////////////////////////////////////////////////////////////////////////////

void scale2x(unsigned char* out, size_t ou, size_t ov, const unsigned char* in, size_t iu, size_t iv, bool wrapping)
{
  (void)ov;
  
  RGBA8* o = (RGBA8*)(&out[0]);
  const RGBA8* i = (const RGBA8*)(&in[0]);
  
  for(size_t y = 0; y < iv; y++)
  for(size_t x = 0; x < iu; x++)
  {
    size_t xm = x > 0 ? x - 1 : (wrapping ? iu - 1 : 0); //"x - 1"
    size_t ym = y > 0 ? y - 1 : (wrapping ? iv - 1 : 0); //"y - 1"
    size_t x1 = x < iu - 1 ? x + 1 : (wrapping ? 0 : iu - 1); //"x + 1"
    size_t y1 = y < iv - 1 ? y + 1 : (wrapping ? 0 : iv - 1); //"y + 1"
    
    //size_t iindexa = ym * iu + xm;
    size_t iindexb = ym * iu + x;
    //size_t iindexc = ym * iu + x1;
    size_t iindexd = y * iu + xm;
    size_t iindexe = y * iu + x;
    size_t iindexf = y * iu + x1;
    //size_t iindexg = y1 * iu + xm;
    size_t iindexh = y1 * iu + x;
    //size_t iindexi = y1 * iu + x1;
    
    size_t oindex0 = y * 2 * ou + x * 2;
    size_t oindex1 = y * 2 * ou + (x * 2 + 1);
    size_t oindex2 = (y * 2 + 1) * ou + x * 2;
    size_t oindex3 = (y * 2 + 1) * ou + (x * 2 + 1);
    
    if(i[iindexb] != i[iindexh] && i[iindexd] != i[iindexf])
    {
      o[oindex0] = (i[iindexd] == i[iindexb]) ? i[iindexd] : i[iindexe];
      o[oindex1] = (i[iindexb] == i[iindexf]) ? i[iindexf] : i[iindexe];
      o[oindex2] = (i[iindexd] == i[iindexh]) ? i[iindexd] : i[iindexe];
      o[oindex3] = (i[iindexh] == i[iindexf]) ? i[iindexf] : i[iindexe];
    }
      else
    {
      o[oindex0] = o[oindex1] = o[oindex2] = o[oindex3] = i[iindexe];
    }
  }
}


void scale3x(unsigned char* out, size_t ou, size_t ov, const unsigned char* in, size_t iu, size_t iv, bool wrapping)
{
  (void)ov;
  
  RGBA8* o = (RGBA8*)(&out[0]);
  const RGBA8* i = (const RGBA8*)(&in[0]);
  
  for(size_t y = 0; y < iv; y++)
  for(size_t x = 0; x < iu; x++)
  {
    size_t xm = x > 0 ? x - 1 : (wrapping ? iu - 1 : 0); //"x - 1"
    size_t ym = y > 0 ? y - 1 : (wrapping ? iv - 1 : 0); //"y - 1"
    size_t x1 = x < iu - 1 ? x + 1 : (wrapping ? 0 : iu - 1); //"x + 1"
    size_t y1 = y < iv - 1 ? y + 1 : (wrapping ? 0 : iv - 1); //"y + 1"
    
    size_t iindexa = ym * iu + xm;
    size_t iindexb = ym * iu + x;
    size_t iindexc = ym * iu + x1;
    size_t iindexd = y * iu + xm;
    size_t iindexe = y * iu + x;
    size_t iindexf = y * iu + x1;
    size_t iindexg = y1 * iu + xm;
    size_t iindexh = y1 * iu + x;
    size_t iindexi = y1 * iu + x1;
    
    size_t oindex0 = (y * 3 + 0) * ou + (x * 3 + 0);
    size_t oindex1 = (y * 3 + 0) * ou + (x * 3 + 1);
    size_t oindex2 = (y * 3 + 0) * ou + (x * 3 + 2);
    size_t oindex3 = (y * 3 + 1) * ou + (x * 3 + 0);
    size_t oindex4 = (y * 3 + 1) * ou + (x * 3 + 1);
    size_t oindex5 = (y * 3 + 1) * ou + (x * 3 + 2);
    size_t oindex6 = (y * 3 + 2) * ou + (x * 3 + 0);
    size_t oindex7 = (y * 3 + 2) * ou + (x * 3 + 1);
    size_t oindex8 = (y * 3 + 2) * ou + (x * 3 + 2);
    
    if (i[iindexb] != i[iindexh] && i[iindexd] != i[iindexf])
    {
      o[oindex0] = i[iindexd] == i[iindexb] ? i[iindexd] : i[iindexe];
      o[oindex1] = (i[iindexd] == i[iindexb] && i[iindexe] != i[iindexc]) || (i[iindexb] == i[iindexf] && i[iindexe] != i[iindexa]) ? i[iindexb] : i[iindexe];
      o[oindex2] = i[iindexb] == i[iindexf] ? i[iindexf] : i[iindexe];
      o[oindex3] = (i[iindexd] == i[iindexb] && i[iindexe] != i[iindexg]) || (i[iindexd] == i[iindexh] && i[iindexe] != i[iindexa]) ? i[iindexd] : i[iindexe];
      o[oindex4] = i[iindexe];
      o[oindex5] = (i[iindexb] == i[iindexf] && i[iindexe] != i[iindexi]) || (i[iindexh] == i[iindexf] && i[iindexe] != i[iindexc]) ? i[iindexf] : i[iindexe];
      o[oindex6] = i[iindexd] == i[iindexh] ? i[iindexd] : i[iindexe];
      o[oindex7] = (i[iindexd] == i[iindexh] && i[iindexe] != i[iindexi]) || (i[iindexh] == i[iindexf] && i[iindexe] != i[iindexg]) ? i[iindexh] : i[iindexe];
      o[oindex8] = i[iindexh] == i[iindexf] ? i[iindexf] : i[iindexe];
    }
    else
    {
      o[oindex0] = i[iindexe];
      o[oindex1] = i[iindexe];
      o[oindex2] = i[iindexe];
      o[oindex3] = i[iindexe];
      o[oindex4] = i[iindexe];
      o[oindex5] = i[iindexe];
      o[oindex6] = i[iindexe];
      o[oindex7] = i[iindexe];
      o[oindex8] = i[iindexe];
    }
  }
}

////////////////////////////////////////////////////////////////////////////////

void shiftImageSlow(TextureRGBA8& texture, int shiftx, int shifty)
{
  //TODO: make use less memory

  unsigned char* buffer2 = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();
  
  shiftx = lpi::wrap(shiftx, u);
  shifty = lpi::wrap(shifty, v);

  std::vector<unsigned char> buffer(u * v * 4);

  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  for(size_t c = 0; c < 4; c++)
  {
    size_t index = y * u * 4 + x * 4 + c;
    size_t index2 = y * u2 * 4 + x * 4 + c;
    buffer[index] = buffer2[index2];
  }

  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  for(size_t c = 0; c < 4; c++)
  {
    size_t x2 = x + shiftx; if(x2 >= u) x2 -= u;
    size_t y2 = y + shifty; if(y2 >= v) y2 -= v;
    size_t index = y * u * 4 + x * 4 + c;
    size_t index2 = y2 * u2 * 4 + x2 * 4 + c;
    buffer2[index2] = buffer[index];
  }
}

void shiftImageSlow(TextureRGBA32F& texture, int shiftx, int shifty)
{
  //TODO: make use less memory

  float* buffer2 = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();

  std::vector<float> buffer(u * v * 4);

  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  for(size_t c = 0; c < 4; c++)
  {
    size_t index = y * u * 4 + x * 4 + c;
    size_t index2 = y * u2 * 4 + x * 4 + c;
    buffer[index] = buffer2[index2];
  }

  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  for(size_t c = 0; c < 4; c++)
  {
    size_t x2 = x + shiftx; if(x2 >= u) x2 -= u;
    size_t y2 = y + shifty; if(y2 >= v) y2 -= v;
    size_t index = y * u * 4 + x * 4 + c;
    size_t index2 = y2 * u2 * 4 + x2 * 4 + c;
    buffer2[index2] = buffer[index];
  }
}


////////////////////////////////////////////////////////////////////////////////

