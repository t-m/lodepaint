/*
LodePaint

Copyright (c) 2009 Lode Vandevenne
All rights reserved.

This file is part of LodePaint.

LodePaint is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LodePaint is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LodePaint.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "paint_palette.h"

void Palette::init()
{
  palette = new lpi::gui::MultiColorPalette;
}

void Palette::cleanup()
{
  delete palette;
}

void generatePaletteDefault(Palette& palette)
{
  lpi::gui::MultiColorPalette& pal = *palette.palette;
  
  palette.name = "Default";

  pal.generateSimple12x2();
}

void generatePaletteRoyal(Palette& palette)
{
  lpi::gui::MultiColorPalette& pal = *palette.palette;

  palette.name = "Royal";
  
  pal.setPaletteSize(16, 2);
  pal.setColor255(0, lpi::ColorRGB(0,0,0));
  pal.setColor255(1, lpi::ColorRGB(76,0,100));
  pal.setColor255(2, lpi::ColorRGB(88,0,120));
  pal.setColor255(3, lpi::ColorRGB(104,0,140));
  pal.setColor255(4, lpi::ColorRGB(124,4,160));
  pal.setColor255(5, lpi::ColorRGB(156,68,184));
  pal.setColor255(6, lpi::ColorRGB(188,132,208));
  pal.setColor255(7, lpi::ColorRGB(220,196,228));
  pal.setColor255(8, lpi::ColorRGB(255,255,255));
  pal.setColor255(9, lpi::ColorRGB(252,252,186));
  pal.setColor255(10, lpi::ColorRGB(252,252,120));
  pal.setColor255(11, lpi::ColorRGB(252,252,56));
  pal.setColor255(12, lpi::ColorRGB(248,244,0));
  pal.setColor255(13, lpi::ColorRGB(200,180,20));
  pal.setColor255(14, lpi::ColorRGB(148,116,40));
  pal.setColor255(15, lpi::ColorRGB(100,52,64));

  pal.setColor255(16, lpi::ColorRGB(68,0,88));
  pal.setColor255(17, lpi::ColorRGB(80,0,108));
  pal.setColor255(18, lpi::ColorRGB(96,0,132));
  pal.setColor255(19, lpi::ColorRGB(112,0,152));
  pal.setColor255(20, lpi::ColorRGB(140,36,172));
  pal.setColor255(21, lpi::ColorRGB(172,100,196));
  pal.setColor255(22, lpi::ColorRGB(204,164,220));
  pal.setColor255(23, lpi::ColorRGB(236,228,240));
  pal.setColor255(24, lpi::ColorRGB(252,252,216));
  pal.setColor255(25, lpi::ColorRGB(252,252,152));
  pal.setColor255(26, lpi::ColorRGB(252,252,88));
  pal.setColor255(27, lpi::ColorRGB(252,252,24));
  pal.setColor255(28, lpi::ColorRGB(224,212,12));
  pal.setColor255(29, lpi::ColorRGB(172,148,32));
  pal.setColor255(30, lpi::ColorRGB(124,84,52));
  pal.setColor255(31, lpi::ColorRGB(76,20,72));
}

void generatePalettePixelArt(Palette& palette)
{
  lpi::gui::MultiColorPalette& pal = *palette.palette;

  palette.name = "Pixel Art";

  //The pixel art palette has various 4-color transitions inspired by good pixel art paintings
  
  int N = 21;
  pal.setPaletteSize(N, 2);
  pal.setColor255(0 + 0, lpi::ColorRGB(0,0,0));
  pal.setColor255(N + 0, lpi::ColorRGB(85,85,85));
  pal.setColor255(0 + 1, lpi::ColorRGB(140,140,140));
  pal.setColor255(N + 1, lpi::ColorRGB(170,170,170));
  pal.setColor255(0 + 2, lpi::ColorRGB(200,200,200));
  pal.setColor255(N + 2, lpi::ColorRGB(255,255,255));

  pal.setColor255(0 + 3, lpi::ColorRGB(80,96,168));
  pal.setColor255(N + 3, lpi::ColorRGB(112,120,208));
  pal.setColor255(0 + 4, lpi::ColorRGB(128,144,216));
  pal.setColor255(N + 4, lpi::ColorRGB(184,192,240));

  pal.setColor255(0 + 5, lpi::ColorRGB(38,72,72));
  pal.setColor255(N + 5, lpi::ColorRGB(44,104,72));
  pal.setColor255(0 + 6, lpi::ColorRGB(44,144,80));
  pal.setColor255(N + 6, lpi::ColorRGB(72,192,72));

  pal.setColor255(0 + 7, lpi::ColorRGB(116,130,48));
  pal.setColor255(N + 7, lpi::ColorRGB(150,166,54));
  pal.setColor255(0 + 8, lpi::ColorRGB(192,210,50));
  pal.setColor255(N + 8, lpi::ColorRGB(224,236,56));

  pal.setColor255(0 + 9, lpi::ColorRGB(0,94,94));
  pal.setColor255(N + 9, lpi::ColorRGB(36,160,160));
  pal.setColor255(0 + 10, lpi::ColorRGB(150,220,220));
  pal.setColor255(N + 10, lpi::ColorRGB(180,255,255));

  pal.setColor255(0 + 11, lpi::ColorRGB(106,46,60));
  pal.setColor255(N + 11, lpi::ColorRGB(200,50,20));
  pal.setColor255(0 + 12, lpi::ColorRGB(248,148,90));
  pal.setColor255(N + 12, lpi::ColorRGB(255,212,160));

  pal.setColor255(0 + 13, lpi::ColorRGB(200,92,136));
  pal.setColor255(N + 13, lpi::ColorRGB(220,120,170));
  pal.setColor255(0 + 14, lpi::ColorRGB(248,168,180));
  pal.setColor255(N + 14, lpi::ColorRGB(248,200,200));

  pal.setColor255(0 + 15, lpi::ColorRGB(220,138,70));
  pal.setColor255(N + 15, lpi::ColorRGB(255,200,20));
  pal.setColor255(0 + 16, lpi::ColorRGB(224,208,0));
  pal.setColor255(N + 16, lpi::ColorRGB(255,240,0));

  pal.setColor255(0 + 17, lpi::ColorRGB(172,154,70));
  pal.setColor255(N + 17, lpi::ColorRGB(192,168,76));
  pal.setColor255(0 + 18, lpi::ColorRGB(202,180,80));
  pal.setColor255(N + 18, lpi::ColorRGB(220,196,96));

  pal.setColor255(0 + 19, lpi::ColorRGB(80,64,48));
  pal.setColor255(N + 19, lpi::ColorRGB(104,96,72));
  pal.setColor255(0 + 20, lpi::ColorRGB(144,136,112));
  pal.setColor255(N + 20, lpi::ColorRGB(208,200,184));
}



void generatePaletteGreyScale(Palette& palette)
{
  lpi::gui::MultiColorPalette& pal = *palette.palette;

  palette.name = "Grey";

  pal.setPaletteSize(16, 2);
  for(size_t i = 0; i < 32; i++)
  {
    pal.setColor255(i, lpi::ColorRGB(i*8,i*8,i*8));
  }
}

void generatePaletteCGA(Palette& palette, bool highest252)
{
  lpi::gui::MultiColorPalette& pal = *palette.palette;

  palette.name = highest252 ? "CGA (252)" : "CGA (255)";

  pal.setPaletteSize(8, 2);
  
  /*
  There are two versions of this palette out there: one where the hex values
  used by color channels are 0x55, 0xAA and 0xFF (decimal: 85, 170 and 255)
  and one where the values are 0x54, 0xA8, 0xFC (decimal: 84, 168, 252).
  
  The first has the best range and is given on Wikipedia. But the other (where
  the max value is only 252) is more true to the screenshots of good old DOS games,
  so that version is chosen here.
  */

  int i = 0;

  if(highest252)
  {
    //True version
    pal.setColor255(i++, lpi::ColorRGB(0,0,0));
    pal.setColor255(i++, lpi::ColorRGB(0,0,168));
    pal.setColor255(i++, lpi::ColorRGB(0,168,0));
    pal.setColor255(i++, lpi::ColorRGB(0,168,168));
    pal.setColor255(i++, lpi::ColorRGB(168,0,0));
    pal.setColor255(i++, lpi::ColorRGB(168,0,168));
    pal.setColor255(i++, lpi::ColorRGB(168,84,0));
    pal.setColor255(i++, lpi::ColorRGB(168,168,168));
    pal.setColor255(i++, lpi::ColorRGB(84,84,84));
    pal.setColor255(i++, lpi::ColorRGB(84,84,252));
    pal.setColor255(i++, lpi::ColorRGB(84,252,84));
    pal.setColor255(i++, lpi::ColorRGB(84,252,252));
    pal.setColor255(i++, lpi::ColorRGB(252,84,84));
    pal.setColor255(i++, lpi::ColorRGB(252,84,252));
    pal.setColor255(i++, lpi::ColorRGB(252,252,84));
    pal.setColor255(i++, lpi::ColorRGB(252,252,252));
  }
  else
  {
    //Wikipedia Version
    pal.setColor255(i++, lpi::ColorRGB(0,0,0));
    pal.setColor255(i++, lpi::ColorRGB(0,0,170));
    pal.setColor255(i++, lpi::ColorRGB(0,170,0));
    pal.setColor255(i++, lpi::ColorRGB(0,170,170));
    pal.setColor255(i++, lpi::ColorRGB(170,0,0));
    pal.setColor255(i++, lpi::ColorRGB(170,0,170));
    pal.setColor255(i++, lpi::ColorRGB(170,85,0));
    pal.setColor255(i++, lpi::ColorRGB(170,170,170));
    pal.setColor255(i++, lpi::ColorRGB(85,85,85));
    pal.setColor255(i++, lpi::ColorRGB(85,85,255));
    pal.setColor255(i++, lpi::ColorRGB(85,255,85));
    pal.setColor255(i++, lpi::ColorRGB(85,255,255));
    pal.setColor255(i++, lpi::ColorRGB(255,85,85));
    pal.setColor255(i++, lpi::ColorRGB(255,85,255));
    pal.setColor255(i++, lpi::ColorRGB(255,255,85));
    pal.setColor255(i++, lpi::ColorRGB(255,255,255));
  }
}


void generatePaletteTango(Palette& palette)
{
  lpi::gui::MultiColorPalette& pal = *palette.palette;

  palette.name = "Tango";
  
  /*
  This is the palette recommended by the Tango Icon Theme Guidelines, for Tango Desktop Icons.
  http://tango.freedesktop.org/Generic_Icon_Theme_Guidelines
  */

  pal.setPaletteSize(9, 3);
  
  int i = 0;
  pal.setColor255(i++, lpi::ColorRGB(0xfc,0xe9,0x4f));
  pal.setColor255(i++, lpi::ColorRGB(0xfc,0xaf,0x3e));
  pal.setColor255(i++, lpi::ColorRGB(0xe9,0xb9,0x6e));
  pal.setColor255(i++, lpi::ColorRGB(0x8a,0xe2,0x34));
  pal.setColor255(i++, lpi::ColorRGB(0x72,0x9f,0xcf));
  pal.setColor255(i++, lpi::ColorRGB(0xad,0x7f,0xa8));
  pal.setColor255(i++, lpi::ColorRGB(0xef,0x29,0x29));
  pal.setColor255(i++, lpi::ColorRGB(0xee,0xee,0xec));
  pal.setColor255(i++, lpi::ColorRGB(0x88,0x8a,0x85));
  
  pal.setColor255(i++, lpi::ColorRGB(0xed,0xd4,0x00));
  pal.setColor255(i++, lpi::ColorRGB(0xf5,0x79,0x00));
  pal.setColor255(i++, lpi::ColorRGB(0xc1,0x7d,0x11));
  pal.setColor255(i++, lpi::ColorRGB(0x73,0xd2,0x16));
  pal.setColor255(i++, lpi::ColorRGB(0x34,0x65,0xa4));
  pal.setColor255(i++, lpi::ColorRGB(0x75,0x50,0x7b));
  pal.setColor255(i++, lpi::ColorRGB(0xcc,0x00,0x00));
  pal.setColor255(i++, lpi::ColorRGB(0xd3,0xd7,0xcf));
  pal.setColor255(i++, lpi::ColorRGB(0x55,0x57,0x53));
  
  pal.setColor255(i++, lpi::ColorRGB(0xc4,0xa0,0x00));
  pal.setColor255(i++, lpi::ColorRGB(0xce,0x5c,0x00));
  pal.setColor255(i++, lpi::ColorRGB(0x8f,0x59,0x02));
  pal.setColor255(i++, lpi::ColorRGB(0x4e,0x9a,0x06));
  pal.setColor255(i++, lpi::ColorRGB(0x20,0x4a,0x87));
  pal.setColor255(i++, lpi::ColorRGB(0x5c,0x35,0x66));
  pal.setColor255(i++, lpi::ColorRGB(0xa4,0x00,0x00));
  pal.setColor255(i++, lpi::ColorRGB(0xba,0xbd,0xb6));
  pal.setColor255(i++, lpi::ColorRGB(0x2e,0x34,0x36));
}

void generatePaletteCrayons(Palette& palette)
{
  lpi::gui::MultiColorPalette& pal = *palette.palette;

  palette.name = "Crayons";

  /*
  This is the palette recommended by the Tango Icon Theme Guidelines, for Tango Desktop Icons.
  http://tango.freedesktop.org/Generic_Icon_Theme_Guidelines
  */

  pal.setPaletteSize(32, 1);

  int i = 0;
  pal.setColor255(i++, lpi::ColorRGB(255,255,255));
  pal.setColor255(i++, lpi::ColorRGB(255,215,180));
  pal.setColor255(i++, lpi::ColorRGB(255,255,42));
  pal.setColor255(i++, lpi::ColorRGB(255,185,20));
  pal.setColor255(i++, lpi::ColorRGB(240,160,0));
  //pal.setColor255(i++, lpi::ColorRGB(220,140,80));
  pal.setColor255(i++, lpi::ColorRGB(200,100,32));
  pal.setColor255(i++, lpi::ColorRGB(240,128,25));
  pal.setColor255(i++, lpi::ColorRGB(240,100,25));
  pal.setColor255(i++, lpi::ColorRGB(230,50,20));
  //pal.setColor255(i++, lpi::ColorRGB(224,32,20));
  pal.setColor255(i++, lpi::ColorRGB(224,42,64));
  pal.setColor255(i++, lpi::ColorRGB(220,20,20));
  pal.setColor255(i++, lpi::ColorRGB(255,50,40));
  pal.setColor255(i++, lpi::ColorRGB(240,70,128));
  pal.setColor255(i++, lpi::ColorRGB(192,128,170));
  pal.setColor255(i++, lpi::ColorRGB(170,40,100));
  pal.setColor255(i++, lpi::ColorRGB(0,32,128));

  pal.setColor255(i++, lpi::ColorRGB(25,90,160));
  //pal.setColor255(i++, lpi::ColorRGB(0,80,145));
  pal.setColor255(i++, lpi::ColorRGB(25,120,160));
  pal.setColor255(i++, lpi::ColorRGB(0,128,208));
  pal.setColor255(i++, lpi::ColorRGB(0,160,200));
  pal.setColor255(i++, lpi::ColorRGB(0,144,128));
  pal.setColor255(i++, lpi::ColorRGB(120,180,20));
  pal.setColor255(i++, lpi::ColorRGB(32,120,32));
  pal.setColor255(i++, lpi::ColorRGB(0,100,40));
  //pal.setColor255(i++, lpi::ColorRGB(0,108,56));
  pal.setColor255(i++, lpi::ColorRGB(96,120,25));
  pal.setColor255(i++, lpi::ColorRGB(64,96,20));
  pal.setColor255(i++, lpi::ColorRGB(160,84,32));
  pal.setColor255(i++, lpi::ColorRGB(80,40,20));
  pal.setColor255(i++, lpi::ColorRGB(64,32,32));
  pal.setColor255(i++, lpi::ColorRGB(200,200,200));
  pal.setColor255(i++, lpi::ColorRGB(128,128,128));
  pal.setColor255(i++, lpi::ColorRGB(0,0,0));

}
