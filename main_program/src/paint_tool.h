/*
LodePaint

Copyright (c) 2009-2010 Lode Vandevenne
All rights reserved.

This file is part of LodePaint.

LodePaint is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LodePaint is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LodePaint.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "lpi/gio/lpi_draw2d_buffer.h"
#include "lpi/gui/lpi_gui.h"
#include "lpi/gio/lpi_input.h"

#include "paint_window.h"
#include "paint_gui_operation.h"
#include "paint_operation.h"
#include "paint_skin.h"

class BrushSettingWindow;
class ColorWindow;

class ITool : public IOperation //tool = interactive operation (as opposed to something that works in one click, that is called "filter")
{
  public:
  
  public:
    ITool(GlobalToolSettings& settings);
    virtual void handle(const lpi::IInput& input, Paint& paint) = 0;
    virtual void draw(lpi::gui::IGUIDrawer& drawer, Paint& paint) { (void)drawer; (void)paint; }  //draw indications of the tool, such as brush outlines, ...
    
    virtual bool done() { return false; } //returns true once per time that it wants to store undo information. So if this returns true, then you should call getUndoInfo next and add this to the undo stack. Example: user released mouse button after drawing with brush. That is the point where it returns true and wants to store the original image data under the brush in the undo state.
    virtual void getUndoState(UndoState& state) { (void)state; } //when done() was true, let it move its undo data into the given undostate, which can be used with the "undo" function later on if needed. The GUI elements should edit the parameters of the operation directly.
    
    virtual std::string getToolTip() const { return getLabel(); }

    void drawPenPixel(lpi::gui::IGUIDrawer& drawer, Paint& paint); //utility function, draws rectangle around pixel over which mouse is if zoomed in deep enough
    void drawSpot(lpi::gui::IGUIDrawer& drawer, Paint& paint, int x, int y); //x, y are coordinates on the drawing
    void drawCommonCrosshair(lpi::gui::IGUIDrawer& drawer, Paint& paint, int position_x, int position_y); //position: -1 for left/top, 0 for center of pixel, 1 for right/bottom, 2 for exact mouse pos
    void drawSmallCrosshair(lpi::gui::IGUIDrawer& drawer, Paint& paint, int position_x, int position_y, int size); 
    void drawOwnIcon(lpi::gui::IGUIDrawer& drawer, Paint& paint, int hotspot_x, int hotspot_y); //hotspot = the pixel of the image that should be at the mouse point
    
    void addBrushSettingsToPage(lpi::gui::DynamicPage& page, const lpi::gui::IGUIDrawer& geom); //another utility function
    void addPenSettingsToPage(lpi::gui::DynamicPage& page, const lpi::gui::IGUIDrawer& geom);
    void addGeomSettingsToPage(lpi::gui::DynamicPage& page, const lpi::gui::IGUIDrawer& geom);
    
    /*
    getDialog:
    Return a composite GUI element with your settings.
    Return 0 if no dialog
    Don't add a window or frame around the dialog, the painting program does that (it adds it in a tab)
    Don't add an OK and cancel button, there shouldn't be any (this is added in a tab).
    DO make sure that the tool knows the settings from the dialog at all times, because the user can change
    settings in this dialog and use the tool on the painting at any time.
    */
    virtual lpi::gui::Element* getDialog() { return 0; }
};

class DialogTool : public ITool
{
  protected:
  
    DynamicParameters params;
    DynamicPageWithAutoUpdate dialog;
  
  public:
  
    DialogTool(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom);
    
    virtual lpi::gui::Element* getDialog() { return &dialog; }
};

class GeomDialogTool : public ITool
{
  protected:
  
    DynamicPageWithAutoUpdate dialog;
  
  public:
  
    GeomDialogTool(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom)
    : ITool(settings)
    {
      addGeomSettingsToPage(dialog, geom);
    }
    
    virtual lpi::gui::Element* getDialog() { return &dialog; }
};
