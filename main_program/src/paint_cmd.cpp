/*
LodePaint

Copyright (c) 2009-2010 Lode Vandevenne
All rights reserved.

This file is part of LodePaint.

LodePaint is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LodePaint is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LodePaint.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "paint_cmd.h"

#include <iostream>

#include "main.h"

#include "lpi/lpi_os.h"

#if defined(LPI_OS_WINDOWS)
#include <windows.h>
#endif

bool processCommandLine(std::string& commandLineFileName, Options& options, CMDOptions& cmdoptions, int argc, char* argv[])
{
  std::vector<std::string> arg(argc);
  options.command  = "";

  for(int i = 0; i < argc; i++)
  {
    arg[i] = argv[i];
    options.command += arg[i] + " ";
  }

  for(int i = 1; i < argc; i++)
  {
    if(arg[i] == "-v" || arg[i] == "--version")
    {
      std::cout << "LodePaint version " + LODEPAINT_VERSION << std::endl;
      return false;
    }
    else if(arg[i] == "-h" || arg[i] == "--help")
    {
      std::cout << "Usage:\n\
  lodepaint [options] [FILE]\n\
\n\
LodePaint - Painting Program using OpenGL\n\
\n\
Options:\n\
  -h, --help                 Show command line help options\n\
  -v, --version              Show version information\n\
  --license                  Show license information\n\
  --fullscreen               Run program in fullscreen mode (not recommended)\n\
  --nofullscreen, --window   Run program in windowed mode (recommended)\n\
  --resizable                Allow resizing program window (recommended)\n\
  --noresizable              Lock window size (use in case of problems)\n\
  --datadir PATH             Override whatever it normally chooses as Data path (for skins, languages, ...)\n\
  --plugindir PATH           Override whatever it normally chooses as Plugins path\n\
\n\
Arguments:\n\
  FILE: image file to open\n\
";
      return false;
    }
    if(arg[i] == "--license")
    {
      std::cout << "LodePaint version " + LODEPAINT_VERSION << "\
\n\
Copyright (c) 2009-2010 Lode Vandevenne\n\
All rights reserved.\n\
\n\
LodePaint is free software: you can redistribute it and/or modify\n\
it under the terms of the GNU General Public License as published by\n\
the Free Software Foundation, either version 3 of the License, or\n\
(at your option) any later version.\n\
\n\
LodePaint is distributed in the hope that it will be useful,\n\
but WITHOUT ANY WARRANTY; without even the implied warranty of\n\
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n\
GNU General Public License for more details.\n\
\n\
You should have received a copy of the GNU General Public License\n\
along with LodePaint.  If not, see <http://www.gnu.org/licenses/>.\n\
";
      return false;
    }
    else if(arg[i] == "--fullscreen")
    {
      cmdoptions.override_fullscreen = true,
      cmdoptions.fullscreen = true;
    }
    else if(arg[i] == "--nofullscreen" || arg[i] == "--window")
    {
      cmdoptions.override_fullscreen = true;
      cmdoptions.fullscreen = false;
    }
    else if(arg[i] == "--resizable")
    {
      cmdoptions.override_resizable = true;
      cmdoptions.resizable = true;
    }
    else if(arg[i] == "--noresizable")
    {
      cmdoptions.override_resizable = true;
      cmdoptions.resizable = false;
    }
    else if(arg[i] == "--datadir")
    {
      i++;
      if (i < argc) {
        options.paths.datadir = arg[i];
        lpi::ensureDirectoryEndSlash(options.paths.datadir);
      }
    }
    else if(arg[i] == "--plugindir")
    {
      i++;
      if (i < argc) {
        options.paths.plugindir = arg[i];
        lpi::ensureDirectoryEndSlash(options.paths.plugindir);
      }
    }
    else
    {
      commandLineFileName = arg[i];
    }
  }


  options.paths.exedir = "";
#if defined(LPI_OS_WINDOWS)
  {
    char c[MAX_PATH];
    GetModuleFileName(GetModuleHandle(NULL),c,MAX_PATH);
    std::string s = c;
    size_t slashpos = s.find_last_of("\\/");
    if (slashpos < s.size())
      options.paths.exedir = s.substr(0, slashpos) + "\\";
  }
#else
  if(argc > 0)
  {
    std::string s = argv[0];
    size_t slashpos = s.find_last_of("\\/");
    if (slashpos < s.size())
      options.paths.exedir = s.substr(0, slashpos) + "/";
  }
#endif

  return true;
}
