/*
LodePaint

Copyright (c) 2009-2012 Lode Vandevenne
All rights reserved.

This file is part of LodePaint.

LodePaint is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LodePaint is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LodePaint.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "paint_filter_image.h"


#include "lpi/gui/lpi_gui_color.h"
#include "lpi/lpi_math.h"
#include "lpi/lpi_math2d.h"

#include "imalg/blur.h"
#include "imalg/colorop.h"
#include "imalg/hqnx.h"
#include "imalg/reduction.h"
#include "imalg/transform.h"

#include "paint_algos.h"
#include "paint_clipboard.h"

#include <cfloat>
#include <cmath>
#include <iostream>
#include <limits>

////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////

OperationRepeat::OperationRepeat(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom)
: AOperationFilterBase(settings)
, numx(2)
, numy(2)
{
  params.addInt("Num X", &numx, 1, 4, 1);
  params.addInt("Num Y", &numy, 1, 4, 1);

  paramsToDialog(dialog, params, geom);
}

void OperationRepeat::doit(Paint& paint, int numx, int numy, Progress&)
{
  TextureRGBA8& texture = paint.canvasRGBA8;
  size_t oldu = texture.getU();
  size_t oldv = texture.getV();

  std::vector<unsigned char> old;
  getAlignedBuffer(old, &texture);

  size_t u = oldu * numx;
  size_t v = oldv * numy;

  texture.setSize(u, v);

  unsigned char* buffer = texture.getBuffer();
  size_t u2 = texture.getU2();
  
  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  for(size_t c = 0; c < 4; c++)
  {
    size_t oindex = y * u2 * 4 + x * 4 + c; //output index
    size_t ix = x % oldu;
    size_t iy = y % oldv;
    size_t iindex = iy * oldu * 4 + ix * 4 + c; //input index
    buffer[oindex] = old[iindex];
  }
}



bool OperationRepeat::operate(Paint& paint, UndoState& state, Progress& progress)
{
  //not thread safe
  (void)paint;
  (void)state;
  (void)progress;
  return true;
}

bool OperationRepeat::operateGL(Paint& paint, UndoState& state)
{
  state.addInt(numx);
  state.addInt(numy);
  
  doit(paint, numx, numy, dummyProgress);
  paint.update();
  
  return true;
}

void OperationRepeat::undo(Paint& paint, UndoState& state)
{
  UndoStateIndex index;
  int numx = state.getInt(index.iint++);
  int numy = state.getInt(index.iint++);
  
  TextureRGBA8& texture = paint.canvasRGBA8;
  size_t oldu = texture.getU();
  size_t oldv = texture.getV();

  std::vector<unsigned char> old;
  getAlignedBuffer(old, &texture);

  size_t u = oldu / numx;
  size_t v = oldv / numy;

  texture.setSize(u, v);

  unsigned char* buffer = texture.getBuffer();
  size_t u2 = texture.getU2();

  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  for(size_t c = 0; c < 4; c++)
  {
    size_t oindex = y * u2 * 4 + x * 4 + c; //output index
    size_t iindex = y * oldu * 4 + x * 4 + c; //input index
    buffer[oindex] = old[iindex];
  }

  paint.update();
}

void OperationRepeat::redo(Paint& paint, UndoState& state)
{
  UndoStateIndex index;
  int numx = state.getInt(index.iint++);
  int numy = state.getInt(index.iint++);
  
  doit(paint, numx, numy, dummyProgress);
  paint.update();
}

////////////////////////////////////////////////////////////////////////////////

void OperationRepeat2x2::doit(Paint& paint, Progress&)
{
  TextureRGBA8& texture = paint.canvasRGBA8;
  size_t oldu = texture.getU();
  size_t oldv = texture.getV();

  std::vector<unsigned char> old;
  getAlignedBuffer(old, &texture);

  size_t u = oldu * 2;
  size_t v = oldv * 2;

  texture.setSize(u, v);

  unsigned char* buffer = texture.getBuffer();
  size_t u2 = texture.getU2();
  
  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  for(size_t c = 0; c < 4; c++)
  {
    size_t oindex = y * u2 * 4 + x * 4 + c; //output index
    size_t ix = x % oldu;
    size_t iy = y % oldv;
    size_t iindex = iy * oldu * 4 + ix * 4 + c; //input index
    buffer[oindex] = old[iindex];
  }
}



bool OperationRepeat2x2::operate(Paint& paint, UndoState& state, Progress& progress)
{
  //not thread safe
  (void)paint;
  (void)state;
  (void)progress;
  return true;
}

bool OperationRepeat2x2::operateGL(Paint& paint, UndoState& state)
{
  (void)state;
  doit(paint, dummyProgress);
  paint.update();

  return true;
}

void OperationRepeat2x2::undo(Paint& paint, UndoState& state)
{
  (void)state;
  
  TextureRGBA8& texture = paint.canvasRGBA8;
  size_t oldu = texture.getU();
  size_t oldv = texture.getV();

  std::vector<unsigned char> old;
  getAlignedBuffer(old, &texture);

  size_t u = oldu / 2;
  size_t v = oldv / 2;

  texture.setSize(u, v);

  unsigned char* buffer = texture.getBuffer();
  size_t u2 = texture.getU2();

  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  for(size_t c = 0; c < 4; c++)
  {
    size_t oindex = y * u2 * 4 + x * 4 + c; //output index
    size_t iindex = y * oldu * 4 + x * 4 + c; //input index
    buffer[oindex] = old[iindex];
  }

  paint.update();
}

void OperationRepeat2x2::redo(Paint& paint, UndoState& state)
{
  (void)state;
  doit(paint, dummyProgress);
  paint.update();
}

////////////////////////////////////////////////////////////////////////////////

void OperationMirrorTile::doit(Paint& paint, Progress&)
{
  TextureRGBA8& texture = paint.canvasRGBA8;
  size_t oldu = texture.getU();
  size_t oldv = texture.getV();

  std::vector<unsigned char> old;
  getAlignedBuffer(old, &texture);

  size_t u = oldu * 2;
  size_t v = oldv * 2;

  texture.setSize(u, v);

  unsigned char* buffer = texture.getBuffer();
  size_t u2 = texture.getU2();
  
  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  for(size_t c = 0; c < 4; c++)
  {
    size_t oindex = y * u2 * 4 + x * 4 + c; //output index
    size_t ix = x < oldu ? x : 2 * oldu - x - 1;
    size_t iy = y < oldv ? y : 2 * oldv - y - 1;
    size_t iindex = iy * oldu * 4 + ix * 4 + c; //input index
    buffer[oindex] = old[iindex];
  }
}



bool OperationMirrorTile::operate(Paint& paint, UndoState& state, Progress& progress)
{
  //not thread safe
  (void)paint;
  (void)state;
  (void)progress;
  return true;
}

bool OperationMirrorTile::operateGL(Paint& paint, UndoState& state)
{
  (void)state;
  doit(paint, dummyProgress);
  paint.update();

  return true;
}

void OperationMirrorTile::undo(Paint& paint, UndoState& state)
{
  (void)state;
  
  TextureRGBA8& texture = paint.canvasRGBA8;
  size_t oldu = texture.getU();
  size_t oldv = texture.getV();

  std::vector<unsigned char> old;
  getAlignedBuffer(old, &texture);

  size_t u = oldu / 2;
  size_t v = oldv / 2;

  texture.setSize(u, v);

  unsigned char* buffer = texture.getBuffer();
  size_t u2 = texture.getU2();

  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  for(size_t c = 0; c < 4; c++)
  {
    size_t oindex = y * u2 * 4 + x * 4 + c; //output index
    size_t iindex = y * oldu * 4 + x * 4 + c; //input index
    buffer[oindex] = old[iindex];
  }

  paint.update();
}

void OperationMirrorTile::redo(Paint& paint, UndoState& state)
{
  (void)state;
  doit(paint, dummyProgress);
  paint.update();
}

////////////////////////////////////////////////////////////////////////////////

void OperationSwapColors::doit(TextureRGBA8& texture, const GlobalToolSettings& settings, Progress&)
{
  lpi::ColorRGB a = lpi::RGBdtoRGB(settings.leftColor);
  lpi::ColorRGB b = lpi::RGBdtoRGB(settings.rightColor);
  
  unsigned char* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();
  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  {
    
    size_t index = y * u2 * 4 + x * 4;
    if(buffer[index + 0] == a.r && buffer[index + 1] == a.g && buffer[index + 2] == a.b && buffer[index + 3] == a.a)
    {
      buffer[index + 0] = b.r;
      buffer[index + 1] = b.g;
      buffer[index + 2] = b.b;
      buffer[index + 3] = b.a;
    }
    else if(buffer[index + 0] == b.r && buffer[index + 1] == b.g && buffer[index + 2] == b.b && buffer[index + 3] == b.a)
    {
      buffer[index + 0] = a.r;
      buffer[index + 1] = a.g;
      buffer[index + 2] = a.b;
      buffer[index + 3] = a.a;
    }
  }
}

////////////////////////////////////////////////////////////////////////////////

//sets most common color to BG, other colors to FG
void OperationOneBitFGBG::doit(TextureRGBA8& texture, const GlobalToolSettings& settings, Progress&)
{
  lpi::ColorRGB a = lpi::RGBdtoRGB(settings.leftColor);
  lpi::ColorRGB b = lpi::RGBdtoRGB(settings.rightColor);
  
  unsigned char* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();

  //Find most common color, that becomes the background
  std::map<unsigned, unsigned> m;
  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  {
    size_t index = y * u2 * 4 + x * 4;
    unsigned rgba = buffer[index] * 256 * 256 * 256 + buffer[index + 1] * 256 * 256 + buffer[index + 2] * 256 + buffer[index + 3];
    m[rgba]++;
  }

  unsigned biggest = 0;
  unsigned color = 0;
  for(std::map<unsigned, unsigned>::const_iterator it = m.begin(); it != m.end(); it++)
  {
    if(it->second > biggest)
    {
      biggest = it->second;
      color = it->first;
    }
  }

  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  {
    size_t index = y * u2 * 4 + x * 4;
    unsigned rgba = buffer[index] * 256 * 256 * 256 + buffer[index + 1] * 256 * 256 + buffer[index + 2] * 256 + buffer[index + 3];
    if(rgba == color)
    {
      buffer[index + 0] = b.r;
      buffer[index + 1] = b.g;
      buffer[index + 2] = b.b;
      buffer[index + 3] = b.a;
    }
    else
    {
      buffer[index + 0] = a.r;
      buffer[index + 1] = a.g;
      buffer[index + 2] = a.b;
      buffer[index + 3] = a.a;
    }
  }
}

////////////////////////////////////////////////////////////////////////////////

void OperationColorAverage::doit(TextureRGBA8& texture, Progress&)
{
  unsigned char* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();

  double tr = 0, tg = 0, tb = 0, ta = 0; //total average color

  // The std::map is used to group colors so that there's less to sort.
  // The std::map also does the actual sorting
  std::map<unsigned, unsigned> m;
  for(size_t y = 0; y < v; y++)
  {
    double sr = 0, sg = 0, sb = 0, sa = 0; //scanline average
    for(size_t x = 0; x < u; x++)
    {
      size_t index = y * u2 * 4 + x * 4;
      sr += buffer[index + 0] / (double)u;
      sg += buffer[index + 1] / (double)u;
      sb += buffer[index + 2] / (double)u;
      sa += buffer[index + 3] / (double)u;
    }
    tr += sr / (double)v;
    tg += sg / (double)v;
    tb += sb / (double)v;
    ta += sa / (double)v;
  }

  unsigned char r = (unsigned char)(tr + 0.5);
  unsigned char g = (unsigned char)(tg + 0.5);
  unsigned char b = (unsigned char)(tb + 0.5);
  unsigned char a = (unsigned char)(ta + 0.5);
  
  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  {
    size_t index = y * u2 * 4 + x * 4;
    buffer[index + 0] = r;
    buffer[index + 1] = g;
    buffer[index + 2] = b;
    buffer[index + 3] = a;
  }
}

////////////////////////////////////////////////////////////////////////////////




void OperationSortColors::doit(TextureRGBA8& texture, Progress& progress)
{
  unsigned char* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();

  // The std::map is used to group colors so that there's less to sort.
  // The std::map also does the actual sorting
  std::map<unsigned, unsigned> m;
  for(size_t y = 0; y < v; y++)
  {
    if(progress.handle(y / 2, v)) return;
    for(size_t x = 0; x < u; x++)
    {
      size_t index = y * u2 * 4 + x * 4;
      unsigned rgba = lpi::RGBAtoINT(buffer[index], buffer[index + 1], buffer[index + 2], buffer[index + 3]);
      m[rgba]++;
    }
  }

  // Iterating through the map is in sorted order (sorted by the "unsigned" color integer value)
  size_t k = 0;
  for(std::map<unsigned, unsigned>::const_iterator it = m.begin(); it != m.end(); it++)
  {
    unsigned color = it->first;
    unsigned amount = it->second;
    for(size_t j = 0; j < amount; j++)
    {
      size_t x = k % u;
      size_t y = k / u;
      size_t index = y * u2 * 4 + x * 4;

      lpi::INTtoRGBA(&buffer[index], color);

      k++;
      if(k % 1024 == 0 && progress.handle(u * v / 2 + k / 2, u * v)) return;
    }
  }
}

////////////////////////////////////////////////////////////////////////////////

#if 0 //Disabled, RGBA hilbert makes it not smooth for pure RGB image (as an anology, an image with all blue at zero, and doing RGB hilbert on it, also results in non smooth transitions)

//for 4-bit number, with 4-bit mask
unsigned char toRotatedGrayCode(unsigned char c, unsigned start, unsigned end)
{
  c = (c >> 1) ^ c; //binary to gray code
  c = (c * 2 * (start ^ end));
  c = ((c | (c / 16)) & 15) ^ start;
  return c;
}

unsigned char fromRotatedGrayCode(unsigned char c, unsigned start, unsigned end)
{
  c = (c ^ start) * (16 / (2 * (start ^ end)));
  c = (c | (c / 16)) & 15;
  //4-bit gray code to binary
  c ^= c >> 1;
  c ^= c >> 2;
  return c;
}

void updateRotation(unsigned& start, unsigned& end, unsigned c)
{
  unsigned s = c == 0 ? 0 : ((c - 1) / 2) * 2; //next smaller even number (min. 0)
  unsigned e = c >= 15 ? 15 : ((c + 1) / 2) * 2 + 1; //next larger odd number (max. mask)
  s = toRotatedGrayCode(s, start, end);
  e = toRotatedGrayCode(e, start, end);
  start = s;
  end = e;
}

unsigned rgbaToHilbert(unsigned char* rgba)
{
  unsigned char r = rgba[0];
  unsigned char g = rgba[1];
  unsigned char b = rgba[2];
  unsigned char a = rgba[3];

  unsigned result = 0;
  
  //interleave the bits
  for(int i = 0; i < 8; i++)
  {
    result |= ((r >> i) & 1) << (i * 4 + 0);
    result |= ((g >> i) & 1) << (i * 4 + 1);
    result |= ((b >> i) & 1) << (i * 4 + 2);
    result |= ((a >> i) & 1) << (i * 4 + 3);
  }

  //represent the rotation of the gray codes
  unsigned start = 0;
  unsigned end = 8;

  //calculate the inverse rotated gray code of every group of 4 bits
  for(size_t i = 0; i < 8; i++)
  {
    unsigned char c = (result >> ((7 - i) * 4)) & 15; //extract 4 bits
    c = fromRotatedGrayCode(c, start, end);
    updateRotation(start, end, c);
    //insert the new 4 bits at the correct location in result
    result &= ~(15 << ((7 - i) * 4));
    result |= (c << ((7 - i) * 4));
  }

  return result;
}

void hilbertToRGBA(unsigned char* rgba, unsigned hilbert)
{
  unsigned char r = 0, g = 0, b = 0, a = 0;

  //represent the rotation of the gray codes
  unsigned start = 0;
  unsigned end = 8;

  //calculate the rotated gray code of every group of 4 bits
  for(size_t i = 0; i < 8; i++)
  {
    unsigned char c = (hilbert >> ((7 - i) * 4)) & 15; //extract 4 bits
    unsigned char c2 = toRotatedGrayCode(c, start, end);
    updateRotation(start, end, c);
    //insert the new 4 bits at the correct location in result
    hilbert &= ~(15 << ((7 - i) * 4));
    hilbert |= (c2 << ((7 - i) * 4));
  }
  
  //uninterleave the bits
  for(int i = 0; i < 8; i++)
  {
    r |= ((hilbert >> (i * 4 + 0)) & 1) << i;
    g |= ((hilbert >> (i * 4 + 1)) & 1) << i;
    b |= ((hilbert >> (i * 4 + 2)) & 1) << i;
    a |= ((hilbert >> (i * 4 + 3)) & 1) << i;
  }

  rgba[0] = r;
  rgba[1] = g;
  rgba[2] = b;
  rgba[3] = a;
}

#else //RGB hilbert with extra hack for alpha channel to ensure smooth RGB transitions

//for 3-bit number, with 3-bit mask
unsigned char toRotatedGrayCode(unsigned char c, unsigned start, unsigned end)
{
  c = (c >> 1) ^ c; //binary to gray code
  c = (c * 2 * (start ^ end));
  c = ((c | (c / 8)) & 7) ^ start;
  return c;
}

unsigned char fromRotatedGrayCode(unsigned char c, unsigned start, unsigned end)
{
  c = (c ^ start) * (8 / (2 * (start ^ end)));
  c = (c | (c / 8)) & 7;
  //3-bit gray code to binary
  c ^= c >> 1;
  c ^= c >> 2;
  return c;
}

void updateRotation(unsigned& start, unsigned& end, unsigned c)
{
  unsigned s = c == 0 ? 0 : ((c - 1) / 2) * 2; //next smaller even number (min. 0)
  unsigned e = c >= 7 ? 7 : ((c + 1) / 2) * 2 + 1; //next larger odd number (max. mask)
  s = toRotatedGrayCode(s, start, end);
  e = toRotatedGrayCode(e, start, end);
  start = s;
  end = e;
}

unsigned rgbaToHilbert(unsigned char* rgba)
{
  unsigned char r = rgba[0];
  unsigned char g = rgba[1];
  unsigned char b = rgba[2];

  unsigned result = 0;
  
  //interleave the bits
  for(int i = 0; i < 8; i++)
  {
    result |= ((r >> i) & 1) << (i * 3 + 0);
    result |= ((g >> i) & 1) << (i * 3 + 1);
    result |= ((b >> i) & 1) << (i * 3 + 2);
  }

  //represent the rotation of the gray codes
  unsigned start = 0;
  unsigned end = 1;

  //calculate the inverse rotated gray code of every group of 3 bits
  for(size_t i = 0; i < 8; i++)
  {
    unsigned char c = (result >> ((7 - i) * 3)) & 7; //extract 3 bits
    c = fromRotatedGrayCode(c, start, end);
    updateRotation(start, end, c);
    //insert the new 3 bits at the correct location in result
    result &= ~(7 << ((7 - i) * 3));
    result |= (c << ((7 - i) * 3));
  }

  if(rgba[3] % 2 == 1) result = 0xFFFFFF - (result & 0xFFFFFF); //for alpha "snaking" zigzag pattern
  result |= (rgba[3] << 24); //alpha is separate

  return result;
}

void hilbertToRGBA(unsigned char* rgba, unsigned hilbert)
{
  rgba[3] = (hilbert >> 24) & 255; //alpha is separate
  if(rgba[3] % 2 == 1) hilbert = 0xFFFFFF - (hilbert & 0xFFFFFF); //for alpha "snaking" zigzag pattern

  unsigned char r = 0, g = 0, b = 0;

  //represent the rotation of the gray codes
  unsigned start = 0;
  unsigned end = 1;

  //calculate the rotated gray code of every group of 4 bits
  for(size_t i = 0; i < 8; i++)
  {
    unsigned char c = (hilbert >> ((7 - i) * 3)) & 7; //extract 3 bits
    unsigned char c2 = toRotatedGrayCode(c, start, end);
    updateRotation(start, end, c);
    //insert the new 3 bits at the correct location in result
    hilbert &= ~(7 << ((7 - i) * 3));
    hilbert |= (c2 << ((7 - i) * 3));
  }
  
  //uninterleave the bits
  for(int i = 0; i < 8; i++)
  {
    r |= ((hilbert >> (i * 3 + 0)) & 1) << i;
    g |= ((hilbert >> (i * 3 + 1)) & 1) << i;
    b |= ((hilbert >> (i * 3 + 2)) & 1) << i;
  }

  rgba[0] = r;
  rgba[1] = g;
  rgba[2] = b;
}

#endif

void OperationSortColorsHilbert::doit(TextureRGBA8& texture, Progress& progress)
{
  unsigned char* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();

  // The std::map is used to group colors so that there's less to sort.
  // The std::map also does the actual sorting
  std::map<unsigned, unsigned> m;
  for(size_t y = 0; y < v; y++)
  {
    if(progress.handle(y / 2, v)) return;
    for(size_t x = 0; x < u; x++)
    {
      size_t index = y * u2 * 4 + x * 4;
      unsigned hilbert = rgbaToHilbert(&buffer[index]);
      m[hilbert]++;
    }
  }

  // Iterating through the map is in sorted order (sorted by the "unsigned" color integer value)
  size_t k = 0;
  for(std::map<unsigned, unsigned>::const_iterator it = m.begin(); it != m.end(); it++)
  {
    unsigned hilbert = it->first;
    unsigned amount = it->second;
    for(size_t j = 0; j < amount; j++)
    {
      size_t x = k % u;
      size_t y = k / u;
      size_t index = y * u2 * 4 + x * 4;

      hilbertToRGBA(&buffer[index], hilbert);
      k++;
      if(k % 1024 == 0 && progress.handle(u * v / 2 + k / 2, u * v)) return;
    }
  }
}

////////////////////////////////////////////////////////////////////////////////

struct OrderColorStruct
{
  unsigned color;
  size_t amount;
  size_t time; //the time when this color was first encountered (to undo the sorting effect of the std::map)
};

bool OrderColorFun(const OrderColorStruct& a, const OrderColorStruct& b)
{
  if(a.amount == b.amount) return a.time < b.time;
  return a.amount > b.amount;
}

void OperationOrderColors::doit(TextureRGBA8& texture, Progress& progress)
{
  unsigned char* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();

  //The std::map is to count amount of each color
  std::map<unsigned, OrderColorStruct> m;
  size_t time = 0;
  for(size_t y = 0; y < v; y++)
  {
    if(progress.handle(y / 2, v)) return;
    for(size_t x = 0; x < u; x++)
    {
      size_t index = y * u2 * 4 + x * 4;
      unsigned rgba = buffer[index] * 256 * 256 * 256 + buffer[index + 1] * 256 * 256 + buffer[index + 2] * 256 + buffer[index + 3];
      if(m.count(rgba) == 0)
      {
        OrderColorStruct o;
        o.color = rgba;
        o.amount = 1;
        o.time = time++;
        m[rgba] = o;
      }
      else
      {
        m[rgba].amount++;
      }
    }
  }

  std::vector<OrderColorStruct> colors;
  for(std::map<unsigned, OrderColorStruct>::const_iterator it = m.begin(); it != m.end(); it++)
  {
    colors.push_back(it->second);
  }
  std::stable_sort(colors.begin(), colors.end(), OrderColorFun);

  size_t k = 0;
  for(size_t i = 0; i < colors.size(); i++)
  {
    unsigned color = colors[i].color;
    unsigned amount = colors[i].amount;
    for(size_t j = 0; j < amount; j++)
    {
      size_t x = k % u;
      size_t y = k / u;
      size_t index = y * u2 * 4 + x * 4;
      
      buffer[index + 0] = (color >> 24) % 256;
      buffer[index + 1] = (color >> 16) % 256;
      buffer[index + 2] = (color >> 8) % 256;
      buffer[index + 3] = color % 256;

      k++;
      if(k % 1024 == 0 && progress.handle(u * v / 2 + k / 2, u * v)) return;
    }
  }
}

////////////////////////////////////////////////////////////////////////////////

void OperationNegateRGB::doit(TextureRGBA8& texture, Progress&)
{
  unsigned char* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();
  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  for(size_t c = 0; c < 4; c++)
  {
    size_t index = y * u2 * 4 + x * 4 + c;
    if(c != 3) buffer[index] = 255 - buffer[index];
  }
}

void OperationNegateRGB::doit(TextureRGBA32F& texture, Progress&)
{
  float* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();
  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  for(size_t c = 0; c < 4; c++)
  {
    size_t index = y * u2 * 4 + x * 4 + c;
    if(c != 3) buffer[index] = -buffer[index]; //and NOT 1 - buffer[index], on purpose! to keep the operation perfectly invertible without information loss. Use normalize after this to get result similar to 8-bit invert.
  }
}

void OperationNegateRGB::doit(TextureGrey8& texture, Progress&)
{
  unsigned char* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();
  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  {
    size_t index = y * u2 + x;
    buffer[index] = 255 - buffer[index];
  }
}

void OperationNegateRGB::doit(TextureGrey32F& texture, Progress&)
{
  float* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();
  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  {
    size_t index = y * u2 + x;
    buffer[index] = -buffer[index]; //and NOT 1 - buffer[index], on purpose! to keep the operation perfectly invertible without information loss. Use normalize after this to get result similar to 8-bit invert.
  }
}

////////////////////////////////////////////////////////////////////////////////

void OperationNegateLightness::doit(TextureRGBA8& texture, Progress&)
{
  unsigned char* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();
  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  {
    size_t i = y * u2 * 4 + x * 4;
    lpi::ColorRGB rgb(buffer[i], buffer[i+1], buffer[i+2], buffer[i+3]);
    lpi::ColorHSL hsl = lpi::RGBtoHSL(rgb);
    hsl.l = 255 - hsl.l;
    rgb = lpi::HSLtoRGB(hsl);
    buffer[i+0] = rgb.r;
    buffer[i+1] = rgb.g;
    buffer[i+2] = rgb.b;
  }
}

////////////////////////////////////////////////////////////////////////////////

OperationNegateAdvanced::OperationNegateAdvanced(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom)
: AOperationFilterUtil(settings, true, true)
, r(false)
, g(false)
, b(false)
, a(false)
, lightness(false)
, value(false)
, saturation(false)
, labl(true)
, laba(false)
, labb(false)
, dialog(this)
{
  params.addBool("Negate R", &r);
  params.addBool("Negate G", &g);
  params.addBool("Negate B", &b);
  params.addBool("Negate A", &a);
  params.addBool("Negate Lightness", &lightness);
  params.addBool("Negate Value", &value);
  params.addBool("Negate Saturation", &saturation);
  params.addBool("Negate Lab L", &labl);
  params.addBool("Negate Lab a", &laba);
  params.addBool("Negate Lab b", &labb);
  
  paramsToDialog(dialog.page, params, geom);
}

void OperationNegateAdvanced::doit(TextureRGBA8& texture, Progress&)
{
  unsigned char* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();
  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  {
    size_t i = y * u2 * 4 + x * 4;
    
    if(saturation)
    {
      lpi::ColorRGB rgb(buffer[i], buffer[i+1], buffer[i+2], buffer[i+3]);
      lpi::ColorHSL hsl = lpi::RGBtoHSL(rgb);
      hsl.s = 255 - hsl.s;
      rgb = lpi::HSLtoRGB(hsl);
      buffer[i+0] = rgb.r;
      buffer[i+1] = rgb.g;
      buffer[i+2] = rgb.b;
    }
    if(lightness)
    {
      size_t i = y * u2 * 4 + x * 4;
      lpi::ColorRGB rgb(buffer[i], buffer[i+1], buffer[i+2], buffer[i+3]);
      lpi::ColorHSL hsl = lpi::RGBtoHSL(rgb);
      hsl.l = 255 - hsl.l;
      rgb = lpi::HSLtoRGB(hsl);
      buffer[i+0] = rgb.r;
      buffer[i+1] = rgb.g;
      buffer[i+2] = rgb.b;
    }
    if(value)
    {
      size_t i = y * u2 * 4 + x * 4;
      lpi::ColorRGB rgb(buffer[i], buffer[i+1], buffer[i+2], buffer[i+3]);
      lpi::ColorHSV hsv = lpi::RGBtoHSV(rgb);
      hsv.v = 255 - hsv.v;
      rgb = lpi::HSVtoRGB(hsv);
      buffer[i+0] = rgb.r;
      buffer[i+1] = rgb.g;
      buffer[i+2] = rgb.b;
    }
    if(labl || laba || labb)
    {
      size_t i = y * u2 * 4 + x * 4;
      lpi::ColorRGB rgb(buffer[i], buffer[i+1], buffer[i+2], buffer[i+3]);
      lpi::ColorLab lab = lpi::RGBtoLab(rgb);
      if(labl) lab.l = 255 - lab.l;
      if(laba) lab.a = 255 - lab.a;
      if(labb) lab.b = 255 - lab.b;
      rgb = lpi::LabtoRGB(lab);
      rgb.clamp();
      buffer[i+0] = rgb.r;
      buffer[i+1] = rgb.g;
      buffer[i+2] = rgb.b;
    }
    if(r) buffer[i + 0] = 255 - buffer[i + 0];
    if(g) buffer[i + 1] = 255 - buffer[i + 1];
    if(b) buffer[i + 2] = 255 - buffer[i + 2];
    if(a) buffer[i + 3] = 255 - buffer[i + 3];
  }
}

////////////////////////////////////////////////////////////////////////////////

void OperationNegateRGBA::doit(TextureRGBA8& texture, Progress&)
{
  unsigned char* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();
  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  for(size_t c = 0; c < 4; c++)
  {
    size_t index = y * u2 * 4 + x * 4 + c;
    buffer[index] = 255 - buffer[index];
  }
}

void OperationNegateRGBA::doit(TextureRGBA32F& texture, Progress&)
{
  float* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();
  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  for(size_t c = 0; c < 4; c++)
  {
    size_t index = y * u2 * 4 + x * 4 + c;
    buffer[index] = -buffer[index];
  }
}


////////////////////////////////////////////////////////////////////////////////

void OperationNegateAlpha::doit(TextureRGBA8& texture, Progress&)
{
  unsigned char* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();
  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  {
    size_t index = y * u2 * 4 + x * 4 + 3;
    buffer[index] = 255 - buffer[index];
  }
}

void OperationNegateAlpha::doit(TextureRGBA32F& texture, Progress&)
{
  float* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();
  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  for(size_t c = 0; c < 4; c++)
  {
    size_t index = y * u2 * 4 + x * 4 + 3;
    buffer[index] = -buffer[index];
  }
}

////////////////////////////////////////////////////////////////////////////////

void OperationSolarizeRGB::doit(TextureRGBA8& texture, Progress&)
{
  unsigned char* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();
  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  for(size_t c = 0; c < 4; c++)
  {
    size_t index = y * u2 * 4 + x * 4 + c;
    if(c != 3)
    {
      unsigned char val = buffer[index];
      if(val < 128) buffer[index] = val * 2;
      else buffer[index] = 255 - (val - 128) * 2;
    }
  }
}

void OperationSolarizeRGB::doit(TextureRGBA32F& texture, Progress&)
{
  float* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();
  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  for(size_t c = 0; c < 4; c++)
  {
    size_t index = y * u2 * 4 + x * 4 + c;
    if(c != 3)
    {
      float val = buffer[index];
      if(val < 0.5) buffer[index] = val * 2;
      else buffer[index] = 1.0 - (val - 0.5) * 2;
    }
  }
}

////////////////////////////////////////////////////////////////////////////////

OperationExtractChannel::OperationExtractChannel(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom)
: AOperationFilterUtil(settings, true, true)
, dialog(this)
, affect_alpha(true)
, channel(C_CHROMA)
{
  params.addBool("Affect Alpha", &affect_alpha);
  
  std::vector<Channel> enums;
  enums.push_back(C_CHROMA);
  enums.push_back(C_HUE);
  enums.push_back(C_LIGHTNESS);
  enums.push_back(C_VALUE);
  enums.push_back(C_INTENSITY);
  enums.push_back(C_LUMA);
  enums.push_back(C_SATURATION_L);
  enums.push_back(C_SATURATION_V);
  enums.push_back(C_SATURATION_I);
  enums.push_back(C_ALPHA);
  enums.push_back(C_RGB_R);
  enums.push_back(C_RGB_G);
  enums.push_back(C_RGB_B);
  enums.push_back(C_CMYK_C);
  enums.push_back(C_CMYK_M);
  enums.push_back(C_CMYK_Y);
  enums.push_back(C_CMYK_K);
  enums.push_back(C_Lab_L);
  enums.push_back(C_Lab_a);
  enums.push_back(C_Lab_b);
  enums.push_back(C_XYZ_X);
  enums.push_back(C_XYZ_Y);
  enums.push_back(C_XYZ_Z);
  enums.push_back(C_YPbPr_Y);
  enums.push_back(C_YPbPr_Pb);
  enums.push_back(C_YPbPr_Pr);
  enums.push_back(C_YCbCr_Y);
  enums.push_back(C_YCbCr_Cb);
  enums.push_back(C_YCbCr_Cr);
  enums.push_back(C_Min);
  enums.push_back(C_Middle);
  enums.push_back(C_Max);
  std::vector<std::string> names;
  names.push_back("Chroma");
  names.push_back("Hue");
  names.push_back("Lightness");
  names.push_back("Value");
  names.push_back("Intensity");
  names.push_back("Luma");
  names.push_back("Saturation L");
  names.push_back("Saturation V");
  names.push_back("Saturation I");
  names.push_back("Alpha");
  names.push_back("RGB R");
  names.push_back("RGB G");
  names.push_back("RGB B");
  names.push_back("CMYK C");
  names.push_back("CMYK M");
  names.push_back("CMYK Y");
  names.push_back("CMYK K");
  names.push_back("Lab L");
  names.push_back("Lab a");
  names.push_back("Lab b");
  names.push_back("XYZ X");
  names.push_back("XYZ Y");
  names.push_back("XYZ Z");
  names.push_back("YPbPr Y");
  names.push_back("YPbPr Pb");
  names.push_back("YPbPr Pr");
  names.push_back("YCbCr Y");
  names.push_back("YCbCr Cb");
  names.push_back("YCbCr Cr");
  names.push_back("Min");
  names.push_back("Middle");
  names.push_back("Max");
  
  params.addEnum("Channel", &channel, names, enums);
  
  paramsToDialog(dialog.page, params, geom);
}

static unsigned char maxc(unsigned char* p)
{
  return std::max(std::max(p[0], p[1]), p[2]);
}

static unsigned char minc(unsigned char* p)
{
  return std::min(std::min(p[0], p[1]), p[2]);
}

static unsigned char chroma(unsigned char* p)
{
  return maxc(p) - minc(p);
}

void OperationExtractChannel::doit(TextureRGBA8& texture, Progress&)
{
  unsigned char* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();
  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  {
    size_t index = y * u2 * 4 + x * 4;
    unsigned char* p = &buffer[index + 0];
    int r = p[0], g = p[1], b = p[2];
    unsigned char c;
    
    if(channel == C_CHROMA) c = chroma(p);
    else if(channel == C_HUE) c = lpi::RGBtoHue(lpi::ColorRGB(r, g, b));
    else if(channel == C_LIGHTNESS) c = (maxc(p) + minc(p)) / 2;
    else if(channel == C_SATURATION_L)
    {
      int ch = chroma(p);
      if(ch == 0) c = 0;
      else c = (ch * 255) / (255 - std::abs(maxc(p) + minc(p) - 255));
    }
    else if(channel == C_VALUE) c = maxc(p);
    else if(channel == C_SATURATION_V)
    {
      int ch = chroma(p);
      if(ch == 0) c = 0;
      else c = ch * 255 / maxc(p);
    }
    else if(channel == C_INTENSITY) c = (r + g + b) / 3;
    else if(channel == C_SATURATION_I)
    {
      int ch = chroma(p);
      if(ch == 0) c = 0;
      else c = 255 - (3 * 255 * minc(p) / (r + g + b));
    }
    else if(channel == C_LUMA) c = lpi::RGBtoLuma(lpi::ColorRGB(r, g, b));
    else if(channel == C_ALPHA) c = p[3];
    else if(channel == C_RGB_R) c = r;
    else if(channel == C_RGB_G) c = g;
    else if(channel == C_RGB_B) c = b;
    else if(channel == C_CMYK_C) c = lpi::RGBtoCMYK(lpi::ColorRGB(r, g, b)).c;
    else if(channel == C_CMYK_M) c = lpi::RGBtoCMYK(lpi::ColorRGB(r, g, b)).m;
    else if(channel == C_CMYK_Y) c = lpi::RGBtoCMYK(lpi::ColorRGB(r, g, b)).y;
    else if(channel == C_CMYK_K) c = lpi::RGBtoCMYK(lpi::ColorRGB(r, g, b)).k;
    else if(channel == C_Lab_L) c = lpi::RGBtoLab(lpi::ColorRGB(r, g, b)).l;
    else if(channel == C_Lab_a) c = lpi::clamp((lpi::RGBtoLab(lpi::ColorRGB(r, g, b)).a + 255) / 2, 0, 255);
    else if(channel == C_Lab_b) c = lpi::clamp((lpi::RGBtoLab(lpi::ColorRGB(r, g, b)).b + 255) / 2, 0, 255);
    else if(channel == C_XYZ_X) c = lpi::RGBtoXYZ(lpi::ColorRGB(r, g, b)).x;
    else if(channel == C_XYZ_Y) c = lpi::RGBtoXYZ(lpi::ColorRGB(r, g, b)).y;
    else if(channel == C_XYZ_Z) c = lpi::RGBtoXYZ(lpi::ColorRGB(r, g, b)).z;
    else if(channel == C_YPbPr_Y) c = lpi::RGBtoYPbPr(lpi::ColorRGB(r, g, b)).y;
    else if(channel == C_YPbPr_Pb) c = (lpi::RGBtoYPbPr(lpi::ColorRGB(r, g, b)).pb + 128);
    else if(channel == C_YPbPr_Pr) c = (lpi::RGBtoYPbPr(lpi::ColorRGB(r, g, b)).pr + 128);
    else if(channel == C_YCbCr_Y) c = lpi::RGBtoYCbCr(lpi::ColorRGB(r, g, b)).y;
    else if(channel == C_YCbCr_Cb) c = lpi::RGBtoYCbCr(lpi::ColorRGB(r, g, b)).cb;
    else if(channel == C_YCbCr_Cr) c = lpi::RGBtoYCbCr(lpi::ColorRGB(r, g, b)).cr;
    else if(channel == C_Min) c = minc(p);
    else if(channel == C_Middle)
    {
      int ma = maxc(p);
      int mi = minc(p);
      if((g == ma && b == mi) || (g == mi && b == ma)) c = r;
      else if((r == ma && b == mi) || (r == mi && b == ma)) c = g;
      else c = b;
    }
    else if(channel == C_Max) c = maxc(p);

    
    buffer[index + 0] = buffer[index + 1] = buffer[index + 2] = c;
    if(affect_alpha) buffer[index + 3] = 255;
  }
}

////////////////////////////////////////////////////////////////////////////////

OperationAbsoluteValue::OperationAbsoluteValue(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom)
: AOperationFilterUtil(settings, true, true)
, affect_alpha(true)
, dialog(this)
{
  params.addLabel("For HDR images with neg. pixels");
  params.addBool("Affect Alpha", &affect_alpha);
  
  paramsToDialog(dialog.page, params, geom);
}

void OperationAbsoluteValue::doit(TextureRGBA8& texture, Progress&)
{
  unsigned char* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();
  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  for(size_t c = 0; c < 4; c++)
  {
    if(!affect_alpha && c == 3) continue;
    size_t index = y * u2 * 4 + x * 4 + c;
    if(buffer[index] < 128) buffer[index] = 255 - buffer[index];
  }
}

void OperationAbsoluteValue::doit(TextureRGBA32F& texture, Progress&)
{
  float* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();
  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  for(size_t c = 0; c < 4; c++)
  {
    if(!affect_alpha && c == 3) continue;
    size_t index = y * u2 * 4 + x * 4 + c;
    if(buffer[index] < 0) buffer[index] = -buffer[index];
  }
}

////////////////////////////////////////////////////////////////////////////////

void OperationFlipX::doit(TextureRGBA8& texture, Progress&)
{
  unsigned char* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();
  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u / 2; x++)
  for(size_t c = 0; c < 4; c++)
  {
    size_t index = y * u2 * 4 + x * 4 + c;
    size_t index2 = y * u2 * 4 + (u - x - 1) * 4 + c;
    std::swap(buffer[index], buffer[index2]);
  }
}

////////////////////////////////////////////////////////////////////////////////

void OperationFlipY::doit(TextureRGBA8& texture, Progress&)
{
  unsigned char* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();
  for(size_t y = 0; y < v / 2; y++)
  for(size_t x = 0; x < u; x++)
  for(size_t c = 0; c < 4; c++)
  {
    size_t index = y * u2 * 4 + x * 4 + c;
    size_t index2 = (v - y - 1) * u2 * 4 + x * 4 + c;
    std::swap(buffer[index], buffer[index2]);
  }
}

////////////////////////////////////////////////////////////////////////////////

OperationRotate::OperationRotate(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom)
: AOperationFilterBase(settings)
, angle(45.0)
, ccw(false)
, smooth(true)
, lock_scale(false)
{
  params.addDouble("Angle", &angle, 0.0, 360.0, 0.5);
  params.addBool("Counterclockwise", &ccw);
  params.addBool("Smooth", &smooth);
  params.addBool("Repeating", &wrapping);
  params.addBool("Lock Scale", &lock_scale);
  
  paramsToDialog(dialog, params, geom);
}

void OperationRotate180::doit(TextureRGBA8& texture, Progress&)
{
  unsigned char* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();
  for(size_t y = 0; y < v / 2; y++)
  for(size_t x = 0; x < u; x++)
  for(size_t c = 0; c < 4; c++)
  {
    size_t index = y * u2 * 4 + x * 4 + c;
    size_t index2 = (v - y - 1) * u2 * 4 + (u - x - 1) * 4 + c;
    std::swap(buffer[index], buffer[index2]);
  }
  if(v % 2 == 1)
  for(size_t x = 0; x < u / 2; x++)
  for(size_t c = 0; c < 4; c++)
  {
    size_t y = v / 2;
    size_t index = y * u2 * 4 + x * 4 + c;
    size_t index2 = (v - y - 1) * u2 * 4 + (u - x - 1) * 4 + c;
    std::swap(buffer[index], buffer[index2]);
  }
}

////////////////////////////////////////////////////////////////////////////////

namespace
{
//TODO: put this in imalg and make version for all color modes
void rotate90CW(Paint& paint)
{
  unsigned char* buffer = paint.canvasRGBA8.getBuffer();
  size_t u = paint.canvasRGBA8.getU();
  size_t v = paint.canvasRGBA8.getV();
  size_t u2 = paint.canvasRGBA8.getU2();
  size_t v2 = paint.canvasRGBA8.getV2();
  
  std::vector<unsigned char> newbuffer(u2 * v2 * 4); //slow implementation with temporary buffer. TODO: work directly in one buffer instead.
  
  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  for(size_t c = 0; c < 4; c++)
  {
    size_t index = y * u2 * 4 + x * 4 + c;
    newbuffer[index] = buffer[index];
  }
  
  paint.canvasRGBA8.setSize(v, u);

  size_t u22 = paint.canvasRGBA8.getU2();
  
  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  for(size_t c = 0; c < 4; c++)
  {
    size_t index = (v - y - 1) * u2 * 4 + x * 4 + c;
    size_t index2 = x * u22 * 4 + y * 4 + c;
    buffer[index2] = newbuffer[index];
  }
}

void rotate90CCW(Paint& paint)
{
  unsigned char* buffer = paint.canvasRGBA8.getBuffer();
  size_t u = paint.canvasRGBA8.getU();
  size_t v = paint.canvasRGBA8.getV();
  size_t u2 = paint.canvasRGBA8.getU2();
  size_t v2 = paint.canvasRGBA8.getV2();
  
  std::vector<unsigned char> newbuffer(u2 * v2 * 4); //slow implementation with temporary buffer. TODO: work directly in one buffer instead.
  
  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  for(size_t c = 0; c < 4; c++)
  {
    size_t index = y * u2 * 4 + x * 4 + c;
    newbuffer[index] = buffer[index];
  }
  
  paint.canvasRGBA8.setSize(v, u);

  size_t u22 = paint.canvasRGBA8.getU2();
  
  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  for(size_t c = 0; c < 4; c++)
  {
    size_t index = y * u2 * 4 + (u - x - 1) * 4 + c;
    size_t index2 = x * u22 * 4 + y * 4 + c;
    buffer[index2] = newbuffer[index];
  }
}

}

////////////////////////////////////////////////////////////////////////////////


bool OperationRotate90CW::operate(Paint& paint, UndoState& state, Progress& progress)
{
  //not thread save
  (void)paint;
  (void)state;
  (void)progress;

  return true;
}

bool OperationRotate90CW::operateGL(Paint& paint, UndoState& state)
{
  (void)state;
  rotate90CW(paint);
  paint.update();

  return true;
}

void OperationRotate90CW::undo(Paint& paint, UndoState& state)
{
  (void)state;
  rotate90CCW(paint);
  paint.update();
}

void OperationRotate90CW::redo(Paint& paint, UndoState& state)
{
  (void)state;
  rotate90CW(paint);
  paint.update();
}

////////////////////////////////////////////////////////////////////////////////

bool OperationRotate90CCW::operate(Paint& paint, UndoState& state, Progress& progress)
{
  //not thread save
  (void)paint;
  (void)state;
  (void)progress;

  return true;
}

bool OperationRotate90CCW::operateGL(Paint& paint, UndoState& state)
{
  (void)state;
  rotate90CCW(paint);
  paint.update();

  return true;
}

void OperationRotate90CCW::undo(Paint& paint, UndoState& state)
{
  (void)state;
  rotate90CW(paint);
  paint.update();
}

void OperationRotate90CCW::redo(Paint& paint, UndoState& state)
{
  (void)state;
  rotate90CCW(paint);
  paint.update();
}

////////////////////////////////////////////////////////////////////////////////

void OperationShift50::doit(TextureRGBA8& texture, Progress&)
{
  unsigned char* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();
  
  bool oddu = (u % 2 == 1);
  bool oddv = (v % 2 == 1);
  
  if(!oddu && !oddv)
  {
    for(size_t y = 0; y < v / 2; y++)
    for(size_t x = 0; x < u; x++)
    for(size_t c = 0; c < 4; c++)
    {
      size_t index = y * u2 * 4 + x * 4 + c;
      size_t x2 = x + u / 2; if(x2 >= u) x2 -= u;
      size_t y2 = y + v / 2; if(y2 >= v) y2 -= v;
      size_t index2 = y2 * u2 * 4 + x2 * 4 + c;
      std::swap(buffer[index2], buffer[index]);
    }
  }
  else
  {
    shiftImageSlow(texture, u / 2, v / 2);
  }
}

////////////////////////////////////////////////////////////////////////////////

OperationWrappingShift::OperationWrappingShift(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom)
: AOperationFilterUtil(settings, true, true)
, x(32)
, y(32)
, dialog(this)
{
  params.addInt("X", &x, -1024, +1024, 1);
  params.addInt("Y", &y, -1024, +1024, 1);
  
  paramsToDialog(dialog.page, params, geom);
}

void OperationWrappingShift::doit(TextureRGBA8& texture, Progress&)
{
  shiftImageSlow(texture, x, y);
}

////////////////////////////////////////////////////////////////////////////////

void OperationGrayscale::doit(TextureRGBA8& texture, Progress&)
{
  unsigned char* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();
  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  {
    size_t index = y * u2 * 4 + x * 4;
    int intensity = (buffer[index + 0] + buffer[index + 1] + buffer[index + 2]) / 3;
    buffer[index + 0] = buffer[index + 1] = buffer[index + 2] = intensity;
  }
}

////////////////////////////////////////////////////////////////////////////////

void OperationGrayscalePhysiological::doit(TextureRGBA8& texture, Progress&)
{
  unsigned char* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();
  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  {
    size_t index = y * u2 * 4 + x * 4;
    int luma = (int)(0.3 * buffer[index + 0] + 0.59 * buffer[index + 1] + 0.11 * buffer[index + 2]);
    buffer[index + 0] = buffer[index + 1] = buffer[index + 2] = luma;
  }
}

////////////////////////////////////////////////////////////////////////////////

void OperationGrayscaleAlpha::doit(TextureRGBA8& texture, Progress&)
{
  unsigned char* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();
  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  {
    size_t index = y * u2 * 4 + x * 4;
    int grey = (buffer[index + 0] + buffer[index + 1] + buffer[index + 2] + buffer[index + 3]) / 4;
    buffer[index + 0] = buffer[index + 1] = buffer[index + 2] = buffer[index + 3] = grey;
  }
}

////////////////////////////////////////////////////////////////////////////////


void OperationClear::doit(TextureRGBA8& texture, const GlobalToolSettings& settings, Progress&)
{
  lpi::ColorRGB color = settings.rightColor255();
  unsigned char* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();
  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  {
    size_t index = y * u2 * 4 + x * 4;
    buffer[index + 0] = color.r;
    buffer[index + 1] = color.g;
    buffer[index + 2] = color.b;
    buffer[index + 3] = color.a;
  }
}

////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////

OperationColorize::OperationColorize(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom)
: AOperationFilterUtilWithSettings(settings, true, false)
, affect_alpha(true)
, dialog(this)
{
  params.addBool("Affect Alpha", &affect_alpha);
  params.addColor("FG Color", &settings.leftColor);
  params.addColor("BG Color", &settings.rightColor);
  
  paramsToDialog(dialog.page, params, geom);
}

void OperationColorize::doit(TextureRGBA8& texture, const GlobalToolSettings& settings, Progress&)
{
  lpi::ColorRGB a = settings.leftColor255();
  lpi::ColorRGB b = settings.rightColor255();
  unsigned char* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();

  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  {
    size_t index = y * u2 * 4 + x * 4;
    int v = (buffer[index + 0] + buffer[index + 1] + buffer[index + 2]) / 3;
    buffer[index + 0] = (a.r * v + b.r * (255 - v)) / 255;
    buffer[index + 1] = (a.g * v + b.g * (255 - v)) / 255;
    buffer[index + 2] = (a.b * v + b.b * (255 - v)) / 255;
    if(affect_alpha) buffer[index + 3] = (a.a * v + b.a * (255 - v)) / 255;
  }
}

////////////////////////////////////////////////////////////////////////////////

void OperationNormalize::doit(TextureRGBA8& texture, Progress&)
{
  unsigned char* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();
  
  normalizeRGBA8(buffer, u, v, u2, true, false, true);
}

void OperationNormalize::doit(TextureRGBA32F& texture, Progress&)
{
  float* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();

  normalizeRGBA32F(buffer, u, v, u2, true, false, true);
}

void OperationNormalize::doit(TextureGrey8& texture, Progress&)
{
  unsigned char* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();
  
  normalizeGrey8(buffer, u, v, u2);
}

void OperationNormalize::doit(TextureGrey32F& texture, Progress&)
{
  float* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();

  normalizeGrey32F(buffer, u, v, u2);
}

////////////////////////////////////////////////////////////////////////////////

OperationNormalize2::OperationNormalize2(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom)
: AOperationFilterUtil(settings, true, true)
, link_channels(true)
, affect_alpha(false)
, dialog(this)
{
  params.addBool("Link Channels", &link_channels);
  params.addBool("Affect Alpha", &affect_alpha);
  
  paramsToDialog(dialog.page, params, geom);
}

void OperationNormalize2::doit(TextureRGBA8& texture, Progress&)
{
  unsigned char* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();
  
  normalizeRGBA8(buffer, u, v, u2, false, link_channels, affect_alpha);
}

void OperationNormalize2::doit(TextureRGBA32F& texture, Progress&)
{
  float* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();

  normalizeRGBA32F(buffer, u, v, u2, false, link_channels, affect_alpha);
}

////////////////////////////////////////////////////////////////////////////////

template<typename T>
void clampT(T& c, double low, double high)
{
  if((double)c < low) c = (T)low;
  if((double)c > high) c = (T)high;
}

OperationClipColor::OperationClipColor(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom)
: AOperationFilterUtil(settings, true, true)
, affect_rgb(true)
, affect_alpha(false)
, low(0.0)
, high(128.0)
, dialog(this)
{
  params.addDouble("Low", &low, 0.0, 255.0, 1.0);
  params.addDouble("High", &high, 0.0, 255.0, 1.0);
  params.addBool("Affect RGB", &affect_rgb);
  params.addBool("Affect Alpha", &affect_alpha);

  paramsToDialog(dialog.page, params, geom);
}

void OperationClipColor::doit(TextureRGBA8& texture, Progress&)
{
  unsigned char* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();

  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  {
    size_t index = y * u2 * 4 + x * 4;
    if(affect_rgb)
    {
      clampT(buffer[index + 0], low, high);
      clampT(buffer[index + 1], low, high);
      clampT(buffer[index + 2], low, high);

    }
    if(affect_alpha) clampT(buffer[index + 3], low, high);
  }
}

void OperationClipColor::doit(TextureRGBA32F& texture, Progress&)
{
  float* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();

  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  {
    size_t index = y * u2 * 4 + x * 4;
    if(affect_rgb)
    {
      clampT(buffer[index + 0], low, high);
      clampT(buffer[index + 1], low, high);
      clampT(buffer[index + 2], low, high);
    }
    if(affect_alpha) clampT(buffer[index + 3], low, high);
  }
}

////////////////////////////////////////////////////////////////////////////////

OperationThreshold::OperationThreshold(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom)
: AOperationFilterUtil(settings, true, true)
, threshold(128.0)
, dialog(this)
{
  params.addDouble("Threshold", &threshold, 0.0, 255.0, 1.0);

  paramsToDialog(dialog.page, params, geom);
}
    
void OperationThreshold::doit(TextureRGBA8& texture, Progress&)
{
  unsigned char* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();

  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  {
    size_t index = y * u2 * 4 + x * 4;
    int value = (buffer[index + 0] + buffer[index + 1] + buffer[index + 2]) / 3;
    if(value < (int)threshold) buffer[index + 0] = buffer[index + 1] = buffer[index + 2] = 0;
    else buffer[index + 0] = buffer[index + 1] = buffer[index + 2] = 255;
  }
}

void OperationThreshold::doit(TextureRGBA32F& texture, Progress&)
{
  float* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();

  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  {
    size_t index = y * u2 * 4 + x * 4;
    float value = (buffer[index + 0] + buffer[index + 1] + buffer[index + 2]) / 3.0f;
    if(value < (float)threshold) buffer[index + 0] = buffer[index + 1] = buffer[index + 2] = 0.0f;
    else buffer[index + 0] = buffer[index + 1] = buffer[index + 2] = 1.0f;
  }
}

////////////////////////////////////////////////////////////////////////////////

namespace
{
  unsigned char clamp0255(int value)
  {
    return value > 255 ? 255 : (value < 0 ? 0 : value);
  }
  
  void addAndClamp(unsigned char* color, int add)
  {
    color[0] = clamp0255(color[0] + add);
    color[1] = clamp0255(color[1] + add);
    color[2] = clamp0255(color[2] + add);
  }
}

void OperationDitherMonochrome::doit(TextureRGBA8& texture, Progress&)
{
  unsigned char* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();

  //random
  //for(size_t y = 0; y < v; y++)
  //for(size_t x = 0; x < u; x++)
  //{
    //size_t index = y * u2 * 4 + x * 4;
    //double value = ((buffer[index + 0] + buffer[index + 1] + buffer[index + 2]) / 3) / 255.0;
    
    //double r = lpi::getRandom();
    
    
    //if(value <= r) buffer[index + 0] = buffer[index + 1] = buffer[index + 2] = 0;
    //else buffer[index + 0] = buffer[index + 1] = buffer[index + 2] = 255;
  //}

  //Floyd-Steinberg
  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  {
    size_t index = y * u2 * 4 + x * 4;
    size_t index10 = y * u2 * 4 + (x+1) * 4;
    size_t index11 = (y+1) * u2 * 4 + (x+1) * 4;
    size_t index01 = (y+1) * u2 * 4 + x * 4;
    size_t index91 = (y+1) * u2 * 4 + (x-1) * 4;
    
    int value = (buffer[index + 0] + buffer[index + 1] + buffer[index + 2]) / 3;
    
    if(value <= 128) buffer[index + 0] = buffer[index + 1] = buffer[index + 2] = 0;
    else buffer[index + 0] = buffer[index + 1] = buffer[index + 2] = 255;
    
    int error = value - (buffer[index + 0] + buffer[index + 1] + buffer[index + 2]) / 3;
    
    if(x + 1 < u) addAndClamp(&buffer[index10], (7 * error) / 16);
    if(y + 1 < v)
    {
      if(x >= 1) addAndClamp(&buffer[index91], (3 * error) / 16);
      addAndClamp(&buffer[index01], (5 * error) / 16);
      if(x + 1 < u) addAndClamp(&buffer[index11], (1 * error) / 16);
    }
    
    buffer[index + 3] = 255; //alpha channel makes no sense for monochrome
  }
}

void OperationDitherRGB1::doit(TextureRGBA8& texture, Progress&)
{
  unsigned char* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();

  //Floyd-Steinberg
  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  for(size_t c = 0; c < 4; c++)
  {
    size_t index = y * u2 * 4 + x * 4 + c;
    size_t index10 = y * u2 * 4 + (x+1) * 4 + c;
    size_t index11 = (y+1) * u2 * 4 + (x+1) * 4 + c;
    size_t index01 = (y+1) * u2 * 4 + x * 4 + c;
    size_t index91 = (y+1) * u2 * 4 + (x-1) * 4 + c;
    
    int value = buffer[index];
    
    if(value <= 128) buffer[index] = 0;
    else buffer[index] = 255;
    
    int error = value - buffer[index];
    if(x + 1 < u) buffer[index10] = clamp0255(buffer[index10] + (7 * error) / 16);
    if(y + 1 < v)
    {
      if(x >= 1) buffer[index91] = clamp0255(buffer[index91] + (3 * error) / 16);
      buffer[index01] = clamp0255(buffer[index01] + (5 * error) / 16);
      if(x + 1 < u) buffer[index11] = clamp0255(buffer[index11] + (1 * error) / 16);
    }
  }
  


  //Floyd-Steinberg (wrapping, not fully good looking)
  /*for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  for(size_t c = 0; c < 4; c++)
  {
    size_t x9 = x == 0 ? u - 1 : x - 1;
    size_t x1 = x == u - 1 ? 0 : x + 1;
    size_t y1 = y == v - 1 ? 0 : y + 1;
    size_t index = y * u2 * 4 + x * 4 + c;
    size_t index10 = y * u2 * 4 + x1 * 4 + c;
    size_t index11 = y1 * u2 * 4 + x1 * 4 + c;
    size_t index01 = y1 * u2 * 4 + x * 4 + c;
    size_t index91 = y1 * u2 * 4 + x9 * 4 + c;
    
    int value = buffer[index];
    
    if(value <= 192) buffer[index] = 0;
    else buffer[index] = 255;
    
    int error = value - buffer[index];
    buffer[index10] = clamp0255(buffer[index10] + (7 * error) / 16);
    buffer[index91] = clamp0255(buffer[index91] + (3 * error) / 16);
    buffer[index01] = clamp0255(buffer[index01] + (5 * error) / 16);
    buffer[index11] = clamp0255(buffer[index11] + (1 * error) / 16);
  }*/
}


////////////////////////////////////////////////////////////////////////////////

void OperationCrop::doit(Paint& paint, Progress&)
{
  int x0 = paint.sel.x0, y0 = paint.sel.y0, x1 = paint.sel.x1, y1 = paint.sel.y1;
  
  if(x1 <= x0 || y1 <= y0)
  {
    return;
  }
  
  if(paint.floatsel.active)
  {
    std::vector<unsigned char> crop;
    getAlignedBuffer(crop, &paint.floatsel.texture);
    paint.canvasRGBA8.setSize(x1 - x0, y1 - y0);
    setAlignedBuffer(&paint.canvasRGBA8, crop.empty() ? 0 : &crop[0]);
  }
  else
  {
    if(x0 < 0) x0 = 0;
    if(y0 < 0) y0 = 0;
    if(x1 > (int)paint.canvasRGBA8.getU()) x1 = paint.canvasRGBA8.getU();
    if(y1 > (int)paint.canvasRGBA8.getV()) y1 = paint.canvasRGBA8.getV();
    if(x1 <= x0 || y1 <= y0) return;
    
    std::vector<unsigned char> crop(4 * (y1 - y0) * (x1 - x0));
    const unsigned char* buffer = paint.canvasRGBA8.getBuffer();
    size_t u2 = paint.canvasRGBA8.getU2();
    for(int y = 0; y < y1 - y0; y++)
    for(int x = 0; x < x1 - x0; x++)
    for(int c = 0; c < 4; c++)
    {
      size_t index = 4 * u2 * (y0 + y) + 4 * (x0 + x) + c;
      size_t index2 = 4 * (x1 - x0) * y + 4 * x + c;
      crop[index2] = buffer[index];
    }
    paint.canvasRGBA8.setSize(x1 - x0, y1 - y0);
    setAlignedBuffer(&paint.canvasRGBA8, crop.empty() ? 0 : &crop[0]);
  }
  paint.sel.x0 = paint.sel.y0 = paint.sel.x1 = paint.sel.y1 = 0;
  paint.floatsel.active = false;
}

bool OperationCrop::operate(Paint& paint, UndoState& state, Progress& progress)
{
  //not thread safe
  (void)paint;
  (void)state;
  (void)progress;
  return true;
}

bool OperationCrop::operateGL(Paint& paint, UndoState& state)
{
  if(paint.sel.x1 <= paint.sel.x0 || paint.sel.y1 <= paint.sel.y0)
  {
    lastError = "\
The crop filter uses the rectangular selection\n\
area as its boundary marker.\n\
\n\
Please create a rectangular selection with the\n\
selection tool to indicate the crop area, then\n\
activate the crop filter again.";
    return false;
  }
  
  state.addInt(paint.sel.x0);
  state.addInt(paint.sel.y0);
  state.addInt(paint.sel.x1);
  state.addInt(paint.sel.y1);
  state.addBool(paint.floatsel.active);
  
  storeCompleteTextureInUndo(state, paint.canvasRGBA8);
  doit(paint, dummyProgress);
  paint.update();

  return true;
}

void OperationCrop::undo(Paint& paint, UndoState& state)
{
  UndoStateIndex index;
  
  int x0 = state.getInt(index.iint++);
  int y0 = state.getInt(index.iint++);
  int x1 = state.getInt(index.iint++);
  int y1 = state.getInt(index.iint++);
  bool floating = state.getBool(index.ibool++);
  
  if(floating)
  {
    paint.sel.x0 = 0;
    paint.sel.y0 = 0;
    paint.sel.x1 = x1 - x0;
    paint.sel.y1 = y1 - y0;
    paint.makeSelectionFloating(lpi::RGB_Black, false);
  }
  
  paint.sel.x0 = x0;
  paint.sel.y0 = y0;
  paint.sel.x1 = x1;
  paint.sel.y1 = y1;
  
  readCompleteTextureFromUndo(paint.canvasRGBA8, state, index);
  paint.update();
}

void OperationCrop::redo(Paint& paint, UndoState& state)
{
  UndoStateIndex index;
  
  paint.sel.x0 = state.getInt(index.iint++);
  paint.sel.y0 = state.getInt(index.iint++);
  paint.sel.x1 = state.getInt(index.iint++);
  paint.sel.y1 = state.getInt(index.iint++);
  paint.floatsel.active = state.getBool(index.ibool++);

  doit(paint, dummyProgress);
  paint.update();
}

////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////

OperationAddRemoveBordersInternal::OperationAddRemoveBordersInternal(GlobalToolSettings& settings)
: AOperationFilterUtil(settings, true, true)
, left(0)
, right(0)
, top(0)
, bottom(0)
{
}

void OperationAddRemoveBordersInternal::doit(TextureRGBA8& texture, Progress&)
{
  int oldu = texture.getU();
  int oldv = texture.getV();
  if(-(left + right) >= oldu) return; //too small
  if(-(top + bottom) >= oldv) return; //too small

  std::vector<unsigned char> old;
  getAlignedBuffer(old, &texture);

  int u = oldu + left + right;
  int v = oldv + top + bottom;

  texture.setSize(u, v);

  unsigned char* buffer = texture.getBuffer();
  size_t u2 = texture.getU2();

  addRemoveBordersRGBA8(buffer, u2, u, v, &old[0], oldu, oldu, oldv, top, left, bottom, right, settings.rightColor255());
}

bool OperationAddRemoveBordersInternal::operate(Paint& paint, UndoState& state, Progress& progress)
{
  //not thread safe
  (void)paint;
  (void)state;
  (void)progress;
  return true;
}

bool OperationAddRemoveBordersInternal::operateGL(Paint& paint, UndoState& state)
{
  bool result = AOperationFilter::operate(paint, state, dummyProgress);
  paint.update();
  return result;
}

void OperationAddRemoveBordersInternal::doit(TextureRGBA32F& texture, Progress&)
{
  int oldu = texture.getU();
  int oldv = texture.getV();
  if(-(left + right) >= oldu) return; //too small
  if(-(top + bottom) >= oldv) return; //too small

  std::vector<float> old;
  getAlignedBuffer(old, &texture);

  int u = oldu + left + right;
  int v = oldv + top + bottom;

  texture.setSize(u, v);

  float* buffer = texture.getBuffer();
  size_t u2 = texture.getU2();

  addRemoveBordersRGBA32F(buffer, u2, u, v, &old[0], oldu, oldu, oldv, top, left, bottom, right, settings.rightColor);
}

void OperationAddRemoveBordersInternal::doit(TextureGrey8& texture, Progress&)
{
  int oldu = texture.getU();
  int oldv = texture.getV();
  if(-(left + right) >= oldu) return; //too small
  if(-(top + bottom) >= oldv) return; //too small

  std::vector<unsigned char> old;
  getAlignedBuffer(old, &texture);

  int u = oldu + left + right;
  int v = oldv + top + bottom;

  texture.setSize(u, v);

  unsigned char* buffer = texture.getBuffer();
  size_t u2 = texture.getU2();

  addRemoveBordersGrey8(buffer, u2, u, v, &old[0], oldu, oldu, oldv, top, left, bottom, right, settings.rightColor255());
}

void OperationAddRemoveBordersInternal::doit(TextureGrey32F& texture, Progress&)
{
  int oldu = texture.getU();
  int oldv = texture.getV();
  if(-(left + right) >= oldu) return; //too small
  if(-(top + bottom) >= oldv) return; //too small

  std::vector<float> old;
  getAlignedBuffer(old, &texture);

  int u = oldu + left + right;
  int v = oldv + top + bottom;

  texture.setSize(u, v);

  float* buffer = texture.getBuffer();
  size_t u2 = texture.getU2();

  addRemoveBordersGrey32F(buffer, u2, u, v, &old[0], oldu, oldu, oldv, top, left, bottom, right, settings.rightColor);
}

void OperationAddRemoveBordersInternal::setParameters(int left, int right, int top, int bottom)
{
  this->left = left;
  this->right = right;
  this->top = top;
  this->bottom = bottom;
}


////////////////////////////////////////////////////////////////////////////////

OperationAddRemoveBorders::OperationAddRemoveBorders(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom)
: OperationAddRemoveBordersInternal(settings)
{
  params.addLabel("Negative values decrease size");
  params.addInt("Left", &left, -1024, +1024, 1);
  params.addInt("Right", &right, -1024, +1024, 1);
  params.addInt("Top", &top, -1024, +1024, 1);
  params.addInt("Bottom", &bottom, -1024, +1024, 1);

  paramsToDialog(dialog, params, geom);
}


////////////////////////////////////////////////////////////////////////////////

OperationRescale::OperationRescale(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom)
: AOperationFilterUtil(settings, true, true)
, algo(ALGO_NEAREST)
, width(256)
, height(256)
, width_percent(200)
, height_percent(200)
, percent(true)
, wrapping(false)
{
  params.addResize("Size"
                 , &origwidth, &origheight
                 , &width, &height
                 , &width_percent, &height_percent
                 , &percent);

  std::vector<Algo> enums;
  enums.push_back(ALGO_NEAREST);
  enums.push_back(ALGO_BILINEAR);
  enums.push_back(ALGO_BICUBIC);
  enums.push_back(ALGO_RAREST);
  enums.push_back(ALGO_ADDREMOVEBORDERS);
  enums.push_back(ALGO_SKEWED);
  std::vector<std::string> names;
  names.push_back("Nearest");
  names.push_back("Bilinear");
  names.push_back("Bicubic");
  names.push_back("Rarest");
  names.push_back("Clip/Borders");
  names.push_back("Skewed (Ugly!)");
  params.addEnum("Interpolation", &algo, names, enums);

  params.addBool("Wrapping", &wrapping);
  
  paramsToDialog(dialog, params, geom);
}

void OperationRescale::init(PaintWindow* pw)
{
  AOperationFilterUtil::init(pw);
  origwidth = pw->getPaint().getU();
  origheight = pw->getPaint().getV();
}

bool OperationRescale::operate(Paint& paint, UndoState& state, Progress& progress)
{
  //not thread safe
  (void)paint;
  (void)state;
  (void)progress;
  return true;
}

bool OperationRescale::operateGL(Paint& paint, UndoState& state)
{
  bool result = AOperationFilter::operate(paint, state, dummyProgress);
  paint.update();
  return result;
}

void OperationRescale::doit(TextureRGBA8& texture, Progress&)
{
  size_t oldu = texture.getU();
  size_t oldv = texture.getV();

  std::vector<unsigned char> old;
  getAlignedBuffer(old, &texture);

  size_t u = percent ? (int)(oldu * width_percent / 100.0) : width;
  size_t v = percent ? (int)(oldv * height_percent / 100.0) : height;

  texture.setSize(u, v);

  unsigned char* buffer = texture.getBuffer();
  size_t u2 = texture.getU2();

  if(algo == ALGO_NEAREST)
  {
    rescaleNearestRGBA8(buffer, u2, u, v, &old[0], oldu, oldu, oldv);
  }
  else if(algo == ALGO_BILINEAR)
  {
    rescaleBilinearRGBA8(buffer, u2, u, v, &old[0], oldu, oldu, oldv, wrapping);
  }
  else if(algo == ALGO_BICUBIC)
  {
    rescaleBicubicRGBA8(buffer, u2, u, v, &old[0], oldu, oldu, oldv, wrapping);
  }
  else if(algo == ALGO_RAREST)
  {
    rescaleRarestRGBA8(buffer, u2, u, v, &old[0], oldu, oldu, oldv, wrapping);
  }
  else if(algo == ALGO_ADDREMOVEBORDERS)
  {
    addRemoveBordersRGBA8(buffer, u2, u, v, &old[0], oldu, oldu, oldv, 0, 0, u - oldu, v - oldv, settings.rightColor255());
  }
  else if(algo == ALGO_SKEWED)
  {
    rescaleSkewedRGBA8(buffer, u2, u, v, &old[0], oldu, oldu, oldv, settings.rightColor255());
  }
}

void OperationRescale::doit(TextureRGBA32F& texture, Progress&)
{
  size_t oldu = texture.getU();
  size_t oldv = texture.getV();

  std::vector<float> old;
  getAlignedBuffer(old, &texture);

  size_t u = percent ? (int)(oldu * width_percent / 100.0) : width;
  size_t v = percent ? (int)(oldv * height_percent / 100.0) : height;

  texture.setSize(u, v);

  float* buffer = texture.getBuffer();
  size_t u2 = texture.getU2();

  if(algo == ALGO_NEAREST)
  {
    rescaleNearestRGBA32F(buffer, u2, u, v, &old[0], oldu, oldu, oldv);
  }
  else if(algo == ALGO_BILINEAR)
  {
    rescaleBilinearRGBA32F(buffer, u2, u, v, &old[0], oldu, oldu, oldv, wrapping);
  }
  else if(algo == ALGO_BICUBIC)
  {
    rescaleBicubicRGBA32F(buffer, u2, u, v, &old[0], oldu, oldu, oldv, wrapping);
  }
  else if(algo == ALGO_RAREST)
  {
    rescaleRarestRGBA32F(buffer, u2, u, v, &old[0], oldu, oldu, oldv, wrapping);
  }
  else if(algo == ALGO_ADDREMOVEBORDERS)
  {
    addRemoveBordersRGBA32F(buffer, u2, u, v, &old[0], oldu, oldu, oldv, 0, 0, u - oldu, v - oldv, settings.rightColor);
  }
  else if(algo == ALGO_SKEWED)
  {
    rescaleSkewedRGBA32F(buffer, u2, u, v, &old[0], oldu, oldu, oldv, settings.rightColor);
  }
}

void OperationRescale::doit(TextureGrey8& texture, Progress&)
{
  size_t oldu = texture.getU();
  size_t oldv = texture.getV();

  std::vector<unsigned char> old;
  getAlignedBuffer(old, &texture);

  size_t u = percent ? (int)(oldu * width_percent / 100.0) : width;
  size_t v = percent ? (int)(oldv * height_percent / 100.0) : height;

  texture.setSize(u, v);

  unsigned char* buffer = texture.getBuffer();
  size_t u2 = texture.getU2();

  if(algo == ALGO_NEAREST)
  {
    rescaleNearestGrey8(buffer, u2, u, v, &old[0], oldu, oldu, oldv);
  }
  else if(algo == ALGO_BILINEAR)
  {
    rescaleBilinearGrey8(buffer, u2, u, v, &old[0], oldu, oldu, oldv, wrapping);
  }
  else if(algo == ALGO_BICUBIC)
  {
    rescaleBicubicGrey8(buffer, u2, u, v, &old[0], oldu, oldu, oldv, wrapping);
  }
  else if(algo == ALGO_RAREST)
  {
    rescaleRarestGrey8(buffer, u2, u, v, &old[0], oldu, oldu, oldv, wrapping);
  }
  else if(algo == ALGO_ADDREMOVEBORDERS)
  {
    addRemoveBordersGrey8(buffer, u2, u, v, &old[0], oldu, oldu, oldv, 0, 0, u - oldu, v - oldv, settings.rightColor255());
  }
  else if(algo == ALGO_SKEWED)
  {
    rescaleSkewedGrey8(buffer, u2, u, v, &old[0], oldu, oldu, oldv, settings.rightColor255());
  }
}

void OperationRescale::doit(TextureGrey32F& texture, Progress&)
{
  size_t oldu = texture.getU();
  size_t oldv = texture.getV();

  std::vector<float> old;
  getAlignedBuffer(old, &texture);

  size_t u = percent ? (int)(oldu * width_percent / 100.0) : width;
  size_t v = percent ? (int)(oldv * height_percent / 100.0) : height;

  texture.setSize(u, v);

  float* buffer = texture.getBuffer();
  size_t u2 = texture.getU2();

  if(algo == ALGO_NEAREST)
  {
    rescaleNearestGrey32F(buffer, u2, u, v, &old[0], oldu, oldu, oldv);
  }
  else if(algo == ALGO_BILINEAR)
  {
    rescaleBilinearGrey32F(buffer, u2, u, v, &old[0], oldu, oldu, oldv, wrapping);
  }
  else if(algo == ALGO_BICUBIC)
  {
    rescaleBicubicGrey32F(buffer, u2, u, v, &old[0], oldu, oldu, oldv, wrapping);
  }
  else if(algo == ALGO_RAREST)
  {
    rescaleRarestGrey32F(buffer, u2, u, v, &old[0], oldu, oldu, oldv, wrapping);
  }
  else if(algo == ALGO_ADDREMOVEBORDERS)
  {
    addRemoveBordersGrey32F(buffer, u2, u, v, &old[0], oldu, oldu, oldv, 0, 0, u - oldu, v - oldv, settings.rightColor);
  }
  else if(algo == ALGO_SKEWED)
  {
    rescaleSkewedGrey32F(buffer, u2, u, v, &old[0], oldu, oldu, oldv, settings.rightColor);
  }
}

////////////////////////////////////////////////////////////////////////////////


OperationPixelArtScaling::OperationPixelArtScaling(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom)
: AOperationFilterBase(settings)
, algo(ALGO_hq2x)
, wrapping(false)
{

  std::vector<Algo> enums;
  enums.push_back(ALGO_Scale2x); enums.push_back(ALGO_Scale3x); enums.push_back(ALGO_hq2x);
  std::vector<std::string> names;
  names.push_back("Scale2x"); names.push_back("Scale3x"); names.push_back("hq2x");
  params.addEnum("Algorithm", &algo, names, enums);
  params.addBool("Wrapping", &wrapping);

  paramsToDialog(dialog, params, geom);
}

void OperationPixelArtScaling::doit(Paint& paint, Progress&)
{
  TextureRGBA8& texture = paint.canvasRGBA8;
  size_t u = texture.getU();
  size_t v = texture.getV();
  
  std::vector<unsigned char> in;
  getAlignedBuffer(in, &texture);
  
  if(algo == ALGO_Scale2x)
  {
    texture.setSize(u * 2, v * 2);
    scale2x(texture.getBuffer(), texture.getU2(), texture.getV2(), &in[0], u, v, wrapping);
  }
  else if(algo == ALGO_Scale3x)
  {
    texture.setSize(u * 3, v * 3);
    scale3x(texture.getBuffer(), texture.getU2(), texture.getV2(), &in[0], u, v, wrapping);
  }
  else if(algo == ALGO_hq2x)
  {
    texture.setSize(u * 2, v * 2);
    hq2x(texture.getBuffer(), texture.getU2(), texture.getV2(), &in[0], u, v, wrapping);
  }
}


bool OperationPixelArtScaling::operate(Paint& paint, UndoState& state, Progress& progress)
{
  //not thread safe
  (void)paint;
  (void)state;
  (void)progress;
  return true;
}

bool OperationPixelArtScaling::operateGL(Paint& paint, UndoState& state)
{
  storeCompleteTextureInUndo(state, paint.canvasRGBA8);
  doit(paint, dummyProgress);
  paint.update();

  return true;
}

void OperationPixelArtScaling::undo(Paint& paint, UndoState& state)
{
  UndoStateIndex index;
  UndoState temp;
  storeCompleteTextureInUndo(temp, paint.canvasRGBA8);
  readCompleteTextureFromUndo(paint.canvasRGBA8, state, index);
  state.swap(temp);
  paint.update();
}

void OperationPixelArtScaling::redo(Paint& paint, UndoState& state)
{
  UndoStateIndex index;
  UndoState temp;
  storeCompleteTextureInUndo(temp, paint.canvasRGBA8);
  readCompleteTextureFromUndo(paint.canvasRGBA8, state, index);
  state.swap(temp);
  paint.update();
}

////////////////////////////////////////////////////////////////////////////////

namespace
{
//todo: put this in imalg and for all color modes

/*
lock_scale: only scales if there's a scaling component, not if there's rotation or shear
wrapping: takes pixels from the original image instead of bg color for pixels outside of the original image after the transformation
*/
void applyTransformationMatrix(Paint& paint, const lpi::Matrix2& matrix, const lpi::ColorRGB& bg, bool smooth, bool lock_scale, bool wrapping)
{
  TextureRGBA8& texture = paint.canvasRGBA8;
  int oldu = texture.getU();
  int oldv = texture.getV();
  
  std::vector<unsigned char> old;
  getAlignedBuffer(old, &texture);
  
  lpi::Vector2 v00(0, 0);
  lpi::Vector2 v01(0, oldv);
  lpi::Vector2 v10(oldu, 0);
  lpi::Vector2 v11(oldu, oldv);
  
  lpi::Matrix2 inv = inverse(matrix);
  
  int xmin = 0, ymin = 0, xmax = 0, ymax = 0;
  int u, v;
  
  if(lock_scale)
  {
    /*//get scaling component from the matrix by taking length of the two vectors
    double scalex = matrix[0].length();
    double scaley = matrix[1].length();

    u = (int)(oldu * scalex + 0.5);
    v = (int)(oldv * scaley + 0.5);*/
    
    u = oldu;
    v = oldv;
  }
  else
  {
    lpi::Vector2 v00t = matrix * v00;
    lpi::Vector2 v01t = matrix * v01;
    lpi::Vector2 v10t = matrix * v10;
    lpi::Vector2 v11t = matrix * v11;

    xmin = (int)std::min(std::min(v00t.x,v01t.x), std::min(v10t.x,v11t.x));
    ymin = (int)std::min(std::min(v00t.y,v01t.y), std::min(v10t.y,v11t.y));
    xmax = (int)std::max(std::max(v00t.x,v01t.x), std::max(v10t.x,v11t.x));
    ymax = (int)std::max(std::max(v00t.y,v01t.y), std::max(v10t.y,v11t.y));

    u = xmax - xmin;
    v = ymax - ymin;
  }

  texture.setSize(u, v);
  
  size_t u2 = texture.getU2();
  unsigned char* buffer = texture.getBuffer();
  
  if(!smooth)
  {
    for(int y = 0; y < v; y++)
    for(int x = 0; x < u; x++)
    {
      size_t oindex = y * u2 * 4 + x * 4; //output index
      
      lpi::Vector2 v(x + xmin, y + ymin);
      lpi::Vector2 t = inv * v;
      int x2 = (int)t.x;
      int y2 = (int)t.y;
      
      if(wrapping)
      {
        //wrap x2 and y2 in the coordinates of the old image
        if(x2 < 0) x2 += oldu * ((-x2) / oldu + 1);
        if(x2 >= oldu) x2 -= oldu * ((x2 - oldu) / oldu);
        if(y2 < 0) y2 += oldv * ((-y2) / oldv + 1);
        if(y2 >= oldv) x2 -= oldv * ((y2 - oldv) / oldv);
        //TODO: be less lazy here and make the 4 lines of code above immediatly do the correct thing without the while loops below
        while(x2 < 0) x2 += oldu;
        while(x2 >= oldu) x2 -= oldu;
        while(y2 < 0) y2 += oldv;
        while(y2 >= oldv) y2 -= oldv;
      }
      
      if(x2 < (int)oldu && x2 >= 0 && y2 < (int)oldv && y2 >= 0)
      {
        size_t iindex = y2 * oldu * 4 + x2 * 4; //input index
        buffer[oindex + 0] = old[iindex + 0];
        buffer[oindex + 1] = old[iindex + 1];
        buffer[oindex + 2] = old[iindex + 2];
        buffer[oindex + 3] = old[iindex + 3];
      }
      else
      {
        buffer[oindex + 0] = bg.r;
        buffer[oindex + 1] = bg.g;
        buffer[oindex + 2] = bg.b;
        buffer[oindex + 3] = bg.a;
      }
    }
  }
  else
  {
    for(int y = 0; y < v; y++)
    for(int x = 0; x < u; x++)
    {
      lpi::ColorRGB color(0, 0, 0, 0);
      
      size_t oindex = y * u2 * 4 + x * 4; //output index
      
      static const int N = 2;
      static const double Ni = 1.0 / (double)N;
      
      //take N*N sample points for smooth
      for(int ys = 0; ys < N; ys++)
      for(int xs = 0; xs < N; xs++)
      {
        lpi::Vector2 v(x + xmin + xs * Ni + Ni / 2.0, y + ymin + ys * Ni + Ni / 2.0);
        lpi::Vector2 t = inv * v;
        int x2 = (int)t.x;
        int y2 = (int)t.y;
        
        if(wrapping)
        {
          //wrap x2 and y2 in the coordinates of the old image
          if(x2 < 0) x2 += oldu * ((-x2) / oldu + 1);
          if(x2 >= oldu) x2 -= oldu * ((x2 - oldu) / oldu);
          if(y2 < 0) y2 += oldv * ((-y2) / oldv + 1);
          if(y2 >= oldv) x2 -= oldv * ((y2 - oldv) / oldv);
          //TODO: be less lazy here and make the 4 lines of code above immediatly do the correct thing without the while loops below
          while(x2 < 0) x2 += oldu;
          while(x2 >= oldu) x2 -= oldu;
          while(y2 < 0) y2 += oldv;
          while(y2 >= oldv) y2 -= oldv;
        }


        if(x2 < (int)oldu && x2 >= 0 && y2 < (int)oldv && y2 >= 0)
        {
          size_t iindex = y2 * oldu * 4 + x2 * 4; //input index
          color.r += old[iindex + 0];
          color.g += old[iindex + 1];
          color.b += old[iindex + 2];
          color.a += old[iindex + 3];
        }
        else
        {
          color = color | bg;
        }
      }
      
      int num = N * N;
      color.r /= num;
      color.g /= num;
      color.b /= num;
      color.a /= num;
      
      buffer[oindex + 0] = color.r;
      buffer[oindex + 1] = color.g;
      buffer[oindex + 2] = color.b;
      buffer[oindex + 3] = color.a;
    }
  }
}
}

////////////////////////////////////////////////////////////////////////////////

void OperationRotate::doit(Paint& paint, Progress&)
{
  double rad = (angle / 360.0) * lpi::twopi;
  if(ccw) rad = -rad;
  
  lpi::Matrix2 matrix(std::cos(rad), std::sin(rad), -std::sin(rad), std::cos(rad));

  applyTransformationMatrix(paint, matrix, settings.rightColor255(), smooth, lock_scale, wrapping);
}


bool OperationRotate::operate(Paint& paint, UndoState& state, Progress& progress)
{
  //not thread safe
  (void)paint;
  (void)state;
  (void)progress;
  return true;
}

bool OperationRotate::operateGL(Paint& paint, UndoState& state)
{
  storeCompleteTextureInUndo(state, paint.canvasRGBA8);
  doit(paint, dummyProgress);
  paint.update();

  return true;
}

void OperationRotate::undo(Paint& paint, UndoState& state)
{
  UndoStateIndex index;
  UndoState temp;
  storeCompleteTextureInUndo(temp, paint.canvasRGBA8);
  readCompleteTextureFromUndo(paint.canvasRGBA8, state, index);
  state.swap(temp);
  paint.update();
}

void OperationRotate::redo(Paint& paint, UndoState& state)
{
  UndoStateIndex index;
  UndoState temp;
  storeCompleteTextureInUndo(temp, paint.canvasRGBA8);
  readCompleteTextureFromUndo(paint.canvasRGBA8, state, index);
  state.swap(temp);
  paint.update();
}

////////////////////////////////////////////////////////////////////////////////

Transform2DMatrixDialog::Transform2DMatrixDialog(OperationTransform2D* op, const lpi::gui::IGUIDrawer& geom)
: op(op)
, numbers(geom, 2, 2)
{
  addSubElement(&numbers, lpi::gui::Sticky(0.33,0, 0.0,0, 0.66,0, 0.66,0));
  addSubElement(&page, lpi::gui::Sticky(0.0,0, 0.66,0, 1.0,0, 1.0,0));

  std::vector<double> m(4);
  m[0] = 2; m[1] = 0; m[2] = 0; m[3] = 2;
  numbers.addPreset("Grow 2x", m);
  m[0] = 0.5; m[1] = 0; m[2] = 0; m[3] = 0.5;
  numbers.addPreset("Shrink 2x", m);
  m[0] = 1; m[1] = 1; m[2] = 0; m[3] = 1;
  numbers.addPreset("Skew left", m);
  m[0] = 1; m[1] = -1; m[2] = 0; m[3] = 1;
  numbers.addPreset("Skew right", m);
  m[0] = 1; m[1] = 0; m[2] = -1; m[3] = 1;
  numbers.addPreset("Skew up", m);
  m[0] = 1; m[1] = 0; m[2] = 1; m[3] = 1;
  numbers.addPreset("Skew down", m);
  
  page.addControl("Inverse", new lpi::gui::DynamicCheckbox(&op->inverse));
  page.addControl("Smooth", new lpi::gui::DynamicCheckbox(&op->smooth));
  page.addControl("Wrapping", new lpi::gui::DynamicCheckbox(&op->wrapping));
  page.addControl("Lock Scale", new lpi::gui::DynamicCheckbox(&op->lock_scale));
}

void Transform2DMatrixDialog::drawImpl(lpi::gui::IGUIDrawer& drawer) const
{
  numbers.draw(drawer);
  page.draw(drawer);
  numbers.controlToValue(op->matrix);
}

void Transform2DMatrixDialog::handleImpl(const lpi::IInput& input)
{
  numbers.handle(input);
  page.handle(input);
}


void OperationTransform2D::doit(Paint& paint, Progress&)
{
  lpi::Matrix2 matrix2(matrix[0], matrix[2], matrix[1], matrix[3]);
  if(inverse) matrix2.invert();
  applyTransformationMatrix(paint, matrix2, settings.rightColor255(), smooth, lock_scale, wrapping);
}

bool OperationTransform2D::operate(Paint& paint, UndoState& state, Progress& progress)
{
  //not thread save
  (void)paint;
  (void)state;
  (void)progress;

  return true;
}

bool OperationTransform2D::operateGL(Paint& paint, UndoState& state)
{
  storeCompleteTextureInUndo(state, paint.canvasRGBA8);
  doit(paint, dummyProgress);
  paint.update();

  return true;
}

void OperationTransform2D::undo(Paint& paint, UndoState& state)
{
  UndoStateIndex index;
  UndoState temp;
  storeCompleteTextureInUndo(temp, paint.canvasRGBA8);
  readCompleteTextureFromUndo(paint.canvasRGBA8, state, index);
  state.swap(temp);
  paint.update();
}

void OperationTransform2D::redo(Paint& paint, UndoState& state)
{
  UndoStateIndex index;
  UndoState temp;
  storeCompleteTextureInUndo(temp, paint.canvasRGBA8);
  readCompleteTextureFromUndo(paint.canvasRGBA8, state, index);
  state.swap(temp);
  paint.update();
} 

////////////////////////////////////////////////////////////////////////////////

OperationAdjustBrightnessContrast::OperationAdjustBrightnessContrast(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom)
: AOperationFilterUtil(settings, true, true)
, dialog(this)
, brightness(0)
, contrast(0)
, lgamma(0.0)
, affect_alpha(false)
{
  params.addDouble("Brightness", &brightness, -1.0, 1.0, 0.01);
  params.addDouble("Contrast", &contrast, -1.0, 1.0, 0.01);
  params.addDouble("Gamma (log)", &lgamma, -1.0, 1.0, 0.1);
  params.addBool("Affect Alpha", &affect_alpha);
  
  paramsToDialog(dialog.page, params, geom);
}

void OperationAdjustBrightnessContrast::doit(TextureRGBA8& texture, Progress&)
{
  unsigned char* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();
  
  adjustBrightnessContrastGammaRGBA8(buffer, u, v, u2, brightness, contrast, lgamma, affect_alpha);
}

void OperationAdjustBrightnessContrast::doit(TextureRGBA32F& texture, Progress&)
{
  float* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();
  
  adjustBrightnessContrastGammaRGBA32F(buffer, u, v, u2, brightness, contrast, lgamma, affect_alpha);
}

////////////////////////////////////////////////////////////////////////////////

OperationAdjustTemperature::OperationAdjustTemperature(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom)
: AOperationFilterUtil(settings, true, true)
, dialog(this)
, adjust(0)
{
  params.addDouble("Adjust", &adjust, -4.0, 4.0, 0.01);
  
  paramsToDialog(dialog.page, params, geom);
}

void OperationAdjustTemperature::doit(TextureRGBA8& texture, Progress&)
{
  unsigned char* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();
  
  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  {
    size_t index = y * u2 * 4 + x * 4;

    //TODO: this just adds some blue or yellow. Make an actual implementation that really adjusts the temperature of the color (in Kelvin or mired)
    buffer[index + 0] = std::max(0, std::min(255, (int)(buffer[index + 0] + 20 * adjust)));
    buffer[index + 1] = std::max(0, std::min(255, (int)(buffer[index + 1] + 0 * adjust)));
    buffer[index + 2] = std::max(0, std::min(255, (int)(buffer[index + 2] - 40 * adjust)));

  }
}

////////////////////////////////////////////////////////////////////////////////

OperationPosterize::OperationPosterize(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom)
: AOperationFilterUtil(settings, true, true)
, dialog(this)
, amount(4)
, affect_alpha(true)
{
  params.addInt("Amount", &amount, 2, 256, 1);
  params.addBool("Affect Alpha", &affect_alpha);
  
  paramsToDialog(dialog.page, params, geom);
}

void OperationPosterize::doit(TextureRGBA8& texture, Progress&)
{
  if(amount <= 1) return; //div through 0!
  
  unsigned char* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();

  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  for(size_t c = 0; c < 4; c++)
  {
    if(c == 3 && !affect_alpha) continue;
    
    size_t index = y * u2 * 4 + x * 4 + c;
    
    int value = buffer[index];
    
    value *= amount;
    value /= 256;
    value *= 255;
    value /= (amount - 1);
    
    buffer[index] = value;
  }
}

////////////////////////////////////////////////////////////////////////////////

OperationReduceColors::OperationReduceColors(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom)
: AOperationFilterUtil(settings, true, true)
, dialog(this)
, amount(256)
{
  params.addInt("Amount", &amount, 2, 256, 1);
  
  paramsToDialog(dialog.page, params, geom);
}

void OperationReduceColors::doit(TextureRGBA8& texture, Progress& progress)
{
  unsigned char* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();
  
  mediancutRGBA8(buffer, u, v, u2, amount, progress);
}

////////////////////////////////////////////////////////////////////////////////

OperationChannelSwitcher::OperationChannelSwitcher(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom)
: AOperationFilterUtil(settings, true, true)
, dialog(this)
, cm(CM_HSV)
, c1(SC_Luma)
, c2(SC_255)
, c3(SC_255)
, c4(SC_0)
, ca(SC_Alpha)
{

  std::vector<ColorModel> cmenums;
  for(size_t i = 0; i < CM_Count; i++) cmenums.push_back((ColorModel)i);
  std::vector<std::string> cmnames;
  cmnames.push_back("RGB");
  cmnames.push_back("HSV");
  cmnames.push_back("HSL");
  cmnames.push_back("CMYK");
  cmnames.push_back("CIE XYZ");
  cmnames.push_back("CIE Lab");
  cmnames.push_back("YPbPr");
  cmnames.push_back("YCbCr");


  params.addEnum("Color Model", &cm, cmnames, cmenums);

  std::vector<SourceChannel> scenums;
  for(size_t i = 0; i < SC_Count; i++) scenums.push_back((SourceChannel)i);
  std::vector<std::string> scnames;
  scnames.push_back("Red");
  scnames.push_back("Green");
  scnames.push_back("Blue");
  scnames.push_back("Alpha");
  scnames.push_back("Lightness");
  scnames.push_back("Value");
  scnames.push_back("Intensity");
  scnames.push_back("Luma");
  scnames.push_back("Chroma");
  scnames.push_back("Hue");
  scnames.push_back("Min");
  scnames.push_back("Middle");
  scnames.push_back("Max");
  scnames.push_back("0");
  scnames.push_back("64");
  scnames.push_back("128");
  scnames.push_back("192");
  scnames.push_back("255");

  params.addEnum("Channel 1", &c1, scnames, scenums);
  params.addEnum("Channel 2", &c2, scnames, scenums);
  params.addEnum("Channel 3", &c3, scnames, scenums);
  params.addEnum("(Channel 4)", &c4, scnames, scenums);
  params.addEnum("Channel Alpha", &ca, scnames, scenums);

  paramsToDialog(dialog.page, params, geom);
}

int OperationChannelSwitcher::getChannel(SourceChannel c, unsigned char* pixel)
{
  switch(c)
  {
    case SC_Red: return pixel[0];
    case SC_Green: return pixel[1];
    case SC_Blue: return pixel[2];
    case SC_Alpha: return pixel[3];
    case SC_Lightness: return (maxc(&pixel[0]) + minc(&pixel[0])) / 2;
    case SC_Value: return maxc(&pixel[0]);
    case SC_Intensity: return (pixel[0] + pixel[1] + pixel[2]) / 3;
    case SC_Luma: return lpi::RGBtoLuma(lpi::ColorRGB(pixel[0], pixel[1], pixel[2]));
    case SC_Chroma: return chroma(&pixel[0]);
    case SC_Hue: return lpi::RGBtoHue(lpi::ColorRGB(pixel[0], pixel[1], pixel[2]));
    case SC_Min: return minc(&pixel[0]);
    case SC_Middle:
    {
      int ma = maxc(&pixel[0]);
      int mi = minc(&pixel[0]);
      if((pixel[1] == ma && pixel[2] == mi) || (pixel[1] == mi && pixel[2] == ma)) return pixel[0];
      else if((pixel[0] == ma && pixel[2] == mi) || (pixel[0] == mi && pixel[2] == ma)) return pixel[1];
      else return pixel[2];
      
    }
    case SC_Max: return maxc(&pixel[0]);
    case SC_0: return 0;
    case SC_64: return 64;
    case SC_128: return 128;
    case SC_192: return 192;
    case SC_255: return 255;
    default: return 0;
  }
  
  return 0;
}

lpi::ColorRGB OperationChannelSwitcher::getColor(int channel1, int channel2, int channel3, int channel4, int channela,  ColorModel cm)
{
  switch(cm)
  {
    case CM_RGB:
    {
      return lpi::ColorRGB(channel1, channel2, channel3, channela);
      break;
    }
    case CM_HSV:
    {
      lpi::ColorHSV color(channel1, channel2, channel3, channela);
      return lpi::HSVtoRGB(color);
      break;
    }
    case CM_HSL:
    {
      lpi::ColorHSL color(channel1, channel2, channel3, channela);
      return lpi::HSLtoRGB(color);
      break;
    }
    case CM_CMYK:
    {
      lpi::ColorCMYK color(channel1, channel2, channel3, channel4, channela);
      return lpi::CMYKtoRGB(color);
      break;
    }
    case CM_XYZ:
    {
      lpi::ColorXYZ color(channel1, channel2, channel3, channela);
      return lpi::XYZtoRGB(color);
      break;
    }
    case CM_Lab:
    {
      lpi::ColorLab color(channel1, channel2, channel3, channela);
      return lpi::LabtoRGB(color);
      break;
    }
    case CM_YPbPr:
    {
      lpi::ColorYPbPr color(channel1, channel2, channel3, channela);
      return lpi::YPbPrtoRGB(color);
      break;
    }
    case CM_YCbCr:
    {
      lpi::ColorYCbCr color(channel1, channel2, channel3, channela);
      return lpi::YCbCrtoRGB(color);
      break;
    }
    default: return lpi::RGB_Black;
  }
  
  return lpi::RGB_Black;
}

void OperationChannelSwitcher::doit(TextureRGBA8& texture, Progress&)
{
  unsigned char* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();
  
  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  {
    
    size_t index = y * u2 * 4 + x * 4;
    
    int channel1 = getChannel(c1, &buffer[index]);
    int channel2 = getChannel(c2, &buffer[index]);
    int channel3 = getChannel(c3, &buffer[index]);
    int channel4 = getChannel(c4, &buffer[index]);
    int channela = getChannel(ca, &buffer[index]);
    
    lpi::ColorRGB c = getColor(channel1, channel2, channel3, channel4, channela, cm);
    
    c.clamp();
    
    buffer[index + 0] = c.r;
    buffer[index + 1] = c.g;
    buffer[index + 2] = c.b;
    buffer[index + 3] = c.a;
  }
}

////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////

OperationChannelSwitcherII::OperationChannelSwitcherII(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom)
: AOperationFilterUtil(settings, true, true)
, dialog(this)
, cr(CC_RGB_R)
, cg(CC_RGB_G)
, cb(CC_RGB_B)
, ca(CC_A)
, greyscale(false)
{
  std::vector<ColorChannel> enums;
  for(size_t i = 0; i < CC_Count; i++) enums.push_back((ColorChannel)i);
  std::vector<std::string> names;
  names.push_back("RGB - R"); names.push_back("RGB - G"); names.push_back("RGB - B");
  names.push_back("HSV - H"); names.push_back("HSV - S"); names.push_back("HSV - V");
  names.push_back("HSL - H"); names.push_back("HSL - S"); names.push_back("HSL - L");
  names.push_back("CMYK - C"); names.push_back("CMYK - M"); names.push_back("CMYK - Y"); names.push_back("CMYK - K");
  names.push_back("CIE XYZ - X"); names.push_back("CIE XYZ - Y"); names.push_back("CIE XYZ - Z");
  names.push_back("CIE Lab - L"); names.push_back("CIE Lab - a"); names.push_back("CIE Lab - b");
  names.push_back("YPbPr Y"); names.push_back("YPbPr Pb"); names.push_back("YPbPr Pr");
  names.push_back("YCbCr - Y"); names.push_back("YCbCr - Cb"); names.push_back("YCbCr - Cr");
  names.push_back("Alpha"); names.push_back("0");

  params.addEnum("Channel for R", &cr, names, enums);
  params.addEnum("Channel for G", &cg, names, enums);
  params.addEnum("Channel for B", &cb, names, enums);
  params.addEnum("Channel for A", &ca, names, enums);
  params.addBool("Greyscale (uses R)", &greyscale);
  
  paramsToDialog(dialog.page, params, geom);
}

double OperationChannelSwitcherII::getChannel(const lpi::ColorRGBd& color, ColorChannel type)
{
  using namespace lpi;

  switch(type)
  {
    case CC_RGB_R:
    case CC_RGB_G:
    case CC_RGB_B:
      {
        if(type == CC_RGB_R) return color.r;
        else if(type == CC_RGB_G) return color.g;
        else if(type == CC_RGB_B) return color.b;
      }
      break;
    case CC_HSV_H:
    case CC_HSV_S:
    case CC_HSV_V:
      {
        ColorHSVd convert = RGBtoHSV(color);
        if(type == CC_HSV_H) return convert.h;
        else if(type == CC_HSV_S) return convert.s;
        else if(type == CC_HSV_V) return convert.v;
      }
      break;
    case CC_HSL_H:
    case CC_HSL_S:
    case CC_HSL_L:
      {
        ColorHSLd convert = RGBtoHSL(color);
        if(type == CC_HSL_H) return convert.h;
        else if(type == CC_HSL_S) return convert.s;
        else if(type == CC_HSL_L) return convert.l;
      }
      break;
    case CC_CMYK_C:
    case CC_CMYK_M:
    case CC_CMYK_Y:
    case CC_CMYK_K:
      {
        ColorCMYKd convert = RGBtoCMYK(color);
        if(type == CC_CMYK_C) return convert.c;
        else if(type == CC_CMYK_M) return convert.m;
        else if(type == CC_CMYK_Y) return convert.y;
        else if(type == CC_CMYK_K) return convert.k;
      }
      break;
    case CC_XYZ_X:
    case CC_XYZ_Y:
    case CC_XYZ_Z:
      {
        ColorXYZd convert = RGBtoXYZ(color);
        if(type == CC_XYZ_X) return convert.x;
        else if(type == CC_XYZ_Y) return convert.y;
        else if(type == CC_XYZ_Z) return convert.z;
      }
      break;
    case CC_Lab_L:
    case CC_Lab_a:
    case CC_Lab_b:
      {
        ColorLabd convert = RGBtoLab(color);
        if(type == CC_Lab_L) return convert.l;
        else if(type == CC_Lab_a) return lpi::clamp(convert.a, 0.0, 1.0);
        else if(type == CC_Lab_b) return lpi::clamp(convert.b, 0.0, 1.0);
      }
      break;
    case CC_YPbPr_Y:
    case CC_YPbPr_Pb:
    case CC_YPbPr_Pr:
      {
        ColorYPbPrd convert = RGBtoYPbPr(color);
        if(type == CC_YPbPr_Y) return convert.y;
        else if(type == CC_YPbPr_Pb) return convert.pb;
        else if(type == CC_YPbPr_Pr) return convert.pr;
      }
      break;
    case CC_YCbCr_Y:
    case CC_YCbCr_Cb:
    case CC_YCbCr_Cr:
      {
        ColorYCbCrd convert = RGBtoYCbCr(color);
        if(type == CC_YCbCr_Y) return convert.y;
        else if(type == CC_YCbCr_Cb) return convert.cb;
        else if(type == CC_YCbCr_Cr) return convert.cr;
      }
      break;
    case CC_A:
      {
        return color.a;
      }
      break;
    case CC_0:
      {
        return 0;
      }
    default:
      {
        return 0;
      }
      break;
  }

  return 0;
}

void OperationChannelSwitcherII::doit(TextureRGBA8& texture, Progress&)
{
  unsigned char* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();

  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  {
    size_t index = y * u2 * 4 + x * 4;
    lpi::ColorRGBd color(buffer[index+0]/255.0, buffer[index+1]/255.0, buffer[index+2]/255.0, buffer[index+3]/255.0);
    if(greyscale)
    {
      int v = (int)(255.0 * getChannel(color, cr));
      buffer[index + 0] = buffer[index + 1] = buffer[index + 2] = v;
    }
    else
    {
      buffer[index + 0] = (int)(255.0 * getChannel(color, cr));
      buffer[index + 1] = (int)(255.0 * getChannel(color, cg));
      buffer[index + 2] = (int)(255.0 * getChannel(color, cb));
      buffer[index + 3] = (int)(255.0 * getChannel(color, ca));
    }
  }
}

////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////

OperationAdjustHSL::OperationAdjustHSL(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom)
: AOperationFilterUtil(settings, true, true)
, dialog(this)
, h(0)
, s(0)
, l(0)
, a(0)
{
  params.addInt("Hue Adjust", &h, -180, 180, 1);
  params.addInt("Saturation Adjust", &s, -100, 100, 1);
  params.addInt("Lightness Adjust", &l, -100, 100, 1);
  params.addInt("Alpha Adjust", &a, -100, 100, 1);
  
  paramsToDialog(dialog.page, params, geom);
}

void OperationAdjustHSL::doit(TextureRGBA8& texture, Progress& progress)
{
  unsigned char* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();
  
  for(size_t y = 0; y < v; y++)
  {
    if(progress.handle(y, v)) return;
    for(size_t x = 0; x < u; x++)
    {
      size_t index = y * u2 * 4 + x * 4;
      
      lpi::ColorRGBd rgb(buffer[index + 0] / 255.0, buffer[index + 1] / 255.0, buffer[index + 2] / 255.0, buffer[index + 3] / 255.0);
      lpi::ColorHSLd hsl = RGBtoHSL(rgb);
      hsl.h += h / 360.0; while(hsl.h < 0.0) hsl.h += 1.0; while(hsl.h > 1.0) hsl.h -= 1.0;
      hsl.s += s / 100.0; if(hsl.s < 0.0) hsl.s = 0.0; if(hsl.s > 1.0) hsl.s = 1.0;
      hsl.l += l / 100.0; if(hsl.l < 0.0) hsl.l = 0.0; if(hsl.l > 1.0) hsl.l = 1.0;
      hsl.a += a / 100.0; if(hsl.a < 0.0) hsl.a = 0.0; if(hsl.a > 1.0) hsl.a = 1.0;
      rgb = lpi::HSLtoRGB(hsl);
      buffer[index + 0] = (int)(rgb.r * 255);
      buffer[index + 1] = (int)(rgb.g * 255);
      buffer[index + 2] = (int)(rgb.b * 255);
      buffer[index + 3] = (int)(rgb.a * 255);
    }
  }
}

////////////////////////////////////////////////////////////////////////////////

OperationAdjustHSV::OperationAdjustHSV(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom)
: AOperationFilterUtil(settings, true, true)
, dialog(this)
, h(0)
, s(0)
, v(0)
, a(0)
{
  params.addInt("Hue Adjust", &h, -180, 180, 1);
  params.addInt("Saturation Adjust", &s, -100, 100, 1);
  params.addInt("Value Adjust", &v, -100, 100, 1);
  params.addInt("Alpha Adjust", &a, -100, 100, 1);
  
  paramsToDialog(dialog.page, params, geom);
}

void OperationAdjustHSV::doit(TextureRGBA8& texture, Progress& progress)
{
  unsigned char* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t vv = texture.getV();
  size_t u2 = texture.getU2();

  for(size_t y = 0; y < vv; y++)
  {
    if(progress.handle(y, vv)) return;
    for(size_t x = 0; x < u; x++)
    {
      size_t index = y * u2 * 4 + x * 4;

      lpi::ColorRGBd rgb(buffer[index + 0] / 255.0, buffer[index + 1] / 255.0, buffer[index + 2] / 255.0, buffer[index + 3] / 255.0);
      lpi::ColorHSVd hsv = RGBtoHSV(rgb);
      hsv.h += h / 360.0; while(hsv.h < 0.0) hsv.h += 1.0; while(hsv.h > 1.0) hsv.h -= 1.0;
      hsv.s += s / 100.0; if(hsv.s < 0.0) hsv.s = 0.0; if(hsv.s > 1.0) hsv.s = 1.0;
      hsv.v += v / 100.0; if(hsv.v < 0.0) hsv.v = 0.0; if(hsv.v > 1.0) hsv.v = 1.0;
      hsv.a += a / 100.0; if(hsv.a < 0.0) hsv.a = 0.0; if(hsv.a > 1.0) hsv.a = 1.0;
      rgb = lpi::HSVtoRGB(hsv);
      buffer[index + 0] = (int)(rgb.r * 255);
      buffer[index + 1] = (int)(rgb.g * 255);
      buffer[index + 2] = (int)(rgb.b * 255);
      buffer[index + 3] = (int)(rgb.a * 255);
    }
  }
}

////////////////////////////////////////////////////////////////////////////////

OperationMultiplySLA::OperationMultiplySLA(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom)
: AOperationFilterUtil(settings, true, true)
, dialog(this)
, s(1)
, l(1)
, a(1)
, inverted(false)
{
  params.addDouble("Saturation Factor", &s, 0.0, 5.0, 0.01);
  params.addDouble("Lightness Factor", &l, 0.0, 5.0, 0.01);
  params.addDouble("Alpha Factor", &a, 0.0, 5.0, 0.01);
  params.addBool("Inverted", &inverted);
  
  paramsToDialog(dialog.page, params, geom);
}

void OperationMultiplySLA::doit(TextureRGBA8& texture, Progress&)
{
  unsigned char* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();

  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  {
    size_t index = y * u2 * 4 + x * 4;

    lpi::ColorRGBd rgb(buffer[index + 0] / 255.0, buffer[index + 1] / 255.0, buffer[index + 2] / 255.0, buffer[index + 3] / 255.0);
    lpi::ColorHSLd hsl = RGBtoHSL(rgb);
    if(inverted)
    {
      hsl.l = 1.0 - ((1.0 - hsl.l) * l);
      hsl.s = 1.0 - ((1.0 - hsl.s) * s);
      hsl.a = 1.0 - ((1.0 - hsl.a) * a);
    }
    else
    {
      hsl.l *= l;
      hsl.s *= s;
      hsl.a *= a;
    }
    if(hsl.s < 0.0) hsl.s = 0.0; if(hsl.s > 1.0) hsl.s = 1.0;
    if(hsl.l < 0.0) hsl.l = 0.0; if(hsl.l > 1.0) hsl.l = 1.0;
    if(hsl.a < 0.0) hsl.a = 0.0; if(hsl.a > 1.0) hsl.a = 1.0;
    rgb = HSLtoRGB(hsl);
    buffer[index + 0] = (int)(rgb.r * 255);
    buffer[index + 1] = (int)(rgb.g * 255);
    buffer[index + 2] = (int)(rgb.b * 255);
    buffer[index + 3] = (int)(rgb.a * 255);
  }
}

////////////////////////////////////////////////////////////////////////////////

OperationColorArithmetic::OperationColorArithmetic(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom)
: AOperationFilterUtil(settings, true, true)
, dialog(this)
, color(0.0, 0.33, 0.66, 1.0)
, affect_alpha(false)
, op(OP_ADD)
, overflow(OF_CLAMP)
{
  params.addColor("Color", &color);
  params.addBool("Affect Alpha", &affect_alpha);

  std::vector<Op> enums;
  enums.push_back(OP_ADD);
  enums.push_back(OP_SUBTRACT);
  enums.push_back(OP_MULTIPLY);
  enums.push_back(OP_INVMULTIPLY);
  enums.push_back(OP_DIVIDE);
  enums.push_back(OP_LIGHTEST);
  enums.push_back(OP_DARKEST);
  //enums.push_back(OP_GREYEST);
  enums.push_back(OP_AND255);
  enums.push_back(OP_OR255);
  enums.push_back(OP_XOR255);
  std::vector<std::string> names;
  names.push_back("Add");
  names.push_back("Subtract");
  names.push_back("Multiply");
  names.push_back("Inverted Multiply");
  names.push_back("Divide");
  names.push_back("Lightest");
  names.push_back("Darkest");
  //names.push_back("Most Grey");
  names.push_back("And");
  names.push_back("Or");
  names.push_back("Xor");
  params.addEnum("Operation", &op, names, enums);
  
  std::vector<Overflow> enums2;
  enums2.push_back(OF_CLAMP);
  enums2.push_back(OF_WRAP);
  std::vector<std::string> names2;
  names2.push_back("Clamp");
  names2.push_back("Wrap");
  params.addEnum("Overflow", &overflow, names2, enums2);
  params.addToolTipToLastRow("Not appliccable for floating point colors");
  
  
  paramsToDialog(dialog.page, params, geom);
}

void OperationColorArithmetic::calc(lpi::ColorRGBd& rgb)
{
  switch(op)
  {
    case OP_ADD:
    {
      rgb.r += color.r;
      rgb.g += color.g;
      rgb.b += color.b;
      rgb.a += color.a;
      break;
    }
    case OP_SUBTRACT:
    {
      rgb.r -= color.r;
      rgb.g -= color.g;
      rgb.b -= color.b;
      rgb.a -= color.a;
      break;
    }
    case OP_MULTIPLY:
    {
      rgb.r *= color.r;
      rgb.g *= color.g;
      rgb.b *= color.b;
      rgb.a *= color.a;
      break;
    }
    case OP_INVMULTIPLY:
    {
      rgb.r = 1.0 - ((1.0 - rgb.r) * (1.0 - color.r));
      rgb.g = 1.0 - ((1.0 - rgb.g) * (1.0 - color.g));
      rgb.b = 1.0 - ((1.0 - rgb.b) * (1.0 - color.b));
      rgb.a = 1.0 - ((1.0 - rgb.a) * (1.0 - color.a));
      break;
    }
    case OP_DIVIDE:
    {
      rgb.r /= color.r;
      rgb.g /= color.g;
      rgb.b /= color.b;
      rgb.a /= color.a;
      break;
    }
    case OP_LIGHTEST:
    {
      rgb.r = lpi::template_max(rgb.r, color.r);
      rgb.g = lpi::template_max(rgb.g, color.g);
      rgb.b = lpi::template_max(rgb.b, color.b);
      rgb.a = lpi::template_max(rgb.a, color.a);
      break;
    }
    case OP_DARKEST:
    {
      rgb.r = lpi::template_min(rgb.r, color.r);
      rgb.g = lpi::template_min(rgb.g, color.g);
      rgb.b = lpi::template_min(rgb.b, color.b);
      rgb.a = lpi::template_min(rgb.a, color.a);
      break;
    }
    /*case OP_GREYEST:
    {
      rgb.r = lpi::template_abs(rgb.r - 0.5) > lpi::template_abs(color.r - 0.5) ? color.r : rgb.r;
      rgb.g = lpi::template_abs(rgb.g - 0.5) > lpi::template_abs(color.g - 0.5) ? color.g : rgb.g;
      rgb.b = lpi::template_abs(rgb.b - 0.5) > lpi::template_abs(color.b - 0.5) ? color.b : rgb.b;
      rgb.a = lpi::template_abs(rgb.a - 0.5) > lpi::template_abs(color.a - 0.5) ? color.a : rgb.a;
      break;
    }*/
    case OP_OR255:
    {
      rgb.r = ((int)(255*rgb.r) | (int)(255*color.r)) / 255.0;
      rgb.g = ((int)(255*rgb.g) | (int)(255*color.g)) / 255.0;
      rgb.b = ((int)(255*rgb.b) | (int)(255*color.b)) / 255.0;
      rgb.a = ((int)(255*rgb.a) | (int)(255*color.a)) / 255.0;
      break;
    }
    case OP_AND255:
    {
      rgb.r = ((int)(255*rgb.r) & (int)(255*color.r)) / 255.0;
      rgb.g = ((int)(255*rgb.g) & (int)(255*color.g)) / 255.0;
      rgb.b = ((int)(255*rgb.b) & (int)(255*color.b)) / 255.0;
      rgb.a = ((int)(255*rgb.a) & (int)(255*color.a)) / 255.0;
      break;
    }
    case OP_XOR255:
    {
      rgb.r = ((int)(255*rgb.r) ^ (int)(255*color.r)) / 255.0;
      rgb.g = ((int)(255*rgb.g) ^ (int)(255*color.g)) / 255.0;
      rgb.b = ((int)(255*rgb.b) ^ (int)(255*color.b)) / 255.0;
      rgb.a = ((int)(255*rgb.a) ^ (int)(255*color.a)) / 255.0;
      break;
    }
  }
}

void OperationColorArithmetic::doit(TextureRGBA8& texture, Progress&)
{
  unsigned char* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();

  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  {
    size_t index = y * u2 * 4 + x * 4;

    lpi::ColorRGBd rgb(buffer[index + 0] / 255.0, buffer[index + 1] / 255.0, buffer[index + 2] / 255.0, buffer[index + 3] / 255.0);
    
    calc(rgb);

    if(overflow == OF_CLAMP) rgb.clamp();

    buffer[index + 0] = (int)(rgb.r * 255);
    buffer[index + 1] = (int)(rgb.g * 255);
    buffer[index + 2] = (int)(rgb.b * 255);
    if(affect_alpha) buffer[index + 3] = (int)(rgb.a * 255);
  }
}

void OperationColorArithmetic::doit(TextureRGBA32F& texture, Progress&)
{
  float* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();

  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  {
    size_t index = y * u2 * 4 + x * 4;

    lpi::ColorRGBd rgb(buffer[index + 0], buffer[index + 1], buffer[index + 2], buffer[index + 3]);

    calc(rgb);

    buffer[index + 0] = rgb.r;
    buffer[index + 1] = rgb.g;
    buffer[index + 2] = rgb.b;
    if(affect_alpha) buffer[index + 3] = rgb.a;
  }
}

////////////////////////////////////////////////////////////////////////////////

OperationColorArithmeticII::OperationColorArithmeticII(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom)
: AOperationFilterUtil(settings, true, true)
, a(1.0)
, b(0.0)
, c(1.0)
, d(1.0)
, e(2.0)
, affect_r(true)
, affect_g(true)
, affect_b(true)
, affect_a(false)
, formula(FORMULA1)
, dialog(this)
{
  //dialog.page.addTextRow("Formula: d * ((ax + b) ^ c) + f * (e ^ (ax + b))"); //x = color component, e is choosable value (not necessarily euler's number)
  //x = color component, e is choosable value (not necessarily euler's number)
  params.addLabel("Formula 1: d * ((ax + b) ^ c)");
  params.addLabel("Formula 2: d * (e ^ (ax + b))");
  params.addLabel("Formula 3: d * (log_e(ax + b))");
  params.addLabel("Formula 4: d / (ax + b)");

  std::vector<Formula> enums;
  enums.push_back(FORMULA1); enums.push_back(FORMULA2); enums.push_back(FORMULA3); enums.push_back(FORMULA4);
  std::vector<std::string> names;
  names.push_back("Formula 1"); names.push_back("Formula 2"); names.push_back("Formula 3"); names.push_back("Formula 4");
  params.addEnum("Formula", &formula, names, enums);

  params.addDouble("a", &a, -16.0, 16.0, 0.01);
  params.addDouble("b", &b, -255.0, 255.0, 0.01);
  params.addDouble("c", &c, -4.0, 4.0, 0.01);
  params.addDouble("d", &d, -16.0, 16.0, 0.01);
  params.addDouble("e", &e, 0.0, 4.0, 0.01);
  params.addBool("Affect R", &affect_r);
  params.addBool("Affect G", &affect_g);
  params.addBool("Affect B", &affect_b);
  params.addBool("Affect A", &affect_a);
  
  paramsToDialog(dialog.page, params, geom);
}

double OperationColorArithmeticII::calc(double value)
{
  double axb = a * value + b;
  
  double result = 0;
  
  switch(formula)
  {
    case FORMULA1:
      {
        result = axb;
        if(c != 1) result = std::pow(result, c);
        result *= d;
      }
      break;
    case FORMULA2:
      {
        result = axb;
        result = std::pow(e, result);
        result *= d;
      }
      break;
    case FORMULA3:
      {
        result = axb;
        result = std::log(result) / std::log(e);
        result *= d;
      }
      break;
    case FORMULA4:
      {
        result = d / axb;
      }
      break;
  }

  return result;
}

unsigned char OperationColorArithmeticII::clamp(double value)
{
  if(value > 255) value = 255;
  if(value < 0) value = 0;
  return (unsigned char)value;
}

void OperationColorArithmeticII::doit(TextureRGBA8& texture, Progress&)
{
  unsigned char* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();

  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  {
    size_t index = y * u2 * 4 + x * 4;
    if(affect_r) buffer[index + 0] = clamp(calc(buffer[index + 0]));
    if(affect_g) buffer[index + 1] = clamp(calc(buffer[index + 1]));
    if(affect_b) buffer[index + 2] = clamp(calc(buffer[index + 2]));
    if(affect_a) buffer[index + 3] = clamp(calc(buffer[index + 3]));
  }
}

void OperationColorArithmeticII::doit(TextureRGBA32F& texture, Progress&)
{
  float* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();

  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  {
    size_t index = y * u2 * 4 + x * 4;
    if(affect_r) buffer[index + 0] = calc(buffer[index + 0]);
    if(affect_g) buffer[index + 1] = calc(buffer[index + 1]);
    if(affect_b) buffer[index + 2] = calc(buffer[index + 2]);
    if(affect_a) buffer[index + 3] = calc(buffer[index + 3]);
  }
}

////////////////////////////////////////////////////////////////////////////////

static const int HISTOGRAMSIZE = 65536;

static int valueToIndex(float value)
{
  bool sign = (value < 0);
  
  if(sign) value = -value;
  
  static const int HH = HISTOGRAMSIZE / 2; //size of the positive or negative half (HH = half histogramsize)

  static const double p = std::pow((double)FLT_MAX, 2.0 / HH);
  static const double d = std::log(p);
  
  int i = HH / 2 + (int)(std::log(value) / d);
  if(i < 0 || value == 0) i = 0;
  if(i >= HH) i = HH - 1;
  
  if(sign) i = HH - 1 - i;
  else i = HH + i;
  
  return i;
}

    OperationToneMapHistogram::OperationToneMapHistogram(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom)
    : AOperationFilterUtil(settings, true, true)
    , dialog(this)
    , desaturate(false)
    {
      params.addBool("Desaturate", &desaturate);
      
      paramsToDialog(dialog.page, params, geom);
    }

void OperationToneMapHistogram::doit(TextureRGBA8& texture, Progress&)
{
  //negative values are made positive.
  
  std::vector<int> histogram(256);
  for(int i = 0; i < 256; i++)
  {
    histogram[i] = 0;
  }
  
  unsigned char* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();

  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  {
    size_t index = y * u2 * 4 + x * 4;
    
    lpi::ColorHSV hsv = lpi::RGBtoHSV(lpi::ColorRGB(buffer[index + 0], buffer[index + 1], buffer[index + 2]));
    unsigned char i = hsv.v;
    
    histogram[i]++;
  }
  
  std::vector<float> cumulativeHistogram(256);
  
  float f = 0.0f;
  
  for(int i = 0; i < 256; i++)
  {
    f += histogram[i] / (float)(u * v);
    
    cumulativeHistogram[i] = f;
  }

  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  {
    size_t index = y * u2 * 4 + x * 4;

    lpi::ColorHSV hsv = lpi::RGBtoHSV(lpi::ColorRGB(buffer[index + 0], buffer[index + 1], buffer[index + 2]));
    unsigned char i = hsv.v;
    
    float h = cumulativeHistogram[i];
    
    //make sure that it starts at zero
    h -= cumulativeHistogram[0];
    h /= (1.0 - cumulativeHistogram[0]);
    
    hsv.v = (int)(h * 255);
    if(desaturate) hsv.s = (int)(hsv.s * (1.0-h*h*h*h));
    
    lpi::ColorRGB c = lpi::HSVtoRGB(hsv);
    
    buffer[index + 0] = c.r;
    buffer[index + 1] = c.g;
    buffer[index + 2] = c.b;
  }
}

void OperationToneMapHistogram::doit(TextureGrey8& texture, Progress&)
{
  //negative values are made positive.
  
  std::vector<int> histogram(256);
  for(int i = 0; i < 256; i++)
  {
    histogram[i] = 0;
  }
  
  unsigned char* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();

  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  {
    size_t index = y * u2 + x;
    histogram[buffer[index]]++;
  }
  
  std::vector<float> cumulativeHistogram(256);
  
  float f = 0.0f;
  
  for(int i = 0; i < 256; i++)
  {
    f += histogram[i] / (float)(u * v);
    
    cumulativeHistogram[i] = f;
  }

  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  {
    size_t index = y * u2 + x;
    float h = cumulativeHistogram[buffer[index]];
    
    //make sure that it starts at zero
    h -= cumulativeHistogram[0];
    h /= (1.0 - cumulativeHistogram[0]);
    
    buffer[index] = (unsigned char)(h * 255);
  }
}

void OperationToneMapHistogram::doit(TextureRGBA32F& texture, Progress&)
{
  //negative values are made positive.
  
  std::vector<int> histogram(HISTOGRAMSIZE);
  for(int i = 0; i < HISTOGRAMSIZE; i++)
  {
    histogram[i] = 0;
  }
  
  float* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();

  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  {
    size_t index = y * u2 * 4 + x * 4;
    
    lpi::ColorHSVd hsv = lpi::RGBtoHSV(lpi::ColorRGBd(buffer[index + 0], buffer[index + 1], buffer[index + 2]));
    float value = hsv.v;
    
    
    int i = valueToIndex(value);
    
    histogram[i]++;
  }
  
  std::vector<float> cumulativeHistogram(HISTOGRAMSIZE);
  
  float f = 0.0f;
  
  for(int i = 0; i < HISTOGRAMSIZE; i++)
  {
    f += histogram[i] / (float)(u * v);
    
    cumulativeHistogram[i] = f;
  }

  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  {
    size_t index = y * u2 * 4 + x * 4;

    lpi::ColorHSVd hsv = lpi::RGBtoHSV(lpi::ColorRGBd(buffer[index + 0], buffer[index + 1], buffer[index + 2]));
    float value = hsv.v;
    
    int i = valueToIndex(value);
    
    float h = cumulativeHistogram[i];
    
    //make sure that it starts at zero
    h -= cumulativeHistogram[0];
    h /= (1.0 - cumulativeHistogram[0]);
    
    hsv.v = h;
    if(desaturate) hsv.s *= (1.0-h*h*h*h);
    
    lpi::ColorRGBd c = lpi::HSVtoRGB(hsv);
    
    buffer[index + 0] = c.r;
    buffer[index + 1] = c.g;
    buffer[index + 2] = c.b;
  }
}

void OperationToneMapHistogram::doit(TextureGrey32F& texture, Progress&)
{
  //negative values are made positive.
  
  std::vector<int> histogram(HISTOGRAMSIZE);
  for(int i = 0; i < HISTOGRAMSIZE; i++)
  {
    histogram[i] = 0;
  }
  
  float* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();

  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  {
    size_t index = y * u2 + x;
    float value = buffer[index];

    int i = valueToIndex(value);
    
    histogram[i]++;
  }
  
  std::vector<float> cumulativeHistogram(HISTOGRAMSIZE);
  
  float f = 0.0f;
  
  for(int i = 0; i < HISTOGRAMSIZE; i++)
  {
    f += histogram[i] / (float)(u * v);
    
    cumulativeHistogram[i] = f;
    
    //std::cout <<histogram[i]<<" "<<f<<std::endl;
    
    //std::cout << f << " ";
  }
  //std::cout<<std::endl;

  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  {
    size_t index = y * u2 + x;
    
    float value = buffer[index + 0];

    if(value != 0)
    {
      int i = valueToIndex(value);
      
      float h = cumulativeHistogram[i];
      
      //make sure that it starts at zero
      h -= cumulativeHistogram[0];
      h /= (1.0 - cumulativeHistogram[0]); 
      
      buffer[index] = h;

    }
  }
}

////////////////////////////////////////////////////////////////////////////////

static std::vector<double> makeLumaVector(double r, double g, double b)
{
  std::vector<double> result(16, 0);
  result[0] = r;
  result[5] = g;
  result[10] = b;
  result[15] = 1;
  return result;
}

static std::vector<double> makeLumaVector2(double r, double g, double b)
{
  std::vector<double> result(16, 0);
  result[0] = result[4] = result[8] = r;
  result[1] = result[5] = result[9] = g;
  result[2] = result[6] = result[10] = b;
  result[15] = 1;
  return result;
}

static std::vector<double> make9Vector(double a, double b, double c, double d, double e, double f, double g, double h, double i)
{
  std::vector<double> result(16, 0);
  result[0] = a;
  result[1] = b;
  result[2] = c;
  result[3] = 0;
  result[4] = d;
  result[5] = e;
  result[6] = f;
  result[7] = 0;
  result[8] = g;
  result[9] = h;
  result[10] = i;
  result[11] = 0;
  result[12] = 0;
  result[13] = 0;
  result[14] = 0;
  result[15] = 1;
  return result;
}

static void populateDefaultRGBAMatrices(Numbers& numbers)
{
  std::vector<double> base(16, 0);

  std::vector<double> rb = base;
  rb[2] = rb[5] = rb[8] = rb[15] = 1;
  numbers.addPreset("Swap R & B", rb);
  std::vector<double> rg = base;
  rg[1] = rg[4] = rg[10] = rg[15] = 1;
  numbers.addPreset("Swap R & G", rg);
  std::vector<double> gb = base;
  gb[0] = gb[6] = gb[9] = gb[15] = 1;
  numbers.addPreset("Swap G & B", gb);
  
  numbers.addPreset("CIE XYZ to CIE RGB", make9Vector(
      3.2406, -1.5372, -0.4983,
      -0.9689, 1.8758, 0.0415,
      0.0557, -0.2040, 1.0570));
  numbers.addPreset("sRGB to CIE XYZ", make9Vector(
      0.4124, 0.3576, 0.1805,
      0.2126, 0.7152,  0.0722,
      0.0193, 0.1192, 0.9505));
  
  numbers.addPreset("Luma BT.601", makeLumaVector2(0.299, 0.587, 0.114));
  numbers.addPreset("Luma BT.709", makeLumaVector2(0.2126, 0.7152, 0.0722));
  numbers.addPreset("Premultiply Luma BT.601", makeLumaVector(0.299, 0.587, 0.114));
  numbers.addPreset("Premultiply Luma BT.709", makeLumaVector(0.2126, 0.7152, 0.0722));
}

RGBAMatrixDialog::RGBAMatrixDialog(OperationRGBAMatrix* op, const lpi::gui::IGUIDrawer& geom)
: FilterDialogWithPreview(op)
, op(op)
, numbers(geom, 4, 4)
{
  addSubElement(&numbers, lpi::gui::Sticky(0.0,16, 0.0,0, 1.0,-16, 0.66,-MAXPREVIEWV));
  addSubElement(&page, lpi::gui::Sticky(0.0,0, 0.66,-MAXPREVIEWV, 1.0,0, 1.0,-MAXPREVIEWV));

  page.addControl("Mul", new lpi::gui::DynamicSliderSpinner<double>(&op->mul, -256.0, 256.0, 1.0, geom));
  page.addControl("Div", new lpi::gui::DynamicSliderSpinner<double>(&op->div, -256.0, 256.0, 1.0, geom));
  page.addControl("Add", new lpi::gui::DynamicSliderSpinner<double>(&op->add, -1.0, 1.0, 1.0, geom));
  page.addControl("Affect Alpha", new lpi::gui::DynamicCheckbox(&op->affect_alpha));

  populateDefaultRGBAMatrices(numbers);

}

void RGBAMatrixDialog::drawImpl(lpi::gui::IGUIDrawer& drawer) const
{
  numbers.draw(drawer);
  page.draw(drawer);
  FilterDialogWithPreview::drawImpl(drawer);
}

void RGBAMatrixDialog::handleImpl(const lpi::IInput& input)
{
  numbers.handle(input);
  page.handle(input);
  FilterDialogWithPreview::handleImpl(input);
  numbers.controlToValue(op->matrix);
  if(numbers.hasChanged()) updatePreview();
}

void OperationRGBAMatrix::calc(lpi::ColorRGBd& rgba)
{
  lpi::ColorRGBd result;
  result.r = rgba.r * matrix[0] + rgba.g * matrix[1] + rgba.b * matrix[2] + rgba.a * matrix[3];
  result.g = rgba.r * matrix[4] + rgba.g * matrix[5] + rgba.b * matrix[6] + rgba.a * matrix[7];
  result.b = rgba.r * matrix[8] + rgba.g * matrix[9] + rgba.b * matrix[10] + rgba.a * matrix[11];
  if(affect_alpha) result.a = rgba.r * matrix[12] + rgba.g * matrix[13] + rgba.b * matrix[14] + rgba.a * matrix[15];
  result.r *= mul; result.g *= mul; result.b *= mul; if(affect_alpha) result.a *= mul;
  result.r /= div; result.g /= div; result.b /= div; if(affect_alpha) result.a /= div;
  result.r += add; result.g += add; result.b += add; if(affect_alpha) result.a += add;
  rgba = result;
}

void OperationRGBAMatrix::doit(TextureRGBA8& texture, Progress&)
{
  unsigned char* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();

  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  {
    size_t index = y * u2 * 4 + x * 4;

    lpi::ColorRGBd rgba(buffer[index + 0] / 255.0, buffer[index + 1] / 255.0, buffer[index + 2] / 255.0, buffer[index + 3] / 255.0);

    calc(rgba);

    if(rgba.r < 0.0) rgba.r = 0.0; if(rgba.r > 1.0) rgba.r = 1.0;
    if(rgba.g < 0.0) rgba.g = 0.0; if(rgba.g > 1.0) rgba.g = 1.0;
    if(rgba.b < 0.0) rgba.b = 0.0; if(rgba.b > 1.0) rgba.b = 1.0;
    if(rgba.a < 0.0) rgba.a = 0.0; if(rgba.a > 1.0) rgba.a = 1.0;

    buffer[index + 0] = (int)(rgba.r * 255);
    buffer[index + 1] = (int)(rgba.g * 255);
    buffer[index + 2] = (int)(rgba.b * 255);
    buffer[index + 3] = (int)(rgba.a * 255);
  }
}

void OperationRGBAMatrix::doit(TextureRGBA32F& texture, Progress&)
{
  float* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();

  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  {
    size_t index = y * u2 * 4 + x * 4;

    lpi::ColorRGBd rgba(buffer[index + 0], buffer[index + 1], buffer[index + 2], buffer[index + 3]);

    calc(rgba);

    buffer[index + 0] = rgba.r;
    buffer[index + 1] = rgba.g;
    buffer[index + 2] = rgba.b;
    buffer[index + 3] = rgba.a;
  }
}

////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////

bool OperationDeleteSelection::operate(Paint& paint, UndoState& state, Progress& progress)
{
  (void)paint;
  (void)state;
  (void)progress;
  return true;
}

bool OperationDeleteSelection::operateGL(Paint& paint, UndoState& state)
{
  state.addBool(paint.floatsel.active);
  state.addInt(paint.sel.x0);
  state.addInt(paint.sel.y0);
  state.addInt(paint.sel.x1);
  state.addInt(paint.sel.y1);
  storeColorInUndo(state, settings.rightColor255());
  
  if(paint.floatsel.active)
  {
    storeCompleteTextureInUndo(state, paint.floatsel.texture);
  }
  else
  {
    storePartialTextureInUndo(state, paint.canvasRGBA8, paint.sel.x0, paint.sel.y0, paint.sel.x1, paint.sel.y1);
  }
  
  paint.deleteSelection(settings.rightColor255());
  paint.update();

  return true;
}

void OperationDeleteSelection::undo(Paint& paint, UndoState& state)
{
  UndoStateIndex index;
  paint.floatsel.active = state.getBool(index.ibool++);
  paint.sel.x0 = state.getInt(index.iint++);
  paint.sel.y0 = state.getInt(index.iint++);
  paint.sel.x1 = state.getInt(index.iint++);
  paint.sel.y1 = state.getInt(index.iint++);
  /*lpi::ColorRGB bg =*/ readColorFromUndo(state, index);
  
  if(paint.floatsel.active)
  {
    readCompleteTextureFromUndo(paint.floatsel.texture, state, index);
  }
  else
  {
    readPartialTextureFromUndo(paint.canvasRGBA8, state, index);
  }
  paint.update();
}

void OperationDeleteSelection::redo(Paint& paint, UndoState& state)
{
  UndoStateIndex index;
  paint.floatsel.active = state.getBool(index.ibool++);
  paint.sel.x0 = state.getInt(index.iint++);
  paint.sel.y0 = state.getInt(index.iint++);
  paint.sel.x1 = state.getInt(index.iint++);
  paint.sel.y1 = state.getInt(index.iint++);
  lpi::ColorRGB bg = readColorFromUndo(state, index);
  
  paint.deleteSelection(bg);
  paint.update();
}

////////////////////////////////////////////////////////////////////////////////

OperationBlendSelection::OperationBlendSelection(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom)
: AOperationFilterBase(settings)
, opacity(1.0)
, treat_alpha_as_opacity(true)
, affect_alpha(false)
, blendMode(BM_AVERAGE)
{
  params.addDouble("Opacity", &opacity, 0.0, 1.0, 0.01);
  params.addBool("Treat Source Alpha As Opacity", &treat_alpha_as_opacity);
  params.addBool("Affect Target Alpha", &affect_alpha);

  std::vector<BlendMode> enums;
  enums.push_back(BM_ALPHA_BLEND);
  enums.push_back(BM_AVERAGE);
  enums.push_back(BM_DIFFERENCE);
  enums.push_back(BM_ADD);
  enums.push_back(BM_ADD_MIN_128);
  enums.push_back(BM_SUBTRACT);
  enums.push_back(BM_MULTIPLY);
  enums.push_back(BM_INVMULTIPLY);
  enums.push_back(BM_OVERLAY);
  enums.push_back(BM_HARD_LIGHT);
  enums.push_back(BM_SOFT_LIGHT);
  enums.push_back(BM_DIVIDE);
  enums.push_back(BM_DODGE);
  enums.push_back(BM_BURN);
  enums.push_back(BM_EXCLUSION);
  enums.push_back(BM_LIGHTEST);
  enums.push_back(BM_DARKEST);
  enums.push_back(BM_AND);
  enums.push_back(BM_OR);
  enums.push_back(BM_XOR);
  enums.push_back(BM_COPY_ALPHA);
  std::vector<std::string> names;
  names.push_back("Alpha Blend");
  names.push_back("Average");
  names.push_back("Difference");
  names.push_back("Add");
  names.push_back("Add Minus 128");
  names.push_back("Subtract");
  names.push_back("Multiply");
  names.push_back("Screen");
  names.push_back("Overlay");
  names.push_back("Hard Light");
  names.push_back("Soft Light");
  names.push_back("Divide");
  names.push_back("Dodge");
  names.push_back("Burn");
  names.push_back("Exclusion");
  names.push_back("Lightest");
  names.push_back("Darkest");
  names.push_back("And");
  names.push_back("Or");
  names.push_back("Xor");
  names.push_back("Copy Alpha");
  
  params.addEnum("Operation", &blendMode, names, enums);

  paramsToDialog(dialog, params, geom);
}

bool OperationBlendSelection::operate(Paint& paint, UndoState& state, Progress&)
{
  //not thread safe
  (void)paint;
  (void)state;
  return true;
}

bool OperationBlendSelection::operateGL(Paint& paint, UndoState& state)
{
  if(paint.getColorMode() != MODE_RGBA_8)
  {
    return false;
  }
  
  state.addBool(paint.floatsel.active);
  state.addInt(paint.sel.x0);
  state.addInt(paint.sel.y0);
  state.addInt(paint.sel.x1);
  state.addInt(paint.sel.y1);
  if(paint.floatsel.active) storeCompleteTextureInUndo(state, paint.floatsel.texture);
  storePartialTextureInUndo(state, paint.canvasRGBA8, paint.sel.x0, paint.sel.y0, paint.sel.x1, paint.sel.y1);

  if(paint.hasNonFloatingSelection()) paint.makeSelectionFloating(settings.rightColor255(), false); //This OpenGL call cannot be done in the "::operate" thread :(
  
  int x0 = paint.sel.x0;
  int y0 = paint.sel.y0;
  int x1 = paint.sel.x1;
  int y1 = paint.sel.y1;
  
  int u = paint.canvasRGBA8.getU();
  int v = paint.canvasRGBA8.getV();
  int u2 = paint.canvasRGBA8.getU2();
  unsigned char* buffer = paint.canvasRGBA8.getBuffer();

  int su = paint.floatsel.texture.getU();
  int sv = paint.floatsel.texture.getV();
  int su2 = paint.floatsel.texture.getU2();
  unsigned char* sbuffer = paint.floatsel.texture.getBuffer();

  for(int y = y0; y < y1; y++)
  for(int x = x0; x < x1; x++)
  {
    int sx = x - x0;
    int sy = y - y0;
    if(x < 0 || y < 0 || x >= u || y >= v) continue; //selection partially outside image
    if(sx < 0 || sy < 0 || sx >= (int)su || sy >= (int)sv) continue; //extra safety
    
    
    size_t index = y * u2 * 4 + x * 4;
    size_t sindex = sy * su2 * 4 + sx * 4;
    
    //background
    int r = buffer[index + 0];
    int g = buffer[index + 1];
    int b = buffer[index + 2];
    int a = buffer[index + 3];
    //foreground
    int r2 = sbuffer[sindex + 0];
    int g2 = sbuffer[sindex + 1];
    int b2 = sbuffer[sindex + 2];
    int a2 = sbuffer[sindex + 3];
    
    int a3 = 255 - ((255 - a2) * a) / 255;
    if(a2 < a) a3 = std::min(a2, a3); //avoid color of fully transparent foreground leaking through
    
    
    //result
    double rr = 0;
    double rg = 0;
    double rb = 0;
    double ra = 0;
    
    double o = treat_alpha_as_opacity ? opacity * a2 / 255.0 : opacity;
    if(blendMode == BM_ALPHA_BLEND) o = opacity;

    switch(blendMode)
    {
      case BM_ALPHA_BLEND:
      {
        if(treat_alpha_as_opacity)
        {
          rr = (r2 * a3 + r * (255 - a3)) / 255.0;
          rg = (g2 * a3 + g * (255 - a3)) / 255.0;
          rb = (b2 * a3 + b * (255 - a3)) / 255.0;
          ra = a2 + ((255 - a2) * a) / 255.0;
        }
        else
        {
          rr = r2;
          rg = g2;
          rb = b2;
          ra = a2;
        }
        break;
      }
      case BM_AVERAGE:
      {
        rr = (r + r2) / 2;
        rg = (g + g2) / 2;
        rb = (b + b2) / 2;
        ra = (a + a2) / 2;
        break;
      }
      case BM_DIFFERENCE:
      {
        rr = r > r2 ? r - r2 : r2 - r;
        rg = g > g2 ? g - g2 : g2 - g;
        rb = b > b2 ? b - b2 : b2 - b;
        ra = a > a2 ? a - a2 : a2 - a;
        break;
      }
      case BM_ADD:
      {
        rr = r + r2;
        rg = g + g2;
        rb = b + b2;
        ra = a + a2;
        break;
      }
      case BM_ADD_MIN_128:
      {
        rr = r + r2 - 128;
        rg = g + g2 - 128;
        rb = b + b2 - 128;
        ra = a + a2 - 128;
        break;
      }
      case BM_SUBTRACT:
      {
        rr = r - r2;
        rg = g - g2;
        rb = b - b2;
        ra = a - a2;
        break;
      }
      case BM_MULTIPLY:
      {
        rr = r * r2 / 255.0;
        rg = g * g2 / 255.0;
        rb = b * b2 / 255.0;
        ra = a * a2 / 255.0;
        break;
      }
      case BM_INVMULTIPLY: //"Screen"
      {
        rr = 255.0 - ((255.0 - r) * (255.0 - r2) / 255.0);
        rg = 255.0 - ((255.0 - g) * (255.0 - g2) / 255.0);
        rb = 255.0 - ((255.0 - b) * (255.0 - b2) / 255.0);
        ra = 255.0 - ((255.0 - a) * (255.0 - a2) / 255.0);
        break;
      }
      case BM_OVERLAY:
      {
        rr = r < 128 ? r * r2 / 127.5 : 255.0 - ((255.0 - 2.0 * (r - 127.5)) * (255.0 - r2)) / 255.0;
        rg = g < 128 ? g * g2 / 127.5 : 255.0 - ((255.0 - 2.0 * (g - 127.5)) * (255.0 - g2)) / 255.0;
        rb = b < 128 ? b * b2 / 127.5 : 255.0 - ((255.0 - 2.0 * (b - 127.5)) * (255.0 - b2)) / 255.0;
        ra = a < 128 ? a * a2 / 127.5 : 255.0 - ((255.0 - 2.0 * (a - 127.5)) * (255.0 - a2)) / 255.0;
        break;
      }
      case BM_HARD_LIGHT:
      {
        rr = r2 < 128 ? r * r2 / 127.5 : 255.0 - ((255.0 - 2.0 * (r2 - 127.5)) * (255.0 - r)) / 255.0;
        rg = g2 < 128 ? g * g2 / 127.5 : 255.0 - ((255.0 - 2.0 * (g2 - 127.5)) * (255.0 - g)) / 255.0;
        rb = b2 < 128 ? b * b2 / 127.5 : 255.0 - ((255.0 - 2.0 * (b2 - 127.5)) * (255.0 - b)) / 255.0;
        ra = a2 < 128 ? a * a2 / 127.5 : 255.0 - ((255.0 - 2.0 * (a2 - 127.5)) * (255.0 - a)) / 255.0;
        break;
      }
      case BM_SOFT_LIGHT:
      {
        rr = r2 < 128 ? r * (r2 + 127.5) / 255.0 : 255.0 - (255.0 - r) * (1.5 * 255.0 - r2) / 255.0;
        rg = g2 < 128 ? g * (g2 + 127.5) / 255.0 : 255.0 - (255.0 - g) * (1.5 * 255.0 - g2) / 255.0;
        rb = b2 < 128 ? b * (b2 + 127.5) / 255.0 : 255.0 - (255.0 - b) * (1.5 * 255.0 - b2) / 255.0;
        ra = a2 < 128 ? a * (a2 + 127.5) / 255.0 : 255.0 - (255.0 - a) * (1.5 * 255.0 - a2) / 255.0;
        break;
      }
      case BM_DIVIDE:
      {
        rr = ((r/255.0) / ((r2+1)/256.0)) * 255.0;
        rg = ((g/255.0) / ((g2+1)/256.0)) * 255.0;
        rb = ((b/255.0) / ((b2+1)/256.0)) * 255.0;
        ra = ((a/255.0) / ((a2+1)/256.0)) * 255.0;
        break;
      }
      case BM_DODGE:
      {
        rr = r / (256.0 - r2) * 255.0;
        rg = g / (256.0 - g2) * 255.0;
        rb = b / (256.0 - b2) * 255.0;
        ra = a / (256.0 - a2) * 255.0;
        break;
      }
      case BM_BURN:
      {
        rr = 255.0 - (255.0 - r) / (1.0 + r2) * 256.0;
        rg = 255.0 - (255.0 - g) / (1.0 + g2) * 256.0;
        rb = 255.0 - (255.0 - b) / (1.0 + b2) * 256.0;
        ra = 255.0 - (255.0 - a) / (1.0 + a2) * 256.0;
        break;
      }
      case BM_EXCLUSION:
      {
        rr = 127.5 - (r - 127.5) * (r2 - 127.5) / 127.5;
        rg = 127.5 - (g - 127.5) * (g2 - 127.5) / 127.5;
        rb = 127.5 - (b - 127.5) * (b2 - 127.5) / 127.5;
        ra = 127.5 - (a - 127.5) * (a2 - 127.5) / 127.5;
        break;
      }
      case BM_LIGHTEST:
      {
        rr = std::max(r, r2);
        rg = std::max(g, g2);
        rb = std::max(b, b2);
        ra = std::max(a, a2);
        break;
      }
      case BM_DARKEST:
      {
        rr = std::min(r, r2);
        rg = std::min(g, g2);
        rb = std::min(b, b2);
        ra = std::min(a, a2);
        break;
      }
      case BM_AND:
      {
        rr = r & r2;
        rg = g & g2;
        rb = b & b2;
        ra = a & a2;
        break;
      }
      case BM_OR:
      {
        rr = r | r2;
        rg = g | g2;
        rb = b | b2;
        ra = a | a2;
        break;
      }
      case BM_XOR:
      {
        rr = r ^ r2;
        rg = g ^ g2;
        rb = b ^ b2;
        ra = a ^ a2;
        break;
      }
      case BM_COPY_ALPHA:
      {
        rr = r;
        rg = g;
        rb = b;
        ra = a2;
        break;
      }
      default:
      {
        rr = 0;
        rg = 0;
        rb = 0;
        ra = 0;
        break;
      }
      
    }
    
    buffer[index + 0] = lpi::clamp<int>((int)(rr * o + r * (1.0 - o)), 0, 255);
    buffer[index + 1] = lpi::clamp<int>((int)(rg * o + g * (1.0 - o)), 0, 255);
    buffer[index + 2] = lpi::clamp<int>((int)(rb * o + b * (1.0 - o)), 0, 255);
    if(affect_alpha) buffer[index + 3] = lpi::clamp<int>((int)(ra * o + a * (1.0 - o)), 0, 255);
  }

  paint.deleteSelection(lpi::ColorRGB(0,0,0,0));
  paint.uploadPartial(x0, y0, x1, y1);
  
  return true;
}

void OperationBlendSelection::undo(Paint& paint, UndoState& state)
{
  if(paint.getColorMode() != MODE_RGBA_8) return;

  UndoStateIndex index;
  paint.floatsel.active = state.getBool(index.ibool++);
  paint.sel.x0 = state.getInt(index.iint++);
  paint.sel.y0 = state.getInt(index.iint++);
  paint.sel.x1 = state.getInt(index.iint++);
  paint.sel.y1 = state.getInt(index.iint++);

  UndoState temp;
  temp.addBool(paint.floatsel.active);
  temp.addInt(paint.sel.x0);
  temp.addInt(paint.sel.y0);
  temp.addInt(paint.sel.x1);
  temp.addInt(paint.sel.y1);
  storePartialTextureInUndo(temp, paint.canvasRGBA8, paint.sel.x0, paint.sel.y0, paint.sel.x1, paint.sel.y1);

  if(paint.floatsel.active) readCompleteTextureFromUndo(paint.floatsel.texture, state, index);
  readPartialTextureFromUndo(paint.canvasRGBA8, state, index);
  
  state.swap(temp);
  
  paint.update();
}

void OperationBlendSelection::redo(Paint& paint, UndoState& state)
{
  if(paint.getColorMode() != MODE_RGBA_8) return;

  UndoStateIndex index;
  paint.floatsel.active = state.getBool(index.ibool++);
  paint.sel.x0 = state.getInt(index.iint++);
  paint.sel.y0 = state.getInt(index.iint++);
  paint.sel.x1 = state.getInt(index.iint++);
  paint.sel.y1 = state.getInt(index.iint++);

  UndoState temp;
  temp.addBool(paint.floatsel.active);
  temp.addInt(paint.sel.x0);
  temp.addInt(paint.sel.y0);
  temp.addInt(paint.sel.x1);
  temp.addInt(paint.sel.y1);
  if(paint.floatsel.active) storeCompleteTextureInUndo(temp, paint.floatsel.texture);
  storePartialTextureInUndo(temp, paint.canvasRGBA8, paint.sel.x0, paint.sel.y0, paint.sel.x1, paint.sel.y1);
  
  paint.deleteSelection(lpi::ColorRGB(0,0,0,0));
  readPartialTextureFromUndo(paint.canvasRGBA8, state, index);
  
  state.swap(temp);
  
  paint.update();
}

////////////////////////////////////////////////////////////////////////////////

bool OperationSelectAll::operate(Paint& paint, UndoState& state, Progress& progress)
{
  (void)paint;
  (void)state;
  (void)progress;
  return true;
}

bool OperationSelectAll::operateGL(Paint& paint, UndoState& state)
{
  state.addBool(paint.floatsel.active);
  state.addInt(paint.sel.x0);
  state.addInt(paint.sel.y0);
  state.addInt(paint.sel.x1);
  state.addInt(paint.sel.y1);
  
  if(paint.floatsel.active)
  {
    storeCompleteTextureInUndo(state, paint.floatsel.texture);
  }
  storePartialTextureInUndo(state, paint.canvasRGBA8, paint.sel.x0, paint.sel.y0, paint.sel.x1, paint.sel.y1);
  
  paint.selectAll();

  return true;

}

void OperationSelectAll::undo(Paint& paint, UndoState& state)
{
  UndoStateIndex index;
  paint.floatsel.active = state.getBool(index.ibool++);
  paint.sel.x0 = state.getInt(index.iint++);
  paint.sel.y0 = state.getInt(index.iint++);
  paint.sel.x1 = state.getInt(index.iint++);
  paint.sel.y1 = state.getInt(index.iint++);
  
  if(paint.floatsel.active)
  {
    readCompleteTextureFromUndo(paint.floatsel.texture, state, index);
  }
  readPartialTextureFromUndo(paint.canvasRGBA8, state, index);
}

void OperationSelectAll::redo(Paint& paint, UndoState& state)
{
  UndoStateIndex index;
  paint.floatsel.active = state.getBool(index.ibool++);
  paint.sel.x0 = state.getInt(index.iint++);
  paint.sel.y0 = state.getInt(index.iint++);
  paint.sel.x1 = state.getInt(index.iint++);
  paint.sel.y1 = state.getInt(index.iint++);
  
  paint.selectAll();
}

////////////////////////////////////////////////////////////////////////////////


bool OperationSelectNone::operate(Paint& paint, UndoState& state, Progress& progress)
{
  (void)paint;
  (void)state;
  (void)progress;
  return true;
}

bool OperationSelectNone::operateGL(Paint& paint, UndoState& state)
{
  state.addBool(paint.floatsel.active);
  state.addInt(paint.sel.x0);
  state.addInt(paint.sel.y0);
  state.addInt(paint.sel.x1);
  state.addInt(paint.sel.y1);
  
  if(paint.floatsel.active)
  {
    storeCompleteTextureInUndo(state, paint.floatsel.texture);
  }
  storePartialTextureInUndo(state, paint.canvasRGBA8, paint.sel.x0, paint.sel.y0, paint.sel.x1, paint.sel.y1);
  
  paint.selectNone();

  return true;
}

void OperationSelectNone::undo(Paint& paint, UndoState& state)
{
  UndoStateIndex index;
  paint.floatsel.active = state.getBool(index.ibool++);
  paint.sel.x0 = state.getInt(index.iint++);
  paint.sel.y0 = state.getInt(index.iint++);
  paint.sel.x1 = state.getInt(index.iint++);
  paint.sel.y1 = state.getInt(index.iint++);
  
  if(paint.floatsel.active)
  {
    readCompleteTextureFromUndo(paint.floatsel.texture, state, index);
  }
  readPartialTextureFromUndo(paint.canvasRGBA8, state, index);
}

void OperationSelectNone::redo(Paint& paint, UndoState& state)
{
  UndoStateIndex index;
  paint.floatsel.active = state.getBool(index.ibool++);
  paint.sel.x0 = state.getInt(index.iint++);
  paint.sel.y0 = state.getInt(index.iint++);
  paint.sel.x1 = state.getInt(index.iint++);
  paint.sel.y1 = state.getInt(index.iint++);
  
  paint.selectNone();
}


////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////

bool OperationSetColorMode::operate(Paint& paint, UndoState& state, Progress& progress)
{
  (void)paint;
  (void)state;
  (void)progress;
  return true;
}

bool OperationSetColorMode::operateGL(Paint& paint, UndoState& state)
{
  int oldMode = paint.getColorMode();
  int newMode = mode;
  
  state.addInt(oldMode);
  
  if(newMode != oldMode)
  {
    if(oldMode == MODE_RGBA_8) storeCompleteTextureInUndo(state, paint.canvasRGBA8);
    else if(oldMode == MODE_RGBA_32F) storeCompleteTextureInUndo(state, paint.canvasRGBA32F);
    else if(oldMode == MODE_GREY_8) storeCompleteTextureInUndo(state, paint.canvasGrey8);
    else if(oldMode == MODE_GREY_32F) storeCompleteTextureInUndo(state, paint.canvasGrey32F);
  
    paint.setColorMode((ColorMode)newMode);
  }

  return true;
}

void OperationSetColorMode::undo(Paint& paint, UndoState& state)
{
  UndoStateIndex index;
  int oldMode = state.getInt(index.iint++);
  int newMode = paint.getColorMode();
  if(oldMode != newMode)
  {
    paint.setColorMode((ColorMode)oldMode);
    if(oldMode == MODE_RGBA_8) readCompleteTextureFromUndo(paint.canvasRGBA8, state, index);
    else if(oldMode == MODE_RGBA_32F) readCompleteTextureFromUndo(paint.canvasRGBA32F, state, index);
    else if(oldMode == MODE_GREY_8) readCompleteTextureFromUndo(paint.canvasGrey8, state, index);
    else if(oldMode == MODE_GREY_32F) readCompleteTextureFromUndo(paint.canvasGrey32F, state, index);

  }
}

void OperationSetColorMode::redo(Paint& paint, UndoState& state)
{
  (void)state;
  if(paint.getColorMode() != mode) paint.setColorMode(mode);
}

std::string OperationSetColorMode::getLabel() const
{
  if(mode == MODE_RGBA_8) return "RGBA8 (32 bpp)";
  else if(mode == MODE_RGBA_32F) return "RGBA32F (128 bpp, floating point)";
  else if(mode == MODE_GREY_8) return "Grey8 (greyscale, 8 bpp)";
  else if(mode == MODE_GREY_32F) return "Grey32F (greyscale, 32 bpp, floating point)";
  return "Unknown Mode?";
}

////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////

CurvesFilterDialog::CurvesFilterDialog(OperationCurves* filter, const lpi::gui::IGUIDrawer& geom)
: FilterDialogWithPreview(filter)
, filter(filter)
, curves(geom)
{
  (void)geom;
  addSubElement(&curves, lpi::gui::Sticky(0.0,16, 0.0,0, 1.0,-16, 0.66,-MAXPREVIEWV));
  addSubElement(&page, lpi::gui::Sticky(0.0,0, 0.66,-MAXPREVIEWV, 1.0,0, 1.0,-MAXPREVIEWV));

  page.addControl("Include Alpha", new lpi::gui::DynamicCheckbox(&filter->affect_alpha));

  std::vector<OperationCurves::Type> enums;
  enums.push_back(OperationCurves::TYPE_COLOR); enums.push_back(OperationCurves::TYPE_GREY);
  std::vector<std::string> names;
  names.push_back("Color"); names.push_back("Brightness");
  page.addControl("Type", new lpi::gui::DynamicEnum<OperationCurves::Type>(&filter->type, names, enums, geom));
}

void CurvesFilterDialog::drawImpl(lpi::gui::IGUIDrawer& drawer) const
{
  curves.draw(drawer);
  page.draw(drawer);
  FilterDialogWithPreview::drawImpl(drawer);
}

void CurvesFilterDialog::handleImpl(const lpi::IInput& input)
{
  curves.handle(input);
  curves.setMode(filter->type == OperationCurves::TYPE_GREY, filter->affect_alpha);
  page.handle(input);
  FilterDialogWithPreview::handleImpl(input);
}

////////////////////////////////////////////////////////////////////////////////

void OperationCurves::doit(TextureRGBA8& texture, Progress&)
{
  unsigned char* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();
  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  {
    size_t index = y * u2 * 4 + x * 4;
    if(type == TYPE_GREY)
    {
      lpi::ColorRGB rgb(buffer[index], buffer[index+1], buffer[index+2], buffer[index+3]);
      lpi::ColorHSL hsl = lpi::RGBtoHSL(rgb);
      if(hsl.l < 0) hsl.l = 0;
      if(hsl.l > 255) hsl.l = 255;
      hsl.l = dialog.curves.l[hsl.l];
      if(hsl.l < 0) hsl.l = 0;
      if(hsl.l > 255) hsl.l = 255;
      rgb = lpi::HSLtoRGB(hsl);
      buffer[index+0] = rgb.r;
      buffer[index+1] = rgb.g;
      buffer[index+2] = rgb.b;
    }
    else if(type == TYPE_COLOR)
    {
      buffer[index + 0] = dialog.curves.r[buffer[index + 0]];
      buffer[index + 1] = dialog.curves.g[buffer[index + 1]];
      buffer[index + 2] = dialog.curves.b[buffer[index + 2]];
    }
    if(affect_alpha)
    {
      buffer[index + 3] = dialog.curves.a[buffer[index + 3]];
    }
  }
}



////////////////////////////////////////////////////////////////////////////////

OperationLevels::OperationLevels(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom)
: AOperationFilterUtil(settings, true, true)
, dialog(this)
, input_black(0)
, input_midpoint(0.5)
, input_white(1)
, output_black(0)
, output_white(1)
, affect_r(true)
, affect_g(true)
, affect_b(true)
, affect_a(false)
{
  params.addDouble("Input Low", &input_black, 0.0, 1.0, 0.01);
  params.addDouble("Input Mid (relative)", &input_midpoint, 0.0, 1.0, 0.01);
  params.addDouble("Input High", &input_white, 0.0, 1.0, 0.01);
  params.addDouble("Output Low", &output_black, 0.0, 1.0, 0.01);
  params.addDouble("Output High", &output_white, 0.0, 1.0, 0.01);
  std::vector<bool*> bools; bools.push_back(&affect_r); bools.push_back(&affect_g); bools.push_back(&affect_b); bools.push_back(&affect_a);
  params.addMultiBool("Affect R, G, B, A", bools);

  paramsToDialog(dialog.page, params, geom);
}

double OperationLevels::doFormula(double val) const
{
  //double grey = (input_black + input_white) / 2;
  //double grey = (input_mid - input_black) / (input_white - input_black);
  double grey = input_black * (1.0 - input_midpoint) + input_white * input_midpoint;
  
  if(val <= input_black) val = 0.0;
  else if(val >= input_white) val = 1.0;
  else if(val < grey)
  {
    val -= input_black;
    val /= ((grey - input_black) * 2);
  }
  else if(val >= grey)
  {
    val -= grey;
    val /= ((input_white - grey) * 2);
    val += 0.5;
  }
  
  val -= output_black;
  val *= (output_white - output_black);
  val += output_black;
  
  return val;
}

void OperationLevels::doit(TextureRGBA8& texture, Progress&)
{
  unsigned char* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();
  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  {
    size_t index = y * u2 * 4 + x * 4;
    
    if(affect_r) buffer[index + 0] = (int)(255 * doFormula(buffer[index + 0] / 255.0));
    if(affect_g) buffer[index + 1] = (int)(255 * doFormula(buffer[index + 1] / 255.0));
    if(affect_b) buffer[index + 2] = (int)(255 * doFormula(buffer[index + 2] / 255.0));
    if(affect_a) buffer[index + 3] = (int)(255 * doFormula(buffer[index + 3] / 255.0));
  }
}

////////////////////////////////////////////////////////////////////////////////

void OperationRGChromaticity::doit(TextureRGBA8& texture, Progress&)
{
  unsigned char* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();

  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  {
    size_t i = y * u2 * 4 + x * 4;
    double r = buffer[i + 0] / 255.0;
    double g = buffer[i + 1] / 255.0;
    double b = buffer[i + 2] / 255.0;
    double rgb = r + g + b;

    if(rgb == 0)
    {
      buffer[i + 0] = 85; //255/3
      buffer[i + 1] = 85;
      buffer[i + 2] = 85;
    }
    else
    {
      double r2 = r / rgb;
      double g2 = g / rgb;

      //due to small floating point error
      if(r2 + g2 > 1)
      {
        double f = r2 + g2;
        r2 /= f;
        g2 /= f;
      }

      double b2 = 1 - r2 - g2;
      buffer[i + 0] = (int)(r2 * 255);
      buffer[i + 1] = (int)(g2 * 255);
      buffer[i + 2] = (int)(b2 * 255);
    }
  }
}

////////////////////////////////////////////////////////////////////////////////

void OperationSpectrum::doit(TextureRGBA8& texture, Progress&)
{
  unsigned char* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();

  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  {
    size_t i = y * u2 * 4 + x * 4;
    double r = buffer[i + 0] / 255.0;
    double g = buffer[i + 1] / 255.0;
    double b = buffer[i + 2] / 255.0;

    //take 400-700 nm
    lpi::ColorXYZd wr = lpi::spectrumToXYZ(r * 300 + 400);
    lpi::ColorXYZd wg = lpi::spectrumToXYZ(g * 300 + 400);
    lpi::ColorXYZd wb = lpi::spectrumToXYZ(b * 300 + 400);
    lpi::ColorXYZd xyz;
    xyz.x = (wr.x + wg.x + wb.x) / 3;
    xyz.y = (wr.y + wg.y + wb.y) / 3;
    xyz.z = (wr.z + wg.z + wb.z) / 3;
    lpi::ColorRGBd rgb = lpi::XYZtoRGB(xyz);
    rgb.clamp();

    buffer[i + 0] = (int)(rgb.r * 255);
    buffer[i + 1] = (int)(rgb.g * 255);
    buffer[i + 2] = (int)(rgb.b * 255);
  }
}


////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
