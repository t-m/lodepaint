/*
LodePaint

Copyright (c) 2009-2010 Lode Vandevenne
All rights reserved.

This file is part of LodePaint.

LodePaint is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LodePaint is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LodePaint.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "paint_algos_geom.h"

#include "paint_algos.h"
#include "imalg/contour.h"
#include "lpi/lpi_math.h"
#include "lpi/lpi_math2d.h"

#include <algorithm>
#include <cmath>
#include <iostream>
#include <vector>

/*
intersectHLineLineSegment: returns the x coordinate of the location where the horizontal
line with y coordinate "y" intersects the line segment. Returns false if it doesn't intersect.
*/
static bool intersectHLineLineSegment(int& x, int y, int x0, int y0, int x1, int y1)
{
  if(y < std::min(y0, y1)) return false;
  if(y >= std::max(y0, y1)) return false;
  if(y1 == y0) return false;

  if(y0 > y1) { std::swap(x0, x1); std::swap(y0, y1); } //this is needed for having always the same behaviour, meant for semi translucent triangles that touch each other (having no overlap and no 1-pixel gaps between two triangles)

  x = ((y - y0) * (x1 - x0)) / (y1 - y0) + x0;

  return true;
}

void drawPolygonBorderThin(unsigned char* buffer, int buffer_w, int buffer_h, int* xs, int* ys, int num, const lpi::ColorRGB& color, double opacity, bool affect_alpha)
{
    for(int i = 0; i < num; i++)
    {
      int i1 = i + 1; if(i1 == num) i1 = 0;
      drawLine(buffer, buffer_w, buffer_h, xs[i], ys[i], xs[i1], ys[i1], 1, color, opacity, affect_alpha, false);
    }
}

void fillPolygon(unsigned char* buffer, int buffer_w, int buffer_h, int* xs, int* ys, int num, const lpi::ColorRGB& color, double opacity, bool affect_alpha)
{
  if(num < 3) return;

  int miny = ys[0];
  int maxy = ys[0];
  
  for(int i = 1; i < num; i++)
  {
    miny = std::min(miny, ys[i]);
    maxy = std::max(maxy, ys[i]);
  }

  int x;
  std::vector<int> positions;

  for(int y = miny; y <= maxy; y++)
  {
    positions.clear();
    for(int i = 0; i < num; i++)
    {
      int i1 = i + 1; if(i1 == num) i1 = 0;
      if(intersectHLineLineSegment(x, y, xs[i], ys[i], xs[i1], ys[i1])) positions.push_back(x);
    }

    if(positions.size() < 2 || positions.size() % 2 != 0) continue;
    std::sort(positions.begin(), positions.end());
    
    for(size_t i = 0; i < positions.size() - 1; i += 2)
    {
      horLine(buffer, buffer_w, buffer_h, buffer_w, y, positions[i], positions[i + 1], color, opacity, affect_alpha);
    }
  }

}

void fillPolygon(unsigned char* buffer, int buffer_w, int buffer_h, const Contour& polygon, const lpi::ColorRGB& color, double opacity, bool affect_alpha)
{
  if(polygon.size() < 3) return;

  double minx, maxx, miny, maxy;
  getContourBounds(minx, miny, maxx, maxy, polygon);

  double x;
  std::vector<double> positions;

  for(int y = (int)miny; y <= (int)maxy; y++)
  {
    positions.clear();
    for(int i = 0; i < (int)polygon.size(); i++)
    {
      int i1 = lpi::wrap(i + 1, 0, polygon.size());
      if(intersectHLineLineSegment(x, (double)y, polygon[i].x, polygon[i].y, polygon[i1].x, polygon[i1].y))
      {
        positions.push_back(x);
      }
    }

    if(positions.size() < 2 || positions.size() % 2 != 0) continue;
    std::sort(positions.begin(), positions.end());

    for(size_t i = 0; i < positions.size() - 1; i += 2)
    {
      horLine(buffer, buffer_w, buffer_h, buffer_w, y, (int)positions[i], (int)positions[i + 1], color, opacity, affect_alpha);
    }
  }
}


void fillPolyPolygon(unsigned char* buffer, int buffer_w, int buffer_h, PolyContour& poly, const lpi::ColorRGB& color, double opacity, bool affect_alpha)
{
  if(poly.size() < 1) return;

  double minx, maxx, miny, maxy;
  getContourBounds(minx, miny, maxx, maxy, poly);

  double x;
  std::vector<double> positions;

  for(int y = (int)miny; y <= (int)maxy; y++)
  {
    positions.clear();
    
    for(size_t j = 0; j < poly.size(); j++)
    {
      if(poly[j]->size() < 3) continue;
      const Contour& polygon = *poly[j];
      for(size_t i = 0; i < polygon.size(); i++)
      {
        int i1 = lpi::wrap(i + 1, 0, polygon.size());
        if(intersectHLineLineSegment(x, (double)y, polygon[i].x, polygon[i].y, polygon[i1].x, polygon[i1].y))
        {
          positions.push_back(x);
        }
      }
    }

    if(positions.size() < 2 || positions.size() % 2 != 0) continue;
    std::sort(positions.begin(), positions.end());

    for(size_t i = 0; i < positions.size() - 1; i += 2)
    {
      horLine(buffer, buffer_w, buffer_h, buffer_w, y, (int)positions[i], (int)positions[i + 1], color, opacity, affect_alpha);
    }
  }
}

/*
num = num polygons that make up the polypolygon
nums = number of vertices of each polygon
xs and ys: all the x/y data of all points of all polygons together
*/
void fillPolyPolygon(unsigned char* buffer, int buffer_w, int buffer_h, int** xs, int** ys, int* nums, int num, const lpi::ColorRGB& color, double opacity, bool affect_alpha)
{
  if(num < 1) return;
  if(nums[0] < 3) return;

  int miny = ys[0][0];
  int maxy = ys[0][0];
  
  for(int i = 1; i < num; i++)
  for(int j = 0; j < nums[i]; j++)
  {
    miny = std::min(miny, ys[i][j]);
    maxy = std::max(maxy, ys[i][j]);
  }

  int x;
  std::vector<int> positions;

  for(int y = miny; y < maxy; y++)
  {
    positions.clear();
    
    for(int i = 1; i < num; i++)
    for(int j = 0; j < nums[i]; j++)
    {
      int j1 = j + 1; if(j1 == nums[i]) j1 = 0;
      if(intersectHLineLineSegment(x, y, xs[i][j], ys[i][j], xs[i][j1], ys[i][j1])) positions.push_back(x);
    }
    
    if(positions.size() < 2 || positions.size() % 2 != 0) continue;
    std::sort(positions.begin(), positions.end());
    
    for(size_t i = 0; i < positions.size() - 1; i += 2)
    {
      horLine(buffer, buffer_w, buffer_h, buffer_w, y, positions[i], positions[i + 1], color, opacity, affect_alpha);
    }
  }
}



void drawPolygon(unsigned char* buffer, int buffer_w, int buffer_h
                , int* xs, int* ys, int num
                , int thickness
                , bool outline, const lpi::ColorRGB& lineColor, double lineOpacity
                , bool filled, const lpi::ColorRGB& fillColor, double fillOpacity
                , bool affect_alpha)
{
  if(outline)
  {
    if(thickness <= 1 || (thickness <= 2 && !filled))
    {
      if(filled) fillPolygon(buffer, buffer_w, buffer_h, xs, ys, num, fillColor, fillOpacity, affect_alpha);
      drawPolygonBorderThin(buffer, buffer_w, buffer_h, xs, ys, num, lineColor, lineOpacity, affect_alpha);
    }
    else
    {
      Contour polygon;
      intsToContour(polygon, xs, ys, num);
      PolyContour inner;
      
      offset(inner, polygon, thickness);

      inner.push_back(&polygon);
      fillPolyPolygon(buffer, buffer_w, buffer_h, inner, lineColor, lineOpacity, affect_alpha);
      inner.pop_back();
      if(filled) fillPolyPolygon(buffer, buffer_w, buffer_h, inner, fillColor, fillOpacity, affect_alpha);
    }
  }
  else if(filled) fillPolygon(buffer, buffer_w, buffer_h, xs, ys, num, fillColor, fillOpacity, affect_alpha);
}





//returns false if the triangle is too small to contain inner triangle for this thickness
//the points p0, p1, p2 must be counter clockwise.
bool getInnerTriangle(lpi::Vector2& i0, lpi::Vector2& i1, lpi::Vector2& i2, const lpi::Vector2& p0, const lpi::Vector2& p1, const lpi::Vector2& p2, int thickness)
{
  //     p0 
  //     /\                                                                .
  //  s2/  \s1
  //   /    \                                                              .
  //  /______\                                                             .
  // p1  s0   p2
  
  //sides
  lpi::Vector2 s0(p2 - p1);
  lpi::Vector2 s1(p0 - p2);
  lpi::Vector2 s2(p1 - p0);
  
  //sides rotated 90 degrees to the left & length set to thickness
  lpi::Vector2 r0(s0.y, -s0.x); r0.normalize(); r0 *= thickness;
  lpi::Vector2 r1(s1.y, -s1.x); r1.normalize(); r1 *= thickness;
  lpi::Vector2 r2(s2.y, -s2.x); r2.normalize(); r2 *= thickness;
  
  //inner triangle
  lpi::intersectLineLine(i0, p0 + r2, p1 + r2, p0 + r1, p2 + r1);
  lpi::intersectLineLine(i1, p1 + r2, p0 + r2, p1 + r0, p2 + r0);
  lpi::intersectLineLine(i2, p2 + r1, p0 + r1, p2 + r0, p1 + r0);

  bool big_enough = true;
  big_enough &= lpi::sideOfLineGivenByTwoPoints(i0, p1 + r0, p2 + r0) == sideOfLineGivenByTwoPoints(i0, p1, p2);
  big_enough &= lpi::sideOfLineGivenByTwoPoints(i1, p2 + r1, p0 + r1) == sideOfLineGivenByTwoPoints(i1, p2, p0);
  big_enough &= lpi::sideOfLineGivenByTwoPoints(i2, p0 + r2, p1 + r2) == sideOfLineGivenByTwoPoints(i2, p0, p1);
  big_enough &= pointInsideTriangle(i0, p0, p1, p2);
  big_enough &= pointInsideTriangle(i1, p0, p1, p2);
  big_enough &= pointInsideTriangle(i2, p0, p1, p2);

  return big_enough;
}

void fillTriangle(unsigned char* buffer, int buffer_w, int buffer_h, int x0, int y0, int x1, int y1, int x2, int y2, const lpi::ColorRGB& color, double opacity, bool affect_alpha)
{
  //make ccw? not needed
  //if((x1 - x0) * (y2 - y0) - (y1 - y0) * (x2 - x0) < 0)
  //{
    //std::swap(x0, x1);
    //std::swap(y0, y1);
  //}

  int miny = std::min(y0, std::min(y1, y2));
  int maxy = std::max(y0, std::max(y1, y2));

  int x;
  std::vector<int> positions;

  for(int y = miny; y < maxy; y++)
  {
    positions.clear();
    if(intersectHLineLineSegment(x, y, x0, y0, x1, y1)) positions.push_back(x);
    if(intersectHLineLineSegment(x, y, x0, y0, x2, y2)) positions.push_back(x);
    if(intersectHLineLineSegment(x, y, x1, y1, x2, y2)) positions.push_back(x);
    if(positions.size() < 2) continue;
    std::sort(positions.begin(), positions.end());
    horLine(buffer, buffer_w, buffer_h, buffer_w, y, positions[0], positions[1], color, opacity, affect_alpha);
  }

}

void drawTriangleOutline(unsigned char* buffer, int buffer_w, int buffer_h, int x0, int y0, int x1, int y1, int x2, int y2, int ix0, int iy0, int ix1, int iy1, int ix2, int iy2, const lpi::ColorRGB& color, double opacity, bool affect_alpha)
{
  fillTriangle(buffer, buffer_w, buffer_h, x0, y0, ix0, iy0, x2, y2, color, opacity, affect_alpha);
  fillTriangle(buffer, buffer_w, buffer_h, ix0, iy0, ix2, iy2, x2, y2, color, opacity, affect_alpha);
  fillTriangle(buffer, buffer_w, buffer_h, ix2, iy2, x1, y1, x2, y2, color, opacity, affect_alpha);
  fillTriangle(buffer, buffer_w, buffer_h, ix1, iy1, x1, y1, ix2, iy2, color, opacity, affect_alpha);
  fillTriangle(buffer, buffer_w, buffer_h, x0, y0, x1, y1, ix1, iy1, color, opacity, affect_alpha);
  fillTriangle(buffer, buffer_w, buffer_h, x0, y0, ix1, iy1, ix0, iy0, color, opacity, affect_alpha);
}

void drawTriangle(unsigned char* buffer, int buffer_w, int buffer_h
                , int x0, int y0, int x1, int y1, int x2, int y2
                , int thickness
                , bool outline, const lpi::ColorRGB& lineColor, double lineOpacity
                , bool filled, const lpi::ColorRGB& fillColor, double fillOpacity
                , bool affect_alpha)
{
  if(outline)
  {
    if(thickness <= 1 || (thickness <= 2 && !filled))
    {
      if(filled) fillTriangle(buffer, buffer_w, buffer_h, x0, y0, x1, y1, x2, y2, fillColor, fillOpacity, affect_alpha);
      drawLine(buffer, buffer_w, buffer_h, x0, y0, x1, y1, thickness, lineColor, lineOpacity, affect_alpha, false);
      drawLine(buffer, buffer_w, buffer_h, x1, y1, x2, y2, thickness, lineColor, lineOpacity, affect_alpha, false);
      drawLine(buffer, buffer_w, buffer_h, x2, y2, x0, y0, thickness, lineColor, lineOpacity, affect_alpha, false);
    }
    else
    {
      //points
      lpi::Vector2 p0(x0, y0);
      lpi::Vector2 p1(x1, y1);
      lpi::Vector2 p2(x2, y2);
      //I want them counterclockwise
      if(!ccw(p0, p1, p2)) std::swap(p0, p1);
      lpi::Vector2 i0, i1, i2;

      if(getInnerTriangle(i0, i1, i2, p0, p1, p2, thickness))
      {
        drawTriangleOutline(buffer, buffer_w, buffer_h
                          , (int)p0.x, (int)p0.y, (int)p1.x, (int)p1.y, (int)p2.x, (int)p2.y
                          , (int)i0.x, (int)i0.y, (int)i1.x, (int)i1.y, (int)i2.x, (int)i2.y
                          , lineColor, lineOpacity, affect_alpha);
        if(filled) fillTriangle(buffer, buffer_w, buffer_h, (int)i0.x, (int)i0.y, (int)i1.x, (int)i1.y, (int)i2.x, (int)i2.y, fillColor, fillOpacity, affect_alpha);
      }
      else
      {
        fillTriangle(buffer, buffer_w, buffer_h, x0, y0, x1, y1, x2, y2, lineColor, lineOpacity, affect_alpha);
      }
    }
  }
  else if(filled) fillTriangle(buffer, buffer_w, buffer_h, x0, y0, x1, y1, x2, y2, fillColor, fillOpacity, affect_alpha);
}

void fillRectangle(unsigned char* buffer, int buffer_w, int buffer_h, int x0, int y0, int x1, int y1, const lpi::ColorRGB& color, double opacity, bool affect_alpha)
{
  if(y0 > y1)
  {
    std::swap(x0, x1);
    std::swap(y0, y1);
  }

  if(x1 - x0 == 1) verLine(buffer, buffer_w, buffer_h, buffer_w, x0, y0, y1, color, opacity, affect_alpha);
  else
  {
    for(int y = y0; y < y1; y++)
    {
      horLine(buffer, buffer_w, buffer_h, buffer_w, y, x0, x1, color, opacity, affect_alpha);
    }
  }
}

void drawRectangleOutline(unsigned char* buffer, int buffer_w, int buffer_h, int x0, int y0, int x1, int y1, int ix0, int iy0, int ix1, int iy1, const lpi::ColorRGB& color, double opacity, bool affect_alpha)
{
  fillRectangle(buffer, buffer_w, buffer_h, x0, y0, x1, iy0, color, opacity, affect_alpha);
  fillRectangle(buffer, buffer_w, buffer_h, x0, iy0, ix0, iy1, color, opacity, affect_alpha);
  fillRectangle(buffer, buffer_w, buffer_h, x0, iy1, x1, y1, color, opacity, affect_alpha);
  fillRectangle(buffer, buffer_w, buffer_h, ix1, iy0, x1, iy1, color, opacity, affect_alpha);
}

void drawRectangle(unsigned char* buffer, int buffer_w, int buffer_h
                 , int x0, int y0, int x1, int y1
                 , int thickness
                 , bool outline, const lpi::ColorRGB& lineColor, double lineOpacity
                 , bool filled, const lpi::ColorRGB& fillColor, double fillOpacity
                 , bool affect_alpha)
{
  if(outline)
  {
    bool big_enough = lpi::template_abs(x0 - x1) > thickness * 2 && lpi::template_abs(y0 - y1) > thickness * 2;

    if(big_enough)
    {
      int ix0 = x1 > x0 ? x0 + thickness : x0 - thickness;
      int ix1 = x1 > x0 ? x1 - thickness : x1 + thickness;
      int iy0 = y1 > y0 ? y0 + thickness : y0 - thickness;
      int iy1 = y1 > y0 ? y1 - thickness : y1 + thickness;
      drawRectangleOutline(buffer, buffer_w, buffer_h
                        , x0, y0, x1, y1
                        , ix0, iy0, ix1, iy1
                        , lineColor, lineOpacity
                        , affect_alpha);
      if(filled) fillRectangle(buffer, buffer_w, buffer_h, ix0, iy0, ix1, iy1, fillColor, fillOpacity, affect_alpha);
    }
    else
    {
      fillRectangle(buffer, buffer_w, buffer_h, x0, y0, x1, y1, lineColor, lineOpacity, affect_alpha);
    }
  }
  else if(filled) fillRectangle(buffer, buffer_w, buffer_h, x0, y0, x1, y1, fillColor, fillOpacity, affect_alpha);
}

static void getRoundedRecangleContour(std::vector<int>& xs, std::vector<int>& ys
                                     , int x0, int y0, int x1, int y1
                                     , int radiusx, int radiusy)
{
  int cx0 = x0 + radiusx;
  int cy0 = y0 + radiusy;
  int cx1 = x1 - radiusx;
  int cy1 = y1 - radiusy;

  static const size_t NUM = 16;

  xs.resize(4 + NUM * 4);
  ys.resize(4 + NUM * 4);

  xs[0*(NUM + 1) + 0] = x1;
  ys[0*(NUM + 1) + 0] = cy0;
  xs[0*(NUM + 1) + 1] = x1;
  ys[0*(NUM + 1) + 1] = cy1;
  xs[1*(NUM + 1) + 0] = cx1;
  ys[1*(NUM + 1) + 0] = y1;
  xs[1*(NUM + 1) + 1] = cx0;
  ys[1*(NUM + 1) + 1] = y1;
  xs[2*(NUM + 1) + 0] = x0;
  ys[2*(NUM + 1) + 0] = cy1;
  xs[2*(NUM + 1) + 1] = x0;
  ys[2*(NUM + 1) + 1] = cy0;
  xs[3*(NUM + 1) + 0] = cx0;
  ys[3*(NUM + 1) + 0] = y0;
  xs[3*(NUM + 1) + 1] = cx1;
  ys[3*(NUM + 1) + 1] = y0;

  for(size_t i = 1; i < NUM; i++)
  {
    double rot0 = 0.25 * ((double)i / NUM);
    double angle0 = rot0 * 2 * 3.14159;

    int xa = (int)(radiusx * std::cos(angle0));
    int ya = (int)(radiusy * std::sin(angle0));

      xs[0*(NUM+1) + 1 + i] = cx1 + xa;
      ys[0*(NUM+1) + 1 + i] = cy1 + ya;
      xs[2*(NUM+1) + 1 + i] = cx0 - xa;
      ys[2*(NUM+1) + 1 + i] = cy0 - ya;
      xs[2*(NUM+1) - 0 - i] = cx0 - xa;
      ys[2*(NUM+1) - 0 - i] = cy1 + ya;
      xs[4*(NUM+1) - 0 - i] = cx1 + xa;
      ys[4*(NUM+1) - 0 - i] = cy0 - ya;
  }
}


void fillRoundedRectangle(unsigned char* buffer, int buffer_w, int buffer_h, int x0, int y0, int x1, int y1, int radiusx, int radiusy, const lpi::ColorRGB& color, double opacity, bool affect_alpha)
{
  
  std::vector<int> xs, ys;
  getRoundedRecangleContour(xs, ys, x0, y0, x1, y1, radiusx, radiusy);
  
  //drawPolygonBorderThin(buffer, buffer_w, buffer_h, &xs[0], &ys[0], xs.size(), lpi::RGB_Green, opacity);
  fillPolygon(buffer, buffer_w, buffer_h, &xs[0], &ys[0], xs.size(), color, opacity, affect_alpha);
}

void drawRoundedRectangle(unsigned char* buffer, int buffer_w, int buffer_h
                        , int x0, int y0, int x1, int y1
                        , int radiusx, int radiusy
                        , int thickness
                        , bool outline, const lpi::ColorRGB& lineColor, double lineOpacity
                        , bool filled, const lpi::ColorRGB& fillColor, double fillOpacity
                        , bool affect_alpha)
{
  (void)thickness; (void)outline; (void)lineColor; (void)lineOpacity; (void)filled;
  
  if(x0 > x1) std::swap(x0, x1);
  if(y0 > y1) std::swap(y0, y1);
  
  if(radiusx > (x1 - x0) / 2 - 1) radiusx = (x1 - x0) / 2 - 1;
  if(radiusy > (y1 - y0) / 2 - 1) radiusy = (y1 - y0) / 2 - 1;
  
  if(radiusx <= 0 || radiusy <= 0) return;
  
  std::vector<int> xs, ys;
  getRoundedRecangleContour(xs, ys, x0, y0, x1 - 1, y1 - 1, radiusx, radiusy);
  
  drawPolygon(buffer, buffer_w, buffer_h, &xs[0], &ys[0], xs.size(), thickness, outline, lineColor, lineOpacity, filled, fillColor, fillOpacity, affect_alpha);

  
  /*if(outline)
  {
    int ix0 = x1 > x0 ? x0 + thickness : x0 - thickness;
    int ix1 = x1 > x0 ? x1 - thickness : x1 + thickness;
    int iy0 = y1 > y0 ? y0 + thickness : y0 - thickness;
    int iy1 = y1 > y0 ? y1 - thickness : y1 + thickness;
      
    drawRoundedRectangleOutline(buffer, buffer_w, buffer_h, x0, y0, x1, y1, ix0, iy0, ix1, iy1, radiusx, radiusy, lineColor, lineOpacity);
    if(filled) fillRoundedRectangle(buffer, buffer_w, buffer_h, ix0, iy0, ix1, iy1, radiusx - thickness, radiusy - thickness, fillColor, fillOpacity);
  }
  else if(filled)*/ /*fillRoundedRectangle(buffer, buffer_w, buffer_h, x0, y0, x1, y1, radiusx, radiusy, fillColor, fillOpacity, affect_alpha);*/
}

//thickness exactly 1 (nicer looking border than with the thick border algo in case of thickness 1)
//predondition: x1 >= x0, y1 >= y0
//x1, y1 are NOT included in the ellipse (similar to rectangle and such, there they also are end coordinates outside of the figure)
void drawEllipseBorder(unsigned char* buffer, int buffer_w, int buffer_h, int x0, int y0, int x1, int y1, const lpi::ColorRGB& color, double opacity, bool affect_alpha)
{
  //this is because the end coordinates are not included in the figure
  x1--;
  y1--;
  
  if(x0 > x1 || y0 > y1) return; //prevent infinite loop
  if(x0 == x1)
  {
    verLine(buffer, buffer_w, buffer_h, buffer_w, x0, y0, y1 + 1, color, opacity, affect_alpha);
    return; //prevent infinite loop
  }
  if(y0 == y1)
  {
    horLine(buffer, buffer_w, buffer_h, buffer_w, y0, x0, x1 + 1, color, opacity, affect_alpha);
    return; //prevent infinite loop
  }
  
  bool oddx = (x1-x0)%2==0;
  bool oddy = (y1-y0)%2==0;
  if(oddx) x1++;
  if(oddy) y1++;

  int cx1 = (x0 + x1) / 2;
  int cy1 = (y0 + y1) / 2;
  int cx2 = (x0 + x1) / 2 + !oddx;
  int cy2 = (y0 + y1) / 2 + !oddy;
  int radiusx = (x1 - x0) / 2;
  int radiusy = (y1 - y0) / 2;
  
  int twoASquare = 2 * radiusx * radiusx;
  int twoBSquare = 2 * radiusy * radiusy;
  
  if(twoASquare == 0) twoASquare = 1; //prevent infinite loop
  if(twoBSquare == 0) twoBSquare = 1; //prevent infinite loop
  
  int bx0 = 0, by0 = 0, bx1 = radiusx, by1 = radiusy;
  
  
  int x = radiusx;
  int y = 0;
  int xchange = radiusy * radiusy * (1 - 2 * radiusx);
  int ychange = radiusx * radiusx;
  int ellipseError = 0;
  int stoppingx = twoBSquare * radiusx;
  int stoppingy = 0;

  while(stoppingx >= stoppingy)
  {
    if(x < bx1) bx1 = x;
    if(y > by0) by0 = y;

    pset(buffer, buffer_w, buffer_h, buffer_w, cx2 + x, cy2 + y, color, opacity, affect_alpha);
    pset(buffer, buffer_w, buffer_h, buffer_w, cx1 - x, cy2 + y, color, opacity, affect_alpha);
    if(y > 0 || !oddy)
    {
      pset(buffer, buffer_w, buffer_h, buffer_w, cx1 - x, cy1 - y, color, opacity, affect_alpha);
      pset(buffer, buffer_w, buffer_h, buffer_w, cx2 + x, cy1 - y, color, opacity, affect_alpha);
    }
    
    y++;
    stoppingy += twoASquare;
    ellipseError += ychange;
    ychange += twoASquare;
    if((2 * ellipseError + xchange) > 0)
    {
      x--;
      stoppingx -= twoBSquare;
      ellipseError += xchange;
      xchange += twoBSquare;
    }
  }

  x = 0;
  y = radiusy;
  xchange = radiusy * radiusy;
  ychange = radiusx * radiusx * (1 - 2 * radiusy);
  ellipseError = 0;
  stoppingx = 0;
  stoppingy = twoASquare * radiusy;

  while(stoppingx <= stoppingy)
  {
    if(y < by1) by1 = y;
    if(x > bx0) bx0 = x;
    
    if(x == bx1) break; //avoid a rare situation (e.g. with 8x8 ellipse) where this will overdraw a pixel already drawn by the previous while loop. That is not good for translucent ellipse.
    
    pset(buffer, buffer_w, buffer_h, buffer_w, cx2 + x, cy2 + y, color, opacity, affect_alpha);
    pset(buffer, buffer_w, buffer_h, buffer_w, cx2 + x, cy1 - y, color, opacity, affect_alpha);
    if(x > 0 || !oddx)
    {
      pset(buffer, buffer_w, buffer_h, buffer_w, cx1 - x, cy2 + y, color, opacity, affect_alpha);
      pset(buffer, buffer_w, buffer_h, buffer_w, cx1 - x, cy1 - y, color, opacity, affect_alpha);
    }

    x++;
    stoppingx += twoBSquare;
    ellipseError += xchange;
    xchange += twoBSquare;
    if ((2*ellipseError + ychange) > 0 )
    {
      y--;
      stoppingy -= twoASquare;
      ellipseError += ychange;
      ychange += twoASquare;
    }
  }
  
  //thin ellipses have missing parts at the tips with the code above, fill the missing pixels here
  if(bx1 - bx0 > 1)
  {
    horLine(buffer, buffer_w, buffer_h, buffer_w, cy1, cx2 + bx0 + 1, cx2 + bx1, color, opacity, affect_alpha);
    horLine(buffer, buffer_w, buffer_h, buffer_w, cy1, cx1 - bx1 + 1, cx1 - bx0, color, opacity, affect_alpha);
    if(!oddy)
    {
      horLine(buffer, buffer_w, buffer_h, buffer_w, cy1 + 1, cx2 + bx0 + 1, cx2 + bx1, color, opacity, affect_alpha);
      horLine(buffer, buffer_w, buffer_h, buffer_w, cy1 + 1, cx1 - bx1 + 1, cx1 - bx0, color, opacity, affect_alpha);
    }
  }
  if(by1 - by0 > 1)
  {
    verLine(buffer, buffer_w, buffer_h, buffer_w, cx1, cy2 + by0 + 1, cy2 + by1, color, opacity, affect_alpha);
    verLine(buffer, buffer_w, buffer_h, buffer_w, cx1, cy1 - by1 + 1, cy1 - by0, color, opacity, affect_alpha);
    if(!oddx)
    {
      verLine(buffer, buffer_w, buffer_h, buffer_w, cx1 + 1, cy2 + by0 + 1, cy2 + by1, color, opacity, affect_alpha);
      verLine(buffer, buffer_w, buffer_h, buffer_w, cx1 + 1, cy1 - by1 + 1, cy1 - by0, color, opacity, affect_alpha);
    }
  }
}

void drawEllipse(unsigned char* buffer, int buffer_w, int buffer_h
                , int x0, int y0, int x1, int y1
                , int thickness
                , bool outline, const lpi::ColorRGB& lineColor, double lineOpacity
                , bool filled, const lpi::ColorRGB& fillColor, double fillOpacity
                , bool affect_alpha)
{
  if(x0 > x1) std::swap(x0, x1);
  if(y0 > y1) std::swap(y0, y1);
  
  int sx0 = x0; if(sx0 < 0) sx0 = 0; if(sx0 >= buffer_w) sx0 = buffer_w - 1;
  int sy0 = y0; if(sy0 < 0) sy0 = 0; if(sy0 >= buffer_h) sy0 = buffer_h - 1;
  int sx1 = x1; if(sx1 < 0) sx1 = 0; if(sx1 >= buffer_w) sx1 = buffer_w - 1;
  int sy1 = y1; if(sy1 < 0) sy1 = 0; if(sy1 >= buffer_h) sy1 = buffer_h - 1;

  bool too_thick = thickness > (x1 - x0) / 2 || thickness > (y1 - y0) / 2; //this means the outline is so thick that there's no "fill" anymore, instead it becomes a filled ellipse with the outline color

  if(outline && !filled && thickness == 1)
  {
    drawEllipseBorder(buffer, buffer_w, buffer_h, x0, y0, x1, y1, lineColor, lineOpacity, affect_alpha);
  }
  else if(!too_thick && outline)
  {
    for(int y = sy0; y < sy1; y++)
    for(int x = sx0; x < sx1; x++)
    {
      double dx = ((double)x - (x1 + x0 - 1) / 2.0) / ((x1 - x0 + 1) / 2.0);
      double dy = ((double)y - (y1 + y0 - 1) / 2.0) / ((y1 - y0 + 1) / 2.0);
      if(dx*dx + dy*dy < 1.0) //don't do <=, it's uglier (1-pixel side-points)
      {
        double dx2 = ((double)x - (x1 + x0 - 1) / 2.0) / ((x1 - x0 + 1 - thickness * 2) / 2.0);
        double dy2 = ((double)y - (y1 + y0 - 1) / 2.0) / ((y1 - y0 + 1 - thickness * 2) / 2.0);
        bool side = dx2*dx2 + dy2*dy2 >= 1.0; //the point belongs to the side
        if(side)
        {
          pset(buffer, buffer_w, buffer_h, buffer_w, x, y, lineColor, lineOpacity, affect_alpha);
        }
        else
        {
          if(filled) pset(buffer, buffer_w, buffer_h, buffer_w, x, y, fillColor, fillOpacity, affect_alpha);
        }
      }
    }
  }
  else
  {
    lpi::ColorRGB color = too_thick ? lineColor : fillColor;
    double opacity = too_thick ? lineOpacity : fillOpacity;
    for(int y = sy0; y < sy1; y++)
    for(int x = sx0; x < sx1; x++)
    {
      double dx = ((double)x - (x1 + x0 - 1) / 2.0) / ((x1 - x0 + 1) / 2.0);
      double dy = ((double)y - (y1 + y0 - 1) / 2.0) / ((y1 - y0 + 1) / 2.0);
      if(dx*dx + dy*dy < 1.0) //don't do <=, it's uglier (1-pixel side-points)
      {
        pset(buffer, buffer_w, buffer_h, buffer_w, x, y, color, opacity, affect_alpha);
      }
    }
  }
}
