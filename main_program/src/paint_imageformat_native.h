/*
LodePaint

Copyright (c) 2009-2010 Lode Vandevenne
All rights reserved.

This file is part of LodePaint.

LodePaint is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LodePaint is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LodePaint.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once 

#include "paint_imageformats.h"

#if defined(LPI_OS_WINDOWS)
#include "windows.h"
#endif


class ImageFormatNative : public AImageFormat
{
  private:
    virtual bool isFileOfFormat(const unsigned char* file, size_t filesize) const;
  public:
    virtual bool canDecode(const unsigned char* file, size_t filesize) const;
    virtual bool canEncode(const Paint& paint) const;

    //these are to check that you can decode or encode in general
    virtual bool canDecode() const;
    virtual bool canEncode() const;

    virtual bool decode(std::string& error, Paint& paint, const unsigned char* file, size_t filesize) const;
    virtual bool encode(std::string& error, std::vector<unsigned char>& file, const Paint& paint) const;

    virtual size_t getNumExtensions() const;
    virtual std::string getExtension(size_t i) const;
    virtual std::string getDescription() const;
};
