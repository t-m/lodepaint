/*
LodePaint

Copyright (c) 2009-2010 Lode Vandevenne
All rights reserved.

This file is part of LodePaint.

LodePaint is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LodePaint is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LodePaint.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "paint_filter_plugin.h"

#if defined(LPI_OS_LINUX) || defined(LPI_OS_AMIGA)

#include <dlfcn.h>

namespace
{
  template<typename F>
  void do_dlsym(F& functionPointer, void* dl, const char* name) //to avoid warning "dereferencing type-punned pointer will break strict-aliasing rulesdereferencing type-punned pointer will break strict-aliasing rules" when using dlsym
  {
    //*(void **)(&functionPointer) = dlsym(dl, name); //this gives the warning
    
    union { F func; void* obj; } alias; //Wikipedia's way to avoid the warning!
#if defined(LPI_OS_AMIGA)
    alias.obj = dlsym(dl, (char*)name); //Amiga's dlsym takes char* instead of const char*
#else
    alias.obj = dlsym(dl, name);
#endif
    functionPointer = alias.func;

  }
}

FilterPlugin::FilterPlugin(const std::string& dynamicLib, GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom)
: AOperationFilterUtil(settings, true, true)
, isvalid(true)
, dialog(0)
, p_pluginFreeUnsignedChar(0)
, p_pluginFreeFloat(0)
, p_filterGetName(0)
, p_filterGetSubMenu(0)
, p_filterExecuteRGBA8(0)
, p_filterExecuteRGBA32F(0)
, p_filterExecuteGrey8(0)
, p_filterExecuteGrey32F(0)
, p_filterGetParametersXMLSize(0)
, p_filterGetParametersXML(0)
{
  dl = dlopen(dynamicLib.c_str(), RTLD_LOCAL | RTLD_LAZY);
  if(dl)
  {
    int (*p_getLodePaintPluginType)();
    do_dlsym(p_getLodePaintPluginType, dl, "getLodePaintPluginType");
    if(p_getLodePaintPluginType && p_getLodePaintPluginType() == 1)
    {
      //filter plugin identifier found!
    }
    else
    {
      isvalid = false;
      dlclose(dl);
      dl = 0;
    }
  }
  
  if(dl)
  {
    std::cout<<"Loading Filter Plugin: " << dynamicLib << std::endl;
    
    do_dlsym(p_filterGetName, dl, "filterGetName");
    if(p_filterGetName)
    {
      char name[80];
      p_filterGetName(name, 80, 0);
      label = name;
    }
    else
    {
      std::cout << "filterGetName not found "<< dlerror()<<std::endl;
      isvalid = false;
    }
    
    do_dlsym(p_filterGetSubMenu, dl, "filterGetSubMenu");
    if(p_filterGetSubMenu)
    {
      char name[80];
      p_filterGetSubMenu(name, 80, 0);
      submenu = name;
    }

    do_dlsym(p_pluginFreeUnsignedChar, dl, "pluginFreeUnsignedChar");
    do_dlsym(p_pluginFreeFloat, dl, "pluginFreeFloat");
    
    do_dlsym(p_filterExecuteRGBA8, dl, "filterExecuteRGBA8");
    do_dlsym(p_filterExecuteRGBA32F, dl, "filterExecuteRGBA32F");
    do_dlsym(p_filterExecuteGrey8, dl, "filterExecuteGrey8");
    do_dlsym(p_filterExecuteGrey32F, dl, "filterExecuteGrey32F");

    
    if((p_filterExecuteGrey8 && !p_pluginFreeUnsignedChar) || (p_filterExecuteGrey32F && !p_pluginFreeFloat))
    {
      std::cout << "filter doesn't include pluginFreeUnsignedChar or pluginFreeFloat function"<<std::endl;
      isvalid = false;
    }

    do_dlsym(p_filterGetParametersXMLSize, dl, "filterGetParametersXMLSize");
    do_dlsym(p_filterGetParametersXML, dl, "filterGetParametersXML");

    bool hasUChar = p_filterExecuteRGBA8 != 0 || p_filterExecuteGrey8 != 0;
    bool hasFloat = p_filterExecuteRGBA32F != 0 || p_filterExecuteGrey32F != 0;
    
    if(!hasUChar && !hasFloat)
    {
      std::cout << "no filterExecuteRGBA8, filterExecuteRGBA32F, filterExecuteGrey8 or filterExecuteGrey32F function in filter plugin" << std::endl;
      isvalid = false;
    }
    
    initGUI(geom);

  }
  else isvalid = false;
  
  if(!isvalid && dl != 0)
  {
    dlclose(dl);
    dl = 0;
  }
}

FilterPlugin::~FilterPlugin()
{
  if(dl) dlclose(dl);
  if(dialog) delete dialog;
}

#elif defined(LPI_OS_WINDOWS)



FilterPlugin::FilterPlugin(const std::string& dynamicLib, GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom)
: AOperationFilterUtil(settings, true, true)
, dl(0)
, isvalid(true)
, dialog(0)
, p_pluginFreeUnsignedChar(0)
, p_pluginFreeFloat(0)
, p_filterGetName(0)
, p_filterGetSubMenu(0)
, p_filterExecuteRGBA8(0)
, p_filterExecuteRGBA32F(0)
, p_filterExecuteGrey8(0)
, p_filterExecuteGrey32F(0)
, p_filterGetParametersXMLSize(0)
, p_filterGetParametersXML(0)
{
  bool isDLLFileName = lpi::extEqualsIgnoreCase(dynamicLib, "dll");
  //LOAD_WITH_ALTERED_SEARCH_PATH ensures it looks in the folder where we load the dll from, for possible other dll dependencies of that dll (instead of searching in the main lodepaint dir)
  if(isDLLFileName) dl = LoadLibraryEx(dynamicLib.c_str(), 0, LOAD_WITH_ALTERED_SEARCH_PATH);
  else dl = 0; //avoid MS Windows dialog boxes for files in the plugin folder that aren't a DLL
  
  
  if(dl)
  {
    typedef int (*T_getLodePaintPluginType)();
    T_getLodePaintPluginType p_getLodePaintPluginType;
    p_getLodePaintPluginType = (T_getLodePaintPluginType)GetProcAddress(dl, "getLodePaintPluginType");

    if(p_getLodePaintPluginType && p_getLodePaintPluginType() == 1)
    {
      //filter plugin identifier found!
    }
    else
    {
      isvalid = false;
      FreeLibrary(dl);
      dl = 0;
    }
  }

  if(dl)
  {
    std::cout<<"Loading Filter Plugin: " << dynamicLib << std::endl;

    p_filterGetName = (T_filterGetName)GetProcAddress(dl, "filterGetName");
    if(p_filterGetName)
    {
      char name[80];
      p_filterGetName(name, 80, 0);
      label = name;
    }
    else
    {
      std::cout << "filterGetName not found"<<std::endl;
      isvalid = false;
    }
    
    p_filterGetSubMenu = (T_filterGetSubMenu)GetProcAddress(dl, "filterGetSubMenu");
    if(p_filterGetSubMenu)
    {
      char name[80];
      p_filterGetSubMenu(name, 80, 0);
      submenu = name;
    }
    
    p_pluginFreeUnsignedChar = (T_pluginFreeUnsignedChar)GetProcAddress(dl, "pluginFreeUnsignedChar");
    p_pluginFreeFloat = (T_pluginFreeFloat)GetProcAddress(dl, "pluginFreeFloat");
    
    p_filterExecuteRGBA8 = (T_filterExecuteRGBA8)GetProcAddress(dl, "filterExecuteRGBA8");
    p_filterExecuteRGBA32F = (T_filterExecuteRGBA32F)GetProcAddress(dl, "filterExecuteRGBA32F");
    p_filterExecuteGrey8 = (T_filterExecuteGrey8)GetProcAddress(dl, "filterExecuteGrey8");
    p_filterExecuteGrey32F = (T_filterExecuteGrey32F)GetProcAddress(dl, "filterExecuteGrey32F");
    
    if(((p_filterExecuteRGBA8 || p_filterExecuteGrey8) && !p_pluginFreeUnsignedChar) || ((p_filterExecuteRGBA32F || p_filterExecuteGrey32F) && !p_pluginFreeFloat))
    {
      std::cout << "filter doesn't include pluginFreeUnsignedChar or pluginFreeFloat function"<<std::endl;
      isvalid = false;
    }

    p_filterGetParametersXMLSize = (T_filterGetParametersXMLSize)GetProcAddress(dl, "filterGetParametersXMLSize");
    p_filterGetParametersXML = (T_filterGetParametersXML)GetProcAddress(dl, "filterGetParametersXML");

    bool hasUChar = p_filterExecuteRGBA8 != 0 || p_filterExecuteGrey8 != 0;
    bool hasFloat = p_filterExecuteRGBA32F != 0 || p_filterExecuteGrey32F != 0;
    
    if(!hasUChar && !hasFloat)
    {
      std::cout << "no filterExecuteRGBA8, filterExecuteRGBA32F, filterExecuteGrey8 or filterExecuteGrey32F function in filter plugin" << std::endl;
      isvalid = false;
    }

    initGUI(geom);

  }
  else isvalid = false;

  if(!isvalid && dl != 0)
  {
    FreeLibrary(dl);
    dl = 0;
  }
}

FilterPlugin::~FilterPlugin()
{
  if(dl) FreeLibrary(dl);
}

#elif defined(LPI_OS_AROS)
FilterPlugin::FilterPlugin(const std::string& dynamicLib, GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom)
: AOperationFilterUtil(settings, true, true)
, isvalid(true)
, dialog(0)
, p_pluginFreeUnsignedChar(0)
, p_pluginFreeFloat(0)
, p_filterGetName(0)
, p_filterGetSubMenu(0)
, p_filterExecuteRGBA8(0)
, p_filterExecuteRGBA32F(0)
, p_filterExecuteGrey8(0)
, p_filterExecuteGrey32F(0)
, p_filterGetParametersXMLSize(0)
, p_filterGetParametersXML(0)
{
}

FilterPlugin::~FilterPlugin()
{
}
#endif

void FilterPlugin::initGUI(const lpi::gui::IGUIDrawer& geom)
{
  hasgui = false;

  dialog = new DynamicFilterPageWithPreview(this);
  
  DynamicPageWithAutoUpdate* page = p_filterExecuteRGBA8 ? &(dynamic_cast<DynamicFilterPageWithPreview*>(dialog))->page : dynamic_cast<DynamicPageWithAutoUpdate*>(dialog);

  if(p_filterGetParametersXMLSize && p_filterGetParametersXML)
  {
    hasgui = true;
    params.setCleanMemory(true);
    
    
    char* c = new char[p_filterGetParametersXMLSize(0) + 1];
    p_filterGetParametersXML(c, 0);
    std::string xml = c;
    delete[] c;
    
    xmlToDynamicParameters(params, xml);
    
    paramsToDialog(*page, params, geom);
  }
}


void FilterPlugin::init(PaintWindow* pw)
{
  AOperationFilterUtil::init(pw);
  //origwidth = pw->getPaint().getU();
  //origheight = pw->getPaint().getV();
  
  
  for(size_t i = 0; i < params.size(); i++)
  {
    Param& param = *params.get(i);
    if(param.getType() == PG_ABS_REL_SIZE)
    {
      ParamResize& p = dynamic_cast<ParamResize&>(param);
      *p.origwidth = pw->getPaint().getU();
      *p.origheight = pw->getPaint().getV();
      break;
    }
  }  
}

template<typename T>
T* data(std::vector<T>& v)
{
  return v.empty() ? 0 : &v[0];
}


bool FilterPlugin::supportsColorMode(ColorMode mode) const
{
  return sup(mode,
             p_filterExecuteRGBA8 != 0,
             p_filterExecuteRGBA32F != 0,
             p_filterExecuteGrey8 != 0,
             p_filterExecuteGrey32F != 0);
}

bool FilterPlugin::operate(Paint& paint, UndoState& state, Progress& progress)
{
  //not thread safe
  (void)paint;
  (void)state;
  (void)progress;
  return true;
}

bool FilterPlugin::operateGL(Paint& paint, UndoState& state)
{
  bool result = AOperationFilter::operate(paint, state, dummyProgress);
  paint.update();
  return result;
}

void FilterPlugin::doit(TextureRGBA8& texture, Progress&)
{
  if(p_filterExecuteRGBA8)
  {
    CParams mainparams;
    initCParams(mainparams);
    double fgbg[8] = { settings.leftColor.r, settings.leftColor.g, settings.leftColor.b, settings.leftColor.a
                     , settings.rightColor.r, settings.rightColor.g, settings.rightColor.b, settings.rightColor.a };
    mainparams.colors = fgbg;
    
    CParams cparams;
    dynamicParametersToCParams(cparams, params);
    
    unsigned char* buffer;
    int u, v;
    p_filterExecuteRGBA8(&buffer, &u, &v, texture.getBuffer(), texture.getU(), texture.getV(), texture.getU2(), &cparams, &mainparams, 0);

    if(buffer != texture.getBuffer())
    {
      texture.setSize(u, v);
      size_t u2 = texture.getU2();
      unsigned char* buffer2 = texture.getBuffer();
      for(int y = 0; y < v; y++)
      for(int x = 0; x < u; x++)
      for(int c = 0; c < 4; c++)
      {
        buffer2[4 * u2 * y + 4 * x + c] = buffer[4 * u * y + 4 * x + c];
      }
      p_pluginFreeUnsignedChar(buffer);
    }
    
    cleanupCParams(cparams);
  }

  texture.update(); //todo: check if this is maybe not needed anymore
}

void FilterPlugin::doit(TextureRGBA32F& texture, Progress&)
{
  if(p_filterExecuteRGBA32F)
  {
    CParams mainparams;
    initCParams(mainparams);
    double fgbg[8] = { settings.leftColor.r, settings.leftColor.g, settings.leftColor.b, settings.leftColor.a
                     , settings.rightColor.r, settings.rightColor.g, settings.rightColor.b, settings.rightColor.a };
    mainparams.colors = fgbg;
    
    CParams cparams;
    dynamicParametersToCParams(cparams, params);
    
    float* buffer;
    int u, v;
    p_filterExecuteRGBA32F(&buffer, &u, &v, texture.getBuffer(), texture.getU(), texture.getV(), texture.getU2(), &cparams, &mainparams, 0);
    
    if(buffer != texture.getBuffer())
    {
      texture.setSize(u, v);
      size_t u2 = texture.getU2();
      float* buffer2 = texture.getBuffer();
      for(int y = 0; y < v; y++)
      for(int x = 0; x < u; x++)
      for(int c = 0; c < 4; c++)
      {
        buffer2[4 * u2 * y + 4 * x + c] = buffer[4 * u * y + 4 * x + c];
      }
      p_pluginFreeFloat(buffer);
    }
    
    cleanupCParams(cparams);
  }
  
  texture.update(); //todo: check if this is maybe not needed anymore
}

void FilterPlugin::doit(TextureGrey8& texture, Progress&)
{
  if(p_filterExecuteGrey8)
  {
    CParams mainparams;
    initCParams(mainparams);
    double fgbg[8] = { settings.leftColor.r, settings.leftColor.g, settings.leftColor.b, settings.leftColor.a
                     , settings.rightColor.r, settings.rightColor.g, settings.rightColor.b, settings.rightColor.a };
    mainparams.colors = fgbg;
    
    CParams cparams;
    dynamicParametersToCParams(cparams, params);

    unsigned char* buffer;
    int u, v;
    p_filterExecuteGrey8(&buffer, &u, &v, texture.getBuffer(), texture.getU(), texture.getV(), texture.getU2(), &cparams, &mainparams, 0);
    
    if(buffer != texture.getBuffer())
    {
      texture.setSize(u, v);
      size_t u2 = texture.getU2();
      unsigned char* buffer2 = texture.getBuffer();
      for(int y = 0; y < v; y++)
      for(int x = 0; x < u; x++)
      {
        buffer2[u2 * y + x] = buffer[u * y + x];
      }
      p_pluginFreeUnsignedChar(buffer);
    }
    
    cleanupCParams(cparams);
  }

  texture.update(); //todo: check if this is maybe not needed anymore
}

void FilterPlugin::doit(TextureGrey32F& texture, Progress&)
{
  if(p_filterExecuteGrey32F)
  {
    CParams mainparams;
    initCParams(mainparams);
    double fgbg[8] = { settings.leftColor.r, settings.leftColor.g, settings.leftColor.b, settings.leftColor.a
                     , settings.rightColor.r, settings.rightColor.g, settings.rightColor.b, settings.rightColor.a };
    mainparams.colors = fgbg;
    
    CParams cparams;
    dynamicParametersToCParams(cparams, params);
    
    float* buffer;
    int u, v;
    p_filterExecuteGrey32F(&buffer, &u, &v, texture.getBuffer(), texture.getU(), texture.getV(), texture.getU2(), &cparams, &mainparams, 0);
    if(buffer != texture.getBuffer())
    {
      texture.setSize(u, v);
      size_t u2 = texture.getU2();
      float* buffer2 = texture.getBuffer();
      for(int y = 0; y < v; y++)
      for(int x = 0; x < u; x++)
      {
        buffer2[u2 * y + x] = buffer[u * y + x];
      }
      p_pluginFreeFloat(buffer);
    }
    
    cleanupCParams(cparams);
  }
  
  texture.update(); //todo: check if this is maybe not needed anymore
}

lpi::gui::Element* FilterPlugin::getDialog()
{
  return hasgui ? dialog : 0;
}

std::string FilterPlugin::getLabel() const
{
  return label;
}

std::string FilterPlugin::getSubMenu() const
{
  return submenu;
}


bool FilterPlugin::isValidFilterPlugin() const
{
  return isvalid;
}
