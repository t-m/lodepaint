/*
LodePaint

Copyright (c) 2009 Lode Vandevenne
All rights reserved.

This file is part of LodePaint.

LodePaint is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LodePaint is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LodePaint.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "paint_algos_dsp.h"

#include "lpi/lpi_math.h"

#include "paint_window.h"

#include "imalg/sing.h"
#include "imalg/dsp.h"

#include <algorithm>
#include <cmath>
#include <vector>


//n is total size divided through 4 (so size of 1 channel, and there are 4 channels in total)
template<typename T>
void interleavedToPlanar(T* d, size_t n)
{
  std::vector<T> r(n * 4);
  size_t i0 = 0;
  size_t i1 = n;
  size_t i2 = n * 2;
  size_t i3 = n * 3;
  for(size_t i = 0; i < n; i++)
  {
    r[i0 + i] = d[i * 4 + 0];
    r[i1 + i] = d[i * 4 + 1];
    r[i2 + i] = d[i * 4 + 2];
    r[i3 + i] = d[i * 4 + 3];
  }
  
  for(size_t i = 0; i < r.size(); i++) d[i] = r[i];
}

//n is total size divided through 4 (so size of 1 channel, and there are 4 channels in total)
template<typename T>
void planarToInterleaved(T* d, size_t n)
{
  std::vector<T> r(n * 4);
  size_t i0 = 0;
  size_t i1 = n;
  size_t i2 = n * 2;
  size_t i3 = n * 3;
  for(size_t i = 0; i < n; i++)
  {
    r[i * 4 + 0] = d[i0 + i];
    r[i * 4 + 1] = d[i1 + i];
    r[i * 4 + 2] = d[i2 + i];
    r[i * 4 + 3] = d[i3 + i];
  }

  for(size_t i = 0; i < r.size(); i++) d[i] = r[i];
}

//d has n * m values in it. n*m is size of input, output is m*n
template<typename T>
void transpose(T* d, size_t n, size_t m)
{
  //TODO: use faster method that doesn't use intermediate buffer
  std::vector<T> r(n * m);

  for(size_t y = 0; y < m; y++)
  for(size_t x = 0; x < n; x++)
  {
    r[x * m + y] = d[y * n + x];
  }

  for(size_t i = 0; i < r.size(); i++) d[i] = r[i];
}

template<typename T>
void multiply(T* d, T factor, size_t n)
{
  for(size_t i = 0; i < n; i++) d[i] *= factor;
}

template<typename T>
void FFT2D_template(T* ore, T* oim, const T* ire, const T* iim, int n, int m, bool inverse, bool affect_alpha)
{
  //just for test//if(lpi::isPowerOfTwo(n) && lpi::isPowerOfTwo(m)) { FFT2D_POT(ore, oim, ire, iim, n, m, inverse); return; }
  
  int inverse_int = inverse ? 1 : -1;
  
  for(int i = 0; i < n * m * 4; i++)
  {
    ore[i] = ire[i];
    oim[i] = iim[i];
  }
  
  interleavedToPlanar(ore, n * m);
  interleavedToPlanar(oim, n * m);
  
  T* ore0 = &ore[n * m * 0];
  T* oim0 = &oim[n * m * 0];
  T* ore1 = &ore[n * m * 1];
  T* oim1 = &oim[n * m * 1];
  T* ore2 = &ore[n * m * 2];
  T* oim2 = &oim[n * m * 2];
  T* ore3 = &ore[n * m * 3];
  T* oim3 = &oim[n * m * 3];

  for(int i = 0; i < m; i++)
  {
    sing(&ore0[i*n], &oim0[i*n], n, n, n, inverse_int);
    sing(&ore1[i*n], &oim1[i*n], n, n, n, inverse_int);
    sing(&ore2[i*n], &oim2[i*n], n, n, n, inverse_int);
    if(affect_alpha) sing(&ore3[i*n], &oim3[i*n], n, n, n, inverse_int);
  }

  transpose(ore0, n, m);
  transpose(oim0, n, m);
  transpose(ore1, n, m);
  transpose(oim1, n, m);
  transpose(ore2, n, m);
  transpose(oim2, n, m);
  if(affect_alpha) transpose(ore3, n, m);
  if(affect_alpha) transpose(oim3, n, m);

  for(int i = 0; i < n; i++)
  {
    sing(&ore0[i*m], &oim0[i*m], m, m, m, inverse_int);
    sing(&ore1[i*m], &oim1[i*m], m, m, m, inverse_int);
    sing(&ore2[i*m], &oim2[i*m], m, m, m, inverse_int);
    if(affect_alpha) sing(&ore3[i*m], &oim3[i*m], m, m, m, inverse_int);
  }

  transpose(ore0, m, n);
  transpose(oim0, m, n);
  transpose(ore1, m, n);
  transpose(oim1, m, n);
  transpose(ore2, m, n);
  transpose(oim2, m, n);
  if(affect_alpha) transpose(ore3, m, n);
  if(affect_alpha) transpose(oim3, m, n);
  
  /*T d = inverse ? 1.0 / n : 1.0 / m;
  //alpha channel not included
  multiply(ore, d, n * m * 3);
  multiply(oim, d, n * m * 3);*/

  planarToInterleaved(ore, n * m);
  planarToInterleaved(oim, n * m);
}

void FFT2D(float* ore, float* oim, const float* ire, const float* iim, int n, int m, bool inverse, bool affect_alpha)
{
  FFT2D_template(ore, oim, ire, iim, n, m, inverse, affect_alpha);
}

void FFT2D(double* ore, double* oim, const double* ire, const double* iim, int n, int m, bool inverse, bool affect_alpha)
{
  FFT2D_template(ore, oim, ire, iim, n, m, inverse, affect_alpha);
}

template<typename T>
void FFT2D_greyscale_template(T* ore, T* oim, const T* ire, const T* iim, int n, int m, bool inverse)
{
  //just for test//if(lpi::isPowerOfTwo(n) && lpi::isPowerOfTwo(m)) { FFT2D_POT(ore, oim, ire, iim, n, m, inverse); return; }

  int inverse_int = inverse ? 1 : -1;

  for(int i = 0; i < n * m; i++)
  {
    ore[i] = ire[i];
    oim[i] = iim[i];
  }

  for(int i = 0; i < m; i++)
  {
    sing(&ore[i*n], &oim[i*n], n, n, n, inverse_int);
  }

  transpose(ore, n, m);
  transpose(oim, n, m);

  for(int i = 0; i < n; i++)
  {
    sing(&ore[i*m], &oim[i*m], m, m, m, inverse_int);
  }

  transpose(ore, m, n);
  transpose(oim, m, n);
}

void FFT2D_greyscale(double* ore, double* oim, const double* ire, const double* iim, int n, int m, bool inverse)
{
  FFT2D_greyscale_template(ore, oim, ire, iim, n, m, inverse);
}

void FFT2D_greyscale(float* ore, float* oim, const float* ire, const float* iim, int n, int m, bool inverse)
{
  FFT2D_greyscale_template(ore, oim, ire, iim, n, m, inverse);
}

/*
  The 2D patters below are too hard to keep half the values from, so we
  do it twice in 1D instead. (see ReImToRe1D function)

  EVEN:

  re:
  DC   a   b   c   X   c   b   a
   d   1   2   3   g   4   5   6
   e   7   8   9   h  10  11  12
   f  13  14  15   i  16  17  18
   Y   j   k   l  [Z]  l   k   j
   f  18  17  16   i  15  14  13
   e  12  11  10   h   9   8   7
   d   6   5   4   g   3   2   1

  im
   0   a   b   c   0  -c  -b  -a
   d   1   2   3   g   4   5   6
   e   7   8   9   h  10  11  12
   f  13  14  15   i  16  17  18
   0   j   k   l  [0] -l  -k  -j
  -f -18 -17 -16  -i -15 -14 -13
  -e -12 -11 -10  -h  -9  -8  -7
  -d  -6  -5  -4  -g  -3  -2  -1


  ODD:
  re:
  DC   a   b   c   c   b   a
   d   1   2   3   4   5   6
   e   7   8   9  10  11  12
   f  13  14  15  16  17  18
   f  18  17  16  15  14  13
   e  12  11  10   9   8   7
   d   6   5   4   3   2   1

  im
   0   a   b   c  -c  -b  -a
   d   1   2   3   4   5   6
   e   7   8   9  10  11  12
   f  13  14  15  16  17  18
  -f -18 -17 -16 -15 -14 -13
  -e -12 -11 -10  -9  -8  -7
  -d  -6  -5  -4  -3  -2  -1
  
  In the 2D case, also only half the values need to be remembered, but the shape
  is too irregular. In the below pictures, X must be remembered, r can be derived
  from the X's and 0 is always 0 so doesn't need to be remembered:

  re:

  x X X X X r r r
  X X X X X X X X
  X X X X X X X X
  X X X X X X X X
  X X X X X r r r
  r r r r r r r r
  r r r r r r r r
  r r r r r r r r

  im:

  0 X X X 0 r r r
  X X X X X X X X
  X X X X X X X X
  X X X X X X X X
  0 X X X 0 r r r
  r r r r r r r r
  r r r r r r r r
  r r r r r r r r

*/

template<typename T>
void ReImToRe1D(T* re, const T* im, size_t n)
{
  //EVEN:
  //re is like so: DC  a   b   c   X   c   b   a
  //im is like so:  0  d   e   f   0  -f  -e  -d
  //--------------------------------------------
  //out is set to: DC  a   b   c   X  -f  -e  -d
  //So of the real part, n / 2 + 1 values must be kept, from the im part n / 2 - 1 values (not the 0's)

  //ODD:
  //re is like so: DC  a   b   c   c   b   a
  //im is like so:  0  d   e   f  -f  -e  -d
  //----------------------------------------
  //out is set to: DC  a   b   c  -f  -e  -d
  //So of the real part, n / 2 + 1 values must be kept, from the im part n / 2 values (not the 0's)

  for(size_t i = n / 2 + 1; i < n; i++) re[i] = im[i];
  
  //Version that averages each two corresponding numbers in case they differ slightly to attempt slightly better quality
  /*
  for(size_t i = 1; i < n / 2 + 1; i++) re[i] = (re[i] + re[n - i]) / 2;
  for(size_t i = n / 2 + 1; i < n; i++) re[i] = (im[i] - im[n - i]) / 2;
  */
  
}

template<typename T>
void ReToReIm1D(T* re, T* im, size_t n)
{
  //EVEN:
  //re is like so: DC  a   b   c   X   c   b   a
  //im is like so:  0  d   e   f   0  -f  -e  -d
  //--------------------------------------------
  //the input was: DC  a   b   c   X  -f  -e  -d
  //So of the real part, n / 2 + 1 values must be kept, from the im part n / 2 - 1 values (not the 0's)

  //ODD:
  //re is like so: DC  a   b   c   c   b   a
  //im is like so:  0  d   e   f  -f  -e  -d
  //----------------------------------------
  //the input was: DC  a   b   c  -f  -e  -d
  //So of the real part, n / 2 + 1 values must be kept, from the im part n / 2 values (not the 0's)

  for(size_t i = n / 2 + 1; i < n; i++)
  {
    im[i] = re[i];
    im[n - i] = -re[i];
    re[i] = re[n - i];
  }
  
  //Actually, these 0 values don't even matter, they only affect imaginary numbers in the result, but for correctness let's set them.
  im[0] = 0;
  if(n % 2 == 0) im[n / 2] = 0;
}

template<typename T>
void FFT2D_real_template(T* iore, int n, int m, bool inverse, bool affect_alpha)
{
  int inverse_int = inverse ? 1 : -1;

  interleavedToPlanar(iore, n * m);

  T* ore0 = &iore[n * m * 0];
  T* ore1 = &iore[n * m * 1];
  T* ore2 = &iore[n * m * 2];
  T* ore3 = &iore[n * m * 3];
  
  if(inverse)
  {
    std::vector<T> oim;

    oim.resize(n);
    for(int i = 0; i < m; i++)
    {
      ReToReIm1D(&ore0[i*n], &oim[0], n);
      sing(&ore0[i*n], &oim[0], n, n, n, inverse_int);

      ReToReIm1D(&ore1[i*n], &oim[0], n);
      sing(&ore1[i*n], &oim[0], n, n, n, inverse_int);

      ReToReIm1D(&ore2[i*n], &oim[0], n);
      sing(&ore2[i*n], &oim[0], n, n, n, inverse_int);

      if(affect_alpha)
      {
        ReToReIm1D(&ore3[i*n], &oim[0], n);
        sing(&ore3[i*n], &oim[0], n, n, n, inverse_int);
      }
    }

    transpose(ore0, n, m);
    transpose(ore1, n, m);
    transpose(ore2, n, m);
    if(affect_alpha) transpose(ore3, n, m);

    oim.resize(m);
    for(int i = 0; i < n; i++)
    {
      ReToReIm1D(&ore0[i*m], &oim[0], m);
      sing(&ore0[i*m], &oim[0], m, m, m, inverse_int);

      ReToReIm1D(&ore1[i*m], &oim[0], m);
      sing(&ore1[i*m], &oim[0], m, m, m, inverse_int);

      ReToReIm1D(&ore2[i*m], &oim[0], m);
      sing(&ore2[i*m], &oim[0], m, m, m, inverse_int);

      if(affect_alpha)
      {
        ReToReIm1D(&ore3[i*m], &oim[0], m);
        sing(&ore3[i*m], &oim[0], m, m, m, inverse_int);
      }
    }

    oim.clear();
  }
  else
  {
    std::vector<T> oim;
    for(int i = 0; i < m; i++)
    {
      oim.clear(); oim.resize(n, 0);
      sing(&ore0[i*n], &oim[0], n, n, n, inverse_int);
      ReImToRe1D(&ore0[i*n], &oim[0], n);
      
      oim.clear(); oim.resize(n, 0);
      sing(&ore1[i*n], &oim[0], n, n, n, inverse_int);
      ReImToRe1D(&ore1[i*n], &oim[0], n);

      oim.clear(); oim.resize(n, 0);
      sing(&ore2[i*n], &oim[0], n, n, n, inverse_int);
      ReImToRe1D(&ore2[i*n], &oim[0], n);

      if(affect_alpha)
      {
        oim.clear(); oim.resize(n, 0);
        sing(&ore3[i*n], &oim[0], n, n, n, inverse_int);
        ReImToRe1D(&ore3[i*n], &oim[0], n);
      }
    }

    transpose(ore0, n, m);
    transpose(ore1, n, m);
    transpose(ore2, n, m);
    if(affect_alpha) transpose(ore3, n, m);

    for(int i = 0; i < n; i++)
    {
      oim.clear(); oim.resize(m, 0);
      sing(&ore0[i*m], &oim[0], m, m, m, inverse_int);
      ReImToRe1D(&ore0[i*m], &oim[0], m);

      oim.clear(); oim.resize(m, 0);
      sing(&ore1[i*m], &oim[0], m, m, m, inverse_int);
      ReImToRe1D(&ore1[i*m], &oim[0], m);

      oim.clear(); oim.resize(m, 0);
      sing(&ore2[i*m], &oim[0], m, m, m, inverse_int);
      ReImToRe1D(&ore2[i*m], &oim[0], m);

      if(affect_alpha)
      {
        oim.clear(); oim.resize(m, 0);
        sing(&ore3[i*m], &oim[0], m, m, m, inverse_int);
        ReImToRe1D(&ore3[i*m], &oim[0], m);
      }
    }
  }

  transpose(ore0, m, n);
  transpose(ore1, m, n);
  transpose(ore2, m, n);
  if(affect_alpha) transpose(ore3, m, n);

  planarToInterleaved(iore, n * m);
}

void FFT2D_real(float* iore, int n, int m, bool inverse, bool affect_alpha)
{
  FFT2D_real_template(iore, n, m, inverse, affect_alpha);
}

void FFT2D_real(double* iore, int n, int m, bool inverse, bool affect_alpha)
{
  FFT2D_real_template(iore, n, m, inverse, affect_alpha);
}

template<typename T>
void multiplyForFFT_template(T* iore, T* ioim, int n, int m, T factor, bool affect_alpha)
{
  int nm = n * m;
  for(int i = 0; i < nm; i++)
  {
    int index = i * 4;
    iore[index + 0] *= factor;
    ioim[index + 0] *= factor;
    iore[index + 1] *= factor;
    ioim[index + 1] *= factor;
    iore[index + 2] *= factor;
    ioim[index + 2] *= factor;
    if(affect_alpha) iore[index + 3] *= factor;
    if(affect_alpha) ioim[index + 3] *= factor;
  }
}

void multiplyForFFT(double* iore, double* ioim, int n, int m, double factor, bool affect_alpha)
{
  multiplyForFFT_template(iore, ioim, n, m, factor, affect_alpha);
}

void multiplyForFFT(float* iore, float* ioim, int n, int m, float factor, bool affect_alpha)
{
  multiplyForFFT_template(iore, ioim, n, m, factor, affect_alpha);
}

template<typename T>
void multiplyForFFT_template(T* io, int n, int m, T factor, bool affect_alpha)
{
  int nm = n * m;
  for(int i = 0; i < nm; i++)
  {
    int index = i * 4;
    io[index + 0] *= factor;
    io[index + 1] *= factor;
    io[index + 2] *= factor;
    if(affect_alpha) io[index + 3] *= factor;
  }
}

void multiplyForFFT(double* io, int n, int m, double factor, bool affect_alpha)
{
  multiplyForFFT_template(io, n, m, factor, affect_alpha);
}

void multiplyForFFT(float* io, int n, int m, float factor, bool affect_alpha)
{
  multiplyForFFT_template(io, n, m, factor, affect_alpha);
}

////////////////////////////////////////////////////////////////////////////////

void convolute2x2(TextureRGBA8& texture, const double matrix[4], bool wrapping, bool alpha, double extra_term)
{
  unsigned char* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();
  
  convoluteRGBA8<2, 2>(buffer, u, v, u2, matrix, wrapping, alpha, extra_term, 1.0);
}


void convolute3x3(TextureRGBA8& texture, const double matrix[9], bool wrapping, bool alpha, double extra_term)
{
  unsigned char* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();
  
  convoluteRGBA8<3, 3>(buffer, u, v, u2, matrix, wrapping, alpha, extra_term, 1.0);
}

void convolute5x5(TextureRGBA8& texture, const double matrix[25], bool wrapping, bool alpha, double extra_term)
{
  unsigned char* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();
  
  convoluteRGBA8<5, 5>(buffer, u, v, u2, matrix, wrapping, alpha, extra_term, 1.0);
}

void convolute7x7(TextureRGBA8& texture, const double matrix[49], bool wrapping, bool alpha, double extra_term, double mul)
{
  unsigned char* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();
  
  convoluteRGBA8<7, 7>(buffer, u, v, u2, matrix, wrapping, alpha, extra_term, mul);
}

////////////////////////////////////////////////////////////////////////////////

void convolute2x2(TextureRGBA32F& texture, const double matrix[4], bool wrapping, bool alpha, double extra_term)
{
  float* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();
  
  convoluteRGBA32F<2, 2>(buffer, u, v, u2, matrix, wrapping, alpha, extra_term, 1.0);
}


void convolute3x3(TextureRGBA32F& texture, const double matrix[9], bool wrapping, bool alpha, double extra_term)
{
  float* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();
  
  convoluteRGBA32F<3, 3>(buffer, u, v, u2, matrix, wrapping, alpha, extra_term, 1.0);
}

void convolute5x5(TextureRGBA32F& texture, const double matrix[25], bool wrapping, bool alpha, double extra_term)
{
  float* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();
  
  convoluteRGBA32F<5, 5>(buffer, u, v, u2, matrix, wrapping, alpha, extra_term, 1.0);
}

void convolute7x7(TextureRGBA32F& texture, const double matrix[49], bool wrapping, bool alpha, double extra_term, double mul)
{
  float* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();
  
  convoluteRGBA32F<7, 7>(buffer, u, v, u2, matrix, wrapping, alpha, extra_term, mul);
}
