/*
LodePaint

Copyright (c) 2009 Lode Vandevenne
All rights reserved.

This file is part of LodePaint.

LodePaint is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LodePaint is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LodePaint.  If not, see <http://www.gnu.org/licenses/>.
*/

 
#include "paint_algos.h"
#include "paint_algos_geom.h"
#include "paint_tool_geom.h"
#include "imalg/contour.h"
#include "lpi/lpi_math.h"
#include "lpi/lpi_math2d.h"
#include "lpi/lpi_translate.h"

////////////////////////////////////////////////////////////////////////////////


Tool_Geom::Tool_Geom(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom)
: GeomDialogTool(settings, geom)
, justdone(false)
{
}

bool Tool_Geom::done()
{
  bool result = justdone;
  justdone = false;
  return result;
}

void Tool_Geom::getUndoState(UndoState& state)
{
  state.swap(tempundo);
  tempundo.clear();
}

void Tool_Geom::undo(Paint& paint, UndoState& state)
{
  UndoStateIndex index;
  UndoState redostate;
  readPartialTextureFromUndo(paint.canvasRGBA8, redostate, state, index);
  state.swap(redostate);
  
}

void Tool_Geom::redo(Paint& paint, UndoState& state)
{
  UndoStateIndex index;
  UndoState undostate;
  readPartialTextureFromUndo(paint.canvasRGBA8, undostate, state, index);
  state.swap(undostate);
}


////////////////////////////////////////////////////////////////////////////////

Tool_Line::Tool_Line(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom)
: Tool_Geom(settings, geom)
, busy(false)
{
  dialog.addControl(_t("Affect Alpha"), new lpi::gui::DynamicCheckbox(&settings.other.affect_alpha));
}

void Tool_Line::handle(const lpi::IInput& input, Paint& paint)
{
  bool lmb = paint.mouseGrabbed(input, lpi::LMB);
  bool rmb = paint.mouseGrabbed(input, lpi::RMB);
  if(lmb || rmb)
  {
    if(!busy)
    {
      int drawx, drawy;
      paint.screenToPixel(drawx, drawy, input);

      x0 = drawx;
      y0 = drawy;
      left = lmb;
      busy = true;
    }
  }
  else if(busy)
  {
    int drawx, drawy;
    paint.screenToPixel(drawx, drawy, input);
    
    busy = false;
    x1 = drawx;
    y1 = drawy;
    busy = false;
    int half = settings.shape.line_thickness / 2 + 1;
    int ux0 = std::max(std::min(x0, x1) - half, 0);
    int uy0 = std::max(std::min(y0, y1) - half, 0);
    int ux1 = std::min(std::max(x0, x1) + half, (int)paint.canvasRGBA8.getU());
    int uy1 = std::min(std::max(y0, y1) + half, (int)paint.canvasRGBA8.getV());
    storePartialTextureInUndo(tempundo, paint.canvasRGBA8, ux0, uy0, ux1, uy1);
    drawLine(paint.canvasRGBA8.getBuffer(), paint.canvasRGBA8.getU2(), paint.canvasRGBA8.getV2(), x0, y0, x1, y1, settings.shape.line_thickness, left ? settings.leftColor255() : settings.rightColor255(), settings.shape.line_opacity, settings.other.affect_alpha, true);
    paint.uploadPartial(ux0, uy0, ux1, uy1);
    justdone = true;
  }
}

void Tool_Line::draw(lpi::gui::IGUIDrawer& drawer, Paint& paint)
{
  drawCommonCrosshair(drawer, paint, 0, 0);
  
  drawPenPixel(drawer, paint);
  
  if(busy)
  {
    int mx, my;
    paint.screenToNearestCenterOnScreen(mx, my, drawer.getInput().mouseX(), drawer.getInput().mouseY());
    int x, y;
    paint.pixelToScreen(x, y, x0, y0);
    lpi::ColorRGB color = left ? settings.leftColor255() : settings.rightColor255();
    color.a = (int)(255 * settings.shape.line_opacity);
    drawer.drawLine(x, y, mx, my, color);
  }
}

////////////////////////////////////////////////////////////////////////////////

Tool_Rectangle::Tool_Rectangle(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom)
: Tool_Geom(settings, geom)
, point(0)
, roundedcorners(0)
, square(false)
{
  dialog.addControl(_t("Rounded Corners"), new lpi::gui::DynamicSliderSpinner<int>(&roundedcorners, 0, 32, 1, geom));
  dialog.addControl(_t("Affect Alpha"), new lpi::gui::DynamicCheckbox(&settings.other.affect_alpha));
  dialog.addControl(_t("Square"), new lpi::gui::DynamicCheckbox(&square));
}

void Tool_Rectangle::handle(const lpi::IInput& input, Paint& paint)
{
  bool lmb = paint.mouseGrabbed(input, lpi::LMB);
  bool rmb = paint.mouseGrabbed(input, lpi::RMB);
  if(lmb || rmb)
  {
  
    if(point == 0)
    {
      int drawx, drawy;
      paint.screenToPixel(drawx, drawy, input);

      x0 = drawx;
      y0 = drawy;
      left = lmb;
      point = 1;
    }
  }
  else if(point == 1)
  {
    int drawx, drawy;
    paint.screenToPixel(drawx, drawy, input);

    x1 = drawx;
    y1 = drawy;
    if(square) y1 = y1 < y0 ? y0 - std::abs(x1 - x0) : y0 + std::abs(x1 - x0);
    point = 0;
    
    if(x0 > x1) std::swap(x0, x1);
    if(y0 > y1) std::swap(y0, y1);
    //the draw rectangle function doesn't include last row/column of pixels, but for the tool we want it
    x1++;
    y1++;
    
    int ux0 = std::max(x0, 0);
    int uy0 = std::max(y0, 0);
    int ux1 = std::min(x1, (int)paint.canvasRGBA8.getU());
    int uy1 = std::min(y1, (int)paint.canvasRGBA8.getV());
    storePartialTextureInUndo(tempundo, paint.canvasRGBA8, ux0, uy0, ux1, uy1);
    lpi::ColorRGB mainColor = left ? settings.leftColor255() : settings.rightColor255();
    lpi::ColorRGB otherColor = left ? settings.rightColor255() : settings.leftColor255();
    const lpi::ColorRGB& lineColor = mainColor;
    const lpi::ColorRGB& fillColor = settings.shape.line_enabled ? otherColor : mainColor;
    
    if(roundedcorners == 0)
    {
      drawRectangle(paint.canvasRGBA8.getBuffer(), paint.canvasRGBA8.getU2(), paint.canvasRGBA8.getV2()
                  , x0, y0, x1, y1
                  , settings.shape.line_thickness
                  , settings.shape.line_enabled, lineColor, settings.shape.line_opacity
                  , settings.shape.fill_enabled, fillColor, settings.shape.fill_opacity
                  , settings.other.affect_alpha);
    }
    else
    {
      drawRoundedRectangle(paint.canvasRGBA8.getBuffer(), paint.canvasRGBA8.getU2(), paint.canvasRGBA8.getV2()
                         , x0, y0, x1, y1
                         , roundedcorners, roundedcorners
                         , settings.shape.line_thickness
                         , settings.shape.line_enabled, lineColor, settings.shape.line_opacity
                         , settings.shape.fill_enabled, fillColor, settings.shape.fill_opacity
                         , settings.other.affect_alpha);
    }
    
    paint.uploadPartial(ux0, uy0, ux1, uy1);
    justdone = true;
  }
}

void Tool_Rectangle::draw(lpi::gui::IGUIDrawer& drawer, Paint& paint)
{
  drawCommonCrosshair(drawer, paint, 0, 0);
  
  drawPenPixel(drawer, paint);
  lpi::ColorRGB color = left ? settings.leftColor255() : settings.rightColor255();
  //color.a = (int)(255 * settings.shape.line_opacity);
  if(color.a < 64) color.a = 64; //so that it doesn't become too invisible...
  
  if(point == 1)
  {
    int mx0, my0, mx1, my1;
    paint.screenToPixelCornersOnScreen(mx0, my0, mx1, my1, drawer.getInput().mouseX(), drawer.getInput().mouseY());
    int px0, py0, px1, py1;
    paint.pixelToScreen(px0, py0, px1, py1, x0, y0);
    
    if(square)
    {
      my0 = my0 < py0 ? py0 - std::abs(mx0 - px0) : py0 + std::abs(mx0 - px0);
      my1 = my1 < py1 ? py1 - std::abs(mx1 - px1) : py1 + std::abs(mx1 - px1);
    }
    
    int sx0 = px0 < mx0 ? px0 : mx0;
    int sx1 = px1 > mx1 ? px1 : mx1;
    int sy0 = py0 < my0 ? py0 : my0;
    int sy1 = py1 > my1 ? py1 : my1;
  
    drawer.drawLine(sx0, sy0, sx1, sy0, color);
    drawer.drawLine(sx1, sy0, sx1, sy1, color);
    drawer.drawLine(sx1, sy1, sx0, sy1, color);
    drawer.drawLine(sx0, sy1, sx0, sy0, color);
    
    int thickness = (int)(settings.shape.line_thickness * paint.getZoom());
    
    if(settings.shape.line_enabled && thickness > 1 && lpi::template_abs(sx0 - sx1) > thickness * 2 && lpi::template_abs(sy0 - sy1) > thickness * 2)
    {
      int ix0 = sx1 > sx0 ? sx0 + thickness : sx0 - thickness;
      int ix1 = sx1 > sx0 ? sx1 - thickness : sx1 + thickness;
      int iy0 = sy1 > sy0 ? sy0 + thickness : sy0 - thickness;
      int iy1 = sy1 > sy0 ? sy1 - thickness : sy1 + thickness;
      
      drawer.drawLine(ix0, iy0, ix1, iy0, color);
      drawer.drawLine(ix1, iy0, ix1, iy1, color);
      drawer.drawLine(ix1, iy1, ix0, iy1, color);
      drawer.drawLine(ix0, iy1, ix0, iy0, color);
    }
  }
}

////////////////////////////////////////////////////////////////////////////////



Tool_Ellipse::Tool_Ellipse(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom)
: Tool_Geom(settings, geom)
, point(0)
, clickStatusL(false)
, clickStatusR(false)
, type(TYPE_ELLIPSE)
{
  std::vector<InputType> enums;
  enums.push_back(TYPE_ELLIPSE);
  enums.push_back(TYPE_CIRCLE_2);
  enums.push_back(TYPE_CIRCLE_3);
  std::vector<std::string> names;
  names.push_back(_t("Ellipse"));
  names.push_back(_t("Circle (2 points)"));
  names.push_back(_t("Circle (3 points)"));
  dialog.addControl(_t("Input"), new lpi::gui::DynamicEnum<InputType>(&type, names, enums, geom));
  dialog.addControl(_t("Affect Alpha"), new lpi::gui::DynamicCheckbox(&settings.other.affect_alpha));
}

bool circleBy3Points(double& x, double& y, double& radius
                   , double x0, double y0, double x1, double y1, double x2, double y2)
{
  double ma = (y1 - y0) / (x1 - x0);
  double mb = (y2 - y1) / (x2 - x1);

  if(ma == mb) return false; //divisions through 0

  x = (ma*mb*(y0 - y2) + mb*(x0 + x1) - ma*(x1 + x2)) / (2 * (mb - ma));
  if(ma != 0) y = ((x0+x1)/2.0 - x) / ma + (y0+y1)/2.0;
  else y = ((x2+x1)/2.0 - x) / mb + (y2+y1)/2.0;
  radius = std::sqrt((x0-x)*(x0-x) + (y0-y)*(y0-y));

  return true;
}

/*
output is the bounding square
this version is made specifically for the pixely paint tool
*/
bool circleBy3Points(int& cx0, int& cy0, int& cx1, int& cy1
                   , int x0, int y0, int x1, int y1, int x2, int y2)
{
  if(x1 == x2 && y1 == y2) //special case
  {
    double radius = std::sqrt((x1-x0)*(x1-x0) + (y1-y0)*(y1-y0)) / 2;

    cx0 = (int)((x0+x1)/2 - radius);
    cy0 = (int)((y0+y1)/2 - radius);
    cx1 = (int)((x0+x1)/2 + radius + 1);
    cy1 = (int)((y0+y1)/2 + radius + 1);

    return true;
  }
  
  double x, y, radius;
  if(!circleBy3Points(x, y, radius, x0, y0, x1, y1, x2, y2)) return false;

  cx0 = (int)(x - radius);
  cy0 = (int)(y - radius);
  cx1 = (int)(x + radius + 1);
  cy1 = (int)(y + radius + 1);
  
  return true;
}

void Tool_Ellipse::handle(const lpi::IInput& input, Paint& paint)
{
  bool lmb = false, rmb = false;
  
  if(type != TYPE_CIRCLE_3)
  {
    bool l = paint.mouseGrabbed(input, lpi::LMB);
    bool r = paint.mouseGrabbed(input, lpi::RMB);
    
    if(l && !clickStatusL) lmb = true;
    else if(!l && clickStatusL) lmb = true;
    clickStatusL = l;
    
    if(r && !clickStatusR) rmb = true;
    else if(!r && clickStatusR) rmb = true;
    clickStatusR = r;
  }
  else
  {
    lmb = paint.clicked(input, lpi::LMB);
    rmb = paint.clicked(input, lpi::RMB);
  }
  
  
  if(lmb || rmb)
  {

    int drawx, drawy;
    paint.screenToPixel(drawx, drawy, input);
    
    bool done = false;
    int cx0 = 0, cy0 = 0, cx1 = 0, cy1 = 0;


    if(point == 0)
    {
      x0 = drawx;
      y0 = drawy;
      left = lmb;
      point = 1;
    }
    else if(point == 1)
    {
      x1 = drawx;
      y1 = drawy;
      
      if(type == TYPE_ELLIPSE)
      {
        point = 0;

        if(x0 > x1) std::swap(x0, x1);
        if(y0 > y1) std::swap(y0, y1);
        
        cx0 = x0;
        cy0 = y0;
        //the draw functions don't include last row/column of pixels, but for the tool we want it
        cx1 = x1 + 1;
        cy1 = y1 + 1;

        done = true;
      }
      else if(type == TYPE_CIRCLE_2)
      {
        point = 0;

        double radius = std::sqrt((x1-x0)*(x1-x0) + (y1-y0)*(y1-y0));

        cx0 = (int)(x0 - radius);
        cy0 = (int)(y0 - radius);
        cx1 = (int)(x0 + radius + 1);
        cy1 = (int)(y0 + radius + 1);
        
        done = true;
      }
      else if(type == TYPE_CIRCLE_3)
      {
        x1 = drawx;
        y1 = drawy;
        point = 2;
      }
    }
    else if(point == 2)
    {
      if(type == TYPE_CIRCLE_3)
      {
        point = 0;
        if(circleBy3Points(cx0, cy0, cx1, cy1, x0, y0, x1, y1, drawx, drawy))
        {
          done = true;
        }
        else
        {
          //no valid 3 points, reset without drawing anything
          done = false;
        }
      }
    }
    
    if(done)
    {
      done = false;
      
      int ux0 = std::max(cx0, 0);
      int uy0 = std::max(cy0, 0);
      int ux1 = std::min(cx1 + 1, (int)paint.canvasRGBA8.getU());
      int uy1 = std::min(cy1 + 1, (int)paint.canvasRGBA8.getV());
      
      storePartialTextureInUndo(tempundo, paint.canvasRGBA8, ux0, uy0, ux1, uy1);
      lpi::ColorRGB mainColor = left ? settings.leftColor255() : settings.rightColor255();
      lpi::ColorRGB otherColor = left ? settings.rightColor255() : settings.leftColor255();
      const lpi::ColorRGB& lineColor = mainColor;
      const lpi::ColorRGB& fillColor = settings.shape.line_enabled ? otherColor : mainColor;

      drawEllipse(paint.canvasRGBA8.getBuffer(), paint.canvasRGBA8.getU2(), paint.canvasRGBA8.getV2()
                 , cx0, cy0, cx1, cy1
                 , settings.shape.line_thickness
                 , settings.shape.line_enabled, lineColor, settings.shape.line_opacity
                 , settings.shape.fill_enabled, fillColor, settings.shape.fill_opacity
                 , settings.other.affect_alpha);

      paint.uploadPartial(ux0, uy0, ux1, uy1);
      justdone = true;
    }
  }
}

void Tool_Ellipse::draw(lpi::gui::IGUIDrawer& drawer, Paint& paint)
{
  drawCommonCrosshair(drawer, paint, 0, 0);
  
  drawPenPixel(drawer, paint);
  lpi::ColorRGB color = left ? settings.leftColor255() : settings.rightColor255();
  if(color.a < 64) color.a = 64; //so that it doesn't become too invisible...
  
  bool draw = false;
  int sx0, sy0, sx1, sy1;

  if(type == TYPE_ELLIPSE)
  {
    if(point == 1)
    {
      int mx0, my0, mx1, my1;
      paint.screenToPixelCornersOnScreen(mx0, my0, mx1, my1, drawer.getInput().mouseX(), drawer.getInput().mouseY());
      int px0, py0, px1, py1;
      paint.pixelToScreen(px0, py0, px1, py1, x0, y0);

      sx0 = px0 < mx0 ? px0 : mx0;
      sx1 = px1 > mx1 ? px1 : mx1;
      sy0 = py0 < my0 ? py0 : my0;
      sy1 = py1 > my1 ? py1 : my1;

      draw = true;
    }
  }
  else if(type == TYPE_CIRCLE_2)
  {
    if(point == 1)
    {
      int mx0, my0, mx1, my1;
      paint.screenToPixelCornersOnScreen(mx0, my0, mx1, my1, drawer.getInput().mouseX(), drawer.getInput().mouseY());
      int cx0, cy0, cx1, cy1;
      paint.pixelToScreen(cx0, cy0, cx1, cy1, x0, y0);
      
      double radius = std::sqrt((mx0-cx0)*(mx0-cx0) + (my0-cy0)*(my0-cy0));
      
      sx0 = (int)(cx0 - radius);
      sy0 = (int)(cy0 - radius);
      sx1 = (int)(cx1 + radius);
      sy1 = (int)(cy1 + radius);

      draw = true;
    }
  }
  else if(type == TYPE_CIRCLE_3)
  {
    int mx0, my0, mx1, my1;
    paint.screenToPixelCornersOnScreen(mx0, my0, mx1, my1, drawer.getInput().mouseX(), drawer.getInput().mouseY());
    int ax0, ay0, ax1, ay1;
    paint.pixelToScreen(ax0, ay0, ax1, ay1, x0, y0);
      
    if(point == 1)
    {
      int mx0, my0, mx1, my1;
      paint.screenToPixelCornersOnScreen(mx0, my0, mx1, my1, drawer.getInput().mouseX(), drawer.getInput().mouseY());

      double radius = std::sqrt((mx0-ax0)*(mx0-ax0) + (my0-ay0)*(my0-ay0)) / 2;

      sx0 = (int)((ax0+mx0)/2 - radius);
      sy0 = (int)((ay0+my0)/2 - radius);
      sx1 = (int)((ax1+mx0)/2 + radius);
      sy1 = (int)((ay1+my0)/2 + radius);

      draw = true;
      
      drawer.drawText("2", drawer.getInput().mouseX() + 16, drawer.getInput().mouseY());
      drawSpot(drawer, paint, x0, y0);
    }
    else if(point == 2)
    {
      int bx0, by0, bx1, by1;
      paint.pixelToScreen(bx0, by0, bx1, by1, x1, y1);
      
      if(circleBy3Points(sx0, sy0, sx1, sy1, ax0, ay0, bx0, by0, mx0, my0))
        draw = true;
        
      drawer.drawText("3", drawer.getInput().mouseX() + 16, drawer.getInput().mouseY());
      drawSpot(drawer, paint, x0, y0);
      drawSpot(drawer, paint, x1, y1);
    }
  }
  
  if(draw)
  {
    drawer.drawEllipse(sx0, sy0, sx1, sy1, color, false);

    int thickness = (int)(settings.shape.line_thickness * paint.getZoom());

    if(settings.shape.line_enabled && thickness > 1 && lpi::template_abs(sx0 - sx1) > thickness * 2 && lpi::template_abs(sy0 - sy1) > thickness * 2)
    {
      int ix0 = sx1 > sx0 ? sx0 + thickness : sx0 - thickness;
      int ix1 = sx1 > sx0 ? sx1 - thickness : sx1 + thickness;
      int iy0 = sy1 > sy0 ? sy0 + thickness : sy0 - thickness;
      int iy1 = sy1 > sy0 ? sy1 - thickness : sy1 + thickness;

      drawer.drawEllipse(ix0, iy0, ix1, iy1, color, false);
    }
  }
}


////////////////////////////////////////////////////////////////////////////////

Tool_Polygon::Tool_Polygon(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom)
: Tool_Geom(settings, geom)
, flag(false)
, allow_freehand(false)
{
  dialog.addControl(_t("Affect Alpha"), new lpi::gui::DynamicCheckbox(&settings.other.affect_alpha));
  dialog.addControl(_t("Allow Freehand"), new lpi::gui::DynamicCheckbox(&allow_freehand));
  
}

void Tool_Polygon::handle(const lpi::IInput& input, Paint& paint)
{
  bool lmb = allow_freehand ? paint.mouseDownHere(input, lpi::LMB) : paint.clicked(input, lpi::LMB);
  bool rmb = allow_freehand ? paint.mouseDownHere(input, lpi::RMB) : paint.clicked(input, lpi::RMB);
  bool dclick = paint.mouseDoubleClicked(input, lpi::LMB) || paint.mouseDoubleClicked(input, lpi::RMB);
  
  if((lmb || rmb || dclick) && !flag)
  {
    int drawx, drawy;
    paint.screenToPixel(drawx, drawy, input);
    
    if(xs.empty()) left = lmb;
    
    if(left != lmb || dclick) //done
    {
      if(!dclick) //because this point is already added if you double clicked
      {
        xs.push_back(drawx);
        ys.push_back(drawy);
      }
     
      if(xs.size() > 2)
      {
        int ux0, uy0, ux1, uy1;
        getContourBounds(ux0, uy0, ux1, uy1, &xs[0], &ys[0], xs.size());
        
        ux0 = lpi::clamp<int>(ux0 - 1, 0, paint.canvasRGBA8.getU());
        uy0 = lpi::clamp<int>(uy0 - 1, 0, paint.canvasRGBA8.getV());
        ux1 = lpi::clamp<int>(ux1 + 1, 0, paint.canvasRGBA8.getU());
        uy1 = lpi::clamp<int>(uy1 + 1, 0, paint.canvasRGBA8.getV());

        storePartialTextureInUndo(tempundo, paint.canvasRGBA8, ux0, uy0, ux1, uy1);
        
        const lpi::ColorRGB& mainColor = left ? settings.leftColor255() : settings.rightColor255();
        const lpi::ColorRGB& otherColor = left ? settings.rightColor255() : settings.leftColor255();
        const lpi::ColorRGB& lineColor = mainColor;
        const lpi::ColorRGB& fillColor = settings.shape.line_enabled ? otherColor : mainColor;
        
        if(xs.size() > 3)
        {
          drawPolygon(paint.canvasRGBA8.getBuffer(), paint.canvasRGBA8.getU2(), paint.canvasRGBA8.getV2()
                     , &xs[0], &ys[0], xs.size()
                     , settings.shape.line_thickness
                     , settings.shape.line_enabled, lineColor, settings.shape.line_opacity
                     , settings.shape.fill_enabled, fillColor, settings.shape.fill_opacity
                     , settings.other.affect_alpha);
        }
        else if(xs.size() == 3)
        {
          drawTriangle(paint.canvasRGBA8.getBuffer(), paint.canvasRGBA8.getU2(), paint.canvasRGBA8.getV2()
                     , xs[0], ys[0], xs[1], ys[1], xs[2], ys[2]
                     , settings.shape.line_thickness
                     , settings.shape.line_enabled, lineColor, settings.shape.line_opacity
                     , settings.shape.fill_enabled, fillColor, settings.shape.fill_opacity
                     , settings.other.affect_alpha);
        }
    
                  
                  

        //this is just a test.
        /*Contour contour;
        intsToContour(contour, &xs[0], &ys[0], xs.size());
        bool ccw = calcDirection(contour);
        PolyContour poly, dummy;
        if(ccw) groupSubContoursByDirection(dummy, poly, contour);
        else groupSubContoursByDirection(poly, dummy, contour);
        for(size_t i = 0; i < poly.size(); i++)
        {
          std::vector<int> xs2;
          std::vector<int> ys2;
          contourToInts(xs2, ys2, *poly[i]);
          fillPolygon(paint.canvasRGBA8.getBuffer(), paint.canvasRGBA8.getU2(), paint.canvasRGBA8.getV2(),
                      &xs2[0], &ys2[0], xs2.size(),
                      fillColor, settings.shape.fill_opacity);
        }*/
                   
        paint.uploadPartial(ux0, uy0, ux1, uy1);
        justdone = true;
      }
      xs.clear();
      ys.clear();
      flag = true; //done
    }
    else if(!dclick) //add point
    {
      if(xs.empty() || xs.back() != drawx || ys.back() != drawy) //avoid adding the same point twice, simply for performance reasons
      {
        xs.push_back(drawx);
        ys.push_back(drawy);
      }
    }
  }
  if(!lmb && !rmb) flag = false;
}

void Tool_Polygon::draw(lpi::gui::IGUIDrawer& drawer, Paint& paint)
{
  drawCommonCrosshair(drawer, paint, 0, 0);
  
  drawPenPixel(drawer, paint);
  
  lpi::ColorRGB color = left ? settings.leftColor255() : settings.rightColor255();
  
  if(xs.size() == 2)
  {
    int mx, my;
    paint.screenToNearestCenterOnScreen(mx, my, drawer.getInput().mouseX(), drawer.getInput().mouseY());

    int sx0, sy0, sx1, sy1;
    paint.pixelToScreen(sx0, sy0, xs[0], ys[0]);
    paint.pixelToScreen(sx1, sy1, xs[1], ys[1]);
    drawer.drawLine(sx0, sy0, sx1, sy1, color);
    drawer.drawLine(sx0, sy0, mx, my, color);
    drawer.drawLine(sx1, sy1, mx, my, color);

    int thickness = (int)(settings.shape.line_thickness * paint.getZoom());

    if(settings.shape.line_enabled && thickness > 1.0)
    {
      //points
      lpi::Vector2 p0(sx0, sy0);
      lpi::Vector2 p1(sx1, sy1);
      lpi::Vector2 p2(mx, my);
      //I want them counterclockwise
      if(!ccw(p0, p1, p2)) std::swap(p0, p1);
      lpi::Vector2 i0, i1, i2;


      if(getInnerTriangle(i0, i1, i2, p0, p1, p2, thickness))
      {
        //draw it
        drawer.drawLine((int)i0.x, (int)i0.y, (int)i1.x, (int)i1.y, color);
        drawer.drawLine((int)i1.x, (int)i1.y, (int)i2.x, (int)i2.y, color);
        drawer.drawLine((int)i2.x, (int)i2.y, (int)i0.x, (int)i0.y, color);
      }
    }
  }
  else if(xs.size() > 0)
  {
    for(size_t i = 0; i < xs.size() - 1; i++)
    {
      int sx0, sy0, sx1, sy1;
      paint.pixelToScreen(sx0, sy0, xs[i], ys[i]);
      paint.pixelToScreen(sx1, sy1, xs[i + 1], ys[i + 1]);
      drawer.drawLine(sx0, sy0, sx1, sy1, color);
    }
    
    int mx, my;
    paint.screenToNearestCenterOnScreen(mx, my, drawer.getInput().mouseX(), drawer.getInput().mouseY());
    int sx0, sy0, sx1, sy1;
    paint.pixelToScreen(sx0, sy0, xs[0], ys[0]);
    paint.pixelToScreen(sx1, sy1, xs[xs.size() - 1], ys[ys.size() - 1]);
    drawer.drawLine(sx0, sy0, mx, my, color);
    drawer.drawLine(sx1, sy1, mx, my, color);
  }
}

////////////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////////////

Tool_PerspectiveCrop::Tool_PerspectiveCrop(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom)
: ITool(settings)
, eight(false)
, point(0)
{
  (void)geom;
  dialog.addControl(_t("8 points"), new lpi::gui::DynamicCheckbox(&eight));
}

void Tool_PerspectiveCrop::doit(Paint& paint)
{
  TextureRGBA8& texture = paint.canvasRGBA8;
  size_t oldu = texture.getU();
  size_t oldv = texture.getV();

  std::vector<unsigned char> old;
  getAlignedBuffer(old, &texture);

  int u = oldu;
  int v = oldv;

  texture.setSize(u, v);

  size_t u2 = texture.getU2();
  unsigned char* buffer = texture.getBuffer();
  
  for(int y = 0; y < v; y++)
  for(int x = 0; x < u; x++)
  {
    size_t oindex = y * u2 * 4 + x * 4; //output index
    
    //relative x and y
    double rx = (double)x / (double)u;
    double ry = (double)y / (double)v;
    
    //perspective x and y on the 4 sides
    /*
     0    v01   1
      *----.----*
      |        /
   v03|       /v12
      |      /
     3*--.--*2
        v32
    */
    
    double px01 = x0 + rx * (x1 - x0);
    double py01 = y0 + rx * (y1 - y0);
    double px12 = x1 + ry * (x2 - x1);
    double py12 = y1 + ry * (y2 - y1);
    double px32 = x3 + rx * (x2 - x3);
    double py32 = y3 + rx * (y2 - y3);
    double px03 = x0 + ry * (x3 - x0);
    double py03 = y0 + ry * (y3 - y0);
    lpi::Vector2 v01(px01, py01);
    lpi::Vector2 v12(px12, py12);
    lpi::Vector2 v32(px32, py32);
    lpi::Vector2 v03(px03, py03);
    
    lpi::Vector2 intersection;

    lpi::intersectLineLine(intersection, v01, v32, v03, v12);
    
    int x2 = (int)intersection.x;
    int y2 = (int)intersection.y;
    
    if(x2 < (int)oldu && x2 >= 0 && y2 < (int)oldv && y2 >= 0)
    {
      size_t iindex = y2 * oldu * 4 + x2 * 4; //input index
      buffer[oindex + 0] = old[iindex + 0];
      buffer[oindex + 1] = old[iindex + 1];
      buffer[oindex + 2] = old[iindex + 2];
      buffer[oindex + 3] = old[iindex + 3];
    }
    else
    {
      buffer[oindex + 0] = settings.rightColor255().r;
      buffer[oindex + 1] = settings.rightColor255().g;
      buffer[oindex + 2] = settings.rightColor255().b;
      buffer[oindex + 3] = settings.rightColor255().a;
    }
  }
  
  paint.update();
}

void Tool_PerspectiveCrop::handle(const lpi::IInput& input, Paint& paint)
{
  bool lmb = paint.clicked(input, lpi::LMB);
  bool rmb = paint.clicked(input, lpi::RMB);
  if(rmb)
  {
    point = 0;
  }
  else if(lmb)
  {

    int drawx, drawy;
    paint.screenToPixel(drawx, drawy, input);

    if(eight)
    {
      p8[point].x = drawx;
      p8[point].y = drawy;
      
      point++;
      
      if(point == 8)
      {
        point = 0;
        
        /*
        The 8 points (0-7) and the 4 points gotten from them (v0-v3)
          0     1
      v0*-----------*v1
        |           |
       7|           |2
        |           |
       6|           |3
        |           |
      v3*-----------*v2
           5     4
        */
        
        
        lpi::Vector2 v0, v1, v2, v3;
        lpi::intersectLineLine(v0, p8[0], p8[1], p8[6], p8[7]);
        lpi::intersectLineLine(v1, p8[0], p8[1], p8[2], p8[3]);
        lpi::intersectLineLine(v2, p8[2], p8[3], p8[4], p8[5]);
        lpi::intersectLineLine(v3, p8[4], p8[5], p8[6], p8[7]);
        x0 = v0.x;
        y0 = v0.y;
        x1 = v1.x;
        y1 = v1.y;
        x2 = v2.x;
        y2 = v2.y;
        x3 = v3.x;
        y3 = v3.y;

        storeCompleteTextureInUndo(tempundo, paint.canvasRGBA8);
        doit(paint);
        justdone = true;
      }
    }
    else
    {
      if(point == 0)
      {
        x0 = drawx;
        y0 = drawy;
        point++;
      }
      else if(point == 1)
      {
        x1 = drawx;
        y1 = drawy;
        point++;
      }
      else if(point == 2)
      {
        x2 = drawx;
        y2 = drawy;
        point++;
      }
      else
      {
        x3 = drawx;
        y3 = drawy;
        point = 0;

        storeCompleteTextureInUndo(tempundo, paint.canvasRGBA8);
        doit(paint);
        justdone = true;
      }
    }
  }
}

void Tool_PerspectiveCrop::draw(lpi::gui::IGUIDrawer& drawer, Paint& paint)
{
  drawCommonCrosshair(drawer, paint, 0, 0);
  
  drawPenPixel(drawer, paint);

  int mx, my;
  paint.screenToNearestCenterOnScreen(mx, my, drawer.getInput().mouseX(), drawer.getInput().mouseY());
  
  int sx0, sy0, sx1, sy1, sx2, sy2;
  paint.pixelToScreen(sx0, sy0, (int)x0, (int)y0);
  paint.pixelToScreen(sx1, sy1, (int)x1, (int)y1);
  paint.pixelToScreen(sx2, sy2, (int)x2, (int)y2);
  
  lpi::ColorRGB color = lpi::RGB_Black;
  
  if(eight)
  {
    for(int i = 0; i < point; i++) drawSpot(drawer, paint, (int)p8[i].x, (int)p8[i].y);
    if(point > 0 && point < 7)
    {
      std::string s;
      s += (char)('1' + point);
      drawer.drawText(s, drawer.getInput().mouseX() + 16, drawer.getInput().mouseY());
    }
    else if(point == 7) drawer.drawText(_t("Go"), drawer.getInput().mouseX() + 16, drawer.getInput().mouseY());
  }
  else
  {
    if(point >= 1) drawSpot(drawer, paint, (int)x0, (int)y0);
    if(point >= 2) drawSpot(drawer, paint, (int)x1, (int)y1);
    if(point >= 3) drawSpot(drawer, paint, (int)x2, (int)y2);

    if(point == 1)
    {
      drawer.drawLine(mx, my, sx0, sy0, color);
    }
    if(point == 2)
    {
      drawer.drawLine(sx0, sy0, sx1, sy1, color);
      drawer.drawLine(mx, my, sx1, sy1, color);
    }
    if(point == 3)
    {
      drawer.drawLine(sx0, sy0, sx1, sy1, color);
      drawer.drawLine(sx1, sy1, sx2, sy2, color);
      drawer.drawLine(sx0, sy0, mx, my, color);
      drawer.drawLine(sx2, sy2, mx, my, color);
    }

    if(point == 1) drawer.drawText("2", drawer.getInput().mouseX() + 16, drawer.getInput().mouseY());
    else if(point == 2) drawer.drawText("3", drawer.getInput().mouseX() + 16, drawer.getInput().mouseY());
    else if(point == 3) drawer.drawText(_t("Go"), drawer.getInput().mouseX() + 16, drawer.getInput().mouseY());
  }
}

bool Tool_PerspectiveCrop::done()
{
  bool result = justdone;
  justdone = false;
  return result;
}

void Tool_PerspectiveCrop::getUndoState(UndoState& state)
{
  state.swap(tempundo);
  tempundo.clear();
}

void Tool_PerspectiveCrop::undo(Paint& paint, UndoState& state)
{
  UndoStateIndex index;
  UndoState temp;
  storeCompleteTextureInUndo(temp, paint.canvasRGBA8);
  readCompleteTextureFromUndo(paint.canvasRGBA8, state, index);
  state.swap(temp);
}

void Tool_PerspectiveCrop::redo(Paint& paint, UndoState& state)
{
  UndoStateIndex index;
  UndoState temp;
  storeCompleteTextureInUndo(temp, paint.canvasRGBA8);
  readCompleteTextureFromUndo(paint.canvasRGBA8, state, index);
  state.swap(temp);
}

////////////////////////////////////////////////////////////////////////////////

Tool_SierpinskiTriangle::Tool_SierpinskiTriangle(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom)
: Tool_Geom(settings, geom)
, point(0)
{
  dialog.addControl(_t("Affect Alpha"), new lpi::gui::DynamicCheckbox(&settings.other.affect_alpha));
}

void Tool_SierpinskiTriangle::handle(const lpi::IInput& input, Paint& paint)
{
  bool lmb = paint.clicked(input, lpi::LMB);
  bool rmb = paint.clicked(input, lpi::RMB);
  if(lmb || rmb)
  {
  
    int drawx, drawy;
    paint.screenToPixel(drawx, drawy, input);
  
    if(point == 0)
    {
      x0 = drawx;
      y0 = drawy;
      left = lmb;
      point = 1;
    }
    else if(point == 1)
    {
      x1 = drawx;
      y1 = drawy;
      left = lmb;
      point = 2;
    }
    else
    {
      x2 = drawx;
      y2 = drawy;
      point = 0;
      int half = settings.shape.line_thickness / 2 + 1;
      int ux0 = std::max(std::min(x0, std::min(x1, x2)) - half, 0);
      int uy0 = std::max(std::min(y0, std::min(y1, y2)) - half, 0);
      int ux1 = std::min(std::max(x0, std::max(x1, x2)) + half, (int)paint.canvasRGBA8.getU());
      int uy1 = std::min(std::max(y0, std::max(y1, y2)) + half, (int)paint.canvasRGBA8.getV());
      storePartialTextureInUndo(tempundo, paint.canvasRGBA8, ux0, uy0, ux1, uy1);
      
      //fillTriangle(paint.canvasRGBA8.getBuffer(), paint.canvasRGBA8.getU2(), paint.canvasRGBA8.getV2(), x0, y0, x1, y1, x2, y2, left ? settings.leftColor : settings.rightColor, settings.shape.fill_opacity);
      
      
      int minx = std::min(x0, std::min(x1, x2));
      int maxx = std::max(x0, std::max(x1, x2));
      int miny = std::min(y0, std::min(y1, y2));
      int maxy = std::max(y0, std::max(y1, y2));
      int triangleArea = (maxx - minx) * (maxy - miny) / 2;
      int numSteps = triangleArea; //todo: make custom tool parameter that affects this
      
      float px = x0;
      float py = y0;
      
      //do the process numSteps times
      for(int n = 0; n < numSteps; n++)
      {
          //draw the pixel
          pset(paint.canvasRGBA8.getBuffer(), paint.canvasRGBA8.getU(), paint.canvasRGBA8.getV(), paint.canvasRGBA8.getU2(),
               int(px), int(py), left ? settings.leftColor255() : settings.rightColor255(), settings.shape.fill_opacity, settings.other.affect_alpha);
          
          //pick a random number 0, 1 or 2
          switch(lpi::getRandom(0, 2))
          {
            //depending on the number, choose another new coordinate for point p
            case 0:
              px = (px + x0) / 2.0;
              py = (py + y0) / 2.0;
              break;
            case 1:
              px = (px + x1) / 2.0;
              py = (py + y1) / 2.0;
              break;
            case 2:
              px = (px + x2) / 2.0;
              py = (py + y2) / 2.0;
              break;
          }
      }

      
      paint.uploadPartial(ux0, uy0, ux1, uy1);
      justdone = true;
    }
  }
}

void Tool_SierpinskiTriangle::draw(lpi::gui::IGUIDrawer& drawer, Paint& paint)
{
  drawCommonCrosshair(drawer, paint, 0, 0);
  
  drawPenPixel(drawer, paint);
  lpi::ColorRGB color = left ? settings.leftColor255() : settings.rightColor255();
  //color.a = (int)(255 * settings.shape.line_opacity);
  
  int mx, my;
  paint.screenToNearestCenterOnScreen(mx, my, drawer.getInput().mouseX(), drawer.getInput().mouseY());
  
  if(point == 1)
  {
    int x, y;
    paint.pixelToScreen(x, y, x0, y0);
    drawer.drawLine(x, y, mx, my, color);
    
    drawer.drawText("2", drawer.getInput().mouseX() + 16, drawer.getInput().mouseY());
    drawSpot(drawer, paint, x0, y0);
  }
  else if(point == 2)
  {
    int sx0, sy0, sx1, sy1;
    paint.pixelToScreen(sx0, sy0, x0, y0);
    paint.pixelToScreen(sx1, sy1, x1, y1);
    drawer.drawLine(sx0, sy0, sx1, sy1, color);
    drawer.drawLine(sx0, sy0, mx, my, color);
    drawer.drawLine(sx1, sy1, mx, my, color);
    drawer.drawLine((sx0 + sx1)/2, (sy0 + sy1)/2, (sx1 + mx)/2, (sy1 + my)/2, color);
    drawer.drawLine((sx0 + sx1)/2, (sy0 + sy1)/2, (mx + sx0)/2, (my + sy0)/2, color);
    drawer.drawLine((sx1 + mx)/2, (sy1 + my)/2, (sx0 + mx)/2, (sy0 + my)/2, color);

    drawer.drawText("3", drawer.getInput().mouseX() + 16, drawer.getInput().mouseY());
    drawSpot(drawer, paint, x0, y0);
    drawSpot(drawer, paint, x1, y1);
  }
}


////////////////////////////////////////////////////////////////////////////////

Tool_GradientLine::Tool_GradientLine(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom)
: Tool_Geom(settings, geom)
, busy(false)
{
  dialog.addControl(_t("Affect Alpha"), new lpi::gui::DynamicCheckbox(&settings.other.affect_alpha));
}

static int roundValue(double d)
{
  double frac = d - (int)d;
  return frac < 0.5 ? (int)d : (int)d + 1;
}

//Using the color math formulas of lpi results in problems due to integer rounding, hence this function
static lpi::ColorRGB weighedColor(const lpi::ColorRGB& a, const lpi::ColorRGB& b, double w)
{
  lpi::ColorRGB r;
  r.r = roundValue(w * a.r + (1.0 - w) * b.r);
  r.g = roundValue(w * a.g + (1.0 - w) * b.g);
  r.b = roundValue(w * a.b + (1.0 - w) * b.b);
  r.a = roundValue(w * a.a + (1.0 - w) * b.a);
  return r;
}


void Tool_GradientLine::drawGradientLine(Paint& paint, int x0, int y0, int x1, int y1, bool colorInvert)
{
  if(std::abs(x1 - x0) > std::abs(y1 - y0))
  {
    if(x0 > x1)
    {
      std::swap(x0, x1);
      std::swap(y0, y1);
      colorInvert = !colorInvert;
    }
    for(int x = x0; x <= x1; x++)
    {
      double d = (x - x0) / (double)(x1 - x0);
      double dd = colorInvert ? 1.0 - d : d;
      lpi::ColorRGB c = weighedColor(settings.rightColor255(), settings.leftColor255(), dd);
      int y = y0 + (int)(d * (y1 - y0));
      pset(paint.canvasRGBA8.getBuffer(), paint.canvasRGBA8.getU(), paint.canvasRGBA8.getV(), paint.canvasRGBA8.getU2(),
           x, y, c, settings.shape.line_opacity, settings.other.affect_alpha);
    }
  }
  else
  {
    if(y0 > y1)
    {
      std::swap(x0, x1);
      std::swap(y0, y1);
      colorInvert = !colorInvert;
    }
    for(int y = y0; y <= y1; y++)
    {
      double d = (y - y0) / (double)(y1 - y0);
      double dd = colorInvert ? 1.0 - d : d;
      lpi::ColorRGB c = weighedColor(settings.rightColor255(), settings.leftColor255(), dd);
      int x = x0 + (int)(d * (x1 - x0));
      pset(paint.canvasRGBA8.getBuffer(), paint.canvasRGBA8.getU(), paint.canvasRGBA8.getV(), paint.canvasRGBA8.getU2(),
           x, y, c, settings.shape.line_opacity, settings.other.affect_alpha);
    }
  }
}

void Tool_GradientLine::handle(const lpi::IInput& input, Paint& paint)
{
  bool lmb = paint.mouseGrabbed(input, lpi::LMB);
  bool rmb = paint.mouseGrabbed(input, lpi::RMB);
  if(lmb || rmb)
  {

    if(!busy)
    {
      int drawx, drawy;
      paint.screenToPixel(drawx, drawy, input);

      x0 = drawx;
      y0 = drawy;
      left = lmb;
      busy = true;
    }
  }
  else if(busy)
  {
    int drawx, drawy;
    paint.screenToPixel(drawx, drawy, input);

    x1 = drawx;
    y1 = drawy;
    busy = false;
    int half = settings.shape.line_thickness / 2 + 1;
    int ux0 = std::max(std::min(x0, x1) - half, 0);
    int uy0 = std::max(std::min(y0, y1) - half, 0);
    int ux1 = std::min(std::max(x0, x1) + half, (int)paint.canvasRGBA8.getU());
    int uy1 = std::min(std::max(y0, y1) + half, (int)paint.canvasRGBA8.getV());
    storePartialTextureInUndo(tempundo, paint.canvasRGBA8, ux0, uy0, ux1, uy1);
    drawGradientLine(paint, x0, y0, x1, y1, rmb);
    paint.uploadPartial(ux0, uy0, ux1, uy1);
    justdone = true;

  }
}

void Tool_GradientLine::draw(lpi::gui::IGUIDrawer& drawer, Paint& paint)
{
  drawCommonCrosshair(drawer, paint, 0, 0);

  drawPenPixel(drawer, paint);

  if(busy)
  {
    int mx, my;
    paint.screenToNearestCenterOnScreen(mx, my, drawer.getInput().mouseX(), drawer.getInput().mouseY());
    int x, y;
    paint.pixelToScreen(x, y, x0, y0);
    lpi::ColorRGB color = left ? settings.leftColor255() : settings.rightColor255();
    color.a = (int)(255 * settings.shape.line_opacity);
    drawer.drawLine(x, y, mx, my, color);
  }
}

////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
