/*
LodePaint

Copyright (c) 2009-2010 Lode Vandevenne
All rights reserved.

This file is part of LodePaint.

LodePaint is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LodePaint is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LodePaint.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "paint_undo.h" 
#include "paint_tool.h"
#include "main.h"

////////////////////////////////////////////////////////////////////////////////

UndoState::UndoState()
{
}

UndoState::~UndoState()
{
  clear();
}


void UndoState::addData(const std::vector<unsigned char>& data)
{
  this->data.push_back(new std::vector<unsigned char>);
  (*(this->data.back())) = data;
}

void UndoState::addData(const unsigned char* data, size_t size)
{
  this->data.push_back(new std::vector<unsigned char>);
  std::vector<unsigned char>& v = *(this->data.back());
  v.resize(size);
  for(size_t i = 0; i < size; i++) v[i] = data[i];
}

std::vector<unsigned char>& UndoState::getData(size_t index)
{
  return *(data[index]);
}

const std::vector<unsigned char>& UndoState::getData(size_t index) const
{
  return *(data[index]);
}

size_t UndoState::getNumData() const
{
  return data.size();
}

////////////////////////////////////////////////////////////////////////////////

void UndoState::addFData(const std::vector<float>& fdata)
{
  this->fdata.push_back(new std::vector<float>);
  (*(this->fdata.back())) = fdata;
}

void UndoState::addFData(const float* fdata, size_t size)
{
  this->fdata.push_back(new std::vector<float>);
  std::vector<float>& v = *(this->fdata.back());
  v.resize(size);
  for(size_t i = 0; i < size; i++) v[i] = fdata[i];
}

std::vector<float>& UndoState::getFData(size_t index)
{
  return *(fdata[index]);
}

const std::vector<float>& UndoState::getFData(size_t index) const
{
  return *(fdata[index]);
}

size_t UndoState::getNumFData() const
{
  return fdata.size();
}

////////////////////////////////////////////////////////////////////////////////


//The following primitives are always kept in memory and it is assumed no operation adds very many of them.

void UndoState::addBool(bool b) { bools.push_back(b); }
bool UndoState::getBool(size_t index) const { return bools[index]; }
size_t UndoState::getNumBool() const { return bools.size(); }

void UndoState::addInt(int i) { ints.push_back(i); }
int UndoState::getInt(size_t index) const { return ints[index]; }
size_t UndoState::getNumInt() const { return ints.size(); }

void UndoState::addDouble(double d) { doubles.push_back(d); }
double UndoState::getDouble(size_t index) const { return doubles[index]; }
size_t UndoState::getNumDouble() const { return doubles.size(); }

void UndoState::addString(const std::string& s) { strings.push_back(s); }
std::string UndoState::getString(size_t index) const { return strings[index]; }
size_t UndoState::getNumString() const { return strings.size(); }

void UndoState::clear()
{
  for(size_t i = 0; i < data.size(); i++) delete data[i];
  data.clear();
  for(size_t i = 0; i < fdata.size(); i++) delete fdata[i];
  fdata.clear();

  bools.clear();
  ints.clear();
  doubles.clear();
  strings.clear();
}

size_t UndoState::calculateMemorySize() const
{
  size_t result = 0;
  result += bools.size() * sizeof(bool);
  result += ints.size() * sizeof(int);
  result += doubles.size() * sizeof(double);
  result += strings.size() * sizeof(std::string);
  for(size_t i = 0; i < strings.size(); i++) result += strings[i].size();
  result += data.size() * sizeof(std::vector<unsigned char>*);
  for(size_t i = 0; i < data.size(); i++) result += data[i]->size();
  result += fdata.size() * sizeof(std::vector<float>*);
  for(size_t i = 0; i < fdata.size(); i++) result += fdata[i]->size() * sizeof(float);
  return result;
}


void UndoState::swap(UndoState& other)
{
  data.swap(other.data);
  fdata.swap(other.fdata);
  bools.swap(other.bools);
  ints.swap(other.ints);
  doubles.swap(other.doubles);
  strings.swap(other.strings);
}

////////////////////////////////////////////////////////////////////////////////

UndoStateIndex::UndoStateIndex()
{
  reset();
}

void UndoStateIndex::reset()
{
  idata = 0;
  ifdata = 0;
  ibool = 0;
  iint = 0;
  idouble = 0;
  istring = 0;
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
//TextureRGBA8

void storeCompleteTextureInUndo(UndoState& state, const TextureRGBA8& texture)
{
  state.addInt(texture.getU());
  state.addInt(texture.getV());
  state.addData(texture.getBuffer(), 4 * texture.getU2() * texture.getV2());
}

void readCompleteTextureFromUndo(TextureRGBA8& texture, const UndoState& state, UndoStateIndex& index)
{
  size_t u = state.getInt(index.iint++);
  size_t v = state.getInt(index.iint++);
  texture.setSize(u, v);
  const std::vector<unsigned char>& vec = state.getData(index.idata++);
  unsigned char* buffer = texture.getBuffer();
  for(size_t i = 0; i < vec.size(); i++) buffer[i] = vec[i];
  texture.update();
}

void storePartialTextureInUndo(UndoState& state, const TextureRGBA8& texture, int x0, int y0, int x1, int y1)
{
  x0 = std::min((int)texture.getU(), std::max(0, x0));
  y0 = std::min((int)texture.getV(), std::max(0, y0));
  x1 = std::min((int)texture.getU(), std::max(0, x1));
  y1 = std::min((int)texture.getV(), std::max(0, y1));

  state.addInt(x0);
  state.addInt(y0);
  state.addInt(x1);
  state.addInt(y1);

  state.addData(0, 0);
  std::vector<unsigned char>& data = state.getData(state.getNumData() - 1);
  data.resize((x1 - x0) * (y1 - y0) * 4);
  const unsigned char* buffer = texture.getBuffer();
  size_t u2 = texture.getU2();
  for(int y = 0; y < y1 - y0; y++)
  for(int x = 0; x < x1 - x0; x++)
  for(int c = 0; c < 4; c++)
  {
    size_t index = 4 * u2 * (y0 + y) + 4 * (x0 + x) + c;
    size_t index2 = 4 * (x1 - x0) * y + 4 * x + c;
    data[index2] = buffer[index];
  }
}

void readPartialTextureFromUndo(TextureRGBA8& texture, const UndoState& state, UndoStateIndex& index)
{
  int x0 = state.getInt(index.iint++);
  int y0 = state.getInt(index.iint++);
  int x1 = state.getInt(index.iint++);
  int y1 = state.getInt(index.iint++);

  const std::vector<unsigned char>& data = state.getData(index.idata++);

  size_t u2 = texture.getU2();
  unsigned char* buffer = texture.getBuffer();
  for(int y = 0; y < y1 - y0; y++)
  for(int x = 0; x < x1 - x0; x++)
  for(int c = 0; c < 4; c++)
  {
    size_t index = 4 * u2 * (y0 + y) + 4 * (x0 + x) + c;
    size_t index2 = 4 * (x1 - x0) * y + 4 * x + c;
    buffer[index] = data[index2];
  }

  texture.updatePartial(x0, y0, x1, y1);

}

void readPartialTextureFromUndo(TextureRGBA8& texture, UndoState& redo, const UndoState& state, UndoStateIndex& index)
{
  int x0 = state.getInt(index.iint++);
  int y0 = state.getInt(index.iint++);
  int x1 = state.getInt(index.iint++);
  int y1 = state.getInt(index.iint++);

  storePartialTextureInUndo(redo, texture, x0, y0, x1, y1);

  const std::vector<unsigned char>& data = state.getData(index.idata++);

  size_t u2 = texture.getU2();
  unsigned char* buffer = texture.getBuffer();
  for(int y = 0; y < y1 - y0; y++)
  for(int x = 0; x < x1 - x0; x++)
  for(int c = 0; c < 4; c++)
  {
    size_t index = 4 * u2 * (y0 + y) + 4 * (x0 + x) + c;
    size_t index2 = 4 * (x1 - x0) * y + 4 * x + c;
    buffer[index] = data[index2];
  }

  texture.updatePartial(x0, y0, x1, y1);
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
//TextureRGBA32F

void storeCompleteTextureInUndo(UndoState& state, const TextureRGBA32F& texture)
{
  state.addInt(texture.getU());
  state.addInt(texture.getV());
  state.addFData((const float*)texture.getBuffer(), 4 * texture.getU2() * texture.getV2());
}

void readCompleteTextureFromUndo(TextureRGBA32F& texture, const UndoState& state, UndoStateIndex& index)
{
  size_t u = state.getInt(index.iint++);
  size_t v = state.getInt(index.iint++);
  texture.setSize(u, v);
  const std::vector<float>& vec = state.getFData(index.ifdata++);
  float* buffer = (float*)texture.getBuffer();
  for(size_t i = 0; i < vec.size(); i++) buffer[i] = vec[i];
  texture.update();
}

void storePartialTextureInUndo(UndoState& state, const TextureRGBA32F& texture, int x0, int y0, int x1, int y1)
{
  x0 = std::min((int)texture.getU(), std::max(0, x0));
  y0 = std::min((int)texture.getV(), std::max(0, y0));
  x1 = std::min((int)texture.getU(), std::max(0, x1));
  y1 = std::min((int)texture.getV(), std::max(0, y1));

  state.addInt(x0);
  state.addInt(y0);
  state.addInt(x1);
  state.addInt(y1);

  state.addFData(0, 0);
  std::vector<float>& data = state.getFData(state.getNumFData() - 1);
  data.resize((x1 - x0) * (y1 - y0) * 4);
  const float* buffer = (const float*)texture.getBuffer();
  size_t u2 = texture.getU2();
  for(int y = 0; y < y1 - y0; y++)
  for(int x = 0; x < x1 - x0; x++)
  for(int c = 0; c < 4; c++)
  {
    size_t index = 4 * u2 * (y0 + y) + 4 * (x0 + x) + c;
    size_t index2 = 4 * (x1 - x0) * y + 4 * x + c;
    data[index2] = buffer[index];
  }
}

void readPartialTextureFromUndo(TextureRGBA32F& texture, const UndoState& state, UndoStateIndex& index)
{
  int x0 = state.getInt(index.iint++);
  int y0 = state.getInt(index.iint++);
  int x1 = state.getInt(index.iint++);
  int y1 = state.getInt(index.iint++);

  const std::vector<float>& data = state.getFData(index.ifdata++);

  size_t u2 = texture.getU2();
  float* buffer = (float*)texture.getBuffer();
  for(int y = 0; y < y1 - y0; y++)
  for(int x = 0; x < x1 - x0; x++)
  for(int c = 0; c < 4; c++)
  {
    size_t index = 4 * u2 * (y0 + y) + 4 * (x0 + x) + c;
    size_t index2 = 4 * (x1 - x0) * y + 4 * x + c;
    buffer[index] = data[index2];
  }

  texture.updatePartial(x0, y0, x1, y1);
}

void readPartialTextureFromUndo(TextureRGBA32F& texture, UndoState& redo, const UndoState& state, UndoStateIndex& index)
{
  int x0 = state.getInt(index.iint++);
  int y0 = state.getInt(index.iint++);
  int x1 = state.getInt(index.iint++);
  int y1 = state.getInt(index.iint++);

  storePartialTextureInUndo(redo, texture, x0, y0, x1, y1);

  const std::vector<float>& data = state.getFData(index.ifdata++);

  size_t u2 = texture.getU2();
  float* buffer = (float*)texture.getBuffer();
  for(int y = 0; y < y1 - y0; y++)
  for(int x = 0; x < x1 - x0; x++)
  for(int c = 0; c < 4; c++)
  {
    size_t index = 4 * u2 * (y0 + y) + 4 * (x0 + x) + c;
    size_t index2 = 4 * (x1 - x0) * y + 4 * x + c;
    buffer[index] = data[index2];
  }

  texture.updatePartial(x0, y0, x1, y1);
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
//TextureGrey8

void storeCompleteTextureInUndo(UndoState& state, const TextureGrey8& texture)
{
  state.addInt(texture.getU());
  state.addInt(texture.getV());
  state.addData((const unsigned char*)texture.getBuffer(), texture.getU2() * texture.getV2());
}

void readCompleteTextureFromUndo(TextureGrey8& texture, const UndoState& state, UndoStateIndex& index)
{
  size_t u = state.getInt(index.iint++);
  size_t v = state.getInt(index.iint++);
  texture.setSize(u, v);
  const std::vector<unsigned char>& vec = state.getData(index.idata++);
  unsigned char* buffer = (unsigned char*)texture.getBuffer();
  for(size_t i = 0; i < vec.size(); i++) buffer[i] = vec[i];
  texture.update();
}

void storePartialTextureInUndo(UndoState& state, const TextureGrey8& texture, int x0, int y0, int x1, int y1)
{
  x0 = std::min((int)texture.getU(), std::max(0, x0));
  y0 = std::min((int)texture.getV(), std::max(0, y0));
  x1 = std::min((int)texture.getU(), std::max(0, x1));
  y1 = std::min((int)texture.getV(), std::max(0, y1));

  state.addInt(x0);
  state.addInt(y0);
  state.addInt(x1);
  state.addInt(y1);

  state.addData(0, 0);
  std::vector<unsigned char>& data = state.getData(state.getNumData() - 1);
  data.resize((x1 - x0) * (y1 - y0));
  const unsigned char* buffer = (const unsigned char*)texture.getBuffer();
  size_t u2 = texture.getU2();
  for(int y = 0; y < y1 - y0; y++)
  for(int x = 0; x < x1 - x0; x++)
  {
    size_t index = u2 * (y0 + y) + (x0 + x);
    size_t index2 = (x1 - x0) * y + x;
    data[index2] = buffer[index];
  }
}

void readPartialTextureFromUndo(TextureGrey8& texture, const UndoState& state, UndoStateIndex& index)
{
  int x0 = state.getInt(index.iint++);
  int y0 = state.getInt(index.iint++);
  int x1 = state.getInt(index.iint++);
  int y1 = state.getInt(index.iint++);

  const std::vector<unsigned char>& data = state.getData(index.idata++);

  size_t u2 = texture.getU2();
  unsigned char* buffer = (unsigned char*)texture.getBuffer();
  for(int y = 0; y < y1 - y0; y++)
  for(int x = 0; x < x1 - x0; x++)
  {
    size_t index = u2 * (y0 + y) + (x0 + x);
    size_t index2 = (x1 - x0) * y + x;
    buffer[index] = data[index2];
  }

  texture.updatePartial(x0, y0, x1, y1);
}

void readPartialTextureFromUndo(TextureGrey8& texture, UndoState& redo, const UndoState& state, UndoStateIndex& index)
{
  int x0 = state.getInt(index.iint++);
  int y0 = state.getInt(index.iint++);
  int x1 = state.getInt(index.iint++);
  int y1 = state.getInt(index.iint++);

  storePartialTextureInUndo(redo, texture, x0, y0, x1, y1);

  const std::vector<unsigned char>& data = state.getData(index.idata++);

  size_t u2 = texture.getU2();
  unsigned char* buffer = (unsigned char*)texture.getBuffer();
  for(int y = 0; y < y1 - y0; y++)
  for(int x = 0; x < x1 - x0; x++)
  {
    size_t index = u2 * (y0 + y) + (x0 + x);
    size_t index2 = (x1 - x0) * y + x;
    buffer[index] = data[index2];
  }

  texture.updatePartial(x0, y0, x1, y1);
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
//TextureGrey32F

void storeCompleteTextureInUndo(UndoState& state, const TextureGrey32F& texture)
{
  state.addInt(texture.getU());
  state.addInt(texture.getV());
  state.addFData((const float*)texture.getBuffer(), texture.getU2() * texture.getV2());
}

void readCompleteTextureFromUndo(TextureGrey32F& texture, const UndoState& state, UndoStateIndex& index)
{
  size_t u = state.getInt(index.iint++);
  size_t v = state.getInt(index.iint++);
  texture.setSize(u, v);
  const std::vector<float>& vec = state.getFData(index.ifdata++);
  float* buffer = (float*)texture.getBuffer();
  for(size_t i = 0; i < vec.size(); i++) buffer[i] = vec[i];
  texture.update();
}

void storePartialTextureInUndo(UndoState& state, const TextureGrey32F& texture, int x0, int y0, int x1, int y1)
{
  x0 = std::min((int)texture.getU(), std::max(0, x0));
  y0 = std::min((int)texture.getV(), std::max(0, y0));
  x1 = std::min((int)texture.getU(), std::max(0, x1));
  y1 = std::min((int)texture.getV(), std::max(0, y1));

  state.addInt(x0);
  state.addInt(y0);
  state.addInt(x1);
  state.addInt(y1);

  state.addFData(0, 0);
  std::vector<float>& data = state.getFData(state.getNumFData() - 1);
  data.resize((x1 - x0) * (y1 - y0));
  const float* buffer = (float*)texture.getBuffer();
  size_t u2 = texture.getU2();
  for(int y = 0; y < y1 - y0; y++)
  for(int x = 0; x < x1 - x0; x++)
  {
    size_t index = u2 * (y0 + y) + (x0 + x);
    size_t index2 = (x1 - x0) * y + x;
    data[index2] = buffer[index];
  }
}

void readPartialTextureFromUndo(TextureGrey32F& texture, const UndoState& state, UndoStateIndex& index)
{
  int x0 = state.getInt(index.iint++);
  int y0 = state.getInt(index.iint++);
  int x1 = state.getInt(index.iint++);
  int y1 = state.getInt(index.iint++);

  const std::vector<float>& data = state.getFData(index.ifdata++);

  size_t u2 = texture.getU2();
  float* buffer = (float*)texture.getBuffer();
  for(int y = 0; y < y1 - y0; y++)
  for(int x = 0; x < x1 - x0; x++)
  {
    size_t index = u2 * (y0 + y) + (x0 + x);
    size_t index2 = (x1 - x0) * y + x;
    buffer[index] = data[index2];
  }

  texture.updatePartial(x0, y0, x1, y1);

}

void readPartialTextureFromUndo(TextureGrey32F& texture, UndoState& redo, const UndoState& state, UndoStateIndex& index)
{
  int x0 = state.getInt(index.iint++);
  int y0 = state.getInt(index.iint++);
  int x1 = state.getInt(index.iint++);
  int y1 = state.getInt(index.iint++);

  storePartialTextureInUndo(redo, texture, x0, y0, x1, y1);

  const std::vector<float>& data = state.getFData(index.ifdata++);

  size_t u2 = texture.getU2();
  float* buffer = (float*)texture.getBuffer();
  for(int y = 0; y < y1 - y0; y++)
  for(int x = 0; x < x1 - x0; x++)
  {
    size_t index = u2 * (y0 + y) + (x0 + x);
    size_t index2 = (x1 - x0) * y + x;
    buffer[index] = data[index2];
  }

  texture.updatePartial(x0, y0, x1, y1);
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////


void storeColorInUndo(UndoState& state, const lpi::ColorRGB& color)
{
  state.addInt(color.r);
  state.addInt(color.g);
  state.addInt(color.b);
  state.addInt(color.a);
}

lpi::ColorRGB readColorFromUndo(const UndoState& state, UndoStateIndex& index)
{
  lpi::ColorRGB result;
  result.r = state.getInt(index.iint++);
  result.g = state.getInt(index.iint++);
  result.b = state.getInt(index.iint++);
  result.a = state.getInt(index.iint++);
  return result;
}

void storeColordInUndo(UndoState& state, const lpi::ColorRGBd& color)
{
  state.addDouble(color.r);
  state.addDouble(color.g);
  state.addDouble(color.b);
  state.addDouble(color.a);
}

lpi::ColorRGBd readColordFromUndo(const UndoState& state, UndoStateIndex& index)
{
  lpi::ColorRGBd result;
  result.r = state.getDouble(index.idouble++);
  result.g = state.getDouble(index.idouble++);
  result.b = state.getDouble(index.idouble++);
  result.a = state.getDouble(index.idouble++);
  return result;
}

void storeSettingsInUndo(UndoState& state, const GlobalToolSettings& settings)
{
  storeColordInUndo(state, settings.leftColor);
  storeColordInUndo(state, settings.rightColor);
  //TODO: all the other settings!
}

void readSettingsFromUndo(GlobalToolSettings& settings, const UndoState& state, UndoStateIndex& index)
{
  settings.leftColor = readColordFromUndo(state, index);
  settings.rightColor = readColordFromUndo(state, index);
  //TODO: all the other settings!
}





////////////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////////////

UndoStack::UndoStack()
: index(0)
{
}

UndoStack::~UndoStack()
{
  for(size_t i = 0; i < states.size(); i++) delete states[i];
}

size_t UndoStack::size()
{
  return states.size();
}

size_t UndoStack::getIndex()
{
  return index;
}

UndoState* UndoStack::addNewState(IOperation* operation)
{
  //remove all states after this new operation (in other words: redo stack goes away)
  if(!states.empty())
  {
    for(size_t i = index; i < states.size(); i++) delete states[i];
    states.resize(index);
  }
  
  //make the new state
  states.push_back(new UndoState);
  states.back()->operation = operation;
  index++;
  return states.back();
}

void UndoStack::popState()
{
  if(index == 0) return;
  index--;
  states.pop_back();
}

void UndoStack::undo(Paint& paint)
{
  if(index == 0) return;
  index--;
  states[index]->operation->undo(paint, *states[index]);
  paint.updateSize();
}

void UndoStack::redo(Paint& paint)
{
  if(index >= size()) return;
  states[index]->operation->redo(paint, *states[index]);
  index++;
  paint.updateSize();
}

size_t UndoStack::calculateMemorySize() const
{
  size_t result = 0;
  for(size_t i = 0; i < states.size(); i++) result += states[i]->calculateMemorySize();
  return result;
}


void UndoStack::trimMemorySize(size_t size)
{
  if(size == 0) return; //avoid infinite loop
  
  while(calculateMemorySize() > size && states.size() > 1)
  {
    if(index < states.size() - 1)
    {
      delete states.back();
      states.resize(states.size() - 1);
    }
    else
    {
      delete states[0];
      states.erase(states.begin());
      index--;
    }
  }
}

////////////////////////////////////////////////////////////////////////////////

ToolUndoHelper::ToolUndoHelper()
{
  reset();
}

void ToolUndoHelper::reset()
{
  x0 = x1 = 0;
  y0 = y1 = 0; //by giving them same size, it knows it's not really a working bounding box yet
  startx = starty = 0;
  data.clear();
  fdata.clear();
  sides.clear();
}

/*void ToolUndoHelper::addPoint(int x0, int y0)
{
  if(x < x0) x0 = x;
  if(y < y0) y0 = y;
  if(x > x1) x1 = x;
  if(y > y1) y1 = y;
}

void ToolUndoHelper::addBounds(int x0, int y0, int x1, int y1)
{
  if(x0 < this->x0) this->x0 = x0;
  if(y0 < this->y0) this->y0 = y0;
  if(x1 > this->x1) this->x1 = x1;
  if(y1 > this->y1) this->y1 = y1;
}*/

void ToolUndoHelper::add(int ax0, int ay0, int ax1, int ay1, const TextureRGBA8& texture)
{
  mode = MODE_RGBA_8;
  
  if(x0 >= x1)
  {
    const unsigned char* buffer = texture.getBuffer();
    size_t u2 = texture.getU2();

    x0 = ax0;
    y0 = ay0;
    x1 = ax0 + 1;
    y1 = ay0 + 1;
    startx = x0;
    starty = y0;
    
    size_t index = u2 * 4 * y0 + 4 * x0;
    
    data.resize(data.size() + 1);
    data.back().push_back(buffer[index + 0]);
    data.back().push_back(buffer[index + 1]);
    data.back().push_back(buffer[index + 2]);
    data.back().push_back(buffer[index + 3]);
    sides.push_back(0); //in fact it doesn't matter what side the first pixel is...
  }
  
  while(ay0 < y0) addSide(0, texture); //up
  while(ax1 > x1) addSide(1, texture); //right
  while(ay1 > y1) addSide(2, texture); //down
  while(ax0 < x0) addSide(3, texture); //left
}

void ToolUndoHelper::addSide(int side, const TextureRGBA8& texture)  //0 = up, 1 = right, 2 = down, 3 = left
{
  const unsigned char* buffer = texture.getBuffer();
  size_t u2 = texture.getU2();
  sides.push_back(side);
  data.resize(data.size() + 1);
  std::vector<unsigned char>& vec = data.back();
  switch(side)
  {
    case 0: //up
      {
        size_t len = x1 - x0;
        vec.resize(len * 4);
        for(size_t i = 0; i < len; i++)
        for(size_t c = 0; c < 4; c++)
        {
          size_t index = 4 * u2 * (y0 - 1) + 4 * (x0 + i) + c;
          vec[i * 4 + c] = buffer[index];
        }
        y0--;
      }
      break;
    case 1: //right
      {
        size_t len = y1 - y0;
        vec.resize(len * 4);
        for(size_t i = 0; i < len; i++)
        for(size_t c = 0; c < 4; c++)
        {
          size_t index = 4 * u2 * (y0 + i) + 4 * x1 + c;
          vec[i * 4 + c] = buffer[index];
        }
        x1++;
      }
      break;
    case 2: //down
      {
        size_t len = x1 - x0;
        vec.resize(len * 4);
        for(size_t i = 0; i < len; i++)
        for(size_t c = 0; c < 4; c++)
        {
          size_t index = 4 * u2 * y1 + 4 * (x0 + i) + c;
          vec[i * 4 + c] = buffer[index];
        }
        y1++;
      }
      break;
    case 3: //left
      {
        size_t len = y1 - y0;
        vec.resize(len * 4);
        for(size_t i = 0; i < len; i++)
        for(size_t c = 0; c < 4; c++)
        {
          size_t index = 4 * u2 * (y0 + i) + 4 * (x0 - 1) + c;
          vec[i * 4 + c] = buffer[index];
        }
        x0--;
      }
      break;
  }
}

void ToolUndoHelper::add(int ax0, int ay0, int ax1, int ay1, const TextureGrey8& texture)
{
  mode = MODE_GREY_8;
  
  if(x0 >= x1)
  {
    const unsigned char* buffer = texture.getBuffer();
    size_t u2 = texture.getU2();

    x0 = ax0;
    y0 = ay0;
    x1 = ax0 + 1;
    y1 = ay0 + 1;
    startx = x0;
    starty = y0;

    size_t index = u2 * y0 + x0;

    data.resize(data.size() + 1);
    data.back().push_back(buffer[index]);
    sides.push_back(0); //in fact it doesn't matter what side the first pixel is...
  }

  while(ay0 < y0) addSide(0, texture); //up
  while(ax1 > x1) addSide(1, texture); //right
  while(ay1 > y1) addSide(2, texture); //down
  while(ax0 < x0) addSide(3, texture); //left
}

void ToolUndoHelper::addSide(int side, const TextureGrey8& texture)  //0 = up, 1 = right, 2 = down, 3 = left
{
  const unsigned char* buffer = texture.getBuffer();
  size_t u2 = texture.getU2();
  sides.push_back(side);
  data.resize(data.size() + 1);
  std::vector<unsigned char>& vec = data.back();
  switch(side)
  {
    case 0: //up
      {
        size_t len = x1 - x0;
        vec.resize(len);
        for(size_t i = 0; i < len; i++)
        {
          size_t index = u2 * (y0 - 1) + (x0 + i);
          vec[i] = buffer[index];
        }
        y0--;
      }
      break;
    case 1: //right
      {
        size_t len = y1 - y0;
        vec.resize(len);
        for(size_t i = 0; i < len; i++)
        {
          size_t index = u2 * (y0 + i) + x1;
          vec[i] = buffer[index];
        }
        x1++;
      }
      break;
    case 2: //down
      {
        size_t len = x1 - x0;
        vec.resize(len);
        for(size_t i = 0; i < len; i++)
        {
          size_t index = u2 * y1 + (x0 + i);
          vec[i] = buffer[index];
        }
        y1++;
      }
      break;
    case 3: //left
      {
        size_t len = y1 - y0;
        vec.resize(len);
        for(size_t i = 0; i < len; i++)
        {
          size_t index = u2 * (y0 + i) + (x0 - 1);
          vec[i] = buffer[index];
        }
        x0--;
      }
      break;
  }
}

void ToolUndoHelper::add(int ax0, int ay0, int ax1, int ay1, const TextureRGBA32F& texture)
{
  mode = MODE_RGBA_32F;
  
  if(x0 >= x1)
  {
    const float* buffer = texture.getBuffer();
    size_t u2 = texture.getU2();

    x0 = ax0;
    y0 = ay0;
    x1 = ax0 + 1;
    y1 = ay0 + 1;
    startx = x0;
    starty = y0;
    
    size_t index = u2 * 4 * y0 + 4 * x0;
    
    fdata.resize(fdata.size() + 1);
    fdata.back().push_back(buffer[index + 0]);
    fdata.back().push_back(buffer[index + 1]);
    fdata.back().push_back(buffer[index + 2]);
    fdata.back().push_back(buffer[index + 3]);
    sides.push_back(0); //in fact it doesn't matter what side the first pixel is...
  }
  
  while(ay0 < y0) addSide(0, texture); //up
  while(ax1 > x1) addSide(1, texture); //right
  while(ay1 > y1) addSide(2, texture); //down
  while(ax0 < x0) addSide(3, texture); //left
}

void ToolUndoHelper::addSide(int side, const TextureRGBA32F& texture)  //0 = up, 1 = right, 2 = down, 3 = left
{
  const float* buffer = texture.getBuffer();
  size_t u2 = texture.getU2();
  sides.push_back(side);
  fdata.resize(fdata.size() + 1);
  std::vector<float>& vec = fdata.back();
  switch(side)
  {
    case 0: //up
      {
        size_t len = x1 - x0;
        vec.resize(len * 4);
        for(size_t i = 0; i < len; i++)
        for(size_t c = 0; c < 4; c++)
        {
          size_t index = 4 * u2 * (y0 - 1) + 4 * (x0 + i) + c;
          vec[i * 4 + c] = buffer[index];
        }
        y0--;
      }
      break;
    case 1: //right
      {
        size_t len = y1 - y0;
        vec.resize(len * 4);
        for(size_t i = 0; i < len; i++)
        for(size_t c = 0; c < 4; c++)
        {
          size_t index = 4 * u2 * (y0 + i) + 4 * x1 + c;
          vec[i * 4 + c] = buffer[index];
        }
        x1++;
      }
      break;
    case 2: //down
      {
        size_t len = x1 - x0;
        vec.resize(len * 4);
        for(size_t i = 0; i < len; i++)
        for(size_t c = 0; c < 4; c++)
        {
          size_t index = 4 * u2 * y1 + 4 * (x0 + i) + c;
          vec[i * 4 + c] = buffer[index];
        }
        y1++;
      }
      break;
    case 3: //left
      {
        size_t len = y1 - y0;
        vec.resize(len * 4);
        for(size_t i = 0; i < len; i++)
        for(size_t c = 0; c < 4; c++)
        {
          size_t index = 4 * u2 * (y0 + i) + 4 * (x0 - 1) + c;
          vec[i * 4 + c] = buffer[index];
        }
        x0--;
      }
      break;
  }
}

void ToolUndoHelper::add(int ax0, int ay0, int ax1, int ay1, const TextureGrey32F& texture)
{
  mode = MODE_GREY_32F;
  
  if(x0 >= x1)
  {
    const float* buffer = texture.getBuffer();
    size_t u2 = texture.getU2();

    x0 = ax0;
    y0 = ay0;
    x1 = ax0 + 1;
    y1 = ay0 + 1;
    startx = x0;
    starty = y0;

    size_t index = u2 * y0 + x0;

    fdata.resize(fdata.size() + 1);
    fdata.back().push_back(buffer[index]);
    sides.push_back(0); //in fact it doesn't matter what side the first pixel is...
  }

  while(ay0 < y0) addSide(0, texture); //up
  while(ax1 > x1) addSide(1, texture); //right
  while(ay1 > y1) addSide(2, texture); //down
  while(ax0 < x0) addSide(3, texture); //left
}

void ToolUndoHelper::addSide(int side, const TextureGrey32F& texture)  //0 = up, 1 = right, 2 = down, 3 = left
{
  const float* buffer = texture.getBuffer();
  size_t u2 = texture.getU2();
  sides.push_back(side);
  fdata.resize(fdata.size() + 1);
  std::vector<float>& vec = fdata.back();
  switch(side)
  {
    case 0: //up
      {
        size_t len = x1 - x0;
        vec.resize(len);
        for(size_t i = 0; i < len; i++)
        {
          size_t index = u2 * (y0 - 1) + (x0 + i);
          vec[i] = buffer[index];
        }
        y0--;
      }
      break;
    case 1: //right
      {
        size_t len = y1 - y0;
        vec.resize(len);
        for(size_t i = 0; i < len; i++)
        {
          size_t index = u2 * (y0 + i) + x1;
          vec[i] = buffer[index];
        }
        x1++;
      }
      break;
    case 2: //down
      {
        size_t len = x1 - x0;
        vec.resize(len);
        for(size_t i = 0; i < len; i++)
        {
          size_t index = u2 * y1 + (x0 + i);
          vec[i] = buffer[index];
        }
        y1++;
      }
      break;
    case 3: //left
      {
        size_t len = y1 - y0;
        vec.resize(len);
        for(size_t i = 0; i < len; i++)
        {
          size_t index = u2 * (y0 + i) + (x0 - 1);
          vec[i] = buffer[index];
        }
        x0--;
      }
      break;
  }
}

////////////////////////////////////////////////////////////////////////////////


void ToolUndoHelper::addToUndo(UndoState& state)
{
  if(mode == MODE_RGBA_8)
  {
    size_t vu = x1-x0;
    size_t vv = y1-y0;
    std::vector<unsigned char> part(vu*vv*4);
    int px = x0;
    int py = y0;
    x0 = startx;
    y0 = starty;
    x1 = x0 + 1;
    y1 = y0 + 1;
    for(size_t i = 0; i < data.size(); i++)
    {
      std::vector<unsigned char>& vec = data[i];
      int side = sides[i];

      if(i == 0)
      {
        for(size_t c = 0; c < 4; c++)
        {
          size_t index = 4 * vu * (starty - py) + 4 * (startx - px) + c;
          part[index] = vec[c];
        }
        continue;
      }

      switch(side)
      {
        case 0: //up
          {
            size_t len = x1 - x0;
            for(size_t i = 0; i < len; i++)
            for(size_t c = 0; c < 4; c++)
            {
              size_t index = 4 * vu * (y0 - 1 - py) + 4 * (x0  + i - px) + c;
              part[index] = vec[i * 4 + c];
            }
            y0--;
          }
          break;
        case 1: //right
          {
            size_t len = y1 - y0;
            for(size_t i = 0; i < len; i++)
            for(size_t c = 0; c < 4; c++)
            {
              size_t index = 4 * vu * (y0 + i - py) + 4 * (x1 - px) + c;
              part[index] = vec[i * 4 + c];
            }
            x1++;
          }
          break;
        case 2: //down
          {
            size_t len = x1 - x0;
            for(size_t i = 0; i < len; i++)
            for(size_t c = 0; c < 4; c++)
            {
              size_t index = 4 * vu * (y1 - py) + 4 * (x0  + i - px) + c;
              part[index] = vec[i * 4 + c];
            }
            y1++;
          }
          break;
        case 3: //left
          {
            size_t len = y1 - y0;
            for(size_t i = 0; i < len; i++)
            for(size_t c = 0; c < 4; c++)
            {
              size_t index = 4 * vu * (y0 + i - py) + 4 * (x0  - 1 - px) + c;
              part[index] = vec[i * 4 + c];
            }
            x0--;
          }
          break;
      }
    }

    state.addInt(x0);
    state.addInt(y0);
    state.addInt(x1);
    state.addInt(y1);
    state.addData(part);
  }
  else if(mode == MODE_GREY_8)
  {
    size_t vu = x1-x0;
    size_t vv = y1-y0;
    std::vector<unsigned char> part(vu*vv);
    int px = x0;
    int py = y0;
    x0 = startx;
    y0 = starty;
    x1 = x0 + 1;
    y1 = y0 + 1;
    for(size_t i = 0; i < data.size(); i++)
    {
      std::vector<unsigned char>& vec = data[i];
      int side = sides[i];

      if(i == 0)
      {
        size_t index = vu * (starty - py) + (startx - px);
        part[index] = vec[0];
        continue;
      }

      switch(side)
      {
        case 0: //up
          {
            size_t len = x1 - x0;
            for(size_t i = 0; i < len; i++)
            {
              size_t index = vu * (y0 - 1 - py) + (x0  + i - px);
              part[index] = vec[i];
            }
            y0--;
          }
          break;
        case 1: //right
          {
            size_t len = y1 - y0;
            for(size_t i = 0; i < len; i++)
            {
              size_t index = vu * (y0 + i - py) + (x1 - px);
              part[index] = vec[i];
            }
            x1++;
          }
          break;
        case 2: //down
          {
            size_t len = x1 - x0;
            for(size_t i = 0; i < len; i++)
            {
              size_t index = vu * (y1 - py) + (x0  + i - px);
              part[index] = vec[i];
            }
            y1++;
          }
          break;
        case 3: //left
          {
            size_t len = y1 - y0;
            for(size_t i = 0; i < len; i++)
            {
              size_t index = vu * (y0 + i - py) + (x0  - 1 - px);
              part[index] = vec[i];
            }
            x0--;
          }
          break;
      }
    }

    state.addInt(x0);
    state.addInt(y0);
    state.addInt(x1);
    state.addInt(y1);
    state.addData(part);
  }
  else if(mode == MODE_RGBA_32F)
  {
    size_t vu = x1-x0;
    size_t vv = y1-y0;
    std::vector<float> part(vu*vv*4);
    int px = x0;
    int py = y0;
    x0 = startx;
    y0 = starty;
    x1 = x0 + 1;
    y1 = y0 + 1;
    for(size_t i = 0; i < fdata.size(); i++)
    {
      std::vector<float>& vec = fdata[i];
      int side = sides[i];

      if(i == 0)
      {
        for(size_t c = 0; c < 4; c++)
        {
          size_t index = 4 * vu * (starty - py) + 4 * (startx - px) + c;
          part[index] = vec[c];
        }
        continue;
      }

      switch(side)
      {
        case 0: //up
          {
            size_t len = x1 - x0;
            for(size_t i = 0; i < len; i++)
            for(size_t c = 0; c < 4; c++)
            {
              size_t index = 4 * vu * (y0 - 1 - py) + 4 * (x0  + i - px) + c;
              part[index] = vec[i * 4 + c];
            }
            y0--;
          }
          break;
        case 1: //right
          {
            size_t len = y1 - y0;
            for(size_t i = 0; i < len; i++)
            for(size_t c = 0; c < 4; c++)
            {
              size_t index = 4 * vu * (y0 + i - py) + 4 * (x1 - px) + c;
              part[index] = vec[i * 4 + c];
            }
            x1++;
          }
          break;
        case 2: //down
          {
            size_t len = x1 - x0;
            for(size_t i = 0; i < len; i++)
            for(size_t c = 0; c < 4; c++)
            {
              size_t index = 4 * vu * (y1 - py) + 4 * (x0  + i - px) + c;
              part[index] = vec[i * 4 + c];
            }
            y1++;
          }
          break;
        case 3: //left
          {
            size_t len = y1 - y0;
            for(size_t i = 0; i < len; i++)
            for(size_t c = 0; c < 4; c++)
            {
              size_t index = 4 * vu * (y0 + i - py) + 4 * (x0  - 1 - px) + c;
              part[index] = vec[i * 4 + c];
            }
            x0--;
          }
          break;
      }
    }

    state.addInt(x0);
    state.addInt(y0);
    state.addInt(x1);
    state.addInt(y1);
    state.addFData(part);
  }
  else if(mode == MODE_GREY_32F)
  {
    size_t vu = x1-x0;
    size_t vv = y1-y0;
    std::vector<float> part(vu*vv);
    int px = x0;
    int py = y0;
    x0 = startx;
    y0 = starty;
    x1 = x0 + 1;
    y1 = y0 + 1;
    for(size_t i = 0; i < fdata.size(); i++)
    {
      std::vector<float>& vec = fdata[i];
      int side = sides[i];

      if(i == 0)
      {
        size_t index = vu * (starty - py) + (startx - px);
        part[index] = vec[0];
        continue;
      }

      switch(side)
      {
        case 0: //up
          {
            size_t len = x1 - x0;
            for(size_t i = 0; i < len; i++)
            {
              size_t index = vu * (y0 - 1 - py) + (x0  + i - px);
              part[index] = vec[i];
            }
            y0--;
          }
          break;
        case 1: //right
          {
            size_t len = y1 - y0;
            for(size_t i = 0; i < len; i++)
            {
              size_t index = vu * (y0 + i - py) + (x1 - px);
              part[index] = vec[i];
            }
            x1++;
          }
          break;
        case 2: //down
          {
            size_t len = x1 - x0;
            for(size_t i = 0; i < len; i++)
            {
              size_t index = vu * (y1 - py) + (x0  + i - px);
              part[index] = vec[i];
            }
            y1++;
          }
          break;
        case 3: //left
          {
            size_t len = y1 - y0;
            for(size_t i = 0; i < len; i++)
            {
              size_t index = vu * (y0 + i - py) + (x0  - 1 - px);
              part[index] = vec[i];
            }
            x0--;
          }
          break;
      }
    }

    state.addInt(x0);
    state.addInt(y0);
    state.addInt(x1);
    state.addInt(y1);
    state.addFData(part);
  }
}

////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////

