/*
LodePaint

Copyright (c) 2009-2010 Lode Vandevenne
All rights reserved.

This file is part of LodePaint.

LodePaint is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LodePaint is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LodePaint.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "lpi/lpi_color.h"
#include "lpi/gui/lpi_gui_file.h"
#include "lpi/os/lpi_persist.h"
#include "lpi/gl/lpi_gl_context.h"

struct CMDOptions
{
  bool override_fullscreen;
  bool fullscreen;
  bool override_resizable;
  bool resizable;
  
  CMDOptions();
};

struct Brush
{
  int size; //diameter in case of circle
  double step;
  double opacity;
  double softness; //0 = all the same color, 1 = softest
  //bool shape; //TODO make this an enum or so. currently, false is round, true is square
  enum Shape
  {
    SHAPE_ROUND,
    SHAPE_SQUARE,
    SHAPE_DIAMOND
    //TODO: hor line, ver line, \, /
  };

  Shape shape;

  Brush();
};

struct Shape //settings for geometric shapes (line shapes, filled shapes, ...)
{
  /*
  Note: colors aren't in here, the regular left and right color are used.
  Here's how the colors are used:
  -if only line is enabled, line has mouse button color
  -if only fill is enabled, fill has mouse button color
  -if both line and fill are enabled, and if it's a shape that can have both (so NOT if it's a polyline or so), then line has the mouse color, fill has the color of the OPPOSITE mouse button.
  */

  //Line part
  bool line_enabled;
  int line_thickness;
  double line_opacity;

  //Fill part
  bool fill_enabled;
  double fill_opacity;

  Shape();
};

enum SelectionBGType //background color used when turning the selection into floating selection to drag it away
{
  BG_BG, //the bg color selected with color picker
  BG_FG, //the fg color selected with color picker
  BG_TRANSPARENT, //alpha 0
  BG_AUTO //it looks at the pixel 1 pixel outside each corner of the selection rectangle. The one that occurs the most, is used as background color.
};

struct Other //other global settings
{
  //this setting belongs to selection tools. It's shared here because it's used by paintwindow too...
  bool alpha_as_opacity;
  
  bool affect_alpha; //shared by all tools
  
  SelectionBGType selection_bg_type;

  Other();
};

/*
Settings are global variables in the painting program, shared by all tools. For
example the left and right color. These are not copied to every brush and tool and
such, but instead they all refer to these shared settings.
*/
struct GlobalToolSettings
{
  lpi::ColorRGBd leftColor;
  lpi::ColorRGBd rightColor;
  
  lpi::ColorRGB leftColor255() const { return lpi::RGBdtoRGB(leftColor); }
  lpi::ColorRGB rightColor255() const { return lpi::RGBdtoRGB(rightColor); }
  
  Brush brush; //the standard brush shape, used by tools that use and want to use such shared shape (e.g. NOT by the pen because that one is always one-pixel)
  Shape shape;
  Other other;
  
  int crosshairMode; //crosshair or mouse at location of mouse pointer. 0: nothing. 1: small crosshair black & white. 2: huge crosshair black. 3: huge crosshair white.
  
  GlobalToolSettings(/*lpi::GLContext* context*/);
};

struct DynamicBrushChange //what alt+scrollwheel affects
{
  enum Change
  {
    C_NOTHING, //change nothing
    //brush
    C_BRUSH_SIZE,
    C_BRUSH_OPACITY,
    C_BRUSH_SOFTNESS,
    //foreground color
    C_FG_HUE,
    C_FG_SATURATION,
    C_FG_BRIGHTNESS,
    C_FG_ALPHA,
    //background color
    C_BG_HUE,
    C_BG_SATURATION,
    C_BG_BRIGHTNESS,
    C_BG_ALPHA,
    //...
    C_NUM_ENUM //don't use except to count number of elements in this enum
  };

  //it can change two settings at the same time if wanted
  Change change1;
  double amount1;
  Change change2;
  double amount2;
  
  DynamicBrushChange();
};

struct Paths
{
  std::string exedir; //directory where the exe of the program is
  std::string datadir; //directory where icons_toolbar.png, icons_tools.png and gui.png are
  std::string plugindir; //directory where it'll scan for plugins at startup
  //std::string settingsdir; //directory where the settings.txt XML file is (e.g. ~/.config/LodePaint/) --> commented out because lpi::persist handles this instead
};

struct PathsOverride
{
  std::string plugindir;
};

struct Options
{
  int screenWidth; //in pixels
  int screenHeight; //in pixels
  int undoSize; //in MB
  int fullscreen;
  int resizable;
  lpi::ColorRGB bgcolor; //background color of the program, has nothing to do with the painting
  lpi::ColorRGB drawingWindowColor;
  lpi::ColorRGB windowTopColor;
  int newImageSizeX;
  int newImageSizeY;
  Paths paths;
  PathsOverride pathsOverride;
  std::string command; //command with which LodePaint was started by the shell or OS
  std::string skin; //subdirectory name of the current GUI skin (e.g. "default")
  std::string language;
  
  //paintwindow options
  int bgpattern;
  int maskColorIndex;
  int pixelGridIndex; //0=none, 1=1 pixel, 2 = custom option
  int pixelGridCustomWidth;
  int pixelGridCustomHeight;
  
  //GUI state
  int selectedColorWindowPalette;
  
  DynamicBrushChange dyn;
  
  bool useOSNativeFileDialog; //if 1, use native file dialog. If 0, use built-in OpenGL lpi file dialog. Only supported for operating systems for which their native file dialog was implemented in LodePaint (e.g. the win32 file dialog). Others always use the built-in file dialog.

  Options();
};


void writeColorToPersist(lpi::Persist& persist, const lpi::ColorRGB& color, const std::string& name);
void readColorFromPersist(lpi::ColorRGB& color, const lpi::Persist& persist, const std::string& name, const lpi::ColorRGB& defaultColor);
void writeColorToPersist(lpi::Persist& persist, const lpi::ColorRGBd& color, const std::string& name);
void readColorFromPersist(lpi::ColorRGBd& color, const lpi::Persist& persist, const std::string& name, const lpi::ColorRGBd& defaultColor);
void writePaintSettingsToPersist(lpi::Persist& persist, const GlobalToolSettings& globalToolSettings, const std::string& name);
void readPaintSettingsFromPersist(GlobalToolSettings& globalToolSettings, const lpi::Persist& persist, const std::string& name);
void writeOptionsToPersist(lpi::Persist& persist, const Options& options, const std::string& name);
void readOptionsFromPersist(Options& options, const lpi::Persist& persist, const std::string& name);


void optionsLaterToOptionsNow(Options& now, Options& later);
void cmdoptionsToOptions(Options& options, const CMDOptions& cmdoptions);


////////////////////////////////////////////////////////////////////////////////

void recentFilesToPersist(lpi::Persist& persist, const lpi::gui::RecentFiles& recent, const std::string& name);
void persistToRecentFiles(lpi::gui::RecentFiles& recent, const lpi::Persist& persist, const std::string& name);
void fileDialogToPersist(lpi::Persist& persist, const lpi::gui::FileDialogPersist& fd, const std::string& name);
void persistToFileDialog(lpi::gui::FileDialogPersist& fd, const lpi::Persist& persist, const std::string& name);
