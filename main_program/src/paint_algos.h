/*
LodePaint

Copyright (c) 2009-2010 Lode Vandevenne
All rights reserved.

This file is part of LodePaint.

LodePaint is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LodePaint is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LodePaint.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "paint_settings.h"
#include "lpi/lpi_color.h"

//forward declarations
class TextureRGBA8;
class TextureRGBA32F;

namespace lpi
{
  class Vector2;
  class ITexture;
}

////////////////////////////////////////////////////////////////////////////////

/*
Some of these functions are similar to those found in lpi_draw2d_buffer, but there
is a difference: in lpi_2d_buffer, alpha channel is seen as opacity. Here it is not,
e.g. if you draw a pixel with alpha channel 0 here, it makes the alpha channel equal to 0
there on the painting (while in lpi_2d_buffer it does nothing)
*/



void pset(unsigned char* buffer, int u, int v, int u2, int x, int y, const lpi::ColorRGB& color, double opacity, bool affect_alpha, bool wrapping = false);
void horLine(unsigned char* buffer, int u, int v, int u2, int y, int x1, int x2, const lpi::ColorRGB& color, double opacity, bool affect_alpha, bool wrapping = false);
void verLine(unsigned char* buffer, int u, int v, int u2, int x, int y1, int y2, const lpi::ColorRGB& color, double opacity, bool affect_alpha, bool wrapping = false);
void drawLine(unsigned char* buffer, int buffer_w, int buffer_h, int x0, int y0, int x1, int y1, int thickness, const lpi::ColorRGB& color, double opacity, bool affect_alpha, bool include_last_pixel);
void drawThickLine(unsigned char* buffer, int u, int v, int u2, int x0, int y0, int x1, int y1, int thickness, const lpi::ColorRGB& color, double opacity, bool affect_alpha, bool wrapping);



void psetGrey8(unsigned char* buffer, int u, int v, int u2, int x, int y, int color, double opacity, bool wrapping = false);
void pset(float* buffer, int u, int v, int u2, int x, int y, const lpi::ColorRGBd& color, double opacity, bool affect_alpha, bool wrapping = false);
void psetGrey32f(float* buffer, int u, int v, int u2, int x, int y, float color, double opacity, bool wrapping = false);


void scale2x(unsigned char* out, size_t ou, size_t ov, const unsigned char* in, size_t iu, size_t iv, bool wrapping); //Scale2X pixel scaling algorithm. Output image must be large enough or larger than 2x input image.
void scale3x(unsigned char* out, size_t ou, size_t ov, const unsigned char* in, size_t iu, size_t iv, bool wrapping); //Scale3X pixel scaling algorithm. Output image must be large enough or larger than 3x input image.
//hq2x is in other file

void shiftImageSlow(TextureRGBA8& texture, int shiftx, int shifty);
void shiftImageSlow(TextureRGBA32F& texture, int shiftx, int shifty);


