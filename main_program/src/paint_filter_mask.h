/*
LodePaint

Copyright (c) 2009-2010 Lode Vandevenne
All rights reserved.

This file is part of LodePaint.

LodePaint is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LodePaint is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LodePaint.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

//filters related to mask and selection

#include "paint_filter_image.h"

class OperationClearMask : public AOperationFilterUtil
{
  protected:
    virtual bool worksOnMask() {return true;}
    virtual void doit(TextureGrey8& texture, Progress& progress);
    virtual void doit(TextureGrey32F& texture, Progress& progress);
    virtual bool supportsColorMode(ColorMode mode) const { return sup(mode, true, true, false, false); }
  public:
    OperationClearMask(GlobalToolSettings& settings)
    : AOperationFilterUtil(settings, true, true)
    {
    }

    virtual std::string getLabel() const { return "Clear Mask"; }
};

class OperationClearMaskToValue : public AOperationFilterUtil
{
  private:

    double value;

    DynamicPageWithAutoUpdate dialog;

  protected:
    virtual bool worksOnMask() {return true;}
    virtual void doit(TextureGrey8& texture, Progress& progress);
    virtual void doit(TextureGrey32F& texture, Progress& progress);
  public:
    OperationClearMaskToValue(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom);

    virtual lpi::gui::Element* getDialog() { return &dialog; }
    virtual std::string getLabel() const { return "Clear Mask To Value"; }
    virtual std::string getHelp() { return "[OperationClearMaskToValue Help]"; }
};

class OperationNegateMask : public OperationNegateRGB
{
  protected:
    virtual bool worksOnMask() {return true;}
  public:
    OperationNegateMask(GlobalToolSettings& settings)
    : OperationNegateRGB(settings)
    {
    }

    virtual std::string getLabel() const { return "Invert Mask"; }
};


class OperationSwapMask : public AOperationFilterBase
{
  public:
    OperationSwapMask(GlobalToolSettings& settings) : AOperationFilterBase(settings) {}
    virtual bool operate(Paint& paint, UndoState& state, Progress& progress);
    virtual bool operateGL(Paint& paint, UndoState& state);

    virtual void undo(Paint& paint, UndoState& state);
    virtual void redo(Paint& paint, UndoState& state);

    virtual std::string getLabel() const { return "Swap Mask With Alpha"; }
};

class OperationChannelToMask : public AOperationFilterBase //multiplies the mask with the chosen channel
{
  private:

    enum Channel
    {
      C_A, //alpha
      C_H, //hue
      C_S, //saturation
      C_L, //lightness
      C_R,
      C_G,
      C_B
    };

    Channel channel;
    double hue; //only used when channel is C_H
    double huedist; //distance from base hue

    DynamicPageWithAutoUpdate dialog;

  public:
    OperationChannelToMask(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom);
    virtual bool operate(Paint& paint, UndoState& state, Progress& progress);

    virtual void undo(Paint& paint, UndoState& state);
    virtual void redo(Paint& paint, UndoState& state);

    virtual lpi::gui::Element* getDialog() { return &dialog; }
    virtual std::string getLabel() const { return "Channel To Mask"; }
    virtual std::string getHelp() { return "[OperationChannelToMask Help]"; }
};

class OperationHueToMask : public AOperationFilterBase //multiplies the mask with the chosen channel
{
  private:

    DynamicPageWithAutoUpdate dialog;

  public:
    OperationHueToMask(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom);
    virtual bool operate(Paint& paint, UndoState& state, Progress& progress);

    virtual void undo(Paint& paint, UndoState& state);
    virtual void redo(Paint& paint, UndoState& state);

    virtual lpi::gui::Element* getDialog() { return &dialog; }
    virtual std::string getLabel() const { return "Hue To Mask"; }
};

class OperationSelectionToMask : public AOperationFilterBase //makes parts inside the selection transparent in the mask. Other mask values aren't touched.
{
  public:
    OperationSelectionToMask(GlobalToolSettings& settings) : AOperationFilterBase(settings) {}
    virtual bool operate(Paint& paint, UndoState& state, Progress& progress);

    virtual void undo(Paint& paint, UndoState& state);
    virtual void redo(Paint& paint, UndoState& state);

    virtual std::string getLabel() const { return "Selection To Mask"; }
};

class OperationFeatherMask : public AOperationFilterUtil
{
  private:

    double radius;
    bool wrapping;

    DynamicPageWithAutoUpdate dialog;

  protected:
    virtual bool worksOnMask() {return true;}
    virtual void doit(TextureGrey8& texture, Progress& progress);
  public:
    OperationFeatherMask(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom);
    
    virtual lpi::gui::Element* getDialog() { return &dialog; }
    virtual std::string getLabel() const { return "Feather"; }
    virtual std::string getHelp() { return "[OperationFeatherMask Help]"; }
};

class OperationPasteAsSelection : public AOperationFilterBase
{
  private:
  
    PaintWindow* pw;

  public:
    OperationPasteAsSelection(GlobalToolSettings& settings)
    : AOperationFilterBase(settings)
    , pw(0)
    {
    }
    virtual bool operate(Paint& paint, UndoState& state, Progress& progress);
    virtual bool operateGL(Paint& paint, UndoState& state);

    virtual void undo(Paint& paint, UndoState& state);
    virtual void redo(Paint& paint, UndoState& state);

    virtual std::string getLabel() const { return "Paste As Selection"; }
    virtual lpi::gui::Element* getDialog() { return 0; }
    
    void setPaintWindow(PaintWindow* pw);
};
