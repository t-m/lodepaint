/*
LodePaint

Copyright (c) 2009-2018 Lode Vandevenne
All rights reserved.

This file is part of LodePaint.

LodePaint is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LodePaint is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LodePaint.  If not, see <http://www.gnu.org/licenses/>.
*/


//Linux release compile:
//g++ src/*.cpp src/lpi/*.cpp src/lpi/gio/*.cpp src/lpi/gl/*.cpp src/lpi/gui/*.cpp src/lpi/os/*.cpp src/lpi/sdl/*.cpp src/imalg/*.cpp src/imfmt/*.cpp -o lodepaint -lSDL -lGL -ldl -lX11 -W -Wall -Wextra -pedantic -ansi -Wno-misleading-indentation -Wno-unused-function -O3 -s

//debug compile:
//g++ src/*.cpp src/lpi/*.cpp src/lpi/gio/*.cpp src/lpi/gl/*.cpp src/lpi/gui/*.cpp src/lpi/os/*.cpp src/lpi/sdl/*.cpp src/imalg/*.cpp src/imfmt/*.cpp -o lodepaint -lSDL -lGL -ldl -lX11 -W -Wall -Wextra -pedantic -ansi -Wno-misleading-indentation -Wno-unused-function -g3

//Recursief alle .svn directories deleten:
//rm -rf `find . -type d -name .svn`

/*
commands for developing in Linux:

make DESTDIR=/tmp/lodepaint LODEPAINTDATADIR=/tmp/lodepaint DEBUG=1

make DESTDIR=/tmp/lodepaint LODEPAINTDATADIR=/tmp/lodepaint DEBUG=1 | grep -v g++

make DESTDIR=/tmp/lodepaint LODEPAINTDATADIR=/tmp/lodepaint install

make clean
*/


#include "main.h"



#include "lpi/lpi_math.h"
#include "lpi/lpi_os.h"
#include "lpi/lpi_pvector.h"
#include "lpi/lpi_tools.h"
#include "lpi/lpi_translate.h"
#include "lpi/gui/lpi_gui_drawer_gl.h"
#include "lpi/os/lpi_filebrowse.h"
#include "lpi/os/lpi_persist.h"
#include "lpi/sdl/lpi_time.h"

#include "imalg/contour.h"
#include "imalg/progress.h"
#include "imalg/util.h"

#include "imfmt/imageformats.h"

#include "paint_clipboard.h"
#include "paint_cmd.h"
#include "paint_file.h"
#include "paint_filter.h"
#include "paint_filter_image.h"
#include "paint_filter_mask.h"
#include "paint_gui.h"
#include "paint_gui_items.h"
#include "paint_gui_main.h"
#include "paint_imageformats.h"
#include "paint_tool.h"
#include "paint_tool_other.h"
#include "paint_window.h"

#include <cmath>
#include <sstream>
#include <iostream>

#include <GL/gl.h> //for warning about generic driver


#ifdef _WIN32
#include <windows.h>
#endif

GlobalToolSettings globalToolSettings;
Options options;
Options optionsLater; //these are options for the next startup of the program (such as resolution, which can't change immediatly)
CMDOptions cmdoptions;
ITool* tool = 0;

static bool doOnAll = false;

////////////////////////////////////////////////////////////////////////////////

void waitForProgressBar(MainProgram& p, Progress& progress)
{
  MyModalFrameHandler& frame = *p.modalFrameHandler;

  int x0, y0, x1, y1;
  frame.getScreenSize(x0, y0, x1, y1);

  lpi::gui::ProgressBarDialog dialog(frame.getDrawer());
  dialog.moveCenterTo((x0+x1)/2, (y0+y1)/2);
  dialog.setElementOver(false);

  double startTime = p.guidrawer->getInput().getSeconds();


  for(;;)
  {

    if(p.guidrawer->getInput().getSeconds() - startTime < 0.3) //don't flicker screen if filter doesn't take long
    {
      SDL_Delay(50);
    }
    else
    {
      frame.doFrame();

      if(dialog.done())
      {
        progress.setUserCancel(true);
      }

      dialog.setProgress(progress.getProgress());

      frame.getScreenSize(x0, y0, x1, y1); //gotten again every frame in case size changed
      dialog.handle(frame.getDrawer().getInput());
      p.c->draw(frame.getDrawer());
      frame.getDrawer().drawRectangle(x0, y0, x1, y1, lpi::ColorRGB(0,0,0,128), true); //"modal darkness"
      dialog.draw(frame.getDrawer());
    }

    if(progress.getStatus() == S_DONE || progress.getStatus() == S_KILLED)
    {
      break;
    }
  }
}


////////////////////////////////////////////////////////////////////////////////

void getStandardPaintingArea(int& x0, int& y0, int& x1, int& y1, const MainProgram& p)
{
  //x0 = 4;
  x0 = 304;
  y0 = p.toolbar2.getY1() + 2;
  //x1 = p.optionsNow.screenWidth - 304;
  x1 = p.optionsNow.screenWidth - 4;
  y1 = p.optionsNow.screenHeight - 20;
}

void fitWindowAroundPainting(PaintWindow& window, const MainProgram& p)
{
  int x0, y0, x1, y1;
  getStandardPaintingArea(x0, y0, x1, y1, p);
  window.fitAroundPainting(x0, y0, x1, y1);
}

void maximizePaintWindow(Painting& painting, const MainProgram& p)
{
  //painting.window.resize(p.toolBox.getX1() + 2, p.toolbar2.getY1() + 2, p.colorWindow->getX0() - 2, options.screenHeight - 20);
  //painting.window.resize(2, p.toolbar2.getY1() + 2, p.colorWindow->getX0() - 2, p.optionsNow.screenHeight - 20);

  int x0, y0, x1, y1;
  getStandardPaintingArea(x0, y0, x1, y1, p);
  painting.window.resize(x0, y0, x1, y1);
  painting.window.centerPaint();
}

////////////////////////////////////////////////////////////////////////////////

MainProgram::MainProgram(int width, int height, bool fullscreen, bool resizable, const std::string& title, Options& optionsNow, Options& optionsLater)
: title(title)
, optionsNow(optionsNow)
, optionsLater(optionsLater)
{
#ifdef _WIN32
  //On Windows, the restoring of main program window is broken, even with the SDL_VIDEO_CENTERED=center below.
  //And that means that if LodePaint was maximized, and now its window is created with those sizes, stuff goes outside the screen.
  //Avoid that by allowing a smaller maximum size
  int maxWindowsXResolution = GetSystemMetrics(SM_CXSCREEN) - 20;
  int maxWindowsYResolution = GetSystemMetrics(SM_CYSCREEN) - 120; //some room for start menu bar, window top bar, ...
  if(width > maxWindowsXResolution) width = maxWindowsXResolution;
  if(height > maxWindowsYResolution) height = maxWindowsYResolution;
#endif

  //Set SDL_VIDEO_CENTERED environment variable, otherwise, in MS Windows, the window tends to have too high X/Y position causing it to be partially outside of the screen
  //I'm less-than-pleasantly surprised by SDL using environment vars for some random things, like, why is XY pos of window a env far, while size is not? Nothing should be imho...
  //the (char*) conversion is due to compiler warning, to be able to pass that argument to SDL's function without warning.
  SDL_putenv((char*)"SDL_VIDEO_CENTERED=center");

  create(width, height, fullscreen, resizable, title);

  //Clear it again so that manually resizing after this doesn't do annoying
  SDL_putenv((char*)"SDL_VIDEO_CENTERED=");
}

MainProgram::~MainProgram()
{
  destruct();
}

void MainProgram::recreateScreen(int width, int height, bool fullscreen, bool resizable)
{
  int oldw = optionsNow.screenWidth;
  int oldh = optionsNow.screenHeight;
  if(width < 640) width = 640;
  if(height < 480) height = 480;

  screen->changeResolution(width, height, fullscreen, false, resizable, title.c_str(), false);
  c->resize(0, 0, width, height);

  windowInsideKeeper.handleMainContainerResize(oldw, oldh, width, height);

  optionsNow.screenWidth = optionsLater.screenWidth = width;
  optionsNow.screenHeight = optionsLater.screenHeight = height;
}

void MainProgram::create(int width, int height, bool fullscreen, bool resizable, const std::string& title)
{
  std::stringstream ss;
  ss << "Creating screen. " << "Width: " << width << ", height: " << height << ", fullscreen: " << fullscreen << ", resizable: " << resizable;
  paintDebugMessage(ss.str());
  screen = new lpi::ScreenGL(width, height, fullscreen, false, resizable, title.c_str(), true);
  paintDebugMessage("Screen created. Creating gui drawer");
  guidrawer = new lpi::gui::GUIDrawerGL(screen);
  paintDebugMessage("Gui drawer created. Creating main container");
  c = new lpi::gui::MainContainer(*guidrawer);
  paintDebugMessage("Main container created");
  recentMenu = new lpi::gui::RecentFilesMenu(20);
  modalFrameHandler = new MyModalFrameHandler(*screen, *guidrawer, *this);
  filebrowser = new lpi::FileBrowse();
  formats = new ImageFormats();

  colorWindow = new ColorWindow(*guidrawer, optionsLater);
  overviewWindow = new OverviewWindow(*guidrawer);
}

void MainProgram::destruct()
{
  delete overviewWindow;
  delete colorWindow;
  delete filebrowser;
  delete modalFrameHandler;
  delete recentMenu;
  delete c;
  delete guidrawer;
  delete screen;
  delete formats;
}

void handlePaint(PaintWindow& paintWindow, UndoStack& undoStack, const lpi::IInput& input)
{
  if(tool)
  {
    tool->handle(input, paintWindow.getPaint());
    if(tool->done())
    {
      UndoState* state = undoStack.addNewState(tool);
      tool->getUndoState(*state);
      paintWindow.setStatus(PaintWindow::S_MODIFIED);
    }
  }
}

void drawPaint(PaintWindow& paintWindow, lpi::gui::IGUIDrawer& drawer)
{
  Paint& paint = paintWindow.getPaint();
  if(tool)
  {
    drawer.pushSmallestScissor(paintWindow.getVisibleX0() + 1, paintWindow.getVisibleY0(), paintWindow.getVisibleX1() - 1, paintWindow.getVisibleY1() - 1);
    tool->draw(drawer, paint);

    //Show an alpha symbol if painting while alpha is not 255, but still pretty opaque so you might not be noticing that you're using alpha
    bool darkalpha = false;
    double zoom = paintWindow.getPaint().getZoom();
    double dark_threshold = zoom > 4 ? 0.8 : zoom > 2 ? 0.6 : 0.0;
    if(paintWindow.mouseDown(drawer.getInput(), lpi::LMB) && globalToolSettings.leftColor.a > dark_threshold && globalToolSettings.leftColor.a < 1) darkalpha = true;
    if(paintWindow.mouseDown(drawer.getInput(), lpi::RMB) && globalToolSettings.rightColor.a > dark_threshold && globalToolSettings.rightColor.a < 1) darkalpha = true;
    if(darkalpha)
    {
      std::string alphasymbol = "\u03b1";
      int x = drawer.getInput().mouseX();
      int y = drawer.getInput().mouseY();
      drawer.drawText(alphasymbol, x - 17, y - 7, lpi::RGB_Black);
      drawer.drawText(alphasymbol, x - 17, y - 9, lpi::RGB_Black);
      drawer.drawText(alphasymbol, x - 15, y - 7, lpi::RGB_Black);
      drawer.drawText(alphasymbol, x - 15, y - 9, lpi::RGB_Black);
      drawer.drawText(alphasymbol, x - 16, y - 8, lpi::RGB_White);
    }

    drawer.popScissor();
  }
}

////////////////////////////////////////////////////////////////////////////////

struct DoFilterOnAllMultithreadedParam
{
  Paintings* paintings;
  IOperationFilter* filter;
  Progress* progress;
  std::vector<bool> results;
  std::vector<UndoState*> undoStates;
};

int doFilterOnAllMultithreadedFun(void* param)
{
  DoFilterOnAllMultithreadedParam& p = *(DoFilterOnAllMultithreadedParam*)(param);

  p.progress->setStatus(S_BUSY);

  for(size_t i = 0; i < p.paintings->paintings.size(); i++)
  {
    p.progress->setProgress((i+1) / (double)(p.paintings->paintings.size()));

    UndoState* state = (*p.paintings)[i]->undoStack.addNewState(p.filter);
    p.undoStates.push_back(state);
    bool result = p.filter->operate((*p.paintings)[i]->paint, *state, *p.progress);
    p.results.push_back(result);
    (*p.paintings)[i]->window.setStatus(PaintWindow::S_MODIFIED);

    if(p.progress->isUserCancel())
    {
      p.progress->setStatus(S_KILLED);
      return 1;
    }
  }
  p.progress->setStatus(S_DONE);

  return 0;
}

//everything must already be prepared (GUI dialog, checks, ...)
void doFilterOnAllMultithreaded(Paintings& paintings, IOperationFilter& filter, MainProgram& p)
{
  Progress progress;

  DoFilterOnAllMultithreadedParam param;
  param.paintings = &paintings;
  param.filter = &filter;
  param.progress = &progress;

  SDL_CreateThread(doFilterOnAllMultithreadedFun, &param);

  MyModalFrameHandler& frame = *p.modalFrameHandler;

  int x0, y0, x1, y1;
  frame.getScreenSize(x0, y0, x1, y1);

  lpi::gui::ProgressBarDialog dialog(frame.getDrawer());
  dialog.moveCenterTo((x0+x1)/2, (y0+y1)/2);
  dialog.setElementOver(false);

  for(;;)
  {
    frame.doFrame();

    if(dialog.done())
    {
      progress.setUserCancel(true);
    }

    if(progress.getStatus() == S_DONE || progress.getStatus() == S_KILLED)
    {
      break;
    }
    dialog.setProgress(progress.getProgress());

    frame.getScreenSize(x0, y0, x1, y1); //gotten again every frame in case size changed
    dialog.handle(frame.getDrawer().getInput());
    p.c->draw(frame.getDrawer());
    frame.getDrawer().drawRectangle(x0, y0, x1, y1, lpi::ColorRGB(0,0,0,128), true); //"modal darkness"
    dialog.draw(frame.getDrawer());
  }

  for(size_t i = 0; i < /*paintings.paintings.size();*/param.results.size(); i++)
  {
    if(!param.results[i]) continue;

    filter.operateGL(paintings[i]->paint, *param.undoStates[i]);
  }
}

struct DoFilterMultithreadedParam
{
  Painting* painting;
  IOperationFilter* filter;
  Progress* progress;
  bool result;
  UndoState* undoState;
};

int doFilterMultithreadedFun(void* param)
{
  DoFilterMultithreadedParam& p = *(DoFilterMultithreadedParam*)(param);

  UndoState* state = p.painting->undoStack.addNewState(p.filter);
  p.undoState = state;
  p.result = p.filter->operate(p.painting->paint, *state, *p.progress);
  p.painting->window.setStatus(PaintWindow::S_MODIFIED);

  p.progress->setStatus(S_DONE);

  return 0;
}

//everything must already be prepared (GUI dialog, checks, ...)
void doFilterMultithreaded(Painting& painting, IOperationFilter& filter, MainProgram& p)
{
  Progress progress;

  DoFilterMultithreadedParam param;
  param.painting = &painting;
  param.filter = &filter;
  param.progress = &progress;
  param.result = false;
  param.undoState = 0;

  SDL_CreateThread(doFilterMultithreadedFun, &param);

  waitForProgressBar(p, progress);

  if(!param.result || !filter.operateGL(painting.paint, *param.undoState))
  {
    lpi::gui::showMessageBox(*p.c, *p.modalFrameHandler, filter.getLastError());
    painting.undoStack.popState();
  }
  else painting.window.setStatus(PaintWindow::S_MODIFIED);
}

bool filterHelpHandler(lpi::gui::Dialog* dialog, void* context)
{
  FilterDialog* filterDialog = (FilterDialog*)dialog;
  MainProgram& h = *(MainProgram*)context;

  if(filterDialog->help.clicked(h.guidrawer->getInput()))
  {
    lpi::gui::showMessageBox(*h.c, *h.modalFrameHandler, filterDialog->helpText, "help");
  }
  return false;
}

void doFilter(MainProgram& h, IOperationFilter& filter, Paintings& paintings)
{
  if(doOnAll && paintings.paintings.empty()) return;
  if(!doOnAll && paintings.activePainting == 0) return;

  Painting& painting = doOnAll ? (paintings.activePainting ? *paintings.activePainting : *paintings[0]) : *paintings.activePainting;
  Paint& paint = painting.paint;

  std::string reason;
  if(doOnAll || filter.canOperate(reason, paint))
  {
    if(filter.getDialog() != 0)
    {
      int x0, y0, x1, y1;
      h.modalFrameHandler->getScreenSize(x0, y0, x1, y1);

      FilterDialog dialog(filter.getDialog(), *h.guidrawer, _t(filter.getLabel()));
      dialog.moveCenterTo((x0+x1)/2, (y0+y1)/2);
      filter.init(&painting.window);

      std::string helpText = _t(filter.getHelp());
      if(helpText.size() > 0)
      {
        dialog.addHelp(helpText);
      }

      h.c->doModalDialog(dialog, *h.modalFrameHandler, filterHelpHandler, &h);
      if(dialog.getResult() == lpi::gui::Dialog::CANCEL)
      {
        return;
      }
    }

    if(doOnAll)
    {
      doFilterOnAllMultithreaded(paintings, filter, h);
    }
    else
    {
      doFilterMultithreaded(painting, filter, h);
    }
  }
  else
  {
    lpi::gui::showMessageBox(*h.c, *h.modalFrameHandler, reason);
  }
}

////////////////////////////////////////////////////////////////////////////////

void showOptionsDialog(MainProgram& h, Options& optionsNow, Options& optionsLater, OptionsWindow& dialog)
{
  dialog.getOptions() = optionsLater;
  dialog.valueToControls();

  int x0, y0, x1, y1;
  h.modalFrameHandler->getScreenSize(x0, y0, x1, y1);

  dialog.moveCenterTo((x0+x1)/2, (y0+y1)/2);
  dialog.setEnabled(true);
  if(h.c->doModalDialog(dialog, *h.modalFrameHandler))
  {
    if(dialog.getResult() == lpi::gui::Dialog::OK)
    {
      optionsLater = dialog.getOptions();
      optionsLaterToOptionsNow(optionsNow, optionsLater);
    }
  }
}


void showShortcutsDialog(MainProgram& h, ShortCutManager& manager, ShortcutWindow& dialog)
{
  dialog.reinit();
  int x0, y0, x1, y1;
  h.modalFrameHandler->getScreenSize(x0, y0, x1, y1);

  std::vector<KeyCombo> old0, old1;
  manager.getCombos(old0, old1);

  dialog.moveCenterTo((x0+x1)/2, (y0+y1)/2);
  dialog.setEnabled(true);
  if(h.c->doModalDialog(dialog, *h.modalFrameHandler))
  {
    if(dialog.getResult() != lpi::gui::Dialog::OK)
    {
      manager.setCombos(old0, old1);
    }
  }
}

void addPaintWindow(MainProgram& p, Paintings& paintings, Painting* w)
{
  paintings.paintings.push_back(w);
  //p.c->pushTop(&w->window, lpi::gui::Sticky(0.0, 0, true, 0.0, 0, true, 1.0, 0, true, 1.0, 0, true));
  p.c->pushTop(&w->window, lpi::gui::STICKYOFF);


  int x0, y0, x1, y1;
  p.modalFrameHandler->getScreenSize(x0, y0, x1, y1);

  /*int sizex = w->paint.getU() + 16;
  int sizey = w->paint.getV() + 48;
  sizex = lpi::clamp(sizex, 500, 800);
  sizey = lpi::clamp(sizey, 256, 600);
  w->window.resize(0, 0, sizex, sizey);
  w->window.moveCenterTo((x0 + x1) / 2, (y0 + y1) / 2);*/

  PaintWindow& pw = w->window;

  /*pw.resize(x0 + 144, y0 + 47 + 24, x1 - 304, y1 - 18);
  int smallerx = pw.getSizeX() - 32 - w->paint.getU();
  int smallery = pw.getSizeY() - 64 - w->paint.getV();

  if(smallerx > 0) pw.resize(pw.getX0() + smallerx / 2, pw.getY0(), pw.getX1() - smallerx / 2, pw.getY1());
  if(smallery > 0) pw.resize(pw.getX0(), pw.getY0() + smallery / 2, pw.getX1(), pw.getY1() - smallery / 2);*/

  //maximizePaintWindow(*w, p);
  fitWindowAroundPainting(w->window, p);

  pw.centerPaint();
}

//To be called after program window resize: small painting windows are kept as is, too large ones are shrinked.
void updatePaintingsAfterProgramResize(int oldw, int oldh, int neww, int newh, Paintings& paintings)
{
  for(size_t i = 0; i < paintings.paintings.size(); i++)
  {
    lpi::gui::Element& window = paintings[i]->window;
    if(neww < oldw && window.getX1() >= neww - 2)
    {
      window.resize(window.getX0(), window.getY0(), neww - 2, window.getY1());
    }
    // Only if actually at the side, or if there's exactly one painting it won't overlap others so increase then too
    if(neww > oldw && (window.getX1() >= oldw - 10 || paintings.paintings.size() == 1))
    {
      window.resize(window.getX0(), window.getY0(), window.getX1() + neww - oldw, window.getY1());
    }
    if(newh < oldh && window.getY1() >= newh - 18)
    {
      window.resize(window.getX0(), window.getY0(), window.getX1(), newh - 18);
    }
    // Only if actually at the side, or if there's exactly one painting it won't overlap others so increase then too
    if(newh > oldh && (window.getY1() >= oldh - 28 || paintings.paintings.size() == 1))
    {
      window.resize(window.getX0(), window.getY0(), window.getX1(), window.getY1() + newh - oldh);
    }
  }
}


void handleFilterMenu(IOperationFilter*& last_filter, IOperationFilter*& last_filter_2, lpi::gui::MenuVertical& menu, std::vector<lpi::gui::MenuVertical*>& submenu, Filters& filters, lpi::IInput& input, MainProgram& h, Paintings& paintings)
{
  for(size_t i = 0; i < submenu.size(); i++)
  {
    size_t filter_clicked = submenu[i]->itemClicked(input);
    if(filter_clicked < submenu[i]->getNumItems())
    {
      doFilter(h, *filters.getSubFilters()[i].filters[filter_clicked], paintings);
      last_filter_2 = last_filter;
      last_filter = filters.getSubFilters()[i].filters[filter_clicked];
    }
  }

  // Don't do a filter if a menu item larger than the filters is pressed (the last menu items can be non-filters and their menu clicks checked elsewhere)
  for(size_t i = 0; i < filters.getRootFilters().filters.size(); i++)
  {
    if(menu.itemClicked(submenu.size() + i, input))
    {
      doFilter(h, *filters.getRootFilters().filters[i], paintings);
      last_filter_2 = last_filter;
      last_filter = filters.getRootFilters().filters[i];
    }
  }
}

void doNew(MainProgram& p, Paintings& paintings)
{
  Painting* w = new Painting(p.optionsNow, p.screen->getGLContext());
  w->window.make(0, 0, 1, 1, *p.guidrawer);
  if(makeNewImage(p, w->window, globalToolSettings, options))
  {
    options.newImageSizeX = optionsLater.newImageSizeX = w->window.getPaint().canvasRGBA8.getU();
    options.newImageSizeY = optionsLater.newImageSizeY = w->window.getPaint().canvasRGBA8.getV();
    addPaintWindow(p, paintings, w);
    fitWindowAroundPainting(w->window, p);
  }
  else delete w;
}

void doOpen(MainProgram& p, Paintings& paintings)
{
  open(paintings, p);
}

bool doPaste(PaintWindow& paintWindow) //paste as new image
{
  std::vector<unsigned char> image;
  int w, h;
  if(getClipboardImageRGBA8(image, w, h))
  {
    paintWindow.getPaint().canvasRGBA8.setSize(w, h);
    setAlignedBuffer(&paintWindow.getPaint().canvasRGBA8, &image[0]);
    paintWindow.getPaint().update();
    paintWindow.setStatus(PaintWindow::S_MODIFIED);
    return true;
  }

  return false;
}

//paintings is the complete list of paintings, painting is the currently active one
void doPaste(MainProgram& p, Paintings& paintings, Painting* painting)
{
  if(painting && painting->window.getStatus() == PaintWindow::S_NEW)
  {
    doPaste(painting->window);
  }
  else
  {
    Painting* w = new Painting(p.optionsNow, p.screen->getGLContext());
    w->window.make(0, 0, 1, 1, *p.guidrawer);
    if(doPaste(w->window)) addPaintWindow(p, paintings, w);
    else delete w;
  }
}

void doPasteSel(MainProgram& p, Paintings& paintings, GlobalToolSettings& settings, ToolsCollection& tools) //paste as selection
{
  p.toolBox.selectTool(tools.getShortcutTool('s'));

  static OperationPasteAsSelection op(settings);

  if(doOnAll && paintings.paintings.empty()) return;
  if(!doOnAll && paintings.activePainting == 0) return;

  Painting& painting = doOnAll ? (paintings.activePainting ? *paintings.activePainting : *paintings[0]) : *paintings.activePainting;
  op.setPaintWindow(&painting.window);

  doFilter(p, op, paintings);
}


void doCopy(MainProgram& p, PaintWindow* paintWindow)
{
  Paint& paint = paintWindow->getPaint();

  bool success = true;

  if(paint.getColorMode() == MODE_RGBA_8)
  {
    bool self = paint.hasFloatingSelection();
    bool seln = paint.hasNonFloatingSelection();

    if(seln) paint.makeSelectionFloating(lpi::ColorRGB(0,0,0,0), false);

    if(seln || self)
    {
      success &= setClipboardImageRGBA8(paint.floatsel.texture.getBuffer(), paint.floatsel.texture.getU(), paint.floatsel.texture.getV(), paint.floatsel.texture.getU2());
    }
    else
    {
      success &= setClipboardImageRGBA8(paint.canvasRGBA8.getBuffer(), paint.canvasRGBA8.getU(), paint.canvasRGBA8.getV(), paint.canvasRGBA8.getU2());
    }

    if(seln)
    {
      paint.floatsel.treat_alpha_as_opacity = false;
      paint.unFloatSelection();
    }
  }
  else
  {
    lpi::gui::showMessageBox(*p.c, *p.modalFrameHandler, _t("copy not yet supported for this color mode, try RGBA8"));
    return;
  }

  if(!success) lpi::gui::showMessageBox(*p.c, *p.modalFrameHandler, _t("Copy to clipboard failed. Maybe it's not yet supported for this OS.\nAs alternative, try making a selection and drag it while holding CTRL."));
}

bool ensureReallyCloseAll(Paintings& paintings, MainProgram& p)
{
  bool unsavedChanges = false;
  for(size_t i = 0; i < paintings.paintings.size(); i++)
  {
    if(paintings[i]->window.getStatus() == PaintWindow::S_MODIFIED)
    {
      unsavedChanges = true;
      break;
    }
  }
  bool reallyClose = !unsavedChanges || lpi::gui::getYesNoModal(*p.c, *p.modalFrameHandler, _t("Some images have unsaved changes. Really close?"), _t("Discard"), _t("Keep Open"));
  return reallyClose;
}

void doClose(PaintWindow*& paintWindow, PaintWindow*& prevTopPaintWindow,
             MainProgram& p, Paintings& paintings, Painting* painting)
{
  if (!painting) return;
  bool reallyClose = painting->window.getStatus() != PaintWindow::S_MODIFIED || lpi::gui::getYesNoModal(*p.c, *p.modalFrameHandler, _t("Image has unsaved changes. Really close it?"), _t("Discard"), _t("Keep Open"));
  if(reallyClose)
  {
    paintings.paintings.erase(std::remove(paintings.paintings.begin(), paintings.paintings.end(), painting), paintings.paintings.end());
    p.c->remove(&painting->window);
    delete painting;
    paintWindow = prevTopPaintWindow = 0;
    p.overviewWindow->setPaintWindow(0);
  }
}

void doCloseAll(PaintWindow*& paintWindow, PaintWindow*& prevTopPaintWindow,
                MainProgram& p, Paintings& paintings)
{
  bool reallyClose = ensureReallyCloseAll(paintings, p);
  if(reallyClose)
  {
    size_t amount = paintings.paintings.size();
    for(size_t i = 0; i < amount; i++)
    {
      Painting* painting = paintings.paintings.back();
      paintings.paintings.pop_back();
      p.c->remove(&painting->window);
      delete painting;
    }
    paintWindow = prevTopPaintWindow = 0;
    p.overviewWindow->setPaintWindow(0);
  }
}

void selectTool(ITool* newtool, ToolSettingWindow& w)
{
  tool = newtool;
  lpi::gui::Element* e = tool->getDialog();
  w.clearCustomTab();
  if(e)
  {
    w.setCustomTab(e);
  }
}

void dynChangeSettings(GlobalToolSettings& globalToolSettings, DynamicBrushChange::Change change, double amount)
{
  Brush& brush = globalToolSettings.brush;
  lpi::ColorRGBd& fg = globalToolSettings.leftColor;
  lpi::ColorRGBd& bg = globalToolSettings.rightColor;

  //the 0.004 appearing here and there below comes from 1/255.0

  switch(change)
  {
    case DynamicBrushChange::C_NOTHING:
    {
    }
    break;
    case DynamicBrushChange::C_BRUSH_SIZE:
    {
      brush.size += (int)(amount);
      if(brush.size < 1) brush.size = 1;
    }
    break;
    case DynamicBrushChange::C_BRUSH_OPACITY:
    {
      brush.opacity += amount * 0.01;
      if(brush.opacity > 1) brush.opacity = 1;
      if(brush.opacity < 0) brush.opacity = 0;
    }
    break;
    case DynamicBrushChange::C_BRUSH_SOFTNESS:
    {
      brush.softness += amount * 0.01;
      if(brush.softness > 1) brush.softness = 1;
      if(brush.softness < 0) brush.softness = 0;
    }
    break;
    case DynamicBrushChange::C_FG_HUE:
    {
      lpi::ColorHSVd hsv = lpi::RGBtoHSV(fg);
      hsv.h += amount * 0.004;
      while(hsv.h > 1.0) hsv.h -= 1.0;
      while(hsv.h < 0.0) hsv.h += 1.0;
      fg = lpi::HSVtoRGB(hsv);
    }
    break;
    //saturation uses HSV while brightness uses HSL. For brightness HSL is definatly better (goes from black to white instead of black to color), but for saturation HSL gives numerical problems while HSV doesn't.
    case DynamicBrushChange::C_FG_SATURATION:
    {
      lpi::ColorHSVd hsv = lpi::RGBtoHSV(fg);
      hsv.s += amount * 0.004;
      if(hsv.s > 1.0) hsv.s = 1.0;
      if(hsv.s < 0.01) hsv.s = 0.01;
      fg = lpi::HSVtoRGB(hsv);
    }
    break;
    case DynamicBrushChange::C_FG_BRIGHTNESS:
    {
      lpi::ColorHSLd hsl = lpi::RGBtoHSL(fg);
      hsl.l += amount * 0.004;
      if(hsl.l > 0.99) hsl.l = 0.99;
      if(hsl.l < 0.01) hsl.l = 0.01;
      fg = lpi::HSLtoRGB(hsl);
    }
    break;
    case DynamicBrushChange::C_FG_ALPHA:
    {
      fg.a += amount * 0.004;
      if(fg.a > 1.0) fg.a = 1.0;
      if(fg.a < 0.0) fg.a = 0.0;
    }
    break;
    case DynamicBrushChange::C_BG_HUE:
    {
      lpi::ColorHSVd hsv = lpi::RGBtoHSV(bg);
      hsv.h += amount * 0.004;
      while(hsv.h > 1.0) hsv.h -= 1.0;
      while(hsv.h < 0.0) hsv.h += 1.0;
      bg = lpi::HSVtoRGB(hsv);
    }
    break;
    case DynamicBrushChange::C_BG_SATURATION:
    {
      lpi::ColorHSVd hsv = lpi::RGBtoHSV(bg);
      hsv.s += amount * 0.004;
      if(hsv.s > 1.0) hsv.s = 1.0;
      if(hsv.s < 0.01) hsv.s = 0.01;
      bg = lpi::HSVtoRGB(hsv);
    }
    break;
    case DynamicBrushChange::C_BG_BRIGHTNESS:
    {
      lpi::ColorHSLd hsl = lpi::RGBtoHSL(bg);
      hsl.l += amount * 0.004;
      if(hsl.l > 0.99) hsl.l = 0.99;
      if(hsl.l < 0.01) hsl.l = 0.01;
      bg = lpi::HSLtoRGB(hsl);
    }
    break;
    case DynamicBrushChange::C_BG_ALPHA:
    {
      bg.a += amount * 0.004;
      if(bg.a > 1.0) bg.a = 1.0;
      if(bg.a < 0.0) bg.a = 0.0;
    }
    break;
    default:
      break;
  }
}

bool stringContainsIgnoreCase(const std::string& s, const std::string& sub)
{
  size_t j = 0;

  for(size_t i = 0; i < s.size() && j < sub.size(); i++)
  {
    char c = s[i];
    char subc = sub[j];

    if(c >= 'a' && c <= 'z') c -= ('a' - 'A');
    if(subc >= 'a' && subc <= 'z') subc -= ('a' - 'A');

    if(c == subc)
    {
      j++;
      if(j == sub.size()) return true;
    }
    else j = 0;
  }

  return false;
}

template<typename T>
void cycle(T& t, T num, bool dir)
{
  if(dir)
  {
    t++;
    if(t >= num) t = 0;
  }
  else
  {
    if(t == 0) t = num - 1;
    else t--;
  }
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

bool handleMenusAndToolbar(MenuToolbarIndices& m
                         , Paintings& paintings
                         , PaintWindow*& paintWindow, PaintWindow*& prevTopPaintWindow
                         , MainProgram& p, lpi::IInput& input, lpi::Persist& persist
                         , FiltersCollection& filters
                         , std::stringstream& compiledate
                         , OptionsWindow& optionsWindow
                         , ShortcutWindow& shortcutsWindow, ShortCutManager& shortcutManager
                         , IOperationFilter*& last_filter, IOperationFilter*& last_filter_2
                         , ToolsCollection& tools)
{
  lpi::gui::GUIDrawerGL& guidrawer = *p.guidrawer;

  if(m.menu_image.itemClicked(m.menuindex_imageinfo, input) && paintWindow)
  {
    PaintWindow& pw = *paintWindow;
    Paint& pa = pw.getPaint();
    std::stringstream info;

    info << _t("Image information and statistics") << "\n\n";

    std::string filename = pw.getFilename() == "" ? "N/A" : pw.getFilename();
    info << _t("Filename") << ": " << filename << "\n";
    info << _t("Size in pixels") << ": " << pa.getU() << " x " << pa.getV() << "\n";
    info << _t("Total pixels") << ": " << pa.getU() * pa.getV() << "\n";

    std::string colormode;
    int channels;
    int bpp;
    bool docolorcount = pa.getU() * pa.getV() < 5000 * 5000; //it's a bit slow on large images :(
    size_t colorcount = 0;
    if(pa.getColorMode() == MODE_RGBA_8)
    {
      colormode = "RGBA8";
      channels = 4;
      bpp = 32;
      if(docolorcount) colorcount = countColorsRGBA8(pa.canvasRGBA8.getBuffer(), pa.canvasRGBA8.getU(), pa.canvasRGBA8.getV(), pa.canvasRGBA8.getU2());
    }
    else if(pa.getColorMode() == MODE_RGBA_32F)
    {
      colormode = "RGBA32F";
      channels = 4;
      bpp = 128;
      if(docolorcount) colorcount = countColorsRGBA32F(pa.canvasRGBA32F.getBuffer(), pa.canvasRGBA32F.getU(), pa.canvasRGBA32F.getV(), pa.canvasRGBA32F.getU2());
    }
    else if(pa.getColorMode() == MODE_GREY_8)
    {
      colormode = "Grey8";
      channels = 1;
      bpp = 8;
      if(docolorcount) colorcount = countColorsGrey8(pa.canvasGrey8.getBuffer(), pa.canvasGrey8.getU(), pa.canvasGrey8.getV(), pa.canvasGrey8.getU2());
    }
    else
    {
      colormode = "Grey32F";
      channels = 1;
      bpp = 32;
      if(docolorcount) colorcount = countColorsGrey32F(pa.canvasGrey32F.getBuffer(), pa.canvasGrey32F.getU(), pa.canvasGrey32F.getV(), pa.canvasGrey32F.getU2());
    }

    info << _t("Color mode") << ": " << colormode << "\n";
    info << _t("Color channels") << ": " << channels << "\n";
    info << _t("Bits per pixel") << ": " << bpp << "\n";

    info << _t("Size in memory") << ": " << bpp / 8 * pa.getU() * pa.getV() << _t(" bytes") << "\n";
    if(docolorcount) info << _t("Unique color count") << ": " << colorcount << "\n";

    lpi::gui::showMessageBox(*p.c, *p.modalFrameHandler, info.str());
  }


  if(m.menu_file.itemClicked(m.menuindex_exit, input)) return false; //exit

  if(m.menu_file.itemClicked(m.menuindex_saveas, input) || p.toolbar.itemClicked(m.toolbar_save_as, input))
  {
    if(paintWindow) saveAs(*paintWindow, true, p);
  }
  if(m.menu_file.itemClicked(m.menuindex_saveall, input) || p.toolbar.itemClicked(m.toolbar_save_all, input))
  {
    for(size_t i = 0; i < paintings.paintings.size(); i++) save(paintings[i]->window, p);
  }
  if(m.menu_file.itemClicked(m.menuindex_savecopyas, input))
  {
    if(paintWindow) saveAs(*paintWindow, false, p);
  }
  if(m.menu_file.itemClicked(m.menuindex_save, input) || p.toolbar.itemClicked(m.toolbar_save, input))
  {
    if(paintWindow) save(*paintWindow, p);
  }
  if(m.menu_file.itemClicked(m.menuindex_open, input) || p.toolbar.itemClicked(m.toolbar_open, input))
  {
    doOpen(p, paintings);
  }

  if(m.menu_file.itemClicked(m.menuindex_close, input))
  {
    doClose(paintWindow, prevTopPaintWindow, p, paintings, paintings.activePainting);
  }

  if(m.menu_file.itemClicked(m.menuindex_closeall, input))
  {
    doCloseAll(paintWindow, prevTopPaintWindow, p, paintings);
  }

  if(m.menu_file.itemClicked(m.menuindex_reload, input))
  {
    //TODO: since you're reloading the image and forgetting all changes, ask confirmation if the image was changed
    if(paintWindow && paintWindow->hasFilename()) openImageFile(*paintWindow, paintWindow->getFilename(), p);
  }

  p.toolbar.setEnabled(m.toolbar_undo, true);
  if(paintWindow && !doOnAll && paintings.activePainting->undoStack.getIndex() == 0) p.toolbar.setEnabled(m.toolbar_undo, false);

  p.toolbar.setEnabled(m.toolbar_redo, true);
  if(paintWindow && !doOnAll && paintings.activePainting->undoStack.getIndex() >= paintings.activePainting->undoStack.size()) p.toolbar.setEnabled(m.toolbar_redo, false);


  if(m.menu_edit.itemClicked(m.menuindex_undo, input) || p.toolbar.itemClicked(m.toolbar_undo, input))
  {
    if(doOnAll)
    {
      for(size_t i = 0; i < paintings.paintings.size(); i++) paintings[i]->undoStack.undo(paintings[i]->paint);
    }
    else if(paintWindow) paintings.activePainting->undoStack.undo(paintWindow->getPaint());
  }
  if(m.menu_edit.itemClicked(m.menuindex_redo, input) || p.toolbar.itemClicked(m.toolbar_redo, input))
  {
    if(doOnAll)
    {
      for(size_t i = 0; i < paintings.paintings.size(); i++) paintings[i]->undoStack.redo(paintings[i]->paint);
    }
    else if(paintWindow) paintings.activePainting->undoStack.redo(paintWindow->getPaint());
  }
  //if(m.menu_edit.itemClicked(m.menuindex_temp_debug, input)) {}
  if(m.menuindex_skin_default != (size_t)(-1) && m.menu_skins.itemClicked(m.menuindex_skin_default, input))
  {
    trySkin(p, optionsLater, "default");
  }
  if(m.menuindex_skin_aos4 != (size_t)(-1) && m.menu_skins.itemClicked(m.menuindex_skin_aos4, input))
  {
    trySkin(p, optionsLater, "aos4");
  }
  if(m.menuindex_skin_dark != (size_t)(-1) && m.menu_skins.itemClicked(m.menuindex_skin_dark, input))
  {
    trySkin(p, optionsLater, "dark");
  }
  if(m.menuindex_skin_old != (size_t)(-1) && m.menu_skins.itemClicked(m.menuindex_skin_old, input))
  {
    trySkin(p, optionsLater, "old");
  }
  if(m.menuindex_skin_none_found_message != (size_t)(-1) && m.menu_skins.itemClicked(m.menuindex_skin_none_found_message, input))
  {
    lpi::gui::showMessageBox(*p.c, *p.modalFrameHandler, _t("No skins found in the expected disk path (see help: Data/ under system info). Using built-in skin instead."));
  }

  for(size_t i = 0; i < m.menu_language.getNumItems() && i < lpi::globalTranslate.getNumLanguages(); i++)
  {
    if(m.menu_language.itemClicked(i, input))
    {
      lpi::globalTranslate.setLanguage(lpi::globalTranslate.getLanguageKey(i));
      options.language = optionsLater.language = lpi::globalTranslate.getLanguageKey(i);
      lpi::gui::showMessageBox(*p.c, *p.modalFrameHandler, _t("Please restart the program for the new language setting to fully take effect."));
    }
  }
  if(m.menuindex_lang_none_found_message != (size_t)(-1) && m.menu_language.itemClicked(m.menuindex_lang_none_found_message, input))
  {
    lpi::gui::showMessageBox(*p.c, *p.modalFrameHandler, _t("No languages found in the expected disk path (see help: Data/ under system info). Using built-in language instead."));
  }

  if(m.menuindex_plugins_none_found_message != (size_t)(-1) && m.menu_plugins.itemClicked(m.menuindex_plugins_none_found_message, input)) {
    lpi::gui::showMessageBox(*p.c, *p.modalFrameHandler, _t("Shows plugins found in the expected disk path (see help: Data/ under system info)."));
  }

  if(m.menu_help.itemClicked(m.menuindex_help, input))
  {
    lpi::gui::showMessageBox(*p.c, *p.modalFrameHandler, _t("[Help Text]"));
  }
  if(m.menu_help.itemClicked(m.menuindex_about, input))
  {
    std::stringstream message;
    message << "\
LodePaint\n\
\n\
Version: " + LODEPAINT_VERSION + "\n\
Compile Date: ";
    message << compiledate.str() << std::endl;
    message << "\
\n\
Get the latest version here:\n\
http://sourceforge.net/projects/lodepaint/\n\
\n\
Acknowledgements:\n\
Radix FFT by Javier Soley, Ph. D\n\
TGA support by http://nothings.org/stb_image.c\n\
JPEG support by jpgd.cpp by Rich Geldreich\n\
RGBE support by Bruce Walter and Greg Ward\n\
PNG support by LodePNG\n\
GIF loader: Copyright (c) 2000, Juan Soulie <jsoulie@cplusplus.com>\n\
LED icon set: http://led24.de/iconset/\n\
AmigaOS4 and AROS version: kas1e and Yannick\n\
\n\
This software is provided 'as-is', without any express\n\
or implied warranty. In no event will the authors be held\n\
liable for any damages arising from the use of this software.\n\
\n\
Copyright (c) 2009-2018 by Lode Vandevenne\n\
";
    lpi::gui::showMessageBox(*p.c, *p.modalFrameHandler, message.str());
  }
  if(m.menu_help.itemClicked(m.menuindex_systeminfo, input))
  {
    const SDL_version* sdl_version = SDL_Linked_Version();
    std::stringstream message;
    message << _t("Version: ") << LODEPAINT_VERSION << std::endl;
    message << _t("Compile Date: ") << compiledate.str() << std::endl;
    message << _t("Shell Command: ") << options.command << std::endl;
    message << _t("\nSystem Info: ") << std::endl;
    message << _t("SDL Version: ") << (int)sdl_version->major << "." << (int)sdl_version->minor << "." << (int)sdl_version->patch << std::endl;
    message << _t("OpenGL Version: ") << glGetString(GL_VERSION) << std::endl;
    message << _t("OpenGL Renderer: ") << glGetString(GL_RENDERER) << std::endl;
    message << _t("OpenGL Vendor: ") << glGetString(GL_VENDOR) << std::endl;
    message << _t("\nDirectories used by this program:") << std::endl;
    message << _t("Exe     : ") << options.paths.exedir << std::endl;
    message << _t("Data    : ") << options.paths.datadir << std::endl;
#ifdef DATAROOTDIR
    {
      std::string datarootdir = DATAROOTDIR;
      lpi::ensureDirectoryEndSlash(datarootdir);
      if(datarootdir != options.paths.datadir) {
        message << _t("DATAROOTDIR: ") << DATAROOTDIR << std::endl;
      }
    }
#endif
    message << _t("Plugins : ") << options.paths.plugindir << std::endl;
    message << _t("Settings: ") << persist.getURLIndicationForUser() << std::endl;
    lpi::gui::showMessageBox(*p.c, *p.modalFrameHandler, message.str());
  }

  if(m.menu_settings.itemClicked(m.menuindex_options, input))
  {
    showOptionsDialog(p, options, optionsLater, optionsWindow);
  }
  if(m.menu_settings.itemClicked(m.menuindex_shortcuts, input))
  {
    showShortcutsDialog(p, shortcutManager, shortcutsWindow);
  }


  size_t recentFile = p.recentMenu->itemClicked(input);
  if(recentFile < p.recentMenu->getNumFiles())
  {
    std::string filename = p.recentMenu->getFile(recentFile);

    if(paintings.activePainting && paintings.activePainting->window.getStatus() == PaintWindow::S_NEW)
    {
      openImageFile(paintings.activePainting->window, filename, p);
    }
    else
    {
      Painting* w = new Painting(p.optionsNow, p.screen->getGLContext());
      w->window.make(0, 0, 1, 1, guidrawer);
      if(openImageFile(w->window, filename, p)) addPaintWindow(p, paintings, w);
      else delete w;
    }
  }
  else if(recentFile == p.recentMenu->getClearIndex())
  {
    bool really = lpi::gui::getYesNoModal(*p.c, *p.modalFrameHandler, _t("Clear recent files list?"), _t("Yes"), _t("No"));
    if(really)
    {
      p.recentMenu->getRecent().clear();
      p.recentMenu->regenerate(guidrawer);
    }
  }

  if(p.toolbar.itemClicked(m.toolbar_new, input) || m.menu_file.itemClicked(m.menuindex_new, input))
  {
    doNew(p, paintings);
  }

  if(paintWindow)
  {
    if(p.toolbar2.itemClicked(m.toolbar_crop, input)) doFilter(p, *filters.getCropFilter(), paintings);
    if(p.toolbar2.itemClicked(m.toolbar_negate_rgb, input)) doFilter(p, *filters.getNegateRGBFilter(), paintings);
    if(p.toolbar2.itemClicked(m.toolbar_negate_mask, input)) doFilter(p, *filters.getNegateMaskFilter(), paintings);
    if(p.toolbar2.itemClicked(m.toolbar_clear_image, input)) doFilter(p, *filters.getClearImageFilter(), paintings);
    if(p.toolbar2.itemClicked(m.toolbar_gaussian, input)) doFilter(p, *filters.getGaussianBlurFilter(), paintings);
    if(p.toolbar2.itemClicked(m.toolbar_pixel_antialias, input)) doFilter(p, *filters.getPixelAntialiasFilter(), paintings);
    if(p.toolbar2.itemClicked(m.toolbar_blend_sel, input)) doFilter(p, *filters.getBlendSelectionFilter(), paintings);
    if(p.toolbar2.itemClicked(m.toolbar_wrap_50, input)) doFilter(p, *filters.getWrap50Filter(), paintings);
    if(p.toolbar2.itemClicked(m.toolbar_alpha_opaque, input)) doFilter(p, *filters.getAlphaOpaqueFilter(), paintings);
    if(p.toolbar2.itemClicked(m.toolbar_alpha_apply, input)) doFilter(p, *filters.getAlphaApplyFilter(), paintings);
    if(p.toolbar2.itemClicked(m.toolbar_normalize, input)) doFilter(p, *filters.getNormalizeFilter(), paintings);
    if(p.toolbar2.itemClicked(m.toolbar_brightness_contrast, input)) doFilter(p, *filters.getBrightnessContrastFilter(), paintings);
    if(p.toolbar2.itemClicked(m.toolbar_hue_saturation, input)) doFilter(p, *filters.getHueSaturationFilter(), paintings);
    if(p.toolbar2.itemClicked(m.toolbar_multiply_sla, input)) doFilter(p, *filters.getMultiplySLAFilter(), paintings);
    if(p.toolbar2.itemClicked(m.toolbar_greyscale, input)) doFilter(p, *filters.getGreyscaleFilter(), paintings);
    if(p.toolbar2.itemClicked(m.toolbar_flip_x, input)) doFilter(p, *filters.getFlipXFilter(), paintings);
    if(p.toolbar2.itemClicked(m.toolbar_flip_y, input)) doFilter(p, *filters.getFlipYFilter(), paintings);
    if(p.toolbar2.itemClicked(m.toolbar_rescale, input)) doFilter(p, *filters.getRescaleFilter(), paintings);
    if(p.toolbar2.itemClicked(m.toolbar_test_image, input)) doFilter(p, *filters.getTestImageFilter(), paintings);


    if(p.toolbar.itemClicked(m.toolbar_zoomin, input)) paintWindow->zoom(true);
    if(p.toolbar.itemClicked(m.toolbar_zoomout, input)) paintWindow->zoom(false);
    if(p.toolbar.itemClicked(m.toolbar_zoom1, input))
    {
      paintWindow->getPaint().resetZoom();
      paintWindow->centerPaint();
    }
    if(p.toolbar.itemClicked(m.toolbar_bgpattern, input))
    {
      cycle(optionsLater.bgpattern, 6, true);
    }
    if(p.toolbar.itemClicked(m.toolbar_bgpattern, input, lpi::RMB))
    {
      cycle(optionsLater.bgpattern, 6, false);
    }
    if(p.toolbar.itemClicked(m.toolbar_pixel_raster, input))
    {
      cycle(optionsLater.pixelGridIndex, 3, true);
    }
    if(p.toolbar.itemClicked(m.toolbar_pixel_raster, input, lpi::RMB))
    {
      cycle(optionsLater.pixelGridIndex, 3, false);
    }

    if(p.toolbar.itemClicked(m.toolbar_crosshair, input))
    {
      cycle(globalToolSettings.crosshairMode, 4, true);
    }
    if(p.toolbar.itemClicked(m.toolbar_crosshair, input, lpi::RMB))
    {
      cycle(globalToolSettings.crosshairMode, 4, false);
    }

    if(p.toolbar.itemClicked(m.toolbar_mask, input)) paintWindow->getPaint().enableMask(!paintWindow->getPaint().maskEnabled());
    if(p.toolbar.itemClicked(m.toolbar_maskcolor, input))
    {
      cycle(optionsLater.maskColorIndex, 6, true);
    }
    if(p.toolbar.itemClicked(m.toolbar_maskcolor, input, lpi::RMB))
    {
      cycle(optionsLater.maskColorIndex, 6, false);
    }

    if(p.toolbar.itemClicked(m.toolbar_fullscreen, input) && paintWindow->getPaint().canvasRGBA8.getU() > 0 && paintWindow->getPaint().canvasRGBA8.getV() > 0)
    {
      p.screen->cls(options.bgcolor);
      int sizex = paintWindow->getPaint().canvasRGBA8.getU();
      int sizey = paintWindow->getPaint().canvasRGBA8.getV();

      if(sizex == 0 || sizey * options.screenWidth / sizex > options.screenHeight)
      {
        sizex = sizex * options.screenHeight / sizey;
        sizey = options.screenHeight;
      }
      else
      {
        sizey = sizey * options.screenWidth / sizex;
        sizex = options.screenWidth;
      }

      drawTextureSizedCentered(((lpi::gui::GUIDrawerGL&)guidrawer).getScreen(), &paintWindow->getPaint().canvasRGBA8
                             , options.screenWidth / 2, options.screenHeight / 2
                             , sizex, sizey);
      p.screen->redraw();
      lpi::sleep(true, false);
    }

    (void)m.toolbar_cut;
    (void)m.toolbar_copy;

    if(p.toolbar.itemClicked(m.toolbar_paste_sel, input) || m.menu_edit.itemClicked(m.menuindex_paste_sel, input))
    {
      doPasteSel(p, paintings, globalToolSettings, tools);
    }
    if(p.toolbar.itemClicked(m.toolbar_copy, input) || m.menu_edit.itemClicked(m.menuindex_copy, input))
    {
      doCopy(p, paintWindow);
    }
    if( p.toolbar.itemClicked(m.toolbar_cut, input) || m.menu_edit.itemClicked(m.menuindex_cut, input))
    {
      doCopy(p, paintWindow);
      if(paintWindow->getPaint().hasSelection()) doFilter(p, *filters.getDeleteSelectionFilter(), paintings);
      else doFilter(p, *filters.getClearImageFilter(), paintings);
    }

    if(p.toolbar2.itemClicked(m.toolbar_repeat, input) && last_filter)
    {
      if(last_filter) doFilter(p, *last_filter, paintings);
    }

    if(p.toolbar2.itemClicked(m.toolbar_repeat_2, input) && last_filter_2)
    {
      doFilter(p, *last_filter_2, paintings);
    }
  }

  if(p.toolbar.itemClicked(m.toolbar_paste_image, input) || m.menu_edit.itemClicked(m.menuindex_paste_image, input))
  {
    doPaste(p, paintings, paintings.activePainting);
  }

  return true;
}

void warnAboutGenericDriver(lpi::gui::MainContainer& c, MyModalFrameHandler& modalFrameHandler)
{
  std::string renderer = reinterpret_cast< char const * >(glGetString(GL_RENDERER));

  if(stringContainsIgnoreCase(renderer, "generic") || stringContainsIgnoreCase(renderer, "Software Rasterizer"))
  {
    lpi::gui::showMessageBox(c, modalFrameHandler, "\
Warning! Slow video drivers detected. LodePaint will run slow. Please\n\
install the official drivers for your graphics card to make it run fast.\n\
\n\
LodePaint requires hardware accelerated OpenGL.\n\
\n\
Your current OpenGL renderer is:\n"
 + renderer + "\n\
\n\
If the renderer is 'generic' or 'software rasterizer', it is slow for OpenGL.\n\
To get a fast driver, please download and install it from the website of\n\
your graphics card vendor, or find the graphics card installation disk\n\
that came with your computer.",
"Warning, OpenGL Graphics Card Drivers Required");
  }
}

void initTranslations(const std::string& datadir)
{
  lpi::FileBrowse browse;
  std::vector<std::string> files;
  browse.getFiles(files, datadir + "Translations/");

  for(size_t i = 0; i < files.size(); i++)
  {
    if(lpi::extEqualsIgnoreCase(files[i], "txt"))
    {
      lpi::globalTranslate.addFile(datadir + "Translations/" + files[i]);
    }
  }

  lpi::globalTranslate.setFallBackLanguage("en");
  lpi::globalTranslate.setLanguage(options.language);
}

void createMainMenu(MenuToolbarIndices*& m, MainProgram& p, FiltersCollection& filters)
{
  m = new MenuToolbarIndices();
  fillMenusAndToolbars(*m, p, filters);
  m->mainmenu.resize(0, 0, options.screenWidth, 20);
  p.c->pushTop(&m->mainmenu, lpi::gui::Sticky(0.0,0, 0.0,m->mainmenu.getY0(), 1.0,0, 0.0,m->mainmenu.getY1()));
}

//also recreates toolbar.
void recreateMainMenu(MenuToolbarIndices*& m, MainProgram& p, FiltersCollection& filters)
{
  p.c->remove(&m->mainmenu);
  delete m;
  p.toolbar.clear();
  p.toolbar2.clear();
  createMainMenu(m, p, filters);
}

////////////////////////////////////////////////////////////////////////////////

void doMainLoop(MainProgram& p, lpi::InputSDL& input, lpi::gui::GUIDrawerGL& guidrawer,
                Paintings& paintings, FiltersCollection& filters, ToolsCollection& tools,
                lpi::gui::StatusBar& statusbar, MenuToolbarIndices* m,
                OptionsWindow& optionsWindow, ToolSettingWindow& toolSettingWindow,
                ShortcutWindow& shortcutsWindow, ShortCutManager& shortcutManager,
                lpi::Persist& persist,lpi::GameTime& fpsCounter,
                std::string& languageKey, std::stringstream& compiledate)
{
  int prev_tool_index = -1;
  int prev_frame_tool_index = -1;

  PaintWindow* paintWindow = 0;
  PaintWindow* prevTopPaintWindow = 0;

  lpi::gui::MainContainer& c = *p.c;
  lpi::ScreenGL& screen = *p.screen;

  IOperationFilter* last_filter = 0;
  IOperationFilter* last_filter_2 = 0;

  lpi::StandByUtil standby;


  for(;;) //the main loop
  {
    fpsCounter.update();

    if(languageKey != lpi::globalTranslate.getLanguageKey())
    {
      recreateMainMenu(m, p, filters);
      languageKey = lpi::globalTranslate.getLanguageKey();
    }

    if(!lpi::frame(false, true))
    {
      if(ensureReallyCloseAll(paintings, p)) break; //break out of main loop to close program
    }

    if(prev_frame_tool_index != p.toolBox.toolButtons.check())
    {
      prev_tool_index = prev_frame_tool_index;
    }
    if(prev_tool_index == -1) prev_tool_index = prev_frame_tool_index = p.toolBox.toolButtons.check();
    prev_frame_tool_index = p.toolBox.toolButtons.check();

    if(lpi::resizeEventHappened())
    {
      int oldw = p.screen->screenWidth();
      int oldh = p.screen->screenHeight();
      p.recreateScreen(lpi::resizeEventNewSizeX(), lpi::resizeEventNewSizeY(), (bool)options.fullscreen, (bool)options.resizable);
      //can be different than the event size values because it has a minimum size of 640*480
      int neww = p.screen->screenWidth();
      int newh = p.screen->screenHeight();
      updatePaintingsAfterProgramResize(oldw, oldh, neww, newh, paintings);
    }

    for(size_t i = 0; i < paintings.paintings.size(); i++) paintings[i]->window.setColorMod(options.drawingWindowColor);
    p.colorWindow->setColorMod(options.drawingWindowColor);
    p.toolBox.setColorMod(options.drawingWindowColor);
    p.overviewWindow->setColorMod(options.drawingWindowColor);
    toolSettingWindow.setColorMod(options.drawingWindowColor);
    optionsWindow.setColorMod(options.drawingWindowColor);
    shortcutsWindow.setColorMod(options.drawingWindowColor);
    guidrawer.getInternal().windowTopColor = options.windowTopColor;

    PaintWindow* topPaintWindow = 0;
    for(size_t i = 0; i < c.size(); i++) //find topmost (= active) paint window
    {
      PaintWindow* e = dynamic_cast<PaintWindow*>(c.getElement(i));
      //below is for when you start pressing mouse, so drawing with pen or brush works immediatly
      if(e && (e->mouseDownHere(input, lpi::LMB) || e->mouseDownHere(input, lpi::MMB) || e->mouseDownHere(input, lpi::RMB))) paintWindow = e;
      //the topWindow system is to detect brand new active windows by using new image or open image
      if(e) topPaintWindow = e;
    }
    if(topPaintWindow != prevTopPaintWindow) paintWindow = topPaintWindow;
    prevTopPaintWindow = topPaintWindow;
    paintings.activePainting = 0;
    for(size_t i = 0; i < paintings.paintings.size(); i++) if(&paintings[i]->window == paintWindow) paintings.activePainting = paintings[i];

    p.colorWindow->setLeftColor(globalToolSettings.leftColor);
    p.colorWindow->setRightColor(globalToolSettings.rightColor);

    c.handle(input);
    windowInsideKeeper.handle(input, options.screenWidth, options.screenHeight);

    p.overviewWindow->setPaintWindow(paintWindow);

    if(paintWindow)
    {
      handlePaint(*paintWindow, paintings.activePainting->undoStack, input);
      paintings.activePainting->undoStack.trimMemorySize(options.undoSize * 1024 * 1024);
      std::string text = std::string("undo memory size: ")
                       + lpi::valtostr(paintings.activePainting->undoStack.calculateMemorySize() / 1024)
                       + "K of "
                       + lpi::valtostr(options.undoSize  * 1024)
                       + "K";
      statusbar.setZoneText(0, text);

      //making it smaller/bigger using the dragging corners
      if(paintWindow->pc.hasChanged())
      {
        static OperationAddRemoveBordersInternal op(globalToolSettings);
        int dx0 = paintWindow->pc.dx0;
        int dy0 = paintWindow->pc.dy0;
        int dx1 = paintWindow->pc.dx1;
        int dy1 = paintWindow->pc.dy1;
        static const int MAXCHANGE = 4000; //avoid easily accidently creating huge memory hogs
        if(std::abs(dx0) < MAXCHANGE && std::abs(dx1) < MAXCHANGE && std::abs(dy0) < MAXCHANGE && std::abs(dy1) < MAXCHANGE)
        {
          op.setParameters(dx0, dx1, dy0, dy1);
          doFilter(p, op, paintings);
          double z = paintWindow->getPaint().getZoom();
          paintWindow->getPaint().move(-dx0 * z, -dy0 * z);
        }
      }
    }

    std::stringstream fpsText;
    fpsText << "fps: " << (int)fpsCounter.fps();
    statusbar.setZoneText(1, fpsText.str());

    screen.cls(options.bgcolor);
    c.draw(guidrawer);
    if(paintWindow) drawPaint(*paintWindow, guidrawer);


    standby.handle(input);
    //if(standby.isStandBy(input)) guidrawer.drawText("STANDBY", options.screenWidth / 2 - 32, options.screenHeight / 2 - 4);

    size_t selectedTool = p.toolBox.toolButtons.check();
    if(selectedTool < tools.getNumTools())
    {
      if(tool != tools.getTool(selectedTool))
      {
        selectTool(tools.getTool(selectedTool), toolSettingWindow);
        // Select none if other than selection tool selected
        //TODO: this is for removing selection when selecting other tool. If multiple selection tools exist, this must go behind a
        // check like "!newtool->isSelectionTool()". If using other tools on top of a selection is supported some day, this "feature"
        // must be removed entirely.
        bool seltool = dynamic_cast<Tool_SelRect*>(tools.getTool(selectedTool)) != 0;
        if(paintWindow && paintWindow->getPaint().hasSelection()
            && !seltool) doFilter(p, *filters.getSelectNoneFilter(), paintings);
      }
    }
    globalToolSettings.leftColor = p.colorWindow->getLeftColord();
    globalToolSettings.rightColor = p.colorWindow->getRightColord();



    if(paintWindow)
    {
      handleFilterMenu(last_filter, last_filter_2, m->menu_image, m->submenu_image, filters.getImageMenuFilters(), input, p, paintings);
      handleFilterMenu(last_filter, last_filter_2, m->menu_filters, m->submenu_filters, filters.getFiltersMenuFilters(), input, p, paintings);
      handleFilterMenu(last_filter, last_filter_2, m->menu_plugins, m->submenu_plugins, filters.getPluginsMenuFilters(), input, p, paintings);
    }

    if(!handleMenusAndToolbar(*m, paintings, paintWindow, prevTopPaintWindow
                            , p, input, persist
                            , filters
                            , compiledate
                            , optionsWindow
                            , shortcutsWindow, shortcutManager
                            , last_filter, last_filter_2
                            , tools))
    {
      if(ensureReallyCloseAll(paintings, p)) break; //break out of main loop to close program
    }

    /*if(p.toolbar.itemClicked(toolbar_filter_on_all, input))
    {
      doOnAll = !doOnAll;
    }*/
    doOnAll = p.toolbar.toggleEnabled(m->toolbar_filter_on_all);



    //the shortcuts
    //bool ctrl = input.keyDown(SDLK_LCTRL) || input.keyDown(SDLK_RCTRL);
    bool alt = input.keyDown(SDLK_LALT) || input.keyDown(SDLK_RALT);
    //bool shift = input.keyDown(SDLK_LSHIFT) || input.keyDown(SDLK_RSHIFT);
    int keyFocus = c.getKeyboardFocus();
    if(!(keyFocus & lpi::gui::KM_TEXT) && !(keyFocus & lpi::gui::KM_CTRL))
    {
      ShortCut shortcut = shortcutManager.getShortCut(input);
      switch(shortcut)
      {
        case SC_TOOL_PEN: p.toolBox.selectTool(tools.getShortcutTool('p')); break;
        case SC_TOOL_BRUSH: p.toolBox.selectTool(tools.getShortcutTool('b')); break;
        case SC_TOOL_PIXEL_BRUSH: p.toolBox.selectTool(tools.getShortcutTool('i')); break;
        case SC_TOOL_ERASER: p.toolBox.selectTool(tools.getShortcutTool('e')); break;
        case SC_TOOL_COLORPICKER: p.toolBox.selectTool(tools.getShortcutTool('c')); break;
        case SC_TOOL_FLOODFILL: p.toolBox.selectTool(tools.getShortcutTool('f')); break;
        case SC_TOOL_LINE: p.toolBox.selectTool(tools.getShortcutTool('l')); break;
        case SC_TOOL_SELRECT: p.toolBox.selectTool(tools.getShortcutTool('s')); break;
        case SC_TOOL_PREVIOUS: p.toolBox.selectTool(prev_tool_index); break;

        case SC_OPERATION_CROP: if(paintWindow) doFilter(p, *filters.getCropFilter(), paintings); break;
        case SC_OPERATION_DELETE_SELECTION: if(paintWindow) doFilter(p, *filters.getDeleteSelectionFilter(), paintings); break;
        case SC_OPERATION_SELECT_ALL: if(paintWindow) doFilter(p, *filters.getSelectAllFilter(), paintings); break;
        case SC_OPERATION_SELECT_NONE: if(paintWindow) doFilter(p, *filters.getSelectNoneFilter(), paintings); break;
        case SC_SWAP_FG_BG: std::swap(globalToolSettings.leftColor, globalToolSettings.rightColor); break;
        case SC_NEGATE_FG_BG:
        {
          globalToolSettings.leftColor.negateRGB();
          globalToolSettings.rightColor.negateRGB();
          break;
        }
        case SC_DEFAULT_FG_BG:
        {
          globalToolSettings.leftColor = lpi::RGBd_Black;
          globalToolSettings.rightColor = lpi::RGBd_White;
          break;
        }
        case SC_CUT:
        {
          if(paintWindow)
          {
            doCopy(p, paintWindow);
            if(paintWindow->getPaint().hasSelection()) doFilter(p, *filters.getDeleteSelectionFilter(), paintings);
            else doFilter(p, *filters.getClearImageFilter(), paintings);
          }
          break;
        }
        case SC_COPY: if(paintWindow) doCopy(p, paintWindow); break;
        case SC_PASTE_IMAGE: doPaste(p, paintings, paintings.activePainting); break;
        case SC_PASTE_SELECTION: if(paintWindow) doPasteSel(p, paintings, globalToolSettings, tools); break;
        case SC_UNDO: if(paintWindow) paintings.activePainting->undoStack.undo(paintWindow->getPaint()); break;
        case SC_REDO: if(paintWindow) paintings.activePainting->undoStack.redo(paintWindow->getPaint()); break;
        case SC_OPEN: doOpen(p, paintings); break;
        case SC_NEW:
        {
          doNew(p, paintings);
          break;
        }
        case SC_SAVE: if(paintWindow) save(*paintWindow, p); break;
        case SC_RELOAD:
        {
          //TODO: since you're reloading the image and forgetting all changes, ask confirmation if the image was changed
          if(paintWindow && paintWindow->hasFilename()) openImageFile(*paintWindow, paintWindow->getFilename(), p);
          break;
        }
        case SC_ZOOM_IN: if(paintWindow) paintWindow->zoom(true, input); break;
        case SC_ZOOM_OUT: if(paintWindow) paintWindow->zoom(false, input); break;
        case SC_DYN_CHANGE_UP:
        {
          double amount1 = options.dyn.amount1;
          double amount2 = options.dyn.amount2;
          dynChangeSettings(globalToolSettings, options.dyn.change1, amount1);
          dynChangeSettings(globalToolSettings, options.dyn.change2, amount2);
          break;
        }
        case SC_DYN_CHANGE_DOWN:
        {
          double amount1 = -options.dyn.amount1;
          double amount2 = -options.dyn.amount2;
          dynChangeSettings(globalToolSettings, options.dyn.change1, amount1);
          dynChangeSettings(globalToolSettings, options.dyn.change2, amount2);
          break;
        }
        default: break;
      }
    }

    if(alt)
    {
      double amount = 0;
      if(input.mouseWheelUp())
        amount++;
      if(input.mouseWheelDown())
        amount--;
      if(amount != 0)
      {
        double amount1 = amount * options.dyn.amount1;
        double amount2 = amount * options.dyn.amount2;
        dynChangeSettings(globalToolSettings, options.dyn.change1, amount1);
        dynChangeSettings(globalToolSettings, options.dyn.change2, amount2);
      }
    }

    for(size_t i = 0; i < paintings.paintings.size(); i++)
    {
      if(paintings[i]->window.closeButtonClicked(input))
      {
        doClose(paintWindow, prevTopPaintWindow, p, paintings, paintings[i]);
        break;
      }
      if(paintings[i]->window.maximizeButtonClicked(input))
      {
        PaintWindow& w = paintings[i]->window;
        int x0 = w.getX0();
        int y0 = w.getY0();
        int x1 = w.getX1();
        int y1 = w.getY1();
        maximizePaintWindow(*paintings[i], p);
        if(w.getX0() == x0 && w.getY0() == y0 && w.getX1() == x1 && w.getY1() == y1)
        {
          // Already maximized, for convenience make it smaller (if the painting is small) to fit instead.
          fitWindowAroundPainting(w, p);
        }
        break;
      }
    }

    optionsLaterToOptionsNow(options, optionsLater);

    //lpi::graphicalKeyBoardNumberTest(guidrawer);
    screen.redraw();
    processClipboardEvents();
  }
}


////////////////////////////////////////////////////////////////////////////////

int doMain(const std::string& commandLineFileName)
{
  paintDebugMessage("Starting LodePaint");

  std::stringstream compiledate;
  compiledate << __DATE__ << " " << __TIME__;
  paintDebugMessage("Version: " + LODEPAINT_VERSION);
  paintDebugMessage("Compile Date: " + compiledate.str());
  std::string OS = "Unknown";

#if defined(LPI_OS_WINDOWS)
    OS = "MS Windows";
#elif defined(LPI_OS_LINUX)
    OS = "Linux";
#endif
  paintDebugMessage("OS: " + OS);

  // it may already be filled in by the command line options
  if(options.paths.datadir.empty()) {
#ifdef DATAROOTDIR
    options.paths.datadir = DATAROOTDIR;
    lpi::ensureDirectoryEndSlash(options.paths.datadir);
    std::string testfile0 = "Skins/default/icons_toolbar.png";
    std::string testfile1 = "Translations/en.txt";
    if(!lpi::fileExists(options.paths.datadir + testfile0) && !lpi::fileExists(options.paths.datadir + testfile1)) {
      std::string alt = options.paths.exedir; // if not found in default path (e.g. /usr/share), try alternative options
      lpi::ensureDirectoryEndSlash(alt);
      if(lpi::fileExists(alt + testfile0) && lpi::fileExists(alt + testfile1)) {
        std::cout << "WARNING: Didn't find " << options.paths.datadir << ", using " << alt << " instead as Data dir" << std::endl;
        options.paths.datadir = alt;
      }
    }
#else
    options.paths.datadir = options.paths.exedir;
#endif
  }

  paintDebugMessage("Loading persistent data");
  lpi::Persist persist("LodePaint", false);
  paintDebugMessage("Persistent data loaded");
  readOptionsFromPersist(options, persist, "options");

  //needs to be done very first (but after loading options since that contains the language), so that strings are ready
  paintDebugMessage("Loading language data");
  initTranslations(options.paths.datadir);
  paintDebugMessage("Language data loaded");

  ShortCutManager shortcutManager;
  shortcutManager.readFromPersist(persist, "shortcuts");
  cmdoptionsToOptions(options, cmdoptions);
  options.screenWidth = lpi::clamp(options.screenWidth, 640, 2048);
  options.screenHeight = lpi::clamp(options.screenHeight, 480, 1536);
  optionsLater = options;

  readPaintSettingsFromPersist(globalToolSettings, persist, "paintSettings");

  lpi::InputSDL input;
  MainProgram p(options.screenWidth, options.screenHeight, (bool)options.fullscreen, (bool)options.resizable, "LodePaint", options, optionsLater);
  lpi::gui::GUIDrawerGL& guidrawer = *p.guidrawer;
  //lpi::FileBrowse& filebrowser = *p.filebrowser;
  lpi::gui::MainContainer& c = *p.c;
  lpi::gui::RecentFilesMenu& recentMenu = *p.recentMenu;
  MyModalFrameHandler& modalFrameHandler = *p.modalFrameHandler;

  contextForPaintTextures = p.screen->getGLContext();

  warnAboutGenericDriver(c, modalFrameHandler);

  persistToFileDialog(p.fileDialogPersist, persist, "filedialog");
  persistToRecentFiles(recentMenu.getRecent(), persist, "recent.file");
  recentMenu.regenerate(guidrawer);

  p.overviewWindow->moveTo(10, 600);

  p.colorWindow->setLeftColor(globalToolSettings.leftColor);
  p.colorWindow->setRightColor(globalToolSettings.rightColor);
  c.pushTop(p.colorWindow);
  p.colorWindow->palettesFromPersist(persist, "palette");

  c.pushTop(p.overviewWindow);

  ToolSettingWindow toolSettingWindow(p.colorWindow->getX1() + 4, p.colorWindow->getY0(), guidrawer);
  c.pushTop(&toolSettingWindow);

  OptionsWindow optionsWindow(guidrawer);
  optionsWindow.moveCenterTo(options.screenWidth / 2, options.screenHeight / 2);

  ShortcutWindow shortcutsWindow(shortcutManager, guidrawer);
  shortcutsWindow.moveCenterTo(options.screenWidth / 2, options.screenHeight / 2);

  std::vector<lpi::HTexture> builtInGUIIcons;
  builtInGUIIcons.resize(lpi::gui::GI_END_DONT_USE);
  for(size_t i = 0; i < builtInGUIIcons.size(); i++)
  {
    builtInGUIIcons[i].texture = guidrawer.createTexture();
    guidrawer.createIcon(*builtInGUIIcons[i].texture, (lpi::gui::GUIIcon)i);
  }

  ToolsCollection tools(globalToolSettings, *p.colorWindow, guidrawer);


  paintDebugMessage("Executable Directory: " + options.paths.exedir);

  if(!trySkin(p, optionsLater, options.skin))
  {
    if(!loadSkinInitial(p, options.paths.datadir + "Skins/default/"))
    {
      return 1;
    }
  }

  initGuiDrawerStyle(p);


  p.toolBox.make(10, 44, guidrawer, tools, p);
  c.pushTop(&p.toolBox);

  // it may already have been filled in by command line argument
  if(options.paths.plugindir.empty()) {
    options.paths.plugindir = optionsLater.paths.plugindir = options.paths.exedir + "Plugins";
    p.filebrowser->ensureDirectoryEndSlash(options.paths.plugindir);
    if(!options.pathsOverride.plugindir.empty())
    {
      options.paths.plugindir = optionsLater.paths.plugindir = options.pathsOverride.plugindir;
    }
  }

  Paintings paintings;

  FiltersCollection filters(globalToolSettings, guidrawer);

  p.formats->addMainBuiltInFormats();
  p.formats->addPluginFormats(*p.filebrowser, options.paths.plugindir);
  p.formats->addOtherBuiltInFormats();
  filters.addPluginFilters(*p.filebrowser, options.paths.plugindir, globalToolSettings, guidrawer);

  MenuToolbarIndices* m;

  createMainMenu(m, p, filters);

  p.toolbar.resize(0, m->mainmenu.getY1(), options.screenWidth, m->mainmenu.getY1() + p.toolbar.getSizeY());
  c.pushTop(&p.toolbar, lpi::gui::Sticky(0.0,0, 0.0,p.toolbar.getY0(), 1.0,0, 0.0,p.toolbar.getY1()));

  p.toolbar2.resize(0, p.toolbar.getY1(), options.screenWidth, p.toolbar.getY1() + p.toolbar2.getSizeY());
  c.pushTop(&p.toolbar2, lpi::gui::Sticky(0.0,0, 0.0,p.toolbar2.getY0(), 1.0,0, 0.0,p.toolbar2.getY1()));

  lpi::gui::StatusBar statusbar;
  statusbar.setNumZones(2);
  statusbar.setZoneSize(0, 1.0, 0);
  statusbar.setZoneSize(1, 0.0, 128);
  statusbar.resize(0, options.screenHeight - 16, options.screenWidth, options.screenHeight);
  c.pushTop(&statusbar, lpi::gui::Sticky(0.0,0, 1.0,-16, 1.0,0, 1.0,0));

  windowInsideKeeper.add(p.overviewWindow);
  windowInsideKeeper.add(p.colorWindow);
  windowInsideKeeper.add(&toolSettingWindow);
  windowInsideKeeper.add(&optionsWindow);
  //windowInsideKeeper.add(&paintWindow);
  windowInsideKeeper.add(&p.toolBox);

  //The final positions of the windows
  //p.toolBox.moveTo(2, p.toolbar2.getY1() + 2);
  lpi::gui::Element* prev;
  //p.overviewWindow->moveTo(options.screenWidth - 2 - p.colorWindow->getSizeX(), p.toolbar2.getY1() + 2); //RIGHT
  p.overviewWindow->moveTo(2, p.toolbar2.getY1() + 2); //LEFT
  prev = p.overviewWindow;
  p.colorWindow->resize(prev->getX0(), prev->getY1() + 2, prev->getX1(), prev->getY1() + 2 + p.colorWindow->getSizeY());
  prev = p.colorWindow;
  p.toolBox.resize(prev->getX0(), prev->getY1() + 2, prev->getX1(), prev->getY1() + 2 + p.toolBox.getSizeY());
  prev = &p.toolBox;
  toolSettingWindow.resize(prev->getX0(), prev->getY1() + 2, prev->getX1(), prev->getY1() + 2 + toolSettingWindow.getSizeY());

  paintDebugMessage("Creating blank painting");
  Painting* initialPainting = new Painting(p.optionsNow, p.screen->getGLContext());
  initialPainting->window.make(0, 0, options.newImageSizeX, options.newImageSizeY, guidrawer);
  addPaintWindow(p, paintings, initialPainting);
  //initialPainting->window.centerPaint();
  //fitWindowAroundPainting(initialPainting->window, p);
  maximizePaintWindow(*initialPainting, p);
  paintDebugMessage("Blank painting created");

  if(!commandLineFileName.empty())
  {
    paintDebugMessage("Opening command line argument image file: " + commandLineFileName);
    openImageFile(paintings[0]->window, commandLineFileName, p);
  }

  lpi::GameTime fpsCounter;
  fpsCounter.init_fps();

  std::string languageKey = lpi::globalTranslate.getLanguageKey();

  paintDebugMessage("Starting main loop");
  doMainLoop(p, input, guidrawer, paintings, filters, tools, statusbar, m,
             optionsWindow, toolSettingWindow, shortcutsWindow, shortcutManager,
             persist, fpsCounter, languageKey, compiledate);
  //paintDebugMessage("Main loop finished");


  writeOptionsToPersist(persist, optionsLater, "options");
  recentFilesToPersist(persist, recentMenu.getRecent(), "recent.file");
  fileDialogToPersist(persist, p.fileDialogPersist, "filedialog");
  p.colorWindow->palettesToPersist(persist, "palette");
  //paintDebugMessage("Writing persistent data");
  writePaintSettingsToPersist(persist, globalToolSettings, "paintSettings");
  //paintDebugMessage("Persistent data written");
  shortcutManager.writeToPersist(persist, "shortcuts");

  paintDebugMessage("Main Loop finished successfully");

  textures_tools.clear();
  textures_filters.clear();
  textures_toolbar.clear();

  delete m;

  return 0;
}

int main(int argc, char* argv[])
{
  try
  {
    std::string commandLineFileName;
    if(!processCommandLine(commandLineFileName, options, cmdoptions, argc, argv))
    {
      return 0;
    }
    return doMain(commandLineFileName);
  }
  catch(...)
  {
    paintDebugMessage("Unhandled exception was thrown");
  }
}




/*
Recipes:
Interesting things you can do in LodePaint by combining several filters:

*) spacy texture
-first generate random rgb noise
-then do interpolating blur with radius 22 or so (gaussian blur goes too, or both)
-then do "spooky neon" 3 times in a row (which in itself involves bitshifts and gray codes)

*) animal-fur like texture

The following steps in order (default settings unless otherwise noted):
-fg and bg color black and white
-marble
-50% clouds blended with that (opacity 0.5)
-sepia
-pointillism
-negate lightness

*) "game of life" filter on photos
-open a photo
-run the game of life filter many times, 10-20 times
-run the game of life extinguish filter a few times
-if needed, run the "adaptive median filter" a few times to remove more artefacts
sometimes, the result looks very artistic

*) optical illusion
-create new 512x512 pixel image
-do xor texture filter
-then do twice spooky neon
-> the image appears curvy instead of square

*) List of filters that have RGB(A) input but produce 2-color output
-threshold
-monochrome (dithered)
-canny edge detector, but only if you do grayscale operation before it
-contour
-set 1-bit FG BG
-...

*) List of filters that keeps the same colors in the image, but shuffle all pixels around
-mirrorring
-rotating with multiples of 90 degrees
-wrapping shift
-sort colors by #hex value, hilbert space, or usage count
-random shuffle and random walk

*) List of filters that, if you run them N times, gives the original image again
-mirroring: N=2
-rotate 90 degrees: N=4
-negate RGB(A): N=2
-bit shift 1: N=8, bit shift 4: N=2, mirror bits: N=2
-spooky neon: N=8
-binary to/from gray code: N=4
-binary to/from gray code RGBA: N=32
-wrapping shift (50%): N=2
-...

*) List of filters intended for pixel art
-Pixel Art Scaling
-Pixel Antialias
-Pixel Shadow
-Pixel Border
-Pixel Edge Detect
-Swap Colors

*) Greyscale, dan emboss, dan tone map (histogram adjustment) ==> hoge contrast emboss, is intressant

*) Generate random noise (greyscale). Then gaussian blur. Then twice "Spectrum": interesting pattern, somewhat biological.
   After that, try alternating between gaussian blur and Spectrum for more variants of the pattern.
   Make the gaussian blur "wrapping" to have this as a tileable texture.
   Variations:
   -use "Spooky Neon" instead of "Spectrum"
   -instead of random noise, draw something random with a brush, and still use "wrapping" and gaussian blur with large radius to have it seamless
*/
