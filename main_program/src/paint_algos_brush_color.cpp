/*
LodePaint

Copyright (c) 2009 Lode Vandevenne
All rights reserved.

This file is part of LodePaint.

LodePaint is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LodePaint is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LodePaint.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "paint_algos_brush_color.h"
#include "paint_algos.h"

#include "lpi/gio/lpi_draw2d.h"
#include "lpi/lpi_math.h"
#include "lpi/lpi_math2d.h"

#include <algorithm>

////////////////////////////////////////////////////////////////////////////////


ExtraBrushOptions::ExtraBrushOptions()
: affect_r(true)
, affect_g(true)
, affect_b(true)
, affect_a(true)
, erase_content(true)
{
}

ExtraBrushOptions::ExtraBrushOptions(bool affect_alpha)
: affect_r(true)
, affect_g(true)
, affect_b(true)
, affect_a(affect_alpha)
, erase_content(true)
{
}

////////////////////////////////////////////////////////////////////////////////


void pset(unsigned char* buffer, int buffer_w, int buffer_h, int x, int y, const lpi::ColorRGB& color, int centerx, int centery, const Brush& brush, const ExtraBrushOptions& extra)
{
  if(x < 0 || y < 0 || x >= buffer_w || y >= buffer_h) return;

  int dx = x - centerx;
  int dy = y - centery;

  double opacity = brush.opacity;

  if(brush.softness != 0)
  {
    double distance = 0.0;
    if(brush.shape == Brush::SHAPE_SQUARE) //square
    {
      distance = std::max((dx > 0 ? dx : -dx), (dy > 0 ? dy : -dy)) / (brush.size / 2.0);
    }
    else if(brush.shape == Brush::SHAPE_ROUND) //round
    {
      distance = (dx * dx + dy * dy) / ((brush.size / 2.0) * (brush.size / 2.0)); //squared version
      //distance = std::sqrt(dx * dx + dy * dy) / (brush.size / 2); //sqrt version
    }
    else if(brush.shape == Brush::SHAPE_DIAMOND)
    {
      distance = ((dx > 0 ? dx : -dx) + (dy > 0 ? dy : -dy)) / (brush.size / 2.0);
    }

    double antidistance = 1.0 - distance;
    if(antidistance < 0.0) antidistance = 0.0;

    antidistance /= brush.softness;
    if(antidistance < 0.0) antidistance = 0.0;
    if(antidistance > 1.0) antidistance = 1.0;

    opacity = brush.opacity * antidistance;
  }

  int bufferPos = 4 * buffer_w * y + 4 * x;

  lpi::ColorRGB old;
  old.r = buffer[bufferPos + 0];
  old.g = buffer[bufferPos + 1];
  old.b = buffer[bufferPos + 2];
  old.a = buffer[bufferPos + 3];

  lpi::ColorRGB result;
  if(old.r == color.r) result.r = old.r; else result.r = int(old.r * (1.0 - opacity) + color.r * opacity);
  if(old.g == color.g) result.g = old.g; else result.g = int(old.g * (1.0 - opacity) + color.g * opacity);
  if(old.b == color.b) result.b = old.b; else result.b = int(old.b * (1.0 - opacity) + color.b * opacity);
  if(old.a == color.a) result.a = old.a; else result.a = int(old.a * (1.0 - opacity) + color.a * opacity);
  
  if(extra.affect_r) buffer[bufferPos + 0] = result.r;
  if(extra.affect_g) buffer[bufferPos + 1] = result.g;
  if(extra.affect_b) buffer[bufferPos + 2] = result.b;
  if(extra.affect_a) buffer[bufferPos + 3] = result.a;

  if(extra.erase_content && buffer[bufferPos + 3] == 0)
  {
    buffer[bufferPos + 0] = color.r;
    buffer[bufferPos + 1] = color.g;
    buffer[bufferPos + 2] = color.b;
  }
}

void horLineBrush(unsigned char* buffer, int buffer_w, int buffer_h, int y, int x1, int x2, const lpi::ColorRGB& color, int centerx, int centery, const Brush& brush, const ExtraBrushOptions& extra)
{
  if(x2 < x1) {x1 += x2; x2 = x1 - x2; x1 -= x2;} //swap x1 and x2, x1 must be the leftmost endpoint
  if(x2 < 0 || x1 >= buffer_w || y < 0 || y >= buffer_h) return; //no single point of the line is on screen

  if(x1 < 0) x1 = 0;
  if(x2 > buffer_w) x2 = buffer_w;

  for(int x = x1; x < x2; x++)
  {
    pset(buffer, buffer_w, buffer_h, x, y, color, centerx, centery, brush, extra);
  }
}

void drawBrushDisk(unsigned char* buffer, int buffer_w, int buffer_h, int xc, int yc, const lpi::ColorRGB& color, const Brush& brush, const ExtraBrushOptions& extra)
{
  int radius = brush.size / 2;
  if(xc + radius < 0 || xc - radius >= buffer_w || yc + radius < 0 || yc - radius >= buffer_h) return; //every single pixel outside screen, so don't waste time on it
  int x = 0;
  int y = radius;
  int p = 3 - (radius << 1);
  int a, b, c, d, e, f, g, h;
  int pb = xc - radius, pd = xc - radius; //previous values: to avoid drawing horizontal lines multiple times
  while (x <= y)
  {
    // write data
    a = xc + x;
    b = yc + y;
    c = xc - x;
    d = yc - y;
    e = xc + y;
    f = yc + x;
    g = xc - y;
    h = yc - x;
    /*if(b != pb) horLineBrush(buffer, buffer_w, buffer_h, b, a, c, color, brush.opacity);
    if(d != pd) horLineBrush(buffer, buffer_w, buffer_h, d, a, c, color, brush.opacity);
    if(f != b)  horLineBrush(buffer, buffer_w, buffer_h, f, e, g, color, brush.opacity);
    if(h != d && h != f) horLineBrush(buffer, buffer_w, buffer_h, h, e, g, color, brush.opacity);*/

    if(b != pb) horLineBrush(buffer, buffer_w, buffer_h, b, a, c, color, xc, yc, brush, extra);
    if(d != pd) horLineBrush(buffer, buffer_w, buffer_h, d, a, c, color, xc, yc, brush, extra);
    if(f != b)  horLineBrush(buffer, buffer_w, buffer_h, f, e, g, color, xc, yc, brush, extra);
    if(h != d && h != f) horLineBrush(buffer, buffer_w, buffer_h, h, e, g, color, xc, yc, brush, extra);

    pb = b;
    pd = d;
    if(p < 0) p += (x++ << 2) + 6;
    else p += ((x++ - y--) << 2) + 10;
  }
}

void drawBrushDiamond(unsigned char* buffer, int buffer_w, int buffer_h, int xc, int yc, const lpi::ColorRGB& color, const Brush& brush, const ExtraBrushOptions& extra)
{
  int radius = brush.size / 2;

  for(int y = yc - radius; y < yc - radius + brush.size; y++)
  {
    int dist = y - yc;
    if(dist < 0) dist = -dist;
    int size = brush.size - dist * 2;
    int x0 = xc - size / 2;
    int x1 = xc - size / 2 + size;

    horLineBrush(buffer, buffer_w, buffer_h, y, x0, x1, color, xc, yc, brush, extra);
  }
}

void drawBrushSquare(unsigned char* buffer, int buffer_w, int buffer_h, int xc, int yc, const lpi::ColorRGB& color, const Brush& brush, const ExtraBrushOptions& extra)
{
  int radius = brush.size / 2;
  for(int y = yc - radius; y < yc - radius + brush.size; y++) horLineBrush(buffer, buffer_w, buffer_h, y, xc - radius, xc - radius + brush.size, color, xc, yc, brush, extra);
}

void drawBrushShape(unsigned char* buffer, int buffer_w, int buffer_h, int xc, int yc, const lpi::ColorRGB& color, const Brush& brush, const ExtraBrushOptions& extra)
{
  if(brush.shape == Brush::SHAPE_SQUARE) drawBrushSquare(buffer, buffer_w, buffer_h, xc, yc, color, brush, extra);
  else if(brush.shape == Brush::SHAPE_ROUND) drawBrushDisk(buffer, buffer_w, buffer_h, xc, yc, color, brush, extra);
  else if(brush.shape == Brush::SHAPE_DIAMOND) drawBrushDiamond(buffer, buffer_w, buffer_h, xc, yc, color, brush, extra);
}

void pset(unsigned char* buffer, size_t u, size_t v, size_t u2, int x, int y, const lpi::ColorRGB& color, double opacity, const ExtraBrushOptions& extra)
{
  if(x < 0 || y < 0 || x >= (int)u || y >= (int)v) return;

  int bufferPos = 4 * u2 * y + 4 * x;

  lpi::ColorRGB result = color;

  if(opacity < 1.0)
  {
    lpi::ColorRGB old;
    old.r = buffer[bufferPos + 0];
    old.g = buffer[bufferPos + 1];
    old.b = buffer[bufferPos + 2];
    old.a = buffer[bufferPos + 3];

    //result = (old & (1.0 - opacity)) | (color & opacity);

    int o = (int)(opacity * 255);
    result.r = (old.r * (255 - o) + color.r * o) / 255;
    result.g = (old.g * (255 - o) + color.g * o) / 255;
    result.b = (old.b * (255 - o) + color.b * o) / 255;
    result.a = (old.a * (255 - o) + color.a * o) / 255;
  }

  if(extra.affect_r) buffer[bufferPos + 0] = result.r;
  if(extra.affect_g) buffer[bufferPos + 1] = result.g;
  if(extra.affect_b) buffer[bufferPos + 2] = result.b;
  if(extra.affect_a) buffer[bufferPos + 3] = result.a;
}

//this one does NOT check if the line is inside the buffer! (hence only width, not height, as parameter)
void bresenhamLine(unsigned char* buffer, size_t u, size_t v, size_t u2, int x0, int y0, int x1, int y1, const lpi::ColorRGB& color, double opacity, const ExtraBrushOptions& extra, bool include_last_pixel)
{
  //draw the line with bresenham
  int deltax = (int)std::abs((double)(x1 - x0));    // The difference between the x's
  int deltay = (int)std::abs((double)(y1 - y0));    // The difference between the y's
  int x = x0;           // Start x off at the first pixel
  int y = y0;           // Start y off at the first pixel
  int xinc0, xinc1, yinc0, yinc1, den, num, numadd, numpixels, curpixel;

  if(x1 >= x0)         // The x-values are increasing
  {
    xinc0 = 1;
    xinc1 = 1;
  }
  else              // The x-values are decreasing
  {
    xinc0 = -1;
    xinc1 = -1;
  }
  if (y1 >= y0)         // The y-values are increasing
  {
    yinc0 = 1;
    yinc1 = 1;
  }
  else              // The y-values are decreasing
  {
    yinc0 = -1;
    yinc1 = -1;
  }
  if(deltax >= deltay)     // There is at least one x-value for every y-value
  {
    xinc0 = 0;          // Don't change the x when numerator >= denominator
    yinc1 = 0;          // Don't change the y for every iteration
    den = deltax;
    num = deltax / 2;
    numadd = deltay;
    numpixels = deltax;     // There are more x-values than y-values
  }
  else              // There is at least one y-value for every x-value
  {
    xinc1 = 0;          // Don't change the x for every iteration
    yinc0 = 0;          // Don't change the y when numerator >= denominator
    den = deltay;
    num = deltay / 2;
    numadd = deltax;
    numpixels = deltay;     // There are more y-values than x-values
  }

  if(!include_last_pixel) numpixels--;

  for(curpixel = 0; curpixel <= numpixels; curpixel++)
  {
    pset(buffer, u, v, u2, x, y, color, opacity, extra);  // Draw the current pixel
    num += numadd;        // Increase the numerator by the top of the fraction
    if (num >= den)       // Check if numerator >= denominator
    {
      num -= den;       // Calculate the new numerator value
      x += xinc0;       // Change the x as appropriate
      y += yinc0;       // Change the y as appropriate
    }
    x += xinc1;         // Change the x as appropriate
    y += yinc1;         // Change the y as appropriate
  }
}

void drawThinLine(unsigned char* buffer, size_t u, size_t v, size_t u2, int x0, int y0, int x1, int y1, const lpi::ColorRGB& color, double opacity, const ExtraBrushOptions& extra, bool include_last_pixel)
{
  //clip if some point is outside the clipping area
  if(x0 < 0 || x0 >= (int)u || x1 < 0 || x1 >= (int)u || y0 < 0 || y0 >= (int)v || y1 < 0 || y1 >= (int)v)
  {
    int x2 = x0, y2 = y0, x3 = x1, y3 = y1;
    if(!include_last_pixel && (x1 < 0 || x1 > (int)u || y1 < 0 || y1 > (int)v)) include_last_pixel = true; //if include_last_pixel is false, don't clip too much...
    if(!lpi::clipLine(x0, y0, x1, y1, x2, y2, x3, y3, 0, 0, u, v)) return;
  }
  bresenhamLine(buffer, u, v, u2, x0, y0, x1, y1, color, opacity, extra, include_last_pixel);
}


////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////


void psetGrey8(unsigned char* buffer, int buffer_w, int buffer_h, int x, int y, int color, int centerx, int centery, const Brush& brush)
{
  if(x < 0 || y < 0 || x >= buffer_w || y >= buffer_h) return;

  int dx = x - centerx;
  int dy = y - centery;

  double opacity = brush.opacity;

  if(brush.softness != 0)
  {
    double distance = 0.0;
    if(brush.shape == Brush::SHAPE_SQUARE) //square
    {
      distance = std::max((dx > 0 ? dx : -dx), (dy > 0 ? dy : -dy)) / double(brush.size / 2);
    }
    else if(brush.shape == Brush::SHAPE_ROUND) //round
    {
      distance = (dx * dx + dy * dy) / double((brush.size / 2) * (brush.size / 2)); //squared version
      //distance = std::sqrt(dx * dx + dy * dy) / (brush.size / 2); //sqrt version
    }
    else if(brush.shape == Brush::SHAPE_DIAMOND)
    {
      distance = ((dx > 0 ? dx : -dx) + (dy > 0 ? dy : -dy)) / double(brush.size / 2);
    }

    double antidistance = 1.0 - distance;
    if(antidistance < 0.0) antidistance = 0.0;

    antidistance /= brush.softness;
    if(antidistance < 0.0) antidistance = 0.0;
    if(antidistance > 1.0) antidistance = 1.0;

    opacity = brush.opacity * antidistance;
  }

  int bufferPos = buffer_w * y + x;

  int old = buffer[bufferPos];

  int result;
  if(old == color) result = old; else result = int(old * (1.0 - opacity) + color * opacity);

  buffer[bufferPos] = result;
}

void horLineBrushGrey8(unsigned char* buffer, int buffer_w, int buffer_h, int y, int x1, int x2, int color, int centerx, int centery, const Brush& brush)
{
  if(x2 < x1) {x1 += x2; x2 = x1 - x2; x1 -= x2;} //swap x1 and x2, x1 must be the leftmost endpoint
  if(x2 < 0 || x1 >= buffer_w || y < 0 || y >= buffer_h) return; //no single point of the line is on screen

  if(x1 < 0) x1 = 0;
  if(x2 > buffer_w) x2 = buffer_w;

  for(int x = x1; x < x2; x++)
  {
    psetGrey8(buffer, buffer_w, buffer_h, x, y, color, centerx, centery, brush);
  }
}

void drawBrushDiskGrey8(unsigned char* buffer, int buffer_w, int buffer_h, int xc, int yc, int color, const Brush& brush)
{
  int radius = brush.size / 2;
  if(xc + radius < 0 || xc - radius >= buffer_w || yc + radius < 0 || yc - radius >= buffer_h) return; //every single pixel outside screen, so don't waste time on it
  int x = 0;
  int y = radius;
  int p = 3 - (radius << 1);
  int a, b, c, d, e, f, g, h;
  int pb = xc - radius, pd = xc - radius; //previous values: to avoid drawing horizontal lines multiple times
  while (x <= y)
  {
    a = xc + x;
    b = yc + y;
    c = xc - x;
    d = yc - y;
    e = xc + y;
    f = yc + x;
    g = xc - y;
    h = yc - x;

    if(b != pb) horLineBrushGrey8(buffer, buffer_w, buffer_h, b, a, c, color, xc, yc, brush);
    if(d != pd) horLineBrushGrey8(buffer, buffer_w, buffer_h, d, a, c, color, xc, yc, brush);
    if(f != b)  horLineBrushGrey8(buffer, buffer_w, buffer_h, f, e, g, color, xc, yc, brush);
    if(h != d && h != f) horLineBrushGrey8(buffer, buffer_w, buffer_h, h, e, g, color, xc, yc, brush);

    pb = b;
    pd = d;
    if(p < 0) p += (x++ << 2) + 6;
    else p += ((x++ - y--) << 2) + 10;
  }
}

void drawBrushDiamondGrey8(unsigned char* buffer, int buffer_w, int buffer_h, int xc, int yc, int color, const Brush& brush)
{
  int radius = brush.size / 2;

  for(int y = yc - radius; y < yc - radius + brush.size; y++)
  {
    int dist = y - yc;
    if(dist < 0) dist = -dist;
    int size = brush.size - dist * 2;
    int x0 = xc - size / 2;
    int x1 = xc - size / 2 + size;

    horLineBrushGrey8(buffer, buffer_w, buffer_h, y, x0, x1, color, xc, yc, brush);
  }
}

void drawBrushSquareGrey8(unsigned char* buffer, int buffer_w, int buffer_h, int xc, int yc, int color, const Brush& brush)
{
  int radius = brush.size / 2;
  for(int y = yc - radius; y < yc - radius + brush.size; y++) horLineBrushGrey8(buffer, buffer_w, buffer_h, y, xc - radius, xc - radius + brush.size, color, xc, yc, brush);
}

void drawBrushShapeGrey8(unsigned char* buffer, int buffer_w, int buffer_h, int xc, int yc, int color, const Brush& brush)
{
  if(brush.shape == Brush::SHAPE_SQUARE) drawBrushSquareGrey8(buffer, buffer_w, buffer_h, xc, yc, color, brush);
  else if(brush.shape == Brush::SHAPE_ROUND) drawBrushDiskGrey8(buffer, buffer_w, buffer_h, xc, yc, color, brush);
  else if(brush.shape == Brush::SHAPE_DIAMOND) drawBrushDiamondGrey8(buffer, buffer_w, buffer_h, xc, yc, color, brush);
}


void psetGrey8(unsigned char* buffer, size_t u, size_t v, size_t u2, int x, int y, int color, double opacity)
{
  if(x < 0 || y < 0 || x >= (int)u || y >= (int)v) return;

  int bufferPos = u2 * y + x;

  int result = color;

  if(opacity < 1.0)
  {
    int old = buffer[bufferPos + 0];
    int o = (int)(opacity * 255);
    result = (old * (255 - o) + color * o) / 255;
  }

  buffer[bufferPos] = result;
}

//this one does NOT check if the line is inside the buffer! (hence only width, not height, as parameter)
void bresenhamLineGrey8(unsigned char* buffer, size_t u, size_t v, size_t u2, int x0, int y0, int x1, int y1, int color, double opacity, bool include_last_pixel)
{
  //draw the line with bresenham
  int deltax = (int)std::abs((double)(x1 - x0));    // The difference between the x's
  int deltay = (int)std::abs((double)(y1 - y0));    // The difference between the y's
  int x = x0;           // Start x off at the first pixel
  int y = y0;           // Start y off at the first pixel
  int xinc0, xinc1, yinc0, yinc1, den, num, numadd, numpixels, curpixel;

  if(x1 >= x0)         // The x-values are increasing
  {
    xinc0 = 1;
    xinc1 = 1;
  }
  else              // The x-values are decreasing
  {
    xinc0 = -1;
    xinc1 = -1;
  }
  if (y1 >= y0)         // The y-values are increasing
  {
    yinc0 = 1;
    yinc1 = 1;
  }
  else              // The y-values are decreasing
  {
    yinc0 = -1;
    yinc1 = -1;
  }
  if(deltax >= deltay)     // There is at least one x-value for every y-value
  {
    xinc0 = 0;          // Don't change the x when numerator >= denominator
    yinc1 = 0;          // Don't change the y for every iteration
    den = deltax;
    num = deltax / 2;
    numadd = deltay;
    numpixels = deltax;     // There are more x-values than y-values
  }
  else              // There is at least one y-value for every x-value
  {
    xinc1 = 0;          // Don't change the x for every iteration
    yinc0 = 0;          // Don't change the y when numerator >= denominator
    den = deltay;
    num = deltay / 2;
    numadd = deltax;
    numpixels = deltay;     // There are more y-values than x-values
  }

  if(!include_last_pixel) numpixels--;

  for(curpixel = 0; curpixel <= numpixels; curpixel++)
  {
    psetGrey8(buffer, u, v, u2, x, y, color, opacity);  // Draw the current pixel
    num += numadd;        // Increase the numerator by the top of the fraction
    if (num >= den)       // Check if numerator >= denominator
    {
      num -= den;       // Calculate the new numerator value
      x += xinc0;       // Change the x as appropriate
      y += yinc0;       // Change the y as appropriate
    }
    x += xinc1;         // Change the x as appropriate
    y += yinc1;         // Change the y as appropriate
  }
}

void drawThinLineGrey8(unsigned char* buffer, size_t u, size_t v, size_t u2, int x0, int y0, int x1, int y1, int color, double opacity, bool include_last_pixel)
{
  //clip if some point is outside the clipping area
  if(x0 < 0 || x0 >= (int)u || x1 < 0 || x1 >= (int)u || y0 < 0 || y0 >= (int)v || y1 < 0 || y1 >= (int)v)
  {
    int x2 = x0, y2 = y0, x3 = x1, y3 = y1;
    if(!include_last_pixel && (x1 < 0 || x1 > (int)u || y1 < 0 || y1 > (int)v)) include_last_pixel = true; //if include_last_pixel is false, don't clip too much...
    if(!lpi::clipLine(x0, y0, x1, y1, x2, y2, x3, y3, 0, 0, u, v)) return;
  }
  bresenhamLineGrey8(buffer, u, v, u2, x0, y0, x1, y1, color, opacity, include_last_pixel);
}


////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////


void pset(float* buffer, int buffer_w, int buffer_h, int x, int y, const lpi::ColorRGBd& color, int centerx, int centery, const Brush& brush, const ExtraBrushOptions& extra)
{
  if(x < 0 || y < 0 || x >= buffer_w || y >= buffer_h) return;

  int dx = x - centerx;
  int dy = y - centery;

  double opacity = brush.opacity;

  if(brush.softness != 0)
  {
    double distance = 0.0;
    if(brush.shape == Brush::SHAPE_SQUARE) //square
    {
      distance = std::max((dx > 0 ? dx : -dx), (dy > 0 ? dy : -dy)) / double(brush.size / 2);
    }
    else if(brush.shape == Brush::SHAPE_ROUND) //round
    {
      distance = (dx * dx + dy * dy) / double((brush.size / 2) * (brush.size / 2)); //squared version
      //distance = std::sqrt(dx * dx + dy * dy) / (brush.size / 2); //sqrt version
    }
    else if(brush.shape == Brush::SHAPE_DIAMOND)
    {
      distance = ((dx > 0 ? dx : -dx) + (dy > 0 ? dy : -dy)) / double(brush.size / 2);
    }

    double antidistance = 1.0 - distance;
    if(antidistance < 0.0) antidistance = 0.0;

    antidistance /= brush.softness;
    if(antidistance < 0.0) antidistance = 0.0;
    if(antidistance > 1.0) antidistance = 1.0;

    opacity = brush.opacity * antidistance;
  }

  int bufferPos = 4 * buffer_w * y + 4 * x;

  lpi::ColorRGBd old;
  old.r = buffer[bufferPos + 0];
  old.g = buffer[bufferPos + 1];
  old.b = buffer[bufferPos + 2];
  old.a = buffer[bufferPos + 3];

  lpi::ColorRGBd result;
  if(old.r == color.r) result.r = old.r; else result.r = old.r * (1.0 - opacity) + color.r * opacity;
  if(old.g == color.g) result.g = old.g; else result.g = old.g * (1.0 - opacity) + color.g * opacity;
  if(old.b == color.b) result.b = old.b; else result.b = old.b * (1.0 - opacity) + color.b * opacity;
  if(old.a == color.a) result.a = old.a; else result.a = old.a * (1.0 - opacity) + color.a * opacity;

  if(extra.affect_r) buffer[bufferPos + 0] = result.r;
  if(extra.affect_g) buffer[bufferPos + 1] = result.g;
  if(extra.affect_b) buffer[bufferPos + 2] = result.b;
  if(extra.affect_a) buffer[bufferPos + 3] = result.a;
}

void horLineBrush(float* buffer, int buffer_w, int buffer_h, int y, int x1, int x2, const lpi::ColorRGBd& color, int centerx, int centery, const Brush& brush, const ExtraBrushOptions& extra)
{
  if(x2 < x1) {x1 += x2; x2 = x1 - x2; x1 -= x2;} //swap x1 and x2, x1 must be the leftmost endpoint
  if(x2 < 0 || x1 >= buffer_w || y < 0 || y >= buffer_h) return; //no single point of the line is on screen

  if(x1 < 0) x1 = 0;
  if(x2 > buffer_w) x2 = buffer_w;

  for(int x = x1; x < x2; x++)
  {
    pset(buffer, buffer_w, buffer_h, x, y, color, centerx, centery, brush, extra);
  }
}

void drawBrushDisk(float* buffer, int buffer_w, int buffer_h, int xc, int yc, const lpi::ColorRGBd& color, const Brush& brush, const ExtraBrushOptions& extra)
{
  int radius = brush.size / 2;
  if(xc + radius < 0 || xc - radius >= buffer_w || yc + radius < 0 || yc - radius >= buffer_h) return; //every single pixel outside screen, so don't waste time on it
  int x = 0;
  int y = radius;
  int p = 3 - (radius << 1);
  int a, b, c, d, e, f, g, h;
  int pb = xc - radius, pd = xc - radius; //previous values: to avoid drawing horizontal lines multiple times
  while (x <= y)
  {
    // write data
    a = xc + x;
    b = yc + y;
    c = xc - x;
    d = yc - y;
    e = xc + y;
    f = yc + x;
    g = xc - y;
    h = yc - x;
    /*if(b != pb) horLineBrush(buffer, buffer_w, buffer_h, b, a, c, color, brush.opacity);
    if(d != pd) horLineBrush(buffer, buffer_w, buffer_h, d, a, c, color, brush.opacity);
    if(f != b)  horLineBrush(buffer, buffer_w, buffer_h, f, e, g, color, brush.opacity);
    if(h != d && h != f) horLineBrush(buffer, buffer_w, buffer_h, h, e, g, color, brush.opacity);*/

    if(b != pb) horLineBrush(buffer, buffer_w, buffer_h, b, a, c, color, xc, yc, brush, extra);
    if(d != pd) horLineBrush(buffer, buffer_w, buffer_h, d, a, c, color, xc, yc, brush, extra);
    if(f != b)  horLineBrush(buffer, buffer_w, buffer_h, f, e, g, color, xc, yc, brush, extra);
    if(h != d && h != f) horLineBrush(buffer, buffer_w, buffer_h, h, e, g, color, xc, yc, brush, extra);

    pb = b;
    pd = d;
    if(p < 0) p += (x++ << 2) + 6;
    else p += ((x++ - y--) << 2) + 10;
  }
}

void drawBrushDiamond(float* buffer, int buffer_w, int buffer_h, int xc, int yc, const lpi::ColorRGBd& color, const Brush& brush, const ExtraBrushOptions& extra)
{
  int radius = brush.size / 2;

  for(int y = yc - radius; y < yc - radius + brush.size; y++)
  {
    int dist = y - yc;
    if(dist < 0) dist = -dist;
    int size = brush.size - dist * 2;
    int x0 = xc - size / 2;
    int x1 = xc - size / 2 + size;

    horLineBrush(buffer, buffer_w, buffer_h, y, x0, x1, color, xc, yc, brush, extra);
  }
}

void drawBrushSquare(float* buffer, int buffer_w, int buffer_h, int xc, int yc, const lpi::ColorRGBd& color, const Brush& brush, const ExtraBrushOptions& extra)
{
  int radius = brush.size / 2;
  for(int y = yc - radius; y < yc - radius + brush.size; y++) horLineBrush(buffer, buffer_w, buffer_h, y, xc - radius, xc - radius + brush.size, color, xc, yc, brush, extra);
}

void drawBrushShape(float* buffer, int buffer_w, int buffer_h, int xc, int yc, const lpi::ColorRGBd& color, const Brush& brush, const ExtraBrushOptions& extra)
{
  if(brush.shape == Brush::SHAPE_SQUARE) drawBrushSquare(buffer, buffer_w, buffer_h, xc, yc, color, brush, extra);
  else if(brush.shape == Brush::SHAPE_ROUND) drawBrushDisk(buffer, buffer_w, buffer_h, xc, yc, color, brush, extra);
  else if(brush.shape == Brush::SHAPE_DIAMOND) drawBrushDiamond(buffer, buffer_w, buffer_h, xc, yc, color, brush, extra);
}

void pset(float* buffer, size_t u, size_t v, size_t u2, int x, int y, const lpi::ColorRGBd& color, double opacity, const ExtraBrushOptions& extra)
{
  if(x < 0 || y < 0 || x >= (int)u || y >= (int)v) return;

  int bufferPos = 4 * u2 * y + 4 * x;

  lpi::ColorRGBd result = color;

  if(opacity < 1.0)
  {
    lpi::ColorRGBd old;
    old.r = buffer[bufferPos + 0];
    old.g = buffer[bufferPos + 1];
    old.b = buffer[bufferPos + 2];
    old.a = buffer[bufferPos + 3];

    result.r = old.r * (1.0 - opacity) + color.r * opacity;
    result.g = old.g * (1.0 - opacity) + color.g * opacity;
    result.b = old.b * (1.0 - opacity) + color.b * opacity;
    result.a = old.a * (1.0 - opacity) + color.a * opacity;
  }

  if(extra.affect_r) buffer[bufferPos + 0] = result.r;
  if(extra.affect_g) buffer[bufferPos + 1] = result.g;
  if(extra.affect_b) buffer[bufferPos + 2] = result.b;
  if(extra.affect_a) buffer[bufferPos + 3] = result.a;
}

//this one does NOT check if the line is inside the buffer! (hence only width, not height, as parameter)
void bresenhamLine(float* buffer, size_t u, size_t v, size_t u2, int x0, int y0, int x1, int y1, const lpi::ColorRGBd& color, double opacity, const ExtraBrushOptions& extra, bool include_last_pixel)
{
  //draw the line with bresenham
  int deltax = (int)std::abs((double)(x1 - x0));    // The difference between the x's
  int deltay = (int)std::abs((double)(y1 - y0));    // The difference between the y's
  int x = x0;           // Start x off at the first pixel
  int y = y0;           // Start y off at the first pixel
  int xinc0, xinc1, yinc0, yinc1, den, num, numadd, numpixels, curpixel;

  if(x1 >= x0)         // The x-values are increasing
  {
    xinc0 = 1;
    xinc1 = 1;
  }
  else              // The x-values are decreasing
  {
    xinc0 = -1;
    xinc1 = -1;
  }
  if (y1 >= y0)         // The y-values are increasing
  {
    yinc0 = 1;
    yinc1 = 1;
  }
  else              // The y-values are decreasing
  {
    yinc0 = -1;
    yinc1 = -1;
  }
  if(deltax >= deltay)     // There is at least one x-value for every y-value
  {
    xinc0 = 0;          // Don't change the x when numerator >= denominator
    yinc1 = 0;          // Don't change the y for every iteration
    den = deltax;
    num = deltax / 2;
    numadd = deltay;
    numpixels = deltax;     // There are more x-values than y-values
  }
  else              // There is at least one y-value for every x-value
  {
    xinc1 = 0;          // Don't change the x for every iteration
    yinc0 = 0;          // Don't change the y when numerator >= denominator
    den = deltay;
    num = deltay / 2;
    numadd = deltax;
    numpixels = deltay;     // There are more y-values than x-values
  }

  if(!include_last_pixel) numpixels--;

  for(curpixel = 0; curpixel <= numpixels; curpixel++)
  {
    pset(buffer, u, v, u2, x, y, color, opacity, extra);  // Draw the current pixel
    num += numadd;        // Increase the numerator by the top of the fraction
    if (num >= den)       // Check if numerator >= denominator
    {
      num -= den;       // Calculate the new numerator value
      x += xinc0;       // Change the x as appropriate
      y += yinc0;       // Change the y as appropriate
    }
    x += xinc1;         // Change the x as appropriate
    y += yinc1;         // Change the y as appropriate
  }
}

void drawThinLine(float* buffer, size_t u, size_t v, size_t u2, int x0, int y0, int x1, int y1, const lpi::ColorRGBd& color, double opacity, const ExtraBrushOptions& extra, bool include_last_pixel)
{
  //clip if some point is outside the clipping area
  if(x0 < 0 || x0 >= (int)u || x1 < 0 || x1 >= (int)u || y0 < 0 || y0 >= (int)v || y1 < 0 || y1 >= (int)v)
  {
    int x2 = x0, y2 = y0, x3 = x1, y3 = y1;
    if(!include_last_pixel && (x1 < 0 || x1 > (int)u || y1 < 0 || y1 > (int)v)) include_last_pixel = true; //if include_last_pixel is false, don't clip too much...
    if(!lpi::clipLine(x0, y0, x1, y1, x2, y2, x3, y3, 0, 0, u, v)) return;
  }
  bresenhamLine(buffer, u, v, u2, x0, y0, x1, y1, color, opacity, extra, include_last_pixel);
}



////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////


void psetGrey32f(float* buffer, int buffer_w, int buffer_h, int x, int y, float color, int centerx, int centery, const Brush& brush)
{
  if(x < 0 || y < 0 || x >= buffer_w || y >= buffer_h) return;

  int dx = x - centerx;
  int dy = y - centery;

  double opacity = brush.opacity;

  if(brush.softness != 0)
  {
    double distance = 0.0;
    if(brush.shape == Brush::SHAPE_SQUARE) //square
    {
      distance = std::max((dx > 0 ? dx : -dx), (dy > 0 ? dy : -dy)) / double(brush.size / 2);
    }
    else if(brush.shape == Brush::SHAPE_ROUND) //round
    {
      distance = (dx * dx + dy * dy) / double((brush.size / 2) * (brush.size / 2)); //squared version
      //distance = std::sqrt(dx * dx + dy * dy) / (brush.size / 2); //sqrt version
    }
    else if(brush.shape == Brush::SHAPE_DIAMOND)
    {
      distance = ((dx > 0 ? dx : -dx) + (dy > 0 ? dy : -dy)) / double(brush.size / 2);
    }

    double antidistance = 1.0 - distance;
    if(antidistance < 0.0) antidistance = 0.0;

    antidistance /= brush.softness;
    if(antidistance < 0.0) antidistance = 0.0;
    if(antidistance > 1.0) antidistance = 1.0;

    opacity = brush.opacity * antidistance;
  }

  int bufferPos = buffer_w * y + x;

  float old = buffer[bufferPos];

  float result;
  if(old == color) result = old; else result = old * (1.0 - opacity) + color * opacity;

  buffer[bufferPos] = result;
}

void horLineBrushGrey32f(float* buffer, int buffer_w, int buffer_h, int y, int x1, int x2, float color, int centerx, int centery, const Brush& brush)
{
  if(x2 < x1) {x1 += x2; x2 = x1 - x2; x1 -= x2;} //swap x1 and x2, x1 must be the leftmost endpoint
  if(x2 < 0 || x1 >= buffer_w || y < 0 || y >= buffer_h) return; //no single point of the line is on screen

  if(x1 < 0) x1 = 0;
  if(x2 > buffer_w) x2 = buffer_w;

  for(int x = x1; x < x2; x++)
  {
    psetGrey32f(buffer, buffer_w, buffer_h, x, y, color, centerx, centery, brush);
  }
}

void drawBrushDiskGrey32f(float* buffer, int buffer_w, int buffer_h, int xc, int yc, float color, const Brush& brush)
{
  int radius = brush.size / 2;
  if(xc + radius < 0 || xc - radius >= buffer_w || yc + radius < 0 || yc - radius >= buffer_h) return; //every single pixel outside screen, so don't waste time on it
  int x = 0;
  int y = radius;
  int p = 3 - (radius << 1);
  int a, b, c, d, e, f, g, h;
  int pb = xc - radius, pd = xc - radius; //previous values: to avoid drawing horizontal lines multiple times
  while (x <= y)
  {
    a = xc + x;
    b = yc + y;
    c = xc - x;
    d = yc - y;
    e = xc + y;
    f = yc + x;
    g = xc - y;
    h = yc - x;

    if(b != pb) horLineBrushGrey32f(buffer, buffer_w, buffer_h, b, a, c, color, xc, yc, brush);
    if(d != pd) horLineBrushGrey32f(buffer, buffer_w, buffer_h, d, a, c, color, xc, yc, brush);
    if(f != b)  horLineBrushGrey32f(buffer, buffer_w, buffer_h, f, e, g, color, xc, yc, brush);
    if(h != d && h != f) horLineBrushGrey32f(buffer, buffer_w, buffer_h, h, e, g, color, xc, yc, brush);

    pb = b;
    pd = d;
    if(p < 0) p += (x++ << 2) + 6;
    else p += ((x++ - y--) << 2) + 10;
  }
}

void drawBrushDiamondGrey32f(float* buffer, int buffer_w, int buffer_h, int xc, int yc, float color, const Brush& brush)
{
  int radius = brush.size / 2;

  for(int y = yc - radius; y < yc - radius + brush.size; y++)
  {
    int dist = y - yc;
    if(dist < 0) dist = -dist;
    int size = brush.size - dist * 2;
    int x0 = xc - size / 2;
    int x1 = xc - size / 2 + size;

    horLineBrushGrey32f(buffer, buffer_w, buffer_h, y, x0, x1, color, xc, yc, brush);
  }
}

void drawBrushSquareGrey32f(float* buffer, int buffer_w, int buffer_h, int xc, int yc, float color, const Brush& brush)
{
  int radius = brush.size / 2;
  for(int y = yc - radius; y < yc - radius + brush.size; y++) horLineBrushGrey32f(buffer, buffer_w, buffer_h, y, xc - radius, xc - radius + brush.size, color, xc, yc, brush);
}

void drawBrushShapeGrey32f(float* buffer, int buffer_w, int buffer_h, int xc, int yc, float color, const Brush& brush)
{
  if(brush.shape == Brush::SHAPE_SQUARE) drawBrushSquareGrey32f(buffer, buffer_w, buffer_h, xc, yc, color, brush);
  else if(brush.shape == Brush::SHAPE_ROUND) drawBrushDiskGrey32f(buffer, buffer_w, buffer_h, xc, yc, color, brush);
  else if(brush.shape == Brush::SHAPE_DIAMOND) drawBrushDiamondGrey32f(buffer, buffer_w, buffer_h, xc, yc, color, brush);
}


void psetGrey32f(float* buffer, size_t u, size_t v, size_t u2, int x, int y, float color, double opacity)
{
  if(x < 0 || y < 0 || x >= (int)u || y >= (int)v) return;

  int bufferPos = u2 * y + x;

  float result = color;

  if(opacity < 1.0)
  {
    float old = buffer[bufferPos + 0];
    result = old * (1.0 - opacity) + color * opacity;
  }

  buffer[bufferPos] = result;
}

//this one does NOT check if the line is inside the buffer! (hence only width, not height, as parameter)
void bresenhamLineGrey32f(float* buffer, size_t u, size_t v, size_t u2, int x0, int y0, int x1, int y1, float color, double opacity, bool include_last_pixel)
{
  //draw the line with bresenham
  int deltax = (int)std::abs((double)(x1 - x0));    // The difference between the x's
  int deltay = (int)std::abs((double)(y1 - y0));    // The difference between the y's
  int x = x0;           // Start x off at the first pixel
  int y = y0;           // Start y off at the first pixel
  int xinc0, xinc1, yinc0, yinc1, den, num, numadd, numpixels, curpixel;

  if(x1 >= x0)         // The x-values are increasing
  {
    xinc0 = 1;
    xinc1 = 1;
  }
  else              // The x-values are decreasing
  {
    xinc0 = -1;
    xinc1 = -1;
  }
  if (y1 >= y0)         // The y-values are increasing
  {
    yinc0 = 1;
    yinc1 = 1;
  }
  else              // The y-values are decreasing
  {
    yinc0 = -1;
    yinc1 = -1;
  }
  if(deltax >= deltay)     // There is at least one x-value for every y-value
  {
    xinc0 = 0;          // Don't change the x when numerator >= denominator
    yinc1 = 0;          // Don't change the y for every iteration
    den = deltax;
    num = deltax / 2;
    numadd = deltay;
    numpixels = deltax;     // There are more x-values than y-values
  }
  else              // There is at least one y-value for every x-value
  {
    xinc1 = 0;          // Don't change the x for every iteration
    yinc0 = 0;          // Don't change the y when numerator >= denominator
    den = deltay;
    num = deltay / 2;
    numadd = deltax;
    numpixels = deltay;     // There are more y-values than x-values
  }

  if(!include_last_pixel) numpixels--;

  for(curpixel = 0; curpixel <= numpixels; curpixel++)
  {
    psetGrey32f(buffer, u, v, u2, x, y, color, opacity);  // Draw the current pixel
    num += numadd;        // Increase the numerator by the top of the fraction
    if (num >= den)       // Check if numerator >= denominator
    {
      num -= den;       // Calculate the new numerator value
      x += xinc0;       // Change the x as appropriate
      y += yinc0;       // Change the y as appropriate
    }
    x += xinc1;         // Change the x as appropriate
    y += yinc1;         // Change the y as appropriate
  }
}

void drawThinLineGrey32f(float* buffer, size_t u, size_t v, size_t u2, int x0, int y0, int x1, int y1, float color, double opacity, bool include_last_pixel)
{
  //clip if some point is outside the clipping area
  if(x0 < 0 || x0 >= (int)u || x1 < 0 || x1 >= (int)u || y0 < 0 || y0 >= (int)v || y1 < 0 || y1 >= (int)v)
  {
    int x2 = x0, y2 = y0, x3 = x1, y3 = y1;
    if(!include_last_pixel && (x1 < 0 || x1 > (int)u || y1 < 0 || y1 > (int)v)) include_last_pixel = true; //if include_last_pixel is false, don't clip too much...
    if(!lpi::clipLine(x0, y0, x1, y1, x2, y2, x3, y3, 0, 0, u, v)) return;
  }
  bresenhamLineGrey32f(buffer, u, v, u2, x0, y0, x1, y1, color, opacity, include_last_pixel);
}
