/*
LodePaint

Copyright (c) 2009-2010 Lode Vandevenne
All rights reserved.

This file is part of LodePaint.

LodePaint is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LodePaint is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LodePaint.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "lpi/gl/lpi_texture_gl.h"
#include "paint_settings.h"

struct MainProgram;

extern std::vector<lpi::HTexture> textures_tools; //DANGER! this are auto_ptr-like objects in an std::vector, so set the correct size ONCE and then never resize it.
extern std::vector<lpi::HTexture> textures_filters; //idem
extern std::vector<lpi::HTexture> textures_toolbar; //idem

bool skinExists(MainProgram& p, Options& options, const std::string& name);
bool trySkin(MainProgram& p, Options& options, const std::string& name);
bool loadSkin(MainProgram& p, const std::string& path);
bool loadSkinInitial(MainProgram& p, const std::string& path);

void initGuiDrawerStyle(MainProgram& p);
