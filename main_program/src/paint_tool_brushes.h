/*
LodePaint

Copyright (c) 2009-2010 Lode Vandevenne
All rights reserved.

This file is part of LodePaint.

LodePaint is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LodePaint is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LodePaint.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "lpi/gio/lpi_draw2d_buffer.h"
#include "lpi/gio/lpi_input.h"
#include "lpi/gio/lpi_text_drawer_int.h"
#include "lpi/gui/lpi_gui.h"

#include "paint_tool.h"
#include "paint_algos_brush_pixelop.h"
#include "paint_algos_brush_color.h"

class IBrush : public DialogTool //any tool that uses the mouse to affect pixels of the image (not to be confused with the actual "Brush" tool. And NOT for geometric tools to create shapes like circles and lines from multiple points indicated with the mouse.)
{
  public:
  
  protected:
    ToolUndoHelper undohelper;
    
    virtual bool editMask() { return false; }
    
  public:
    IBrush(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom) : DialogTool(settings, geom) {}
    virtual void getUndoState(UndoState& state);
    virtual void undo(Paint& paint, UndoState& state);
    virtual void redo(Paint& paint, UndoState& state);
};

class Tool_Brush : public IBrush
{
  private:

    bool initiate; //when you just start pressing mouse button
    bool justdone;
    int oldMouseX;
    int oldMouseY;
    
    ExtraBrushOptions extra;

  protected:
    virtual lpi::ColorRGB getColor(const lpi::IInput& input, Paint& paint);
    

  public:
  
    Tool_Brush(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom);
    
    void setAffectChannels(bool r, bool g, bool b, bool a);
   
    
    void drawBrush(int& x1, int& y1, int x2, int y2, const lpi::ColorRGB& color, Paint& paint);
    void drawBrush(int& x1, int& y1, int x2, int y2, const lpi::ColorRGB& color, TextureRGBA8& texture);
    void drawBrush(int& x1, int& y1, int x2, int y2, int color, TextureGrey8& texture);
    void drawBrush(int& x1, int& y1, int x2, int y2, const lpi::ColorRGBd& color, TextureRGBA32F& texture);
    void drawBrush(int& x1, int& y1, int x2, int y2, float color, TextureGrey32F& texture);

    virtual void handle(const lpi::IInput& input, Paint& paint);
    virtual void draw(lpi::gui::IGUIDrawer& drawer, Paint& paint);
    
    virtual bool done() { bool result = justdone; justdone = false; return result; }
    
    virtual std::string getLabel() const { return "Brush"; }
    virtual lpi::HTexture* getIcon() const { return &textures_tools[34]; }
};

/*
The erasor is similar to the brush, except how it affects the color: all it does
is make the alpha channel more transparent. This is different from the regular
brush with "invisible" as color, because that one may still affect R, G and B channels (when there is softness),
while Tool_Eraser never does.
*/
class Tool_Eraser : public IBrush
{
  private:

    bool initiate; //when you just start pressing mouse button
    bool justdone;
    int oldMouseX;
    int oldMouseY;
    
    ExtraBrushOptions extra;
    
    int alpha_left;
    int alpha_right;
    bool erase_content;

  protected:
    virtual lpi::ColorRGB getColor(const lpi::IInput& input, Paint& paint);
    

  public:
  
    Tool_Eraser(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom);
    
    void setAffectChannels(bool r, bool g, bool b, bool a);
   
    
    void drawBrush(int& x1, int& y1, int x2, int y2, const lpi::ColorRGB& color, Paint& paint);
    void drawBrush(int& x1, int& y1, int x2, int y2, const lpi::ColorRGB& color, TextureRGBA8& texture);
    void drawBrush(int& x1, int& y1, int x2, int y2, int color, TextureGrey8& texture);
  
    virtual void handle(const lpi::IInput& input, Paint& paint);
    virtual void draw(lpi::gui::IGUIDrawer& drawer, Paint& paint);
    
    virtual bool done() { bool result = justdone; justdone = false; return result; }
    
    virtual std::string getLabel() const { return "Eraser"; }
    virtual lpi::HTexture* getIcon() const { return &textures_tools[35]; }
};



class Tool_MaskBrush : public Tool_Eraser
{
  protected:

    virtual bool editMask() { return true; }

  public:
    Tool_MaskBrush(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom)
    : Tool_Eraser(settings, geom)
    {
    }

    virtual std::string getLabel() const { return "Edit Mask Brush"; }
    virtual lpi::HTexture* getIcon() const { return &textures_tools[116]; }
};

class Tool_Pen : public IBrush //the pen is always 1-pixel in size with 100% opacity, on purpose
{
  private:
    
    bool initiate; //when you just start pressing mouse button
    bool justdone;
    int oldMouseX;
    int oldMouseY;
    //bool avoidUglyLineArt; //avoids ugly pixel combinations, by only drawing if the mouse moved at least 3 pixels.
    double lastTime;

  public:
    Tool_Pen(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom);

    virtual void handle(const lpi::IInput& input, Paint& paint);
    virtual void draw(lpi::gui::IGUIDrawer& drawer, Paint& paint);
    
    virtual bool done() { bool result = justdone; justdone = false; return result; }
    
    virtual std::string getLabel() const { return "Pen (1 pixel)"; }
    virtual lpi::HTexture* getIcon() const { return &textures_tools[32]; }
};

class Tool_ColorReplacer : public IBrush
{
  private:
    class ColorReplace : public IPixelOperation
    {
      public:
        lpi::ColorRGB from;
        lpi::ColorRGB to;
        Tool_ColorReplacer& settings;
        ColorReplace(Tool_ColorReplacer& settings) : settings(settings) {}
        virtual void operate(unsigned char& r, unsigned char& g, unsigned char& b, unsigned char& a, double opacity) const;
    };
    ColorReplace op;
    
    bool initiate; //when you just start pressing mouse button
    bool justdone;
    int oldMouseX;
    int oldMouseY;
    
    Brush brush; //like settings.brush, but with custom opacity and softness

    double tolerance; //for floodfill and color replacer
    bool tolerance_ignores_alpha; //alpha not used in tolerance calculations
    
  public:
    Tool_ColorReplacer(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom);

    void setAffectChannels(bool r, bool g, bool b, bool a);

    void drawBrush(int& x1, int& y1, int x2, int y2, Paint& paint);

    virtual void handle(const lpi::IInput& input, Paint& paint);
    virtual void draw(lpi::gui::IGUIDrawer& drawer, Paint& paint);

    virtual bool done() { bool result = justdone; justdone = false; return result; }

    virtual std::string getLabel() const { return "Color Replacer"; }
    virtual lpi::HTexture* getIcon() const { return &textures_tools[48]; }
};

class Tool_Adjust : public IBrush //todo: allow this one to also change hue, saturation, etc... (and give different name in that case)
{
  private:
    class Adjust : public IPixelOperation
    {
      public:
      
        double adjust;
        
        Tool_Adjust& settings;
        Adjust(Tool_Adjust& settings) : settings(settings) {}
        virtual void operate(unsigned char& r, unsigned char& g, unsigned char& b, unsigned char& a, double opacity) const;
    };
    Adjust op;
    
    enum Mode
    {
      M_LIGHTNESS,
      M_SATURATION,
      M_HUE,
      M_RED,
      M_GREEN,
      M_BLUE,
      M_ALPHA
    };
    
    Mode mode;
    
    bool initiate; //when you just start pressing mouse button
    bool justdone;
    int oldMouseX;
    int oldMouseY;
    
    Brush brush; //like settings.brush, but with custom opacity and softness

    double adjust; //-1 to +1. Negative is darken, positive is brighten
    
  public:
    Tool_Adjust(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom);

    void setAffectChannels(bool r, bool g, bool b, bool a);

    void drawBrush(int& x1, int& y1, int x2, int y2, Paint& paint);

    virtual void handle(const lpi::IInput& input, Paint& paint);
    virtual void draw(lpi::gui::IGUIDrawer& drawer, Paint& paint);

    virtual bool done() { bool result = justdone; justdone = false; return result; }

    virtual std::string getLabel() const { return "Adjust"; }
    virtual lpi::HTexture* getIcon() const { return &textures_tools[49]; }
};

class Tool_PixelBrush : public IBrush //the pen is always 1-pixel in size with 100% opacity, on purpose
{
  private:

    bool initiate; //when you just start pressing mouse button
    bool justdone;
    int oldMouseX;
    int oldMouseY;
    
    int size;

  public:
    Tool_PixelBrush(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom);

    virtual void handle(const lpi::IInput& input, Paint& paint);
    virtual void draw(lpi::gui::IGUIDrawer& drawer, Paint& paint);

    virtual bool done() { bool result = justdone; justdone = false; return result; }

    virtual std::string getLabel() const { return "Pixel Brush"; }
    virtual lpi::HTexture* getIcon() const { return &textures_tools[33]; }
};
