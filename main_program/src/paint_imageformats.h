/*
LodePaint

Copyright (c) 2009-2010 Lode Vandevenne
All rights reserved.

This file is part of LodePaint.

LodePaint is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LodePaint is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LodePaint.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once 

#include "lpi/os/lpi_filebrowse.h"
#include "lpi/lpi_os.h"

#include "paint_window.h"

#include <string>
#include <vector>

class IImageFormat
{
  public:
    //TODO: add something in the interface to know if the encoder can only decode 32-bit or only 128-bit FP, so that the paint program can give meaningful messages to the user about having to convert the image color type
    
    virtual bool canDecode(const unsigned char* file, size_t filesize) const = 0;
    virtual bool canEncode(const Paint& paint) const = 0;
    
    //these are to check that you can decode or encode in general
    virtual bool canDecode() const = 0;
    virtual bool canEncode() const = 0;
  
    //decode will not call any OpenGL commands to allow multithreading. You must update the paint yourself after it to bind the texture to the video card.
    virtual bool decode(std::string& error, Paint& paint, const unsigned char* file, size_t filesize) const = 0;
    virtual bool encode(std::string& error, std::vector<unsigned char>& file, const Paint& paint) const = 0;
    
    virtual size_t getNumExtensions() const { return 1; } //get number of extensions of this file format (e.g. for PNG this is 1, for jpeg this is 2 (jpg and jpeg))
    virtual std::string getExtension(size_t i) const = 0; //return it in all lowercase (and treat extension as case insensitive, even on unix file systems)
    virtual std::string getDescription() const = 0; //description for in the file dialog, e.g. "PNG files"

    virtual ~IImageFormat(){}
};

class AImageFormat : public IImageFormat
{
  protected:
    virtual bool canDecodeRGBA8() const { return false; }
    virtual bool canEncodeRGBA8() const { return false; }

    virtual bool decodeRGBA8(std::string& error, std::vector<unsigned char>& image, int& w, int& h, const unsigned char* file, size_t filesize) const  { (void)error; (void)image; (void)w; (void)h; (void)file; (void)filesize; return false; }
    virtual bool encodeRGBA8(std::string& error, std::vector<unsigned char>& file, const unsigned char* image, int w, int h) const { (void)error; (void)file; (void)image; (void)w; (void)h; return false; }

    virtual bool canDecodeRGBA32F() const { return false; }
    virtual bool canEncodeRGBA32F() const { return false; }

    virtual bool decodeRGBA32F(std::string& error, std::vector<float>& image, int& w, int& h, const unsigned char* file, size_t filesize) const { (void)error; (void)image; (void)w; (void)h; (void)file; (void)filesize; return false; }
    virtual bool encodeRGBA32F(std::string& error, std::vector<unsigned char>& file, const float* image, int w, int h) const { (void)error; (void)file; (void)image; (void)w; (void)h; return false; }

    virtual bool canDecodeGrey8() const { return false; }
    virtual bool canEncodeGrey8() const { return false; }

    virtual bool decodeGrey8(std::string& error, std::vector<unsigned char>& image, int& w, int& h, const unsigned char* file, size_t filesize) const { (void)error; (void)image; (void)w; (void)h; (void)file; (void)filesize; return false; }
    virtual bool encodeGrey8(std::string& error, std::vector<unsigned char>& file, const unsigned char* image, int w, int h) const { (void)error; (void)file; (void)image; (void)w; (void)h; return false; }

    virtual bool canDecodeGrey32F() const { return false; }
    virtual bool canEncodeGrey32F() const { return false; }

    virtual bool decodeGrey32F(std::string& error, std::vector<float>& image, int& w, int& h, const unsigned char* file, size_t filesize) const { (void)error; (void)image; (void)w; (void)h; (void)file; (void)filesize; return false; }
    virtual bool encodeGrey32F(std::string& error, std::vector<unsigned char>& file, const float* image, int w, int h) const { (void)error; (void)file; (void)image; (void)w; (void)h; return false; }


    virtual bool isFileOfFormat(const unsigned char* file, size_t filesize) const = 0;
    
  private:
    bool decodePaintRGBA8(std::string& error, Paint& paint, const unsigned char* file, size_t filesize) const;
    bool decodePaintRGBA32F(std::string& error, Paint& paint, const unsigned char* file, size_t filesize) const;
    bool decodePaintGrey8(std::string& error, Paint& paint, const unsigned char* file, size_t filesize) const;
    bool decodePaintGrey32F(std::string& error, Paint& paint, const unsigned char* file, size_t filesize) const;

  public:
    virtual bool canDecode(const unsigned char* file, size_t filesize) const;
    virtual bool canEncode(const Paint& paint) const;
    
    virtual bool canDecode() const;
    virtual bool canEncode() const;
  
    virtual bool decode(std::string& error, Paint& paint, const unsigned char* file, size_t filesize) const;
    virtual bool encode(std::string& error, std::vector<unsigned char>& file, const Paint& paint) const;
};

class ImageFormatPNG : public AImageFormat
{
  virtual bool canDecodeRGBA8() const;
  virtual bool canEncodeRGBA8() const;
  
  virtual bool decodeRGBA8(std::string& error, std::vector<unsigned char>& image, int& w, int& h, const unsigned char* file, size_t filesize) const;
  virtual bool encodeRGBA8(std::string& error, std::vector<unsigned char>& file, const unsigned char* image, int w, int h) const;

  virtual bool canEncodeGrey8() const;
  virtual bool encodeGrey8(std::string& error, std::vector<unsigned char>& file, const unsigned char* image, int w, int h) const;

  
  virtual bool isFileOfFormat(const unsigned char* file, size_t filesize) const;
  
  virtual size_t getNumExtensions() const;
  virtual std::string getExtension(size_t i) const;
  virtual std::string getDescription() const;
};

class ImageFormatBMP : public AImageFormat
{
  virtual bool canDecodeRGBA8() const;
  virtual bool canEncodeRGBA8() const;
  
  virtual bool decodeRGBA8(std::string& error, std::vector<unsigned char>& image, int& w, int& h, const unsigned char* file, size_t filesize) const;
  virtual bool encodeRGBA8(std::string& error, std::vector<unsigned char>& file, const unsigned char* image, int w, int h) const;
  
  virtual bool isFileOfFormat(const unsigned char* file, size_t filesize) const;
  
  virtual size_t getNumExtensions() const;
  virtual std::string getExtension(size_t i) const;
  virtual std::string getDescription() const;
};

class ImageFormatTGA : public AImageFormat
{
  virtual bool canDecodeRGBA8() const;
  virtual bool canEncodeRGBA8() const;
  
  virtual bool decodeRGBA8(std::string& error, std::vector<unsigned char>& image, int& w, int& h, const unsigned char* file, size_t filesize) const;
  virtual bool encodeRGBA8(std::string& error, std::vector<unsigned char>& file, const unsigned char* image, int w, int h) const;
  
  virtual bool isFileOfFormat(const unsigned char* file, size_t filesize) const;
  
  virtual size_t getNumExtensions() const;
  virtual std::string getExtension(size_t i) const;
  virtual std::string getDescription() const;
};


class ImageFormatJPG : public AImageFormat
{
  virtual bool canDecodeRGBA8() const;
  virtual bool canEncodeRGBA8() const;

  virtual bool decodeRGBA8(std::string& error, std::vector<unsigned char>& image, int& w, int& h, const unsigned char* file, size_t filesize) const;

  virtual bool isFileOfFormat(const unsigned char* file, size_t filesize) const;

  virtual size_t getNumExtensions() const;
  virtual std::string getExtension(size_t i) const;
  virtual std::string getDescription() const;
};

class ImageFormatGIF : public AImageFormat
{
  virtual bool canDecodeRGBA8() const;
  virtual bool canEncodeRGBA8() const;

  virtual bool decodeRGBA8(std::string& error, std::vector<unsigned char>& image, int& w, int& h, const unsigned char* file, size_t filesize) const;

  virtual bool isFileOfFormat(const unsigned char* file, size_t filesize) const;

  virtual size_t getNumExtensions() const;
  virtual std::string getExtension(size_t i) const;
  virtual std::string getDescription() const;
};

class ImageFormatRGBE : public AImageFormat
{
  virtual bool canDecodeRGBA32F() const;
  virtual bool canEncodeRGBA32F() const;
  
  virtual bool decodeRGBA32F(std::string& error, std::vector<float>& image, int& w, int& h, const unsigned char* file, size_t filesize) const;
  virtual bool encodeRGBA32F(std::string& error, std::vector<unsigned char>& file, const float* image, int w, int h) const;
  
  virtual bool isFileOfFormat(const unsigned char* file, size_t filesize) const;
  
  virtual size_t getNumExtensions() const;
  virtual std::string getExtension(size_t i) const;
  virtual std::string getDescription() const;
};

class ImageFormats
{
  public:
    std::vector<IImageFormat*> formats;
    
    ImageFormats();
    ~ImageFormats();
    
    void addMainBuiltInFormats(); //these aren't overridable by plugins... But the reason I do them first is so that when you do save as, by default "PNG" appears in the file dialog instead of an exotic format.
    void addPluginFormats(lpi::FileBrowse& filebrowser, const std::string& directory); //directory must have endslash
    void addOtherBuiltInFormats(); //do this after the plugin formats. That way, plugin formats can override built-in ones (e.g. libjpeg)
};
