/*
LodePaint

Copyright (c) 2009-2012 Lode Vandevenne
All rights reserved.

This file is part of LodePaint.

LodePaint is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LodePaint is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LodePaint.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
Changelist. Most recent on top.
-------------------------------

20121103:

Fixed archlinux package

20120610:

-Added overflow option to image arithmetic (wrap or clamp).
-Made XYZ and Lab color conversions assume sRGB color model for the RGB values. The previous formula was a rather
 ignorant implementation involving CIE RGB and no gamma. The sRGB model is the one good for computer screens,
 the internet, textures, etc..., so ideal for LodePaint.
-In the color picker's advanced and telivision tabs, made it show values in the numeric ranges of the specifications
 of the color models, rather than always 0-255.
-Fixed bug where wrapping gaussian blur was not wrapping.

20120609:
-Added "spectrum" filter under color models, which interprets RGB values as wavelengths and generates new RGB from those wavelengths
-Contour filter now works on R, G and B channel separately (like the other edge filters). To have its old effect, convert the image
 to greyscale from any channel you like first.
-Added button for grayscale (luma) to the toolbar
-Added "YDbDr" color model to color picker
-Made pixel raster more faded, so that is is more useful instead of distracting

20120607:

-Fixed "User defined" filter not responding to menu click
-Added "reduce colors" filter. It uses median cut with special treatment for alpha channel.
-Added some predefined matrices to the Transform 2D filter.
-Added YUV and YIQ color models to advanced color chooser, and moved these along with YCbCr and YPbPr under a "Television" tab
 because these are all television related color models.

20120604:

-Fixed crash when using blend selection filter

20120603:

-improved the behaviour when the main program window is resized again: now paintings will not be made smaller unless they'd go outside
the main window, and paintings that are near the borders are made larger when the main window size is increased
-solved problems with the main program window being positioned in strange/annoying locations when starting LodePaint in Windows. The
solution is not ideal yet (I'd need to know a way to find current screen resolution, start menu bar size, Windows window decoration size, etc...
to somehow find the exact maximum size)

20120528:

-added "sort colors by hilbert index" filter. This makes the sorted colors look a bit nicer (smoother transitions), for in the
rare event that this is actually useful... The hilbert index is the index of the color in Hilbert Space in the 3D RGB color cube, plus a
separate snaking pattern for the alpha channel. The sort result is laid out in scanlines.

20120527:

-paint window is now maximized initially by default
-added 8 draggable corners/sides to each image to easily resize it by dragging
-the contour operation now always outputs white lines on black background (use "Set 1-bit FG BG" operation to color it)
-Added "binary to gray code" and vice versa filters
-Added "Spooky Neon" artistic filter
-Added workaround for something where OpenGL drew ugly artifacts when image was 150% zoomed in (does not happen on every computer...)

20120526:

-Rearranged and renamed some filters
-Added "Set 1-bit FG BG" filter. It turns the most common color into the BG color, everything else in the FT color.
 Its purpose is to quickly and easily change the FG and BG color of a 1-bit image, but it is more generally applicable too
-Added "Average Color" filter
-Added "Sort Colors" filter. It sorts colors based on a numerical integer reprentation, in scanline order. The center pixel
 of the resulting image will contain the median, at least the median based on the HTML/CSS #hex RGBA value of the color, which is
 kind of arbitrary and not really related to a useful color property like lightness.
-Added "Order Colors" filter. It sorts colors in scanline order based on how often they appear. It shows the most common
 colors in the top scanlines, the rarest ones in the bottom scanlines. Order of first appearance in scanline order is kept if counts are the same.
 The top left pixel (and the identical colors next to it) are the mode (= most common color).
-Added "Image Info" window that shows info such as: filename path, size, memory, amount of unique colors, ...
-Added "adaptive median filter"

20120525:

-Fixed the bicubic rescaling, and enabled it in the GUI (in the rescale filter)
-Fixed rounding bug where "make seamless" filter would generate some alpha values of 254 in an opaque image

20120523:

-Added Pixel Edge Detect filter (for use on simple pixel art)
-Added Canny Edge Detector filter
-Fixed the "brightness" terminology in two more places: greyscale (physiological) becomes (luma) and Contours's lightness becomes intensity.

20120520:

-Added preset matrices to all filters that have a matrix (RGBA matrix, 2D Transform and User Defined)
-Added Premultiply Alpha filter

20120519:

-added "Test Image" filter: it generates something like a TV test image, but with features useful to test various filters
-added "Extract Channel" filter, which allows extracting various channels of all color models and more. It's a simpler version of the two "Channel Switcher" filters.
-added more possible input channels to "Channel Switcher" filter
-Fixed bugs with Lab color model components overflow in some filters
-Fixed bug where scrollbar of a dropdown could do annoying when releasing the mouse button over the options after scrolling
-Introduced consistent terminology for brightness types in LodePaint: lightness, value, intensity and luma. Updated terminology in all filters, and the explanation,
with formulas, is in the help of the Extract Channel filter.
-Fixed annoyance where paint windows became very small when shrinking the main LodePaint window.
-Added RG Chromaticity filter (see http://en.wikipedia.org/wiki/RG_Chromaticity)
-If you maximize paint window, it also centers the painting in it now
-Added "Brushy" filter under artistic
-Fixed bug where selecting the hex color text in the color picker and typing a new value, the first character wasn't registered

20120513:

-Using the public domain jpgd.cpp instead of std_image.c for the built-in JPEG
 loader: now it supports progressive JPEGs.
-Finished writing help for all filters

20120512:

-Replaced "Steampunk" skin by "Dark" skin
-Fixed a rounding error that caused annoying white and black dots in the corners after doing FFT, then IFFT on an image.
-Added "Wrapping Shift" filter, where the amount of shift can be chosen, unlike Wrapping Shift 50 which uses a predefined amount.
-Added more filter help texts

20120511:

-Added "Identify Alpha" filter: it makes opaque pixels white, fully tranparent pixels black, and everything in between mid-grey.

20120510:

-Implemented help system for filters and added help text for several so far.

20120509:

-Made paint windows fit around painting when opening or creating new files
-Moved "See program dirs" option from Settings to Help menu.
-Moved location of all the tool, color, etc... windows to the left, so on large monitors the distance
 of all things you need is closer together
-Added maximize button to paint windows

20120508:

-Made images stay better in the visible area of their window when resizing a window.
-Made the alpha warning symbol more clear and appear more often.

20120507:

-Added "rarest" mode for rescale. This is only useful for making 1-pixel wide line art smaller. Lines will be kept connected.
-Added physiological greyscale filter. This one uses the "0.3*r + 0.59*g+0.11*b" formula. Regular greyscale uses (r+g+b)/3.
-Added alpha greyscale filter. This one uses the formula "(r+g+b+a)/4" and sets alpha to it too. Could be useful in some rare edge cases.
-Added "remove jpeg artifacts" filter. This is actually just the same as Selective Gaussian Blur with threshold 25, but it is now more
 discoverable, and may be made more JPEG specific later. It fixes JPEGs with very visible artifacts, e.g. when a line art or comic
 image was encoded as JPEG (for photos, JPEG is fine and there's not much to fix). It may also improve noisy GIFs!
-added "mem" indicator at the bottom of the image. It shows the uncompressed memory size.

20120331:

-Removed new compiler warnings introduced in recent g++ versions
-Added the needed -ldl and -lX11 linker commands to Linux makefile
-Fixed sepia filter bug
-Fixed color key alpha filter
-Added convenient 2x2 version of Repeat filter
-Added 1-bit RGB dithering filter
-Made BMP saving not use alpha channel if not needed
-Made filter window background a bit brighter in default skin
-Using other tool than selection, now deselects (because you can't use the other tools on a selection)
-Toggle Mask button in toolbar now has indication when on
-Added "repeat second last filter" button in the toolbar
-Added "close" and "close all" items in the File menu
-Added "Set Alpha To..." filter
-New version of LodePNG encoder used, so can automatically create 1-bit PNGs and such
-Fixed threshold alpha filter
-Fixed gradient line sometimes setting alpha to 254 for opaque colors
-Show alpha value in red in color window if not 255
-Show alpha symbol when drawing with brush if alpha is between 240 and 254, to warn in case you didn't notice there's a bit of alpha
-Fixed Auto background mode for selection to choose most common value of 4 corners, rather than just the top left corner
-Added toolbar button for Multiply SLA
-Reduced Pixel Grid option cycle length from 8 to 3 values: none, one pixel, custom
-Added "erase content" option to eraser
-Fixed bug where tooltip of last row of dynamic page wouldn't work
-Made tooltips stay inside the screen when on the far right
-Made 255 and 252 version of CGA palette
-Added little "O" button in the color window that makes FG and BG color opaque in a single click
-Tweaked the adjust saturation tool

20110430:

-Added translation support. A text file with translated strings can be made. Follow the example of the Dutch nl.txt.
-Translation of most text strings to the Dutch language done.
-Message windows will now close when pressing enter, space or escape.

20110425:

-Separate width and height for custom grid size can now be set in the options.
-Partial support for many European languages added to the GUI bitmap fonts,
including accented latin characters, cyrillic and greek, using UTF-8.

20110109:

Added a preview of the selected image file in the file open dialog. This is only done to the built-in "weird"
 OpenGL file dialog though, the standard MS Windows file dialog or other OS standard dialogs don't have it yet.
The overview shows the texture a bit smoothed now.

20101121:

Text input fields can now use the ctrl + left/right arrow key combination to jump past words
Submenus now close if mouse moves over menu item without submenu, and a right click closes all menus.
Added "clear list" option to the recent files menu.

20101112:

Added cancel button to the color selection dialogs

20101108:

Third attempt at fixing Windows 7 touch screen (I don't have such PC myself, hence the attempts to try now and then. The incoming mouse down events from SDL have outdated x y position...)
Amiga skin added (created by Amiga community devs)
LodePaint now remembers the chosen skin between sessions

20101107:

Second part of the mouse event code fix to make it hopefully work for real on a touch screen this time (the
first fix fixed registration of mouse clicks on touch screens, this second fix fixes the fact that on a
touch screen the mouse position is only updated right at the time of the click)
Also ask file save confirmation when exiting the program using "File - Exit".

20101009:

Fixed a bug in the mouse event code, that prevented LodePaint from working with touch in Windows 7.

20100821:

Changed "alpha to mask" filter into "channel to mask" allowing to choose what to convert to mask

20100808:

Undo and redo buttons in toolbar become greyed out if there are no more steps
Fixed appearance of empty tooltips over seperators of toolbar
Made median filter slightly faster

20100807:

Added blend modes to the "blend selection" filter (Overlay, Soft/Hard light, Dodge and Burn)
Fixed existing blend modes (formula of Divide and terminology of Screen)
Fixed annoyance of menus: they now close immediatly if you click on the main menu item when submenus are open
When saving to a file that already exists, it now automatically makes a backup with a "~" added to the name, only if the backup didn't exist yet. So originals of photos are maybe backed up when accidently overwriting one. (no guarantees, the authors of this program are not responsible for data loss)
Fixed bug with undo of unfloating of selections partially outside image
Fixed crash when opening image file that didn't exist anymore

20100730:

Added "Make Seamless" filter
Moved mirror-tile, wrapping shift 50% and repeat filters to "Seamless Tiling" submenu under Filters

20100729:

Added busy indicator bar while saving and loading images

20100717:

Added the code created by the Amiga users for Amiga file dialogs
Fixed small bug with typing text in the text tool's dialog

20100713:

Added "interpolating blur" filter
The "Pointillism" filter now puts by default a blurred original image behind the result to avoid colored artefacts.

20100712:

Removed some issues detected by Valgrind debugger.

20100706:

Updated GUI style: brighter toolbar, white rectangle behind number inputs, new slider style, and brushier brush icon.
Fixed the accidently broken "keep only alpha" filter
Tab now works to jump to next control on all text fields and sliders in filter/tool parameters
Added "Copy Alpha" option to the "Blend Selection" filter

20100705:

Fixed an annoying bug that sometimes made it hard to type values in filter/tool parameters.
Fixed the appearance of numbers with many decimals after the point when using some sliders in filter/tool parameters.
Made some filter/tool parameters use percentage (0-100) rather than the range 0.0-1.0.

20100703:

In Windows, it now uses the standard Windows file dialogs by default (there's a setting in Options to use the OpenGL file dialog instead)
Increased the tooltip font size.

20100702:

Gave the jpeg plugin the ability to save jpegs. Quality currently defaults to 80.
Added "Normalized Iteration" option to julia and mandelbrot filters (smoother color transitions)

20100627:

Added FPS counter
Did a few cleanups and optimizations

20100626:

Added "square" option to the rectangle tool
Changed the way mouse input of line, gradient line, rectangle, circle by 2 points and ellipse works: it now uses dragging instead of two clicks, to be more similar to what users are used to.
Made some more operations use the progress bar
Added "Brightness", "Saturation" and "Hue" options to the Generate Random Noise filter
Fixed bug with the morphology filters: the wrong filter doing the wrong operation

20100625:

Preview in filter dialogs now has same width/height ratio as the drawing
Multithreading, progress bars and busy indicators are now used for filters. Not yet supported for operations that change the image size.

20100619:

Upgraded the "brightness/contract" filter with gamma slider, "affect alpha" option, and RGBA32F support
Added icon for the "adjust HSLA" to the toolbar
Added "Crayons" color palette

20100618:

Added a few gradients to the GUI here and there
Changed the "negative" skin into a "steampunk" skin
Added "Color Temperature" filter
Added "Julia" and "Mandelbrot" filters

20100616:

Made a normalize filter that doesn't use dialog (it can be an annoyance)
Tweaked a few filter icons
Also support floating point mode on computers where OpenGL doesn't support floating point textures

20100614:

Give message to the user when a filter doesn't support a certain color mode (before it simply did nothing without any indication why)

20100613:

The crop operation now says in a warning dialog that a selection area is required for it to work.
Began internal code refactoring of dynamic operation parameters. When done, they will be convertible from/to everything, like XML, GUI dialogs, C-structs, plugins, etc...

20100611:

Beginnings of skins support. It can now switch between 3 built-in skins (default, negative, old). Detecting folders with custom skins is still TODO.
Added DNG (Adobe Digital Negative) import plugin (compiled only for Linux so far)
Fixed multiple GIF image import bugs

20100607:

Added Morphology filters: dilate, erode, open, close, gradient and top hat
Changed some more GUI graphics (the style of the menus)
Did some restructuring of the hierarchy of the source code on SVN: split into main_program, user_manual and plugins, so that each plugin can have its own directory structure

20100606:

Added GIF format support (only loading). Animated gifs are loaded as a single image with all frames laid out horizontally.
Removed the triangle tool, it was replaced by the polygon tool long ago already
Added "Pointillism" filter, the first artistic filter

20100605:

Added palettes "CGA" and "Tango"
Fixed bugs with paste as selection: previous selection not lost anymore, and undo works for it
Made the opacity of the generate scanlines filter customizable
Gave the generate tiles filter two separate opacities.


20100604:

Added Generate Gradient Quad filter
Added "Overview" panel showing a small zoomed out version of the painting

20100603:

Made the tool icons smaller
Added Generate Gradient filter
Added shortcut for the pixel brush (i)
Made menus close more easily when clicking next to them

20100602:

Moved tool panel to the right
Added system to keep track of the OpenGL context for not cleaning up textures if it's destroyed

20100530:

Changed some of the toolbar icons to icons from the led24 icon set.
Added "gradient line" tool, it can only draw lines of 1-pixel thickness, but with gradient from left to right color

20100529:

Made the add/remove borders and rescale filters support all 4 color modes
Fixed another color slider bug

20100526:

Added "Levels" filter
Split the "Colors" menu into "Colors" and "Color Models", because it was getting too crowded.

20100524:

Did a few more cosmetic changes to the GUI, making window panels and buttons look slightly more appealing.
Added "generate XOR Texture" filter
Added the "Pixel Art" palette to the built-in palettes, with several good color transitions for pixel art
Made most of the Alpha filters support the RGBA32F color mode too.

20100523:

Made JPEG plugin that uses jpeglib (unlike the internal jpeg decoder in LodePaint), and which enables support for loading progressive JPEGs.

20100522:

Made "OpenEXR" plugin that supports encoding and decoding. Only built it for Linux so far (on Windows it requires finding out how to make the OpenEXR & co libraries work with some compiler)

20100521:

Started some internal refactoring in the code, to separate image algorithms from the program code.
Made the drawing of color sliders more efficient (by using texture instead of series of rectangles)
Added red, green, blue and alpha option to the Adjust brush.
Renamed "Quick Tone Map" to "Tone Map (Histogram Adjustment)" and made it work on 8 bit per channel images too.

20100520:

Added "Selective Gaussian Blur" filter
Made the selective and the regular gaussian blur filter support the greyscale color modes too
Made filter dialogs support the greyscale color modes too (only relevant for filters that work on greyscale)
Added "Quick Tone Map" filter for HDR images. But there are some issues with the way it treats colors currently. The colors become too saturated.

20100516:

-Redesigned the big color dialog (the one that pops up when double clicking a color or a palette)
-Redesigned the small color dialog (the one always visible in LodePaint)
-Added ability to choose a different palette in the small color dialog
-Added persistent, editable, palettes to the small color dialog
-Added automatically rememberred recent colors palette to the small color dialog

20100515:

-improved crosshair options
-selection tool now always has a crosshair
-fixed bug with shortcut for select tool: it accidently selected the magic want instead
-made the background pattern and mask color settings persistant and work on all images at once
-added pixel grid options, for grid sizes 1, 16, 32 or 64 (or none)

20100514:

-added crosshair options for mouse cursor (with toolbar button)

20100502:

-detect if the opengl driver is "generic" and in that case warn that lodepaint will be slow
-a few bugfixes for the windows version

20100418:

-polygon tool and rounded corner rectangle now work with outline enabled. There are bugs left with this but it works for most reasonable cases.

20100416:

-added a second toolbar with various filters (ones that are likely to be needed more often in some scenarios)
-Linux clipboard now also supports "image/bmp" clipboard images instead of only "image/png". This now allows pasting from OpenOffice. And this enables a workaround for the fact that you can't paste a big image from a Qt image to LodePaint (and also not to Gimp): paste it in OpenOffice Writer first, and then copy it there and paste it in LodePaint
-added "See Program Dirs" under the option menu, which shows paths used by the program

20100414:

-added "adjust" tool to adjust hue, saturation or lightness

20100411:

-added ability to use "ctrl+a" to select all text in input fields
-images indicate with a "*" in the name if they have unsaved changes, and the program asks to save unsaved changes when closing
-greyscale images can also be saved to PNG now
-upgraded development environment to use SDL 1.2.14 in Windows

20100410:

-ability to open multiple files at once with the file open dialog
-improved the pixel antialias filter

20100408:

File dialog improvements:
-filename input box has cursor active when dialog opens.
-When doing save as it goes to the path of the image if it already had one
-a new dropdown called "sug" added, which suggests the last 10 used folders, and remembers these in a hidden persistant config file
Added "Batch Mode" button to do filters on multiple images at once

20100407:

Text input fields now support pasting text in them (in Linux. For Windows it's still todo.)
Made paint windows of small new paintings smaller, and painting follows window when resizing it
Fixed a few bugs with Valgrind

20100406:

Added "mirror-tile" filter
Added "swap colors" filter (swaps bg and fg color in the image)

20100405:

Added "Pixel Antialias" filter

20100404:

added "LodePaint Image" file format (experimental)
made pixel brush and color picker work on all color modes
added "pixel antialias" filter
added "difference" mode to blend selection filter

20100402:

added status bars

20100331:

Fixed bug with non power of two floating point images with FFT and with saving HDR image files.
Fixed various problems with Grey32F images.
Changed names of the functions of Filter and File Format plugins to match color mode names in LodePaint (RGBA8 and such).

20100330:

Added "wrapping" and "saturation" option to contour filter
Small usability improvements: file dialog now also accepts the keypad enter, and filter dialogs accept both enters as "ok"

20100329:

extended blend selection filter with much more blend modes (mathematical operators, logical operators, min, max, ...)

20100328:

made menus a bit bigger (in the Y direction) so they're easier to click
added ability to copy RGBA8 images to the clipboard. It's copied to a private format, not a standard bitmap, so you can paste the data only into LodePaint (for now)
added "blend selection" filter. This is the first way so far to be able to blend multiple images together.

20100326:

added "sepia" filter
added greyscale mode (8-bit and 32-bit floating point)
the pen and standard brush can now be used on all 4 image color modes (both greyscale modes and both RGBA modes)
Added "pixel brush", which is always square and draws in a better way for some circumstances, because it doesn't use a "step" like the regular brush

20100324:

added "random walk" filter
added "allow freehand" checkbox to polygon settings to be able to disable the freehand mode
selection now remembers its "Background" setting if LodePaint is closed

20100323:

pressing home or end in text input field now deselects the potentially active text selection (if shift not down)
text input field now supports holding down shift while pressing left/right arrow key, home key or end key to change text selection
made cursor blinking behaviour better while doing key or mouse actions

20100322:

Floodfill now also works on 128-bit floating point images
Floating point images internally use different texture format more similar to other color mode textures

20100321:

Fixed the way lines were drawn with brushes of 1 pixel thick.
Internal refactoring in the code with the way masks, textures and image color modes work.
Masks now use 1 image channel instead of being an RGBA texture where only the alpha channel is used
The confusing "fix mask" operation is gone due to not needed anymore.
Changed "swap mask with image" operation into "swap mask with alpha" operation.
Added a toolbar icon for the invert mask operation.

20100315:

A few small GUI improvements with text input fields.
Fixed bug where on some computers the program could crash when drawing with a tool outside of the image bounderies.
Fixed some problems indicated by the valgrind debugger

20100314:

Added "affect alpha" option to many of the interactive tools
Support for opening multiple paintings at the same time added.

20100313:

Added command line arguments "--version", "--help" and "--license"
Added command line arguments "--fullscreen", "--nofullscreen", "--window", "--resizable" and "--noresizable"
Added "Random Shuffle" filter

20100312:

Added "Threshold alpha" and "Monochrome (Dithered) Alpha" filters

20100311:

Added "Clip Color", "Threshold" and "Monochrome (Dithered)" filters

20100310:

Added "curves" filter.

20100307:

Added "text" tool, it is quite primitive for now, supporting single line text and predefined small bitmap fonts only
Added ability to assign shortcut to "Reload" action
Bugfixes: bilinear rescale to smaller size could crash, blending with colors with very low alpha channel could reveal unwanted colors, color chooser input boxes didn't work

20100306:

Added "Background" option to the selection tool

20100305:

Added "Shift Bits" and "Mirror Bits" operations. The advantage is that they reveal subtle changes in color.
Made it so that if you "paste as selection", the selection is pasted in the visible part of the painting on screen, instead of the 0,0 coordinate which can be outside of the screen when zoomed in or panned.

20100304:

Added "Pixel Shadow" filter
Redesigned the tool icons and buttons to look a bit nicer.
Made the color of the window top bars a customizable option.
Made the GUI textures customizable by loading them from external PNG.
Added shortcut for setting FG and BG color to black and white ('z')
Changed capitalization of plugins folder into "Plugins". This has an effect on some operating/file systems.

20100302:

Changed the "Nothing" tool into "Pan" tool.

20100301:

Added "show full screen" button, which shows the painting in the whole LodePaint window (not actually goes to true fullscreen mode unless it already was due to the config setting)
Added "Binary" distributon to the Generate Noise filter.

20100228:

Added more tools and filters for the mask, such as invert mask, magic wand, selection to mask, ...
Gave the user manual a different color in the css file (greenish instead of purplish)

20100227:

Added polygon tool. Currently it only supports filled polygons without border.
Resize now works on selections and on 128-bit images.
Added "mask" feature. Currently the mask only blocks filters, not tools.

20100226:

Changed the C interface for programming plugins. Added ability for a filter plugin that results in image with different size.
Changed "Negate Saturation" filter into a more general Negate filter that allows choosing various channels to negate.
Added options for Gaussian noise, random opacity, and FG/BG color usage in the Generate Noise filter.

20100224:

Added rounded corners option to the rectangle tool. This doesn't use line settings yet however.

20102020:

Added a second channel switcher filter. Both are sort of each others inverse.

20100214:

Added "enum" parameter ability to SimpleFilter plugins
Added Median Filter

20100213:

Added support for File Format plugins and SimpleFilter plugins, through dynamic libraries, both for Linux and Windows.

20100210:

Added Unsharp Mask filter

20100209:

Added new shortcuts and operations for select all, select none, negate the brush colors, ...
Fixed infinite loop with 2x2 pixel ellipse
Added Gaussian Blur filter, it works on floating point textures too, allowing bloom and such
Added Solarize filter

20100208:

Added "Shortcuts" dialog with customizable shortcuts

20100207:

Added "paste as selection"
Added "ShortCutManager" class with the intention of having customizable shortcuts later
Added + and - shortcuts for zooming in/out, they zoom around the mouse cursor

20100129:

In the pen tool, shows only the opacity setting instead of all other, non-relevant-for-pen brush settings

20100127:

Made the "tab" key jump to next input field in number-matrix controls (user defined filter, RGBA matrix and 2D transform). More tabbed controls are planned for later for convenience.

20100126:

Made "x" key swap foreground and background color.

20100125:

Placed the code in subfolders: src and src/lpi

20100124:

Added "wrapping" and "lock scale" option to Rotate and Transform 2D filters
Added "perspective crop" tool
Added "inverse" option to Transform2D operation
Changed "Particle" filter into "Extract Cloud" and rewrote it to generate alpha out of two colors so you can e.g. extract white clouds out of a blue sky (use keep only alpha or apply bg to alpha after this filter)

20100123:

Made the code a bit more clean here and there: less header inclusions, some public members made private, some splitting in more source files
Added "negate saturation" filter
Added separators in some filter sub menus
Added an extra font for the GUI, which looks better than the system font here and there
Gave "Eraser" brush ability to use custom alpha value instead of only 0 for left and 255 for right mouse button
Allowed cycling through alpha background patterns in two directions (with right click on the toolbar button)

20100117:

Support for dynamically changing brush parameters added (only with ctrl+scrollwheel for now)

20100116:

Added "Absolute Value" filter
Added "Repeat" filter
Added extra pages in the manual

20100115:

Fixed bug with saving to TGA: it saved it upside down and with B and R switched.
Added support for Radiance RGBE (HDR) image format.
Made blur, sharpen and user defined filters work on floating point images - so you can blur HDR images now and see the whiteness spread.

20100113:

Added "Channel Switcher" filter
Added "Posterize" filter

20100112:

Added "smooth" option to rotate and transform 2D operations (it uses 4 samples per pixel but can easily be adjusted in the code to 9, 16, ...)
Added "Brightness/Contrast" operation

20100111:

Added "add/remove borders" operation, allowing adding/removing part independently on the 4 sides.
Added "rotate" operation (with arbitrary angle)
Added "Transform 2D" operation (with arbitrary 2x2 matrix)

20100110:

Added "circle by 2 points" and "circle by 3 points" modes to the ellipse tool.
Changed Global Tool Settings again. The tabs are removed. Only the settings
  relevant for the current tool are shown.
Changed alpha raster colors.

20100109:

Added "negate lightness" filter.
Made the initialization code output info about version and OS for better troubleshooting.
Changed the Global Tool Settings. Now they're just called Tool Settings. Brush and Geom (= the
  geometric shapes like lines and rectangles) settings are two tabs and are global, and the
  third tab is "Custom" and has settings specific for the selected tool, such as tolerance, ...
Fixed bug in blend formula of selection with opacity against background with opacity.
Added ellipse tool.

20100107:

Made the initialization code output some OpenGL strings about vendor etc... for better troubleshooting.

20100103:

Generate Grid filter added
The "negate (RGB)(A)" operation on floating point color works different now, instead
  of doing "1.0f - color", it does "0.0f - color", negating just the sign. There are
  two reasons for this: the first is that the negate operation is made so that it stores
  no undo information, and with floating point values there is loss of information (precision)
  when doing 1.0f - value, while all information is retained when just negating the sign.
  The second reason is that floating point color is a whole different beast than 8-bit per channel
  integer color, it can be negative and such. To emulate the effect of the integer negate, just
  do "normalize" after negate, or use color arithmetic. The FFT operation generates both negative
  and positive pixels, the floating point negate makes the negative ones visible.
Color Arithmetic II filter added
Other color arithmetic filter made to work on floating point textures
RGBA Matrix filter added
Made filter preview work properly on floating point textures
Added "Normalize" operation

20100102:

experimental support for floating-point RGBA color added (and Set Color Depth
  operations to choose between 32-bit RGBA and floating point RGBA). The floating
  point color uses a 32-bit floating point value per color channel (128-bit total).
  There is no file format support for this type of image yet, and almost nothing works on it:
  No tool works on it, and the only filters that work on it are FFT, IFFT and Negate RGB/A.
File dialog has a "automatically add extension" checkbox so you don't need to type ".png" in
  the file dialog, it'll add it automatically (as Windows users expect).
Changed algorithm of the real (I)FFT how it keeps the half of the real and imaginary part (better now)

20091227:

close button of file dialog now works
bug with scrolling in file dialog fixed

20091226:

"User Defined" filter added
Bug with brushes on the rightmost pixel row fixed

20091224:

Added "azerty" option in the settings. SDL uses OS keyboard layout normally, but on Windows not in some rare cases (shortcuts). Hence the setting. This option makes azerty keyboards to undo with "ctrl+z" like it should.

Dec 2009:

Generate tiles, clouds and marble filters
Rescale with multiple algo's, and pixel art scaling
Seperated "Image" and "Filters" menu
Removed "affect alpha" and "wrapping" as global settings, made them settings in each relevant filter instead
Changed default layout of the windows: The color and tool settings windows to the right
Added support to resize the window of the program dynamically
Changed name from "glpaint" to "LodePaint" (there already exist multiple apps called glpaint)

November 2009:

Added toolbar
Added tools select, eraser, color replacer, line, triangle, rectangle, sierpinski triangle
Added settings for geometric figures (line thickness, fill opacity, ...)
Improved file dialog: add extension chooser, ...
Added support for loading jpeg, bmp, tga and saving bmp, tga (PNG saving and loading already worked)
Made the filters and tools object oriented
Added filters such as sharpness, transform filters, alpha channel effects, DSP, generate noise, ...
Made working undo and redo
Moved the settings for background pattern through translucent image to a single toolbar button

Oktober 2009:

Added menu
Made file dialog and ability to save and load to any PNG file
Added file menu with recent file list etc...
Added more advanced color chooser
Made different shapes of the brush: square, diamond (and round already existed)
Gave the brush settings controls with number input box, slider and spinner all in one


Before 2009:

Made the "paint" window with in it zoomable, editable RGBA texture
Made the tools brush, pen, color picker and floodfill
Added brush settings to support thick and soft brushes
Added a window with buttons to negate rgb(a), greyscale, clear, and save/load to a fixed png file (no file dialog)
Added a window to choose background pattern behind translucent image

*/
