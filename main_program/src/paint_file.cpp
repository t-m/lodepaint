/*
LodePaint

Copyright (c) 2009-2010 Lode Vandevenne
All rights reserved.

This file is part of LodePaint.

LodePaint is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LodePaint is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LodePaint.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "paint_file.h"

#include "lpi/gui/lpi_gui_drawer_gl.h"
#include "lpi/lpi_os.h"

#include "paint_texture.h"

#include "main.h"

#ifdef LPI_OS_WINDOWS
#include "windows.h"
#include "SDL/SDL_syswm.h"
#endif

#ifdef LPI_OS_AMIGA
#include "amiga_save_open.h"
#endif

struct SaveImageMultithreadedParam
{
  bool success;
  std::string* error;
  std::string filename;
  PaintWindow* paintWindow;
  Progress* progress;
  IImageFormat* format;
  MainProgram* p;
};

int saveImageMultithreadedFun(void* param)
{
  SaveImageMultithreadedParam& p = *(SaveImageMultithreadedParam*)(param);
  p.success = false;
  
  std::vector<unsigned char> buffer;
  if(!p.format->encode(*p.error, buffer, p.paintWindow->getPaint()))
  {
    p.progress->setStatus(S_DONE);
    return 1;
  }
  std::string backupname = p.filename + "~";
  if(p.p->filebrowser->fileExists(p.filename) && !p.p->filebrowser->fileExists(backupname))
  {
    //TODO: use OS-dependent rename file function instead of manually copying using memory
    std::vector<unsigned char> b;
    lpi::loadFile(b, p.filename);
    lpi::saveFile(b, backupname);
  }
  lpi::saveFile(buffer, p.filename);
  p.success = true;
  p.progress->setStatus(S_DONE);
  return 0;
}

void saveImageFile(PaintWindow& paintWindow, const std::string& filename, IImageFormat* format, bool updatefilename, MainProgram& p)
{
  std::string error;
  if(!format->canEncode(paintWindow.getPaint())) error = "Can't encode to this format: \"" + format->getDescription() + "\"";
  else
  {
    Progress progress;
    SaveImageMultithreadedParam param;
    param.paintWindow = &paintWindow;
    param.error = &error;
    param.success = false;
    param.filename = filename;
    param.progress = &progress;
    param.format = format;
    param.p = &p;
    
    SDL_CreateThread(saveImageMultithreadedFun, &param);
    
    waitForProgressBar(p, progress);
  }
  
  if(!error.empty())
  {
    lpi::gui::showMessageBox(*p.c, *p.modalFrameHandler, error);
    return;
  }
  
  p.recentMenu->addFile(filename, *p.guidrawer);
  if(updatefilename)
  {
    paintWindow.setFilename(filename);
    paintWindow.setImageFormat(format);
  }
  paintWindow.setStatus(PaintWindow::S_SAVED);
}


bool saveFileDialogLpi(std::string& filename,
                       int& encoder,
                       const std::vector<std::string>& extensionTitles,
                       const std::vector<std::vector<std::string> >& extensions,
                       const std::string& currentPath,
                       MainProgram& p)
{
  lpi::gui::FileDialog dialog(*p.guidrawer, p.filebrowser, true, false, &p.fileDialogPersist);

  for(size_t i = 0; i < extensionTitles.size(); i++)
  {
    dialog.addExtensionSet(extensionTitles[i], extensions[i]);
  }
  
  if(!currentPath.empty()) dialog.setPath(currentPath);

  lpi::gui::Dialog::Result result = lpi::gui::getFileNameModal(*p.c, *p.modalFrameHandler, dialog, filename);

  encoder = dialog.getExtensionSet();

  return result == lpi::gui::Dialog::OK;
}

#ifdef LPI_OS_WINDOWS

/*
Windows users are a bit more picky about what file dialog they get, but fortunately
Windows also has a good standard file dialog available that is guaranteed to work!
*/
bool saveFileDialogWin32(std::string& filename,
                         int& encoder,
                         const std::vector<std::string>& extensionTitles,
                         const std::vector<std::vector<std::string> >& extensions,
                         const std::string& currentPath,
                         MainProgram& p)
{
  for(;;)
  {
    OPENFILENAME ofn;       // common dialog box structure
    char szFile[32768];       // buffer for file name
    HWND hwnd;              // owner window
    HANDLE hf;              // file handle

    std::string filter;
    for(size_t i = 0; i < extensionTitles.size(); i++)
    {
      filter += extensionTitles[i];
      filter += '\0';
      if(extensions[i].empty())
      {
        filter += "*.*\0";
      }
      else
      {
        for(size_t j = 0; j < extensions[i].size(); j++)
        {
          filter += "*." + extensions[i][j];
          filter += (j == (extensions[i].size() - 1) ? '\0' : ';');
        }
      }
    }
    filter.push_back('\0'); //lpstrFilter must be appended by two null characters (the second comes from c_str())

    // Initialize OPENFILENAME
    ZeroMemory(&ofn, sizeof(ofn));
    ofn.lStructSize = sizeof(ofn);
    //ofn.hwndOwner = hwnd;
    ofn.lpstrFile = szFile;
    // Set lpstrFile[0] to '\0' so that GetOpenFileName does not
    // use the contents of szFile to initialize itself.
    ofn.lpstrFile[0] = '\0';
    ofn.nMaxFile = sizeof(szFile);
    ofn.lpstrFilter = filter.c_str();
    ofn.nFilterIndex = 1;
    ofn.lpstrFileTitle = NULL;
    ofn.nMaxFileTitle = 0;
    ofn.lpstrInitialDir = NULL;
    ofn.Flags = OFN_EXPLORER; //OFN_OVERWRITEPROMPT not included, I do that manually here because I also add the extension manually if the user didn't type it.

    // Display the Save dialog box.

    if (GetSaveFileName(&ofn)==TRUE)
    {
      filename = ofn.lpstrFile;
      encoder = ofn.nFilterIndex;
      encoder--;
      if(encoder < 0 || encoder >= extensions.size()) return false; //invalid or custom type chosen. An encoder known by LodePaint is required (as index in the extension dropdown box)

      //if the user didn't specify an extension, add the default extension of the current choice
      if(ofn.nFileExtension == 0 && !extensions[encoder].empty())
      {
        filename += ".";
        filename += extensions[encoder][0];
      }

      if(lpi::fileExists(filename))
      {
        std::string message = filename + " already exists.\nDo you want to replace it?";
        int result = MessageBox( NULL,
                                 message.c_str(),
                                 "Save As",
                                 MB_YESNO | MB_ICONWARNING);
        if(result == IDNO) continue; //show the dialog again
        else if(result == IDYES) return true;
        else return false;
      }

      return true;
    }

    return false;
  }
}

#endif

//todo: add path in which the dialog starts as parameter
bool saveFileDialog(std::string& filename,
                    int& encoder,
                    const std::vector<std::string>& extensionTitles,
                    const std::vector<std::vector<std::string> >& extensions,
                    const std::string& currentPath,
                    MainProgram& p)
{
#if defined(LPI_OS_WINDOWS)
  if(p.optionsNow.useOSNativeFileDialog) return saveFileDialogWin32(filename, encoder, extensionTitles, extensions, currentPath, p);
  else return saveFileDialogLpi(filename, encoder, extensionTitles, extensions, currentPath, p);
#elif defined(LPI_OS_AMIGA)

  if(p.optionsNow.useOSNativeFileDialog)
  {
    p.c->handle(p.guidrawer->getInput());
    p.screen->cls();
    p.c->draw(*p.guidrawer);
    p.screen->redraw();

    bool result = do_save_request(0, 0, TRUE, extensionTitles) ;
    encoder = format; //this is the format variable from the save_requester header file
    filename = std::string(filename_amiga);
  
   if(add_amiga_auto_extenstion==TRUE)
  {
     filename += ".";
     filename += extensions[encoder][0];
  }

    return result;
  }
  else
  {
    return saveFileDialogLpi(filename, encoder, extensionTitles, extensions, currentPath, p);
  }
  
#else 
  return saveFileDialogLpi(filename, encoder, extensionTitles, extensions, currentPath, p);
#endif
}

void getSaveExtensions(std::map<int, IImageFormat*>& choices,
                       std::vector<std::string>& extensionTitles,
                       std::vector<std::vector<std::string> >& extensions,
                       PaintWindow& paintWindow,
                       MainProgram& p)
{
  int choice = 0;
  for(size_t i = 0; i < p.formats->formats.size(); i++)
  {
    IImageFormat* format = p.formats->formats[i];
    if(format->canEncode(paintWindow.getPaint()))
    {
      std::vector<std::string> exts;
      std::string title = format->getDescription() + " (";
      for(size_t j = 0; j < format->getNumExtensions(); j++)
      {
        exts.push_back(format->getExtension(j));
        if(j != 0) title += ", ";
        title += "*." + format->getExtension(j);
      }
      title += ")";
      extensionTitles.push_back(title);
      extensions.push_back(exts);
      choices[choice] = format;
      choice++;
    }
  }
}

void saveAs(PaintWindow& paintWindow, bool updatefilename, MainProgram& p) //updatefilename false gives "save copy as..." behaviour
{
  /*if(paintWindow.getPaint().getColorMode() == MODE_GREY_8 || paintWindow.getPaint().getColorMode() == MODE_GREY_32F)
  {
    lpi::gui::showMessageBox(*p.c, *p.modalFrameHandler, "Saving greyscale images to a file isn't supported yet.\nPlease convert the image to an RGBA color mode first.");
    return;
  }*/

  std::string filename;
  
  std::string currentPath;
  
  if(paintWindow.hasFilename())
  {
    currentPath = paintWindow.getFilename();
  }
  
  std::map<int, IImageFormat*> choices;
  std::vector<std::string> extensionTitles;
  std::vector<std::vector<std::string> > extensions;
  getSaveExtensions(choices, extensionTitles, extensions, paintWindow, p);

  int choice;
  bool result = saveFileDialog(filename, choice, extensionTitles, extensions, currentPath, p);
  if(result)
  {
    IImageFormat* format = choices[choice];
    if(format != 0) saveImageFile(paintWindow, filename, format, updatefilename, p);
  }
}

void save(PaintWindow& paintWindow, MainProgram& p)
{
  /*if(paintWindow.getPaint().getColorMode() == MODE_GREY_8 || paintWindow.getPaint().getColorMode() == MODE_GREY_32F)
  {
    lpi::gui::showMessageBox(*p.c, *p.modalFrameHandler, "Saving greyscale images to a file isn't supported yet.\nPlease convert the image to an RGBA color mode first.");
    return;
  }*/

  if(paintWindow.hasFilename() && paintWindow.getImageFormat()->canEncode(paintWindow.getPaint())) saveImageFile(paintWindow, paintWindow.getFilename(), paintWindow.getImageFormat(), true, p);
  else saveAs(paintWindow, true, p);
}

////////////////////////////////////////////////////////////////////////////////

struct OpenImageMultithreadedParam
{
  bool success;
  std::string* error;
  std::string filename;
  PaintWindow* paintWindow;
  Progress* progress;
  MainProgram* p;
  IImageFormat* o_format; //output parameter
};

int openImageMultithreadedFun(void* param)
{
  OpenImageMultithreadedParam& p = *(OpenImageMultithreadedParam*)(param);
  p.success = false;
  
  std::vector<unsigned char> buffer;
  lpi::loadFile(buffer, p.filename);

  IImageFormat* f = 0;
  for(size_t i = 0; i < p.p->formats->formats.size(); i++)
  {
    IImageFormat* format = p.p->formats->formats[i];
    if(format->canDecode(buffer.empty() ? 0 : &buffer[0], buffer.size()))
    {
      f = p.o_format = format;
      break;
    }
  }

  if(buffer.empty()) *p.error = "File is empty or nonexistant";
  else if(f == 0) *p.error = "Unknown File Format";
  
  if(p.error->empty()) p.success = f->decode(*p.error, p.paintWindow->getPaint(), &buffer[0], buffer.size());
  
  p.progress->setStatus(S_DONE);
  return 0;
}

bool openImageFile(PaintWindow& paintWindow, const std::string& filename, MainProgram& p)
{
  std::string error;
  
  Progress progress;
  OpenImageMultithreadedParam param;
  param.paintWindow = &paintWindow;
  param.error = &error;
  param.success = false;
  param.filename = filename;
  param.progress = &progress;
  param.p = &p;
  
  SDL_CreateThread(openImageMultithreadedFun, &param);
  
  waitForProgressBar(p, progress);
    
  if(!error.empty())
  {
    lpi::gui::showMessageBox(*p.c, *p.modalFrameHandler, error);
    return false;
  }
  else
  {
    paintWindow.getPaint().update();
    p.recentMenu->addFile(filename, *p.guidrawer);
    paintWindow.setFilename(filename);
    paintWindow.setImageFormat(param.o_format);
    paintWindow.setStatus(PaintWindow::S_SAVED);
    paintWindow.centerPaint();
  }
  
  return true;
}

struct MultithreadedPreviewParams
{
  bool busy;
  bool success;
  std::vector<unsigned char> pixels;
  std::vector<std::string> paths;
  MainProgram* p;
};

#define PREVIEWW 80
#define PREVIEWH 80

//calculate the preview for the file dialog in the background in another thread to not affect user input responsiveness
int doPreviewMultithreaded(void* param)
{
  MultithreadedPreviewParams& p = *(MultithreadedPreviewParams*)(param);
  Paint paint(p.p->optionsNow, 0);
  
  bool success = false;
  if(p.paths.size() == 1 && lpi::getFilesize(p.paths[0]) < 6000000)
  {
    std::vector<unsigned char> b;
    lpi::loadFile(b, p.paths[0]);
    
    std::vector<IImageFormat*>& formats = p.p->formats->formats;
  
    for(size_t i = 0; i < formats.size(); i++)
    {
      if(formats[i]->canDecode(b.empty() ? 0 : &b[0], b.size()))
      {
        std::string error;
        success = formats[i]->decode(error, paint, b.empty() ? 0 : &b[0], b.size());
        if(paint.getU() == 0 || paint.getV() == 0) success = false;
        if(success)
        {
          paint.setColorMode(MODE_RGBA_8, false);
          TextureRGBA8& texture = paint.canvasRGBA8;
          p.pixels.resize(PREVIEWW * PREVIEWH * 4);
          for(int y = 0; y < PREVIEWH; y++)
          for(int x = 0; x < PREVIEWW; x++)
          {
            int ty = (y * texture.getV()) / PREVIEWH;
            int tx = (x * texture.getU()) / PREVIEWW;
            p.pixels[y * PREVIEWW * 4 + x * 4 + 0] = texture.getBuffer()[ty * texture.getU2() * 4 + tx * 4 + 0];
            p.pixels[y * PREVIEWW * 4 + x * 4 + 1] = texture.getBuffer()[ty * texture.getU2() * 4 + tx * 4 + 1];
            p.pixels[y * PREVIEWW * 4 + x * 4 + 2] = texture.getBuffer()[ty * texture.getU2() * 4 + tx * 4 + 2];
            p.pixels[y * PREVIEWW * 4 + x * 4 + 3] = texture.getBuffer()[ty * texture.getU2() * 4 + tx * 4 + 3];
          }
          p.success = true;
        }
        break;
      }
    }
  }
  if(!success)
  {
    //texture = 0;
    p.success = false;
  }
  
  
  p.busy = false;
  return 0;
}

int cleanupPreviewMultithreaded(void* param)
{
  MultithreadedPreviewParams& p = *(MultithreadedPreviewParams*)(param);
  
  while(p.busy)
  {
    //wait
  }
  
  delete (MultithreadedPreviewParams*)param;
  
  return 0;
}

class PreviewTexture : public lpi::gui::Element
{
  private:
    MainProgram& p;
    bool ok;
    TextureRGBA8 texture;
    std::vector<std::string> paths;
    MultithreadedPreviewParams* params;
    bool calculating;

  public:
    PreviewTexture(MainProgram& p)
    : p(p)
    , ok(false)
    , params(new MultithreadedPreviewParams())
    , calculating(false)
    {
      params->busy = false;
      params->success = false;
      params->p = &p;
      
      createTextureRGBA<unsigned char>(&texture, PREVIEWW, PREVIEWH, lpi::RGB_White);
      texture.update();
      ok = true;
    }
    
    ~PreviewTexture()
    {
      SDL_CreateThread(cleanupPreviewMultithreaded, params);
    }
    
    void load(const std::vector<std::string>& paths)
    {
      this->paths = paths;
    }
  
    virtual void drawImpl(lpi::gui::IGUIDrawer& drawer) const
    {
      (void)drawer;
      if(ok)
        drawTextureSizedCentered(p.screen, &texture, getCenterX(), getCenterY(), PREVIEWW, PREVIEWH);
    }
    
    void handleImpl(const lpi::IInput& input)
    {
      (void)input;
      
      if(!params->busy)
      {
        if(calculating)
        {
          calculating = false;
          if(paths == params->paths)
          {
            if(params->success)
            {
              texture.setSize(PREVIEWW, PREVIEWH);
              for(int y = 0; y < PREVIEWH; y++)
              for(int x = 0; x < PREVIEWW; x++)
              {
                texture.getBuffer()[y * texture.getU2() * 4 + x * 4 + 0] = params->pixels[y * PREVIEWW * 4 + x * 4 + 0];
                texture.getBuffer()[y * texture.getU2() * 4 + x * 4 + 1] = params->pixels[y * PREVIEWW * 4 + x * 4 + 1];
                texture.getBuffer()[y * texture.getU2() * 4 + x * 4 + 2] = params->pixels[y * PREVIEWW * 4 + x * 4 + 2];
                texture.getBuffer()[y * texture.getU2() * 4 + x * 4 + 3] = params->pixels[y * PREVIEWW * 4 + x * 4 + 3];
              }
              texture.update();
              ok = true;
            }
            else
            {
              ok = false;
            }
          }
        }
        
        if(paths != params->paths)
        {
          calculating = true;
          params->paths = paths;
          params->busy = true;
          SDL_CreateThread(doPreviewMultithreaded, params);
          ok = false;
        }
        
      }
    }
};

class Preview : public lpi::gui::IFileDialogCustomControl
{
  MainProgram& p;
  PreviewTexture image;
  
  public:
  
  Preview(MainProgram& p)
  : p(p)
  , image(p)
  {
  }
  
  virtual lpi::gui::Element* getElement()
  {
    return &image;
  }
  virtual void onSelect(const std::vector<std::string>& paths)
  {
    image.load(paths);
  }
};


bool openFileDialogLpi(std::vector<std::string>& filenames,
                       const std::vector<std::string>& extensionTitles,
                       const std::vector<std::vector<std::string> >& extensions,
                       MainProgram& p,
                       bool allowMultiple)
{
  lpi::gui::FileDialog dialog(*p.guidrawer, p.filebrowser, false, allowMultiple, &p.fileDialogPersist);
  
  for(size_t i = 0; i < extensionTitles.size(); i++)
  {
    dialog.addExtensionSet(extensionTitles[i], extensions[i]);
  }

  lpi::gui::Dialog::Result result;

  
  Preview preview(p);
  dialog.addCustomControl(&preview, 100);
  
  if(allowMultiple)
  {
    result = lpi::gui::getFileNamesModal(*p.c, *p.modalFrameHandler, dialog, filenames);
  }
  else
  {
    filenames.resize(1);
    result = lpi::gui::getFileNameModal(*p.c, *p.modalFrameHandler, dialog, filenames[0]);
  }

  return result == lpi::gui::Dialog::OK;
}

#ifdef LPI_OS_WINDOWS

/*void printWin32Error()
{
  // Retrieve the system error message for the last-error code

  LPVOID lpMsgBuf;
  LPVOID lpDisplayBuf;
  DWORD dw = GetLastError();

  FormatMessage(
      FORMAT_MESSAGE_ALLOCATE_BUFFER |
      FORMAT_MESSAGE_FROM_SYSTEM |
      FORMAT_MESSAGE_IGNORE_INSERTS,
      NULL,
      dw,
      MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
      (LPTSTR) &lpMsgBuf,
      0, NULL );
      
  std::string msg;
  msg += (const char*)lpMsgBuf;

  std::cout << "error: " << msg << std::endl;
}*/


/*
Windows users are a bit more picky about what file dialog they get, but fortunately
Windows also has a good standard file dialog available that is guaranteed to work!
*/
bool openFileDialogWin32(std::vector<std::string>& filenames,
                         const std::vector<std::string>& extensionTitles,
                         const std::vector<std::vector<std::string> >& extensions,
                         MainProgram& p,
                         bool allowMultiple)
{
  bool result = false;

  std::string filter;
  for(size_t i = 0; i < extensionTitles.size(); i++)
  {
    filter += extensionTitles[i];
    filter += '\0';
    if(extensions[i].empty())
    {
      filter += "*.*\0";
    }
    else
    {
      for(size_t j = 0; j < extensions[i].size(); j++)
      {
        filter += "*." + extensions[i][j];
        filter += (j == (extensions[i].size() - 1) ? '\0' : ';');
      }
    }
  }
  filter.push_back('\0'); //lpstrFilter must be appended by two null characters (the second comes from c_str())

  OPENFILENAME ofn;       // common dialog box structure
  char szFile[32768];       // buffer for file name

  // Initialize OPENFILENAME
  ZeroMemory(&ofn, sizeof(ofn));
  ofn.lStructSize = sizeof(ofn);
  //ofn.hwndOwner = hwnd;
  ofn.lpstrFile = szFile;
  // Set lpstrFile[0] to '\0' so that GetOpenFileName does not
  // use the contents of szFile to initialize itself.
  ofn.lpstrFile[0] = '\0';
  ofn.nMaxFile = sizeof(szFile);
  ofn.lpstrFilter = filter.c_str(); //"All\0*.*\0Text\0*.TXT\0";
  ofn.nFilterIndex = 1;
  ofn.lpstrFileTitle = NULL;
  ofn.nMaxFileTitle = 0;
  ofn.lpstrInitialDir = NULL;
  ofn.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST | OFN_EXPLORER; //the OFN_EXPLORER is needed especially when OFN_ALLOWMULTISELECT is used, otherwise the dialog box looks like a Windows 3.11 dialog, and the returned string format is different
  
  if(allowMultiple) ofn.Flags |= OFN_ALLOWMULTISELECT;

  // Display the Open dialog box.

  if (GetOpenFileName(&ofn)==TRUE)
  {
    if(allowMultiple)
    {
      //Convert the returned string to std::vector of full paths.
      
      std::string dir;
      int i = 0;
      
      for(;;)
      {
        if(ofn.lpstrFile[i] == 0) break;
        dir += ofn.lpstrFile[i];
        i++;
      }
      
      if(ofn.lpstrFile[i + 1] == 0)
      {
        filenames.push_back(dir); //dir is now actually the full filename (only 1 file was selected in the dialog)
      }
      else
      {
        p.filebrowser->ensureDirectoryEndSlash(dir);
        for(;;)
        {
          if(ofn.lpstrFile[i] == 0) i++;
          if(ofn.lpstrFile[i] == 0) break;

          filenames.push_back(dir);

          for(;;)
          {
            if(ofn.lpstrFile[i] == 0) break;
            filenames.back() += ofn.lpstrFile[i];
            i++;
          }
        }
      }
    }
    else
    {
      filenames.resize(1);
      filenames[0] = ofn.lpstrFile;
    }
                      
    result = true;
  }
  else result = false;
  
  //In Windows when pressing cancel in the file dialog, then mouse focus of the SDL window is screwed up... The commented out code below contains various experiments to solve it, but none worked. TODO!!!
  /*SDL_SysWMinfo info;
  SDL_VERSION(&info.version);
  if(SDL_GetWMInfo(&info) == 1)
  {
    //SetFocus(NULL);
    //SetForegroundWindow(NULL);
    //SetCapture(NULL);
    //SetFocus(info.window);
    //SetForegroundWindow(info.window);
    //SetCapture(info.window);
    //std::cout << ReleaseCapture() << std::endl;
    //SDL_WM_GrabInput(SDL_GRAB_ON);
    //SDL_WM_IconifyWindow();
    //ShowWindow(info.window, SW_MINIMIZE);
    //ShowWindow(info.window, SW_RESTORE);
    //BringWindowToTop(info.window);
  }*/
  
  return result;
}

#endif

//todo: add path in which the dialog starts as parameter
bool openFileDialog(std::vector<std::string>& filenames,
                    const std::vector<std::string>& extensionTitles,
                    const std::vector<std::vector<std::string> >& extensions,
                    MainProgram& p,
                    bool allowMultiple)
{
#if defined(LPI_OS_WINDOWS)
  if(p.optionsNow.useOSNativeFileDialog) return openFileDialogWin32(filenames, extensionTitles, extensions, p, allowMultiple);
  else return openFileDialogLpi(filenames, extensionTitles, extensions, p, allowMultiple);
#else
  return openFileDialogLpi(filenames, extensionTitles, extensions, p, allowMultiple);
#endif
}

void getOpenExtensions(std::vector<std::string>& extensionTitles,
                       std::vector<std::vector<std::string> >& extensions,
                       MainProgram& p)
{
  std::vector<std::string> exts_known;
  for(size_t i = 0; i < p.formats->formats.size(); i++)
  {
    IImageFormat* format = p.formats->formats[i];
    if(format->canDecode())
    {
      for(size_t j = 0; j < format->getNumExtensions(); j++)
      {
        exts_known.push_back(format->getExtension(j));
      }
    }
  }
  extensionTitles.push_back("All known files");
  extensions.push_back(exts_known);
  
  for(size_t i = 0; i < p.formats->formats.size(); i++)
  {
    IImageFormat* format = p.formats->formats[i];
    if(format->canDecode())
    {
      std::vector<std::string> exts;
      std::string title = format->getDescription() + " (";
      for(size_t j = 0; j < format->getNumExtensions(); j++)
      {
        exts.push_back(format->getExtension(j));
        if(j != 0) title += ", ";
        title += "*." + format->getExtension(j);
      }
      title += ")";
      extensionTitles.push_back(title);
      extensions.push_back(exts);
    }
  }

  std::vector<std::string> exts_any; //empty vector indicates "any extension" instead of "none"
  extensionTitles.push_back("Any file");
  extensions.push_back(exts_any);
}

bool open(PaintWindow& paintWindow, MainProgram& p)
{
  std::vector<std::string> filenames;
  
  std::vector<std::string> extensionTitles;
  std::vector<std::vector<std::string> > extensions;
  getOpenExtensions(extensionTitles, extensions, p);

  bool result = openFileDialog(filenames, extensionTitles, extensions, p, false);
  if(result)
  {
    return openImageFile(paintWindow, filenames[0], p);
  }
  else return false;
}

bool open(Paintings& paintings, MainProgram& p)
{
#ifdef LPI_OS_AMIGA
  amiga_open_asl_library();
  amiga_open_file(paintings, p);
  amiga_close_asl_library();
#else
  std::vector<std::string> extensionTitles;
  std::vector<std::vector<std::string> > extensions;
  getOpenExtensions(extensionTitles, extensions, p);


  std::vector<std::string> filenames;
  bool result = openFileDialog(filenames, extensionTitles, extensions, p, true);
  if(result)
  {
    for(size_t i = 0; i < filenames.size(); i++)
    {
      std::string& filename = filenames[i];
      
      if(i == 0 && paintings.activePainting && paintings.activePainting->window.getStatus() == PaintWindow::S_NEW)
      {
        openImageFile(paintings.activePainting->window, filename, p);
      }
      else
      {
        Painting* w = new Painting(p.optionsNow, p.screen->getGLContext());
        w->window.make(0, 0, 1, 1, *p.guidrawer);
        if(openImageFile(w->window, filename, p))
        {
          addPaintWindow(p, paintings, w);
        }
        else delete w;
      }
    }
    return true;
  }
  else return false;
#endif
}

bool makeNewImage(MainProgram& p, PaintWindow& paintWindow, GlobalToolSettings& globalToolSettings, Options& options)
{
  lpi::gui::DynamicPage page;
  int width = options.newImageSizeX;
  int height = options.newImageSizeY;
  page.addControl("Width", new lpi::gui::DynamicValue<int>(&width));
  page.addControl("Height", new lpi::gui::DynamicValue<int>(&height));

  FilterDialog dialog(&page, *p.guidrawer, "New Image");

  int x0, y0, x1, y1;
  p.modalFrameHandler->getScreenSize(x0, y0, x1, y1);
  dialog.moveCenterTo((x0+x1)/2, (y0+y1)/2);

  p.c->doModalDialog(dialog, *p.modalFrameHandler);
  if(dialog.getResult() == lpi::gui::Dialog::CANCEL) return false;

  page.controlToValue();

  //paintWindow.getPaint().canvasRGBA8.setSize(width, height);
  createTextureRGBA<unsigned char>(&paintWindow.getPaint().canvasRGBA8, width, height, globalToolSettings.rightColor255());

  paintWindow.getPaint().update();

  paintWindow.clearFilename();
  paintWindow.setStatus(PaintWindow::S_NEW);
  
  return true;
}
