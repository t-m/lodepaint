/*
LodePaint

Copyright (c) 2009 Lode Vandevenne
All rights reserved.

This file is part of LodePaint.

LodePaint is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LodePaint is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LodePaint.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once
 
#include "paint_tool.h"
#include "lpi/lpi_math2d.h"

class Tool_Geom : public GeomDialogTool //helper class for most of the geom tools
{
  protected:
    bool justdone; //for undo
    UndoState tempundo;
    
  public:
  
    Tool_Geom(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom);
    
    virtual bool done();
    virtual void getUndoState(UndoState& state);
    
    
    virtual void undo(Paint& paint, UndoState& state);
    virtual void redo(Paint& paint, UndoState& state);
};

class Tool_Line : public Tool_Geom
{
  private:
    int x0;
    int y0;
    int x1;
    int y1;
    bool busy; //if false, next click is first point. If true, next click is second point.
    bool left; //true if first point was clicked with left mouse button, false if with right
    
  public:
  
    Tool_Line(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom);
  
    virtual void handle(const lpi::IInput& input, Paint& paint);
    virtual void draw(lpi::gui::IGUIDrawer& drawer, Paint& paint);
    

    
    virtual std::string getLabel() const { return "Line"; }
    
    virtual lpi::HTexture* getIcon() const { return &textures_tools[83]; }
};

class Tool_Rectangle : public Tool_Geom
{
  private:
    int x0;
    int y0;
    int x1;
    int y1;
    int point; //0 if not clicked yet, 1 if first point is placed (and will become 0 again since the shape is complete and next one can be started)
    bool left; //true if first point was clicked with left mouse button, false if with right
    
    int roundedcorners; //0 for none, radius of them otherwise
    bool square;
    
  public:
  
    Tool_Rectangle(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom);
  
    virtual void handle(const lpi::IInput& input, Paint& paint);
    virtual void draw(lpi::gui::IGUIDrawer& drawer, Paint& paint);
    
    virtual std::string getLabel() const { return "Rectangle"; }
    
    virtual lpi::HTexture* getIcon() const { return &textures_tools[80]; }
};

class Tool_Ellipse : public Tool_Geom
{
  private:
    int x0;
    int y0;
    int x1;
    int y1;
    int point; //0 if not clicked yet, 1 if first point is placed (and will become 0 again since the shape is complete and next one can be started)
    bool left; //true if first point was clicked with left mouse button, false if with right
    //clickStatus is used for translating mouse drags into clicks
    bool clickStatusL;
    bool clickStatusR;
    
    enum InputType
    {
      TYPE_ELLIPSE, //ellipse by 2 points (its bounding rectangle)
      TYPE_CIRCLE_2, //circle by 2 points (center and radius)
      TYPE_CIRCLE_3 //circle by 3 points (3 points of the side)
    };
    
    InputType type;

  public:

    Tool_Ellipse(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom);

    virtual void handle(const lpi::IInput& input, Paint& paint);
    virtual void draw(lpi::gui::IGUIDrawer& drawer, Paint& paint);

    virtual std::string getLabel() const { return "Ellipse"; }

    virtual lpi::HTexture* getIcon() const { return &textures_tools[81]; }
};

//this tool is there just for fun and to demonstrate the plugabbility of tools
class Tool_PerspectiveCrop : public ITool
{
  protected:
    bool justdone; //for undo
    UndoState tempundo;
    
    bool eight; //if true, give 8 instead of 4 points
    
    DynamicPageWithAutoUpdate dialog;
    
    void doit(Paint& paint);
    
  private:
    double x0; //top left
    double y0;
    double x1; //top right
    double y1;
    double x2; //bottom right
    double y2;
    double x3; //bottom left
    double y3;
    
    lpi::Vector2 p8[8]; //only used if "eight" is true
    
    int point; //0 if not clicked yet, 1 if first point is placed, 2 if second point is placed, 3 if third point is placed (and finally will become 0 again since the triangle is complete and next one can be started)

  public:

    virtual bool done();
    virtual void getUndoState(UndoState& state);


    virtual void undo(Paint& paint, UndoState& state);
    virtual void redo(Paint& paint, UndoState& state);

    Tool_PerspectiveCrop(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom);

    virtual void handle(const lpi::IInput& input, Paint& paint);
    virtual void draw(lpi::gui::IGUIDrawer& drawer, Paint& paint);

    virtual std::string getLabel() const { return "Perspective Crop"; }
    virtual lpi::HTexture* getIcon() const { return &textures_tools[128]; }
    virtual lpi::gui::Element* getDialog() { return &dialog; }
};

class Tool_Polygon : public Tool_Geom
{
  private:
    bool left; //true if first point was clicked with left mouse button, false if with right
    std::vector<int> xs;
    std::vector<int> ys;
    bool flag; //for stopping a polygon and getting ready for next polygon only next mouse press
    
    bool allow_freehand;
    
  public:
  
    Tool_Polygon(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom);
  
    virtual void handle(const lpi::IInput& input, Paint& paint);
    virtual void draw(lpi::gui::IGUIDrawer& drawer, Paint& paint);
    
    virtual std::string getLabel() const { return "Polygon"; }
    
    virtual lpi::HTexture* getIcon() const { return &textures_tools[82]; }
};

//this tool is there just for fun and to demonstrate the plugabbility of tools. TODO: if plugin for tools where you click a few points are possible, use this as example plugin.
class Tool_SierpinskiTriangle : public Tool_Geom
{
  private:
    int x0;
    int y0;
    int x1;
    int y1;
    int x2;
    int y2;
    int point; //0 if not clicked yet, 1 if first point is placed, 2 if second point is placed (and finally will become 0 again since the triangle is complete and next one can be started)
    bool left; //true if first point was clicked with left mouse button, false if with right
    
  public:
  
    Tool_SierpinskiTriangle(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom);
  
    virtual void handle(const lpi::IInput& input, Paint& paint);
    virtual void draw(lpi::gui::IGUIDrawer& drawer, Paint& paint);
    
    virtual std::string getLabel() const { return "Sierpinski Triangle"; }
    
    virtual lpi::HTexture* getIcon() const { return &textures_tools[129]; }
};


class Tool_GradientLine : public Tool_Geom
{
  private:
    int x0;
    int y0;
    int x1;
    int y1;
    bool busy; //if false, next click is first point. If true, next click is second point.
    bool left; //true if first point was clicked with left mouse button, false if with right
    
    void drawGradientLine(Paint& paint, int x0, int y0, int x1, int y1, bool colorInvert);

  public:

    Tool_GradientLine(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom);

    virtual void handle(const lpi::IInput& input, Paint& paint);
    virtual void draw(lpi::gui::IGUIDrawer& drawer, Paint& paint);



    virtual std::string getLabel() const { return "Gradient Line"; }

    virtual lpi::HTexture* getIcon() const { return &textures_tools[84]; }
};
