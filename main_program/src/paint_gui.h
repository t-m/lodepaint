/*
LodePaint

Copyright (c) 2009-2010 Lode Vandevenne
All rights reserved.

This file is part of LodePaint.

LodePaint is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LodePaint is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LodePaint.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once 


#include <SDL/SDL.h>
#include "lpi/lpi_color.h"
#include "lpi/sdl/lpi_event.h"
#include "lpi/gui/lpi_gui.h"
#include "lpi/gui/lpi_gui_color.h"
#include "lpi/gui/lpi_gui_dynamic.h"
#include "lpi/gui/lpi_gui_ex.h"
#include <vector>
#include <iostream>
#include <cmath>

#include "paint_gui_items.h"
#include "paint_tool.h"
#include "paint_settings.h"
#include "paint_palette.h"

//forward declarations
namespace lpi
{
  class ScreenGL;
  class StandByUtil;
  namespace gui
  {
    class GUIDrawerGL;
  }
}

class ToolsCollection;

void paintDebugMessage(const std::string& message);

class WindowInsideKeeper //keeps top left and top right of element inside the program
{
  std::vector<lpi::gui::Element*> elements;
  static const int DIST = 32;
  static const int DISTTOP = 0; //includes toolbar

  public:

  void add(lpi::gui::Element* element)
  {
    elements.push_back(element);
  }

  void handle(lpi::IInput& input, int width, int height);
  void handleMainContainerResize(int oldw, int oldh, int neww, int newh);
};

extern WindowInsideKeeper windowInsideKeeper;


struct MainProgram;

class MyModalFrameHandler : public lpi::gui::IModalFrameHandler //functionality the MainContainer needs to do modal gameloops
{
  protected:
    lpi::ScreenGL& screen;
    lpi::gui::GUIDrawerGL& guidrawer;
    MainProgram& h;
    lpi::StandByUtil standby;
    
  public:
  
    MyModalFrameHandler(lpi::ScreenGL& screen, lpi::gui::GUIDrawerGL& guidrawer, MainProgram& h)
    : screen(screen)
    , guidrawer(guidrawer)
    , h(h)
    {
    }
    
    virtual bool doFrame();
    
    virtual lpi::gui::IGUIDrawer& getDrawer();
    virtual void getScreenSize(int& x0, int& y0, int& x1, int& y1);
};

class ElementCompositeWrapping : public lpi::gui::AElementComposite //element with "internal container" to automatically handle child elements for you
{
  protected:
    lpi::gui::InternalContainerWrapping my_ic;

  protected:
    ElementCompositeWrapping() : lpi::gui::AElementComposite(my_ic) {};
    void addSubElement(Element* element) { my_ic.addSubElement(element); }
};

class BulletListWrapping : public lpi::gui::ABulletList, public ElementCompositeWrapping
{
  public:
    
    BulletListWrapping();
    ~BulletListWrapping();
    void clear();

    void make(unsigned long amount); //diff = the location difference between successive checkboxes

    int xDiff; //just added for "book keeping"
    int yDiff; //just added for "book keeping"
    
    virtual void drawImpl(lpi::gui::IGUIDrawer& drawer) const;
    virtual void handleImpl(const lpi::IInput& input);
    virtual const Element* hitTest(const lpi::IInput& input) const;
};

class ToolWindow : public lpi::gui::Window
{
  public:
    BulletListWrapping toolButtons;
    
    void make(int x, int y, const lpi::gui::IGUIDrawer& geom, const ToolsCollection& tools, MainProgram& h);
    
    void selectTool(size_t index)
    {
      toolButtons.set(index);
    }
    
    virtual int getMinSizeX() { return 64; }
};

class ColorWindow : public lpi::gui::Window
{
  protected:
  
    Options& options;

    void initPalettes();

  public:
    std::vector<Palette> palette; //0 and 1 are custom. 2 is recent. Rest is built-in defaults.
    lpi::gui::ColorSlidersRGB rgb_fg;
    lpi::gui::ColorSlidersRGB rgb_bg;
    lpi::gui::FGBGColor fgbg;
    lpi::gui::ColorDialog colordialog_fgbg;
    lpi::gui::ColorDialog colordialog_palette;
    lpi::gui::DropDownList pallist;
    lpi::gui::Button opaque;

    lpi::gui::ColorEditorSynchronizer colorSynchronizer;

    ColorWindow(const lpi::gui::IGUIDrawer& geom, Options& options);
    ~ColorWindow();

    virtual void handleImpl(const lpi::IInput& input);

    lpi::ColorRGB getLeftColor() const
    {
      return fgbg.getFG255();
    }

    lpi::ColorRGB getRightColor() const
    {
      return fgbg.getBG255();
    }

    lpi::ColorRGBd getLeftColord() const
    {
      return fgbg.getFG();
    }

    lpi::ColorRGBd getRightColord() const
    {
      return fgbg.getBG();
    }

    void setLeftColor(const lpi::ColorRGB& color)
    {
      colorSynchronizer.setMultiColor(lpi::gui::ColorEditor::FG, lpi::RGBtoRGBd(color));
    }

    void setRightColor(const lpi::ColorRGB& color)
    {
      colorSynchronizer.setMultiColor(lpi::gui::ColorEditor::BG, lpi::RGBtoRGBd(color));
    }

    void setLeftColor(const lpi::ColorRGBd& color)
    {
      colorSynchronizer.setMultiColor(lpi::gui::ColorEditor::FG, color);
    }

    void setRightColor(const lpi::ColorRGBd& color)
    {
      colorSynchronizer.setMultiColor(lpi::gui::ColorEditor::BG, color);
    }
    
    //saves the custom and the recent palette to persist
    void palettesToPersist(lpi::Persist& persist, const std::string& name);
    void palettesFromPersist(const lpi::Persist& persist, const std::string& name);
};

class OverviewWindow : public lpi::gui::Window
{
  private: 
  
    PaintWindow* w;
    
    void getTexturePos(int& tx0, int& ty0, int& tx1, int& ty1, const Paint& paint) const;
    
  public:
    
    OverviewWindow(const lpi::gui::IGUIDrawer& geom);
    void setPaintWindow(PaintWindow* w);
    
    virtual void drawImpl(lpi::gui::IGUIDrawer& drawer) const;
    virtual void handleImpl(const lpi::IInput& input);
  
  
};

class ShortCutField : public lpi::gui::Element
{
  private:
    
    bool active;
    bool readonly;
    lpi::gui::MouseState mouseState;
  public:
    KeyCombo key;
  
    ShortCutField(bool readonly = false);
    virtual void drawImpl(lpi::gui::IGUIDrawer& drawer) const; //also handles it by calling handle(): toggles when mouse down or not
    virtual void handleImpl(const lpi::IInput& input);
};
