/*
LodePaint

Copyright (c) 2009 Lode Vandevenne
All rights reserved.

This file is part of LodePaint.

LodePaint is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LodePaint is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LodePaint.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "lpi/lpi_color.h"

#include "paint_settings.h"

struct ExtraBrushOptions
{
  bool affect_r;
  bool affect_g;
  bool affect_b;
  bool affect_a;
  
  bool erase_content; //erases RGB content if alpha = 0

  ExtraBrushOptions();
  ExtraBrushOptions(bool affect_alpha);
};

void pset(unsigned char* buffer, int buffer_w, int buffer_h, int x, int y, const lpi::ColorRGB& color, int centerx, int centery, const Brush& brush, const ExtraBrushOptions& extra);
void horLine(unsigned char* buffer, int buffer_w, int buffer_h, int y, int x1, int x2, const lpi::ColorRGB& color, int centerx, int centery, const Brush& brush, const ExtraBrushOptions& extra);
void drawBrushShape(unsigned char* buffer, int buffer_w, int buffer_h, int xc, int yc, const lpi::ColorRGB& color, const Brush& brush, const ExtraBrushOptions& extra);
void drawThinLine(unsigned char* buffer, size_t u, size_t v, size_t u2, int x0, int y0, int x1, int y1, const lpi::ColorRGB& color, double opacity, const ExtraBrushOptions& extra, bool include_last_pixel);

void psetGrey8(unsigned char* buffer, int buffer_w, int buffer_h, int x, int y, int color, int centerx, int centery, const Brush& brush);
void horLineGrey8(unsigned char* buffer, int buffer_w, int buffer_h, int y, int x1, int x2, int color, int centerx, int centery, const Brush& brush);
void drawBrushShapeGrey8(unsigned char* buffer, int buffer_w, int buffer_h, int xc, int yc, int color, const Brush& brush);
void drawThinLineGrey8(unsigned char* buffer, size_t u, size_t v, size_t u2, int x0, int y0, int x1, int y1, int color, double opacity, bool include_last_pixel);

void pset(float* buffer, int buffer_w, int buffer_h, int x, int y, const lpi::ColorRGBd& color, int centerx, int centery, const Brush& brush, const ExtraBrushOptions& extra);
void horLine(float* buffer, int buffer_w, int buffer_h, int y, int x1, int x2, const lpi::ColorRGBd& color, int centerx, int centery, const Brush& brush, const ExtraBrushOptions& extra);
void drawBrushShape(float* buffer, int buffer_w, int buffer_h, int xc, int yc, const lpi::ColorRGBd& color, const Brush& brush, const ExtraBrushOptions& extra);
void drawThinLine(float* buffer, size_t u, size_t v, size_t u2, int x0, int y0, int x1, int y1, const lpi::ColorRGBd& color, double opacity, const ExtraBrushOptions& extra, bool include_last_pixel);

void psetGrey32f(float* buffer, int buffer_w, int buffer_h, int x, int y, float color, int centerx, int centery, const Brush& brush);
void horLineGrey32f(float* buffer, int buffer_w, int buffer_h, int y, int x1, int x2, float color, int centerx, int centery, const Brush& brush);
void drawBrushShapeGrey32f(float* buffer, int buffer_w, int buffer_h, int xc, int yc, float color, const Brush& brush);
void drawThinLineGrey32f(float* buffer, size_t u, size_t v, size_t u2, int x0, int y0, int x1, int y1, float color, double opacity, bool include_last_pixel);

