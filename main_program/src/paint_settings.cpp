/*
LodePaint

Copyright (c) 2009 Lode Vandevenne
All rights reserved.

This file is part of LodePaint.

LodePaint is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LodePaint is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LodePaint.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "paint_settings.h"
#include "lpi/lpi_os.h"

GlobalToolSettings::GlobalToolSettings()
: leftColor(lpi::RGBd_Black)
, rightColor(lpi::RGBd_White)
, crosshairMode(0)
{
}


CMDOptions::CMDOptions()
: override_fullscreen(false)
, override_resizable(false)
{
}

Options::Options()
: screenWidth(1024)
, screenHeight(700)
, undoSize(200)
, fullscreen(0)
, resizable(1)
, bgcolor(lpi::ColorRGB(53,33,33))
, drawingWindowColor(lpi::ColorRGB(255,255,255,224))
, windowTopColor(lpi::ColorRGB(196,140,128))
, newImageSizeX(512)
, newImageSizeY(512)
#if defined(LPI_OS_AMIGA) || defined(LPI_OS_AROS)
, skin("aos4")
#else
, skin("default")
#endif
, language("en")
, bgpattern(0)
, maskColorIndex(0)
, pixelGridIndex(1)
, pixelGridCustomWidth(16)
, pixelGridCustomHeight(16)
, selectedColorWindowPalette(3)
, useOSNativeFileDialog(true)
{
}

Brush::Brush()
: size(20)
, step(0.2)
, opacity(1.0)
, softness(0.5)
, shape(SHAPE_ROUND)
{
}

Shape::Shape()
: line_enabled(true)
, line_thickness(1)
, line_opacity(1.0)
, fill_enabled(false)
, fill_opacity(1.0)
{
}

DynamicBrushChange::DynamicBrushChange()
: change1(C_BRUSH_SIZE)
, amount1(5.0)
, change2(C_NOTHING)
, amount2(5.0)
{
}

Other::Other()
: alpha_as_opacity(true)
, affect_alpha(true)
, selection_bg_type(BG_BG)
{
}

void writeColorToPersist(lpi::Persist& persist, const lpi::ColorRGB& color, const std::string& name)
{
  std::stringstream ss;
  ss << color.r << " " << color.g << " " << color.b << " " << color.a;
  persist.setSetting(name, ss.str());
}

void readColorFromPersist(lpi::ColorRGB& color, const lpi::Persist& persist, const std::string& name, const lpi::ColorRGB& defaultColor)
{
  std::stringstream defaultss;
  defaultss << defaultColor.r << " " << defaultColor.g << " " << defaultColor.b << " " << defaultColor.a;

  std::stringstream ss;
  std::string s = persist.getSetting(name, defaultss.str());
  ss.str(s);

  ss >> color.r;
  ss >> color.g;
  ss >> color.b;
  ss >> color.a;
}

void writeColorToPersist(lpi::Persist& persist, const lpi::ColorRGBd& color, const std::string& name)
{
  std::stringstream ss;
  ss << color.r << " " << color.g << " " << color.b << " " << color.a;
  persist.setSetting(name, ss.str());
}

void readColorFromPersist(lpi::ColorRGBd& color, const lpi::Persist& persist, const std::string& name, const lpi::ColorRGBd& defaultColor)
{
  std::stringstream defaultss;
  defaultss << defaultColor.r << " " << defaultColor.g << " " << defaultColor.b << " " << defaultColor.a;

  std::stringstream ss;
  std::string s = persist.getSetting(name, defaultss.str());
  ss.str(s);

  ss >> color.r;
  ss >> color.g;
  ss >> color.b;
  ss >> color.a;
}

void writePaintSettingsToPersist(lpi::Persist& persist, const GlobalToolSettings& globalToolSettings, const std::string& name)
{
  writeColorToPersist(persist, globalToolSettings.leftColor, name + ".leftColor");
  writeColorToPersist(persist, globalToolSettings.rightColor, name + ".rightColor");

  persist.setSettingT(name + ".brush.size", globalToolSettings.brush.size);
  persist.setSettingT(name + ".brush.step", globalToolSettings.brush.step);
  persist.setSettingT(name + ".brush.opacity", globalToolSettings.brush.opacity);
  persist.setSettingT(name + ".brush.softness", globalToolSettings.brush.softness);
  persist.setSettingT(name + ".brush.shape", globalToolSettings.brush.shape);

  persist.setSettingT(name + ".shape.line_enabled", globalToolSettings.shape.line_enabled);
  persist.setSettingT(name + ".shape.line_thickness", globalToolSettings.shape.line_thickness);
  persist.setSettingT(name + ".shape.line_opacity", globalToolSettings.shape.line_opacity);
  persist.setSettingT(name + ".shape.fill_enabled", globalToolSettings.shape.fill_enabled);
  persist.setSettingT(name + ".shape.fill_opacity", globalToolSettings.shape.fill_opacity);
  
  persist.setSettingT(name + ".other.affect_alpha", globalToolSettings.other.affect_alpha);
  persist.setSettingT(name + ".other.selection_bg_type", globalToolSettings.other.selection_bg_type);
  
  persist.setSettingT(name + ".crosshairMode", globalToolSettings.crosshairMode);
}

void readPaintSettingsFromPersist(GlobalToolSettings& globalToolSettings, const lpi::Persist& persist, const std::string& name)
{
  readColorFromPersist(globalToolSettings.leftColor, persist, name + ".leftColor", globalToolSettings.leftColor);
  readColorFromPersist(globalToolSettings.rightColor, persist, name + ".rightColor", globalToolSettings.rightColor);

  persist.getSettingT(name + ".brush.size", globalToolSettings.brush.size);
  persist.getSettingT(name + ".brush.step", globalToolSettings.brush.step);
  persist.getSettingT(name + ".brush.opacity", globalToolSettings.brush.opacity);
  persist.getSettingT(name + ".brush.softness", globalToolSettings.brush.softness);
  persist.getSettingE(name + ".brush.shape", globalToolSettings.brush.shape);

  persist.getSettingT(name + ".shape.line_enabled", globalToolSettings.shape.line_enabled);
  persist.getSettingT(name + ".shape.line_thickness", globalToolSettings.shape.line_thickness);
  persist.getSettingT(name + ".shape.line_opacity", globalToolSettings.shape.line_opacity);
  persist.getSettingT(name + ".shape.fill_enabled", globalToolSettings.shape.fill_enabled);
  persist.getSettingT(name + ".shape.fill_opacity", globalToolSettings.shape.fill_opacity);
  
  persist.getSettingT(name + ".other.affect_alpha", globalToolSettings.other.affect_alpha);
  persist.getSettingE(name + ".other.selection_bg_type", globalToolSettings.other.selection_bg_type);
  
  persist.getSettingT(name + ".crosshairMode", globalToolSettings.crosshairMode);
}

void writeOptionsToPersist(lpi::Persist& persist, const Options& options, const std::string& name)
{
  persist.setSettingT(name + ".screenSizeX", options.screenWidth);
  persist.setSettingT(name + ".screenSizeY", options.screenHeight);
  persist.setSettingT(name + ".fullScreen", options.fullscreen);
  persist.setSettingT(name + ".screenResizable", options.resizable);
  persist.setSettingT(name + ".maxUndoSizeMB", options.undoSize);
  writeColorToPersist(persist, options.bgcolor, name + ".bgcolor");
  writeColorToPersist(persist, options.drawingWindowColor, name + ".drawingWindowColor");
  writeColorToPersist(persist, options.windowTopColor, name + ".windowTopColor");
  persist.setSettingT(name + ".newImageSizeX", options.newImageSizeX);
  persist.setSettingT(name + ".newImageSizeY", options.newImageSizeY);
  persist.setSettingT(name + ".skin", options.skin);
  persist.setSettingT(name + ".language", options.language);

  persist.setSettingT(name + ".dynChange1", options.dyn.change1);
  persist.setSettingT(name + ".dynChangeAmount1", options.dyn.amount1);
  persist.setSettingT(name + ".dynChange2", options.dyn.change2);
  persist.setSettingT(name + ".dynChangeAmount2", options.dyn.amount2);

  int pattern = options.bgpattern;
  if(pattern == 4 || pattern == 5) pattern = 0; //because non-blocked alpha channel BG's can be confusing, don't store those
  persist.setSettingT(name + ".bgpattern", pattern);
  persist.setSettingT(name + ".maskColorIndex", options.maskColorIndex);
  persist.setSettingT(name + ".pixelGridIndex", options.pixelGridIndex);
  persist.setSettingT(name + ".pixelGridCustomWidth", options.pixelGridCustomWidth);
  persist.setSettingT(name + ".pixelGridCustomHeight", options.pixelGridCustomHeight);

  persist.setSettingT(name + ".pathsOverride.plugindir", options.pathsOverride.plugindir);
  
  persist.setSettingT(name + ".selectedColorWindowPalette", options.selectedColorWindowPalette);

  persist.setSettingT(name + ".useOSNativeFileDialog", options.useOSNativeFileDialog);
}

void readOptionsFromPersist(Options& options, const lpi::Persist& persist, const std::string& name)
{
  persist.getSettingT(name + ".screenSizeX", options.screenWidth);
  persist.getSettingT(name + ".screenSizeY", options.screenHeight);
  persist.getSettingT(name + ".fullScreen", options.fullscreen);
  persist.getSettingT(name + ".screenResizable", options.resizable);
  persist.getSettingT(name + ".maxUndoSizeMB", options.undoSize);
  readColorFromPersist(options.bgcolor, persist, name + ".bgcolor", options.bgcolor);
  readColorFromPersist(options.drawingWindowColor, persist, name + ".drawingWindowColor", options.drawingWindowColor);
  readColorFromPersist(options.windowTopColor, persist, name + ".windowTopColor", options.windowTopColor);
  persist.getSettingT(name + ".newImageSizeX", options.newImageSizeX);
  persist.getSettingT(name + ".newImageSizeY", options.newImageSizeY);
  persist.getSettingT(name + ".skin", options.skin);
  persist.getSettingT(name + ".language", options.language);
  
  persist.getSettingE(name + ".dynChange1", options.dyn.change1);
  persist.getSettingT(name + ".dynChangeAmount1", options.dyn.amount1);
  persist.getSettingE(name + ".dynChange2", options.dyn.change2);
  persist.getSettingT(name + ".dynChangeAmount2", options.dyn.amount2);

  persist.getSettingT(name + ".bgpattern", options.bgpattern);
  persist.getSettingT(name + ".maskColorIndex", options.maskColorIndex);
  persist.getSettingT(name + ".pixelGridIndex", options.pixelGridIndex);
  persist.getSettingT(name + ".pixelGridCustomWidth", options.pixelGridCustomWidth);
  persist.getSettingT(name + ".pixelGridCustomHeight", options.pixelGridCustomHeight);
  
  persist.getSettingT(name + ".pathsOverride.plugindir", options.pathsOverride.plugindir);

  persist.getSettingT(name + ".selectedColorWindowPalette", options.selectedColorWindowPalette);
  
  persist.getSettingT(name + ".useOSNativeFileDialog", options.useOSNativeFileDialog);
  
}

void optionsLaterToOptionsNow(Options& now, Options& later) //only copy the settings that have effect immediatly
{
  Options temp = now;
  now = later;
  now.screenWidth = temp.screenWidth;
  now.screenHeight = temp.screenHeight;
  now.fullscreen = temp.fullscreen;
  now.resizable = temp.resizable;
  now.paths = temp.paths;
  now.pathsOverride = temp.pathsOverride;
}

void cmdoptionsToOptions(Options& options, const CMDOptions& cmdoptions)
{
  if(cmdoptions.override_fullscreen) options.fullscreen = cmdoptions.fullscreen;
  if(cmdoptions.override_resizable) options.resizable = cmdoptions.resizable;
}


////////////////////////////////////////////////////////////////////////////////

void recentFilesToPersist(lpi::Persist& persist, const lpi::gui::RecentFiles& recent, const std::string& name)
{
  //first remove all those that are now in there, in case the user chose to clear the list
  bool more = true;
  size_t i = 0;
  while(more)
  {
    std::stringstream ss;
    ss << name << "." << i;
    if(persist.hasSetting(ss.str()))
    {
      persist.removeSetting(ss.str());
      i++;
    }
    else more = false;
  }
  
  //then add the current ones
  for(size_t i = 0; i < recent.getNumFiles(); i++)
  {
    std::stringstream ss;
    ss << name << "." << i;
    persist.setSetting(ss.str(), recent.getFile(i));
  }
}

void persistToRecentFiles(lpi::gui::RecentFiles& recent, const lpi::Persist& persist, const std::string& name)
{
  bool more = true;
  size_t i = 0;
  while(more)
  {
    std::stringstream ss;
    ss << name << "." << i;
    if(persist.hasSetting(ss.str()))
    {
      recent.addFile(persist.getSetting(ss.str()));
      i++;
    }
    else more = false;
  }
}

void fileDialogToPersist(lpi::Persist& persist, const lpi::gui::FileDialogPersist& fd, const std::string& name)
{
  for(size_t i = 0; i < fd.sug.size(); i++)
  {
    std::stringstream ss;
    ss << name << ".filedialogsug." << i;
    persist.setSetting(ss.str(), fd.sug[i]);
  }
}

void persistToFileDialog(lpi::gui::FileDialogPersist& fd, const lpi::Persist& persist, const std::string& name)
{
  bool more = true;
  size_t i = 0;
  while(more)
  {
    std::stringstream ss;
    ss << name << ".filedialogsug." << i;
    if(persist.hasSetting(ss.str()))
    {
      fd.sug.push_back(persist.getSetting(ss.str()));
      i++;
    }
    else more = false;
  }
}
