/*
LodePaint

Copyright (c) 2009-2010 Lode Vandevenne
All rights reserved.

This file is part of LodePaint.

LodePaint is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LodePaint is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LodePaint.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "paint_filter.h"

class OperationFFT : public AOperationFilterUtil
{
  private:
    virtual void doit(TextureRGBA32F& texture, Progress& progress);
    virtual void doit(TextureRGBA8& texture, Progress& progress);
    virtual bool supportsColorMode(ColorMode mode) const { return sup(mode, true, true, false, false); }
    bool inverse;
    bool affect_alpha;
    static double doublecharconv; //static so that it is linked between the FFT and IFFT operation (default value should be 0.125)
    DynamicFilterPageWithPreview dialog;
  public:
    OperationFFT(GlobalToolSettings& settings, bool inverse, const lpi::gui::IGUIDrawer& geom);
    virtual lpi::gui::Element* getDialog() { /*dialog.page.valueToControl();*/ return &dialog; }
    virtual std::string getLabel() const { return inverse ? "IFFT (real)" : "FFT (real)"; }
    virtual std::string getHelp() { return inverse ? "[OperationIFFT Help]" : "[OperationFFT Help]"; }
};

class OperationFFTMagnitude : public AOperationFilterUtil
{
  private:
    virtual void doit(TextureRGBA8& texture, Progress& progress);
    bool affect_alpha;
    DynamicFilterPageWithPreview dialog;
    
  public:
    OperationFFTMagnitude(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom);
    virtual lpi::gui::Element* getDialog() { return &dialog; }
    virtual std::string getLabel() const { return "FFT (magnitude)"; }
    virtual std::string getHelp() { return "[OperationFFTMagnitude Help]"; }
};

class AOperationFFTFilter : public AOperationFilterUtil
{
  protected:

    bool affect_alpha;

  private:
  
    virtual void doit(TextureRGBA8& texture, Progress& progress);
    virtual void affectFreqDom(double* re, double* im, size_t u, size_t v) = 0;
    virtual void affectSpatialDom(double* d, size_t u, size_t v);
    
  public:
    AOperationFFTFilter(GlobalToolSettings& settings)
    : AOperationFilterUtil(settings, true, true)
    , affect_alpha(false)
    {
    }
};

class OperationHighPassFilter : public AOperationFFTFilter
{
  private:
    virtual void affectFreqDom(double* re, double* im, size_t u, size_t v);
    virtual void affectSpatialDom(double* d, size_t u, size_t v); //called AFTER it's transformed back from the frequency domain

    DynamicFilterPageWithPreview dialog;
    int radius;
    bool use_grey_instead_of_dc;
    double factor0;
    double factor1;

  public:
    OperationHighPassFilter(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom);

    virtual std::string getLabel() const { return "Low Cut Filter (FFT)"; } //this is normally called "High Pass Filter", but Low Cut is also a synonim and this way the filter isn't confused with the other High Pass filter
    virtual lpi::gui::Element* getDialog() { return &dialog; }
    virtual std::string getHelp() { return "[OperationHighPassFilter Help]"; }
};

class OperationLowPassFilter : public AOperationFFTFilter
{
  private:
    virtual void affectFreqDom(double* re, double* im, size_t u, size_t v);

    DynamicFilterPageWithPreview dialog;
    int radius;
    double factor0;
    double factor1;

  public:
    OperationLowPassFilter(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom);

    virtual std::string getLabel() const { return "Low Pass Filter (FFT)"; }
    virtual lpi::gui::Element* getDialog() { return &dialog; }
    virtual std::string getHelp() { return "[OperationLowPassFilter Help]"; }
};

class OperationBandPassFilter : public AOperationFFTFilter
{
  private:
    virtual void affectFreqDom(double* re, double* im, size_t u, size_t v);
    virtual void affectSpatialDom(double* d, size_t u, size_t v); //called AFTER it's transformed back from the frequency domain

    DynamicFilterPageWithPreview dialog;
    int radius0;
    int radius1;
    bool use_grey_instead_of_dc;
    double factor0;
    double factor1;

  public:
    OperationBandPassFilter(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom);

    virtual std::string getLabel() const { return "Band Pass Filter (FFT)"; }
    virtual lpi::gui::Element* getDialog() { return &dialog; }
    virtual std::string getHelp() { return "[OperationBandPassFilter Help]"; }
};

class OperationBandStopFilter : public AOperationFFTFilter
{
  private:
    virtual void affectFreqDom(double* re, double* im, size_t u, size_t v);

    DynamicFilterPageWithPreview dialog;
    int radius0;
    int radius1;
    double factor0;
    double factor1;

  public:
    OperationBandStopFilter(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom);

    virtual std::string getLabel() const { return "Band Stop Filter (FFT)"; }
    virtual lpi::gui::Element* getDialog() { return &dialog; }
    virtual std::string getHelp() { return "[OperationBandStopFilter Help]"; }
};

//TODO
//class OperationFFTExtra : public AOperationFFTFilter
//{
  //private:
    //virtual void affectFreqDom(double* re, double* im, size_t u, size_t v);
    //virtual void affectSpatialDom(double* d, size_t u, size_t v); //called AFTER it's transformed back from the frequency domain

    //DynamicPageWithAutoUpdate dialog;
    //double im_factor;
    //double re_factor;
    //bool swap_re_im;
    ////shift of frequency domain
    //int shift_x;
    //int shift_y;
    //bool keep_dc;
    //bool add_grey;

  //public:
    //OperationFFTExtra(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom)
    //: AOperationFFTFilter(settings)
    //{
    //}

    //virtual std::string getLabel() const { return "FFT Extra"; }

    //virtual lpi::gui::Element* getDialog() { return &dialog; }
//};
