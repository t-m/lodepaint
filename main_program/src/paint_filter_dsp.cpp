/*
LodePaint

Copyright (c) 2009 Lode Vandevenne
All rights reserved.

This file is part of LodePaint.

LodePaint is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LodePaint is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LodePaint.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "paint_filter_dsp.h"

#include "lpi/lpi_math.h"
#include "lpi/lpi_math2d.h"
#include "lpi/gui/lpi_gui_color.h"

#include "paint_algos.h"
#include "paint_algos_dsp.h"

#include <iostream>
#include <limits>

////////////////////////////////////////////////////////////////////////////////


namespace
{



void shift50ForFFT(TextureRGBA8& texture, bool inverse)
{
  unsigned char* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();
  
  bool oddu = (u % 2 == 1);
  bool oddv = (v % 2 == 1);
  
  if(!oddu && !oddv)
  {
    for(size_t y = 0; y < v / 2; y++)
    for(size_t x = 0; x < u; x++)
    for(size_t c = 0; c < 4; c++)
    {
      size_t index = y * u2 * 4 + x * 4 + c;
      size_t x2 = x + u / 2; if(x2 >= u) x2 -= u;
      size_t y2 = y + v / 2; if(y2 >= v) y2 -= v;
      size_t index2 = y2 * u2 * 4 + x2 * 4 + c;
      std::swap(buffer[index2], buffer[index]);
    }
  }
  else
  {
    if(inverse) shiftImageSlow(texture, (u + 1) / 2, (v + 1) / 2);
    else shiftImageSlow(texture, u / 2, v / 2);
  }
}

void shift50ForFFT(TextureRGBA32F& texture, bool inverse)
{
  float* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();

  bool oddu = (u % 2 == 1);
  bool oddv = (v % 2 == 1);

  if(!oddu && !oddv)
  {
    for(size_t y = 0; y < v / 2; y++)
    for(size_t x = 0; x < u; x++)
    for(size_t c = 0; c < 4; c++)
    {
      size_t index = y * u2 * 4 + x * 4 + c;
      size_t x2 = x + u / 2; if(x2 >= u) x2 -= u;
      size_t y2 = y + v / 2; if(y2 >= v) y2 -= v;
      size_t index2 = y2 * u2 * 4 + x2 * 4 + c;
      std::swap(buffer[index2], buffer[index]);
    }
  }
  else
  {
    if(inverse) shiftImageSlow(texture, (u + 1) / 2, (v + 1) / 2);
    else shiftImageSlow(texture, u / 2, v / 2);
  }
}
}

double OperationFFT::doublecharconv = 0.125;

void OperationFFT::doit(TextureRGBA8& texture, Progress& progress)
{
  typedef float Scalar;
  
  if(progress.handle(0, 5)) return;
  if(inverse) shift50ForFFT(texture, inverse);
  if(progress.handle(1, 5)) return;

  //DOUBLECHARCONV: the lower the value, the better colors and so on are preserved, but this is at the cost of higher frequencies. The higher, the better higher frequencies are conserved but low frequencies cause weird colors.
  const Scalar DOUBLECHARCONV = (Scalar)doublecharconv;

  unsigned char* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();
  
  Scalar isqrtuv = 1.0 / std::sqrt(u * v);

  std::vector<Scalar> ore(u * v * 4);

  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  for(size_t c = 0; c < 4; c++)
  {
    size_t index = y * u * 4 + x * 4 + c;
    size_t index2 = y * u2 * 4 + x * 4 + c;
    
    bool dc = (x == 0 && y == 0);

    if(inverse)
    {
      if(dc)
      {
        ore[index] = buffer[index2] / isqrtuv;
      }
      else
      {
        ore[index] = (buffer[index2] - 128.0) / DOUBLECHARCONV;
      }
    }
    else
    {
      ore[index] = buffer[index2];
    }
  }
  
  if(progress.handle(2, 5)) return;
  FFT2D_real(&ore[0], u, v, inverse, affect_alpha);
  if(progress.handle(3, 5)) return;
  multiplyForFFT(&ore[0], u, v, isqrtuv, affect_alpha);
  if(progress.handle(4, 5)) return;

  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  for(size_t c = 0; c < 4; c++)
  {
    if(!affect_alpha && c == 3) continue;
    
    bool dc = (x == 0 && y == 0);
    
    size_t index = y * u * 4 + x * 4 + c;
    size_t index2 = y * u2 * 4 + x * 4 + c;

    if(inverse)
    {
      Scalar value = ore[index];

      if(value < 0) value = 0;
      if(value > 255) value = 255;

      buffer[index2] = (int)(value);
    }
    else
    {
      if(dc)
      {
        Scalar value = ore[index] * isqrtuv;

        if(value < 0) value = 0;
        if(value > 255) value = 255;

        buffer[index2] = (int)(value + 0.5);
      }
      else
      {
        Scalar value = ore[index];
        
        value *= DOUBLECHARCONV;
        value += 128.0;

        if(value < 0) value = 0;
        if(value > 255) value = 255;
        
        //without the rounding by adding 0.5, the whole frequency domain is off by a very small value,
        //but having the whole image off by one constant value happens to cause a very visible artefact when
        //taking the IFFT of the image: black and white dots in the corners.
        buffer[index2] = (int)(value + 0.5);
      }
    }
  }
  
  if(progress.handle(5, 5)) return;

  if(!inverse) shift50ForFFT(texture, inverse);
}

/*
Converts buffer of a texture where u2 isn't u, to one where it is, reusing the
same memory locations.

DDDX
DDDX
DDDX
XXXX

to

DDDD
DDDD
DXXX
XXXX
*/
void alignBuffer(float* buffer, int u, int v, int u2)
{
  for(int y = 0; y < v; y++)
  for(int x = 0; x < u; x++)
  for(int c = 0; c < 4; c++)
  {
    buffer[y * u * 4 + x * 4 + c] = buffer[y * u2 * 4 + x * 4 + c];
  }
}

//inverse operation of alignBuffer
void unAlignBuffer(float* buffer, int u, int v, int u2)
{
  for(int y = v - 1; y >= 0; y--)
  for(int x = u - 1; x >= 0; x--)
  for(int c = 0; c < 4; c++)
  {
    buffer[y * u2 * 4 + x * 4 + c] = buffer[y * u * 4 + x * 4 + c];
  }
}

OperationFFT::OperationFFT(GlobalToolSettings& settings, bool inverse, const lpi::gui::IGUIDrawer& geom)
: AOperationFilterUtil(settings, true, true), inverse(inverse)
, affect_alpha(false)
, dialog(this)
{
  params.addBool("Affect Alpha", &affect_alpha);
  params.addDouble("Trade-off (noise <-> waves)", &doublecharconv, 0.03125, 1.0, 0.03125);
  
  paramsToDialog(dialog.page, params, geom);
}

void OperationFFT::doit(TextureRGBA32F& texture, Progress& progress)
{
  if(progress.handle(0, 5)) return;
  if(inverse) shift50ForFFT(texture, inverse);
  if(progress.handle(1, 5)) return;

  float* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();
  alignBuffer(buffer, u, v, u2);

  float isqrtuv = 1.0 / std::sqrt(u * v);

  std::vector<float> ore(u * v * 4);

  if(progress.handle(2, 5)) return;
  FFT2D_real(buffer, u, v, inverse, affect_alpha);
  if(progress.handle(3, 5)) return;
  multiplyForFFT(buffer, u, v, isqrtuv, affect_alpha);
  if(progress.handle(4, 5)) return;
  
  unAlignBuffer(buffer, u, v, u2);
  if(progress.handle(5, 5)) return;

  if(!inverse) shift50ForFFT(texture, inverse);
}

////////////////////////////////////////////////////////////////////////////////

OperationFFTMagnitude::OperationFFTMagnitude(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom)
: AOperationFilterUtil(settings, true, true)
, affect_alpha(false)
, dialog(this)
{
  params.addBool("Affect Alpha", &affect_alpha);
  
  paramsToDialog(dialog.page, params, geom);
}

void OperationFFTMagnitude::doit(TextureRGBA8& texture, Progress& progress)
{
  unsigned char* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();
  
  double isqrtuv = 1.0 / std::sqrt(u * v);

  std::vector<double> ire(u * v * 4);
  std::vector<double> iim(u * v * 4);
  std::vector<double> ore(u * v * 4);
  std::vector<double> oim(u * v * 4);

  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  for(size_t c = 0; c < 4; c++)
  {
    size_t index = y * u * 4 + x * 4 + c;
    size_t index2 = y * u2 * 4 + x * 4 + c;

    ire[index] = buffer[index2];
    iim[index] = 0.0;
  }

  if(progress.handle(1, 4)) return;
  FFT2D(&ore[0], &oim[0], &ire[0], &iim[0], u, v, false, affect_alpha);
  if(progress.handle(2, 4)) return;
  multiplyForFFT(&ore[0], &oim[0], u, v, isqrtuv, affect_alpha);
  if(progress.handle(3, 4)) return;

  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  for(size_t c = 0; c < 4; c++)
  {
    if(!affect_alpha && c == 3) continue;
    
    size_t index = y * u * 4 + x * 4 + c;
    size_t index2 = y * u2 * 4 + x * 4 + c;

    //the upper half has the real part, the bottom half the imaginary part. The result is that all information is contained in the image. (except for the huge loss due to unsigned chars, preventing proper inverse)
    double value = std::sqrt(ore[index] * ore[index] + oim[index] * oim[index]);

    if(value < 0) value = 0;
    if(value > 255) value = 255;
    
    buffer[index2] = (int)(value);
  }

  shift50ForFFT(texture, false);
}

////////////////////////////////////////////////////////////////////////////////

void AOperationFFTFilter::affectSpatialDom(double* d, size_t u, size_t v)
{
  //default implementation does no postprocessing of spatial domain
  (void)d;
  (void)u;
  (void)v;
}

void AOperationFFTFilter::doit(TextureRGBA8& texture, Progress& progress)
{
  unsigned char* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();

  std::vector<double> ire(u * v * 4);
  std::vector<double> iim(u * v * 4);
  std::vector<double> ore(u * v * 4);
  std::vector<double> oim(u * v * 4);


  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  for(size_t c = 0; c < 4; c++)
  {
    size_t index = y * u * 4 + x * 4 + c;
    size_t index2 = y * u2 * 4 + x * 4 + c;
    ire[index] = buffer[index2];
    iim[index] = 0.0;
  }

  if(progress.handle(1, 7)) return;
  FFT2D(&ore[0], &oim[0], &ire[0], &iim[0], u, v, false, affect_alpha);
  if(progress.handle(2, 7)) return;
  affectFreqDom(&ore[0], &oim[0], u, v);
  if(progress.handle(3, 7)) return;
  FFT2D(&ire[0], &iim[0], &ore[0], &oim[0], u, v, true, affect_alpha);
  if(progress.handle(4, 7)) return;
  multiplyForFFT(&ire[0], &iim[0], u, v, 1.0 / (u * v), affect_alpha);
  if(progress.handle(5, 7)) return;
  affectSpatialDom(&ire[0], u, v);
  if(progress.handle(6, 7)) return;

  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  for(size_t c = 0; c < 4; c++)
  {
    if(!affect_alpha && c == 3) continue;
    size_t index = y * u * 4 + x * 4 + c;
    size_t index2 = y * u2 * 4 + x * 4 + c;
    double value =  ire[index];
    if(value < 0) value = 0;
    if(value > 255) value = 255;
    buffer[index2] = (int)(value);
  }
}

//void AOperationFFTFilter::doit(TextureRGBA8& texture, Progress&)
//{
//  unsigned char* buffer = texture.getBuffer();
//  size_t u = texture.getU();
//  size_t v = texture.getV();
//  size_t u2 = texture.getU2();
//  size_t v2 = texture.getV2();
//
//  std::vector<double> ire(u2 * v2 * 4);
//  std::vector<double> iim(u2 * v2 * 4);
//  std::vector<double> ore(u2 * v2 * 4);
//  std::vector<double> oim(u2 * v2 * 4);
//
//  //clean up the random data outside of (u, v) inside (u2, v2) range
//  for(size_t y = 0; y < v2; y++)
//  for(size_t x = 0; x < u2; x++)
//  {
//    if(y >= v || x >= u)
//    {
//      size_t index = y * u2 * 4 + x * 4;
//      buffer[index + 0] = 0;
//      buffer[index + 1] = 0;
//      buffer[index + 2] = 0;
//      buffer[index + 3] = 255;
//    }
//  }
//
//  for(size_t y = 0; y < v2; y++)
//  for(size_t x = 0; x < u2; x++)
//  for(size_t c = 0; c < 4; c++)
//  {
//    size_t index = y * u2 * 4 + x * 4 + c;
//    ire[index] = buffer[index];
//    iim[index] = 0.0;
//  }
//
//  FFT2D(&ore[0], &oim[0], &ire[0], &iim[0], u2, v2, false);
//  for(size_t y = 0; y < v2; y++)
//  for(size_t x = 0; x < u2; x++)
//  {
//    int x2 = x;
//    int y2 = y;
//    if(x2 > u2 / 2) x2 -= u2;
//    if(y2 > v2 / 2) y2 -= v2;
//    //if((x < radius && y < radius) || (u2 - x < radius && v2 - y < radius))
//    if((x2*x2 + y2*y2) < radius * radius)
//    {
//      if(!use_grey_instead_of_dc && x == 0 && y == 0) continue; //keep dc
//      size_t index = y * u2 * 4 + x * 4;
//      ore[index + 0] = 0;
//      oim[index + 0] = 0;
//      ore[index + 1] = 0;
//      oim[index + 1] = 0;
//      ore[index + 2] = 0;
//      oim[index + 2] = 0;
//    }
//  }
//  FFT2D(&ire[0], &iim[0], &ore[0], &oim[0], u2, v2, true);
//
//  for(size_t y = 0; y < v2; y++)
//  for(size_t x = 0; x < u2; x++)
//  for(size_t c = 0; c < 4; c++)
//  {
//    size_t index = y * u2 * 4 + x * 4 + c;
//    double value =  ire[index];
//    if(use_grey_instead_of_dc) value += 128;
//    if(value < 0) value = 0;
//    if(value > 255) value = 255;
//    buffer[index] = (int)(value);
//  }
//}

////////////////////////////////////////////////////////////////////////////////

OperationHighPassFilter::OperationHighPassFilter(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom)
: AOperationFFTFilter(settings)
, dialog(this)
, radius(2)
, use_grey_instead_of_dc(false)
, factor0(0)
, factor1(1)
{
  params.addInt("Radius", &radius, 2, 500, 1);
  params.addBool("Grey instead of DC", &use_grey_instead_of_dc);
  params.addDouble("Stop factor", &factor0, 0.0, 1.0, 0.01);
  params.addDouble("Pass factor", &factor1, 0.0, 4.0, 0.015);
  params.addBool("Affect Alpha", &affect_alpha);
  
  paramsToDialog(dialog.page, params, geom);
}

void OperationHighPassFilter::affectFreqDom(double* re, double* im, size_t u, size_t v)
{
  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  {
    int x2 = x;
    int y2 = y;
    if((size_t)x2 > u / 2) x2 -= u;
    if((size_t)y2 > v / 2) y2 -= v;
    bool stop = ((x2*x2 + y2*y2) < radius * radius);
    if(!use_grey_instead_of_dc && x2 == 0 && y2 == 0) stop = false; //keep DC component
    double factor = stop ? factor0 : factor1;

    size_t index = y * u * 4 + x * 4;
    re[index + 0] *= factor;
    im[index + 0] *= factor;
    re[index + 1] *= factor;
    im[index + 1] *= factor;
    re[index + 2] *= factor;
    im[index + 2] *= factor;
    re[index + 3] *= factor;
    im[index + 3] *= factor;
  }
}

void OperationHighPassFilter::affectSpatialDom(double* d, size_t u, size_t v)
{
  if(!use_grey_instead_of_dc) return;
  
  for(size_t i = 0; i < u * v; i++)
  for(size_t c = 0; c < 4; c++)
  {
    size_t index = i * 4 + c;
     d[index] = 128 + d[index] / 2;
  }
}

////////////////////////////////////////////////////////////////////////////////

OperationLowPassFilter::OperationLowPassFilter(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom)
: AOperationFFTFilter(settings)
, dialog(this)
, radius(2)
, factor0(0)
, factor1(1)
{
  params.addInt("Radius", &radius, 2, 500, 1);
  params.addDouble("Stop factor", &factor0, 0.0, 1.0, 0.01);
  params.addDouble("Pass factor", &factor1, 0.0, 4.0, 0.015);
  params.addBool("Affect Alpha", &affect_alpha);
  
  paramsToDialog(dialog.page, params, geom);
}

void OperationLowPassFilter::affectFreqDom(double* re, double* im, size_t u, size_t v)
{
  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  {
    int x2 = x;
    int y2 = y;
    if((size_t)x2 > u / 2) x2 -= u;
    if((size_t)y2 > v / 2) y2 -= v;
    bool stop = ((x2*x2 + y2*y2) >= radius * radius);
    double factor = stop ? factor0 : factor1;

    size_t index = y * u * 4 + x * 4;
    re[index + 0] *= factor;
    im[index + 0] *= factor;
    re[index + 1] *= factor;
    im[index + 1] *= factor;
    re[index + 2] *= factor;
    im[index + 2] *= factor;
    re[index + 3] *= factor;
    im[index + 3] *= factor;
  }
}

////////////////////////////////////////////////////////////////////////////////

OperationBandPassFilter::OperationBandPassFilter(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom)
: AOperationFFTFilter(settings)
, dialog(this)
, radius0(10)
, radius1(20)
, use_grey_instead_of_dc(false)
, factor0(0)
, factor1(1)
{
  params.addInt("Low Radius", &radius0, 2, 500, 1);
  params.addInt("High Radius", &radius1, 3, 500, 1);
  params.addBool("Grey instead of DC", &use_grey_instead_of_dc);
  params.addDouble("Stop factor", &factor0, 0.0, 1.0, 0.01);
  params.addDouble("Pass factor", &factor1, 0.0, 4.0, 0.015);
  params.addBool("Affect Alpha", &affect_alpha);
  
  paramsToDialog(dialog.page, params, geom);
}

void OperationBandPassFilter::affectFreqDom(double* re, double* im, size_t u, size_t v)
{
  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  {
    int x2 = x;
    int y2 = y;
    if((size_t)x2 > u / 2) x2 -= u;
    if((size_t)y2 > v / 2) y2 -= v;
    int dist = (x2*x2 + y2*y2);
    bool stop = (dist < radius0 * radius0 || dist > radius1 * radius1);
    if(!use_grey_instead_of_dc && x2 == 0 && y2 == 0) stop = false; //keep DC component
    double factor = stop ? factor0 : factor1;

    size_t index = y * u * 4 + x * 4;
    re[index + 0] *= factor;
    im[index + 0] *= factor;
    re[index + 1] *= factor;
    im[index + 1] *= factor;
    re[index + 2] *= factor;
    im[index + 2] *= factor;
    re[index + 3] *= factor;
    im[index + 3] *= factor;
  }
}

void OperationBandPassFilter::affectSpatialDom(double* d, size_t u, size_t v)
{
  if(!use_grey_instead_of_dc) return;

  for(size_t i = 0; i < u * v; i++)
  for(size_t c = 0; c < 4; c++)
  {
    size_t index = i * 4 + c;
     d[index] = 128 + d[index] / 2;
  }
}

////////////////////////////////////////////////////////////////////////////////

OperationBandStopFilter::OperationBandStopFilter(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom)
: AOperationFFTFilter(settings)
, dialog(this)
, radius0(10)
, radius1(20)
, factor0(0)
, factor1(1)
{
  params.addInt("Low Radius", &radius0, 2, 500, 1);
  params.addInt("High Radius", &radius1, 3, 500, 1);
  params.addDouble("Stop factor", &factor0, 0.0, 1.0, 0.01);
  params.addDouble("Pass factor", &factor1, 0.0, 4.0, 0.015);
  params.addBool("Affect Alpha", &affect_alpha);
  
  paramsToDialog(dialog.page, params, geom);
}

void OperationBandStopFilter::affectFreqDom(double* re, double* im, size_t u, size_t v)
{
  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  {
    int x2 = x;
    int y2 = y;
    if((size_t)x2 > u / 2) x2 -= u;
    if((size_t)y2 > v / 2) y2 -= v;
    int dist = (x2*x2 + y2*y2);
    bool stop = (dist >= radius0 * radius0 && dist < radius1 * radius1);
    double factor = stop ? factor0 : factor1;

    size_t index = y * u * 4 + x * 4;
    re[index + 0] *= factor;
    im[index + 0] *= factor;
    re[index + 1] *= factor;
    im[index + 1] *= factor;
    re[index + 2] *= factor;
    im[index + 2] *= factor;
    re[index + 3] *= factor;
    im[index + 3] *= factor;
  }
}

////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
