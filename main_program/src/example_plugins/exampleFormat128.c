/*
LodePaint

Copyright (c) 2009-2010 Lode Vandevenne
All rights reserved.

This file is part of LodePaint.

LodePaint is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LodePaint is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LodePaint.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <math.h>
#include <stdlib.h>

/*
This is an example of how to implement a 128-bit file format plugin for LodePaint.
The file format itself is just an example, it's the simplest file format possible:
First the letters "example" to identify the file format.
Then 4 bytes width, 4 bytes height of image (big endian)
Then the pixels in order RGBARGBARGBARGBA...., starting at top left, row per row.
So the size of the file is 7 + 8 + 16 * width * height

This is only an example file format! As you can imagine, since this format has no
compression, it results in huge files of 16 bytes per pixel, so it's not so useful
in practice.

gcc -shared exampleFormat128.c -o ../../Plugins/exampleFormat128.so
gcc -shared src/example_plugins/exampleFormat128.c -o Plugins/exampleFormat128.so
*/

#if defined(_WIN32) //windows needs something special
#define DLLEXPORT __declspec(dllexport)
#else
#define DLLEXPORT
#endif

DLLEXPORT int getLodePaintPluginType()
{
  return 0;
}

static void strcopy(char* out, int maxstrlen, const char* in)
{
  int i = 0;
  while(in[i] != 0 && i < maxstrlen - 2)
  {
    out[i] = in[i];
    i++;
  }
  out[i] = 0;
}

DLLEXPORT void fileFormatGetDescription(char* name, int maxstrlen)
{
  strcopy(name, maxstrlen, "Example 128-bit file format");
}

DLLEXPORT int fileFormatGetNumExt()
{
  return 1;
}

DLLEXPORT void fileFormatGetExt(char* ext, int maxstrlen, int index)
{
  (void)index;
  strcopy(ext, maxstrlen, "example128");
}

/*
Each file format for LodePaint must have some identification in the file to ensure that the contents of a file
are of that type. The extension cannot be used to identify the file type. The identification should be so that it's
not possible to confuse this file type with other file types.
The rule for this "example" file format is that it must begin with "example", and, the file size must be exactly correct according to the
width and height of the image.
Real-life examples of such identification headers in a file is e.g. the magic bytes at the start of a PNG or JPEG2000 file.
*/
DLLEXPORT int fileFormatIsOfFormat(unsigned char* buffer, unsigned long size)
{
  if(size < 15) return 0;
  if(buffer[0] != 'e' || buffer[1] != 'x'
  || buffer[2] != 'a' || buffer[3] != 'm'
  || buffer[4] != 'p' || buffer[5] != 'l'
  || buffer[6] != 'e') return 0;
  unsigned long width = buffer[7] * 16777216 + buffer[8] * 65536 + buffer[9] * 256 + buffer[10];
  unsigned long height = buffer[11] * 16777216 + buffer[12] * 65536 + buffer[13] * 256 + buffer[14];
  if(size != 7 + 8 + width * height * 16) return 0;
  return 1;
}

DLLEXPORT int fileFormatDecodeRGBA32F(float** image, int* width, int* height, unsigned char* buffer, unsigned long size, int* parameters)
{
  int x, y, u, v;
  
  /*error checking. For this simple format fileFormatIsOfFormat also already
  ensures the buffer is the correct size so no buffer overflows can happen*/
  if(!fileFormatIsOfFormat(buffer, size)) return 1;

  u = buffer[7] * 16777216 + buffer[8] * 65536 + buffer[9] * 256 + buffer[10];
  v = buffer[11] * 16777216 + buffer[12] * 65536 + buffer[13] * 256 + buffer[14];
  *width = u;
  *height = v;
  
  (*image) = (float*)malloc(u * v * 16);
  for(y = 0; y < v; y++)
  for(x = 0; x < u; x++)
  {
    int index1 = 4 * u * y + 4 * x;
    int index2 = 16 * u * y + 16 * x;
    (*image)[index1 + 0] = *(float*)(&buffer[7 + 8 + index2 + 0]);
    (*image)[index1 + 1] = *(float*)(&buffer[7 + 8 + index2 + 4]);
    (*image)[index1 + 2] = *(float*)(&buffer[7 + 8 + index2 + 8]);
    (*image)[index1 + 3] = *(float*)(&buffer[7 + 8 + index2 + 12]);
  }
  return 0; //ok
}

DLLEXPORT int fileFormatEncodeRGBA32F(unsigned char** buffer, unsigned long* size, float* image, int width, int height, int* parameters)
{
  unsigned char* b;
  int x, y;
  
  *size = 7 + 8 + width * height * 16;
  b = (unsigned char*)malloc(*size);
  (*buffer) = b;
  b[0] = 'e'; b[1] = 'x'; b[2] = 'a'; b[3] = 'm'; b[4] = 'p'; b[5] = 'l'; b[6] = 'e';
  b[7] = (width / 16777216) % 256; b[8] = (width / 65536) % 256; b[9] = (width / 256) % 256; b[10] = (width / 1) % 256; 
  b[11] = (height / 16777216) % 256; b[12] = (height / 65536) % 256; b[13] = (height / 256) % 256; b[14] = (height / 1) % 256;
  for(y = 0; y < height; y++)
  for(x = 0; x < width; x++)
  {
    int index1 = 4 * width * y + 4 * x;
    int index2 = 16 * width * y + 16 * x;
    b[index2 + 15] = ((unsigned char*)(&image[index1 + 0]))[0];
    b[index2 + 16] = ((unsigned char*)(&image[index1 + 0]))[1];
    b[index2 + 17] = ((unsigned char*)(&image[index1 + 0]))[2];
    b[index2 + 18] = ((unsigned char*)(&image[index1 + 0]))[3];
    b[index2 + 19] = ((unsigned char*)(&image[index1 + 1]))[0];
    b[index2 + 20] = ((unsigned char*)(&image[index1 + 1]))[1];
    b[index2 + 21] = ((unsigned char*)(&image[index1 + 1]))[2];
    b[index2 + 22] = ((unsigned char*)(&image[index1 + 1]))[3];
    b[index2 + 23] = ((unsigned char*)(&image[index1 + 2]))[0];
    b[index2 + 24] = ((unsigned char*)(&image[index1 + 2]))[1];
    b[index2 + 25] = ((unsigned char*)(&image[index1 + 2]))[2];
    b[index2 + 26] = ((unsigned char*)(&image[index1 + 2]))[3];
    b[index2 + 27] = ((unsigned char*)(&image[index1 + 3]))[0];
    b[index2 + 28] = ((unsigned char*)(&image[index1 + 3]))[1];
    b[index2 + 29] = ((unsigned char*)(&image[index1 + 3]))[2];
    b[index2 + 30] = ((unsigned char*)(&image[index1 + 3]))[3];
  }
  return 0; //ok
}

DLLEXPORT void pluginFreeUnsignedChar(unsigned char* buffer)
{
  free(buffer);
}

DLLEXPORT void pluginFreeFloat(float* buffer)
{
  free(buffer);
}
