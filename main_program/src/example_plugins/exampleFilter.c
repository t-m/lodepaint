/*
LodePaint

Copyright (c) 2009-2010 Lode Vandevenne
All rights reserved.

This file is part of LodePaint.

LodePaint is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LodePaint is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LodePaint.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <math.h>
#include <stdlib.h>
#include <string.h>


/*
Sample filter.

It creates a simple sine pattern.

gcc -shared -fPIC exampleFilter.c -o ../../Plugins/exampleFilter.so
gcc -shared -fPIC src/example_plugins/exampleFilter.c -o Plugins/exampleFilter.so
*/

#if defined(_WIN32) //windows needs something special
#define DLLEXPORT __declspec(dllexport)
#else
#define DLLEXPORT
#endif

typedef struct CParams
{
  int* ints;
  double* doubles;
  int* bools;
  int* enums;
  double* colors;
  char* strings;

  double* matrices;
  double* curves;
  double* gradients;
} CParams;

const char* xmlparams = "\
<parameters>\n\
  <var type='int' name='Shift X' minl='-1024' maxl='1027' minp='-1024' maxp='1024' step='1' default='0'/>\n\
  <var type='int' name='Shift Y' minl='-1024' maxl='1027' minp='-1024' maxp='1024' step='1' default='0'/>\n\
  <var type='double' name='Period X' minl='1' maxl='50' minp='1' maxp='50' step='1' default='5'/>\n\
  <var type='double' name='Period Y' minl='1' maxl='50' minp='1' maxp='50' step='1' default='5'/>\n\
  <var type='double' name='Opacity' minl='0.0' maxl='1.0' minp='0.0' maxp='1.0' step='0.01' default='0.5'/>\n\
  <var type='bool' name='Affect Alpha' default='false'/>\n\
  <var type='color' name='Color 1' default='0.8 0.5 0.25 1.0'/>\n\
  <var type='color' name='Color 2' default='0.0 0.0 0.0 0.0'/>\n\
  <var type='enum' name='Sine Pattern' default='XY'>\n\
    <enumval name='XY'/>\n\
    <enumval name='X'/>\n\
    <enumval name='Y'/>\n\
    <enumval name='Rings'/>\n\
  </var>\n\
</parameters>\
";

static void strcopy(char* out, int maxstrlen, const char* in)
{
  int i = 0;
  while(in[i] != 0 && i < maxstrlen - 2)
  {
    out[i] = in[i];
    i++;
  }
  out[i] = 0;
}

DLLEXPORT int getLodePaintPluginType()
{
  return 1;
}

DLLEXPORT int filterGetNumFilters()
{
  return 1;
}

DLLEXPORT int filterGetParametersXMLSize(int filter_index)
{
  return strlen(xmlparams) + 1;
}

DLLEXPORT void filterGetParametersXML(char* xml, int filter_index)
{
  strcpy(xml, xmlparams);
}


DLLEXPORT void filterGetName(char* name, int maxstrlen, int filter_index)
{
  strcopy(name, maxstrlen, "Example filter (sine)");
}

DLLEXPORT void filterGetSubMenu(char* name, int maxstrlen, int filter_index)
{
  strcopy(name, maxstrlen, "Programming Examples");
}


//Used by both the 32-bit and the 128-bit function to avoid some code duplication
static void getSineColor(double* color, int u, int v, int x, int y, double periodx, double periody, int pattern, double* color1, double* color2)
{
    double val = 0;
    if(pattern == 0) val = ((sin(x / periodx) + sin(y / periody) + 2.0) / 4.0);
    else if(pattern == 1) val = ((sin(x / periodx) + 1.0) / 2.0);
    else if(pattern == 2) val = ((sin(y / periody) + 1.0) / 2.0);
    else if(pattern == 3)
    {
      double dist = sqrt((x - u/2.0) * (x - u/2.0) + (y - v/2.0) * (y - v/2.0));
      val = ((sin(dist / periodx) + 1.0) / 2.0);
    }
    color[0] = color1[0] * val + color2[0] * (1.0 - val);
    color[1] = color1[1] * val + color2[1] * (1.0 - val);
    color[2] = color1[2] * val + color2[2] * (1.0 - val);
    color[3] = color1[3] * val + color2[3] * (1.0 - val);
}

DLLEXPORT void filterExecuteRGBA8(unsigned char** out, int* ou, int* ov, unsigned char* in, int u, int v, int u2, CParams* params, CParams* mainparams, int filter_index)
{
  //not a resizing filter, set output buffer to input buffer
  *out = in;
  *ou = u;
  *ov = v;

  int shiftx = params->ints[0];
  int shifty = params->ints[1];
  double periodx = params->doubles[0];
  double periody = params->doubles[1];
  double opacity = params->doubles[2];
  int affect_alpha = params->bools[0];
  int pattern = params->enums[0];
  int x, y;
  double* color1 = &params->colors[0];
  double* color2 = &params->colors[4];
  double color[4];

  for(y = 0; y < v; y++)
  for(x = 0; x < u; x++)
  {
    getSineColor(color, u, v, x + shiftx, y + shifty, periodx, periody, pattern, color1, color2);

    int index = 4 * u2 * y + 4 * x;

    in[index + 0] = (unsigned char)(255 * (opacity * color[0] + (1.0 - opacity) * in[index + 0] / 255.0));
    in[index + 1] = (unsigned char)(255 * (opacity * color[1] + (1.0 - opacity) * in[index + 1] / 255.0));
    in[index + 2] = (unsigned char)(255 * (opacity * color[2] + (1.0 - opacity) * in[index + 2] / 255.0));
    if(affect_alpha) in[index + 3] = (unsigned char)(255 * (opacity * color[3] + (1.0 - opacity) * in[index + 3] / 255.0));
  }
}

DLLEXPORT void filterExecuteRGBA32F(float** out, int* ou, int* ov, float* in, int u, int v, int u2, CParams* params, CParams* mainparams, int filter_index)
{
  //not a resizing filter, set output buffer to input buffer
  *out = in;
  *ou = u;
  *ov = v;

  int shiftx = params->ints[0];
  int shifty = params->ints[1];
  double periodx = params->doubles[0];
  double periody = params->doubles[1];
  double opacity = params->doubles[2];
  int affect_alpha = params->bools[0];
  int pattern = params->enums[0];
  int x, y;
  double* color1 = &params->colors[0];
  double* color2 = &params->colors[4];
  double color[4];

  for(y = 0; y < v; y++)
  for(x = 0; x < u; x++)
  {
    getSineColor(color, u, v, x + shiftx, y + shifty, periodx, periody, pattern, color1, color2);

    int index = 4 * u2 * y + 4 * x;

    in[index + 0] = opacity * color[0] + (1.0 - opacity) * in[index + 0];
    in[index + 1] = opacity * color[1] + (1.0 - opacity) * in[index + 1];
    in[index + 2] = opacity * color[2] + (1.0 - opacity) * in[index + 2];
    if(affect_alpha) in[index + 3] = opacity * color[3] + (1.0 - opacity) * in[index + 3];
  }
}

DLLEXPORT void filterExecuteGrey8(unsigned char** out, int* ou, int* ov, unsigned char* in, int u, int v, int u2, CParams* params, CParams* mainparams, int filter_index)
{
  //not a resizing filter, set output buffer to input buffer
  *out = in;
  *ou = u;
  *ov = v;

  int shiftx = params->ints[0];
  int shifty = params->ints[1];
  double periodx = params->doubles[0];
  double periody = params->doubles[1];
  double opacity = params->doubles[2];
  int affect_alpha = params->bools[0];
  int pattern = params->enums[0];
  int x, y;
  double* color1 = &params->colors[0];
  double* color2 = &params->colors[4];
  double color[4];

  for(y = 0; y < v; y++)
  for(x = 0; x < u; x++)
  {
    getSineColor(color, u, v, x + shiftx, y + shifty, periodx, periody, pattern, color1, color2);
    double c = (color[0] + color[1] + color[2]) / 3.0;

    int index = u2 * y + x;

   in[index] = (unsigned char)(255 * (opacity * c + (1.0 - opacity) * in[index] / 255.0));
   }
}

DLLEXPORT void filterExecuteGrey32F(float** out, int* ou, int* ov, float* in, int u, int v, int u2, CParams* params, CParams* mainparams, int filter_index)
{
  //not a resizing filter, set output buffer to input buffer
  *out = in;
  *ou = u;
  *ov = v;

  int shiftx = params->ints[0];
  int shifty = params->ints[1];
  double periodx = params->doubles[0];
  double periody = params->doubles[1];
  double opacity = params->doubles[2];
  int affect_alpha = params->bools[0];
  int pattern = params->enums[0];
  int x, y;
  double* color1 = &params->colors[0];
  double* color2 = &params->colors[4];
  double color[4];

  for(y = 0; y < v; y++)
  for(x = 0; x < u; x++)
  {
    getSineColor(color, u, v, x + shiftx, y + shifty, periodx, periody, pattern, color1, color2);
    double c = (color[0] + color[1] + color[2]) / 3.0;

    int index = u2 * y + x;

    in[index] = opacity * c + (1.0 - opacity) * in[index];
  }
}

DLLEXPORT int filterGetParameterCountInt()
{
  return 2;
}

DLLEXPORT void filterGetParameterNameInt(char* name, int maxstrlen, int* def, int* minimum, int* maximum, int* step, int index)
{
  if(index == 0)
  {
    strcopy(name, maxstrlen, "Shift X");
    *def = 0;
    *minimum = -1024;
    *maximum = 1024;
    *step = 1;
  }
  else if(index == 1)
  {
    strcopy(name, maxstrlen, "Shift Y");
    *def = 0;
    *minimum = -1024;
    *maximum = 1024;
    *step = 1;
  }
}

DLLEXPORT int filterGetParameterCountDouble()
{
  return 3;
}



DLLEXPORT void filterGetParameterNameDouble(char* name, int maxstrlen, double* def, double* minimum, double* maximum, double* step, int index)
{
  if(index == 0)
  {
    strcopy(name, maxstrlen, "Period X");
    *def = 5.0;
    *minimum = 1.0;
    *maximum = 50.0;
    *step = 1.0;
  }
  else if(index == 1)
  {
    strcopy(name, maxstrlen, "Period Y");
    *def = 5.0;
    *minimum = 1.0;
    *maximum = 50.0;
    *step = 1.0;
  }
  else if(index == 2)
  {
    strcopy(name, maxstrlen, "Opacity");
    *def = 0.5;
    *minimum = 0.0;
    *maximum = 1.0;
    *step = 0.01;
  }
}

DLLEXPORT int filterGetParameterCountBool()
{
  return 1;
}



DLLEXPORT void filterGetParameterNameBool(char* name, int maxstrlen, int* def, int index)
{
  if(index == 0)
  {
    strcopy(name, maxstrlen, "Affect Alpha");
    *def = 0;
  }
}

DLLEXPORT int filterGetParameterCountColor()
{
  return 2;
}



DLLEXPORT void filterGetParameterNameColor(char* name, int maxstrlen, double* def, int index)
{
  if(index == 0)
  {
  strcopy(name, maxstrlen, "Color 1");
  def[0] = 0.8; def[1] = 0.5; def[2] = 0.25; def[3] = 1;
  }
  else if(index == 1)
  {
    strcopy(name, maxstrlen, "Color 2");
    def[0] = 0.0; def[1] = 0.0; def[2] = 0.0; def[3] = 0.5;
  }
}

DLLEXPORT int filterGetParameterCountEnum()
{
  return 1;
}



DLLEXPORT void filterGetParameterNameEnum(char* name, int maxstrlen, int* def, int* numchoices, int index)
{
  if(index == 0)
  {
    strcopy(name, maxstrlen, "Sine Pattern");
    *def = 0;
    *numchoices = 4;
  }
}

DLLEXPORT void filterGetParameterNameEnumChoice(char* name, int maxstrlen, int choice_index, int index)
{
  if(index == 0)
  {
    switch(choice_index)
    {
      case 0: strcopy(name, maxstrlen, "XY"); break;
      case 1: strcopy(name, maxstrlen, "X"); break;
      case 2: strcopy(name, maxstrlen, "Y"); break;
      case 3: strcopy(name, maxstrlen, "Rings"); break;
      default: strcopy(name, maxstrlen, "Invalid Choice"); break;
    }
  }
}

DLLEXPORT void pluginFreeUnsignedChar(unsigned char* buffer)
{
  free(buffer);
}

DLLEXPORT void pluginFreeFloat(float* buffer)
{
  free(buffer);
}
