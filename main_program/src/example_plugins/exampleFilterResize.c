/*
LodePaint

Copyright (c) 2009-2010 Lode Vandevenne
All rights reserved.

This file is part of LodePaint.

LodePaint is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LodePaint is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LodePaint.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <math.h>
#include <stdlib.h>
#include <string.h>

/*
Sample resizing filter. Unlike a non-resizing filter, this one needs to create a new image
buffer with the new size.

The filter resizes the image and adds some randomness to it by randomly choosing some pixel
coordinates. Not really useful but that's because it's just a simple demonstration of how
to make such a plugin for LodePaint.

gcc -shared exampleFilterResize.c -o ../../Plugins/exampleFilterResize.so
gcc -shared src/example_plugins/exampleFilterResize.c -o Plugins/exampleFilterResize.so
*/

#if defined(_WIN32) //windows needs something special
#define DLLEXPORT __declspec(dllexport)
#else
#define DLLEXPORT
#endif

typedef struct CParams
{
  int* ints;
  double* doubles;
  int* bools;
  int* enums;
  double* colors;
  char* strings;

  double* matrices;
  double* curves;
  double* gradients;
} CParams;

const char* xmlparams = "\
<parameters>\n\
  <var type='absrelsize' name='Size' defaultw='256' defaulth='256' defaultrelw='200' defaultrelh='200' defaultusepercent='true'/>\n\
</parameters>\
";

DLLEXPORT int getLodePaintPluginType()
{
  return 1;
}

static void strcopy(char* out, int maxstrlen, const char* in)
{
  int i = 0;
  while(in[i] != 0 && i < maxstrlen - 2)
  {
    out[i] = in[i];
    i++;
  }
  out[i] = 0;
}

static int getRandom(int first, int last)
{
  int r = rand();
  if(r < 0) r = -r;
  r %= (last - first + 1);
  return first + r;
}

DLLEXPORT int filterGetNumFilters()
{
  return 1;
}

DLLEXPORT int filterGetParametersXMLSize(int filter_index)
{
  return strlen(xmlparams) + 1;
}

DLLEXPORT void filterGetParametersXML(char* xml, int filter_index)
{
  strcpy(xml, xmlparams);
}

DLLEXPORT void filterGetName(char* name, int maxstrlen, int filter_index)
{
  strcopy(name, maxstrlen, "Example filter (random resize)");
}

DLLEXPORT void filterGetSubMenu(char* name, int maxstrlen, int filter_index)
{
  strcopy(name, maxstrlen, "Programming Examples");
}

DLLEXPORT void filterExecuteRGBA8(unsigned char** out, int* ou, int* ov, unsigned char* in, int u, int v, int u2, CParams* params, CParams* mainparams, int filter_index)
{
  int x, y;

  if(params->bools[0])
  {
    *ou = (int)((u * params->doubles[0]) / 100);
    *ov = (int)((v * params->doubles[1]) / 100);
  }
  else
  {
    *ou = params->ints[0];
    *ov = params->ints[1];
  }
  
  (*out) = malloc(*ou * *ov * 4);
  
  for(y = 0; y < *ov; y++)
  for(x = 0; x < *ou; x++)
  {
    int index = 4 * *ou * y + 4 * x;
    
    int r = getRandom(0, 3);
    
    int ix0 = ((x * u) / *ou);
    int iy0 = ((y * v) / *ov);
    int ix1 = ix0 + 1;
    if(ix1 >= u) ix1 = u - 1;
    int iy1 = iy0 + 1;
    if(iy1 >= v) iy1 = v - 1;
    
    int ix = r % 2 ? ix0 : ix1;
    int iy = (r >> 1) % 2 ? iy0 : iy1;
    
    int index2 = 4 * u2 * iy + 4 * ix;
    
    (*out)[index + 0] = in[index2 + 0];
    (*out)[index + 1] = in[index2 + 1];
    (*out)[index + 2] = in[index2 + 2];
    (*out)[index + 3] = in[index2 + 3];
  }
}


DLLEXPORT void pluginFreeUnsignedChar(unsigned char* buffer)
{
  free(buffer);
}

DLLEXPORT void pluginFreeFloat(float* buffer)
{
  free(buffer);
}
