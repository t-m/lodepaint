These are example plugins for LodePaint. They don't do anything really useful
and are meant as an example how to create a plugin.

The examples are in C instead of C++ because the communication with the shared
libraries of the plugin happen through a C interface, but it should be perfectly
possible to make C++ plugins, as long as you have the functions with the exac
 signature in your library and use the syntax 'extern "C"'.

Here's how you can compile them:

Linux: use gcc with the "-shared" option. Output to a ".so" file and put that
  file in the Plugins folder of LodePaint.
Windows: with Dev-C++, for each .c file create a C "DLL" project. Dev-C++
  creates two sample files in that project but remove those, the .c file with
  the "DLLEXPORT" ifdef in it just works. When compiling it creates a .a, a .def
  and a .dll file, but only the .dll is needed, put that it the Plugins folder
  of LodePaint.

This are just example plugins, but LodePaint comes with more plugins than just
these. The other plugins are in the directory ../../../plugins (that is, the
plugins directory under trunk in the SVN code of LodePaint).
