/*
LodePaint

Copyright (c) 2009-2010 Lode Vandevenne
All rights reserved.

This file is part of LodePaint.

LodePaint is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LodePaint is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LodePaint.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "paint_filter.h"

class OperationOpaque : public AOperationFilterUtil
{
  private:
    virtual void doit(TextureRGBA8& texture, Progress& progress);
    virtual void doit(TextureRGBA32F& texture, Progress& progress);
    virtual bool supportsColorMode(ColorMode mode) const { return sup(mode, true, true, false, false); }

  public:
    OperationOpaque(GlobalToolSettings& settings) : AOperationFilterUtil(settings, true, false) {}
    virtual std::string getLabel() const { return "Opaque"; }

};

class OperationSetAlphaTo : public AOperationFilterUtil
{
  private:
    virtual void doit(TextureRGBA8& texture, Progress& progress);
    virtual void doit(TextureRGBA32F& texture, Progress& progress);
    virtual bool supportsColorMode(ColorMode mode) const { return sup(mode, true, true, false, false); }
    
    double value;
    DynamicFilterPageWithPreview dialog;
  public:
    OperationSetAlphaTo(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom);
    
    virtual lpi::gui::Element* getDialog() { return &dialog; }
    virtual std::string getLabel() const { return "Set Alpha To"; }
    virtual std::string getHelp() { return "[OperationSetAlphaTo Help]"; }
};

class OperationTranslucent : public AOperationFilterUtil
{
  private:
    virtual void doit(TextureRGBA8& texture, Progress& progress);
    virtual void doit(TextureRGBA32F& texture, Progress& progress);
    virtual bool supportsColorMode(ColorMode mode) const { return sup(mode, true, true, false, false); }

  public:
    OperationTranslucent(GlobalToolSettings& settings) : AOperationFilterUtil(settings, true, false) {}
    virtual std::string getLabel() const { return "Translucent"; }
};

class OperationAlphaModulated : public AOperationFilterUtil
{
  private:
    virtual void doit(TextureRGBA8& texture, Progress& progress);
    virtual void doit(TextureRGBA32F& texture, Progress& progress);
    virtual bool supportsColorMode(ColorMode mode) const { return sup(mode, true, true, false, false); }

  public:
    OperationAlphaModulated(GlobalToolSettings& settings) : AOperationFilterUtil(settings, true, false) {}
    virtual std::string getLabel() const { return "Modulated"; }
};

class OperationAlphaSaturation : public AOperationFilterUtil
{
  private:
    virtual void doit(TextureRGBA8& texture, Progress& progress);
    virtual void doit(TextureRGBA32F& texture, Progress& progress);
    virtual bool supportsColorMode(ColorMode mode) const { return sup(mode, true, true, false, false); }

  public:
    OperationAlphaSaturation(GlobalToolSettings& settings) : AOperationFilterUtil(settings, true, false) {}
    virtual std::string getLabel() const { return "Saturation"; }
};

class OperationAlphaColorKey : public AOperationFilterUtilWithSettings
{
  private:
    virtual void doit(TextureRGBA8& texture, const GlobalToolSettings& settings, Progress& progress);
    virtual void doit(TextureRGBA32F& texture, const GlobalToolSettings& settings, Progress& progress);
    virtual bool supportsColorMode(ColorMode mode) const { return sup(mode, true, true, false, false); }
  public:
    OperationAlphaColorKey(GlobalToolSettings& settings) : AOperationFilterUtilWithSettings(settings, true, false) {}
    virtual std::string getLabel() const { return "Color Key (BG)"; }

};

class OperationApplyAlpha : public AOperationFilterUtilWithSettings //applies alpha channel to fg or bg color
{
  private:
    virtual void doit(TextureRGBA8& texture, const GlobalToolSettings& settings, Progress& progress);
    bool bg; //if true, uses bg instead of fg color
  public:
    OperationApplyAlpha(GlobalToolSettings& settings, bool bg) : AOperationFilterUtilWithSettings(settings, true, false), bg(bg) {}
    virtual std::string getLabel() const { return bg ? "Apply Alpha To BG" : "Apply Alpha To FG"; }
};

class OperationAlphaSoftKey : public AOperationFilterUtilWithSettings //converts color to alpha, the smaller the distance to the color, the smaller alpha
{
  private:
    virtual void doit(TextureRGBA8& texture, const GlobalToolSettings& settings, Progress& progress);
    virtual void doit(TextureRGBA32F& texture, const GlobalToolSettings& settings, Progress& progress);
    virtual bool supportsColorMode(ColorMode mode) const { return sup(mode, true, true, false, false); }
  public:
    OperationAlphaSoftKey(GlobalToolSettings& settings) : AOperationFilterUtilWithSettings(settings, true, false) {}
    virtual std::string getLabel() const { return "Soft Key (BG)"; }
};


class OperationOnlyAlpha : public AOperationFilterUtilWithSettings //keeps alpha channel, turns RGB in the FG/BG color
{
  private:
    virtual void doit(TextureRGBA8& texture, const GlobalToolSettings& settings, Progress& progress);
    virtual void doit(TextureRGBA32F& texture, const GlobalToolSettings& settings, Progress& progress);
    virtual bool supportsColorMode(ColorMode mode) const { return sup(mode, true, true, false, false); }
  public:
    OperationOnlyAlpha(GlobalToolSettings& settings) : AOperationFilterUtilWithSettings(settings, true, false) {}
    virtual std::string getLabel() const { return "Keep Only Alpha"; }
};

class OperationAlphaToGray : public AOperationFilterUtil
{
  private:
    virtual void doit(TextureRGBA8& texture, Progress& progress);
    virtual void doit(TextureRGBA32F& texture, Progress& progress);
    virtual bool supportsColorMode(ColorMode mode) const { return sup(mode, true, true, false, false); }
  public:
    OperationAlphaToGray(GlobalToolSettings& settings) : AOperationFilterUtil(settings, true, true) {}
    virtual std::string getLabel() const { return "Alpha To Grey"; }
};

class OperationAlphaExtractCloud : public AOperationFilterUtil
{
  private:
    virtual void doit(TextureRGBA8& texture, Progress& progress);
    virtual void doit(TextureRGBA32F& texture, Progress& progress);
    virtual bool supportsColorMode(ColorMode mode) const { return sup(mode, true, true, false, false); }
  public:
    OperationAlphaExtractCloud(GlobalToolSettings& settings)
    : AOperationFilterUtil(settings, true, true)
    {
    }
    virtual std::string getLabel() const { return "Extract Clouds"; }
};

class OperationThresholdAlpha : public AOperationFilterUtil
{
  private:
    virtual void doit(TextureRGBA8& texture, Progress& progress);
    virtual void doit(TextureRGBA32F& texture, Progress& progress);
    virtual bool supportsColorMode(ColorMode mode) const { return sup(mode, true, true, false, false); }
    
    double threshold;
    DynamicFilterPageWithPreview dialog;
  public:
    OperationThresholdAlpha(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom);
    
    virtual lpi::gui::Element* getDialog() { return &dialog; }
    virtual std::string getLabel() const { return "Threshold Alpha"; }
    virtual std::string getHelp() { return "[OperationThresholdAlpha Help]"; }
};

class OperationDitherMonochromeAlpha : public AOperationFilterUtil
{
  private:
    virtual void doit(TextureRGBA8& texture, Progress& progress);
    
  public:
    OperationDitherMonochromeAlpha(GlobalToolSettings& settings)
    : AOperationFilterUtil(settings, true, true)
    {
    }
    
    virtual std::string getLabel() const { return "Alpha Monochrome (Dithered)"; }
};

class OperationIdentifyAlpha : public AOperationFilterUtil
{
  private:
    virtual void doit(TextureRGBA8& texture, Progress& progress);
    
  public:
    OperationIdentifyAlpha(GlobalToolSettings& settings)
    : AOperationFilterUtil(settings, true, true)
    {
    }
    
    virtual std::string getLabel() const { return "Identify Alpha"; }
};

class OperationPremultiplyAlpha : public AOperationFilterUtil
{
  private:
    virtual void doit(TextureRGBA8& texture, Progress& progress);
    
  public:
    OperationPremultiplyAlpha(GlobalToolSettings& settings)
    : AOperationFilterUtil(settings, true, true)
    {
    }
    
    virtual std::string getLabel() const { return "Premultiply Alpha"; }
};
