/*
LodePaint

Copyright (c) 2009-2010 Lode Vandevenne
All rights reserved.

This file is part of LodePaint.

LodePaint is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LodePaint is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LodePaint.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "lpi/gui/lpi_gui.h"
#include "lpi/gui/lpi_gui_dynamic.h"
#include "lpi/gl/lpi_texture_gl.h"
#include "paint_param.h"
#include "paint_window.h"

class AOperationFilter;


//The dialog in which elements returned by getDialog of filter will be embedded
//Do not return THIS class or a child of it in getDialog, instead return the element that will be plugged inside of this dialog.
class FilterDialog : public lpi::gui::Dialog
{
  protected:
    lpi::gui::Element* element; //that what was returned from the IOperationFilter
    lpi::gui::Button ok;
    lpi::gui::Button cancel;

  public:
    lpi::gui::Button help;
    std::string helpText;

    FilterDialog(lpi::gui::Element* element, const lpi::gui::IGUIDrawer& geom, const std::string& title);

    virtual void drawImpl(lpi::gui::IGUIDrawer& drawer) const;
    virtual void handleImpl(const lpi::IInput& input);
    
    void addHelp(const std::string& text);
};

//for convenience, useful for filter's getDialog method
class DynamicPageWithAutoUpdate : public lpi::gui::DynamicPage
{
  virtual void handleImpl(const lpi::IInput& input);
};

class AOperationFilter;

class FilterDialogWithPreview : public lpi::gui::ElementComposite
{
  protected:
    AOperationFilter* filter;
    
    ColorMode mode;
    
    TextureRGBA8 textureSourceRGBA8;
    TextureRGBA32F textureSourceRGBA32F;
    TextureGrey8 textureSourceGrey8;
    TextureGrey32F textureSourceGrey32F;
    
    TextureRGBA8 texturePreviewRGBA8;
    TextureRGBA32F texturePreviewRGBA32F;
    TextureGrey8 texturePreviewGrey8;
    TextureGrey32F texturePreviewGrey32F;
    
    lpi::gui::Button update;
    lpi::gui::Checkbox autoupdate;
    bool ratherlameautoupdateboolean; //lame because a good system would be that every gui element has a "hasChanged" function and the filter preview us updated whenever that is true on some element.
    static const int MAXPREVIEWU = 260;
    static const int MAXPREVIEWV = 180;
    int previewu;
    int previewv;
    
    bool disablepreview;

  protected:
    void updatePreview();

  public:
    FilterDialogWithPreview(AOperationFilter* filter);
    void generatePreviewSource(TextureRGBA8* texture);
    void generatePreviewSource(TextureRGBA32F* texture);
    void generatePreviewSource(TextureGrey8* texture);
    void generatePreviewSource(TextureGrey32F* texture);
    virtual void drawImpl(lpi::gui::IGUIDrawer& drawer) const;
    virtual void handleImpl(const lpi::IInput& input);
};

class DynamicFilterPageWithPreview : public FilterDialogWithPreview
{
  public:
    DynamicPageWithAutoUpdate page;

    DynamicFilterPageWithPreview(AOperationFilter* filter);
    virtual void drawImpl(lpi::gui::IGUIDrawer& drawer) const;
    virtual void handleImpl(const lpi::IInput& input);
};

//It's in fact a Matrix
class Numbers : public lpi::gui::ElementComposite
{
  private:
    mutable int activeNumber;
    
    int getActiveNumber() const;
    
    std::vector<std::vector<double> > presets;

    mutable bool changed;
    
  public:
    lpi::gui::DropDownList presetlist;
    std::vector<lpi::gui::InputLine*> numbers;
  public:
    Numbers(const lpi::gui::IGUIDrawer& geom, int numx, int numy);
    ~Numbers();
    virtual void drawImpl(lpi::gui::IGUIDrawer& drawer) const;
    virtual void handleImpl(const lpi::IInput& input);

    void controlToValue(double* values) const;
    void valueToControl(const double* values);
    
    virtual int getKeyboardFocus() const;

    void addPreset(const std::string& name, const std::vector<double>& values);

    bool hasChanged() const { bool result = changed; changed = false; return result; }
};

class CurvesDialog : public lpi::gui::ElementComposite
{
  private:
    lpi::gui::Dummy curve;
    lpi::gui::BulletList bullets;
    lpi::gui::Button smooth;
    lpi::gui::Button reset;
    
    void resetCurve(unsigned char* curve);
    void smoothCurveOnce(unsigned char* curve);
    void smoothCurve(unsigned char* curve);
    void drawCurve(const unsigned char* curve, lpi::gui::IGUIDrawer& drawer, int x0, int y0, int x1, int y1, const lpi::ColorRGB& color) const;
    void handleCurve(unsigned char* curve, int x0, int y0, int x1, int y1, int mx0, int my0, int mx1, int my1);
    
    bool greyscale;
    bool alpha;
    
    int prevx;
    int prevy;
    
  public:
    CurvesDialog(const lpi::gui::IGUIDrawer& geom);
    virtual void drawImpl(lpi::gui::IGUIDrawer& drawer) const;
    virtual void handleImpl(const lpi::IInput& input);
    
    unsigned char r[256];
    unsigned char g[256];
    unsigned char b[256];
    unsigned char a[256];
    unsigned char l[256]; //for greyscale ("lightness")
    
    void setMode(bool greyscale, bool alpha) { this->greyscale = greyscale; this->alpha = alpha; }

};

class DynamicControlResize : public lpi::gui::IDynamicControl
{
  private:
  
    lpi::gui::InputLine inputw;
    lpi::gui::InputLine inputh;
    lpi::gui::Checkbox inputusepercent;
    bool previnputusepercent;
    lpi::gui::Dummy linksymbol;
    bool link;
    

    const int* origwidth;
    const int* origheight;
    int* width;
    int* height;
    double* relwidth;
    double* relheight;
    bool* usepercent;
    
  
  public:
  
    DynamicControlResize(const int* origwidth, const int* origheight, int* width, int* height, double* relwidth, double* relheight, bool* usepercent, const lpi::gui::IGUIDrawer& geom);
  
    virtual void controlToValue();
    virtual void valueToControl();
    
    virtual void handleImpl(const lpi::IInput& input);
    
    virtual void drawImpl(lpi::gui::IGUIDrawer& drawer) const;
    
    void setControlsAbsolute(int w, int h);
    void setControlsRelative(double w, double h);
};

void paramsToDialog(DynamicPageWithAutoUpdate& page, const DynamicParameters& params, const lpi::gui::IGUIDrawer& geom);
