/*
LodePaint

Copyright (c) 2009-2010 Lode Vandevenne
All rights reserved.

This file is part of LodePaint.

LodePaint is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LodePaint is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LodePaint.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <vector>
#include <string>

#include "lpi/lpi_texture.h"
#include "paint_texture.h"
#include "paint_window.h"

class IOperation;
class Paint;
class GlobalToolSettings;
class TextureRGBA32F;

struct UndoStateIndex //useful when iterating a UndoState to remember the indices of each data type
{
  UndoStateIndex();
  void reset();
  
  size_t idata;
  size_t ifdata;
  size_t ibool;
  size_t iint;
  size_t idouble;
  size_t istring;
};

/*
Keeps your data. The intention is to store everything in it in a certain order,
then read it all again in the same order.
*/
class UndoState
{
  private:
  
  std::vector<std::vector<unsigned char>*> data;
  std::vector<std::vector<float>*> fdata;
  std::vector<bool> bools;
  std::vector<int> ints;
  std::vector<double> doubles;
  std::vector<std::string> strings;
  
  public:
    UndoState();
    ~UndoState();
  
    //TODO: cache this on disk once certain total size is reached
    void addData(const std::vector<unsigned char>& data);
    void addData(const unsigned char* data, size_t size);
    std::vector<unsigned char>& getData(size_t index);
    const std::vector<unsigned char>& getData(size_t index) const;
    size_t getNumData() const;

    void addFData(const std::vector<float>& data);
    void addFData(const float* data, size_t size);
    std::vector<float>& getFData(size_t index);
    const std::vector<float>& getFData(size_t index) const;
    size_t getNumFData() const;

    //The following primitives are always kept in memory and it is assumed no operation adds very many of them.
    
    void addBool(bool b);
    bool getBool(size_t index) const;
    size_t getNumBool() const;

    void addInt(int i);
    int getInt(size_t index) const;
    size_t getNumInt() const;

    void addDouble(double d);
    double getDouble(size_t index) const;
    size_t getNumDouble() const;
    
    void addString(const std::string& s);
    std::string getString(size_t index) const;
    size_t getNumString() const;
    
    void clear();
    
    IOperation* operation; //the operation that did it and thus can use this UndoState to undo or redo the state
    
    void swap(UndoState& other);
    
    size_t calculateMemorySize() const; //in bytes
};

void storeCompleteTextureInUndo(UndoState& state, const TextureRGBA8& texture);
void readCompleteTextureFromUndo(TextureRGBA8& texture, const UndoState& state, UndoStateIndex& index); //also resizes texture if needed
void storePartialTextureInUndo(UndoState& state, const TextureRGBA8& texture, int x0, int y0, int x1, int y1);
void readPartialTextureFromUndo(TextureRGBA8& texture, const UndoState& state, UndoStateIndex& index); //does NOT support resizing the texture, only reading the saved zone
void readPartialTextureFromUndo(TextureRGBA8& texture, UndoState& redo, const UndoState& state, UndoStateIndex& index); //this one stores the new texture data in "redo"

void storeCompleteTextureInUndo(UndoState& state, const TextureRGBA32F& texture);
void readCompleteTextureFromUndo(TextureRGBA32F& texture, const UndoState& state, UndoStateIndex& index); //also resizes texture if needed
void storePartialTextureInUndo(UndoState& state, const TextureRGBA32F& texture, int x0, int y0, int x1, int y1);
void readPartialTextureFromUndo(TextureRGBA32F& texture, const UndoState& state, UndoStateIndex& index); //does NOT support resizing the texture, only reading the saved zone
void readPartialTextureFromUndo(TextureRGBA32F& texture, UndoState& redo, const UndoState& state, UndoStateIndex& index); //this one stores the new texture data in "redo"

void storeCompleteTextureInUndo(UndoState& state, const TextureGrey8& texture);
void readCompleteTextureFromUndo(TextureGrey8& texture, const UndoState& state, UndoStateIndex& index); //also resizes texture if needed
void storePartialTextureInUndo(UndoState& state, const TextureGrey8& texture, int x0, int y0, int x1, int y1);
void readPartialTextureFromUndo(TextureGrey8& texture, const UndoState& state, UndoStateIndex& index); //does NOT support resizing the texture, only reading the saved zone
void readPartialTextureFromUndo(TextureGrey8& texture, UndoState& redo, const UndoState& state, UndoStateIndex& index); //this one stores the new texture data in "redo"

void storeCompleteTextureInUndo(UndoState& state, const TextureGrey32F& texture);
void readCompleteTextureFromUndo(TextureGrey32F& texture, const UndoState& state, UndoStateIndex& index); //also resizes texture if needed
void storePartialTextureInUndo(UndoState& state, const TextureGrey32F& texture, int x0, int y0, int x1, int y1);
void readPartialTextureFromUndo(TextureGrey32F& texture, const UndoState& state, UndoStateIndex& index); //does NOT support resizing the texture, only reading the saved zone
void readPartialTextureFromUndo(TextureGrey32F& texture, UndoState& redo, const UndoState& state, UndoStateIndex& index); //this one stores the new texture data in "redo"

void storeColorInUndo(UndoState& state, const lpi::ColorRGB& color);
lpi::ColorRGB readColorFromUndo(const UndoState& state, UndoStateIndex& index);
void storeSettingsInUndo(UndoState& state, const GlobalToolSettings& settings); //the general painting program working-settings containing brush shape, FG and BG color, line thickness, etc...
void readSettingsFromUndo(GlobalToolSettings& settings, const UndoState& state, UndoStateIndex& index);

class UndoStack
{
  private:
  
    std::vector<UndoState*> states;
    size_t index;
  
  public:
  
    UndoStack();
    ~UndoStack();
    
    size_t size();
    size_t getIndex(); //if getIndex() is size(), then there are no "redo" operations. if getIndex() is 0, there are no "undo "operations left.
    
    UndoState* addNewState(IOperation* operation); //"do"
    void popState(); //use this when an operation failed and the added state should be discarded without using it in any way
    
    void undo(Paint& paint);
    void redo(Paint& paint);
    
    size_t calculateMemorySize() const; //in bytes
    void trimMemorySize(size_t size); //in bytes
};

/*
ToolUndoHelper is there for tools that update parts of the image in multiple
steps, to easily remember the zone in which updates happened, but not more.
Especially useful for a brush, where while the mouse moves, more and more needs
to be remembered.
*/
struct ToolUndoHelper //helps to remember the affected zone
{
  ColorMode mode;
  
  /*
    e.g. (starting from 0, adding a side somewhere each time)
    
    CCCCCCCDEF
    9866666DEF
    9850134DEF
    9852234DEF
    9877777DEF
    AAAAAAADEF
    BBBBBBBDEF
  */
  int x0;
  int y0;
  int x1;
  int y1;
  int startx; //there where the whole thing starts (the initial pixel around which all the sides will be built)
  int starty;
  
  std::vector<int> sides; //0 = up, 1 = right, 2 = down, 3 = left
  std::vector<std::vector<unsigned char> > data;
  std::vector<std::vector<float> > fdata;

  ToolUndoHelper();
  void reset();
  
  //void addPoint(int x0, int y0);
  //void addBounds(int x0, int y0, int x1, int y1);
  
  void add(int ax0, int ay0, int ax1, int ay1, const TextureRGBA8& texture);
  void addSide(int side, const TextureRGBA8& texture);  //0 = up, 1 = right, 2 = down, 3 = left
  void add(int ax0, int ay0, int ax1, int ay1, const TextureGrey8& texture);
  void addSide(int side, const TextureGrey8& texture);  //0 = up, 1 = right, 2 = down, 3 = left
  void add(int ax0, int ay0, int ax1, int ay1, const TextureRGBA32F& texture);
  void addSide(int side, const TextureRGBA32F& texture);  //0 = up, 1 = right, 2 = down, 3 = left
  void add(int ax0, int ay0, int ax1, int ay1, const TextureGrey32F& texture);
  void addSide(int side, const TextureGrey32F& texture);  //0 = up, 1 = right, 2 = down, 3 = left

  void addToUndo(UndoState& state); //there is no read from undo. You can use readPartialTextureFromUndo for that.
};
