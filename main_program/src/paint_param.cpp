/*
LodePaint

Copyright (c) 2009 Lode Vandevenne
All rights reserved.

This file is part of LodePaint.

LodePaint is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LodePaint is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LodePaint.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "paint_param.h"

#include "lpi/lpi_parse.h"
#include "lpi/lpi_xml.h"

#include <iostream>

void initCParams(CParams& params)
{
  params.ints = 0;
  params.doubles = 0;
  params.bools = 0;
  params.enums = 0;
  params.colors = 0;
  params.strings = 0;

  params.matrices = 0;
  params.curves = 0;
  params.gradients = 0;
}

void cleanupCParams(CParams& params)
{
  if(params.ints) delete[] params.ints;
  if(params.doubles) delete[] params.doubles;
  if(params.bools) delete[] params.bools;
  if(params.enums) delete[] params.enums;
  if(params.colors) delete[] params.colors;
  if(params.strings) delete[] params.strings;

  if(params.matrices) delete[] params.matrices;
  if(params.curves) delete[] params.curves;
  if(params.gradients) delete[] params.gradients;
}

////////////////////////////////////////////////////////////////////////////////

ParameterType ParamResize::getType() const
{
  return PG_ABS_REL_SIZE;
}

void ParamResize::cleanup()
{
  delete origwidth;
  delete origheight;
  delete width;
  delete height;
  delete relwidth;
  delete relheight;
  delete percentage;
}

////////////////////////////////////////////////////////////////////////////////

DynamicParameters::DynamicParameters(bool cleanMemory)
: cleanMemory(cleanMemory)
{
}

DynamicParameters::~DynamicParameters()
{
  for(size_t i = 0; i < params.size(); i++)
  {
    if(cleanMemory) params[i]->cleanup();
    delete params[i];
  }
}

void DynamicParameters::addInt(const std::string& name, int* bind, int minl, int maxl, int minp, int maxp, int step)
{
  ParamInt* p = new ParamInt();
  p->name = name;
  p->bind = bind;
  p->def = *bind;
  p->minl = minl;
  p->maxl = maxl;
  p->minp = minp;
  p->maxp = maxp;
  p->step = step;
  params.push_back(p);
}

void DynamicParameters::addInt(const std::string& name, int* bind, int min, int max, int step)
{
  addInt(name, bind, min, max, min, max, step);
}

void DynamicParameters::addDouble(const std::string& name, double* bind, double minl, double maxl, double minp, double maxp, double step)
{
  ParamDouble* p = new ParamDouble();
  p->name = name;
  p->bind = bind;
  p->def = *bind;
  p->minl = minl;
  p->maxl = maxl;
  p->minp = minp;
  p->maxp = maxp;
  p->step = step;
  params.push_back(p);
}

void DynamicParameters::addDouble(const std::string& name, double* bind, double min, double max, double step)
{
  addDouble(name, bind, min, max, min, max, step);
}

void DynamicParameters::addHue(const std::string& name, double* bind)
{
  ParamHue* p = new ParamHue();
  p->name = name;
  p->bind = bind;
  params.push_back(p);
}

void DynamicParameters::addBool(const std::string& name, bool* bind)
{
  ParamBool* p = new ParamBool();
  p->name = name;
  p->bind = bind;
  p->def = *bind;
  params.push_back(p);
}

void DynamicParameters::addNamedBool(const std::string& name, bool* bind, const std::string& name0, const std::string& name1)
{
  ParamNamedBool* p = new ParamNamedBool();
  p->name = name;
  p->name0 = name0;
  p->name1 = name1;
  p->bind = bind;
  p->def = *bind;
  params.push_back(p);
}

void DynamicParameters::addMultiBool(const std::string& name, const std::vector<bool*>& bind)
{
  ParamMultiBool* p = new ParamMultiBool();
  p->name = name;
  p->bind = bind;
  for(size_t i = 0; i < bind.size(); i++) p->def.push_back(*bind[i]);
  params.push_back(p);
}

void DynamicParameters::addString(const std::string& name, std::string* bind)
{
  ParamString* p = new ParamString();
  p->name = name;
  p->bind = bind;
  p->def = *bind;
  params.push_back(p);
}

void DynamicParameters::addColor(const std::string& name, lpi::ColorRGBd* bind)
{
  ParamColor* p = new ParamColor();
  p->name = name;
  p->bind = bind;
  p->def = *bind;
  params.push_back(p);
}

void DynamicParameters::addResize(const std::string& name, int* origw, int* origh, int* w, int* h, double* relw, double* relh, bool* percentage)
{
  ParamResize* p = new ParamResize();
  p->name = name;
  p->origwidth = origw;
  p->origheight = origh;
  p->width = w;
  p->height = h;
  p->relwidth = relw;
  p->relheight = relh;
  p->percentage = percentage;
  params.push_back(p);
}

void DynamicParameters::addLabel(const std::string& name)
{
  ParamLabel* p = new ParamLabel();
  p->name = name;
  params.push_back(p);
}

void DynamicParameters::addToolTipToLastRow(const std::string& name)
{
  ParamToolTip* p = new ParamToolTip();
  p->name = name;
  params.push_back(p);
}

size_t DynamicParameters::size() const
{
  return params.size();
}

const Param* DynamicParameters::get(size_t index) const
{
  return params[index];
}

Param* DynamicParameters::get(size_t index)
{
  return params[index];
}

//returns empty string if it isn't there
std::string getAttr(const lpi::xml::XMLTree& tree, const std::string& attr_name)
{
  for(size_t i = 0; i < tree.attributes.size(); i++)
  {
    if(tree.attributes[i].name == attr_name) return tree.attributes[i].value;
  }
  
  return "";
}

bool xmlToDynamicParameters(DynamicParameters& p, const std::string& xml)
{
  using namespace lpi;
  using namespace xml;
  
  XMLTree tree;
  int error = tree.parse(xml);
  
  (void)error;
  
  for(size_t i = 0; i < tree.children.size(); i++)
  {
    XMLTree& child = *tree.children[i];
    
    if(child.content.name == "var")
    {
      std::string type = getAttr(child, "type");
      std::string name = getAttr(child, "name");

      if(type == "int")
      {
        int minl, maxl, minp, maxp, step;
        int* v = new int(0);

        unconvert(minl, getAttr(child, "minl"));
        unconvert(maxl, getAttr(child, "maxl"));
        unconvert(minp, getAttr(child, "minp"));
        unconvert(maxp, getAttr(child, "maxp"));
        unconvert(step, getAttr(child, "step"));
        unconvert(*v, getAttr(child, "default"));

        p.addInt(name, v, minl, maxl, minp, maxp, step);
      }
      else if(type == "double")
      {
        double minl, maxl, minp, maxp, step;
        double* v = new double(0);

        unconvert(minl, getAttr(child, "minl"));
        unconvert(maxl, getAttr(child, "maxl"));
        unconvert(minp, getAttr(child, "minp"));
        unconvert(maxp, getAttr(child, "maxp"));
        unconvert(step, getAttr(child, "step"));
        unconvert(*v, getAttr(child, "default"));

        p.addDouble(name, v, minl, maxl, minp, maxp, step);
      }
      else if(type == "bool")
      {
        bool* v = new bool(false);

        unconvert(*v, getAttr(child, "default"));

        p.addBool(name, v);
      }
      else if(type == "color")
      {
        lpi::ColorRGBd* v = new lpi::ColorRGBd(0,0,0,0);
        
        std::string def = getAttr(child, "default");
        std::vector<std::string> c;
        lpi::splitString(c, def, ' ');

        if(c.size() == 4)
        {
          v->r = lpi::strtoval<double>(c[0]);
          v->g = lpi::strtoval<double>(c[1]);
          v->b = lpi::strtoval<double>(c[2]);
          v->a = lpi::strtoval<double>(c[3]);
        }

        p.addColor(name, v);
      }
      else if(type == "enum")
      {
        int* v = new int(0);
        
        std::string def = getAttr(child, "default");

        std::vector<std::string> names;
        std::vector<int> values;
        
        for(size_t j = 0; j < child.children.size(); j++)
        {
          XMLTree& e = *child.children[j];
          if(e.content.name == "enumval")
          {
            values.push_back(j);
            names.push_back(getAttr(e, "name"));
            if(names.back() == def) *v = j;
          }
        }

        p.addEnum(name, v, names, values);
      }
      else if(type == "string")
      {
        std::string* v = new std::string("");

        unconvert(*v, getAttr(child, "default"));

        p.addString(name, v);
      }
      else if(type == "absrelsize")
      {
        int* origwidth = new int(1);
        int* origheight = new int(1);
        int* width = new int(1);
        int* height = new int(1);
        double* relwidth = new double(100);
        double* relheight = new double(100);
        bool* percentage = new bool(false);

        *origwidth = 100;
        *origheight = 100;
        unconvert(*width, getAttr(child, "defaultw"));
        unconvert(*height, getAttr(child, "defaulth"));
        unconvert(*relwidth, getAttr(child, "defaultrelw"));
        unconvert(*relheight, getAttr(child, "defaultrelh"));
        unconvert(*percentage, getAttr(child, "defaultusepercent"));

        p.addResize(name, origwidth, origheight, width, height, relwidth, relheight, percentage);
      }
      else if(type == "label")
      {
        p.addLabel(name);
      }
      else if(type == "tooptip")
      {
        p.addToolTipToLastRow(name);
      }
      
    }
  }
  
  return true;
}


void dynamicParametersToCParams(CParams& c, DynamicParameters& d)
{
  int numint=0, numdouble=0, numbool=0, numcolor=0, numstring=0, numenum=0;
  int stringlength = 0;
  
  for(size_t i = 0; i < d.size(); i++)
  {
    const Param& param = *d.get(i);
    ParameterType type = param.getType();

    if(type == P_INT) numint++;
    else if(type == P_DOUBLE) numdouble++;
    else if(type == PG_HUE) numdouble++;
    else if(type == P_BOOL) numbool++;
    else if(type == P_NAMED_BOOL) numbool++;
    else if(type == PG_MULTIBOOL)
    {
      const ParamMultiBool& p = dynamic_cast<const ParamMultiBool&>(param);
      numbool += p.bind.size();
    }
    else if(type == P_COLOR) numcolor++;
    else if(type == P_ENUM) numenum++;
    else if(type == P_STRING)
    {
      const ParamString& p = dynamic_cast<const ParamString&>(param);
      numstring++;
      stringlength += (*p.bind).size() + 1;
    }
    else if(type == PG_ABS_REL_SIZE)
    {
      numint += 2;
      numdouble += 2;
      numbool++;
    }
  }
  
  initCParams(c);
  if(numint) c.ints = new int[numint];
  if(numdouble) c.doubles = new double[numdouble];
  if(numbool) c.bools = new int[numbool];
  if(numcolor) c.colors = new double[numcolor * 4];
  if(numenum) c.enums = new int[numenum];
  if(stringlength) c.strings = new char[stringlength];
  
  int intindex=0, doubleindex=0, boolindex=0, colorindex=0, stringindex=0, enumindex=0;
  
  for(size_t i = 0; i < d.size(); i++)
  {
    const Param& param = *d.get(i);
    ParameterType type = param.getType();

    if(type == P_INT)
    {
      const ParamInt& p = dynamic_cast<const ParamInt&>(param);
      c.ints[intindex] = *p.bind;
      intindex++;
    }
    else if(type == P_DOUBLE)
    {
      const ParamDouble& p = dynamic_cast<const ParamDouble&>(param);
      c.doubles[doubleindex] = *p.bind;
      doubleindex++;
    }
    else if(type == PG_HUE)
    {
      const ParamHue& p = dynamic_cast<const ParamHue&>(param);
      c.doubles[doubleindex] = *p.bind;
      doubleindex++;
    }
    else if(type == P_BOOL)
    {
      const ParamBool& p = dynamic_cast<const ParamBool&>(param);
      c.bools[boolindex] = *p.bind;
      boolindex++;
    }
    else if(type == P_NAMED_BOOL)
    {
      const ParamNamedBool& p = dynamic_cast<const ParamNamedBool&>(param);
      c.bools[boolindex] = *p.bind;
      boolindex++;
    }
    else if(type == PG_MULTIBOOL)
    {
      const ParamMultiBool& p = dynamic_cast<const ParamMultiBool&>(param);
      for(size_t j = 0; j < p.bind.size(); j++)
      {
        c.bools[boolindex] = *p.bind[j];
        boolindex++;
      }
    }
    else if(type == P_COLOR)
    {
      const ParamColor& p = dynamic_cast<const ParamColor&>(param);
      c.colors[colorindex * 4 + 0] = p.bind->r;
      c.colors[colorindex * 4 + 1] = p.bind->g;
      c.colors[colorindex * 4 + 2] = p.bind->b;
      c.colors[colorindex * 4 + 3] = p.bind->a;
      
      colorindex++;
    }
    else if(type == P_ENUM)
    {
      const ParamEnum& p = dynamic_cast<const ParamEnum&>(param);
      c.enums[enumindex] = p.getValueAsInt();
      enumindex++;
    }
    else if(type == PG_ABS_REL_SIZE)
    {
      const ParamResize& p = dynamic_cast<const ParamResize&>(param);
      c.ints[intindex++] = *p.width;
      c.ints[intindex++] = *p.height;
      c.doubles[doubleindex++] = *p.relwidth;
      c.doubles[doubleindex++] = *p.relheight;
      c.bools[boolindex++] = *p.percentage;
    }
    else if(type == P_STRING)
    {
      const ParamString& p = dynamic_cast<const ParamString&>(param);
      std::string s = *p.bind;
      for(size_t j = 0; j < s.size(); j++) c.strings[stringindex++] = s[j];
      c.strings[stringindex++] = 0;
    }
  }
}
