/*
LodePaint

Copyright (c) 2009-2010 Lode Vandevenne
All rights reserved.

This file is part of LodePaint.

LodePaint is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LodePaint is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LodePaint.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "lpi/lpi_color.h"
#include "lpi/gl/lpi_texture_gl.h"
#include "lpi/gui/lpi_gui.h"
#include "lpi/gui/lpi_gui_ex.h"
#include "lpi/gui/lpi_gui_text.h"

#include "paint_texture.h"

#include <vector>
#include <iostream>
#include <cmath>

//extern std::vector<lpi::HTexture> textures_tools; //DANGER! this are auto_ptr-like objects in an std::vector, so set the correct size ONCE and then never resize it.

class UndoState;
struct UndoStateIndex;
class IImageFormat;

class BackgroundPatternDrawer
{
  public:
  
    lpi::TextureGL checker_tex;
    lpi::ColorRGB bg_color1;
    lpi::ColorRGB bg_color2;
    int checker_size;
    
    int patternindex; //index of preset patterns to cycle
    
    BackgroundPatternDrawer(lpi::GLContext* context);
    
    void setPattern(int pattern);
    
    void drawBackgroundPattern(lpi::IDrawer2D& drawer, int x0, int y0, int x1, int y1) const;
};

enum ColorMode //this is NOT color model, but color mode! That is, the memory structure.
{
  MODE_RGBA_8, //4 8-bit unsigned chars, RGBA, interleaved
  MODE_RGBA_32F, //4 32bit floating point numbers, RGBA, interleaved
  MODE_GREY_8, //1 channel of 8-bit unsigned chars
  MODE_GREY_32F //1 channel of 32-bit floating point numbers
};

struct FloatingSelection //not to be confused with floating point texture, in selection floating means "floats over the image", in floating point texture, floating means "32-bit floating point number"
{
  TextureRGBA8 texture;
  bool active;
  bool treat_alpha_as_opacity;
  
  FloatingSelection();
};

struct Options;

class Paint : public lpi::gui::Element
{
  private:
    
    BackgroundPatternDrawer bgpattern;
    double zoom_factor;
    int zoomfactorindex;
    ColorMode colorMode; //is fp enabled? (4x32-bit floating point color)
    bool mask_enabled;
    lpi::ColorRGB maskColor;
    int maskColorIndex;
    int pixelGridWidth; //0 for no grid
    int pixelGridHeight; //0 for no grid
    
    Options& options;
    
  public:
    
    struct RectSel //rectangular selection
    {
      int x0;
      int y0;
      int x1;
      int y1;
      
      RectSel();
    };
    
  public:

    TextureRGBA8 canvasRGBA8; //the OpenGL canvas texture (gets updated with canvas data all the time)
    TextureRGBA32F canvasRGBA32F; //floating point texture for high precision color (not to be confused with "floating selection"
    TextureGrey8 canvasGrey8;
    TextureGrey32F canvasGrey32F;
    
    TextureGrey8 mask8;
    TextureGrey32F mask32F;
    
    RectSel sel;
    FloatingSelection floatsel;

    void setColorMode(ColorMode mode, bool doupdate = true); //enable or disable floating point (4x32-bit floating point color)
    ColorMode getColorMode() const { return colorMode; } //is it floating point (4x32-bit floating point color)

    bool hasFloatingSelection() const { return floatsel.active; }
    bool hasNonFloatingSelection() const { return !floatsel.active && sel.x0 != sel.x1 && sel.y0 != sel.y1; }
    bool hasSelection() const { return hasNonFloatingSelection() || hasFloatingSelection(); }
    
    void makeSelectionFloating(const lpi::ColorRGB& color, bool copy);
    void copyFloatSelection();
    void unFloatSelection();
    void deleteSelection(const lpi::ColorRGB& color); //color is the background color that comes in place of the selection
    void makeFloatingSelection(const unsigned char* image, int w, int h, int x, int y); //Deletes any previously existing floating selection. x and y are position where to place the floating selection
    void makeFloatingSelection(int w, int h, int x, int y); //Deletes any previously existing floating selection. x and y are position where to place the floating selection. Texture data of floating selection will be uninitialized.

    virtual void drawImpl(lpi::gui::IGUIDrawer& drawer) const;
    virtual void handleImpl(const lpi::IInput& input);
    
    void selectNone();
    void selectAll();
    
    Paint(Options& options, lpi::GLContext* context); //set context to 0 to not use any OpenGL commands at all, ever (texture data only mode)
    void make(int sizex, int sizey, const lpi::ColorRGB& backColor = lpi::RGB_White);
    void setBrush(double size = 1.0, double hardness = 1.0, double opacity = 1.0);
    
    void upload(); //updates the texture (except if the texture changed size, use updateSize then). It's called "upload" because it "uploads" it to the video card.
    void uploadPartial(int x0, int y0, int x1, int y1); //updates only this rectangular part of the texture (less data to transfer to the video card if only that part changed)
    void updateSize(); //makes the Paint aware that the texture (canvasRGBA8) has changed size.
    void update(); //both updates size and uploads
    
    void clear();
    
    size_t getU() const;
    size_t getV() const;
    
    
    void zoom(int x, int y, bool in); //if in is false, it means "out"
    double getZoom() const { return zoom_factor; }
    void resetZoom(); //resets it to 1 (and sets int zoomfactorindex correct)
    
    //pixel to mouse/screen
    void pixelToScreen(int& x, int& y, int px, int py) const; //mouse position of center of this pixel
    void pixelToScreen(int& x, int& y, double px, double py) const;
    void pixelToScreen(int& x0, int& y0, int& x1, int& y1, int px, int py) const; //mouse position of corners of the pixel
    
    //mouse/screen to pixel
    void screenToPixel(int& px, int& py, const lpi::IInput& input) const; //results outside image range indicate that the mouse is outside!
    void screenToPixel(double& px, double& py, const lpi::IInput& input) const; //results outside image range indicate that the mouse is outside!
    void screenToPixel(double& px, double& py, int x, int y) const;
    void screenToPixel(int& px, int& py, int x, int y) const;
    
    void screenToNearestCenterOnScreen(int& cx, int& cy, int x, int y) const; //this calls first screenToPixel, then pixelToScreen, so you get a result close to the input, but in the center of the drawing pixel over which the mouse is.
    void screenToPixelCornersOnScreen(int& x0, int& y0, int& x1, int& y1, int x, int y) const;
    
    void setPattern(int pattern) { bgpattern.setPattern(pattern); }
    void setMaskColorIndex(int index);
    void setPixelGridIndex(int index);
    
    void enableMask(bool enabled) { mask_enabled = enabled; }
    bool maskEnabled() const {return mask_enabled; }
};

class PaintCorner : public lpi::gui::Element
{
  public:
  PaintCorner();

  virtual void drawImpl(lpi::gui::IGUIDrawer& drawer) const;
  virtual void handleImpl(const lpi::IInput& input);
};

class PaintCorners : public lpi::gui::Element
{
  private:

  PaintCorner pc[8];
  Paint& p;
  bool changed;
  bool wasgrabbed;
  
  public:
  int dy0; //top change
  int dx1; //right change
  int dy1; //bottom change
  int dx0; //left change

  PaintCorners(Paint& p);

  virtual void drawImpl(lpi::gui::IGUIDrawer& drawer) const;
  virtual void handleImpl(const lpi::IInput& input);

  //which side(s) of the image is the paintcorner with th is index on
  static bool top(int cornerIndex);
  static bool right(int cornerIndex);
  static bool bottom(int cornerIndex);
  static bool left(int cornerIndex);
  /*
   0   1   2
    IIIIIII
    IIIIIII
   7IIIIIII3
    IIIIIII
    IIIIIII
   6   5   4
  */

  bool hasChanged();
  
};



class PaintWindow : public lpi::gui::Window
{
  private:
    //lpi::gui::ScrollElement s;
    std::string knownfilename;
    IImageFormat* format; //related to knownfilename: required to save the file with correct encoding with which it was opened or last saved
    Paint p;
    
  public:
    PaintCorners pc;
    enum Status
    {
      S_NEW, //it's a new image. Not opened from file, created at start of LodePaint or with file new. No tool or filter has been used on it yet.
      S_SAVED, //it's not a new image, but it is on disk
      S_MODIFIED //the image contains changes that are not stored on disk
    };
    
  private:
    Status status;
    
  protected:
    virtual void resizeImpl(const lpi::gui::Pos<int>& newPos);

  public:

    PaintWindow(Options& options, lpi::GLContext* context);

    Paint& getPaint();
    const Paint& getPaint() const;
    
    void make(int x, int y, int paintingsizex, int paintingsizey, lpi::gui::IGUIDrawer& geom);
    
    virtual void handleImpl(const lpi::IInput& input);
    virtual void drawImpl(lpi::gui::IGUIDrawer& drawer) const;
    
    void setFilename(const std::string& filename);
    std::string getFilename() const;
    void clearFilename() { setFilename(""); }
    bool hasFilename();
    void setImageFormat(IImageFormat* format) { this->format = format; }
    IImageFormat* getImageFormat() const { return format; }
    
    
    void centerPaint(); //bring the center of the painting at the center of this window
    void zoom(bool in); //zoom without giving specific location (e.g. for toolbar zoom buttons)
    void zoom(bool in, const lpi::IInput& input); //zoom around mouse cursor
    void fitAroundPainting(int ax0, int ay0, int ax1, int ay1);
    
    Status getStatus() const {return status;}
    void setStatus(Status status) { this->status = status; }
    
    
    int getVisibleX0() const;
    int getVisibleY0() const;
    int getVisibleX1() const;
    int getVisibleY1() const;
};

