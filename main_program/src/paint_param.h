/*
LodePaint

Copyright (c) 2009 Lode Vandevenne
All rights reserved.

This file is part of LodePaint.

LodePaint is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LodePaint is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LodePaint.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <string>
#include <vector>
#include <map>

#include "lpi/lpi_color.h"

/*
Parameters for LodePaint filters, tools and file formats.
Each parameter type determines not only the data structure of the parameter, but
also the type of GUI widget used for it in the dynamic GUI. There also exist
grouped parameters that group multiple of the standard parameter types together
in a different type of GUI widget. There also exist empty parameters that only
define a certain dummy GUI widget.

Eventually, the plan is to support the following types of parameters:

NOTE: many of the elements of the lists below are not supported yet, but are planned.


Standard types:

-int: 32-bit signed integer --> GUI: slider + spinner + text input line
-double: 64-bit IEEE double precision floating point value --> GUI: slider + spinner + text input line
-bool: true or false value, but represented as 32-bit signed integer! --> GUI: checkbox
-string (with 8-bit chars, ASCII for now, maybe UTF-8 some day) --> GUI: text input line
-enum: represented as 32-bit signed integer, involves named choice --> GUI: dropdown box
-color: represented by 4 doubles (with 0.0-1.0 the typical range) --> GUI: colored clickable field that opens color dialog
-matrix: N*M double values --> GUI: matrix widget (see e.g. "user defined", "2D transform" and "RGBA matrix" filter)
-curve: N double values, represented as a graph --> GUI: graph widget in which the user can draw the graph (plus some buttons to manipulate it etc....). There are some variations possible, such as logarithmic or not.
-gradient: a gradient --> GUI: gradient widget allowing to choose colors and positions of the colors

NOTE: matrix and curve aren't Grouped types, because they have their own pointer in the CParams struct.

Grouped types:

-multibool: multiple bools --> GUI: multiple checkboxes in a row
-FG color: this is a standard color, but it's linked to the standard foreground color in LodePaint, so editing it in the filter dialog does the same as editing it in the main painting program
-BG color: this is a standard color, but it's linked to the standard background color in LodePaint, so editing it in the filter dialog does the same as editing it in the main painting program
-RGBA curve: similar to curve, but it's 4 curves: one for each color channel. --> GUI: widget similar to that of curve, but with 4 colors (and greyscale option linking 3 values together)
-absolute/relative size: involves 1 bool, 2 ints and 2 doubles. Allows choice between absolute and relative size of the image. Can also represent a position instead. --> GUI: widget with settings to maintain aspect ratio etc...

Empty types:

-separator: makes the GUI show an empty separater
-label: makes the GUI show a title of a sub-section
-tooltip: makes the GUI add a tooltip to last row
-histogram: makes the GUI show a small histogram of the image
*/


/*
This is a work in progress and idea prototype, nothing from this header is currently used.

The end result of this will be a totally new C interface for plugins, involving XML in the direction
from plugin to LodePaint, and a struct CParams in the direction from LodePaint to the plugin.
*/


/*
This is a pure C-struct, for usage in LodePaint plugins in the future
To be able to use it, you must know how much of each type of parameter there is, to know the length of each array
Both the plugin and LodePaint know these amounts, because the plugin tells to LodePaint which parameter types there are and how many of each

The sizes of the types used must be the following:
  (unsigned) int: 32-bit (un)signed integer
  (unsigned) char: 8-bit string or data characters
  double: 64-bit IEEE double precision
  
  pointers: these are direct pointers to memory in LodePaint that the dynamic plugin lib can access through these.
  The size of pointers, and thus the sizeof(CParams), depends on platform (32-bit or 64-bit), so that means:
    -32-bit version of LodePaint: each pointer should be 32-bit in both the plugin dynamic lib and in LodePaint
    -64-bit version of LodePaint: each pointer should be 64-bit in both the plugin dynamic lib and in LodePaint

Copy this struct in each filter C or C++ source code! Don't include paint_param.h in the filter
source code, just copy this struct and ensure it uses exactly as much memory.

Some parameters are not used yet but already defined here so that the struct is future-proof

colors: 4 doubles per color (r, g, b, a)
strings: there's a 0 character between each string

matrices: the number of doubles of each matrix depends on its size
curves: the number of values depends on the number of x values of each curve (some input types are RGBA curves, these is represented as 4 curves in a row here)
gradients: 5 doubles per color: its position in the gradient and the RGBA value. The first must be at position 0.0, the last must be at position 1.0, in between must be strictly monotonically increasing
*/
typedef struct CParams
{
  int* ints;
  double* doubles;
  int* bools;
  int* enums;
  double* colors;
  char* strings;

  double* matrices;
  double* curves;
  double* gradients;
} CParams;

void initCParams(CParams& params); //sets all pointers to null
void cleanupCParams(CParams& params); //deletes all arrays (only use if they were created with new [])

enum ParameterType
{
  //standard types
  P_INT,
  P_DOUBLE,
  P_BOOL,
  P_NAMED_BOOL,
  P_ENUM,
  P_COLOR,
  P_STRING,
  P_MATRIX,
  P_CURVE,
  P_GRADIENT,
  
  //grouped and special types
  PG_MULTIBOOL,
  PG_FG_COLOR,
  PG_BG_COLOR,
  PG_RGBA_CURVE,
  PG_ABS_REL_SIZE,
  PG_HUE, //a hue colored slider, representing a single P_DOUBLE
  
  //empty types
  PE_SEPARATOR,
  PE_LABEL, //text row
  PE_TOOLTIP,
  PE_HISTOGRAM,
  
  P_NUM_PARAMETER_TYPES //don't use this
};

struct Param
{
  std::string name;
  
  virtual ~Param() {};
  
  virtual ParameterType getType() const = 0;
  
  virtual void cleanup() = 0; //for the cleanMemory system of DynamicParameters
  
  //Param(const std::string& name) : name(name) {}
};

template<typename T>
struct ParamT : public Param
{
  T* bind;
  
  T def; //default
  
  virtual void cleanup() { delete bind; }
  
  //ParamT(const std::string& name, T* bind, const T& def) : Param(name), bind(bind), def(def) {}
};

template<typename T>
struct ParamNumber : public ParamT<T>
{
  //min and max value for the slider
  T minl;
  T maxl;
  //min and max value for the spinner
  T minp;
  T maxp;
  //step for the spinner
  T step;
  
  //ParamNumber(const std::string& name, T* bind, const T& def, const T& min, const T& max, const T& step) : ParamT(bname, ind, def), minl(min), maxl(max), minp(min), maxp(max), step(step) {}
  //ParamNumber(const std::string& name, T* bind, const T& def, const T& minl, const T& maxl, const T& minp, const T& maxp, const T& step) : ParamT(name, bind, def), minl(minl), maxl(maxl), minp(minp), maxp(maxp), step(step) {}
};

struct ParamInt : public ParamNumber<int>
{
  virtual ParameterType getType() const { return P_INT; }
};

struct ParamDouble : public ParamNumber<double>
{
  virtual ParameterType getType() const { return P_DOUBLE; }
};

struct ParamHue : public ParamNumber<double>
{
  virtual ParameterType getType() const { return PG_HUE; }
};

struct ParamBool : public ParamT<bool>
{
  virtual ParameterType getType() const { return P_BOOL; }
};

struct ParamNamedBool : public ParamT<bool>
{
  std::string name0; //name for false value
  std::string name1; //name for true value
  
  virtual ParameterType getType() const { return P_NAMED_BOOL; }
};

struct ParamMultiBool : public Param
{
  std::vector<bool> def; //defaults
  std::vector<bool*> bind;
  
  virtual ParameterType getType() const { return PG_MULTIBOOL; }
  virtual void cleanup() {};
};

struct ParamColor : public ParamT<lpi::ColorRGBd>
{
  virtual ParameterType getType() const { return P_COLOR; }
};

struct ParamString : public ParamT<std::string>
{
  virtual ParameterType getType() const { return P_STRING; }
};

struct ParamEnum : public Param
{
  int def; //default
  std::vector<std::string> names;
  std::vector<int> valuesInt;
  
  virtual int getValueAsInt() const = 0;
  virtual void setValueAsInt(int value) const = 0;
};

template<typename T>
struct ParamEnumT : public ParamEnum
{
  std::vector<T> valuesT;
  
  T* bind;
  
  virtual void cleanup() { delete bind; }
  
  ParamEnumT(const std::vector<std::string>& names, const std::vector<T>& values)
  : valuesT(values)
  {
    for(size_t i = 0; i < values.size(); i++) valuesInt.push_back(values[i]);
    this->names = names;
  }
    
  virtual ParameterType getType() const { return P_ENUM; }
  
  int getValueAsInt()const 
  {
    return (int)(*bind);
  }
  
  void setValueAsInt(int value)const 
  {
    *bind = (T)value;
  }
};

struct ParamLabel : public Param
{

  virtual ParameterType getType() const { return PE_LABEL; }
  virtual void cleanup() {};
};

struct ParamToolTip : public Param
{

  virtual ParameterType getType() const { return PE_TOOLTIP; }
  virtual void cleanup() {};
};

struct ParamResize : public Param
{
  int* origwidth;
  int* origheight;
  int* width;
  int* height;
  double* relwidth;
  double* relheight;
  bool* percentage;
  
  virtual ParameterType getType() const;
  
  virtual void cleanup();
  
};

/*
DynamicParameters: this will be used by all LodePaint filters to describe their
standardized dynamic parameters.

This class will be able to be used to:
-create dynamic GUI allowing to edit all parameters
-convert the parameters from/to XML
-convert the parameters from/to a CParams struct that can be given to C-interface plugins
-remember and reuse values of parameters (for macros, recording, scripts, batch processing, ...)
-...
*/
class DynamicParameters
{
  private:
    std::vector<Param*> params;
    bool cleanMemory;
    
  public:

    /*
    cleanMemory: if true, this class will call "cleanup" on all parameters, if false not. So,
    set "cleanMemory" to false if the values of the parameters (the actual int, double, etc...
    variables) are stored elsewhere. Set it to true if the parameter values are made with "new"
    and you want this DynamicParameters to delete them for you (e.g. for auto generated
    parameter sets from XML, ...).
    */
    DynamicParameters(bool cleanMemory);
    ~DynamicParameters();
    
    //default values aren't given in the constructors: those are given by *bind
  
    void addInt(const std::string& name, int* bind, int minl, int maxl, int minp, int maxp, int step);
    void addInt(const std::string& name, int* bind, int min, int max, int step);
    void addDouble(const std::string& name, double* bind, double minl, double maxl, double minp, double maxp, double step);
    void addDouble(const std::string& name, double* bind, double minl, double max, double step);
    void addHue(const std::string& name, double* bind);
    void addBool(const std::string& name, bool* bind);
    void addNamedBool(const std::string& name, bool* bind, const std::string& name0, const std::string& name1); //it's a bool where each option has its own name (presented as a dropdown with 2 choices in GUI)
    void addMultiBool(const std::string& name, const std::vector<bool*>& bind);
    void addString(const std::string& name, std::string* bind);
    void addColor(const std::string& name, lpi::ColorRGBd* bind);
    void addLabel(const std::string& name);
    void addToolTipToLastRow(const std::string& name);
    
    void addResize(const std::string& name, int* origw, int* origh, int* w, int* h, double* relw, double* relh, bool* percentage);
    
    template<typename T>
    void addEnum(const std::string& name, T* bind, const std::vector<std::string>& names, const std::vector<T> values)
    {
      ParamEnumT<T>* p = new ParamEnumT<T>(names, values);
      p->name = name;
      p->bind = bind;
      p->def = *bind;
      params.push_back(p);
    }
    
    size_t size() const;
    const Param* get(size_t index) const;
    Param* get(size_t index);
    
    void setCleanMemory(bool set) { cleanMemory = set; }
};

//The given DynamicParameters must have cleanMemory to true, or else there'll be a memory leak
bool xmlToDynamicParameters(DynamicParameters& p, const std::string& xml);

void dynamicParametersToCParams(CParams& c, DynamicParameters& d);

/*
XML for describing the filter parameters. This should be used by the plugins in C to communicate
the names and types of parameters. Includes description of GUI for the dynamic dialogs (with rows of settings).

The communcation between plugin and LodePaint will use XML only in the direction from plugin
to LodePaint. In the other direction it uses a simple struct. This because returning an XML
string from within a C plugin is much easier, than adding a complete XML parser to the C plugin, and
LodePaint is about having as little library dependencies as possible.

This represents an XML shema but in the form of a simple example for now, instead of the actual XSD.

<parameters>
  <var type="int" name="..." minl="..." maxl="..." minp="..." maxp="..." step="..." default="..."/> <!--min, max and step are for the slider and spinner buttons in the gui, but are not enforced: they can be overidden by using the text input fields-->
  <var type="double" name="..." minl="..." maxl="..." minp="..." maxp="..." step="..." default="..."/>
  <var type="bool" name="..." default="..."/>
  <var type="string" name="..." default="..."/>
  <var type="color" name="..." default="0.5 1.0 0.5 1.0"/>
  <var type="matrix" name="..." default="..."/> <!--default can be O for all 0's, I for identity, J for all 1's or C for a 1 in the center and the rest 0-->
  <var type="enum" name="..." default="...">
    <enumval name="..."/>
    <enumval name="..."/>
  </var>
  <var type="absrelsize" name="..." defaultw="..." defaulth="..." defaultrelw="..." defaultrelh="..." defaultusepercent="..."/>
  <var type="label" name="..."/>
</parameters>
*/
