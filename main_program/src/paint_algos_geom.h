/*
LodePaint

Copyright (c) 2009-2010 Lode Vandevenne
All rights reserved.

This file is part of LodePaint.

LodePaint is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LodePaint is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LodePaint.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "lpi/lpi_color.h"
#include "lpi/lpi_math2d.h"

void fillPolygon(unsigned char* buffer, int buffer_w, int buffer_h, int* xs, int* ys, int num, const lpi::ColorRGB& color, double opacity, bool affect_alpha);
void fillPolyPolygon(unsigned char* buffer, int buffer_w, int buffer_h, int** xs, int** ys, int* nums, int num, const lpi::ColorRGB& color, double opacity, bool affect_alpha);

void drawPolygon(unsigned char* buffer, int buffer_w, int buffer_h
                , int* xs, int* ys, int num
                , int thickness
                , bool outline, const lpi::ColorRGB& lineColor, double lineOpacity
                , bool filled, const lpi::ColorRGB& fillColor, double fillOpacity
                , bool affect_alpha);

//fillTriangle can be used as the basis for any 2D shape (by building it out of triangles): it's a plain single filled triangle
void fillTriangle(unsigned char* buffer, int buffer_w, int buffer_h, int x0, int y0, int x1, int y1, int x2, int y2, const lpi::ColorRGB& color, double opacity, bool affect_alpha);
void drawTriangle(unsigned char* buffer, int buffer_w, int buffer_h
                , int x0, int y0, int x1, int y1, int x2, int y2
                , int thickness
                , bool outline, const lpi::ColorRGB& lineColor, double lineOpacity
                , bool filled, const lpi::ColorRGB& fillColor, double fillOpacity
                , bool affect_alpha);


void drawRectangle(unsigned char* buffer, int buffer_w, int buffer_h
                 , int x0, int y0, int x1, int y1
                 , int thickness
                 , bool outline, const lpi::ColorRGB& lineColor, double lineOpacity
                 , bool filled, const lpi::ColorRGB& fillColor, double fillOpacity
                 , bool affect_alpha);

void drawRoundedRectangle(unsigned char* buffer, int buffer_w, int buffer_h
                        , int x0, int y0, int x1, int y1
                        , int radiusx, int radiusy
                        , int thickness
                        , bool outline, const lpi::ColorRGB& lineColor, double lineOpacity
                        , bool filled, const lpi::ColorRGB& fillColor, double fillOpacity
                        , bool affect_alpha);


void drawEllipse(unsigned char* buffer, int buffer_w, int buffer_h
                , int x0, int y0, int x1, int y1
                , int thickness
                , bool outline, const lpi::ColorRGB& lineColor, double lineOpacity
                , bool filled, const lpi::ColorRGB& fillColor, double fillOpacity
                , bool affect_alpha);

bool getInnerTriangle(lpi::Vector2& i0, lpi::Vector2& i1, lpi::Vector2& i2, const lpi::Vector2& p0, const lpi::Vector2& p1, const lpi::Vector2& p2, int thickness);
