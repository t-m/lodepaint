/*
LodePaint

Copyright (c) 2009-2010 Lode Vandevenne
All rights reserved.

This file is part of LodePaint.

LodePaint is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LodePaint is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LodePaint.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "paint_gui.h"
#include "paint_imageformats.h"
#include "paint_settings.h"
#include "paint_window.h"

struct Paintings;

bool openImageFile(PaintWindow& paintWindow, const std::string& filename, MainProgram& g);
void saveImageFile(PaintWindow& paintWindow, const std::string& filename, IImageFormat* format, bool updatefilename, MainProgram& h);
void saveAs(PaintWindow& paintWindow, bool updatefilename, MainProgram& h); //updatefilename false gives "save copy as..." behaviour;
void save(PaintWindow& paintWindow, MainProgram& p);
bool open(PaintWindow& paintWindow, MainProgram& p);
bool open(Paintings& paintings, MainProgram& p);
bool makeNewImage(MainProgram& h, PaintWindow& paintWindow, GlobalToolSettings& globalToolSettings, Options& options);

