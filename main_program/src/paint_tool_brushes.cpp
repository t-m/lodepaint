/*
LodePaint

Copyright (c) 2009-2010 Lode Vandevenne
All rights reserved.

This file is part of LodePaint.

LodePaint is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LodePaint is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LodePaint.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "paint_tool_brushes.h"

#include "lpi/gui/lpi_gui_color.h"
#include "lpi/lpi_math.h"
#include "lpi/lpi_translate.h"

#include "imalg/tolerance.h"

#include "paint_algos.h"
#include "paint_gui.h"

#include <iostream>

////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////

void IBrush::getUndoState(UndoState& state)
{
  undohelper.addToUndo(state);
  undohelper.reset();
}

void IBrush::undo(Paint& paint, UndoState& state)
{
  UndoStateIndex index;
  UndoState redostate;
  ColorMode mode = paint.getColorMode();
  
  if(editMask())
  {
    if(mode == MODE_RGBA_8 || mode == MODE_GREY_8) readPartialTextureFromUndo(paint.mask8, redostate, state, index);
    else if(mode == MODE_RGBA_32F || mode == MODE_GREY_32F) readPartialTextureFromUndo(paint.mask32F, redostate, state, index);
  }
  else
  {
    if(mode == MODE_RGBA_8) readPartialTextureFromUndo(paint.canvasRGBA8, redostate, state, index);
    else if(mode == MODE_RGBA_32F) readPartialTextureFromUndo(paint.canvasRGBA32F, redostate, state, index);
    else if(mode == MODE_GREY_8) readPartialTextureFromUndo(paint.canvasGrey8, redostate, state, index);
    else if(mode == MODE_GREY_32F) readPartialTextureFromUndo(paint.canvasGrey32F, redostate, state, index);
  }
  
  state.swap(redostate);
}

void IBrush::redo(Paint& paint, UndoState& state)
{
  UndoStateIndex index;
  UndoState undostate;
  ColorMode mode = paint.getColorMode();
  if(editMask())
  {
    if(mode == MODE_RGBA_8 || mode == MODE_GREY_8) readPartialTextureFromUndo(paint.mask8, undostate, state, index);
    else if(mode == MODE_RGBA_32F || mode == MODE_GREY_32F) readPartialTextureFromUndo(paint.mask32F, undostate, state, index);
  }
  else
  {
    if(mode == MODE_RGBA_8) readPartialTextureFromUndo(paint.canvasRGBA8, undostate, state, index);
    else if(mode == MODE_RGBA_32F) readPartialTextureFromUndo(paint.canvasRGBA32F, undostate, state, index);
    else if(mode == MODE_GREY_8) readPartialTextureFromUndo(paint.canvasGrey8, undostate, state, index);
    else if(mode == MODE_GREY_32F) readPartialTextureFromUndo(paint.canvasGrey32F, undostate, state, index);
  }
  state.swap(undostate);
}

////////////////////////////////////////////////////////////////////////////////

namespace
{

  double distance2D(int x1, int y1, int x2, int y2)
  {
    return std::sqrt(double((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2)));
  }

  //returns true if point2 is between point1 and point3 (the 3 points are supposed to be on the same line)
  bool inBetween(int x1, int y1, int x2, int y2, int x3, int y3)
  {
    if((x1 - x2) * (x3 - x2) <= 0 && (y1 - y2) * (y3 - y2) <= 0) return true;
    else return false;
  }

}

////////////////////////////////////////////////////////////////////////////////
    
Tool_Brush::Tool_Brush(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom)
: IBrush(settings, geom)
, oldMouseX(0)
, oldMouseY(0)
{
  initiate = true;
  justdone = false;
  addBrushSettingsToPage(dialog, geom);
  dialog.addControl(_t("Affect Alpha"), new lpi::gui::DynamicCheckbox(&settings.other.affect_alpha));
}

void Tool_Brush::setAffectChannels(bool r, bool g, bool b, bool a)
{
  extra.affect_r = r;
  extra.affect_g = g;
  extra.affect_b = b;
  extra.affect_a = a;
}

lpi::ColorRGB Tool_Brush::getColor(const lpi::IInput& input, Paint& paint)
{
  (void)paint;
  bool left = paint.mouseGrabbed(input, lpi::LMB);
  return left ? settings.leftColor255() : settings.rightColor255();
}

static int colorToGrey(const lpi::ColorRGB& color)
{
  return (color.r + color.g + color.b) / 3;
}

static float colorToGreyf(const lpi::ColorRGB& color)
{
  return (color.r + color.g + color.b) / (3.0f * 255.0f);
}

void Tool_Brush::drawBrush(int& x1, int& y1, int x2, int y2, const lpi::ColorRGB& color, Paint& paint)
{
  ColorMode mode = paint.getColorMode();
  if(editMask())
  {
    if(mode == MODE_RGBA_8 || mode == MODE_GREY_8) drawBrush(x1, y1, x2, y2, colorToGrey(color), paint.mask8);
    else if(mode == MODE_RGBA_32F || mode == MODE_GREY_32F) drawBrush(x1, y1, x2, y2, colorToGreyf(color), paint.mask32F);
  }
  else
  {
    if(mode == MODE_RGBA_8) drawBrush(x1, y1, x2, y2, color, paint.canvasRGBA8);
    else if(mode == MODE_GREY_8) drawBrush(x1, y1, x2, y2, colorToGrey(color), paint.canvasGrey8);
    else if(mode == MODE_RGBA_32F) drawBrush(x1, y1, x2, y2, lpi::RGBtoRGBd(color), paint.canvasRGBA32F);
    else if(mode == MODE_GREY_32F) drawBrush(x1, y1, x2, y2, colorToGreyf(color), paint.canvasGrey32F);
  }
}

void Tool_Brush::drawBrush(int& x1, int& y1, int x2, int y2, const lpi::ColorRGB& color, TextureRGBA8& texture)
{
  int radius = settings.brush.size / 2;
  int ux0 = lpi::clamp<int>(std::min(x1 - radius, x2 - radius) - 1, 0, texture.getU());
  int uy0 = lpi::clamp<int>(std::min(y1 - radius, y2 - radius) - 1, 0, texture.getV());
  int ux1 = lpi::clamp<int>(std::max(x1 + radius, x2 + radius) + 1, 0, texture.getU());
  int uy1 = lpi::clamp<int>(std::max(y1 + radius, y2 + radius) + 1, 0, texture.getV());

  undohelper.add(ux0, uy0, ux1, uy1, texture);

  if(settings.brush.size > 1 || settings.brush.step > 1.0)
  {
    int x = x1, y = y1;
    if(initiate) drawBrushShape(texture.getBuffer(), texture.getU2(), texture.getV2(), x, y, color, settings.brush, extra);

    int i = 0;
    while(distance2D(x, y, x2, y2) > (settings.brush.size * settings.brush.step) && inBetween(x1, y1, x, y, x2, y2))
    {
      i++;
      x = int(x1 + i * (settings.brush.size * settings.brush.step) * (x2 - x1) / distance2D(x1, y1, x2, y2));
      y = int(y1 + i * (settings.brush.size * settings.brush.step) * (y2 - y1) / distance2D(x1, y1, x2, y2));
      drawBrushShape(texture.getBuffer(), texture.getU2(), texture.getV2(), x, y, color, settings.brush, extra);
      if(settings.brush.step <= 0.0001) break; //avoid infinite loop
    }
    x1 = x;
    y1 = y;
  }
  else //if brush size is 1, drawLine looks nicer
  {
    drawThinLine(texture.getBuffer(), texture.getU(), texture.getV(), texture.getU2(), x1, y1, x2, y2, color, settings.brush.opacity, extra, false);
    x1 = x2;
    y1 = y2;
  }

  texture.updatePartial(ux0, uy0, ux1, uy1);
}

void Tool_Brush::drawBrush(int& x1, int& y1, int x2, int y2, int color, TextureGrey8& texture)
{
  int radius = settings.brush.size / 2;
  int ux0 = lpi::clamp<int>(std::min(x1 - radius, x2 - radius) - 1, 0, texture.getU());
  int uy0 = lpi::clamp<int>(std::min(y1 - radius, y2 - radius) - 1, 0, texture.getV());
  int ux1 = lpi::clamp<int>(std::max(x1 + radius, x2 + radius) + 1, 0, texture.getU());
  int uy1 = lpi::clamp<int>(std::max(y1 + radius, y2 + radius) + 1, 0, texture.getV());

  undohelper.add(ux0, uy0, ux1, uy1, texture);

  if(settings.brush.size > 1 || settings.brush.step > 1.0)
  {
    int x = x1, y = y1;
    if(initiate) drawBrushShapeGrey8(texture.getBuffer(), texture.getU2(), texture.getV2(), x, y, color, settings.brush);

    int i = 0;
    while(distance2D(x, y, x2, y2) > (settings.brush.size * settings.brush.step) && inBetween(x1, y1, x, y, x2, y2))
    {
      i++;
      x = int(x1 + i * (settings.brush.size * settings.brush.step) * (x2 - x1) / distance2D(x1, y1, x2, y2));
      y = int(y1 + i * (settings.brush.size * settings.brush.step) * (y2 - y1) / distance2D(x1, y1, x2, y2));
      drawBrushShapeGrey8(texture.getBuffer(), texture.getU2(), texture.getV2(), x, y, color, settings.brush);
      if(settings.brush.step <= 0.0001) break; //avoid infinite loop
    }
    x1 = x;
    y1 = y;
  }
  else //if brush size is 1, drawLine looks nicer
  {
    drawThinLineGrey8(texture.getBuffer(), texture.getU(), texture.getV(), texture.getU2(), x1, y1, x2, y2, color, settings.brush.opacity, false);
    x1 = x2;
    y1 = y2;
  }

  texture.updatePartial(ux0, uy0, ux1, uy1);
}

void Tool_Brush::drawBrush(int& x1, int& y1, int x2, int y2, const lpi::ColorRGBd& color, TextureRGBA32F& texture)
{
  int radius = settings.brush.size / 2;
  int ux0 = lpi::clamp<int>(std::min(x1 - radius, x2 - radius) - 1, 0, texture.getU());
  int uy0 = lpi::clamp<int>(std::min(y1 - radius, y2 - radius) - 1, 0, texture.getV());
  int ux1 = lpi::clamp<int>(std::max(x1 + radius, x2 + radius) + 1, 0, texture.getU());
  int uy1 = lpi::clamp<int>(std::max(y1 + radius, y2 + radius) + 1, 0, texture.getV());

  undohelper.add(ux0, uy0, ux1, uy1, texture);

  if(settings.brush.size > 1 || settings.brush.step > 1.0)
  {
    int x = x1, y = y1;
    if(initiate) drawBrushShape(texture.getBuffer(), texture.getU2(), texture.getV2(), x, y, color, settings.brush, extra);

    int i = 0;
    while(distance2D(x, y, x2, y2) > (settings.brush.size * settings.brush.step) && inBetween(x1, y1, x, y, x2, y2))
    {
      i++;
      x = int(x1 + i * (settings.brush.size * settings.brush.step) * (x2 - x1) / distance2D(x1, y1, x2, y2));
      y = int(y1 + i * (settings.brush.size * settings.brush.step) * (y2 - y1) / distance2D(x1, y1, x2, y2));
      drawBrushShape(texture.getBuffer(), texture.getU2(), texture.getV2(), x, y, color, settings.brush, extra);
      if(settings.brush.step <= 0.0001) break; //avoid infinite loop
    }
    x1 = x;
    y1 = y;
  }
  else //if brush size is 1, drawLine looks nicer
  {
    drawThinLine(texture.getBuffer(), texture.getU(), texture.getV(), texture.getU2(), x1, y1, x2, y2, color, settings.brush.opacity, extra, false);
    x1 = x2;
    y1 = y2;
  }

  texture.updatePartial(ux0, uy0, ux1, uy1);
}

void Tool_Brush::drawBrush(int& x1, int& y1, int x2, int y2, float color, TextureGrey32F& texture)
{
  int radius = settings.brush.size / 2;
  int ux0 = lpi::clamp<int>(std::min(x1 - radius, x2 - radius) - 1, 0, texture.getU());
  int uy0 = lpi::clamp<int>(std::min(y1 - radius, y2 - radius) - 1, 0, texture.getV());
  int ux1 = lpi::clamp<int>(std::max(x1 + radius, x2 + radius) + 1, 0, texture.getU());
  int uy1 = lpi::clamp<int>(std::max(y1 + radius, y2 + radius) + 1, 0, texture.getV());

  undohelper.add(ux0, uy0, ux1, uy1, texture);

  if(settings.brush.size > 1 || settings.brush.step > 1.0)
  {
    int x = x1, y = y1;
    if(initiate) drawBrushShapeGrey32f(texture.getBuffer(), texture.getU2(), texture.getV2(), x, y, color, settings.brush);

    int i = 0;
    while(distance2D(x, y, x2, y2) > (settings.brush.size * settings.brush.step) && inBetween(x1, y1, x, y, x2, y2))
    {
      i++;
      x = int(x1 + i * (settings.brush.size * settings.brush.step) * (x2 - x1) / distance2D(x1, y1, x2, y2));
      y = int(y1 + i * (settings.brush.size * settings.brush.step) * (y2 - y1) / distance2D(x1, y1, x2, y2));
      drawBrushShapeGrey32f(texture.getBuffer(), texture.getU2(), texture.getV2(), x, y, color, settings.brush);
      if(settings.brush.step <= 0.0001) break; //avoid infinite loop
    }
    x1 = x;
    y1 = y;
  }
  else //if brush size is 1, drawLine looks nicer
  {
    drawThinLineGrey32f(texture.getBuffer(), texture.getU(), texture.getV(), texture.getU2(), x1, y1, x2, y2, color, settings.brush.opacity, false);
    x1 = x2;
    y1 = y2;
  }

  texture.updatePartial(ux0, uy0, ux1, uy1);
}

void Tool_Brush::handle(const lpi::IInput& input, Paint& paint)
{
  bool left = paint.mouseGrabbed(input, lpi::LMB);
  bool right = paint.mouseGrabbed(input, lpi::RMB);
  
  extra.affect_a = settings.other.affect_alpha;
  
  if(left || right)
  {
    lpi::ColorRGB color = getColor(input, paint);
    int drawx, drawy;
    paint.screenToPixel(drawx, drawy, input);
    
    if(input.keyDown(SDLK_LSHIFT)) initiate = false;
    /*int radiantOldMouseX = oldMouseX;
    int radiantOldMouseY = oldMouseY;*/
    
    if(initiate)
    {
      undohelper.reset();
      oldMouseX = drawx;
      oldMouseY = drawy;
      drawBrush(oldMouseX, oldMouseY, drawx, drawy, color, paint);
      initiate = false;
    }
    if(distance2D(drawx, drawy, oldMouseX, oldMouseY) > (settings.brush.size * settings.brush.step))
    {
      drawBrush(oldMouseX, oldMouseY, drawx, drawy, color, paint);
    }
    
    /*if(input.keyDown(SDLK_LCTRL))
    {
      oldMouseX = radiantOldMouseX;
      oldMouseY = radiantOldMouseY;
    }*/
  }
  else
  {
    if(!initiate) justdone = true;
    initiate = true;
  }
}

void Tool_Brush::draw(lpi::gui::IGUIDrawer& drawer, Paint& paint)
{
  drawCommonCrosshair(drawer, paint, 0, 0);
  
  int x = drawer.getInput().mouseX();
  int y = drawer.getInput().mouseY();
  int radius = (int)(paint.getZoom() * settings.brush.size / 2);
  int radius1 = radius - 1;
  //lpi::ColorRGB color = settings.leftColor;
  //color.a = 255;
  //lpi::ColorRGB color1 = lpi::gui::getIndicatorColor(color);
  lpi::ColorRGB color = lpi::RGB_Black;
  lpi::ColorRGB color1 = lpi::RGB_White;
  
  int xc, yc; //center of pixel over which mouse is, in screen coordinates
  paint.screenToPixel(xc, yc, x, y);
  paint.pixelToScreen(xc, yc, xc, yc);
  
  if(paint.mouseOver(drawer.getInput()))
  {
    //TODO: instead of two lines, consider one line with pattern with the two colors (but first a drawer for that needs to be made...)
    if(settings.brush.shape == Brush::SHAPE_SQUARE)
    {
      int x2, y2, x0, y0, x1, y1, dummy;
      paint.screenToPixel(x2, y2, x, y);
      paint.pixelToScreen(x0, y0, dummy, dummy, x2 - settings.brush.size / 2, y2 - settings.brush.size / 2);
      paint.pixelToScreen(x1, y1, dummy, dummy, x2 - settings.brush.size / 2 + settings.brush.size, y2 - settings.brush.size / 2 + settings.brush.size);
    
      drawer.drawRectangle(x0, y0, x1, y1, color, false);
      drawer.drawRectangle(x0-1, y0-1, x1+1, y1+1, color1, false);
    }
    else if(settings.brush.shape == Brush::SHAPE_ROUND)
    {
      drawer.drawCircle(xc, yc, radius, color, false);
      drawer.drawCircle(xc, yc, radius1, color1, false);
    }
    else if(settings.brush.shape == Brush::SHAPE_DIAMOND)
    {
      drawer.drawLine(xc, yc - radius,     xc + radius, yc, color);
      drawer.drawLine(xc + radius, yc,     xc, yc + radius, color);
      drawer.drawLine(xc, yc + radius,     xc - radius, yc, color);
      drawer.drawLine(xc - radius, yc,     xc, yc - radius, color);
      drawer.drawLine(xc, yc - radius - 1, xc + radius + 1, yc, color1);
      drawer.drawLine(xc + radius + 1, yc, xc, yc + radius + 1, color1);
      drawer.drawLine(xc, yc + radius + 1, xc - radius - 1, yc, color1);
      drawer.drawLine(xc - radius - 1, yc, xc, yc - radius - 1, color1);
    }
    else
    {
      //unknown???????
      drawer.drawRectangle(xc - radius, yc - radius, xc + radius, yc + radius, lpi::RGB_Red, false);
    }
  }
}

////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
    
Tool_Eraser::Tool_Eraser(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom)
: IBrush(settings, geom)
, oldMouseX(0)
, oldMouseY(0)
, alpha_left(0)
, alpha_right(255)
{
  initiate = true;
  justdone = false;
  addBrushSettingsToPage(dialog, geom);
  dialog.addControl(_t("Alpha Left"), new lpi::gui::DynamicSliderSpinner<int>(&alpha_left, 0, 255, 1, geom));
  dialog.addControl(_t("Alpha Right"), new lpi::gui::DynamicSliderSpinner<int>(&alpha_right, 0, 255, 1, geom));
  dialog.addControl(_t("Erase Content"), new lpi::gui::DynamicCheckbox(&extra.erase_content));
  dialog.addToolTipToLastRow(_t("Erases content only if alpha = 0"));

  setAffectChannels(false, false, false, true);
}

void Tool_Eraser::setAffectChannels(bool r, bool g, bool b, bool a)
{
  extra.affect_r = r;
  extra.affect_g = g;
  extra.affect_b = b;
  extra.affect_a = a;
}

lpi::ColorRGB Tool_Eraser::getColor(const lpi::IInput& input, Paint& paint)
{
  (void)paint;
  bool left = paint.mouseGrabbed(input, lpi::LMB);
  lpi::ColorRGB result = lpi::RGB_Black;
  result.a = left ? alpha_left : alpha_right;
  return result;
}

void Tool_Eraser::drawBrush(int& x1, int& y1, int x2, int y2, const lpi::ColorRGB& color, Paint& paint)
{
  ColorMode mode = paint.getColorMode();
  
  if(editMask())
  {
    if(mode == MODE_RGBA_8 || mode == MODE_GREY_8) drawBrush(x1, y1, x2, y2, color.a, paint.mask8);
  }
  else
  {
    if(mode == MODE_RGBA_8) drawBrush(x1, y1, x2, y2, color, paint.canvasRGBA8);
    //else if(mode == MODE_GREY_8) drawBrush(x1, y1, x2, y2, colorToGrey(color), paint.canvasGrey8);
  }
}

void Tool_Eraser::drawBrush(int& x1, int& y1, int x2, int y2, const lpi::ColorRGB& color, TextureRGBA8& texture)
{
  int radius = settings.brush.size / 2;
  int ux0 = lpi::clamp<int>(std::min(x1 - radius, x2 - radius) - 1, 0, texture.getU());
  int uy0 = lpi::clamp<int>(std::min(y1 - radius, y2 - radius) - 1, 0, texture.getV());
  int ux1 = lpi::clamp<int>(std::max(x1 + radius, x2 + radius) + 1, 0, texture.getU());
  int uy1 = lpi::clamp<int>(std::max(y1 + radius, y2 + radius) + 1, 0, texture.getV());

  undohelper.add(ux0, uy0, ux1, uy1, texture);

  if(settings.brush.size > 1 || settings.brush.step > 1.0)
  {
    int x = x1, y = y1;
    if(initiate) drawBrushShape(texture.getBuffer(), texture.getU2(), texture.getV2(), x, y, color, settings.brush, extra);
    
    int i = 0;
    while(distance2D(x, y, x2, y2) > (settings.brush.size * settings.brush.step) && inBetween(x1, y1, x, y, x2, y2))
    {
      i++;
      x = int(x1 + i * (settings.brush.size * settings.brush.step) * (x2 - x1) / distance2D(x1, y1, x2, y2));
      y = int(y1 + i * (settings.brush.size * settings.brush.step) * (y2 - y1) / distance2D(x1, y1, x2, y2));
      drawBrushShape(texture.getBuffer(), texture.getU2(), texture.getV2(), x, y, color, settings.brush, extra);
      if(settings.brush.step <= 0.0001) break; //avoid infinite loop
    }
    x1 = x;
    y1 = y;
  }
  else //if brush size is 1, drawLine looks nicer
  {
    drawThinLine(texture.getBuffer(), texture.getU(), texture.getV(), texture.getU2(), x1, y1, x2, y2, color, settings.brush.opacity, extra, false);
    x1 = x2;
    y1 = y2;
  }

  texture.updatePartial(ux0, uy0, ux1, uy1);
}

//BROKEN: doesn't use "extra" brush settings
void Tool_Eraser::drawBrush(int& x1, int& y1, int x2, int y2, int color, TextureGrey8& texture)
{
  int radius = settings.brush.size / 2;
  int ux0 = lpi::clamp<int>(std::min(x1 - radius, x2 - radius) - 1, 0, texture.getU());
  int uy0 = lpi::clamp<int>(std::min(y1 - radius, y2 - radius) - 1, 0, texture.getV());
  int ux1 = lpi::clamp<int>(std::max(x1 + radius, x2 + radius) + 1, 0, texture.getU());
  int uy1 = lpi::clamp<int>(std::max(y1 + radius, y2 + radius) + 1, 0, texture.getV());

  undohelper.add(ux0, uy0, ux1, uy1, texture);

  if(settings.brush.size > 1 || settings.brush.step > 1.0)
  {
    int x = x1, y = y1;
    if(initiate) drawBrushShapeGrey8(texture.getBuffer(), texture.getU2(), texture.getV2(), x, y, color, settings.brush);

    int i = 0;
    while(distance2D(x, y, x2, y2) > (settings.brush.size * settings.brush.step) && inBetween(x1, y1, x, y, x2, y2))
    {
      i++;
      x = int(x1 + i * (settings.brush.size * settings.brush.step) * (x2 - x1) / distance2D(x1, y1, x2, y2));
      y = int(y1 + i * (settings.brush.size * settings.brush.step) * (y2 - y1) / distance2D(x1, y1, x2, y2));
      drawBrushShapeGrey8(texture.getBuffer(), texture.getU2(), texture.getV2(), x, y, color, settings.brush);
      if(settings.brush.step <= 0.0001) break; //avoid infinite loop
    }
    x1 = x;
    y1 = y;
  }
  else //if brush size is 1, drawLine looks nicer
  {
    drawThinLineGrey8(texture.getBuffer(), texture.getU(), texture.getV(), texture.getU2(), x1, y1, x2, y2, color, settings.brush.opacity, false);
    x1 = x2;
    y1 = y2;
  }

  texture.updatePartial(ux0, uy0, ux1, uy1);
}

void Tool_Eraser::handle(const lpi::IInput& input, Paint& paint)
{
  bool left = paint.mouseGrabbed(input, lpi::LMB);
  bool right = paint.mouseGrabbed(input, lpi::RMB);
  
  if(left || right)
  {
    lpi::ColorRGB color = getColor(input, paint);
    int drawx, drawy;
    paint.screenToPixel(drawx, drawy, input);
    
    if(input.keyDown(SDLK_LSHIFT)) initiate = false;
    /*int radiantOldMouseX = oldMouseX;
    int radiantOldMouseY = oldMouseY;*/
    
    if(initiate)
    {
      undohelper.reset();
      oldMouseX = drawx;
      oldMouseY = drawy;
      drawBrush(oldMouseX, oldMouseY, drawx, drawy, color, paint);
      initiate = false;
    }
    if(distance2D(drawx, drawy, oldMouseX, oldMouseY) > (settings.brush.size * settings.brush.step))
    {
      drawBrush(oldMouseX, oldMouseY, drawx, drawy, color, paint);
    }
    
    /*if(input.keyDown(SDLK_LCTRL))
    {
      oldMouseX = radiantOldMouseX;
      oldMouseY = radiantOldMouseY;
    }*/
  }
  else
  {
    if(!initiate) justdone = true;
    initiate = true;
  }
}

void Tool_Eraser::draw(lpi::gui::IGUIDrawer& drawer, Paint& paint)
{
  drawCommonCrosshair(drawer, paint, 0, 0);
  
  int x = drawer.getInput().mouseX();
  int y = drawer.getInput().mouseY();
  int radius = (int)(paint.getZoom() * settings.brush.size / 2);
  int radius1 = radius - 1;
  //lpi::ColorRGB color = settings.leftColor;
  //color.a = 255;
  //lpi::ColorRGB color1 = lpi::gui::getIndicatorColor(color);
  lpi::ColorRGB color = lpi::RGB_Black;
  lpi::ColorRGB color1 = lpi::RGB_White;
  
  int xc, yc; //center of pixel over which mouse is, in screen coordinates
  paint.screenToPixel(xc, yc, x, y);
  paint.pixelToScreen(xc, yc, xc, yc);
  
  if(paint.mouseOver(drawer.getInput()))
  {
    //TODO: instead of two lines, consider one line with pattern with the two colors (but first a drawer for that needs to be made...)
    if(settings.brush.shape == Brush::SHAPE_SQUARE)
    {
      int x2, y2, x0, y0, x1, y1, dummy;
      paint.screenToPixel(x2, y2, x, y);
      paint.pixelToScreen(x0, y0, dummy, dummy, x2 - settings.brush.size / 2, y2 - settings.brush.size / 2);
      paint.pixelToScreen(x1, y1, dummy, dummy, x2 - settings.brush.size / 2 + settings.brush.size, y2 - settings.brush.size / 2 + settings.brush.size);
    
      drawer.drawRectangle(x0, y0, x1, y1, color, false);
      drawer.drawRectangle(x0-1, y0-1, x1+1, y1+1, color1, false);
    }
    else if(settings.brush.shape == Brush::SHAPE_ROUND)
    {
      drawer.drawCircle(xc, yc, radius, color, false);
      drawer.drawCircle(xc, yc, radius1, color1, false);
    }
    else if(settings.brush.shape == Brush::SHAPE_DIAMOND)
    {
      drawer.drawLine(xc, yc - radius,     xc + radius, yc, color);
      drawer.drawLine(xc + radius, yc,     xc, yc + radius, color);
      drawer.drawLine(xc, yc + radius,     xc - radius, yc, color);
      drawer.drawLine(xc - radius, yc,     xc, yc - radius, color);
      drawer.drawLine(xc, yc - radius - 1, xc + radius + 1, yc, color1);
      drawer.drawLine(xc + radius + 1, yc, xc, yc + radius + 1, color1);
      drawer.drawLine(xc, yc + radius + 1, xc - radius - 1, yc, color1);
      drawer.drawLine(xc - radius - 1, yc, xc, yc - radius - 1, color1);
    }
    else
    {
      //unknown???????
      drawer.drawRectangle(xc - radius, yc - radius, xc + radius, yc + radius, lpi::RGB_Red, false);
    }
  }
}

////////////////////////////////////////////////////////////////////////////////

Tool_Pen::Tool_Pen(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom)
: IBrush(settings, geom)
, oldMouseX(0)
, oldMouseY(0)
{
  initiate = true;
  justdone = false;
  addPenSettingsToPage(dialog, geom);
  dialog.addControl(_t("Affect Alpha"), new lpi::gui::DynamicCheckbox(&settings.other.affect_alpha));
}

void Tool_Pen::handle(const lpi::IInput& input, Paint& paint)
{
  bool left = paint.mouseGrabbed(input, lpi::LMB);
  bool right = paint.mouseGrabbed(input, lpi::RMB);
  
  if(left || right)
  {
    lpi::ColorRGB color = left ? settings.leftColor255() : settings.rightColor255();
    int drawx, drawy;
    paint.screenToPixel(drawx, drawy, input);
    
    if(input.keyDown(SDLK_LSHIFT)) initiate = false; //line drawing mode
    
    if(initiate)
    {
      undohelper.reset();
      if(paint.getColorMode() == MODE_RGBA_8)
      {
        undohelper.add(drawx, drawy, drawx + 1, drawy + 1, paint.canvasRGBA8);
        pset(paint.canvasRGBA8.getBuffer(), paint.canvasRGBA8.getU(), paint.canvasRGBA8.getV(), paint.canvasRGBA8.getU2(),
             drawx, drawy, color, settings.brush.opacity, settings.other.affect_alpha);
      }
      else if(paint.getColorMode() == MODE_GREY_8)
      {
        undohelper.add(drawx, drawy, drawx + 1, drawy + 1, paint.canvasGrey8);
        psetGrey8(paint.canvasGrey8.getBuffer(), paint.canvasGrey8.getU(), paint.canvasGrey8.getV(), paint.canvasGrey8.getU2(),
                  drawx, drawy, colorToGrey(color), settings.brush.opacity);
      }
      else if(paint.getColorMode() == MODE_RGBA_32F)
      {
        lpi::ColorRGBd colord(color.r/255.0, color.g/255.0, color.b/255.0, color.a/255.0);
        undohelper.add(drawx, drawy, drawx + 1, drawy + 1, paint.canvasRGBA32F);
        pset(paint.canvasRGBA32F.getBuffer(), paint.canvasRGBA32F.getU(), paint.canvasRGBA32F.getV(), paint.canvasRGBA32F.getU2(),
             drawx, drawy, colord, settings.brush.opacity, settings.other.affect_alpha);
      }
      else if(paint.getColorMode() == MODE_GREY_32F)
      {
        undohelper.add(drawx, drawy, drawx + 1, drawy + 1, paint.canvasGrey32F);
        psetGrey32f(paint.canvasGrey32F.getBuffer(), paint.canvasGrey32F.getU(), paint.canvasGrey32F.getV(), paint.canvasGrey32F.getU2(),
                    drawx, drawy, colorToGreyf(color), settings.brush.opacity);
      }
      initiate = false;
      paint.uploadPartial(drawx, drawy, drawx + 1, drawy + 1);
      
      oldMouseX = drawx;
      oldMouseY = drawy;
      lastTime = input.getSeconds();
    }
    else 
    {
      //normally this could just be "oldMouseX != drawx || oldMouseY != drawy". However, that results in some ugly pixel combinations in the pixel art line stroke. So here instead a system with timer and with a minimul move of > 2 pixels.
      bool a = std::abs(oldMouseX - drawx) + std::abs(oldMouseY - drawy) > 2;
      bool b = input.getSeconds() - lastTime > 0.1;//(int)(oldMouseX != drawx) + (int)(oldMouseY != drawy) == 1;
      if(a || b)
      {
        int ux0 = lpi::clamp<int>(std::min(oldMouseX, drawx) - 1, 0, paint.getU());
        int uy0 = lpi::clamp<int>(std::min(oldMouseY, drawy) - 1, 0, paint.getV());
        int ux1 = lpi::clamp<int>(std::max(oldMouseX, drawx) + 1, 0, paint.getU());
        int uy1 = lpi::clamp<int>(std::max(oldMouseY, drawy) + 1, 0, paint.getV());
        

        //the line goes from the new to the old location (instead of the opposite), with last pixel (= previous location) not included. This is intentional.
        if(paint.getColorMode() == MODE_RGBA_8)
        {
          undohelper.add(ux0, uy0, ux1, uy1, paint.canvasRGBA8);
          drawLine(paint.canvasRGBA8.getBuffer(), paint.canvasRGBA8.getU2(), paint.canvasRGBA8.getV2(), drawx, drawy, oldMouseX, oldMouseY, 1, color, settings.brush.opacity, settings.other.affect_alpha, false);
        }
        else if(paint.getColorMode() == MODE_GREY_8)
        {
          undohelper.add(ux0, uy0, ux1, uy1, paint.canvasGrey8);
          drawThinLineGrey8(paint.canvasGrey8.getBuffer(), paint.canvasGrey8.getU(), paint.canvasGrey8.getV(), paint.canvasGrey8.getU2(), drawx, drawy, oldMouseX, oldMouseY, colorToGrey(color), settings.brush.opacity, false);
        }
        else if(paint.getColorMode() == MODE_RGBA_32F)
        {
          lpi::ColorRGBd colord(color.r/255.0, color.g/255.0, color.b/255.0, color.a/255.0);
          undohelper.add(ux0, uy0, ux1, uy1, paint.canvasRGBA32F);
          drawThinLine(paint.canvasRGBA32F.getBuffer(), paint.canvasRGBA32F.getU(), paint.canvasRGBA32F.getV(), paint.canvasRGBA32F.getU2(), drawx, drawy, oldMouseX, oldMouseY, colord, settings.brush.opacity, ExtraBrushOptions(settings.other.affect_alpha), false);
        }
        else if(paint.getColorMode() == MODE_GREY_32F)
        {
          undohelper.add(ux0, uy0, ux1, uy1, paint.canvasGrey32F);
          drawThinLineGrey32f(paint.canvasGrey32F.getBuffer(), paint.canvasGrey32F.getU(), paint.canvasGrey32F.getV(), paint.canvasGrey32F.getU2(), drawx, drawy, oldMouseX, oldMouseY, colorToGreyf(color), settings.brush.opacity, false);
        }
        
        paint.uploadPartial(ux0, uy0, ux1, uy1);
        
        oldMouseX = drawx;
        oldMouseY = drawy;
        lastTime = input.getSeconds();
      }

    }
    
    //if(!input.keyDown(SDLK_LCTRL))
    //{
    //}
  }
  else
  {
    if(!initiate) justdone = true;
    initiate = true;
  }
}

void Tool_Pen::draw(lpi::gui::IGUIDrawer& drawer, Paint& paint)
{
  drawCommonCrosshair(drawer, paint, 0, 0);
  
  drawPenPixel(drawer, paint);
}

////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////

void Tool_ColorReplacer::ColorReplace::operate(unsigned char& r, unsigned char& g, unsigned char& b, unsigned char& a, double opacity) const
{
  lpi::ColorRGB c(r, g, b, a);
  if(tolcomp(c, from, (int)(settings.tolerance * 255), settings.tolerance_ignores_alpha))
  {
    if(opacity == 1.0)
    {
      r = to.r;
      g = to.g;
      b = to.b;
      a = to.a;
    }
    else
    {
      r = (int)(to.r * opacity + r * (1.0 - opacity));
      g = (int)(to.g * opacity + g * (1.0 - opacity));
      b = (int)(to.b * opacity + b * (1.0 - opacity));
      a = (int)(to.a * opacity + a * (1.0 - opacity));
    }
  }
}

Tool_ColorReplacer::Tool_ColorReplacer(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom)
: IBrush(settings, geom)
, op(*this)
, oldMouseX(0)
, oldMouseY(0)
, tolerance(0.0)
, tolerance_ignores_alpha(false)
{
  initiate = true;
  justdone = false;
  addBrushSettingsToPage(dialog, geom);
  dialog.addControl(_t("Tolerance"), new lpi::gui::DynamicSliderSpinner<double>(&tolerance, 0.0, 1.0, 0.01, geom));
  dialog.addControl(_t("Tol. Ignores A."), new lpi::gui::DynamicCheckbox(&tolerance_ignores_alpha));
  //dialog.addControl(_t("Affect Alpha"), new lpi::gui::DynamicCheckbox(&settings.other.affect_alpha));
}

void Tool_ColorReplacer::drawBrush(int& x1, int& y1, int x2, int y2, Paint& paint)
{
  int radius = settings.brush.size / 2;
  int ux0 = lpi::clamp<int>(std::min(x1 - radius, x2 - radius) - 1, 0, paint.canvasRGBA8.getU());
  int uy0 = lpi::clamp<int>(std::min(y1 - radius, y2 - radius) - 1, 0, paint.canvasRGBA8.getV());
  int ux1 = lpi::clamp<int>(std::max(x1 + radius, x2 + radius) + 1, 0, paint.canvasRGBA8.getU());
  int uy1 = lpi::clamp<int>(std::max(y1 + radius, y2 + radius) + 1, 0, paint.canvasRGBA8.getV());

  undohelper.add(ux0, uy0, ux1, uy1, paint.canvasRGBA8);
  

  brush = settings.brush;
  brush.opacity = 1.0;
  brush.softness = 0.0;

  int x = x1, y = y1;
  if(initiate) drawBrushShape(paint.canvasRGBA8.getBuffer(), paint.canvasRGBA8.getU2(), paint.canvasRGBA8.getV2(), x, y, brush, op);

  int i = 0;
  while(distance2D(x, y, x2, y2) > (settings.brush.size * settings.brush.step) && inBetween(x1, y1, x, y, x2, y2))
  {
    i++;
    x = int(x1 + i * (settings.brush.size * settings.brush.step) * (x2 - x1) / distance2D(x1, y1, x2, y2));
    y = int(y1 + i * (settings.brush.size * settings.brush.step) * (y2 - y1) / distance2D(x1, y1, x2, y2));
    drawBrushShape(paint.canvasRGBA8.getBuffer(), paint.canvasRGBA8.getU2(), paint.canvasRGBA8.getV2(), x, y, brush, op);
    if(settings.brush.step <= 0.0001) break; //avoid infinite loop
  }
  x1 = x;
  y1 = y;

  paint.uploadPartial(ux0, uy0, ux1, uy1);

}

void Tool_ColorReplacer::handle(const lpi::IInput& input, Paint& paint)
{
  bool left = paint.mouseGrabbed(input, lpi::LMB);
  bool right = paint.mouseGrabbed(input, lpi::RMB);

  if(left)
  {
    op.from = settings.rightColor255();
    op.to = settings.leftColor255();
  }
  else if(right)
  {
    op.from = settings.leftColor255();
    op.to = settings.rightColor255();
  }

  if(left || right)
  {
    int drawx, drawy;
    paint.screenToPixel(drawx, drawy, input);

    if(input.keyDown(SDLK_LSHIFT)) initiate = false;
    /*int radiantOldMouseX = oldMouseX;
    int radiantOldMouseY = oldMouseY;*/

    if(initiate)
    {
      undohelper.reset();
      oldMouseX = drawx;
      oldMouseY = drawy;
      drawBrush(oldMouseX, oldMouseY, drawx, drawy, paint);
      initiate = false;
    }
    if(distance2D(drawx, drawy, oldMouseX, oldMouseY) > (settings.brush.size * settings.brush.step))
    {
      drawBrush(oldMouseX, oldMouseY, drawx, drawy, paint);
    }

    /*if(input.keyDown(SDLK_LCTRL))
    {
      oldMouseX = radiantOldMouseX;
      oldMouseY = radiantOldMouseY;
    }*/
  }
  else
  {
    if(!initiate) justdone = true;
    initiate = true;
  }
}

void Tool_ColorReplacer::draw(lpi::gui::IGUIDrawer& drawer, Paint& paint)
{
  drawCommonCrosshair(drawer, paint, 0, 0);
  
  int x = drawer.getInput().mouseX();
  int y = drawer.getInput().mouseY();
  int radius = (int)(paint.getZoom() * settings.brush.size / 2);
  int radius1 = radius - 1;
  //lpi::ColorRGB color = settings.leftColor;
  //color.a = 255;
  //lpi::ColorRGB color1 = lpi::gui::getIndicatorColor(color);
  lpi::ColorRGB color = lpi::RGB_Black;
  lpi::ColorRGB color1 = lpi::RGB_White;

  int xc, yc; //center of pixel over which mouse is, in screen coordinates
  paint.screenToPixel(xc, yc, x, y);
  paint.pixelToScreen(xc, yc, xc, yc);

  if(paint.mouseOver(drawer.getInput()))
  {
    //TODO: instead of two lines, consider one line with pattern with the two colors (but first a drawer for that needs to be made...)
    if(settings.brush.shape == Brush::SHAPE_SQUARE)
    {
      int x2, y2, x0, y0, x1, y1, dummy;
      paint.screenToPixel(x2, y2, x, y);
      paint.pixelToScreen(x0, y0, dummy, dummy, x2 - settings.brush.size / 2, y2 - settings.brush.size / 2);
      paint.pixelToScreen(x1, y1, dummy, dummy, x2 - settings.brush.size / 2 + settings.brush.size, y2 - settings.brush.size / 2 + settings.brush.size);

      drawer.drawRectangle(x0, y0, x1, y1, color, false);
      drawer.drawRectangle(x0-1, y0-1, x1+1, y1+1, color1, false);
    }
    else if(settings.brush.shape == Brush::SHAPE_ROUND)
    {
      drawer.drawCircle(xc, yc, radius, color, false);
      drawer.drawCircle(xc, yc, radius1, color1, false);
    }
    else if(settings.brush.shape == Brush::SHAPE_DIAMOND)
    {
      drawer.drawLine(xc, yc - radius,     xc + radius, yc, color);
      drawer.drawLine(xc + radius, yc,     xc, yc + radius, color);
      drawer.drawLine(xc, yc + radius,     xc - radius, yc, color);
      drawer.drawLine(xc - radius, yc,     xc, yc - radius, color);
      drawer.drawLine(xc, yc - radius - 1, xc + radius + 1, yc, color1);
      drawer.drawLine(xc + radius + 1, yc, xc, yc + radius + 1, color1);
      drawer.drawLine(xc, yc + radius + 1, xc - radius - 1, yc, color1);
      drawer.drawLine(xc - radius - 1, yc, xc, yc - radius - 1, color1);
    }
    else
    {
      //unknown???????
      drawer.drawRectangle(xc - radius, yc - radius, xc + radius, yc + radius, lpi::RGB_Red, false);
    }
  }
}

////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////

Tool_Adjust::Tool_Adjust(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom)
: IBrush(settings, geom)
, op(*this)
, mode(M_LIGHTNESS)
, oldMouseX(0)
, oldMouseY(0)
, adjust(-0.1)
{
  initiate = true;
  justdone = false;
  addBrushSettingsToPage(dialog, geom);

  std::vector<Mode> enums;
  enums.push_back(M_LIGHTNESS); enums.push_back(M_SATURATION); enums.push_back(M_HUE);
  enums.push_back(M_RED); enums.push_back(M_GREEN); enums.push_back(M_BLUE); enums.push_back(M_ALPHA);
  std::vector<std::string> names;
  names.push_back(_t("Lightness")); names.push_back(_t("Saturation")); names.push_back(_t("Hue"));
  names.push_back(_t("Red")); names.push_back(_t("Green")); names.push_back(_t("Blue")); names.push_back(_t("Alpha"));
  dialog.addControl(_t("Mode"), new lpi::gui::DynamicEnum<Mode>(&mode, names, enums, geom));


  dialog.addControl(_t("Adjust"), new lpi::gui::DynamicSliderSpinner<double>(&adjust, -1.0, 1.0, 0.01, geom));
  

}

void Tool_Adjust::Adjust::operate(unsigned char& r, unsigned char& g, unsigned char& b, unsigned char& a, double opacity) const
{
  lpi::ColorRGB c(r, g, b, a);
  
  switch(settings.mode)
  {
    case M_LIGHTNESS:
    {
      lpi::ColorHSL hsl = lpi::RGBtoHSL(c);
      hsl.l += (int)(255 * adjust);
      if(hsl.l < 0) hsl.l = 0;
      if(hsl.l > 255) hsl.l = 255;
      c = lpi::HSLtoRGB(hsl);
      
      /*lpi::ColorHSV hsv = lpi::RGBtoHSV(c);
      hsv.v += (int)(255 * adjust);
      if(hsv.v < 0) hsv.v = 0;
      if(hsv.v > 255) hsv.v = 255;
      c = lpi::HSVtoRGB(hsv);*/
      
      break;
    }
    case M_SATURATION:
    {
      lpi::ColorHSL hsl = lpi::RGBtoHSL(c);
      int origs = hsl.s;
      if(hsl.s > 0)
      {
        hsl.s += (int)(255 * adjust);
        if(hsl.s < 0) hsl.s = 0;
        if(hsl.s > 255) hsl.s = 255;
        if(hsl.s != origs) c = lpi::HSLtoRGB(hsl);
      }

      break;
    }
    case M_HUE:
    {
      lpi::ColorHSL hsl = lpi::RGBtoHSL(c);
      hsl.h += (int)(255 * adjust);
      if(hsl.h < 0) hsl.h += 255;
      if(hsl.h > 255) hsl.h -= 255;
      c = lpi::HSLtoRGB(hsl);
      
      //this block of code prevents the color to darken and become more greyish after applying the brush many times over the same area. The values (+1 and such) are experimentally determinated.
      int s = hsl.s;
      int l = hsl.l;
      hsl = lpi::RGBtoHSL(c);
      hsl.s = lpi::clamp(hsl.s + (s - hsl.s) + 1, 0, 255);
      hsl.l = lpi::clamp(hsl.l + (l - hsl.l) + 1, 0, 255);
      c = lpi::HSLtoRGB(hsl);

      break;
    }
    case M_RED:
    {
      c.r += (int)(255 * adjust);
      if(c.r < 0) c.r = 0;
      if(c.r > 255) c.r = 255;
      break;
    }
    case M_GREEN:
    {
      c.g += (int)(255 * adjust);
      if(c.g < 0) c.g = 0;
      if(c.g > 255) c.g = 255;
      break;
    }
    case M_BLUE:
    {
      c.b += (int)(255 * adjust);
      if(c.b < 0) c.b = 0;
      if(c.b > 255) c.b = 255;
      break;
    }
    case M_ALPHA:
    {
      c.a += (int)(255 * adjust);
      if(c.a < 0) c.a = 0;
      if(c.a > 255) c.a = 255;
      break;
    }
    
  }
  
  
  
  if(opacity == 1.0)
  {
    r = c.r;
    g = c.g;
    b = c.b;
    a = c.a;
  }
  else
  {
    r = (int)(c.r * opacity + r * (1.0 - opacity));
    g = (int)(c.g * opacity + g * (1.0 - opacity));
    b = (int)(c.b * opacity + b * (1.0 - opacity));
    a = (int)(c.a * opacity + a * (1.0 - opacity));
  }
}

void Tool_Adjust::drawBrush(int& x1, int& y1, int x2, int y2, Paint& paint)
{
  int radius = settings.brush.size / 2;
  int ux0 = lpi::clamp<int>(std::min(x1 - radius, x2 - radius) - 1, 0, paint.canvasRGBA8.getU());
  int uy0 = lpi::clamp<int>(std::min(y1 - radius, y2 - radius) - 1, 0, paint.canvasRGBA8.getV());
  int ux1 = lpi::clamp<int>(std::max(x1 + radius, x2 + radius) + 1, 0, paint.canvasRGBA8.getU());
  int uy1 = lpi::clamp<int>(std::max(y1 + radius, y2 + radius) + 1, 0, paint.canvasRGBA8.getV());

  undohelper.add(ux0, uy0, ux1, uy1, paint.canvasRGBA8);
  
  double s = settings.brush.step;
  if(settings.brush.size <= 1 && s < 1) s = 1;
  

  brush = settings.brush;

  int x = x1, y = y1;
  if(initiate) drawBrushShape(paint.canvasRGBA8.getBuffer(), paint.canvasRGBA8.getU2(), paint.canvasRGBA8.getV2(), x, y, brush, op);

  int i = 0;
  while(distance2D(x, y, x2, y2) >= (settings.brush.size * s) && inBetween(x1, y1, x, y, x2, y2))
  {
    i++;
    x = int(x1 + i * (settings.brush.size * s) * (x2 - x1) / distance2D(x1, y1, x2, y2));
    y = int(y1 + i * (settings.brush.size * s) * (y2 - y1) / distance2D(x1, y1, x2, y2));
    drawBrushShape(paint.canvasRGBA8.getBuffer(), paint.canvasRGBA8.getU2(), paint.canvasRGBA8.getV2(), x, y, brush, op);
    if(s <= 0.0001) break; //avoid infinite loop
  }
  x1 = x;
  y1 = y;

  paint.uploadPartial(ux0, uy0, ux1, uy1);

}

void Tool_Adjust::handle(const lpi::IInput& input, Paint& paint)
{
  bool left = paint.mouseGrabbed(input, lpi::LMB);
  bool right = paint.mouseGrabbed(input, lpi::RMB);
  
  if(left)
  {
    op.adjust = adjust;
  }
  else if(right)
  {
    op.adjust = -adjust;
  }

  if(left || right)
  {
    int drawx, drawy;
    paint.screenToPixel(drawx, drawy, input);

    if(input.keyDown(SDLK_LSHIFT)) initiate = false;
    /*int radiantOldMouseX = oldMouseX;
    int radiantOldMouseY = oldMouseY;*/

    if(initiate)
    {
      undohelper.reset();
      oldMouseX = drawx;
      oldMouseY = drawy;
      drawBrush(oldMouseX, oldMouseY, drawx, drawy, paint);
      initiate = false;
    }
    if(distance2D(drawx, drawy, oldMouseX, oldMouseY) > (settings.brush.size * settings.brush.step))
    {
      drawBrush(oldMouseX, oldMouseY, drawx, drawy, paint);
    }

    /*if(input.keyDown(SDLK_LCTRL))
    {
      oldMouseX = radiantOldMouseX;
      oldMouseY = radiantOldMouseY;
    }*/
  }
  else
  {
    if(!initiate) justdone = true;
    initiate = true;
  }
}

void Tool_Adjust::draw(lpi::gui::IGUIDrawer& drawer, Paint& paint)
{
  drawCommonCrosshair(drawer, paint, 0, 0);
  
  int x = drawer.getInput().mouseX();
  int y = drawer.getInput().mouseY();
  int radius = (int)(paint.getZoom() * settings.brush.size / 2);
  int radius1 = radius - 1;
  //lpi::ColorRGB color = settings.leftColor;
  //color.a = 255;
  //lpi::ColorRGB color1 = lpi::gui::getIndicatorColor(color);
  lpi::ColorRGB color = lpi::RGB_Black;
  lpi::ColorRGB color1 = lpi::RGB_White;

  int xc, yc; //center of pixel over which mouse is, in screen coordinates
  paint.screenToPixel(xc, yc, x, y);
  paint.pixelToScreen(xc, yc, xc, yc);

  if(paint.mouseOver(drawer.getInput()))
  {
    //TODO: instead of two lines, consider one line with pattern with the two colors (but first a drawer for that needs to be made...)
    if(settings.brush.shape == Brush::SHAPE_SQUARE)
    {
      int x2, y2, x0, y0, x1, y1, dummy;
      paint.screenToPixel(x2, y2, x, y);
      paint.pixelToScreen(x0, y0, dummy, dummy, x2 - settings.brush.size / 2, y2 - settings.brush.size / 2);
      paint.pixelToScreen(x1, y1, dummy, dummy, x2 - settings.brush.size / 2 + settings.brush.size, y2 - settings.brush.size / 2 + settings.brush.size);

      drawer.drawRectangle(x0, y0, x1, y1, color, false);
      drawer.drawRectangle(x0-1, y0-1, x1+1, y1+1, color1, false);
    }
    else if(settings.brush.shape == Brush::SHAPE_ROUND)
    {
      drawer.drawCircle(xc, yc, radius, color, false);
      drawer.drawCircle(xc, yc, radius1, color1, false);
    }
    else if(settings.brush.shape == Brush::SHAPE_DIAMOND)
    {
      drawer.drawLine(xc, yc - radius,     xc + radius, yc, color);
      drawer.drawLine(xc + radius, yc,     xc, yc + radius, color);
      drawer.drawLine(xc, yc + radius,     xc - radius, yc, color);
      drawer.drawLine(xc - radius, yc,     xc, yc - radius, color);
      drawer.drawLine(xc, yc - radius - 1, xc + radius + 1, yc, color1);
      drawer.drawLine(xc + radius + 1, yc, xc, yc + radius + 1, color1);
      drawer.drawLine(xc, yc + radius + 1, xc - radius - 1, yc, color1);
      drawer.drawLine(xc - radius - 1, yc, xc, yc - radius - 1, color1);
    }
    else
    {
      //unknown???????
      drawer.drawRectangle(xc - radius, yc - radius, xc + radius, yc + radius, lpi::RGB_Red, false);
    }
  }
}



////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////

Tool_PixelBrush::Tool_PixelBrush(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom)
: IBrush(settings, geom)
, oldMouseX(0)
, oldMouseY(0)
, size(8)
{
  initiate = true;
  justdone = false;
  
  
  dialog.addControl(_t("Size"), new lpi::gui::DynamicSliderSpinner<int>(&size, 1, 100, 1, 300, 1, geom));
  dialog.addControl(_t("Affect Alpha"), new lpi::gui::DynamicCheckbox(&settings.other.affect_alpha));
}

void Tool_PixelBrush::handle(const lpi::IInput& input, Paint& paint)
{
  bool left = paint.mouseGrabbed(input, lpi::LMB);
  bool right = paint.mouseGrabbed(input, lpi::RMB);

  if(left || right)
  {
    lpi::ColorRGB color = left ? settings.leftColor255() : settings.rightColor255();
    int drawx, drawy;
    paint.screenToPixel(drawx, drawy, input);

    if(input.keyDown(SDLK_LSHIFT)) initiate = false; //line drawing mode
    
    size_t u = paint.getU();
    size_t v = paint.getV();

    if(initiate)
    {
      int radius = size / 2;
      int ux0 = lpi::clamp<int>(drawx - radius, 0, u);
      int uy0 = lpi::clamp<int>(drawy - radius, 0, v);
      int ux1 = lpi::clamp<int>(drawx - radius + size, 0, u);
      int uy1 = lpi::clamp<int>(drawy - radius + size, 0, v);

      undohelper.reset();
      if(paint.getColorMode() == MODE_RGBA_8)
      {
        undohelper.add(ux0, uy0, ux1, uy1, paint.canvasRGBA8);
        for(int y = uy0; y < uy1; y++)
        for(int x = ux0; x < ux1; x++)
        {
          pset(paint.canvasRGBA8.getBuffer(), paint.canvasRGBA8.getU(), paint.canvasRGBA8.getV(), paint.canvasRGBA8.getU2(),
               x, y, color, 1, settings.other.affect_alpha);
        }
      }
      else if(paint.getColorMode() == MODE_GREY_8)
      {
        undohelper.add(ux0, uy0, ux1, uy1, paint.canvasGrey8);
        for(int y = uy0; y < uy1; y++)
        for(int x = ux0; x < ux1; x++)
        {
          psetGrey8(paint.canvasGrey8.getBuffer(), paint.canvasGrey8.getU(), paint.canvasGrey8.getV(), paint.canvasGrey8.getU2(),
                    x, y, colorToGrey(color), 1);
        }
      }
      else if(paint.getColorMode() == MODE_RGBA_32F)
      {
        undohelper.add(ux0, uy0, ux1, uy1, paint.canvasRGBA32F);
        for(int y = uy0; y < uy1; y++)
        for(int x = ux0; x < ux1; x++)
        {
          pset(paint.canvasRGBA32F.getBuffer(), paint.canvasRGBA32F.getU(), paint.canvasRGBA32F.getV(), paint.canvasRGBA32F.getU2(),
               x, y, lpi::RGBtoRGBd(color), 1, settings.other.affect_alpha);
        }
      }
      else if(paint.getColorMode() == MODE_GREY_32F)
      {
        undohelper.add(ux0, uy0, ux1, uy1, paint.canvasGrey32F);
        for(int y = uy0; y < uy1; y++)
        for(int x = ux0; x < ux1; x++)
        {
          psetGrey32f(paint.canvasGrey32F.getBuffer(), paint.canvasGrey32F.getU(), paint.canvasGrey32F.getV(), paint.canvasGrey32F.getU2(),
                      x, y, colorToGreyf(color), 1);
        }
      }
      initiate = false;
      paint.uploadPartial(ux0, uy0, ux1, uy1);
    }
    else if(oldMouseX != drawx || oldMouseY != drawy)
    {
      int radius = size / 2;
      int ux0 = lpi::clamp<int>(std::min(oldMouseX, drawx) - radius - 1, 0, u);
      int uy0 = lpi::clamp<int>(std::min(oldMouseY, drawy) - radius - 1, 0, v);
      int ux1 = lpi::clamp<int>(std::max(oldMouseX, drawx) - radius + size + 1, 0, u);
      int uy1 = lpi::clamp<int>(std::max(oldMouseY, drawy) - radius + size + 1, 0, v);
      
        //the line goes from the new to the old location (instead of the opposite), with last pixel (= previous location) not included. This is intentional.
      if(paint.getColorMode() == MODE_RGBA_8)
      {
        undohelper.add(ux0, uy0, ux1, uy1, paint.canvasRGBA8);
        unsigned char* buffer = paint.canvasRGBA8.getBuffer();
        size_t u2 = paint.canvasRGBA8.getU2();
        size_t v2 = paint.canvasRGBA8.getV2();
        for(int i = 0; i < size; i++)
        {
          drawLine(buffer, u2, v2, drawx-radius+i           , drawy - radius           , oldMouseX-radius+i           , oldMouseY - radius           , 1, color, 1, settings.other.affect_alpha, true);
          drawLine(buffer, u2, v2, drawx-radius+i           , drawy - radius + size - 1, oldMouseX-radius+i           , oldMouseY - radius + size - 1, 1, color, 1, settings.other.affect_alpha, true);
          drawLine(buffer, u2, v2, drawx - radius           , drawy-radius+i           , oldMouseX - radius           , oldMouseY-radius+i           , 1, color, 1, settings.other.affect_alpha, true);
          drawLine(buffer, u2, v2, drawx - radius + size - 1, drawy-radius+i           , oldMouseX - radius + size - 1, oldMouseY-radius+i           , 1, color, 1, settings.other.affect_alpha, true);
        }
        //the extra lines below are because without this there are pixel artefacts near the side of the image due to the way the bresenham lines are clipped
        if(drawy - radius < 0 && drawy - radius + size > 0)            drawLine(buffer, u2, v2, drawx-radius, 0, drawx-radius+size, 0, 1, color, 1, settings.other.affect_alpha, true);
        if(drawy - radius + size >= (int)v && drawy - radius < (int)v) drawLine(buffer, u2, v2, drawx-radius, v - 1, drawx-radius+size, v - 1, 1, color, 1, settings.other.affect_alpha, true);
        if(drawx - radius < 0 && drawx - radius + size > 0)            drawLine(buffer, u2, v2, 0, drawy-radius, 0, drawy-radius+size, 1, color, 1, settings.other.affect_alpha, true);
        if(drawx - radius + size >= (int)u && drawx - radius < (int)u) drawLine(buffer, u2, v2, u - 1, drawy-radius, u - 1, drawy-radius+size, 1, color, 1, settings.other.affect_alpha, true);
      }
      else if(paint.getColorMode() == MODE_GREY_8)
      {
        int colorGrey = colorToGrey(color);
        undohelper.add(ux0, uy0, ux1, uy1, paint.canvasGrey8);
        unsigned char* buffer = paint.canvasGrey8.getBuffer();
        size_t u2 = paint.canvasGrey8.getU2();
        for(int i = 0; i < size; i++)
        {
          drawThinLineGrey8(buffer, u, v, u2, drawx-radius+i           , drawy - radius           , oldMouseX-radius+i           , oldMouseY - radius           , colorGrey, 1, true);
          drawThinLineGrey8(buffer, u, v, u2, drawx-radius+i           , drawy - radius + size - 1, oldMouseX-radius+i           , oldMouseY - radius + size - 1, colorGrey, 1, true);
          drawThinLineGrey8(buffer, u, v, u2, drawx - radius           , drawy-radius+i           , oldMouseX - radius           , oldMouseY-radius+i           , colorGrey, 1, true);
          drawThinLineGrey8(buffer, u, v, u2, drawx - radius + size - 1, drawy-radius+i           , oldMouseX - radius + size - 1, oldMouseY-radius+i           , colorGrey, 1, true);
        }
        //the extra lines below are because without this there are pixel artefacts near the side of the image due to the way the bresenham lines are clipped
        if(drawy - radius < 0 && drawy - radius + size > 0)            drawThinLineGrey8(buffer, u, v, u2, drawx-radius, 0, drawx-radius+size, 0, colorGrey, 1, true);
        if(drawy - radius + size >= (int)v && drawy - radius < (int)v) drawThinLineGrey8(buffer, u, v, u2, drawx-radius, v - 1, drawx-radius+size, v - 1, colorGrey, 1, true);
        if(drawx - radius < 0 && drawx - radius + size > 0)            drawThinLineGrey8(buffer, u, v, u2, 0, drawy-radius, 0, drawy-radius+size, colorGrey, 1, true);
        if(drawx - radius + size >= (int)u && drawx - radius < (int)u) drawThinLineGrey8(buffer, u, v, u2, u - 1, drawy-radius, u - 1, drawy-radius+size, colorGrey, 1, true);
      }
      else if(paint.getColorMode() == MODE_RGBA_32F)
      {
        lpi::ColorRGBd colorFloat = lpi::RGBtoRGBd(color);
        ExtraBrushOptions dummy;
        undohelper.add(ux0, uy0, ux1, uy1, paint.canvasRGBA32F);
        float* buffer = paint.canvasRGBA32F.getBuffer();
        size_t u2 = paint.canvasRGBA32F.getU2();
        for(int i = 0; i < size; i++)
        {
          drawThinLine(buffer, u, v, u2, drawx-radius+i           , drawy - radius           , oldMouseX-radius+i           , oldMouseY - radius           , colorFloat, 1, dummy, true);
          drawThinLine(buffer, u, v, u2, drawx-radius+i           , drawy - radius + size - 1, oldMouseX-radius+i           , oldMouseY - radius + size - 1, colorFloat, 1, dummy, true);
          drawThinLine(buffer, u, v, u2, drawx - radius           , drawy-radius+i           , oldMouseX - radius           , oldMouseY-radius+i           , colorFloat, 1, dummy, true);
          drawThinLine(buffer, u, v, u2, drawx - radius + size - 1, drawy-radius+i           , oldMouseX - radius + size - 1, oldMouseY-radius+i           , colorFloat, 1, dummy, true);
        }
        //the extra lines below are because without this there are pixel artefacts near the side of the image due to the way the bresenham lines are clipped
        if(drawy - radius < 0 && drawy - radius + size > 0)            drawThinLine(buffer, u, v, u2, drawx-radius, 0, drawx-radius+size, 0, colorFloat, 1, dummy, true);
        if(drawy - radius + size >= (int)v && drawy - radius < (int)v) drawThinLine(buffer, u, v, u2, drawx-radius, v - 1, drawx-radius+size, v - 1, colorFloat, 1, dummy, true);
        if(drawx - radius < 0 && drawx - radius + size > 0)            drawThinLine(buffer, u, v, u2, 0, drawy-radius, 0, drawy-radius+size, colorFloat, 1, dummy, true);
        if(drawx - radius + size >= (int)u && drawx - radius < (int)u) drawThinLine(buffer, u, v, u2, u - 1, drawy-radius, u - 1, drawy-radius+size, colorFloat, 1, dummy, true);
      }
      else if(paint.getColorMode() == MODE_GREY_32F)
      {
        float colorGrey = colorToGreyf(color);
        undohelper.add(ux0, uy0, ux1, uy1, paint.canvasGrey32F);
        float* buffer = paint.canvasGrey32F.getBuffer();
        size_t u2 = paint.canvasGrey32F.getU2();
        for(int i = 0; i < size; i++)
        {
          drawThinLineGrey32f(buffer, u, v, u2, drawx-radius+i           , drawy - radius           , oldMouseX-radius+i           , oldMouseY - radius           , colorGrey, 1, true);
          drawThinLineGrey32f(buffer, u, v, u2, drawx-radius+i           , drawy - radius + size - 1, oldMouseX-radius+i           , oldMouseY - radius + size - 1, colorGrey, 1, true);
          drawThinLineGrey32f(buffer, u, v, u2, drawx - radius           , drawy-radius+i           , oldMouseX - radius           , oldMouseY-radius+i           , colorGrey, 1, true);
          drawThinLineGrey32f(buffer, u, v, u2, drawx - radius + size - 1, drawy-radius+i           , oldMouseX - radius + size - 1, oldMouseY-radius+i           , colorGrey, 1, true);
        }
        //the extra lines below are because without this there are pixel artefacts near the side of the image due to the way the bresenham lines are clipped
        if(drawy - radius < 0 && drawy - radius + size > 0)            drawThinLineGrey32f(buffer, u, v, u2, drawx-radius, 0, drawx-radius+size, 0, colorGrey, 1, true);
        if(drawy - radius + size >= (int)v && drawy - radius < (int)v) drawThinLineGrey32f(buffer, u, v, u2, drawx-radius, v - 1, drawx-radius+size, v - 1, colorGrey, 1, true);
        if(drawx - radius < 0 && drawx - radius + size > 0)            drawThinLineGrey32f(buffer, u, v, u2, 0, drawy-radius, 0, drawy-radius+size, colorGrey, 1, true);
        if(drawx - radius + size >= (int)u && drawx - radius < (int)u) drawThinLineGrey32f(buffer, u, v, u2, u - 1, drawy-radius, u - 1, drawy-radius+size, colorGrey, 1, true);
      }

      paint.uploadPartial(ux0, uy0, ux1, uy1);
    }

    //if(!input.keyDown(SDLK_LCTRL))
    //{
      oldMouseX = drawx;
      oldMouseY = drawy;
    //}
  }
  else
  {
    if(!initiate) justdone = true;
    initiate = true;
  }
}

void Tool_PixelBrush::draw(lpi::gui::IGUIDrawer& drawer, Paint& paint)
{
  drawCommonCrosshair(drawer, paint, 0, 0);
  
  int x = drawer.getInput().mouseX();
  int y = drawer.getInput().mouseY();

  lpi::ColorRGB color = lpi::RGB_Black;
  lpi::ColorRGB color1 = lpi::RGB_White;

  int xc, yc; //center of pixel over which mouse is, in screen coordinates
  paint.screenToPixel(xc, yc, x, y);
  paint.pixelToScreen(xc, yc, xc, yc);

  if(paint.mouseOver(drawer.getInput()))
  {
    int x2, y2, x0, y0, x1, y1, dummy;
    paint.screenToPixel(x2, y2, x, y);
    paint.pixelToScreen(x0, y0, dummy, dummy, x2 - size / 2, y2 - size / 2);
    paint.pixelToScreen(x1, y1, dummy, dummy, x2 - size / 2 + size, y2 - size / 2 + size);

    drawer.drawRectangle(x0, y0, x1, y1, color, false);
    drawer.drawRectangle(x0-1, y0-1, x1+1, y1+1, color1, false);
  }
}

////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////

