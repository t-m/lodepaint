/*
Copyright (c) 2005-2011 Lode Vandevenne
All rights reserved.

This file is part of Lode's Programming Interface.

Lode's Programming Interface is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Lode's Programming Interface is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Lode's Programming Interface.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
Unicode utilities:
The standard unicode and UTF-8 related algorithms.
*/

#pragma once

#include <string>
#include <vector>

namespace lpi
{

int unicodeToUtf8(char chars[6], int unicode);

//pos is octet pos, not codepoint pos
int utf8ToUnicode(size_t pos, const std::string& s);

//convert position between amount of octets or amount of utr-8 codepoints (codepoints, not characters!)
size_t utf8octet2char(size_t octet, const std::string& s);
size_t utf8char2octet(size_t c, const std::string& s);

size_t utf8length(const std::string& s);

template<typename T>
void utf8increment(T& pos, const std::string& s)
{
  unsigned char c = s[pos];
  if(c < 128) pos++;
  else if(c < 192)
  {
    //it's an in between char. Jump to next start octet.
    pos++;
    while(pos < s.length() && (unsigned char)s[pos] >= 128 && (unsigned char)s[pos] < 224) pos++;
  }
  else if(c < 224) pos += 2;
  else if(c < 240) pos += 3;
  else if(c < 248) pos += 4;
  else if(c < 252) pos += 5;
  else if(c < 254) pos += 6;
  else pos++; //avoid infinite loop on these invalid characters
}

//converts a string of chars to a UTF-8 string where each unicode value matches the original 0-255 byte value.
void ansiToUtf8(std::string& out, const std::string& in);

//converts unicode to ansi, unicode values higher than 255 become a question mark
void utf8ToAnsi(std::string& out, const std::string& in);

//converts UTF-16 (little endian) to UTF-8. Input is given as octets.
void utf16leToUtf8(std::string& out, const unsigned char* in, size_t size);

//output given as octets instead of wstring due to platform dependency of wchar_t.
void utf8ToUtf16le(std::vector<unsigned char>& out, const std::string& in);

/*
utf8string is a wrapper around a normal string that has a few functions in common
with std::string for convenience, but instead of operating on octets, it operates
on the unicode codepoints, so that the length is the number of unicode characters
encoded in the UTF-8 string, and positions refer to that instead of the octet.
This makes it less efficient than a regular string of course.
*/
class utf8string
{
  std::string& s; //the wrapped std::string
  mutable size_t l; //length
  mutable bool lengthok; //is length up to date?
  mutable size_t lastglyph;
  mutable size_t lastoctet;
  
  void resynclength();
  void resyncpos();
  
  public:
  
  utf8string(std::string& s);
  
  //call this if the std::string changed by outside means
  void resync();
  
  int operator[](size_t pos) const;
  
  size_t length() const;
  size_t size() const;
  void erase(size_t pos = 0, size_t n = (size_t)(-1));
  void insert(size_t pos1, size_t n, int c);
};

} // namespace lpi
