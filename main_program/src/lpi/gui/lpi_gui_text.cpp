/*
Copyright (c) 2005-2009 Lode Vandevenne
All rights reserved.

This file is part of Lode's Programming Interface.

Lode's Programming Interface is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Lode's Programming Interface is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Lode's Programming Interface.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "lpi_gui_text.h"

#include "../lodepng.h"
#include "../lpi_base64.h"
#include "../os/lpi_clipboard.h"
#include "../gio/lpi_draw2d.h"
#include "../lpi_file.h"
#include "../lpi_parse.h"
#include "../lpi_unicode.h"
#include "../lpi_unicode_data.h"
#include "../lpi_xml.h"
#include <SDL/SDL.h> //this is for the SDLK_... key codes

#include <iostream>
#include <cstdlib>

namespace lpi
{
namespace gui
{

////////////////////////////////////////////////////////////////////////////////
//GUIINPUTLINE//////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

/*
The InputLine Class Functions
InputLine allows the user to enter a single line of text.

How to use it:
  You can give text color styles with color1, color2, style
  When the draw() function is called, it is drawn and handled, i.e. user unicode key presses are detected, ...
  It is only handled if "active" is true, otherwise it's only drawn and it ignores user input
  Fetch the text with getText or by simply using inputLine.text
  Check if the user pressed enter, clicked on it with mouse, .... with the check() function
*/

//the default constructor
InputLine::InputLine()
: last_draw_time(0)
, sel0(0)
, sel1(0)
, color(0, 0, 0, 255)
, utext(text)
{
  x0 = 0;
  y0 = 0;
  l = 0;
  type = 0;
  title = "";
  control_active = 0;
  entering_done = false;
  
  text = "";
  cursor = 0;
  entered = 0;
}

void InputLine::make(int x, int y, unsigned long l, int type)
{
  this->x0 = x;
  this->y0 = y;
  this->l = l;
  this->type = type;
  this->title = title;
  
  this->text = "";
  cursor = 0;
  entered = 0;
  
  this->setSizeY( /*markup.getHeight()*/8); //TODO use IDrawer2D to determine this text size
  this->setSizeX((utf8length(title) + l) * /*markup.getWidth()*/8);
  
  this->enabled = 1;
}

bool InputLine::enteringDone() const
{
  if(control_active)
  {
    entering_done = true;
    return false;
  }
  else if(entering_done)
  {
    entering_done = false;
    return true;
  }
  else return false;
}

//this function draws the line, and makes sure it gets handled too
void InputLine::drawImpl(IGUIDrawer& drawer) const
{
  static const int FONTSIZE = 8; //TODO: use drawer to find out cursor position in text and such

  if(type == 0) drawer.drawGUIPartText(GPT_TEXTINPUTLINE_TITLE, title, x0, y0, x1, y1);
  
  int titlew/*, titleh*/;
  //drawer.calcTextRectSize(titlew, titleh, title, titleFont);
  titlew = FONTSIZE * utf8length(title);
  //titleh = FONTSIZE;

  int inputX = x0 + titlew;
  int inputY = (y0 + y1) / 2;
  if(type == 0) drawer.drawGUIPartText(GPT_TEXTINPUTLINE, text, inputX, y0, x1, y1, GPM_Default, &color);
  else if(type == 1) //password
  {
    std::string s;
    size_t p = 0;
    size_t length = utext.length();
    while(p < length && p < l)
    {
      s += "*";
      p++;
    }
    drawer.drawGUIPartText(GPT_TEXTINPUTLINE, s, inputX, y0, x1, y1, GPM_Default, &color);
  }
  else if(type == 2)
  {
    std::string s = valtostr(getInteger());
    drawer.drawGUIPartText(GPT_TEXTINPUTLINE, s, inputX, y0, x1, y1, GPM_Default, &color);
  }
  
  //draw the cursor if active
  if(control_active && (int((draw_time - last_draw_time) * 2.0) % 2 == 0 || mouseDown(drawer.getInput())))
  {
    int cursorXDraw = inputX + cursor * FONTSIZE;
    drawer.drawLine(cursorXDraw - 1, inputY - FONTSIZE / 2 - 1, cursorXDraw - 1, inputY + FONTSIZE / 2 + 1, /*font.color*/RGB_White);
    drawer.drawLine(cursorXDraw, inputY - FONTSIZE / 2 - 1, cursorXDraw, inputY + FONTSIZE / 2 + 1, /*font.color*/RGB_Black);
  }
  
  //draw selection, if any
  if(sel0 != sel1)
  {
    int start = inputX + sel0 * FONTSIZE;
    int end = inputX + sel1 * FONTSIZE;
    drawer.drawRectangle(start, inputY - FONTSIZE / 2, end, inputY + FONTSIZE / 2, ColorRGB(128, 128, 255, 96), true);
  }
}

bool InputLine::enter()
{
  bool result = false;
  if(entered && text.length() > 0 && enabled) result = true;
  entered = 0;
  return result;
}

void InputLine::deleteSelectedText()
{
  int s0 = sel0, s1 = sel1;
  if(s1 < s0) std::swap(s0, s1);
  
  if(s0 < 0) s0 = 0;
  if(s1 < 0) s1 = 0;
  int length = utext.length();
  if(s0 > length) s0 = length;
  if(s1 > length) s1 = length;
  
  if(utext.length() > 0) utext.erase(s0, s1 - s0);
  
  sel0 = sel1 = cursor = s0;
}

void InputLine::activate(bool i_active)
{
  control_active = i_active;
}


void InputLine::handleImpl(const IInput& input) //both check if you pressed enter, and also check letter keys pressed, backspace, etc...
{
  if(mouseGrabbed(input))
  {
    cursor = mouseToCursor(input.mouseX());
    sel1 = mouseToCursor(input.mouseX());
  }
  if(mouseJustDownHere(input))
  {
    sel0 = mouseToCursor(input.mouseX());
    last_draw_time = draw_time;
  }
  
  bool shift = input.keyDown(SDLK_LSHIFT) || input.keyDown(SDLK_RSHIFT);
  bool ctrl = input.keyDown(SDLK_LCTRL) || input.keyDown(SDLK_RCTRL);
  
  autoActivate(input, auto_activate_mouse_state, control_active);
  if(control_active)
  {
    if(input.keyDown(SDLK_RETURN) || input.keyDown(SDLK_KP_ENTER))
    {
      control_active = false;
      entered = true;
    }
    draw_time = input.getSeconds();
    
    size_t textlength = utext.length();
    if(cursor >= textlength) cursor = textlength;

    if(!ctrl)
    {
      int ascii = input.unicodeKey(0.5, 0.025);
      if(ascii)
      {
        last_draw_time = draw_time;
        switch(ascii)
        {
          case 8: //backspace
            //COMMENTED OUT!! For same reason as case 127, except that this one actually worked in both linux and windows, but you know, the fact that 127 doesn't reliably work everywhere doesn't give the feeling that backspase will. Handled with input.keyPressedTime instead.
            /*if(sel0 == sel1 && cursor > 0) text.erase(--cursor, 1);
            else if(sel0 != sel1) deleteSelectedText();*/
            break;
          case 13: //enter
            //COMMENTED OUT!!!! UNICODEKEY DOES NOT RELIABLY SAY THAT THIS KEY IS DOWN, IT MAY MISS THE UP EVENT. SO FOR THIS ENTER FEATURE, I USE THE SIMPLE KEY INPUT WAY INSTEAD.
            //So, do NOTHING with enter
            /*entered = 1;
            control_active = false;*/
            break;
          case 127: //delete
            //COMMENTED OUT!!! works with this in linux, but not in windows, so I check with keyPressedTime instead below
            /*if(sel0 == sel1 && cursor <= text.length()) text.erase(cursor, 1);
            else if(sel0 != sel1) deleteSelectedText();*/
            break;
          //a few certainly not meant to be printed ones
          case 0:
            break;
          case 10:
            break;
          default:
          {
            if(sel0 != sel1) deleteSelectedText();
            size_t textlength = utext.length();
            if(textlength < l) utext.insert(cursor, 1, ascii);
            if(textlength < l || cursor == l - 1) cursor++;
            break;
          }
        }
      }
    }

    if(ctrl)
    {
      if(input.keyPressed(SDLK_v))
      {
        std::string pasteText;
        if(getClipboardString(pasteText))
        {
          utf8string upaste(pasteText);
          if(sel0 != sel1) deleteSelectedText();
          for(size_t i = 0; i < upaste.length(); i++)
          {
            int c = upaste[i];
            if(c == 10 || c == 13) break; //no newlines in single-line text
            if(utext.length() < l) utext.insert(cursor, 1, c);
            if(utext.length() < l || cursor == l - 1) cursor++;
          }
          last_draw_time = draw_time;
        }
      }
      bool doCopy = input.keyPressed(SDLK_c);
      bool doCut = input.keyPressed(SDLK_x);
      if(doCopy || doCut)
      {
        if(sel0 != sel1)
        {
          std::string copyText;
          if ((int)sel1 < (int)sel0) std::swap(sel0, sel1);
          int s0 = utf8char2octet(sel0, text);
          int s1 = utf8char2octet(sel1, text);
          for(int i = (int)s0; i < (int)s1; i++)
          {
            if(i < 0) { i = -1; continue; }
            if(i >= (int)text.length()) break;
            copyText.push_back(text[i]);
          }
          if(!copyText.empty()) setClipboardString(copyText);
          if(doCut) deleteSelectedText();
        }
      }
      if(input.keyPressed(SDLK_a) || input.keyPressed(SDLK_q)) //the q is for the azerty problem in Windows
      {
        last_draw_time = draw_time;
        selectAll();
      }

    }
    
    if(input.keyPressed(SDLK_HOME))
    {
      last_draw_time = draw_time;
      if(shift)
      {
        if(sel0 == sel1)
        {
          sel0 = cursor;
          sel1 = 0;
        }
        else
        {
          sel1 = 0;
        }
      }
      else selectNone();
      cursor = 0;
    }
    if(input.keyPressed(SDLK_END))
    {
      last_draw_time = draw_time;
      unsigned long pos = utext.length();
      if(shift)
      {
        if(sel0 == sel1)
        {
          sel0 = cursor;
          sel1 = pos;
        }
        else
        {
          sel1 = pos;
        }
      }
      else selectNone();
      cursor = pos;
    }
    if(input.keyPressedTime(SDLK_LEFT, 0.5, 0.025))
    {
      int num = 1;
      if(ctrl && cursor <= utext.length() && cursor > 0)
      {
        bool alpha = lpi::isAlphaNumeric(utext[cursor - 1]);
        int c = cursor - 2;
        while(c >= 0 && lpi::isAlphaNumeric(utext[c]) == alpha) c--;
        num = cursor - c - 1;
      }
      last_draw_time = draw_time;
      if(shift)
      {
        if(sel0 == sel1) sel0 = sel1 = cursor;
        cursor = ((int)cursor > num) ? cursor - num : 0;
        sel1 = cursor;
      }
      else
      {
        if(sel0 == sel1)
          cursor = ((int)cursor > num) ? cursor - num : 0;
        else
          cursor = sel1 = sel0 = (sel1 > sel0 ? sel0 : sel1);
      }
    }
    if(input.keyPressedTime(SDLK_RIGHT, 0.5, 0.025))
    {
      int num = 1;
      if(ctrl && cursor < utext.length() /*&& cursor >= 0*/)
      {
        bool alpha = lpi::isAlphaNumeric(utext[cursor]);
        int c = cursor + 1;
        while(c < (int)utext.length() && lpi::isAlphaNumeric(utext[c]) == alpha) c++;
        num = c - cursor;
      }
      last_draw_time = draw_time;
      if(shift)
      {
        if(sel0 == sel1) sel0 = sel1 = cursor;
        cursor = cursor + num <= utext.length() ? cursor + num : utext.length();
        sel1 = cursor;
      }
      else
      {
        if(sel0 == sel1)
          cursor = cursor + num <= utext.length() ? cursor + num : utext.length();
        else
          cursor = sel0 = sel1 = (sel1 < sel0 ? sel0 : sel1);
      }
    }
    if(input.keyPressedTime(SDLK_DELETE, 0.5, 0.025))
    {
      last_draw_time = draw_time;
      if(sel0 == sel1 && cursor <= utext.length()) utext.erase(cursor, 1);
      else if(sel0 != sel1) deleteSelectedText();
    }
    if(input.keyPressedTime(SDLK_BACKSPACE, 0.5, 0.025))
    {
      last_draw_time = draw_time;
      if(sel0 == sel1 && cursor > 0) utext.erase(--cursor, 1);
      else if(sel0 != sel1) deleteSelectedText();
    }
  }
  else selectNone();
  
  if(mouseDoubleClicked(input))
  {
    last_draw_time = draw_time;
    selectAll();
  }
}

void InputLine::selectAll()
{
  sel0 = 0;
  sel1 = utext.length();
}

void InputLine::selectNone()
{
  sel0 = 0;
  sel1 = 0;
}

int InputLine::mouseToCursor(int mouseX) const
{
  static const int FONTSIZE = 8; //TODO: use drawer to find out cursor position in text and such
  int relMouse = mouseX - x0 + FONTSIZE / 2;
  relMouse -= title.length() * FONTSIZE;
  
  if(relMouse < 0) return 0;
  
  int result = relMouse / FONTSIZE;
  
  int length = utext.length();
  
  if(result < length) return result;
  else return length;
}

void InputLine::clear()
{
  setText("");
  cursor = 0;
}

void InputLine::setText(const std::string& i_text)
{
  text = i_text;
  utext.resync();
}

const std::string& InputLine::getText() const
{
  return text;
}

void InputLine::setTitle(const std::string& i_title)
{
  static const int FONTSIZE = 8; //TODO: use drawer to find out cursor position in text and such
  int size_diff = i_title.length() - title.length();
  this->title = i_title;
  growX1(size_diff * FONTSIZE);
}

const std::string& InputLine::getTitle() const
{
  return title;
}

int InputLine::getInteger() const
{
  return std::atoi(text.c_str());
}

int InputLine::getKeyboardFocus() const
{
  if(control_active) return KM_TEXT;
  else return 0;
}

////////////////////////////////////////////////////////////////////////////////

static const int SPINNERW = 16;

ISpinner::ISpinner()
: changed(false)
{
  setEnabled(true);
  
  addSubElement(&line, Sticky(0.0,0, 0.0,0, 1.0,-SPINNERW, 1.0,0));
  
  line.make(0, 0, 10);
}

void ISpinner::drawImpl(IGUIDrawer& drawer) const
{
  drawer.drawRectangle(getX0(), getY0(), getX1(), getY1(), RGB_White, true);
  drawer.drawRectangle(getX0(), getY0(), getX1(), getY1(), RGB_Grey, false);
  line.draw(drawer);
  drawer.drawGUIPart(GP_SPINNER_UP, x1 - SPINNERW, y0, x1, (y0 + y1) / 2);
  drawer.drawGUIPart(GP_SPINNER_DOWN, x1 - SPINNERW, (y0 + y1) / 2, x1, y1);
}

void ISpinner::handleImpl(const IInput& input)
{
  line.handle(input);
  if(mouseOver(input) && input.mouseButtonDownTimed(LMB))
  {
    int x = input.mouseX();
    int y = input.mouseY();
    if(x >= x1 - SPINNERW && x < x1)
    {
      if(y < (y0 + y1) / 2) increment();
      else decrement();
      changed = true;
    }
  }
}

bool ISpinner::hasChanged()
{
  bool result = changed;
  changed = false;
  return result || line.enteringDone();
}

bool ISpinner::isControlActive() const
{
  return line.isControlActive();
}

void ISpinner::activate(bool i_active)
{
  line.activate(i_active);
}

void ISpinner::selectAll()
{
  line.selectAll();
}

void ISpinner::selectNone()
{
  line.selectNone();
}

//////////////////////////////////////////////////////////////////////////////////
////MultiLineText class
//////////////////////////////////////////////////////////////////////////////////

void MultiLineText::update(int maxWidth, const IGUIDrawer& drawer,  const Font& font)
{
  //For now only fixed-width text is supported...
  int cw0, ch0, cw1, ch1;
  drawer.calcTextRectSize(cw0, ch0, "m", font);
  drawer.calcTextRectSize(cw1, ch1, "mm", font);
  int cw = cw1 - cw0; //fixed char width
  
  lines.clear();
  
  lines.push_back(0);
  
  int x = 0;
  //bool word = false; //false = whitespace, true = anything else
  bool whitespace = true;
  size_t wordstart = 0;
  for(size_t i = 0; i < text.length(); i++)
  {
    char c = text[i];
    if(c == ' ')
    {
      whitespace = true;
      x += cw;
    }
    else if(c == 10 || c == 13)
    {
      if(c == 10 && i + 1 < text.length() && text[i + 1] == 13) i++;
      x = 0;
      lines.push_back(i + 1);
    }
    else
    {
      if(whitespace)
      {
        whitespace = false;
        wordstart = i;
      }
      
      x += cw;
      if(x > maxWidth)
      {
        x = cw * (i - wordstart);
        lines.push_back(wordstart);
      }
    }
  }
  
  lines.push_back(text.length());
}

void MultiLineText::charToCursor(size_t& cline, size_t& column, size_t pos) const
{
  for(size_t i = 0; i + 1 < lines.size(); i++)
  {
    if(getLineEnd(i) > pos)
    {
      cline = i;
      column = pos - getLineStart(i);
      break;
    }
  }
}

size_t MultiLineText::cursorToChar(size_t cline, size_t column) const
{
  if(column > getLineLength(cline)) column = getLineLength(cline);
  return getLineStart(cline) + column;
}

size_t MultiLineText::getNumLines() const
{
  if(lines.empty()) return 0;
  return lines.size() - 1;
}

size_t MultiLineText::getLineStart(size_t i) const
{
  return lines[i];
}

size_t MultiLineText::getLineEnd(size_t i) const
{
  return lines[i + 1];
}

size_t MultiLineText::getLineLength(size_t i) const
{
  return getLineEnd(i) - getLineStart(i);
}




////////////////////////////////////////////////////////////////////////////////
////GUIINPUTBOX/////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

//the default constructor
InputBox::InputBox()
: last_draw_time(0)
, sel0(0)
, sel1(0)
, text(mtext.text)
{
  this->x0 = 0;
  this->y0 = 0;
  this->control_active = 0;
  this->entering_done = false;
  
  cursor = 0;
  
  this->setSizeY(64);
  this->setSizeX(128);
  
  this->enabled = 1;
}

bool InputBox::enteringDone() const
{
  if(control_active)
  {
    entering_done = true;
    return false;
  }
  else if(entering_done)
  {
    entering_done = false;
    return true;
  }
  else return false;
}

//this function draws the line, and makes sure it gets handled too
void InputBox::drawImpl(IGUIDrawer& drawer) const
{
  drawer.drawRectangle(x0, y0, x1, y1, RGB_White, true);
  
  int cw0, ch0, cw1, ch1;
  drawer.calcTextRectSize(cw0, ch0, "m");
  drawer.calcTextRectSize(cw1, ch1, "mm");
  int cw = cw1 - cw0, ch = ch0; //fixed char width and height
  
  drawer.drawGUIPartText(GPT_TEXTINPUTBOX, text, x0, y0, x1, y1);
  
  //draw the cursor if active
  if(control_active && (int((draw_time - last_draw_time) * 2.0) % 2 == 0 || mouseDown(drawer.getInput())))
  {
    int cursorXDraw = x0 + cursor * cw - 1;
    drawer.drawLine(cursorXDraw, y0, cursorXDraw, y0 + ch, RGB_Black);
  }
  
  //draw selection, if any
  if(sel0 != sel1)
  {
    int start = x0 + sel0 * cw;
    int end = x0 + sel1 * cw;
    drawer.drawRectangle(start, y0, end, y0 + ch, ColorRGB(128, 128, 255, 96), true);
  }
}

void InputBox::deleteSelectedText()
{
  int s0 = sel0, s1 = sel1;
  if(s1 < s0) std::swap(s0, s1);
  
  if(s0 < 0) s0 = 0;
  if(s1 < 0) s1 = 0;
  if(s0 > (int)text.length()) s0 = text.length();
  if(s1 > (int)text.length()) s1 = text.length();
  
  if(text.length() > 0) text.erase(s0, s1 - s0);
  
  sel0 = sel1 = cursor = s0;
}

void InputBox::activate(bool i_active)
{
  control_active = i_active;
}

void InputBox::insertCharacter(int c)
{
  if(sel0 != sel1) deleteSelectedText();
  if(c < 128)
  {
    text.insert(cursor, 1, c);
    cursor++;
  }
  else
  {
    char chars[6];
    int num = unicodeToUtf8(chars, c);
    for(int i = 0; i < num; i++)
    {
      text.insert(cursor, 1, chars[i]);
      cursor++;
    }
  }
}

void InputBox::handleImpl(const IInput& input) //both check if you pressed enter, and also check letter keys pressed, backspace, etc...
{
  if(mouseGrabbed(input))
  {
    cursor = mouseToCursor(input);
    sel1 = mouseToCursor(input);
  }
  if(mouseJustDownHere(input))
  {
    sel0 = mouseToCursor(input);
    last_draw_time = draw_time;
  }
  
  bool shift = input.keyDown(SDLK_LSHIFT) || input.keyDown(SDLK_RSHIFT);
  bool ctrl = input.keyDown(SDLK_LCTRL) || input.keyDown(SDLK_RCTRL);
  
  autoActivate(input, auto_activate_mouse_state, control_active);
  if(control_active)
  {
    draw_time = input.getSeconds();
    
    if(cursor >= text.length()) cursor = text.length();

    if(!ctrl)
    {
      int ascii = input.unicodeKey(0.5, 0.025);
      if(ascii)
      {
        last_draw_time = draw_time;
        switch(ascii)
        {
          case 8:
          case 10:
          case 13:
          case 127:
            //backspace, enter, delete, ...: not working here due to unicode system. Handled as keys separately below.
            break;
          //a few certainly not meant to be printed ones
          case 0:
            break;
          default:
            insertCharacter(ascii);
            break;
        }
      }
    }

    if(ctrl)
    {
      if(input.keyPressed(SDLK_v))
      {
        std::string pasteText;
        if(getClipboardString(pasteText))
        {
          if(sel0 != sel1) deleteSelectedText();
          for(size_t i = 0; i < pasteText.length(); i++)
          {
            char c = pasteText[i];
            text.insert(cursor, 1, c);
            cursor++;
          }
          last_draw_time = draw_time;
        }
      }
      bool doCopy = input.keyPressed(SDLK_c);
      bool doCut = input.keyPressed(SDLK_x);
      if(doCopy || doCut)
      {
        if(sel0 != sel1)
        {
          std::string copyText;
          if ((int)sel1 < (int)sel0)
          {
            int tmp = sel0;
            sel0 = sel1;
            sel1 = tmp;
          }
          for(int i = (int)sel0; i < (int)sel1; i++)
          {
            if(i < 0) { i = -1; continue; }
            if(i >= (int)text.length()) break;
            copyText.push_back(text[i]);
          }
          if(!copyText.empty()) setClipboardString(copyText);
          if(doCut) deleteSelectedText();
        }
      }
      if(input.keyPressed(SDLK_a) || input.keyPressed(SDLK_q)) //the q is for the azerty problem in Windows
      {
        last_draw_time = draw_time;
        selectAll();
      }

    }
    
    if(input.keyPressed(SDLK_HOME))
    {
      last_draw_time = draw_time;
      if(shift)
      {
        if(sel0 == sel1)
        {
          sel0 = cursor;
          sel1 = 0;
        }
        else
        {
          sel1 = 0;
        }
      }
      else selectNone();
      cursor = 0;
    }
    if(input.keyPressed(SDLK_END))
    {
      last_draw_time = draw_time;
      unsigned long pos = 0;
      while(text[pos] != 0 && pos < text.length()) pos++;
      if(shift)
      {
        if(sel0 == sel1)
        {
          sel0 = cursor;
          sel1 = pos;
        }
        else
        {
          sel1 = pos;
        }
      }
      else selectNone();
      cursor = pos;
    }
    if(input.keyPressedTime(SDLK_LEFT, 0.5, 0.025))
    {
      int num = 1;
      if(ctrl && cursor <= text.length() && cursor > 0)
      {
        bool alpha = lpi::isAlphaNumeric(text[cursor - 1]);
        int c = cursor - 2;
        while(c >= 0 && lpi::isAlphaNumeric(text[c]) == alpha) c--;
        num = cursor - c - 1;
      }
      last_draw_time = draw_time;
      if(shift)
      {
        if(sel0 == sel1) sel0 = sel1 = cursor;
        cursor = ((int)cursor > num) ? cursor - num : 0;
        sel1 = cursor;
      }
      else
      {
        if(sel0 == sel1)
          cursor = ((int)cursor > num) ? cursor - num : 0;
        else
          cursor = sel1 = sel0 = (sel1 > sel0 ? sel0 : sel1);
      }
    }
    if(input.keyPressedTime(SDLK_RIGHT, 0.5, 0.025))
    {
      int num = 1;
      if(ctrl && cursor < text.length() /*&& cursor >= 0*/)
      {
        bool alpha = lpi::isAlphaNumeric(text[cursor]);
        int c = cursor + 1;
        while(c < (int)text.length() && lpi::isAlphaNumeric(text[c]) == alpha) c++;
        num = c - cursor;
      }
      last_draw_time = draw_time;
      if(shift)
      {
        if(sel0 == sel1) sel0 = sel1 = cursor;
        cursor = cursor + num <= text.length() ? cursor + num : text.length();
        sel1 = cursor;
      }
      else
      {
        if(sel0 == sel1)
          cursor = cursor + num <= text.length() ? cursor + num : text.length();
        else
          cursor = sel0 = sel1 = (sel1 < sel0 ? sel0 : sel1);
      }
    }
    if(input.keyPressedTime(SDLK_DELETE, 0.5, 0.025))
    {
      last_draw_time = draw_time;
      if(sel0 == sel1 && cursor <= text.length()) text.erase(cursor, 1);
      else if(sel0 != sel1) deleteSelectedText();
    }
    if(input.keyPressedTime(SDLK_BACKSPACE, 0.5, 0.025))
    {
      last_draw_time = draw_time;
      if(sel0 == sel1 && cursor > 0) text.erase(--cursor, 1);
      else if(sel0 != sel1) deleteSelectedText();
    }
    if(input.keyPressedTime(SDLK_RETURN, 0.5, 0.025) || input.keyPressedTime(SDLK_KP_ENTER, 0.5, 0.025))
    {
      insertCharacter(10);
    }
  }
  else selectNone();
  
  if(mouseDoubleClicked(input))
  {
    last_draw_time = draw_time;
    selectAll();
  }
}

void InputBox::selectAll()
{
  sel0 = 0;
  sel1 = text.length();
}

void InputBox::selectNone()
{
  sel0 = 0;
  sel1 = 0;
}

int InputBox::mouseToCursor(const IInput& input) const
{
  if(mtext.getNumLines() == 0) return 0;
  
  int mouseX = input.mouseX();
  int mouseY = input.mouseY();
  
  //int cw0, ch0, cw1, ch1;
  //input.getDrawer().calcTextRectSize(cw0, ch0, "m", font);
  //input.getDrawer().calcTextRectSize(cw1, ch1, "mm", font);
  //int cw = cw1 - cw0, ch = ch0; //fixed char width and height
  //TODO: need drawer in input...
  int cw = 8, ch = 8;
  
  int relMouseX = mouseX - x0 + cw / 2;
  int relMouseY = mouseY - y0 + ch;
  
  int cx = relMouseX / cw;
  int cy = relMouseY / ch;
  
  if(cx < 0) cx = 0;
  if(cy < 0) cy = 0;
  if(cy >= (int)mtext.getNumLines()) cy = mtext.getNumLines() - 1;
  
  return mtext.cursorToChar(cx, cy);
}

void InputBox::clear()
{
  setText("");
  cursor = 0;
}

void InputBox::setText(const std::string& i_text)
{
  this->text = i_text;
}

const std::string& InputBox::getText() const
{
  return text;
}

int InputBox::getKeyboardFocus() const
{
  if(control_active) return KM_TEXT;
  else return 0;
}










} //namespace gui
} //namespace lpi

////////////////////////////////////////////////////////////////////////////////

