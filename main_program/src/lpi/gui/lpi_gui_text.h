/*
Copyright (c) 2005-2009 Lode Vandevenne
All rights reserved.

This file is part of Lode's Programming Interface.

Lode's Programming Interface is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Lode's Programming Interface is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Lode's Programming Interface.  If not, see <http://www.gnu.org/licenses/>.
*/


/*
text related GUI elements for lpi_gui

NOTE: some of these elements may currently be broken due to refactoring and will be fixed later
TODO: fix possible broken elements
*/

#pragma once

#include "lpi_gui.h"
#include "../lpi_parse.h"
#include "../lpi_unicode.h"

#include <iostream>

namespace lpi
{
namespace gui
{

class InputLine : public Element //input text line
{
  private:
    //unsigned long titleLength;
    unsigned long cursor; //position of the cursor (0 = before first char)
    bool entered; //after you pressed enter!
    double draw_time; //for drawing the blinking cursor
    double last_draw_time; //time when last done something
    MouseState auto_activate_mouse_state;
    bool control_active;
    mutable bool entering_done;
    size_t sel0; //selection start
    size_t sel1; //selection end
    ColorRGB color;
    
  protected:
    unsigned long l; //max length
    
    /*
    types:
    0: normal text line
    1: password (displays * instead of text)
    2: integer number
    */
    int type;
    std::string title;
    std::string text;
    utf8string utext;
    
    int mouseToCursor(int mouseX) const; //absolute mouse position to cursor position
    
  public:
        
    InputLine();
    void make(int x, int y, unsigned long l, int type = 0);
    
    virtual void drawImpl(IGUIDrawer& drawer) const;
    virtual void handleImpl(const IInput& input);

    void setText(const std::string& i_text);
    const std::string& getText() const;
    
    void setTitle(const std::string& i_title);
    const std::string& getTitle() const;
    
    bool isControlActive() const { return control_active; } //i.e., the cursor is blinking and it's listening to keyboard input
    
    bool enteringDone() const; //returns true if the user has been changing (or not) the text but now is done editing because the control is no longer active for the first time since then
   
    void clear();

    bool enter(); //returns true if the user entered text, i.e. if there's text in it, and the user presses enter, and it's active
    int getInteger() const; //returns any integer number that may be in the string you typed
    
    void selectAll();
    void selectNone();
    void deleteSelectedText();
    
    void activate(bool i_active = true);
    
    virtual int getKeyboardFocus() const;
    
    void setColor(const ColorRGB& color) { this->color = color; }
};

class ISpinner : public ElementComposite
{
  protected:
    InputLine line;
    bool changed;
    
  protected:
    virtual void increment() = 0;
    virtual void decrement() = 0;
    
  public:
    ISpinner();
    virtual void drawImpl(IGUIDrawer& drawer) const;
    virtual void handleImpl(const IInput& input);
    
    bool hasChanged();
    
    bool isControlActive() const;
    void activate(bool i_active = true);
    void selectAll();
    void selectNone();

};

template<typename T>
class SpinnerNumerical : public ISpinner
{
  protected:
  
    T step;
    bool hasmin;
    T minval;
    bool hasmax;
    T maxval;
  
  protected:
  
    virtual void increment()
    {
      T newval = getValue() + step;
      if(hasmax && newval > maxval) newval = maxval;
      setValue(newval);
    }
  
    virtual void decrement()
    {
      T newval = getValue() - step;
      if(hasmin && newval < minval) newval = minval;
      setValue(newval);
    }
  
  public:
  
    SpinnerNumerical(T step=(T)1, bool hasmin=false, T minval=(T)0, bool hasmax=false, T maxval=(T)1)
    : step(step)
    , hasmin(hasmin)
    , minval(minval)
    , hasmax(hasmax)
    , maxval(maxval)
    {
    }
  
    T getValue()
    {
      return strtoval<T>(this->line.getText());
    }

    void setValue(T value)
    {
      this->line.setText(valtostr<T>(value));
    }
};

/*
MultiLineText
text divided over multiple lines, and functions 
to let it create the lines in different ways, such as (including, but not limited to)

-use ascii 10 chars of the string for new lines
-break off between letters in limited rectangle
-break off between words in limited rectangle

the multiple line text is represented by an array of integers (line) containing the positions of the first letter of each new line

all functions that need to draw text this way HAVE to use this class from now on
*/
class MultiLineText
{
  private:
    std::vector<size_t> lines; //Line N starts at line[N] and ends at line[N+1]-1. Newline chars are part of line before them. The first line is line 0.
  public:
    std::string text;
    
    void update(int maxWidth, const IGUIDrawer& drawer, const Font& font = FONT_Default); //drawer is for calculating text size
    
    //cursor 0 = before first character. Cursor text.length() = after last character.
    void charToCursor(size_t& cline, size_t& column, size_t pos) const;
    size_t cursorToChar(size_t cline, size_t column) const;
    
    //may only be called after update was called since the last text change
    size_t getNumLines() const;
    size_t getLineStart(size_t i) const;
    size_t getLineEnd(size_t i) const;
    size_t getLineLength(size_t i) const;
};



class InputBox : public Element //input text box
{
  private:
    //unsigned long titleLength;
    unsigned long cursor; //position of the cursor (0 = before first char)
    double draw_time; //for drawing the blinking cursor
    double last_draw_time; //time when last done something
    MouseState auto_activate_mouse_state;
    bool control_active;
    mutable bool entering_done;
    size_t sel0; //selection start
    size_t sel1; //selection end
    
  protected:
    
    MultiLineText mtext;
    std::string& text;
    
    int mouseToCursor(const IInput& input) const; //absolute mouse position to cursor position
    
  public:
        
    InputBox();
    
    virtual void drawImpl(IGUIDrawer& drawer) const;
    virtual void handleImpl(const IInput& input);

    void setText(const std::string& i_text);
    const std::string& getText() const;
    
    void setTitle(const std::string& i_title);
    const std::string& getTitle() const;
    
    bool isControlActive() const { return control_active; } //i.e., the cursor is blinking and it's listening to keyboard input
    
    bool enteringDone() const; //returns true if the user has been changing (or not) the text but now is done editing because the control is no longer active for the first time since then
   
    void clear();

    void selectAll();
    void selectNone();
    void deleteSelectedText();
    void insertCharacter(int c); //unicode code
    
    void activate(bool i_active = true);
    
    virtual int getKeyboardFocus() const;
};

} //namespace gui
} //namespace lpi

