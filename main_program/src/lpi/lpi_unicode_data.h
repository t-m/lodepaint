/*
Copyright (c) 2005-2011 Lode Vandevenne
All rights reserved.

This file is part of Lode's Programming Interface.

Lode's Programming Interface is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Lode's Programming Interface is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Lode's Programming Interface.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <string>
#include <vector>

namespace lpi
{


//is this unicode codepoint a combining character, that is composed to the previous character instead of taking space itself?
bool isCombining(int unicode);
//these do not cover the full unicode range. Only useful for some accent with latin letter combinations.
int getLatinAccent(int c);
int getLatinLetter(int c);

int unicodeToCp437(int code);
int cp437ToUnicode(int code);


} // namespace lpi
