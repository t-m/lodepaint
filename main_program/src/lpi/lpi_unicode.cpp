/*
Copyright (c) 2005-2011 Lode Vandevenne
All rights reserved.

This file is part of Lode's Programming Interface.

Lode's Programming Interface is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Lode's Programming Interface is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Lode's Programming Interface.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "lpi_unicode.h"

#include <iostream>

namespace lpi
{



int unicodeToUtf8(char chars[6], int unicode)
{
  if(unicode <= 0x7f)
  {
    chars[0] = unicode;
    return 1;
  }
  else if(unicode <= 0x7ff)
  {
    chars[0] = 0xc0 | (unicode >> 6);
    chars[1] = 0x80 | (unicode & 0x3f);
    return 2;
  }
  else if(unicode <= 0xffff)
  {
    chars[0] = 0xe0 | (unicode >> 12);
    chars[1] = 0x80 | ((unicode >> 6) & 0x3f);
    chars[2] = 0x80 | (unicode & 0x3f);
    return 3;
  }
  else if(unicode <= 0x1fffff)
  {
    chars[0] = 0xf0 | (unicode >> 18);
    chars[1] = 0x80 | ((unicode >> 12) & 0x3f);
    chars[2] = 0x80 | ((unicode >> 6) & 0x3f);
    chars[3] = 0x80 | (unicode & 0x3f);
    return 4;
  }
  else if(unicode <= 0x3ffffff)
  {
    chars[0] = 0xf8 | (unicode >> 24);
    chars[1] = 0x80 | ((unicode >> 18) & 0x3f);
    chars[2] = 0x80 | ((unicode >> 12) & 0x3f);
    chars[3] = 0x80 | ((unicode >> 6) & 0x3f);
    chars[4] = 0x80 | (unicode & 0x3f);
    return 4;
  }
  else if(unicode <= 0x7fffffff)
  {
    chars[0] = 0xfc | (unicode >> 30);
    chars[1] = 0x80 | ((unicode >> 24) & 0x3f);
    chars[2] = 0x80 | ((unicode >> 18) & 0x3f);
    chars[3] = 0x80 | ((unicode >> 12) & 0x3f);
    chars[4] = 0x80 | ((unicode >> 6) & 0x3f);
    chars[5] = 0x80 | (unicode & 0x3f);
    return 4;
  }
  else return 0; //invalid
}

/*
0xxxxxxx: 0-127
10xxxxxx: 128-191
110xxxxx: 192-223
1110xxxx: 224-239
11110xxx: 240-247
111110xx: 248-251
1111110x: 252-253
*/

int utf8ToUnicode(size_t pos, const std::string& s)
{
  unsigned char c0 = s[pos];
  if(c0 < 128) return c0;
  else if(c0 < 192)
  {
    //we're in a byte the middle of a character...
    return '?';
  }
  else if(c0 < 224)
  {
    unsigned char c1 = pos + 1 < s.length() ? s[pos + 1] : 0;
    return ((int)(c0 & 0x1f) << 6) + (int)(c1 & 0x3f);
  }
  else if(c0 < 240)
  {
    unsigned char c1 = pos + 1 < s.length() ? s[pos + 1] : 0;
    unsigned char c2 = pos + 2 < s.length() ? s[pos + 2] : 0;
    return ((int)(c0 & 0x0f) << 12) + ((int)(c1 & 0x3f) << 6) + (int)(c2 & 0x3f);
  }
  else if(c0 < 248)
  {
    unsigned char c1 = pos + 1 < s.length() ? s[pos + 1] : 0;
    unsigned char c2 = pos + 2 < s.length() ? s[pos + 2] : 0;
    unsigned char c3 = pos + 3 < s.length() ? s[pos + 3] : 0;
    return ((int)(c0 & 0x07) << 18) + ((int)(c1 & 0x3f) << 12) + ((int)(c2 & 0x3f) << 6) + (int)(c3 & 0x3f);
  }
  else if(c0 < 252)
  {
    pos += 4;
    return '?'; //TODO;
  }
  else if(c0 < 254)
  {
    pos += 5;
    return '?'; //TODO;
  }
  else return '?'; //invalid characters 254 and 255
}

int unicodeToUtf16le(char chars[4], int unicode)
{
  if(unicode < 0xffff)
  {
    chars[0] = unicode & 0xff;
    chars[1] = (unicode >> 8) & 0xff;
    return 2; //1 wide char = 2 octets used
  }
  else
  {
    unicode -= 0x10000;
    chars[0] = (unicode >> 10) & 0xff;
    chars[1] = (0xd800 + ((unicode >> 18) & 0x3)) & 0xff;
    chars[2] = unicode & 0xff;
    chars[3] = (0xdc00 + ((unicode >> 8) & 0x3)) & 0xff;
    return 4; //2 wide chars = 4 octets used
  }
}

int utf16leToUnicode(size_t pos, const unsigned char* s)
{
  unsigned short c0 = s[pos] + 256 * s[pos + 1];
  if(c0 >= 0xd800 && c0 <= 0xdfff)
  {
    unsigned short c1 = s[pos + 2] + 256 * s[pos + 3];
    return ((c0 & 0x3ff) << 10) + (c1 & 0x3ff) + 0x10000;
  }
  else return c0;
}


size_t utf8octet2char(size_t octet, const std::string& s)
{
  size_t result = 0;
  for(size_t i = 0; i < octet; i++)
  {
    unsigned char c = s[i];

    if(c < 128) ;
    else if(c < 224) i++;
    else if(c < 240) i += 2;
    else if(c < 248) i += 3;
    else if(c < 252) i += 4;
    else i += 5;
    
    result++;
  }
  return result;
}

size_t utf8char2octet(size_t c, const std::string& s)
{
  size_t result = 0;
  for(size_t i = 0; i < c && result < s.size(); i++)
  {
    unsigned char c = s[result];

    if(c < 128) result++;
    else if(c < 224) result += 2;
    else if(c < 240) result += 3;
    else if(c < 248) result += 4;
    else if(c < 252) result += 5;
    else result += 6;
  }
  return result;
}

size_t utf8length(const std::string& s)
{
  size_t result = 0;
  for(size_t i = 0; i < s.size(); i++)
  {
    unsigned char c = s[i];

    if(c < 128) ;
    else if(c < 192)
    {
      while(i < s.length() && (unsigned char)s[i] >= 128 && (unsigned char)s[i] < 224) i++;
    }
    else if(c < 224) i++;
    else if(c < 240) i += 2;
    else if(c < 248) i += 3;
    else if(c < 252) i += 4;
    else i += 5;
    
    result++;
  }
  return result;
}

////////////////////////////////////////////////////////////////////////////////

void ansiToUtf8(std::string& out, const std::string& in)
{
  for(size_t i = 0; i < in.size(); i++)
  {
    unsigned char c = in[i];
    if(c < 128) out += c;
    else
    {
      out += 0xc0 | ((c >> 6) & 0x03);
      out += 0x80 | (c & 0x3f);
    }
  }
}

void utf8ToAnsi(std::string& out, const std::string& in)
{
  size_t pos = 0;
  while(pos < in.size())
  {
    int c = utf8ToUnicode(pos, in);
    utf8increment(pos, in);
    if(c <= 255) out += (char)(c);
    else out += '?';
  }
}

template<typename T>
void utf16leincrement(T& pos, const unsigned char* s)
{
  unsigned short c = s[pos] + 256 * s[pos + 1];
  if(c >= 0xd800 && c <= 0xdfff) pos += 4;
  else pos += 2;
}

void utf16leToUtf8(std::string& out, const unsigned char* in, size_t size)
{
  //int unicodeToUtf16le(char chars[4], int unicode)
  //int utf16leToUnicode(size_t pos, const unsigned char* s)
  size_t pos = 0;
  char c[6];
  while(pos + 1 < size)
  {
    int unicode = utf16leToUnicode(pos, in);
    int num = unicodeToUtf8(c, unicode);
    for(int i = 0; i < num; i++) out += c[i];
    utf16leincrement(pos, in);
  }
}

void utf8ToUtf16le(std::vector<unsigned char>& out, const std::string& in)
{
  size_t pos = 0;
  char c[4];
  while(pos < in.size())
  {
    int unicode = utf8ToUnicode(pos, in);
    int num = unicodeToUtf16le(c, unicode);
    for(int i = 0; i < num; i++) out.push_back(c[i]);
    utf8increment(pos, in);
  }
}

////////////////////////////////////////////////////////////////////////////////


utf8string::utf8string(std::string& s)
: s(s)
{
  resync();
}


void utf8string::resynclength()
{
  lengthok = false;
}

void utf8string::resyncpos()
{
  lastglyph = 0;
  lastoctet = 0;
}

void utf8string::resync()
{
  resynclength();
  resyncpos();
}

int utf8string::operator[](size_t pos) const
{
  if(pos >= lastglyph)
  {
    while(lastglyph < pos)
    {
      utf8increment(lastoctet, s);
      lastglyph++;
    }
    return utf8ToUnicode(lastoctet, s);
  }
  else
  {
    size_t o = utf8char2octet(pos, s);
    lastglyph = pos;
    lastoctet = o;
    return utf8ToUnicode(lastoctet, s);
  }
}

size_t utf8string::length() const
{
  if(!lengthok)
  {
    l = utf8length(s);
    lengthok = true;
  }
  return l;
}

size_t utf8string::size() const
{
  return length();
}

void utf8string::erase(size_t pos, size_t n)
{
  size_t b = utf8char2octet(pos, s);
  if(n == (size_t)(-1))
  {
    l = pos;
    s.erase(b, n);
  }
  else
  {
    size_t e = utf8char2octet(pos + n, s);
    s.erase(b, e - b);
    l -= n;
  }

  if(pos < lastglyph) resyncpos();
}

void utf8string::insert(size_t pos1, size_t n, int c)
{
  char chars[6];
  int num = unicodeToUtf8(chars, c);
  size_t b = utf8char2octet(pos1, s); //byte position

  for(size_t i = 0; i < n; i++)
  {
    for(int j = 0; j < num; j++)
    {
      s.insert(b, 1, chars[j]);
      b++;
    }
    
    l++; //update length too
  }
  
  if(pos1 < lastglyph) resyncpos();
}


} // namespace lpi
