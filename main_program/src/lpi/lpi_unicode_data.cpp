/*
Copyright (c) 2005-2011 Lode Vandevenne
All rights reserved.

This file is part of Lode's Programming Interface.

Lode's Programming Interface is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Lode's Programming Interface is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Lode's Programming Interface.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "lpi_unicode_data.h"

#include <iostream>
#include <map>

namespace lpi
{

int getLatinAccent(int c)
{
  if(c < 0xc0) return 0;
  else if(c < 0x100) //Latin-1 Supplement
  {
    switch(c)
    {
      case 0xc0:
      case 0xc8:
      case 0xcc:
      case 0xd2:
      case 0xd9:
      case 0xe0:
      case 0xe8:
      case 0xec:
      case 0xf2:
      case 0xf9: return 0x300; //grave
      
      case 0xc1:
      case 0xc9:
      case 0xcd:
      case 0xd3:
      case 0xda:
      case 0xdd:
      case 0xe1:
      case 0xe9:
      case 0xed:
      case 0xf3:
      case 0xfa:
      case 0xfd: return 0x301; //accute
      
      case 0xc2:
      case 0xca:
      case 0xce:
      case 0xd4:
      case 0xdb:
      case 0xe2:
      case 0xea:
      case 0xee:
      case 0xf4:
      case 0xfb: return 0x302; //circumflex
      
      case 0xc3:
      case 0xd1:
      case 0xd5:
      case 0xe3:
      case 0xf1:
      case 0xf5: return 0x303; //tilde
      
      case 0xc4:
      case 0xcb:
      case 0xcf:
      case 0xd6:
      case 0xdc:
      case 0xe4:
      case 0xeb:
      case 0xef:
      case 0xf6:
      case 0xfc:
      case 0xff: return 0x308; //diaeresis
      
      case 0xc5:
      case 0xe5: return 0x30a; //ring
      
      default: return 0;
    }
  }
  else if(c < 0x180) //Latin extended-A
  {
    switch(c)
    {
      case 0x100: return 0x304;
      case 0x101: return 0x304;
      case 0x102: return 0x306;
      case 0x103: return 0x306;
      case 0x106: return 0x301;
      case 0x107: return 0x301;
      case 0x108: return 0x302;
      case 0x109: return 0x302;
      case 0x10a: return 0x307;
      case 0x10b: return 0x307;
      case 0x10c: return 0x30c;
      case 0x10d: return 0x30c;
      case 0x10e: return 0x30c;
      case 0x10f: return 0x30c; //not 100% correct, it's a caron, but is displayed differently in print
      
      case 0x112: return 0x304;
      case 0x113: return 0x304;
      case 0x114: return 0x306;
      case 0x115: return 0x306;
      case 0x116: return 0x307;
      case 0x117: return 0x307;
      case 0x11a: return 0x30c;
      case 0x11b: return 0x30c;
      case 0x11c: return 0x302;
      case 0x11d: return 0x302;
      case 0x11e: return 0x306;
      case 0x11f: return 0x306;

      case 0x120: return 0x307;
      case 0x121: return 0x307;
      case 0x123: return 0x301;
      case 0x124: return 0x302;
      case 0x125: return 0x302;
      case 0x128: return 0x303;
      case 0x129: return 0x303;
      case 0x12a: return 0x304;
      case 0x12b: return 0x304;
      case 0x12c: return 0x306;
      case 0x12d: return 0x306;

      case 0x130: return 0x307;
      case 0x134: return 0x302;
      case 0x135: return 0x302;
      case 0x139: return 0x301;
      case 0x13a: return 0x301;
      case 0x13d: return 0x30c;
      case 0x13e: return 0x30c;
      
      case 0x143: return 0x301;
      case 0x144: return 0x301;
      case 0x147: return 0x30c;
      case 0x148: return 0x30c;
      case 0x14c: return 0x304;
      case 0x14d: return 0x304;
      case 0x14e: return 0x306;
      case 0x14f: return 0x306;

      case 0x150: return 0x303;
      case 0x151: return 0x303;
      case 0x154: return 0x301;
      case 0x155: return 0x301;
      case 0x158: return 0x30c;
      case 0x159: return 0x30c;
      case 0x15a: return 0x301;
      case 0x15b: return 0x301;
      case 0x15c: return 0x302;
      case 0x15d: return 0x302;

      case 0x160: return 0x30c;
      case 0x161: return 0x30c;
      case 0x164: return 0x30c;
      case 0x165: return 0x30c;
      case 0x168: return 0x303;
      case 0x169: return 0x303;
      case 0x16a: return 0x304;
      case 0x16b: return 0x304;
      case 0x16c: return 0x306;
      case 0x16d: return 0x306;
      case 0x16e: return 0x30a;
      case 0x16f: return 0x30a;

      case 0x170: return 0x303;
      case 0x171: return 0x303;
      case 0x174: return 0x302;
      case 0x175: return 0x302;
      case 0x176: return 0x302;
      case 0x177: return 0x302;
      case 0x178: return 0x308;
      case 0x179: return 0x301;
      case 0x17a: return 0x301;
      case 0x17b: return 0x307;
      case 0x17c: return 0x307;
      case 0x17d: return 0x30c;
      case 0x17e: return 0x30c;

      default: return 0;
    }
  }
  else if(c <= 0x24f) //Latin extended-B
  {
    //Not all Latin extended-B characters are implemented here. Not at all.
    switch(c)
    {
      case 0x1fa: return 0x30a; //TODO: This should actually be BOTH ring and accute!
      case 0x1fb: return 0x30a; //TODO: This should actually be BOTH ring and accute!
      case 0x1fc: return 0x301;
      case 0x1fd: return 0x301;
      case 0x1fe: return 0x301; //Latin Capital Letter O with stroke and acute
      case 0x1ff: return 0x301; //Latin Small Letter O with stroke and acute
      default: return 0;
    }
  }
  else if(c >= 0x1e00 && c <= 0x1eff) //Latin Extended Additional
  {
    //Not all Latin Extended Additional characters are implemented here. Not at all.
    switch(c)
    {
      case 0x1e80: return 0x300;
      case 0x1e81: return 0x300;
      case 0x1e82: return 0x301;
      case 0x1e83: return 0x301;
      case 0x1e84: return 0x308;
      case 0x1e85: return 0x308;
      case 0x1ef2: return 0x300;
      case 0x1ef3: return 0x300;
      default: return 0;
    }
  }
  else if(c >= 0x374 && c <= 0x3ff) //Greek
  {
    switch(c)
    {
      //0x384 - greek accute accent (drawn more to the left for capital letters)
      //0x385 - greek dieresis with accute accent (a bit like ^ but not totally, and not drawn to the left)
      case 0x386: return 0x384;
      case 0x388: return 0x384;
      case 0x389: return 0x384;
      case 0x38a: return 0x384;
      case 0x38c: return 0x384;
      case 0x38e: return 0x384;
      case 0x38f: return 0x384;
      case 0x390: return 0x385;

      case 0x3aa: return 0x308;
      case 0x3ab: return 0x308;
      case 0x3ac: return 0x384;
      case 0x3ad: return 0x384;
      case 0x3ae: return 0x384;
      case 0x3af: return 0x384;
      case 0x3b0: return 0x385;

      case 0x3ca: return 0x308;
      case 0x3cb: return 0x308;
      case 0x3cc: return 0x384;
      case 0x3cd: return 0x384;
      case 0x3ce: return 0x384;
      default: return 0;
    }
  }
  else if(c >= 0x400 && c <= 0x4ff) //Cyrillic
  {
    switch(c)
    {
      case 0x400: return 0x300;
      case 0x401: return 0x308;
      case 0x403: return 0x301;
      case 0x407: return 0x308;
      case 0x40c: return 0x301;
      case 0x40d: return 0x300;
      case 0x40e: return 0x306;
      case 0x419: return 0x306;
      case 0x450: return 0x300;
      case 0x451: return 0x308;
      case 0x453: return 0x301;
      case 0x457: return 0x308;
      case 0x45c: return 0x301;
      case 0x45d: return 0x300;
      case 0x45e: return 0x306;
      case 0x439: return 0x306;
      default: return 0;
    }
  }
  
  return 0; //rest is unsupported for now
}

int getLatinLetter(int c)
{
  if(c < 0xc0) return 0;
  else if(c < 0x100) //Latin-1 Supplement
  {
    if(c >= 0xc0 && c <= 0xc5) return 'A';
    if(c >= 0xc8 && c <= 0xcb) return 'E';
    if(c >= 0xcc && c <= 0xcf) return 'I';
    if(c == 0xd1) return 'N';
    if(c >= 0xd2 && c <= 0xd6) return 'O';
    if(c >= 0xd9 && c <= 0xdc) return 'U';
    if(c == 0xdd) return 'Y';
    
    if(c >= 0xe0 && c <= 0xe5) return 'a';
    if(c >= 0xe8 && c <= 0xeb) return 'e';
    if(c >= 0xec && c <= 0xef) return 'i';
    if(c == 0xf1) return 'n';
    if(c >= 0xf2 && c <= 0xf6) return 'o';
    if(c >= 0xf9 && c <= 0xfc) return 'u';
    if(c == 0xfd || c == 0xff) return 'y';
    return 0;
  }
  else if(c < 0x180) //Latin extended-A
  {
    switch(c)
    {
      case 0x100: return 'A';
      case 0x101: return 'a';
      case 0x102: return 'A';
      case 0x103: return 'a';
      case 0x106: return 'C';
      case 0x107: return 'c';
      case 0x108: return 'C';
      case 0x109: return 'c';
      case 0x10a: return 'C';
      case 0x10b: return 'c';
      case 0x10c: return 'C';
      case 0x10d: return 'c';
      case 0x10e: return 'D';
      case 0x10f: return 'd';

      case 0x112: return 'E';
      case 0x113: return 'e';
      case 0x114: return 'E';
      case 0x115: return 'e';
      case 0x116: return 'E';
      case 0x117: return 'e';
      case 0x11a: return 'E';
      case 0x11b: return 'e';
      case 0x11c: return 'G';
      case 0x11d: return 'g';
      case 0x11e: return 'G';
      case 0x11f: return 'g';

      case 0x120: return 'G';
      case 0x121: return 'g';
      case 0x123: return 'g';
      case 0x124: return 'H';
      case 0x125: return 'h';
      case 0x128: return 'I';
      case 0x129: return 'i';
      case 0x12a: return 'I';
      case 0x12b: return 'i';
      case 0x12c: return 'I';
      case 0x12d: return 'i';

      case 0x130: return 'I';
      case 0x134: return 'J';
      case 0x135: return 'j';
      case 0x139: return 'L';
      case 0x13a: return 'l';
      case 0x13d: return 'L';
      case 0x13e: return 'l';

      case 0x143: return 'N';
      case 0x144: return 'n';
      case 0x147: return 'N';
      case 0x148: return 'n';
      case 0x14c: return 'O';
      case 0x14d: return 'o';
      case 0x14e: return 'O';
      case 0x14f: return 'o';

      case 0x150: return 'O';
      case 0x151: return 'o';
      case 0x154: return 'R';
      case 0x155: return 'R';
      case 0x158: return 'R';
      case 0x159: return 'r';
      case 0x15a: return 'S';
      case 0x15b: return 's';
      case 0x15c: return 'S';
      case 0x15d: return 's';

      case 0x160: return 'S';
      case 0x161: return 's';
      case 0x164: return 'T';
      case 0x165: return 't';
      case 0x168: return 'U';
      case 0x169: return 'u';
      case 0x16a: return 'U';
      case 0x16b: return 'u';
      case 0x16c: return 'U';
      case 0x16d: return 'u';
      case 0x16e: return 'U';
      case 0x16f: return 'u';

      case 0x170: return 'U';
      case 0x171: return 'u';
      case 0x174: return 'W';
      case 0x175: return 'w';
      case 0x176: return 'Y';
      case 0x177: return 'y';
      case 0x178: return 'Y';
      case 0x179: return 'Z';
      case 0x17a: return 'z';
      case 0x17b: return 'Z';
      case 0x17c: return 'z';
      case 0x17d: return 'Z';
      case 0x17e: return 'z';

      default: return 0;
    }
  }
  else if(c <= 0x24f) //Latin extended-B
  {
    //Not all Latin extended-B characters are implemented here. Not at all.
    switch(c)
    {
      case 0x1fa: return 'A';
      case 0x1fb: return 'a';
      case 0x1fc: return 0xc6;
      case 0x1fd: return 0xe6;
      case 0x1fe: return 0xd8; //Latin Capital Letter O with stroke and acute
      case 0x1ff: return 0xf8; //Latin Small Letter O with stroke and acute
      default: return 0;
    }
  }
  else if(c >= 0x1e00 && c <= 0x1eff) //Latin Extended Additional
  {
    //Not all Latin Extended Additional characters are implemented here. Not at all.
    switch(c)
    {
      case 0x1e80: return 'W';
      case 0x1e81: return 'w';
      case 0x1e82: return 'W';
      case 0x1e83: return 'w';
      case 0x1e84: return 'W';
      case 0x1e85: return 'w';
      case 0x1ef2: return 'Y';
      case 0x1ef3: return 'y';
      default: return 0;
    }
  }
  else if(c >= 0x374 && c <= 0x3ff) //Greek
  {
    switch(c)
    {
      case 0x386: return 0x391;
      case 0x388: return 0x395;
      case 0x389: return 0x397;
      case 0x38a: return 0x399;
      case 0x38c: return 0x39f;
      case 0x38e: return 0x3a5;
      case 0x38f: return 0x3a9;
      case 0x390: return 0x3b9;

      case 0x3aa: return 0x399;
      case 0x3ab: return 0x3a5;
      case 0x3ac: return 0x3b1;
      case 0x3ad: return 0x3b5;
      case 0x3ae: return 0x3b7;
      case 0x3af: return 0x3b9;
      case 0x3b0: return 0x3c5;

      case 0x3ca: return 0x3b9;
      case 0x3cb: return 0x3c5;
      case 0x3cc: return 0x3bf;
      case 0x3cd: return 0x3c5;
      case 0x3ce: return 0x3c9;
      default: return 0;
    }
  }
  else if(c >= 0x400 && c <= 0x4ff) //Cyrillic
  {
    switch(c)
    {
      case 0x400: return 'E';
      case 0x401: return 'E';
      case 0x403: return 0x413;
      case 0x407: return 'I';
      case 0x40c: return 0x41a;
      case 0x40d: return 0x418;
      case 0x40e: return 0x423;
      case 0x419: return 0x418;
      case 0x450: return 'e';
      case 0x451: return 'e';
      case 0x453: return 0x433;
      case 0x457: return 'i';
      case 0x45c: return 0x43a;
      case 0x45d: return 0x438;
      case 0x45e: return 'y';
      case 0x439: return 0x438;
      default: return 0;
    }
  }
  
  return 0; //rest is unsupported for now
}


bool isCombining(int unicode)
{
  //TODO: this implementation tries to cover the complete unicode 6.0 range, but I think some single characters outside of complete ranges are missing.
  if(unicode < 0x300) return false;
  if(unicode >= 0x300 && unicode <= 0x36f) return true;
  if(unicode >= 0x1dc0 && unicode <= 0x1de6) return true;
  if(unicode >= 0x1dfc && unicode <= 0x1dff) return true;
  if(unicode >= 0x20d0 && unicode <= 0x20f0) return true;
  if(unicode >= 0xfe20 && unicode <= 0xfe26) return true;
  return false;
}

//unicode values of each code page 437 value before the ascii range
static const int cp437low[32] =
{
  0x2007,0x263a,0x263b,0x2665,0x2666,0x2663,0x2660,0x2022,0x25d8,0x25cb,0x25d9,0x2642,0x2640,0x266a,0x266b,0x263c,
  0x25ba,0x25c4,0x2195,0x203c,0x00b6,0x00a7,0x25ac,0x21a8,0x2191,0x2193,0x2192,0x2190,0x221f,0x2194,0x25b2,0x25bc
};

//unicode values of each code page 437 value after the ascii range
static const int cp437high[129] =
{
  0x2302,
  0x00c7,0x00fc,0x00e9,0x00e2,0x00e4,0x00e0,0x00e5,0x00e7,0x00ea,0x00eb,0x00e8,0x00ef,0x00ee,0x00ec,0x00c4,0x00c5,
  0x00c9,0x00e6,0x00c6,0x00f4,0x00f6,0x00f2,0x00fb,0x00f9,0x00ff,0x00d6,0x00dc,0x00a2,0x00a3,0x00a5,0x20a7,0x0192,
  0x00e1,0x00ed,0x00f3,0x00fa,0x00f1,0x00d1,0x00aa,0x00ba,0x00bf,0x2310,0x00ac,0x00bd,0x00bc,0x00a1,0x00ab,0x00bb,
  0x2591,0x2592,0x2593,0x2502,0x2524,0x2561,0x2562,0x2556,0x2555,0x2563,0x2551,0x2557,0x255d,0x255c,0x255b,0x2510,
  0x2514,0x2534,0x252c,0x251c,0x2500,0x253c,0x255e,0x255f,0x255a,0x2554,0x2569,0x2566,0x2560,0x2550,0x256c,0x2567,
  0x2568,0x2564,0x2565,0x2559,0x2558,0x2552,0x2553,0x256b,0x256a,0x2518,0x250c,0x2588,0x2584,0x258c,0x2590,0x2580,
  0x03b1,0x00df,0x0393,0x03c0,0x03a3,0x03c3,0x00b5,0x03c4,0x03a6,0x0398,0x03a9,0x03b4,0x221e,0x03c6,0x03b5,0x2229,
  0x2261,0x00b1,0x2265,0x2264,0x2320,0x2321,0x00f7,0x2248,0x00b0,0x2219,0x00b7,0x221a,0x207f,0x00b2,0x25a0,0x00a0
};

int unicodeToCp437(int code)
{
  if(code >= 32 && code <= 126) return code; //ascii
  
  static bool inited = false;
  static std::map<int, int> m;
  if(!inited)
  {
    inited = true;
    for(size_t i = 0; i < 32; i++) m[cp437low[i]] = i;
    for(size_t i = 0; i < 129; i++) m[cp437high[i]] = i + 127;
  }
  std::map<int, int>::iterator it = m.find(code);
  if(it != m.end()) return it->second;
  return 0;
}

int cp437ToUnicode(int code)
{
  if(code >= 32 && code <= 126) return code; //ascii
  else if(code < 32) return cp437low[code];
  else if(code < 255) return cp437high[code - 127];
  else return 0;
}


} // namespace lpi
