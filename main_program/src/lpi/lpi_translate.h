/*
Copyright (c) 2005-2011 Lode Vandevenne
All rights reserved.

This file is part of Lode's Programming Interface.

Lode's Programming Interface is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Lode's Programming Interface is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Lode's Programming Interface.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
Tools for translated strings
*/

#pragma once

#include <map>
#include <set>
#include <string>
#include <vector>

/*
Example translate file.
It must be UTF-8.

The string notation is: a string is between two double quotes. The backslash codes
\n, \\, \r and \" are supported like in C. No backslash is needed at the end of
a line in the source file. However, newlines in the source file are not treated
as newlines in the translated string: those must be marked with \n. Newlines are
treated as a space if there is no whitespace right before it already.

First line of the file is the nice
name of the language (the filenames
are ISO 639-1 names + ".txt").
One newline between from/to text.
Two newlines between next pair.
---------------------------------

"Nederlands"

"english text"
"nederlandse text"

"long explanation 452"
"Dit is een lange uitleg die verschillende newlines gebruikt\n
je kan newlines ingeven op dezelfde manier als in C-strings.\n
Ook andere escape codes zoals \" en \\ werken."

"accented text"
"deze tekst zou accentjes kunnen bevatten maar omdat het in een\n
comment in een C++ file is kan ik ze in dit voorbeeld niet\n
toevoegen. Het wordt in elk geval rechtstreeks gedaan normaal,\n
niet met backslash codes."
*/

namespace lpi
{

class Translate
{
  private:
    typedef std::map<std::string, std::string> smap;
    smap strings; //map from code strings to current language
    smap strings_fallback; //map from code strings to default fallback language
    std::vector<std::string> filenames; //filenames of the language files, with full directory path
    
    std::vector<std::string> languageKeys;
    
    std::string languageKey;
    std::string languageNiceName;
    
    //parses a language file and adds strings without first clearing the map (allows parsing multiple files for the same language)
    void parseFile(smap& strings, const std::string& file);
    void parseLanguage(smap& strings, const std::string& language);

    
    
  public:
    void setLanguage(const std::string& language);
    void setFallBackLanguage(const std::string& language);
    void addFile(const std::string& file);
    
    std::string getText(const std::string& text) const;

    std::string getLanguageNiceName() const;
    std::string getLanguageKey() const; //key of the currently active language
    
    bool hasLanguage(const std::string& key);
    
    size_t getNumLanguages();
    std::string getLanguageKey(size_t i);
};

extern Translate globalTranslate;

} //namespace lpi

//deliberately short function name to call lpi::globalTranslate.getText
inline std::string _t(const std::string& text)
{
  return lpi::globalTranslate.getText(text);
}

