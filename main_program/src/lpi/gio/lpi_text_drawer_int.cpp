/*
Copyright (c) 2005-2010 Lode Vandevenne
All rights reserved.

This file is part of Lode's Programming Interface.

Lode's Programming Interface is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Lode's Programming Interface is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Lode's Programming Interface.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "lpi_text_drawer_int.h"

#include "lpi_draw2d.h"
#include "../lpi_unicode.h"
#include "../lpi_unicode_data.h"
#include "../lodepng.h"

//TODO: cover the whole "World Glyph Set (see wikipedia, it's around 650 characters)" with this drawer

namespace lpi
{

////////////////////////////////////////////////////////////////////////////////
//DATA//////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

InternalTextDrawer::InternalTextDrawer(const ITextureFactory& factory, IDrawer2D* drawer)
: drawer(drawer)
, glyphs(&factory, false)
{
}

void InternalTextDrawer::drawText(const std::string& text, int x, int y, const Font& font, const lpi::ColorRGB* optColor, const TextAlign& align)
{
  if(align.halign != HA_LEFT || align.valign != VA_TOP) 
  {
    int w, h;
    calcTextRectSize(w, h, text, font);
    if(align.halign == HA_CENTER) x -= w / 2;
    else if(align.halign == HA_RIGHT) x -= w;
    if(align.valign == VA_CENTER) y -= h / 2;
    else if(align.valign == VA_BOTTOM) y -= h;
  }
  
  printText(text, x, y, font, optColor);
}

void InternalTextDrawer::drawLetter(int n, int x, int y, const InternalGlyphs::Glyphs* glyphs, const Font& font, const lpi::ColorRGB* optColor)
{
  //int italic = 0; //todo: this doesn't work anymore, no function to draw skewed texture available currently!!
  
  //draw the background, using the "completely filled" letter: ascii char 219
  /*if(font->background)
  {
    drawer->drawTexture(glyphs->texture[219], x, y, font.backgroundColor);
  }*/
  if(font.shadow)
  {
    drawer->drawTexture(glyphs->texture[n], x + 1, y + 1, font.shadowColor);
  }
  
  drawer->drawTexture(glyphs->texture[n], x, y, optColor ? *optColor : font.color);
  
  if(font.bold) //bold
  {
    drawer->drawTexture(glyphs->texture[n], x + 1, y, optColor ? *optColor : font.color);
  }
}

const InternalGlyphs::Glyphs* InternalTextDrawer::getGlyphsForFont(const Font& font) const
{
  if(font.typeface == "lpi8") return &glyphs.glyphs8x8;
  else if(font.typeface == "lpi7") return &glyphs.glyphs7x9;
  else if(font.typeface == "lpi6") return &glyphs.glyphs6x6;
  else if(font.typeface == "lpi4") return &glyphs.glyphs4x5;
  else return &glyphs.glyphs8x8;
}

//Draws a string of text, and uses some of the ascii control characters, e.g. newline
//Other control characters (ascii value < 32) are ignored and have no effect.
void InternalTextDrawer::printText(const std::string& text, int x, int y, const Font& font, const lpi::ColorRGB* optColor, unsigned long forceLength)
{
  printTextUTF8(text, x, y, font, optColor, forceLength);
}

void InternalTextDrawer::printTextASCII(const std::string& text, int x, int y, const Font& font, const lpi::ColorRGB* optColor, unsigned long forceLength)
{
  const InternalGlyphs::Glyphs* glyphs = getGlyphsForFont(font);
  unsigned long pos = 0;
  int drawX = x;
  int drawY = y;
  int symbol;
  while((pos < text.length() && forceLength == 0) || (pos < forceLength && pos < text.length() && forceLength > 0))
  {
     symbol = text[pos];
     if(symbol > 31 || symbol < 0) //it's a signed char, below 0 are the ones above 128
     {
       drawLetter(text[pos], drawX, drawY, glyphs, font, optColor);
       drawX += glyphs->width;
     }
     else
     {
       switch(symbol)
       {
         case 10: //newline
           drawX = x;
           drawY += glyphs->height;
           break;
         default: break;
       }
     }
     pos++;
  }
}

void InternalTextDrawer::printTextUTF8(const std::string& text, int x, int y, const Font& font, const lpi::ColorRGB* optColor, unsigned long forceLength)
{
  const InternalGlyphs::Glyphs* glyphs = getGlyphsForFont(font);
  int sw = glyphs->width;
  int sh = glyphs->height;
  size_t pos = 0;
  int drawX = x;
  int drawY = y;
  while((pos < text.length() && forceLength == 0) || (pos < forceLength && pos < text.length() && forceLength > 0))
  {
    int unicode = utf8ToUnicode(pos, text);
    utf8increment(pos, text);
    if(unicode == 10)
    {
      drawX = x;
      drawY += sh;
    }
    else if(unicode == 13)
    {
      //ignore
    }
    else if(unicode == 9)
    {
      //draw tab as a space
      drawX += sw;
    }
    else if(unicode < 32)
    {
      drawLetter(511, drawX, drawY, glyphs, font, optColor); //unknown glyph symbol
      drawX += sw;
    }
    else if(unicode <= 0x7F)
    {
      drawLetter(unicode, drawX, drawY, glyphs, font, optColor);

      drawX += sw;
    }
    else
    {
      bool drawn = false;

      // Try "unicodeToSymbol" function
      if(!drawn)
      {
        int symbol = unicodeToSymbol(unicode);
        if(symbol != 0)
        {
          drawLetter(symbol, drawX, drawY, glyphs, font, optColor);
          drawn = true;
        }
      }

      //Try accent with letter combination
      if(!drawn)
      {
        int baseLetter = getLatinLetter(unicode);
        int l = baseLetter;
        if(l > 127)
        {
          l = unicodeToSymbol(l);
          if(l == 0) l = unicodeToCp437(baseLetter);
          //l is now an index to the internal bitmap font
        }
        if(l != 0)
        {
          int a = getLatinAccent(unicode);
          if(a != 0)
          {
            bool lower = isBaseSymbolLowerCase(baseLetter);

            int accentY = drawY - sh + sh - 3;
            int accentX = drawX;
            int accentSymbol = unicodeAccentToSymbol(a);

            if(!lower) accentY--;

            //Accent shifts to left for greek capitals and accent code 0x384
            if(a == 0x384 && !lower)
            {
              accentX -= sw / 2;
              accentY++;
            }
            //caron takes different form om d, l, L, t
            if(a == 0x30c && (l == 'd' || l == 'l' || l == 'L' || l == 't'))
            {
              accentX += sw / 3 + 1;
              accentSymbol = 269;
            }
            
            drawLetter(accentSymbol, accentX, accentY, glyphs, font, optColor);
            //if(l == 'I') l = 290; //dotless I
            if(l == 'i') l = 306; //dotless i
            //if(l == 'i') l = 291; //dotless J
            if(l == 'j') l = 307; //dotless j
          }

          drawLetter(l, drawX, drawY, glyphs, font, optColor);
          drawn = true;
        }
      }

      //Try codepage 437. This is deliberately at the end, its accented characters are less nice than the ones above.
      if(!drawn)
      {
        int symbol = unicodeToCp437(unicode);
        if(symbol != 0)
        {
          drawLetter(symbol, drawX, drawY, glyphs, font, optColor);
          drawn = true;
        }
      }

      //No symbol found, draw the unknown glyph instead.
      if(!drawn) drawLetter(511, drawX, drawY, glyphs, font, optColor); //unknown glyph symbol


      drawX += sw;
    }
  }
}

void InternalTextDrawer::calcTextRectSize(int& w, int& h, const std::string& text, const Font& font) const
{
  const InternalGlyphs::Glyphs* glyphs = getGlyphsForFont(font);
  //w = glyphs->width * text.size();
  //h = glyphs->height;
  
  int numlines = 1;
  int linelength = 0;
  int longestline = 0;
  for(size_t i = 0; i < text.size(); i++)
  {
    if(text[i] == 10 || (text[i] == 13 && (i == 0 || text[i - 1] != 10)))
    {
      numlines++;
      if(linelength > longestline) longestline = linelength;
      linelength = 0;
    }
    else linelength++;
  }
  if(linelength > longestline) longestline = linelength;
  w = glyphs->width * longestline;
  h = glyphs->height * numlines;
}

InternalGlyphs::InternalGlyphs(const ITextureFactory* factory, bool allInOneBigTexture)
{
  initBuiltInFontTextures(factory, allInOneBigTexture);
}

InternalGlyphs::Glyphs::~Glyphs()
{
  for(size_t i = 0; i < texture.size(); i++) delete texture[i];
}

InternalGlyphs::~InternalGlyphs()
{
}


//this is linked to the base-64 fonts below.
int unicodeToSymbol(int unicode)
{
  if(unicode >= 32 && unicode < 128) return unicode;

  switch(unicode)
  {
    case 0x152: return 288; //OE
    case 0x153: return 304; //oe
    case 0x276: return 304; //small capital oe
    case 0xd0: return 289; //-D
    case 0x110: return 289; //-D
    case 0xf0: return 290; //*d
    case 0x111: return 305; //-d
    //case : return 290; //dotless I
    case 0x131: return 306; //dotless i
    //case : return 291; //dotless J
    case 0x237: return 307; //dotless j
    case 0xd8: return 292; //slashed O
    case 0xf8: return 308; //slashed o
    case 0xde: return 293; //capital thorn
    case 0xfe: return 309; //small thorn
    case 0x104: return 294; //ogenek A
    case 0x105: return 310; //ogenek a
    case 0x118: return 295; //ogenek E
    case 0x119: return 311; //ogenek e
    case 0x141: return 296; //slashed L
    case 0x142: return 312; //slashed l
    case 0x15e: return 297; //S with cedilla
    case 0x15f: return 313; //s with cedilla
    case 0x122: return 291; //G with cedilla
    case 0x126: return 299; //H with stripe
    case 0x127: return 315; //h with stripe
    case 0x12e: return 300; //I with ogenek
    case 0x12f: return 316; //i with ogenek
    case 0x13f: return 301; //L with dot
    case 0x140: return 317; //l with dot
    case 0x14a: return 302; //fat N
    case 0x14b: return 318; //fat n
    case 0x166: return 302; //T with extra stripe
    case 0x167: return 318; //t with extra stripe
    case 0x172: return 302; //U with ogenek
    case 0x173: return 318; //u with ogenek
    
    case 0x132: return 'Y'; //Dutch IJ
    case 0x133: return 'y'; //Dutch ij
    case 0x17f: return 244; //long s (symbol is top of integral sign here)
    case 0x2039: return '<';
    case 0x203a: return '>';
    case 0x2044: return '/';
    case 0x138: return 405; //kra (looks like kappa)
    case 0xa6: return 492; //vertical bar with hole in middle
    
    case 0xa8: return 264; //trema
    case 0xaf: return 261; //long stripe above
    case 0xb4: return 257; //acute accent
    case 0xb8: return 280; //cedille

    case 0x2bb: return 274;
    case 0x2bc: return 275;
    case 0x2bd: return 276;
    case 0x2c6: return 258;
    case 0x2c7: return 268;
    case 0x2c8: return 269;
    case 0x2c9: return 260;
    case 0x2ca: return 257;
    case 0x2cb: return 256;
    case 0x2d8: return 262;
    case 0x2d9: return 263;
    case 0x2da: return 266;
    case 0x2db: return 281; //ogenek
    case 0x2dc: return 259;
    case 0x2dd: return 267;
    
    case 2020: return 492; //dagger
    case 2021: return 493; //double dagger
    case 2026: return 494; //ellipsis (...)

    case 0x20ac: return 480; //euro
    case 0xa4: return 481; //currency sign
    case 0x2013: return 482; //en dash
    case 0x2014: return 483; //em dash
    case 0xa9: return 484; //copyright
    case 0xae: return 485; //registered
    case 0x2122: return 486; //TM
    case 0xb9: return 487; //superscript 1
    case 0xb3: return 488; //superscript 3
    case 0xd7: return 489; //multiplication x
    case 0xbe: return 490; //3/4
    case 0x2260: return 491; //not equal

    //code page 437 characters with multiple unicode equivalents
    case 0xdf: return 225; //est-zet
    case 0x2211: return 228; //sum
    case 0xb5: return 230; //micro sign
    case 0x2126: return 234; //ohm
    case 0x2208: return 238; //element of
    
    //diacritics
    case 0x300: return 256;
    case 0x301: return 257;
    case 0x302: return 258;
    case 0x303: return 259;
    case 0x304: return 260;
    case 0x305: return 261;
    case 0x306: return 262;
    case 0x307: return 263;
    case 0x308: return 264;
    case 0x309: return 265;
    case 0x30a: return 266;
    case 0x30b: return 267;
    case 0x30c: return 268;
    case 0x30d: return 269;
    case 0x30e: return 270;
    case 0x30f: return 271;


    //Cyrillic, capital
    case 0x404: return 325;
    case 0x405: return 'S';
    case 0x406: return 'I';
    case 0x408: return 'J';
    case 0x410: return 'A';
    case 0x411: return 320;
    case 0x412: return 321;
    case 0x413: return 322;
    case 0x414: return 324;
    case 0x415: return 'E';
    case 0x416: return 326;
    case 0x417: return 327;
    case 0x418: return 328;
    case 0x41a: return 329;
    case 0x41b: return 330;
    case 0x41c: return 'M';
    case 0x41d: return 331;
    case 0x41e: return 'O';
    case 0x41f: return 332;
    case 0x420: return 'P';
    case 0x421: return 'C';
    case 0x422: return 'T';
    case 0x423: return 333;
    case 0x424: return 334;
    case 0x425: return 'X';
    case 0x426: return 335;
    case 0x427: return 336;
    case 0x428: return 337;
    case 0x429: return 338;
    case 0x42a: return 339;
    case 0x42b: return 340;
    case 0x42c: return 341;
    case 0x42d: return 342;
    case 0x42e: return 343;
    case 0x42f: return 344;
    case 0x490: return 323;

    //Cyrillic, small
    case 0x454: return 357;
    case 0x455: return 's';
    case 0x456: return 'i';
    case 0x458: return 'j';
    case 0x430: return 'a';
    case 0x431: return 352;
    case 0x432: return 353;
    case 0x433: return 354;
    case 0x434: return 356;
    case 0x435: return 'e';
    case 0x436: return 358;
    case 0x437: return 359;
    case 0x438: return 360;
    case 0x43a: return 361;
    case 0x43b: return 362;
    case 0x43c: return 'm';
    case 0x43d: return 363;
    case 0x43e: return 'o';
    case 0x43f: return 364;
    case 0x440: return 'p';
    case 0x441: return 'c';
    case 0x442: return 365;
    case 0x443: return 'y';
    case 0x444: return 366;
    case 0x445: return 'x';
    case 0x446: return 367;
    case 0x447: return 368;
    case 0x448: return 369;
    case 0x449: return 370;
    case 0x44a: return 371;
    case 0x44b: return 372;
    case 0x44c: return 373;
    case 0x44d: return 374;
    case 0x44e: return 375;
    case 0x44f: return 376;
    case 0x491: return 355;
    
    //Greek, capital
    case 0x391: return 'A';
    case 0x392: return 'B';
    case 0x393: return 226;
    case 0x394: return 384;
    case 0x395: return 'E';
    case 0x396: return 'Z';
    case 0x397: return 'H';
    case 0x398: return 385;
    case 0x399: return 'I';
    case 0x39a: return 'K';
    case 0x39b: return 386;
    case 0x39c: return 'M';
    case 0x39d: return 'N';
    case 0x39e: return 387;
    case 0x39f: return 'O';
    case 0x3a0: return 388;
    case 0x3a1: return 'P';
    //0x3a2 is missing in unicode???
    case 0x3a3: return 228;
    case 0x3a4: return 'T';
    case 0x3a5: return 'Y';
    case 0x3a6: return 389;
    case 0x3a7: return 'X';
    case 0x3a8: return 390;
    case 0x3a9: return 234;
    
    //Greek, small
    
    case 0x3b1: return 224; //alpha
    case 0x3b2: return 225; //beta
    case 0x3b3: return 400; //gamma
    case 0x3b4: return 235; //delta
    case 0x3b5: return 401; //epsilon
    case 0x3b6: return 402; //zeta
    case 0x3b7: return 403; //eta
    case 0x3b8: return 404; //theta
    case 0x3b9: return 306; //iota
    case 0x3ba: return 405; //kappa
    case 0x3bb: return 406; //lambda
    case 0x3bc: return 230; //mu
    case 0x3bd: return 'v';
    case 0x3be: return 407; //ksi
    case 0x3bf: return 'o';
    case 0x3c0: return 227; //pi
    case 0x3c1: return 408; //rho
    case 0x3c2: return 409; //ending sigma
    case 0x3c3: return 229; //sigma
    case 0x3c4: return 231; //tau
    case 0x3c5: return 'u';
    case 0x3c6: return 410; //phi
    case 0x3c7: return 'X';
    case 0x3c8: return 411; //psi
    case 0x3c9: return 412; //omega
  }
  
  return 0;
}

int unicodeAccentToSymbol(int unicode)
{
  if(unicode >= 0x300 && unicode <= 0x30F) return unicode - 0x300 + 256;
  //Greek
  else if(unicode == 0x384) return 257;
  else if(unicode == 0x385) return 258;
  
  return 0;
}

//for accents rendering position
bool isBaseSymbolLowerCase(int code)
{
  if(code >= 'a' && code <= 'z') return true;
  if(code >= 0x3b1 && code <= 0x3c9) return true; //Greek
  if(code >= 0xdf && code <= 0xff) return true; //lowercase part of Latin-1 Supplement
  if(code >= 0x100 && code <= 0x137 && code % 2 == 1) return true; //part of Laten Extended A is lowercase for odd codes
  if(code >= 0x138 && code <= 0x148 && code % 2 == 0) return true; //similar
  if(code >= 0x149 && code <= 0x17e && code % 2 == 1) return true; //similar
  if(code == 0x17f) return true; //similar

  return false;
}

const std::string& getBuiltIn8x8FontTexture()
{
  /*
  A PNG image, encoded in base64, containing the 256 characters of "Code page
  437", then the first 16 diacritics of Unicode, then 16 empty spaces,
  then a row of several capital extra latin letters, then a row of several
  small extra latin letters, then 2 rows containing capital cyrillic letters,
  then 2 rows with small cyrillic letters, then a row of large greek letters,
  then a row of small greek letters, and near the bottom an euro sign and
  unknown symbol sign.
  */
  static const std::string font8x8string = "\
iVBORw0KGgoAAAANSUhEUgAAAIAAAAEAAQMAAABBN+zkAAAABlBMVEUAAAD///+l2Z/dAAAHsElE\n\
QVR42u1XTWgcyRV+6yyKDo3dxwKJlUh80MGBAplxE5qRWebgQy4LXgg5FbKp9aHwTpZl8mArLcXo\n\
4IMPZk82LAkEctnTEnQIyy4UCBofHpNLGAZGMXMaQxCmYYO2wUVPXtWMpLHkBBOS2z719HR/8+r9\n\
1fspARSFSbMUpjC9km/tPAX43bTJLAP5wY/15p1fAfxp1NgsEy/00yt6ayePHE1j84P3v/2tlnde\n\
AHx7YG3TBIDyCDx9kWUDG5aQ6H4Ul6RMQSgV1YSFFgVAlgW1KMbuKexeEnqnACEEzGh8OdejA8hz\n\
ISRcFVOo24UeIS8UiXI6n0LTCP0ZhSVN46YssG4LvULMUASOac4yCljBIhe5kM3VqWAtuV55VJxp\n\
AeCnms04JWmMhEwJFd+WANAYWxolpZYMJAFoHGVOiVzO1kswKHqgxNSD5ze+mkQewpwjSjK1Jggy\n\
uiBdXCLLHgQtEuQuLFLkR4m46jOPGFQoBKqIcqeIKC4hOJRJYmqXEEmpvEjAyyyjpBZoJatIBBxJ\n\
lTQJSWKjmUMCPxLzKxKyK4Oh6D0miAq7UrIWecEMsNLnnveW8L1JVZYZlKi12d7WJBOtXk4MPCft\n\
9L17jgGjmuOSAes0ovMyQTV4zoCPHH1Wb7bLfgmOggzFMvihLA0gBS1VmxAnTdBygTzyH1FZUoMO\n\
UwBNmvYjQEqJLAJj9q809ERJYdh0shkDgwylEqJkjw5xlYHGyG2VhAxQaNgEfFnKUi0FoFqdIHpZ\n\
ltjgJbxoxZSDNoZVyGAsxbgb84UDlYAJ4ZrFj+PPMVU900Utj2qWkmiyvH09tko2Yf+sduQVaSlQ\n\
DgLAvpBTVgeOMgC9b7CHVTLB4QRLOrWkBuieWZPOvlbFuDd7nO+fFPIwg8Pe0NoYIM8AW6jJuHmA\n\
gg9lADAGiDOIOUqwViXXCRtj784AlVS16ElTJp61QAPVe2eWREJYLWgsgW9kx8Q5RnAQLOEbC5AS\n\
HGCOiJZv3azLrnAKLyVJ4thn1sLO8e63rbWu8MFSFiAo/EhWOceA95AUuFUUxVIQlCNzzCOSn8Zi\n\
FdoYrIGSREbV5VByxDaNKQNhaLgCfodmrJaKuzjkdE8aFsd6SnKV/2cBuEMzYkBJYo75u+clE/9c\n\
sIw+LymKJEPhJQ1hYWM4fy53YTXkrs9D4Zfl3LTVGoyG4LjYZNPDDpIJ3WQ4BK0hkUj1UTtX3re/\n\
E7TJNRd0AjiX3PlIMEeoWzosCnJJ/685yyjYiJAm7O+TP5wFIdLaj0Im/+TDv3OXawG0Wnwx8vDL\n\
37wORI66BfVxqzlu1WccYqm1tHTCUdfHDXMcN01dn3C0+GrF1jfXcgZc4OBOG25Bafj7t8Ba68bW\n\
99Mb0++npxxSzjN6bToN140t/jlyQOQRUUC4vQUQLhE+gou4ujJ9A8DX2hp/vcYh+OsEmE6nW2tr\n\
bGbN5lRXgpJW+AQn3hpgakKKZNkqcD7zHjaNBt2LcydRBD2uUFnoQyxLURSOG33tjCAtqCntaOQJ\n\
npEzMgBcyqPRnIN7jOBRxeMvyHDONKiEzF4iqJwzxQXFjscTzJ+5J17mGud0vRJmDE8PKVeE/IQb\n\
Z2KS2HKFWhHwRQSyfMYhhIdMMKDymQwO0Ccc6CNjA0dweCi/CMU3K07vPQOvl6roziv2NRLzbqYS\n\
2eP6AFjeuKrTjVvAHeEwKzC9laa3RLpxFf4vlMKyEEsLQAfT5fTSApCRSJff/e+Ep+eA5fVzgDw5\n\
kLT89fznleTO5LXkqQ/WCG0Ubkvan0o5JJ4V11wf6V425tOEPCLoH1s3MDw6M1/I6xQ4qD9Bf+/X\n\
q4GDggxiGbS9SQxscmtp+V77L9W7zQ7yjNqhaB5bwDNQzosWkpk9fFxRMm4Yd/53Kj6XgK1jWd5P\n\
uthWSJlljmDY51aK+5h4dNbI62zY77m/F8a6LspetOMxAz0klMn1mWH3ezi06tMUazHhLvhp1Lhb\n\
ndqxBI1vlop8gOWkoYakgW3tmmtqQP2718iQ58GrnfuFs4+emwDsG55iDPhMHOM1HwHNHI+cTSZc\n\
HsbOgYZllDxlTMIyvHfuQdTyPMzE5vwGXoL+aFR/N676xQnElffVHSVGtADIOzY5Aw5Ho/Zn2r+y\n\
J4AZjTY/1snotBSSCIiROQEuT6fFq2+qfv9M9TuvW2LhHLl4SF2gIR/XLA6QQsQ8R+yIvf0lB+gw\n\
BEiyt3w+c0/qTHg03kiOB5/gnHeWjnhkRg7keOxFGRwP+aZ4vInqcswH+5M39uGrUlX9xQC1j+2C\n\
txygzb6uX53Gw0TgXDyO/2M8Ukx9k35+BmR72V4nWwA29jagc3sBMPsGO4PbC8Cegc5g4dBZ7pV7\n\
ndsLRdtg6TvZ+SpeLOuH3Yd8360A1ufb8AyHCtxut3trPwJdt+4fT1T1bPdv+9HBYffmUWXkTQYw\n\
HhUeul2qEAMgBzGgz2p6zJVdcQVFoNtdTtRkb9ktp3IeJBEKVbgLLeYH+oHeQCHLrf3HOqfmcvw3\n\
uMOF8ODBhyqFnwqRcylWGwBff/3BuuqUPgI3O3nxxz9/sJ5u/EzG0RA5voR1lY48rzmRwZI3bvCJ\n\
IB/NtfCt8/FN1jJaMODg7ewszgPv/w+AC0LhX0x2cVargfvvAAAAAElFTkSuQmCC\
";
  return font8x8string;
}

const std::string& getBuiltIn7x9FontTexture()
{
  /*
  A PNG image, encoded in base64, containing a bitmap font of 7x9 pixels
  Similar contents to font8x8string, but not fully implemented.
  */
  static const std::string font7x9string = "\
iVBORw0KGgoAAAANSUhEUgAAAHAAAAEgAQMAAABW12VLAAAABlBMVEUAAAD///+l2Z/dAAAHbUlE\n\
QVRYhe2XXUwb2RWAz1wPeGCJPZ6QlnQneDzMAuGhtegqdSvkjs2wOJEVwIu61b6s09CVVuqqdtJV\n\
iOQ2Y8cKY1oFm5YqkSIBSaRN1ZfdSlG7itTyV0jUSuxDH6pKUWxAYvujxE4egLItPXeMjQ3drapu\n\
1T7skbgzH+fcc8+ce++51wBF+btls/jyt4LPylK06EiMZfvsiE1cPN/0ndPOV4F55bWNOac3MfrB\n\
iwFHFzDWnZ2/7vwyMfrn3512vg7MBefODsW/dBsOxLZ76394hH1FH3eYGm/NJ6jnD7uvffFVgK18\n\
3GpD3I43uenAKtiB4yV8Yy+BzuY0Xhc90qxeY/NC9lhB5VRvTDoLhyZF2Iz5NKvK8S0ba84dA3Y+\n\
9Kk14OU3z7KHMOjN7xbUz35V5COzI79Gb1kxC/yvip5RdNancpe8UCFWfe9dmmI44Dm1hNPdP7qy\n\
iyzirbU6TuIlVS8i003Q2pXX7TpHUU3+GHipOwuACJb1uhDiFBrzVOuHMUBXcb2Bp/4I/iFaUAX7\n\
RHRHst3zk6KHQgPIfIqfYx2yhqacgMg7/Bwja+Hw0iJBlL7RPUbELnTFJBHVw9lAQmYBtSxinNcC\n\
SZmDGQmDEmOb2dURVoyEi5/wseI5m3tin2x+vLlx4dj2DdBcgpJMyZoky4euyxAcENieegnRT3Fw\n\
YIXttUkxqSVRP4EYpdo2zSX7A4jBFPY1ZEQ58EMZfI9yy/Yb4uPNpS893Bah62PDGI7m3n64vbH0\n\
ML9QiyErxvg3AxlZS/EJUm8iw2U62vulBHFQzMxy4x04R3GaYI+RdXGjHe2SbqJkjFPUXLyJkehv\n\
5kcaGhejeT1hfnBpyJkr+4O4BBLO1IwF0/iShf6DZ3DNsrhsCWMaRM+s9tqf/3puJXfshAiQGlfu\n\
J1uHJNlXj8mBiwLbl8TUtcTrM4gpgQ2wNJM+E3dyq+tjmOfW9UeY56IQYGYql8TBvPh252/3YX7R\n\
799898n1extvPswjcjB0bY7lMnJ7SoqTQ42gGfEFmpx+X9zyGRlRV05icvrTaCzD8MXV+Su/bfyW\n\
K2+iyhX9StQz7sHdCLLFcfdNmVaNqtk2V6NcjVJ13zJ6qvvyld8IYNYK3BP7hbls/KK2ApOB23V7\n\
aFmrUbg9ZO3EU1ECWNZ+m6s0Jq0VyPxg+XagAscSVveB4anQOkb29hQb4IEofvZ+EUX0QTS9Timi\n\
4ubplAnCbl/gtworRCnHzOnCC2UtQEBv/KO/1NfMQnyl5NmUrIqN9NNIZUCpgfcr8b+n/T8Xa/5/\n\
irmN9Rn6zBaXTYbQBR+L8iqxNkBnhvit415+tG5x2RDgVI746gQuNlp/vLPfBRpqEbUt4Xjnay44\n\
hX0pGjdbl9Mu6MyR3kWRixnTM4Rz4dkGdGXw55twaFcpiFhFQMRcObXm7nHp4TlcVwA1tG7EpDNx\n\
mT2K+Jap3RQYGVrRuJ9zFrUEn2FWtTnNvmcIHIXwnA5OcwRGYFuxBYqw9SxXWrdOs2UObldwq8WH\n\
OzcFWLb56da5egWazvxsfuwq1y75uxP1wsFen5jw4LvM6GXUIngA7KHHmGWY/3AAqQrLN4qiuHcr\n\
SCx/PPv5Bm/kjacu24kLkA6ca1lgBen6KwX2qAxpTWCX6hvdut+Fpz/cWHtao42KkeizXA1iukd4\n\
q9fWOMQVtemTwguaQ1AMRDxTYvnC4ukve2P5FZf9PbN0j+h4tZFwkhha3BgDCxutxDppMK8rNoyK\n\
IYxZQ9EeSw1vWZox18Yb8/NLNrHpImHIidchfU3VFjKy5/LcLMGj7Yah9vTave6oxUVoGIb6kuaQ\n\
+DoBsRViF/O9V53Nnvx61P7e59CZxYBSGJZiDkph4MIp5NtW71598ujpFz5IKqClLDntiCNo3FQC\n\
kwqoKcIEbI5gGnFCgeECIX3J2sHsrbW+CWpMSMDGDGZuKmoRrWtHHEMpWVGx71aBsC13r357+ZzS\n\
l/xe9UyR/VP3OPO1hoAnWDrhtEwID+tQoBJJGYOZl5f6vOdK+GImpNxPhEoXCZZismxcu7Wz3OsJ\n\
lo74f0u6qu+JauHZ6tKEGHzwtG052QjDJwkxbPJg/0054cSDPkrIWJIdDN9aSRxBPEms922dgwOy\n\
qcXUsb0T4tCDc3KC9VYPcyAbB6S2oeI0DE5jNpqVUq4C05gNpkMrI37+5FdK6KEY7ygZc4Xtpb5m\n\
pXEXd4vE0D8flY90P1k5soeGX+ndw3Zjij01sYd3phdCp8uIV0U2pFWgoFRgLHp47VSyajBr9c34\n\
o3MSYCSLzpfvQoHI4kLQkDyRztsqolvvVnrYLi0euiOZ2kTboo1H/ImJcavS87yoxQceUHSfzyrB\n\
DDscyV6ZMZ1J53VoUHn6W4NKmDbqbtn8VD6Vj5Ims+XDFnDQ2ygWcuvyvXd40F34e5uWWs79cjr8\n\
fUeBajfvwHM/D6X5/imVojqw+qcToXS4xZc3tQY8dzekQ//7cVPbj31VevtjiK0DlDvoWcftEWGo\n\
tqkUAP8vQ9xXc+RPDvd5hn8Ag1JLybeSemAAAAAASUVORK5CYII=\
";
  return font7x9string;

}

const std::string& getBuiltIn6x6FontTexture()
{
  /*
  A PNG image, encoded in base64, containing a bitmap font of 6x6 pixels
  Similar contents to font8x8string, but not fully implemented.
  */
  static const std::string font6x6string = "\
iVBORw0KGgoAAAANSUhEUgAAAGAAAADAAQMAAADBSvSVAAAABlBMVEUAAAD///+l2Z/dAAAEo0lE\n\
QVR42u1WTWzTZhh+HBv6UXmp65kth6iyU5cWxiFFOwStap2OELvSplTsMA5oLfvtLYZLu1HNddI6\n\
TKEKEaAeog4x9seFnZC4sDSNCELaxqTtvNIygbQD1STEprXqXjvAqLRNE8eJV1HkJ8/7Pt/r9+dT\n\
gLZTqou1tm/tlnPghq7YDazf2T+cPwW0XfnjbvGbF9630w64a5UjFwm8pwnEnChQzPqd8aPPnCM3\n\
h2uQQFUF4LjK+Coe2IKnfFFi6hTjDGDFVcaWollvnl/uJwY7b1e0BZZMR/2YXemC6mC8reCH8dWH\n\
AphWYvuZ5j+FCPRnTWZMM0Cgg5TqjGO4Ua5KGP292xAwks/c3OFQjCr4CSH4AiWAXi/avtIRr0IS\n\
ojAdt1cIWSq0WghmThxuCPFlZnAizHxoXC+YPgPEvUr7IovXmCSwR1kFT9mGaDfC1qI8qBdxXC9Z\n\
gmROKS+/U8blbslqPDc+FRn8tIwvbxEjm8eVQb0Mu1u062HrqnyEYjZZthG29XNWt+jQoZZe0vQL\n\
lsaMlA+KWtcuyk2ld7C7QwRMnUkEjHpFrSlmQ2TVR+/pWwcMAYyD4Ur+j+ptMVELZyF3NkRwlOi7\n\
sukqsfPlALhh0436AL5byHTl2PkHuYnY+lAyEBc1t4Ds8qytn/b7Na3A6oxo+mk5KcGIwYpt1bpa\n\
22tMdWLkFpWWW3vSRc3phxGEc04g1Zuc3clD2w9NKAZtccQMlEwOSHaGEyul+CL1t4hkV5nKcPDt\n\
guqV0btDpjKMzjA1P4+RFXKb890gw2iZt7w5Sy/s2S2g83pwQlW0hlphXxTjdV9jpBjF4eskUN6r\n\
l5LXGZK/lK1OcZteXM31IAuRJiKxGMxOhJuxqdWgAr1FOsAWBZSbUEaCl+M0B2Dx49toQsrD6Wiy\n\
EX09XyZQHDawl9tRoZgED3/WmyOiHbqmToNLYXgQ+Ohzs3MaK6m+718NmKUTfWpKeLaWaDLKSurF\n\
1TBrxihaSvEliPkLqA6Dq2S2ILOlCXI/HFrfeGN9AwMb3238+nyyOeQI+czkh/d9xgeuwhl+2GZA\n\
JVfp4wzwUxs4sKr0rSprTbDW9pPq3edzGz7g76HvnrIWGtiYompM+wup/h1o9P8GJesJeBMY61Ks\n\
n3s0s2DXIzhZV2IpduDO3IWLOk7qylCaaSndrusYq+L3PMuemON5HdymXcgzyaWRawumpib1OK0Z\n\
sFEvYFSPEmcZL/CMOa2c05Np0GJcjWghhZuJPC4U7Kk62NpbL2RyyjA1U6Kh5YGcovmdfQJ7bUFQ\n\
H23aZCzYw/9mkvMYUJthk4vhl/iocVvWWmSc1EoH86XR0IBG8/bxJ2cm9lWWGrK2r0x1K495pWqs\n\
w2cmF2ePCpWJmtxe9feUrsKrIGluK45lA+kWia5SlFR8wBfjywl4Miq32MH8GduLqud9NTaWnxs5\n\
LKs0vcduMXLTFmTJmaXgyEM1/omq89Se2v/R9pzFxN0bI+I++tcDy8DM9huHd89rdO8sq7h0LzP6\n\
ighibqa//uxHA7l5la6nobO49BXd/tdUdzviwMQSQj3Ov53S8TiQ/xFscsOfKGaD998z3WwAAAAA\n\
SUVORK5CYII=\
";
  return font6x6string;

}

const std::string& getBuiltIn4x5FontTexture()
{
  /*
  A PNG image, encoded in base64, containing a bitmap font of 4x5 pixels
  Similar contents to font8x8string, but not fully implemented.
  */
static const std::string font4x5string = "\
iVBORw0KGgoAAAANSUhEUgAAAEAAAACgAQMAAACG4/gfAAAABlBMVEUAAAD///+l2Z/dAAAAzklE\n\
QVR42mNgoBlgWcDJpMLAwARkMLE4LWFgAYkxOPmpsQApTgaVJQ4eCMVO59z0nriouTEsU2txWsXg\n\
oMSwyOnRkhAXNQcGp3cuLhUsQHPgwKXt3LMlWm0pDKtOrOh4otW9iuHcijWnlqzpXMXw6ti5tiWn\n\
Xi5BKD527NXKVc/cHBhWrfFa6aKitAAo4rXaxUWJgaFpjZ/bimdi/FhM1oKZzAczmQuXySFQk4HO\n\
Bpvs48wFNdmNj3ahOzgBPymMUTAKRjCAFw5cGAxEuQEAZdZWdvFf/QcAAAAASUVORK5CYII=\
";
  return font4x5string;
}


void InternalGlyphs::initBuiltInFontTextures(const ITextureFactory* factory, bool allInOneBigTexture)
{
  AlphaEffect AE_BlackKey(128, 255, lpi::RGB_Black);

  glyphs8x8.width = 8;
  glyphs8x8.height = 8;
  glyphs7x9.width = 7;
  glyphs7x9.height = 9;
  glyphs6x6.width = 6;
  glyphs6x6.height = 6;
  glyphs4x5.width = 4;
  glyphs4x5.height = 5;

  if(allInOneBigTexture)
  {
    loadTexturesFromBase64PNG(glyphs8x8.texture, factory, getBuiltIn8x8FontTexture(), 8*16, 8*32, AE_BlackKey);
    loadTexturesFromBase64PNG(glyphs7x9.texture, factory, getBuiltIn7x9FontTexture(), 7*16, 9*32, AE_BlackKey);
    loadTexturesFromBase64PNG(glyphs6x6.texture, factory, getBuiltIn6x6FontTexture(), 6*16, 6*32, AE_BlackKey);
    loadTexturesFromBase64PNG(glyphs4x5.texture, factory, getBuiltIn4x5FontTexture(), 4*16, 5*32, AE_BlackKey);
  }
  else
  {
    loadTexturesFromBase64PNG(glyphs8x8.texture, factory, getBuiltIn8x8FontTexture(), 8, 8, AE_BlackKey);
    loadTexturesFromBase64PNG(glyphs7x9.texture, factory, getBuiltIn7x9FontTexture(), 7, 9, AE_BlackKey);
    loadTexturesFromBase64PNG(glyphs6x6.texture, factory, getBuiltIn6x6FontTexture(), 6, 6, AE_BlackKey);
    loadTexturesFromBase64PNG(glyphs4x5.texture, factory, getBuiltIn4x5FontTexture(), 4, 5, AE_BlackKey);
  }
}


} //namespace lpi
