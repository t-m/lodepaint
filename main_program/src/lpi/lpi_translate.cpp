/*
Copyright (c) 2005-2011 Lode Vandevenne
All rights reserved.

This file is part of Lode's Programming Interface.

Lode's Programming Interface is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Lode's Programming Interface is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Lode's Programming Interface.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "lpi_translate.h"

#include "lpi_file.h"

#include <iostream>

namespace lpi
{

static bool has_sorted(const std::vector<std::string>& v, const std::string& s)
{
  size_t i0 = 0;
  size_t i1 = v.size();
  
  while(i0 < i1)
  {
    size_t i = (i0 + i1) / 2;
    int c = v[i].compare(s);
    if(c == 0) return true;
    else if(c > 0) i0 = i + 1;
    else i1 = i;
  }
  
  return false;
}

/*
The usage of a vector and sorting for language names is done to ensure each is
remembered max once while having the convenience of std::vector. This is not the
most efficient algorithmical solution due to the inserting but it's good enough.
*/
static void insert_sorted(std::vector<std::string>& v, const std::string& s)
{
  if(has_sorted(v, s)) return;
  
  size_t i0 = 0;
  size_t i1 = v.size();

  while(i0 < i1)
  {
    size_t i = (i0 + i1) / 2;
    int c = v[i].compare(s);
    if(c > 0) i0 = i + 1;
    else i1 = i;
  }
  
  v.insert(v.begin() + i0, s);
}

//returns 0 if ok, not 0 if error (1 if end of file reached, 2 if syntax error)
static int getCString(size_t& pos, std::string& out, const std::string& in)
{
  out = "";
  
  while(pos < in.size() && in[pos] != '"') pos++;
  
  if(pos >= in.size()) return 1; //EOF
  
  pos++; //skip the "
  
  for(;;)
  {
    if(pos >= in.size()) return 2; //unterminated "
    
    if(in[pos] == '"')
    {
      pos++;
      break;
    }
    else if(in[pos] == '\\')
    {
      pos++;
      if(pos >= in.size()) return 2; //unterminated backslash
      char c = in[pos];
      if(c == 'n') out += 10;
      else if(c == 'r') out += 13;
      else if(c == '\\') out += '\\';
      else if(c == 10) /*nothing to do, newline char skipped*/;
      else if(c == 13)
      {
        if(pos + 1 < in.size() && in[pos + 1] == 10) pos++; //MS Windows newline
      }
      else if(c == '#')
      {
        while(pos < in.size() && in[pos] != 10) pos++;
      }
      else return 2; //unsupported escape sequence
    }
    else if(in[pos] == 10)
    {
      char o = out.size() == 0 ? 'a' : out[out.size() - 1];
      if(!(o == ' ' || o == 10 || o == 13)) out += ' '; //treat newline as space if no whitespace in front of it already.
    }
    else if(in[pos] == 13)
    {
      //ignore MS Windows' second newline character
    }
    else out += in[pos];
    
    pos++;
  }
  
  return 0;
}

void Translate::parseFile(smap& strings, const std::string& file)
{
  std::string f;
  loadFile(f, file);

  size_t pos = 0;
  int error = getCString(pos, languageNiceName, f);
  if(error) return; //fail
  
  for(;;)
  {
    std::string from;
    std::string to;
    int error = getCString(pos, from, f);
    if(error) break; //fail or done
    error = getCString(pos, to, f);
    if(error) break; //fail or done

//std::cout << "from to: " << from << " " << to << std::endl;
    strings[from] = to;
  }
}

void Translate::parseLanguage(smap& strings, const std::string& language)
{
  strings.clear();

  for(size_t i = 0; i < filenames.size(); i++)
  {
    std::string lan = getFileNameFilePart(filenames[i]);
    if(lan == language)
    {
      parseFile(strings, filenames[i]);
    }
  }
}

void Translate::setLanguage(const std::string& language)
{
  languageKey = language;
  parseLanguage(strings, language);
}

void Translate::setFallBackLanguage(const std::string& language)
{
  parseLanguage(strings_fallback, language);
}

void Translate::addFile(const std::string& file)
{
  filenames.push_back(file);
  
  insert_sorted(languageKeys, getFileNameFilePart(file));
}

size_t Translate::getNumLanguages()
{
  return languageKeys.size();
}

std::string Translate::getLanguageKey(size_t i)
{
  return languageKeys[i];
}

std::string Translate::getText(const std::string& text) const
{
  smap::const_iterator it;

  it = strings.find(text);
  if(it != strings.end() && !it->second.empty())
  {
    return it->second;
  }

  it = strings_fallback.find(text);
  if(it != strings_fallback.end() && !it->second.empty())
  {
    return it->second;
  }

  return text;
}

std::string Translate::getLanguageNiceName() const
{
  return languageNiceName;
}

std::string Translate::getLanguageKey() const
{
  return languageKey;
}

bool Translate::hasLanguage(const std::string& key)
{
  for(size_t i = 0; i < languageKeys.size(); i++)
  {
    if(languageKeys[i] == key) return true;
  }
  return false;
}

Translate globalTranslate;

} //namespace lpi
