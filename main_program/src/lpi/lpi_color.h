/*
Copyright (c) 2005-2012 Lode Vandevenne
All rights reserved.

This file is part of Lode's Programming Interface.

Lode's Programming Interface is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Lode's Programming Interface is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Lode's Programming Interface.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
TODO:

[ ] For any conversion involving XYZ, Lab, ..., allow specifying XYZ<->RGB matrix, white point, gamma, etc...
[ ] make EVERY "255" class use values in range 0-255, and EVERY "1" class use values in range 0-1. Instead, add getters and setters for nominal min and max
    value, and nominal current values (e.g. -1.333 to 1.333 for YDbDr's Db and Dr channels would have -1.333=0, 1.333=255 by default, but the nominal value
    of 128 would be 0, nominal min -1.333, nominal max 1.333)
[ ] Add support for HunterLab and Luv
[X] Refactor the implementation: make a generic function for matrix RGB to something conversions, and let all the ones that use a matrix (XYZ, all those
    television color spaces, etc...), call that.
[X] remove CIE prefix from names of XYZ and Lab class. Only use a prefix for non-CIE ones.
*/

/*
For each color type struct below:
-For structs with "255" in the name, every valid color channel field is always in range 0-255, no matter what color model
-For structs with "1" in the name, every valid color channel field is always in range 0-1, no matter what color model
Fields can go (be represented) outside this 0-255 or 0-1 range in some situations, but this is not considered valid. This is for
 example to represent an out-of-gamut RGB color.
-The getter and setter functions of channels, instead, use a nominal range. This nominal range always uses the same values
for the "255" and "1" counterparts of the struct. The nominal range is the range that is most often considered the value in which
the channel goes for that color model. E.g. HSV's Hue has 0-360, Lab's a and b have -128 - 127 because that how it is most often
presented, YCbCr has everything from 0-255 because it's JPEG's YCbCr, and YUV's U has -0.436 - 0.436 because that's how it's defined
in that telivision color model, RGB has 0-255 for everything because that's how it is most often used, and XYZ has 0-1 for everything
because it's more a mathematical model (NOTE: for XYZ, things outside the range 0-1 are considered valid too).
-Both the fields and the getter/setters are public, feel free to use which suits your need: you can always work with 0-255 range, 0-1 range, or
 custom nominal range, for any supported color model here.
NOTE: This is a work in progress, color models which don't follow the above rules are TODO.
*/


#pragma once

namespace lpi
{

template<typename T>
struct TColorRGB255
{
  T r;
  T g;
  T b;
  T a;
  
  TColorRGB255(T r, T g, T b, T a)
  {
    this->r = r;
    this->g = g;
    this->b = b;
    this->a = a;
  }
  TColorRGB255(T r, T g, T b)
  {
    this->r = r;
    this->g = g;
    this->b = b;
    this->a = 255;
  }
  
  TColorRGB255()
  {
    this->r = 0;
    this->g = 0;
    this->b = 0;
    this->a = 255;
  }
  
  void negateRGB()
  {
    r = 255 - r;
    g = 255 - g;
    b = 255 - b;
  }
  
  void negateRGBA()
  {
    negateRGB();
    a = 255 - a;
  }
  
  void clamp()
  {
    if(r > 255) r = 255;
    if(r < 0) r = 0;
    if(g > 255) g = 255;
    if(g < 0) g = 0;
    if(b > 255) b = 255;
    if(b < 0) b = 0;
    if(a > 255) a = 255;
    if(a < 0) a = 0;
  }
  
};

template<typename T>
struct TColorRGB1
{
  T r;
  T g;
  T b;
  T a;
  
  TColorRGB1(T r, T g, T b, T a)
  {
    this->r = r;
    this->g = g;
    this->b = b;
    this->a = a;
  }
  TColorRGB1(T r, T g, T b)
  {
    this->r = r;
    this->g = g;
    this->b = b;
    this->a = 1.0;
  }
  
  TColorRGB1()
  {
    this->r = 0.0;
    this->g = 0.0;
    this->b = 0.0;
    this->a = 1.0;
  }
  
  void negateRGB()
  {
    r = 1.0 - r;
    g = 1.0 - g;
    b = 1.0 - b;
  }
  
  void negateRGBA()
  {
    negateRGB();
    a = 1.0 - a;
  }
  
  void clamp()
  {
    if(r > 1.0) r = 1.0;
    if(r < 0.0) r = 0.0;
    if(g > 1.0) g = 1.0;
    if(g < 0.0) g = 0.0;
    if(b > 1.0) b = 1.0;
    if(b < 0.0) b = 0.0;
  }
};

typedef TColorRGB255<int> ColorRGB;
typedef TColorRGB1<double> ColorRGBd;
typedef TColorRGB1<float> ColorRGBf;

//the + and - operator add/subtract the R G and B channels but not the alpha channel
ColorRGB operator+(const ColorRGB& color, const ColorRGB& color2);
ColorRGB operator-(const ColorRGB& color, const ColorRGB& color2);
//these multiplication operators with a constant don't affect the alpha channel
ColorRGB operator*(const ColorRGB& color, int a);
ColorRGB operator*(int a, const ColorRGB& color);
ColorRGB operator*(const ColorRGB& color, double a);
ColorRGB operator*(double a, const ColorRGB& color);
ColorRGB operator/(const ColorRGB& color, int a);
//this multiplies two colors, and the alpha channels too
ColorRGB operator*(const ColorRGB& color1, const ColorRGB& color2);
//The | operator adds two color including their alpha channel
ColorRGB operator|(const ColorRGB& color, const ColorRGB& color2);
//The & operator multiplies a color and also its alpha channel with a constant
ColorRGB operator&(const ColorRGB& color, int a);
ColorRGB operator&(int a, const ColorRGB& color);
ColorRGB operator&(const ColorRGB& color, double a);
ColorRGB operator&(double a, const ColorRGB& color);
//Compare
bool operator==(const ColorRGB& color, const ColorRGB& color2);
bool operator!=(const ColorRGB& color, const ColorRGB& color2);
//Lesser than ONLY there to be able to sort colors for use in std::maps and such, has no real life meaning
bool operator<(const ColorRGB& color, const ColorRGB& color2);

//the + and - operator add/subtract the R G and B channels but not the alpha channel
ColorRGBd operator+(const ColorRGBd& color, const ColorRGBd& color2);
ColorRGBd operator-(const ColorRGBd& color, const ColorRGBd& color2);
//these multiplication operators with a constant don't affect the alpha channel
ColorRGBd operator*(const ColorRGBd& color, double a);
ColorRGBd operator*(double a, const ColorRGBd& color);
ColorRGBd operator/(const ColorRGBd& color, double a);
//this multiplies two colors, and the alpha channels too
ColorRGBd operator*(const ColorRGBd& color1, const ColorRGBd& color2);
//The | operator adds two color including their alpha channel
ColorRGBd operator|(const ColorRGBd& color, const ColorRGBd& color2);
//The & operator multiplies a color and also its alpha channel with a constant
ColorRGBd operator&(const ColorRGBd& color, double a);
ColorRGBd operator&(double a, const ColorRGBd& color);
//Compare
bool operator==(const ColorRGBd& color, const ColorRGBd& color2);
bool operator!=(const ColorRGBd& color, const ColorRGBd& color2);
//Lesser than ONLY there to be able to sort colors for use in std::maps and such, has no real life meaning
bool operator<(const ColorRGBd& color, const ColorRGBd& color2);

//the + and - operator add/subtract the R G and B channels but not the alpha channel
ColorRGBf operator+(const ColorRGBf& color, const ColorRGBf& color2);
ColorRGBf operator-(const ColorRGBf& color, const ColorRGBf& color2);
//these multiplication operators with a constant don't affect the alpha channel
ColorRGBf operator*(const ColorRGBf& color, double a);
ColorRGBf operator*(double a, const ColorRGBf& color);
ColorRGBf operator/(const ColorRGBf& color, double a);
//this multiplies two colors, and the alpha channels too
ColorRGBf operator*(const ColorRGBf& color1, const ColorRGBf& color2);
//The | operator adds two color including their alpha channel
ColorRGBf operator|(const ColorRGBf& color, const ColorRGBf& color2);
//The & operator multiplies a color and also its alpha channel with a constant
ColorRGBf operator&(const ColorRGBf& color, double a);
ColorRGBf operator&(double a, const ColorRGBf& color);
//Compare
bool operator==(const ColorRGBf& color, const ColorRGBf& color2);
bool operator!=(const ColorRGBf& color, const ColorRGBf& color2);
//Lesser than ONLY there to be able to sort colors for use in std::maps and such, has no real life meaning
bool operator<(const ColorRGBf& color, const ColorRGBf& color2);

//These are #defines instead of static consts for static initialization purposes

#define RGB_Black        ColorRGB(  0,   0,   0, 255)
#define RGB_Red          ColorRGB(255,   0,   0, 255)
#define RGB_Green        ColorRGB(  0, 255,   0, 255)
#define RGB_Blue         ColorRGB(  0,   0, 255, 255)
#define RGB_Cyan         ColorRGB(  0, 255, 255, 255)
#define RGB_Magenta      ColorRGB(255,   0, 255, 255)
#define RGB_Yellow       ColorRGB(255, 255,   0, 255)
#define RGB_White        ColorRGB(255, 255, 255, 255)
#define RGB_Gray         ColorRGB(128, 128, 128, 255)
#define RGB_Grey         ColorRGB(192, 192, 192, 255)
#define RGB_Darkred      ColorRGB(128,   0,   0, 255)
#define RGB_Darkgreen    ColorRGB(  0, 128,   0, 255)
#define RGB_Darkblue     ColorRGB(  0,   0, 128, 255)
#define RGB_Darkcyan     ColorRGB(  0, 128, 128, 255)
#define RGB_Darkmagenta  ColorRGB(128,   0, 128, 255)
#define RGB_Darkyellow   ColorRGB(128, 128,   0, 255)
#define RGB_Darkgray     ColorRGB( 64,  64,  64, 255)
#define RGB_Darkgrey     ColorRGB( 96,  96,  96, 255)
#define RGB_Lightred     ColorRGB(255, 128, 128, 255)
#define RGB_Lightgreen   ColorRGB(128, 255, 128, 255)
#define RGB_Lightblue     ColorRGB(128, 128, 255, 255)
#define RGB_Lightcyan    ColorRGB(128, 255, 255, 255)
#define RGB_Lightmagenta ColorRGB(255, 128, 255, 255)
#define RGB_Lightyellow  ColorRGB(255, 255, 128, 255)
#define RGB_Lightgray    ColorRGB(224, 224, 224, 255)
#define RGB_Lightgrey    ColorRGB(240, 240, 240, 255)
#define RGB_Brightred    ColorRGB(255, 192, 192, 255)
#define RGB_Brightgreen  ColorRGB(192, 255, 192, 255)
#define RGB_Brightblue   ColorRGB(192, 192, 255, 255)
#define RGB_Brightcyan   ColorRGB(192, 255, 255, 255)
#define RGB_Brightmagenta ColorRGB(255, 192, 255, 255)
#define RGB_Brightyellow ColorRGB(255, 255, 192, 255)
#define RGB_Orange       ColorRGB(255, 166,   0, 255)
#define RGB_Pink         ColorRGB(255, 192, 204, 255)

#define RGB_Alpha ColorRGB(255, 255, 255, 192)
#define RGB_Translucent ColorRGB(255, 255, 255, 128)
#define RGB_Ghost ColorRGB(255, 255, 255, 64)
#define RGB_Invisible ColorRGB(  0,   0,   0,   0)

#define RGBd_Black        ColorRGBd(0.0, 0.0, 0.0, 1.0)
#define RGBd_Red          ColorRGBd(1.0, 0.0, 0.0, 1.0)
#define RGBd_Green        ColorRGBd(0.0, 1.0, 0.0, 1.0)
#define RGBd_Blue         ColorRGBd(0.0, 0.0, 1.0, 1.0)
#define RGBd_Cyan         ColorRGBd(0.0, 1.0, 1.0, 1.0)
#define RGBd_Magenta      ColorRGBd(1.0, 0.0, 1.0, 1.0)
#define RGBd_Yellow       ColorRGBd(1.0, 1.0, 0.0, 1.0)
#define RGBd_White        ColorRGBd(1.0, 1.0, 1.0, 1.0)
#define RGBd_Gray         ColorRGBd(0.5, 0.5, 0.5, 1.0)
#define RGBd_Grey         ColorRGBd(.75, .75, .75, 1.0)

#define RGBf_Black        ColorRGBf(0.0f, 0.0f, 0.0f, 1.0f)
#define RGBf_Red          ColorRGBf(1.0f, 0.0f, 0.0f, 1.0f)
#define RGBf_Green        ColorRGBf(0.0f, 1.0f, 0.0f, 1.0f)
#define RGBf_Blue         ColorRGBf(0.0f, 0.0f, 1.0f, 1.0f)
#define RGBf_Cyan         ColorRGBf(0.0f, 1.0f, 1.0f, 1.0f)
#define RGBf_Magenta      ColorRGBf(1.0f, 0.0f, 1.0f, 1.0f)
#define RGBf_Yellow       ColorRGBf(1.0f, 1.0f, 0.0f, 1.0f)
#define RGBf_White        ColorRGBf(1.0f, 1.0f, 1.0f, 1.0f)
#define RGBf_Gray         ColorRGBf(0.5f, 0.5f, 0.5f, 1.0f)
#define RGBf_Grey         ColorRGBf(.75f, .75f, .75f, 1.0f)


template<typename T>
struct TColorHSL255
{
  T h; //0-255
  T s; //0-255
  T l; //0-255
  T a;

  double getH() const { return h / 256.0 * 360.0; } //0-360
  double getS() const { return s / 255.0 * 100.0; }; //0-100
  double getL() const { return l / 255.0 * 100.0; }; //0-100

  void setH(double h) { this->h = (T)(h / 360.0 * 256); }
  void setS(double s) { this->s = (T)(s / 100.0 * 255); }
  void setL(double l) { this->l = (T)(l / 100.0 * 255); }
  
  TColorHSL255(T h, T s, T l)
  {
    this->h = h;
    this->s = s;
    this->l = l;
    this->a = 255;
  }
  
  TColorHSL255(T h, T s, T l, T a)
  {
    this->h = h;
    this->s = s;
    this->l = l;
    this->a = a;
  }
  
  TColorHSL255()
  {
    this->h = 0;
    this->s = 0;
    this->l = 0;
    this->a = 255;
  }
};

template<typename T>
struct TColorHSL1
{
  T h; //0-1
  T s; //0-1
  T l; //0-1
  T a;

  double getH() const { return h * 360.0; } //0-360
  double getS() const { return s * 100.0; }; //0-100
  double getL() const { return l * 100.0; }; //0-100

  void setH(double h) { this->h = (T)(h / 360.0); }
  void setS(double s) { this->s = (T)(s / 100.0); }
  void setL(double l) { this->l = (T)(l / 100.0); }
  
  TColorHSL1(T h, T s, T l)
  {
    this->h = h;
    this->s = s;
    this->l = l;
    this->a = 1.0;
  }
  
  TColorHSL1(T h, T s, T l, T a)
  {
    this->h = h;
    this->s = s;
    this->l = l;
    this->a = a;
  }
  
  TColorHSL1()
  {
    this->h = 0.0;
    this->s = 0.0;
    this->l = 0.0;
    this->a = 1.0;
  }
};

typedef TColorHSL255<int> ColorHSL;
typedef TColorHSL1<double> ColorHSLd;
typedef TColorHSL1<float> ColorHSLf;

template<typename T>
struct TColorHSV255
{
  T h; //0-255
  T s; //0-255
  T v; //0-255
  T a;

  double getH() const { return h / 256.0 * 360.0; } //0-360
  double getS() const { return s / 255.0 * 100.0; }; //0-100
  double getV() const { return v / 255.0 * 100.0; }; //0-100

  void setH(double h) { this->h = (T)(h / 360.0 * 256); }
  void setS(double s) { this->s = (T)(s / 100.0 * 255); }
  void setV(double v) { this->v = (T)(v / 100.0 * 255); }
  
  TColorHSV255(T h, T s, T v)
  {
    this->h = h;
    this->s = s;
    this->v = v;
    this->a = 255;
  }
  
  TColorHSV255(T h, T s, T v, T a)
  {
    this->h = h;
    this->s = s;
    this->v = v;
    this->a = a;
  }
  
  TColorHSV255()
  {
    this->h = 0;
    this->s = 0;
    this->v = 0;
    this->a = 255;
  }
};

template<typename T>
struct TColorHSV1
{
  T h; //0-1
  T s; //0-1
  T v; //0-1
  T a;

  double getH() const { return h * 360.0; } //0-360
  double getS() const { return s * 100.0; }; //0-100
  double getV() const { return v * 100.0; }; //0-100

  void setH(double h) { this->h = (T)(h / 360.0); }
  void setS(double s) { this->s = (T)(s / 100.0); }
  void setV(double v) { this->v = (T)(v / 100.0); }
  
  TColorHSV1(T h, T s, T v)
  {
    this->h = h;
    this->s = s;
    this->v = v;
    this->a = 1.0;
  }
  
  TColorHSV1(T h, T s, T v, T a)
  {
    this->h = h;
    this->s = s;
    this->v = v;
    this->a = a;
  }
  
  TColorHSV1()
  {
    this->h = 0.0;
    this->s = 0.0;
    this->v = 0.0;
    this->a = 1.0;
  }
};

typedef TColorHSV255<int> ColorHSV;
typedef TColorHSV1<double> ColorHSVd;
typedef TColorHSV1<float> ColorHSVf;

//CIE Lab
template<typename T>
struct TColorLab255 //NOTE: most colors here are in range 0 to 255, BUT, this one has range ~ -255 to ~ +255 for a and b (but l is still 0-255)
{
  T l; //0-255
  T a; //0-255
  T b; //0-255
  T alpha;

  double getL() const { return l / 255.0 * 100; } //0-100
  double getA() const { return a - 128; } //-128-127
  double getB() const { return b - 128; } //-128-127

  void setL(double l) { this->l = (T)(l / 100.0 * 255.0); }
  void setA(double a) { this->a = (T)(a + 128); }
  void setB(double b) { this->b = (T)(b + 128); }
  
  TColorLab255(T l, T a, T b, T alpha)
  {
    this->l = l;
    this->a = a;
    this->b = b;
    this->alpha = alpha;
  }
  TColorLab255(T l, T a, T b)
  {
    this->l = l;
    this->a = a;
    this->b = b;
    this->alpha = 255;
  }
  
  TColorLab255()
  {
    this->l = 0;
    this->a = 0;
    this->b = 0;
    this->alpha = 255;
  }
};

//CIE Lab
template<typename T>
struct TColorLab1 //NOTE: most colors here are in range 0.0 to 1.0, BUT, this one has range ~ -1.0 to ~ +1.0 for a and b (but l is still 0.0-1.0)
{
  T l; //0-1
  T a; //0-1
  T b; //0-1
  T alpha;

  double getL() const { return l * 100; } //0-100
  double getA() const { return a * 255 - 128; } //-128-127
  double getB() const { return b * 255 - 128; } //-128-127

  void setL(double l) { this->l = (T)(l / 100.0); }
  void setA(double a) { this->a = (T)((a + 128) / 255.0); }
  void setB(double b) { this->b = (T)((b + 128) / 255.0); }
  
  TColorLab1(T l, T a, T b, T alpha)
  {
    this->l = l;
    this->a = a;
    this->b = b;
    this->alpha = alpha;
  }
  TColorLab1(T l, T a, T b)
  {
    this->l = l;
    this->a = a;
    this->b = b;
    this->alpha = 1.0;
  }
  
  TColorLab1()
  {
    this->l = 0.0;
    this->a = 0.0;
    this->b = 0.0;
    this->alpha = 1.0;
  }
};

typedef TColorLab255<int> ColorLab;
typedef TColorLab1<double> ColorLabd;
typedef TColorLab1<float> ColorLabf;

//CIE XYZ
template<typename T>
struct TColorXYZ255
{
  T x; //0-255
  T y; //0-255
  T z; //0-255
  T alpha;

  double getX() const { return x / 255.0; } //0-1
  double getY() const { return y / 255.0; } //0-1
  double getZ() const { return z / 255.0; } //0-1

  void setX(double x) { this->x = (T)(x * 255.0); }
  void setY(double y) { this->y = (T)(y * 255.0); }
  void setZ(double z) { this->z = (T)(z * 255.0); }
  
  TColorXYZ255(T x, T y, T z, T alpha)
  {
    this->x = x;
    this->y = y;
    this->z = z;
    this->alpha = alpha;
  }
  TColorXYZ255(T x, T y, T z)
  {
    this->x = x;
    this->y = y;
    this->z = z;
    this->alpha = 255;
  }
  
  TColorXYZ255()
  {
    this->x = 0;
    this->y = 0;
    this->z = 0;
    this->alpha = 255;
  }
};

//CIE XYZ
template<typename T>
struct TColorXYZ1
{
  T x; //0-1
  T y; //0-1
  T z; //0-1
  T alpha;

  double getX() const { return x; } //0-1
  double getY() const { return y; } //0-1
  double getZ() const { return z; } //0-1

  void setX(double x) { this->x = (T)(x); }
  void setY(double y) { this->y = (T)(y); }
  void setZ(double z) { this->z = (T)(z); }
  
  TColorXYZ1(T x, T y, T z, T alpha)
  {
    this->x = x;
    this->y = y;
    this->z = z;
    this->alpha = alpha;
  }
  TColorXYZ1(T x, T y, T z)
  {
    this->x = x;
    this->y = y;
    this->z = z;
    this->alpha = 1.0;
  }
  
  TColorXYZ1()
  {
    this->x = 0.0;
    this->y = 0.0;
    this->z = 0.0;
    this->alpha = 1.0;
  }
};

typedef TColorXYZ255<int> ColorXYZ;
typedef TColorXYZ1<double> ColorXYZd;
typedef TColorXYZ1<float> ColorXYZf;

template<typename T>
struct TColorCMY255
{
  T c;
  T m;
  T y;
  T a;
  
  TColorCMY255(T c, T m, T y, T a)
  {
    this->c = c;
    this->m = m;
    this->y = y;
    this->a = a;
  }
  TColorCMY255(T c, T m, T y)
  {
    this->c = c;
    this->m = m;
    this->y = y;
    this->a = 255;
  }
  
  TColorCMY255()
  {
    this->c = 0;
    this->m = 0;
    this->y = 0;
    this->a = 255;
  }
};

template<typename T>
struct TColorCMY1
{
  T c;
  T m;
  T y;
  T a;
  
  TColorCMY1(T c, T m, T y, T a)
  {
    this->c = c;
    this->m = m;
    this->y = y;
    this->a = a;
  }
  TColorCMY1(T c, T m, T y)
  {
    this->c = c;
    this->m = m;
    this->y = y;
    this->a = 1.0;
  }
  
  TColorCMY1()
  {
    this->c = 0.0;
    this->m = 0.0;
    this->y = 0.0;
    this->a = 1.0;
  }
};

typedef TColorCMY255<int> ColorCMY;
typedef TColorCMY1<double> ColorCMYd;
typedef TColorCMY1<float> ColorCMYf;

template<typename T>
struct TColorCMYK255
{
  T c;
  T m;
  T y;
  T k;
  T a;
  
  TColorCMYK255(T c, T m, T y, T k, T a)
  {
    this->c = c;
    this->m = m;
    this->y = y;
    this->k = k;
    this->a = a;
  }
  
  TColorCMYK255(T c, T m, T y, T k)
  {
    this->c = c;
    this->m = m;
    this->y = y;
    this->k = k;
    this->a = 255;
  }
  
  TColorCMYK255()
  {
    this->c = 0;
    this->m = 0;
    this->y = 0;
    this->k = 0;
    this->a = 255;
  }
};

template<typename T>
struct TColorCMYK1
{
  T c;
  T m;
  T y;
  T k;
  T a;
  
  TColorCMYK1(T c, T m, T y, T k, T a)
  {
    this->c = c;
    this->m = m;
    this->y = y;
    this->k = k;
    this->a = a;
  }
  
  TColorCMYK1(T c, T m, T y, T k)
  {
    this->c = c;
    this->m = m;
    this->y = y;
    this->k = k;
    this->a = 1.0;
  }
  
  TColorCMYK1()
  {
    this->c = 0.0;
    this->m = 0.0;
    this->y = 0.0;
    this->k = 0.0;
    this->a = 1.0;
  }
};

typedef TColorCMYK255<int> ColorCMYK;
typedef TColorCMYK1<double> ColorCMYKd;
typedef TColorCMYK1<float> ColorCMYKf;

//component video
template<typename T>
struct TColorYPbPr255 //NOTE: most colors here are in range 0 to 255, BUT, this one has range -128 to +128 for pb and pr (but y is still 0-255)
{
  T y; //0-255
  T pb; //0-255
  T pr; //0-255
  T alpha;

  double getY() const { return y; } //0-255
  double getPb() const { return pb - 128; } //-128 - 127
  double getPr() const { return pr - 128; } //-128 - 127

  void setY(double y) { this->y = (T)(y); }
  void setPb(double pb) { this->pb = (T)(pb + 128); }
  void setPr(double pr) { this->pr = (T)(pr + 128); }
  
  TColorYPbPr255(T y, T pb, T pr, T alpha)
  {
    this->y = y;
    this->pb = pb;
    this->pr = pr;
    this->alpha = alpha;
  }
  TColorYPbPr255(T y, T pb, T pr)
  {
    this->y = y;
    this->pb = pb;
    this->pr = pr;
    this->alpha = 255;
  }
  
  TColorYPbPr255()
  {
    this->y = 0;
    this->pb = 0;
    this->pr = 0;
    this->alpha = 255;
  }
};

//component video
template<typename T>
struct TColorYPbPr1 //NOTE: most colors here are in range 0.0 to 1.0, BUT, this one has range -0.5 to +0.5 for pb and pr (but y is still 0.0-1.0)
{
  T y; //0-1
  T pb; //0-1
  T pr; //0-1
  T alpha;

  double getY() const { return y * 255.0; } //0-255
  double getPb() const { return pb * 255.0 - 128; } //-128 - 127
  double getPr() const { return pr * 255.0 - 128; } //-128 - 127

  void setY(double y) { this->y = (T)(y / 255.0); }
  void setPb(double pb) { this->pb = (T)((pb + 128) / 255.0); }
  void setPr(double pr) { this->pr = (T)((pr + 128) / 255.0); }
  
  TColorYPbPr1(T y, T pb, T pr, T alpha)
  {
    this->y = y;
    this->pb = pb;
    this->pr = pr;
    this->alpha = alpha;
  }
  TColorYPbPr1(T y, T pb, T pr)
  {
    this->y = y;
    this->pb = pb;
    this->pr = pr;
    this->alpha = 1.0;
  }
  
  TColorYPbPr1()
  {
    this->y = 0.0;
    this->pb = 0.0;
    this->pr = 0.0;
    this->alpha = 1.0;
  }
};

typedef TColorYPbPr255<int> ColorYPbPr;
typedef TColorYPbPr1<double> ColorYPbPrd;
typedef TColorYPbPr1<float> ColorYPbPrf;

template<typename T>
struct TColorYCbCr255 //this presents JFIF JPEG's YCbCr
{
  T y; //0-255
  T cb; //0-255
  T cr; //0-255
  T alpha;

  double getY() const { return y; } //0-255
  double getCb() const { return cb; } //0-255
  double getCr() const { return cr; } //0-255

  void setY(double y) { this->y = (T)(y); }
  void setCb(double cb) { this->cb = (T)(cb); }
  void setCr(double cr) { this->cr = (T)(cr); }
  
  TColorYCbCr255(T y, T cb, T cr, T alpha)
  {
    this->y = y;
    this->cb = cb;
    this->cr = cr;
    this->alpha = alpha;
  }
  TColorYCbCr255(T y, T cb, T cr)
  {
    this->y = y;
    this->cb = cb;
    this->cr = cr;
    this->alpha = 255;
  }
  
  TColorYCbCr255()
  {
    this->y = 0;
    this->cb = 0;
    this->cr = 0;
    this->alpha = 255;
  }
};

template<typename T>
struct TColorYCbCr1 //this presents JFIF JPEG's YCbCr
{
  T y; //0-1
  T cb; //0-1
  T cr; //0-1
  T alpha;

  double getY() const { return y * 255.0; } //0-255
  double getCb() const { return cb * 255.0; } //0-255
  double getCr() const { return cr * 255.0; } //0-255

  void setY(double y) { this->y = (T)(y / 255.0); }
  void setCb(double cb) { this->cb = (T)(cb / 255.0); }
  void setCr(double cr) { this->cr = (T)(cr / 255.0); }
  
  TColorYCbCr1(T y, T cb, T cr, T alpha)
  {
    this->y = y;
    this->cb = cb;
    this->cr = cr;
    this->alpha = alpha;
  }
  TColorYCbCr1(T y, T cb, T cr)
  {
    this->y = y;
    this->cb = cb;
    this->cr = cr;
    this->alpha = 1.0;
  }
  
  TColorYCbCr1()
  {
    this->y = 0.0;
    this->cb = 0.0;
    this->cr = 0.0;
    this->alpha = 1.0;
  }
};

typedef TColorYCbCr255<int> ColorYCbCr;
typedef TColorYCbCr1<double> ColorYCbCrd;
typedef TColorYCbCr1<float> ColorYCbCrf;

template<typename T>
struct TColorYDbDr255 //SECAM YDbDr
{
  T y; //0-255
  T db; //0-255
  T dr; //0-255
  T alpha;

  double getY() const { return y / 255.0; } //0-1
  double getDb() const { return (db / 255.0 - 0.5) * 2 * 1.333; } //-1.333 - +1.333
  double getDr() const { return (dr / 255.0 - 0.5) * 2 * 1.333; } //-1.333 - +1.333

  void setY(double y) { this->y = (T)(y * 255.0); }
  void setDb(double db) { this->db = (T)(255 * (db / 2 / 1.333 + 0.5)); }
  void setDr(double dr) { this->dr = (T)(255 * (dr / 2 / 1.333 + 0.5)); }
  
  TColorYDbDr255(T y, T db, T dr, T alpha)
  {
    this->y = y;
    this->db = db;
    this->dr = dr;
    this->alpha = alpha;
  }
  TColorYDbDr255(T y, T db, T dr)
  {
    this->y = y;
    this->db = db;
    this->dr = dr;
    this->alpha = 255;
  }
  
  TColorYDbDr255()
  {
    this->y = 0;
    this->db = 0;
    this->dr = 0;
    this->alpha = 255;
  }
};

template<typename T>
struct TColorYDbDr1 //SECAM YDbDr
{
  T y; //0-1
  T db; //0-1
  T dr; //0-1
  T alpha;

  double getY() const { return y; } //0-1
  double getDb() const { return (db - 0.5) * 2 * 1.333; } //-1.333 - +1.333
  double getDr() const { return (dr - 0.5) * 2 * 1.333; } //-1.333 - +1.333

  void setY(double y) { this->y = (T)(y); }
  void setDb(double db) { this->db = (T)(db / 2 / 1.333 + 0.5); }
  void setDr(double dr) { this->dr = (T)(dr / 2 / 1.333 + 0.5); }
  
  TColorYDbDr1(T y, T db, T dr, T alpha)
  {
    this->y = y;
    this->db = db;
    this->dr = dr;
    this->alpha = alpha;
  }
  TColorYDbDr1(T y, T db, T dr)
  {
    this->y = y;
    this->db = db;
    this->dr = dr;
    this->alpha = 1.0;
  }
  
  TColorYDbDr1()
  {
    this->y = 0.0;
    this->db = 0.0;
    this->dr = 0.0;
    this->alpha = 1.0;
  }
};

typedef TColorYDbDr255<int> ColorYDbDr;
typedef TColorYDbDr1<double> ColorYDbDrd;
typedef TColorYDbDr1<float> ColorYDbDrf;

template<typename T>
struct TColorYUV255 //PAL (television) YUV
{
  T y; //0-255
  T u; //0-255
  T v; //0-255
  T alpha;

  double getY() const { return y / 255.0; } //0-1
  double getU() const { return (u / 255.0 - 0.5) * 2 * 0.436; } //-0.436 - +0.436
  double getV() const { return (v / 255.0 - 0.5) * 2 * 0.615; } //-0.615 - +0.615

  void setY(double y) { this->y = (T)(y * 255.0); }
  void setU(double u) { this->u = (T)(255 * (u / 2 / 0.436 + 0.5)); }
  void setV(double v) { this->v = (T)(255 * (v / 2 / 0.615 + 0.5)); }
  
  TColorYUV255(T y, T u, T v, T alpha)
  {
    this->y = y;
    this->u = u;
    this->v = v;
    this->alpha = alpha;
  }
  TColorYUV255(T y, T u, T v)
  {
    this->y = y;
    this->u = u;
    this->v = v;
    this->alpha = 255;
  }
  
  TColorYUV255()
  {
    this->y = 0;
    this->u = 0;
    this->v = 0;
    this->alpha = 255;
  }
};

template<typename T>
struct TColorYUV1 //PAL (television) YUV
{
  T y; //0-1
  T u; //0-1
  T v; //0-1
  T alpha;

  double getY() const { return y; } //0-1
  double getU() const { return (u - 0.5) * 2 * 0.436; } //-0.436 - +0.436
  double getV() const { return (v - 0.5) * 2 * 0.615; } //-0.615 - +0.615

  void setY(double y) { this->y = (T)(y); }
  void setU(double u) { this->u = (T)(u / 2 / 0.436 + 0.5); }
  void setV(double v) { this->v = (T)(v / 2 / 0.615 + 0.5); }
  
  TColorYUV1(T y, T u, T v, T alpha)
  {
    this->y = y;
    this->u = u;
    this->v = v;
    this->alpha = alpha;
  }
  TColorYUV1(T y, T u, T v)
  {
    this->y = y;
    this->u = u;
    this->v = v;
    this->alpha = 1.0;
  }
  
  TColorYUV1()
  {
    this->y = 0.0;
    this->u = 0.0;
    this->v = 0.0;
    this->alpha = 1.0;
  }
};

typedef TColorYUV255<int> ColorYUV;
typedef TColorYUV1<double> ColorYUVd;
typedef TColorYUV1<float> ColorYUVf;

template<typename T>
struct TColorYIQ255 //NTSC (television) YIQ
{
  T y; //0-255
  T i; //0-255
  T q; //0-255
  T alpha;

  double getY() const { return y / 255.0; } //0-1
  double getI() const { return (i / 255.0 - 0.5) * 2 * 0.5957; } //-0.5957 - +0.5957
  double getQ() const { return (q / 255.0 - 0.5) * 2 * 0.5226; } //-0.5226 - +0.5226

  void setY(double y) { this->y = (T)(y * 255.0); }
  void setI(double i) { this->i = (T)(255 * (i / 2 / 0.5957 + 0.5)); }
  void setQ(double q) { this->q = (T)(255 * (q / 2 / 0.5226 + 0.5)); }
  
  TColorYIQ255(T y, T i, T q, T alpha)
  {
    this->y = y;
    this->i = i;
    this->q = q;
    this->alpha = alpha;
  }
  TColorYIQ255(T y, T i, T q)
  {
    this->y = y;
    this->i = i;
    this->q = q;
    this->alpha = 255;
  }
  
  TColorYIQ255()
  {
    this->y = 0;
    this->i = 0;
    this->q = 0;
    this->alpha = 255;
  }
};

template<typename T>
struct TColorYIQ1 //NTSC (television) YIQ
{
  T y; //0-1
  T i; //0-1
  T q; //0-1
  T alpha;

  double getY() const { return y; } //0-1
  double getI() const { return (i - 0.5) * 2 * 0.5957; } //-0.5957 - +0.5957
  double getQ() const { return (q - 0.5) * 2 * 0.5226; } //-0.5226 - +0.5226

  void setY(double y) { this->y = (T)(y); }
  void setI(double i) { this->i = (T)(i / 2 / 0.5957 + 0.5); }
  void setQ(double q) { this->q = (T)(q / 2 / 0.5226 + 0.5); }
  
  TColorYIQ1(T y, T i, T q, T alpha)
  {
    this->y = y;
    this->i = i;
    this->q = q;
    this->alpha = alpha;
  }
  TColorYIQ1(T y, T i, T q)
  {
    this->y = y;
    this->i = i;
    this->q = q;
    this->alpha = 1.0;
  }
  
  TColorYIQ1()
  {
    this->y = 0.0;
    this->i = 0.0;
    this->q = 0.0;
    this->alpha = 1.0;
  }
};

typedef TColorYIQ255<int> ColorYIQ;
typedef TColorYIQ1<double> ColorYIQd;
typedef TColorYIQ1<float> ColorYIQf;

//TODO: HunterLab
//TODO: Luv
//TODO: make conversion function from Lab to XYZ and vica versa, since it's already internally used by the Lab to RGB and vica versa
//TODO: idem for CMYK to/from CMY
//TODO: transformations between different RGB models (and sometimes as preparation step for some models like YPbPr etc...)

ColorHSL RGBtoHSL(const ColorRGB& colorRGB);
ColorRGB HSLtoRGB(const ColorHSL& colorHSL);
ColorHSV RGBtoHSV(const ColorRGB& colorRGB);
ColorRGB HSVtoRGB(const ColorHSV& colorHSV);
ColorCMY RGBtoCMY(const ColorRGB& colorRGB);
ColorRGB CMYtoRGB(const ColorCMY& colorCMY);
ColorCMYK RGBtoCMYK(const ColorRGB& colorRGB);
ColorRGB CMYKtoRGB(const ColorCMYK& colorCMYK);
ColorXYZ RGBtoXYZ(const ColorRGB& colorRGB);
ColorRGB XYZtoRGB(const ColorXYZ& colorXYZ);
ColorLab RGBtoLab(const ColorRGB& colorRGB);
ColorRGB LabtoRGB(const ColorLab& colorLab);
ColorYPbPr RGBtoYPbPr(const ColorRGB& colorRGB);
ColorRGB YPbPrtoRGB(const ColorYPbPr& colorYPbPr);
ColorYCbCr RGBtoYCbCr(const ColorRGB& colorRGB);
ColorRGB YCbCrtoRGB(const ColorYCbCr& colorYCbCr);
ColorYDbDr RGBtoYDbDr(const ColorRGB& colorRGB);
ColorRGB YDbDrtoRGB(const ColorYDbDr& colorYDbDr);
ColorYUV RGBtoYUV(const ColorRGB& colorRGB);
ColorRGB YUVtoRGB(const ColorYUV& colorYUV);
ColorYIQ RGBtoYIQ(const ColorRGB& colorRGB);
ColorRGB YIQtoRGB(const ColorYIQ& colorYIQ);

ColorHSLd RGBtoHSL(const ColorRGBd& colorRGB);
ColorRGBd HSLtoRGB(const ColorHSLd& colorHSL);
ColorHSVd RGBtoHSV(const ColorRGBd& colorRGB);
ColorRGBd HSVtoRGB(const ColorHSVd& colorHSV);
ColorCMYd RGBtoCMY(const ColorRGBd& colorRGB);
ColorRGBd CMYtoRGB(const ColorCMYd& colorCMY);
ColorCMYKd RGBtoCMYK(const ColorRGBd& colorRGB);
ColorRGBd CMYKtoRGB(const ColorCMYKd& colorCMYK);
ColorXYZd RGBtoXYZ(const ColorRGBd& colorRGB);
ColorRGBd XYZtoRGB(const ColorXYZd& colorXYZ);
ColorLabd RGBtoLab(const ColorRGBd& colorRGB);
ColorRGBd LabtoRGB(const ColorLabd& colorLab);
ColorYPbPrd RGBtoYPbPr(const ColorRGBd& colorRGB);
ColorRGBd YPbPrtoRGB(const ColorYPbPrd& colorYPbPr);
ColorYCbCrd RGBtoYCbCr(const ColorRGBd& colorRGB);
ColorRGBd YCbCrtoRGB(const ColorYCbCrd& colorYCbCr);
ColorYDbDrd RGBtoYDbDr(const ColorRGBd& colorRGB);
ColorRGBd YDbDrtoRGB(const ColorYDbDrd& colorYDbDr);
ColorYUVd RGBtoYUV(const ColorRGBd& colorRGB);
ColorRGBd YUVtoRGB(const ColorYUVd& colorYUV);
ColorYIQd RGBtoYIQ(const ColorRGBd& colorRGB);
ColorRGBd YIQtoRGB(const ColorYIQd& colorYIQ);

ColorHSLf RGBtoHSL(const ColorRGBf& colorRGB);
ColorRGBf HSLtoRGB(const ColorHSLf& colorHSL);
ColorHSVf RGBtoHSV(const ColorRGBf& colorRGB);
ColorRGBf HSVtoRGB(const ColorHSVf& colorHSV);
ColorCMYf RGBtoCMY(const ColorRGBf& colorRGB);
ColorRGBf CMYtoRGB(const ColorCMYf& colorCMY);
ColorCMYKf RGBtoCMYK(const ColorRGBf& colorRGB);
ColorRGBf CMYKtoRGB(const ColorCMYKf& colorCMYK);
ColorXYZf RGBtoXYZ(const ColorRGBf& colorRGB);
ColorRGBf XYZtoRGB(const ColorXYZf& colorXYZ);
ColorLabf RGBtoLab(const ColorRGBf& colorRGB);
ColorRGBf LabtoRGB(const ColorLabf& colorLab);
ColorYPbPrf RGBtoYPbPr(const ColorRGBf& colorRGB);
ColorRGBf YPbPrtoRGB(const ColorYPbPrf& colorYPbPr);
ColorYCbCrf RGBtoYCbCr(const ColorRGBf& colorRGB);
ColorRGBf YCbCrtoRGB(const ColorYCbCrf& colorYCbCr);
ColorYDbDrf RGBtoYDbDr(const ColorRGBf& colorRGB);
ColorRGBf YDbDrtoRGB(const ColorYDbDrf& colorYDbDr);
ColorYUVf RGBtoYUV(const ColorRGBf& colorRGB);
ColorRGBf YUVtoRGB(const ColorYUVf& colorYUV);
ColorYIQf RGBtoYIQ(const ColorRGBf& colorRGB);
ColorRGBf YIQtoRGB(const ColorYIQf& colorYIQ);

//For these integers, the byte order used is those for e.g. the hexadecimal notation
//of HTML and CSS colors. So RGB red is #ff0000 which is 16711680, while RGBA red
//is #ff0000ff which is 4278190335. So this is different than what Java awt Color uses!!
unsigned RGBtoINT(const ColorRGB& colorRGB);
unsigned RGBAtoINT(const ColorRGB& colorRGB);
unsigned RGBtoINT(unsigned char r, unsigned char g, unsigned char b);
unsigned RGBAtoINT(unsigned char r, unsigned char g, unsigned char b, unsigned char a);
unsigned RGBtoINT(const unsigned char* rgb);
unsigned RGBAtoINT(const unsigned char* rgba);
ColorRGB INTtoRGB(unsigned colorINT);
ColorRGB INTtoRGBA(unsigned colorINT);
void INTtoRGB(unsigned char* rgb, unsigned colorINT);
void INTtoRGBA(unsigned char* rgba, unsigned colorINT);

ColorRGB RGBdtoRGB(const ColorRGBd& color);
ColorRGBd RGBtoRGBd(const ColorRGB& color);
ColorRGB RGBftoRGB(const ColorRGBf& color);
ColorRGBf RGBtoRGBf(const ColorRGB& color);
ColorRGBd RGBftoRGBd(const ColorRGBf& color);
ColorRGBf RGBdtoRGBf(const ColorRGBd& color);

//extract single channels

//get hue (hexagon based)
int RGBtoHue(const ColorRGB& color);
float RGBtoHue(const ColorRGBf& color);
double RGBtoHue(const ColorRGBd& color);

//Uses the formula 0.3r + 0.59g + 0.11b
int RGBtoLuma(const ColorRGB& color);
float RGBtoLuma(const ColorRGBf& color);
double RGBtoLuma(const ColorRGBd& color);

ColorXYZd spectrumToXYZ(double wavelength); //wavelength should be in range 380-780 (in nm)
ColorRGBd spectrumToRGB(double wavelength); //wavelength should be in range 380-780 (in nm)
}
