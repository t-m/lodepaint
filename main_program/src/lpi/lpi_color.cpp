/*
Copyright (c) 2005-2012 Lode Vandevenne
All rights reserved.

This file is part of Lode's Programming Interface.

Lode's Programming Interface is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Lode's Programming Interface is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Lode's Programming Interface.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "lpi_color.h"

#include <cmath>
#include <algorithm> //std::min and std::max
#include <iostream>

namespace lpi
{

////////////////////////////////////////////////////////////////////////////////
//COLOR STRUCTS/////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

//Add two colors
ColorRGB operator+(const ColorRGB& color, const ColorRGB& color2)
{
  ColorRGB c;
  c.r = color.r + color2.r;
  c.g = color.g + color2.g;
  c.b = color.b + color2.b;
  return c;
}

//Subtract two colors
ColorRGB operator-(const ColorRGB& color, const ColorRGB& color2)
{
  ColorRGB c;
  c.r = color.r - color2.r;
  c.g = color.g - color2.g;
  c.b = color.b - color2.b;
  return c;
}

//Multiplying with a constant does NOT affect the alpha channel, use the "&" operator on two colors instead if you need that

//Multiplies a color with an integer
ColorRGB operator*(const ColorRGB& color, int a)
{
  ColorRGB c;
  c.r = color.r * a;
  c.g = color.g * a;
  c.b = color.b * a;
  c.a = color.a;
  return c;
}

//Multiplies a color with an integer
ColorRGB operator*(int a, const ColorRGB& color)
{
  ColorRGB c;
  c.r = color.r * a;
  c.g = color.g * a;
  c.b = color.b * a;
  c.a = color.a;
  return c;
}

//Multiplies a color with a double
ColorRGB operator*(const ColorRGB& color, double a)
{
  ColorRGB c;
  c.r = int(color.r * a);
  c.g = int(color.g * a);
  c.b = int(color.b * a);
  c.a = color.a;
  return c;
}

//Multiplies a color with a double
ColorRGB operator*(double a, const ColorRGB& color)
{
  ColorRGB c;
  c.r = int(color.r * a);
  c.g = int(color.g * a);
  c.b = int(color.b * a);
  c.a = color.a;
  return c;
}

//Multiply two colors (component by component)
ColorRGB operator*(const ColorRGB& color1, const ColorRGB& color2)
{
  ColorRGB c;
  c.r = (color1.r * color2.r) / 255;
  c.g = (color1.g * color2.g) / 255;
  c.b = (color1.b * color2.b) / 255;
  c.a = (color1.a * color2.a) / 255;
  return c;
}

//Divides a color through an integer
ColorRGB operator/(const ColorRGB& color, int a)
{
  if(a == 0) return color;
  ColorRGB c;
  c.r = color.r / a;
  c.g = color.g / a;
  c.b = color.b / a;
  c.a = color.a;
  return c;
}

//Add two colors including their alpha channel
ColorRGB operator|(const ColorRGB& color, const ColorRGB& color2)
{
  ColorRGB c;
  c.r = color.r + color2.r;
  c.g = color.g + color2.g;
  c.b = color.b + color2.b;
  c.a = color.a + color2.a;
  return c;
}

//Multiplies a color with an integer, including alpha channel
ColorRGB operator&(const ColorRGB& color, int a)
{
  ColorRGB c;
  c.r = color.r * a;
  c.g = color.g * a;
  c.b = color.b * a;
  c.a = color.a * a;
  return c;
}

//Multiplies a color with an integer, including alpha channel
ColorRGB operator&(int a, const ColorRGB& color)
{
  ColorRGB c;
  c.r = color.r * a;
  c.g = color.g * a;
  c.b = color.b * a;
  c.a = color.a * a;
  return c;
}

//Multiplies a color with a double, including alpha channel
ColorRGB operator&(const ColorRGB& color, double a)
{
  ColorRGB c;
  c.r = int(color.r * a);
  c.g = int(color.g * a);
  c.b = int(color.b * a);
  c.a = int(color.a * a);
  return c;
}

//Multiplies a color with a double, including alpha channel
ColorRGB operator&(double a, const ColorRGB& color)
{
  ColorRGB c;
  c.r = int(color.r * a);
  c.g = int(color.g * a);
  c.b = int(color.b * a);
  c.a = int(color.a * a);
  return c;
}

//Are both colors equal?
bool operator==(const ColorRGB& color, const ColorRGB& color2)
{
  return(color.r == color2.r && color.g == color2.g && color.b == color2.b && color.a == color2.a);
}

//Are both colors not equal?
bool operator!=(const ColorRGB& color, const ColorRGB& color2)
{
  return(!(color.r == color2.r && color.g == color2.g && color.b == color2.b && color.a == color2.a));
}

//no real life meaning, only for when sorting algorithm is needed
bool operator<(const ColorRGB& color, const ColorRGB& color2)
{
  return color.r != color2.r ? color.r < color2.r :
         color.g != color2.g ? color.g < color2.g :
         color.b != color2.b ? color.b < color2.b :
         color.a < color2.a;
  
}

////////////////////////////////////////////////////////////////////////////////

//Add two colors
ColorRGBd operator+(const ColorRGBd& color, const ColorRGBd& color2)
{
  ColorRGBd c;
  c.r = color.r + color2.r;
  c.g = color.g + color2.g;
  c.b = color.b + color2.b;
  return c;
}

//Subtract two colors
ColorRGBd operator-(const ColorRGBd& color, const ColorRGBd& color2)
{
  ColorRGBd c;
  c.r = color.r - color2.r;
  c.g = color.g - color2.g;
  c.b = color.b - color2.b;
  return c;
}

//Multiplying with a constant does NOT affect the alpha channel, use the "&" operator on two colors instead if you need that

//Multiplies a color with a double
ColorRGBd operator*(const ColorRGBd& color, double a)
{
  ColorRGBd c;
  c.r = color.r * a;
  c.g = color.g * a;
  c.b = color.b * a;
  c.a = color.a;
  return c;
}

//Multiplies a color with a double
ColorRGBd operator*(double a, const ColorRGBd& color)
{
  ColorRGBd c;
  c.r = color.r * a;
  c.g = color.g * a;
  c.b = color.b * a;
  c.a = color.a;
  return c;
}

//Multiply two colors (component by component)
ColorRGBd operator*(const ColorRGBd& color1, const ColorRGBd& color2)
{
  ColorRGBd c;
  c.r = (color1.r * color2.r);
  c.g = (color1.g * color2.g);
  c.b = (color1.b * color2.b);
  c.a = (color1.a * color2.a);
  return c;
}

//Divides a color through an value
ColorRGBd operator/(const ColorRGBd& color, double d)
{
  if(d == 0.0) return color;
  ColorRGBd c;
  c.r = color.r / d;
  c.g = color.g / d;
  c.b = color.b / d;
  c.a = color.a;
  return c;
}

//Add two colors including their alpha channel
ColorRGBd operator|(const ColorRGBd& color, const ColorRGBd& color2)
{
  ColorRGBd c;
  c.r = color.r + color2.r;
  c.g = color.g + color2.g;
  c.b = color.b + color2.b;
  c.a = color.a + color2.a;
  return c;
}

//Multiplies a color with a double, including alpha channel
ColorRGBd operator&(const ColorRGBd& color, double a)
{
  ColorRGBd c;
  c.r = color.r * a;
  c.g = color.g * a;
  c.b = color.b * a;
  c.a = color.a * a;
  return c;
}

//Multiplies a color with a double, including alpha channel
ColorRGBd operator&(double a, const ColorRGBd& color)
{
  ColorRGBd c;
  c.r = color.r * a;
  c.g = color.g * a;
  c.b = color.b * a;
  c.a = color.a * a;
  return c;
}

//Are both colors equal?
bool operator==(const ColorRGBd& color, const ColorRGBd& color2)
{
  return(color.r == color2.r && color.g == color2.g && color.b == color2.b && color.a == color2.a);
}

//Are both colors not equal?
bool operator!=(const ColorRGBd& color, const ColorRGBd& color2)
{
  return(!(color.r == color2.r && color.g == color2.g && color.b == color2.b && color.a == color2.a));
}

//no real life meaning, only for when sorting algorithm is needed
bool operator<(const ColorRGBd& color, const ColorRGBd& color2)
{
  return color.r != color2.r ? color.r < color2.r :
         color.g != color2.g ? color.g < color2.g :
         color.b != color2.b ? color.b < color2.b :
         color.a < color2.a;
  
}

////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////

//Add two colors
ColorRGBf operator+(const ColorRGBf& color, const ColorRGBf& color2)
{
  ColorRGBf c;
  c.r = color.r + color2.r;
  c.g = color.g + color2.g;
  c.b = color.b + color2.b;
  return c;
}

//Subtract two colors
ColorRGBf operator-(const ColorRGBf& color, const ColorRGBf& color2)
{
  ColorRGBf c;
  c.r = color.r - color2.r;
  c.g = color.g - color2.g;
  c.b = color.b - color2.b;
  return c;
}

//Multiplying with a constant does NOT affect the alpha channel, use the "&" operator on two colors instead if you need that

//Multiplies a color with a double
ColorRGBf operator*(const ColorRGBf& color, double a)
{
  ColorRGBf c;
  c.r = color.r * a;
  c.g = color.g * a;
  c.b = color.b * a;
  c.a = color.a;
  return c;
}

//Multiplies a color with a double
ColorRGBf operator*(double a, const ColorRGBf& color)
{
  ColorRGBf c;
  c.r = color.r * a;
  c.g = color.g * a;
  c.b = color.b * a;
  c.a = color.a;
  return c;
}

//Multiply two colors (component by component)
ColorRGBf operator*(const ColorRGBf& color1, const ColorRGBf& color2)
{
  ColorRGBf c;
  c.r = (color1.r * color2.r);
  c.g = (color1.g * color2.g);
  c.b = (color1.b * color2.b);
  c.a = (color1.a * color2.a);
  return c;
}

//Divides a color through an value
ColorRGBf operator/(const ColorRGBf& color, double d)
{
  if(d == 0.0) return color;
  ColorRGBf c;
  c.r = color.r / d;
  c.g = color.g / d;
  c.b = color.b / d;
  c.a = color.a;
  return c;
}

//Add two colors including their alpha channel
ColorRGBf operator|(const ColorRGBf& color, const ColorRGBf& color2)
{
  ColorRGBf c;
  c.r = color.r + color2.r;
  c.g = color.g + color2.g;
  c.b = color.b + color2.b;
  c.a = color.a + color2.a;
  return c;
}

//Multiplies a color with a double, including alpha channel
ColorRGBf operator&(const ColorRGBf& color, double a)
{
  ColorRGBf c;
  c.r = color.r * a;
  c.g = color.g * a;
  c.b = color.b * a;
  c.a = color.a * a;
  return c;
}

//Multiplies a color with a double, including alpha channel
ColorRGBf operator&(double a, const ColorRGBf& color)
{
  ColorRGBf c;
  c.r = color.r * a;
  c.g = color.g * a;
  c.b = color.b * a;
  c.a = color.a * a;
  return c;
}

//Are both colors equal?
bool operator==(const ColorRGBf& color, const ColorRGBf& color2)
{
  return(color.r == color2.r && color.g == color2.g && color.b == color2.b && color.a == color2.a);
}

//Are both colors not equal?
bool operator!=(const ColorRGBf& color, const ColorRGBf& color2)
{
  return(!(color.r == color2.r && color.g == color2.g && color.b == color2.b && color.a == color2.a));
}

//no real life meaning, only for when sorting algorithm is needed
bool operator<(const ColorRGBf& color, const ColorRGBf& color2)
{
  return color.r != color2.r ? color.r < color2.r :
         color.g != color2.g ? color.g < color2.g :
         color.b != color2.b ? color.b < color2.b :
         color.a < color2.a;
  
}

////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
//COLOR CONVERSIONS/////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////


/*
matrix is in this order:
012
345
678
xyz refers to the 3 components, e.g. rgb, XYZ, ...
o is output, i is input
*/
void triComponentMatrix(double& ox, double& oy, double& oz, double ix, double iy, double iz, double matrix[9])
{
  ox = matrix[0] * ix + matrix[1] * iy + matrix[2] * iz;
  oy = matrix[3] * ix + matrix[4] * iy + matrix[5] * iz;
  oz = matrix[6] * ix + matrix[7] * iy + matrix[8] * iz;
}

void triComponentMatrix(float& ox, float& oy, float& oz, double ix, double iy, double iz, double matrix[9])
{
  ox = matrix[0] * ix + matrix[1] * iy + matrix[2] * iz;
  oy = matrix[3] * ix + matrix[4] * iy + matrix[5] * iz;
  oz = matrix[6] * ix + matrix[7] * iy + matrix[8] * iz;
}

void triComponentMatrix(int& ox, int& oy, int& oz, double ix, double iy, double iz, double matrix[9])
{
  ox = (int)(matrix[0] * ix + matrix[1] * iy + matrix[2] * iz);
  oy = (int)(matrix[3] * ix + matrix[4] * iy + matrix[5] * iz);
  oz = (int)(matrix[6] * ix + matrix[7] * iy + matrix[8] * iz);
}


int RGBtoHue(const ColorRGB& color)
{
  int hue = 0;
  int maxColor = std::max(color.r, std::max(color.g, color.b));
  int minColor = std::min(color.r, std::min(color.g, color.b));
  
  int chroma = maxColor - minColor;
  if(chroma == 0) return 0;
  
  if(color.r == maxColor) hue = (255 * (color.g - color.b)) / chroma;
  else if(color.g == maxColor) hue = 512 + (255 * (color.b - color.r)) / chroma;
  else hue = 1024 + (255 * (color.r - color.g)) / chroma;

  hue /= 6; //to bring it to a number between 0 and 255
  if(hue < 0) hue += 256;
  
  return hue;
}

float RGBtoHue(const ColorRGBf& color)
{
  float hue = 0;
  float maxColor = std::max(color.r, std::max(color.g, color.b));
  float minColor = std::min(color.r, std::min(color.g, color.b));

  float chroma = maxColor - minColor;
  if(chroma == 0) return 0;
  
  if(color.r == maxColor) hue = (color.g - color.b) / (maxColor - minColor);
  else if(color.g == maxColor) hue = 2.0f + (color.b - color.r) / (maxColor - minColor);
  else hue = 4.0f + (color.r - color.g) / (maxColor - minColor);

  hue /= 6.0f; //to bring it to a number between 0 and 1
  if(hue < 0.0f) hue += 1.0f;

  return hue;
}

double RGBtoHue(const ColorRGBd& color)
{
  double hue = 0;
  double maxColor = std::max(color.r, std::max(color.g, color.b));
  double minColor = std::min(color.r, std::min(color.g, color.b));

  double chroma = maxColor - minColor;
  if(chroma == 0) return 0;
  
  if(color.r == maxColor) hue = (color.g - color.b) / (maxColor - minColor);
  else if(color.g == maxColor) hue = 2.0 + (color.b - color.r) / (maxColor - minColor);
  else hue = 4.0 + (color.r - color.g) / (maxColor - minColor);

  hue /= 6.0; //to bring it to a number between 0 and 1
  if(hue < 0.0) hue += 1.0;

  return hue;
}

int RGBtoLuma(const ColorRGB& color)
{
  return (int)(0.3 * color.r + 0.59 * color.g + 0.11 * color.b);
}

float RGBtoLuma(const ColorRGBf& color)
{
  return 0.3f * color.r + 0.59f * color.g + 0.11f * color.b;
}

double RGBtoLuma(const ColorRGBd& color)
{
  return 0.3 * color.r + 0.59 * color.g + 0.11 * color.b;
}



/*
These functions convert colors from one color model to another
r=red  g=green  b=blue  h=hue  s=saturation  l=lightness  v=value
*/

//Converts an RGB color to HSL color
ColorHSL RGBtoHSL(const ColorRGB& colorRGB)
{
  ColorHSL colorHSL;
  colorHSL.a = colorRGB.a;

  int maxColor = std::max(colorRGB.r, std::max(colorRGB.g, colorRGB.b));
  int minColor = std::min(colorRGB.r, std::min(colorRGB.g, colorRGB.b));

  if(minColor == maxColor) //R = G = B, so it's a shade of grey
  {
    colorHSL.h = 0; //it doesn't matter what value it has
    colorHSL.s = 0;
    colorHSL.l = colorRGB.r; //doesn't matter if you pick r, g, or b
  }
  else
  {
    colorHSL.l = (minColor + maxColor) / 2;

    int div = colorHSL.l < 128 ? (maxColor + minColor) : (512 - maxColor - minColor);
    colorHSL.s = div == 0 ? 0 : (255 * (maxColor - minColor)) / div;

    colorHSL.h = RGBtoHue(colorRGB);
  }
  
  return colorHSL;
}

//Converts an HSL color to RGB color
ColorRGB HSLtoRGB(const ColorHSL& colorHSL)
{
  ColorRGB colorRGB;
  colorRGB.a = colorHSL.a;
  int temp1, temp2, tempr, tempg, tempb;

  //If saturation is 0, the color is a shade of grey
  if(colorHSL.s == 0) colorRGB.r = colorRGB.g = colorRGB.b = colorHSL.l;
  //If saturation > 0, more complex calculations are needed
  else
  {
    //set the temporary values
    if(colorHSL.l < 128) temp2 = colorHSL.l * (256 + colorHSL.s);
    else temp2 = 256 * (colorHSL.l + colorHSL.s) - (colorHSL.l * colorHSL.s);
    temp1 = 2 * 256 * colorHSL.l - temp2;
    tempr = colorHSL.h + 256 / 3;
    if(tempr > 255) tempr -= 256;
    tempg = colorHSL.h;
    tempb = colorHSL.h - 256 / 3;
    if(tempb < 0) tempb += 256;

    //red
    if(tempr < 256 / 6) colorRGB.r = (temp1 + ((temp2 - temp1) * 6 * tempr) / 256) / 256;
    else if(tempr < 128) colorRGB.r = temp2 / 256;
    else if(tempr < 512 / 3) colorRGB.r = (temp1 + ((temp2 - temp1) * (((512 / 3) - tempr) * 6)) / 256) / 256;
    else colorRGB.r = temp1 / 256;
    
     //green
    if(tempg < 256 / 6) colorRGB.g = (temp1 + ((temp2 - temp1) * 6 * tempg) / 256) / 256;
    else if(tempg < 128) colorRGB.g = temp2 / 256;
    else if(tempg < 512 / 3) colorRGB.g = (temp1 + ((temp2 - temp1) * (((512 / 3) - tempg) * 6)) / 256) / 256;
    else colorRGB.g = temp1 / 256;

    //blue
    if(tempb < 256 / 6) colorRGB.b = (temp1 + ((temp2 - temp1) * 6 * tempb) / 256) / 256;
    else if(tempb < 128) colorRGB.b = temp2 / 256;
    else if(tempb < 512 / 3) colorRGB.b = (temp1 + ((temp2 - temp1) * (((512 / 3) - tempb) * 6)) / 256) / 256;
    else colorRGB.b = temp1 / 256;
  }

  return colorRGB;
}

//Converts an RGB color to HSV color
ColorHSV RGBtoHSV(const ColorRGB& colorRGB)
{
  ColorHSV colorHSV;
  colorHSV.a = colorRGB.a;

  int maxColor = std::max(colorRGB.r, std::max(colorRGB.g, colorRGB.b));
  int minColor = std::min(colorRGB.r, std::min(colorRGB.g, colorRGB.b));

  colorHSV.v = maxColor;

  if(maxColor == 0) //avoid division by zero when the color is black
  {
    colorHSV.s = 0;
  }
  else
  {
    colorHSV.s = (255 * (maxColor - minColor)) / maxColor;
  }

  colorHSV.h = RGBtoHue(colorRGB);

  return colorHSV;
}

//Converts an HSV color to RGB color
ColorRGB HSVtoRGB(const ColorHSV& colorHSV)
{
  ColorRGB colorRGB;
  colorRGB.a = colorHSV.a;

  //if saturation is 0, the color is a shade of grey
  if(colorHSV.s == 0) colorRGB.r = colorRGB.g = colorRGB.b = colorHSV.v;
  //if saturation > 0, more complex calculations are needed
  else
  {
    int f, p, q, t;
    int h = colorHSV.h * 6; //to bring hue to a number between (0 and 6) * 256, better for the calculations
    f = h % 256;

    p = (colorHSV.v * (255 - colorHSV.s)) / 256;
    q = (colorHSV.v * (255 - ((colorHSV.s * f) / 256))) / 256;
    t = (colorHSV.v * (255 - ((colorHSV.s * (255 - f)) / 256))) / 256;

    switch(h / 256)
    {
      case 0: colorRGB.r=colorHSV.v; colorRGB.g=t; colorRGB.b=p; break;
      case 1: colorRGB.r=q; colorRGB.g=colorHSV.v; colorRGB.b=p; break;
      case 2: colorRGB.r=p; colorRGB.g=colorHSV.v; colorRGB.b=t; break;
      case 3: colorRGB.r=p; colorRGB.g=q; colorRGB.b=colorHSV.v; break;
      case 4: colorRGB.r=t; colorRGB.g=p; colorRGB.b=colorHSV.v; break;
      default:colorRGB.r=colorHSV.v; colorRGB.g=p; colorRGB.b=q; break; //this be case 5, it's mathematically impossible for i to be something else
    }
  }
  
  return colorRGB;
}

//converts an RGB color from 3 unsigned char's to a single integer
unsigned RGBtoINT(const ColorRGB& colorRGB)
{
  return 256 * 256 * colorRGB.r + 256 * colorRGB.g + colorRGB.b;
}

unsigned RGBAtoINT(const ColorRGB& colorRGB)
{
  return 256 * 256 * 256 * colorRGB.r + 256 * 256 * colorRGB.g + 256 * colorRGB.b + colorRGB.a;
}

unsigned RGBtoINT(unsigned char r, unsigned char g, unsigned char b)
{
  return 256 * 256 * r + 256 * g + b;
}

unsigned RGBAtoINT(unsigned char r, unsigned char g, unsigned char b, unsigned char a)
{
  return 256 * 256 * 256 * r + 256 * 256 * g + 256 * b + a;
}

unsigned RGBtoINT(const unsigned char* rgb)
{
  return 256 * 256 * rgb[0] + 256 * rgb[1] + rgb[2];
}

unsigned RGBAtoINT(const unsigned char* rgba)
{
  return 256 * 256 * 256 * rgba[0] + 256 * 256 * rgba[1] + 256 * rgba[2] + rgba[3];
}

//converts an RGB color given as a single integer to three unsigned char's
ColorRGB INTtoRGB(unsigned colorINT)
{
  ColorRGB colorRGB;
  colorRGB.r = (colorINT >> 16) & 255;
  colorRGB.g = (colorINT >> 8) & 255;
  colorRGB.b = colorINT & 255;
  return colorRGB;
}

//converts an RGB color given as a single integer to three unsigned char's
ColorRGB INTtoRGBA(unsigned colorINT)
{
  ColorRGB colorRGB;
  colorRGB.r = (colorINT >> 24) & 255;
  colorRGB.g = (colorINT >> 16) & 255;
  colorRGB.b = (colorINT >> 8) & 255;
  colorRGB.a = colorINT & 255;
  return colorRGB;
}
void INTtoRGB(unsigned char* rgb, unsigned colorINT)
{
  rgb[0] = (colorINT >> 16) & 255;
  rgb[1] = (colorINT >> 8) & 255;
  rgb[2] = colorINT & 255;
}

void INTtoRGBA(unsigned char* rgba, unsigned colorINT)
{
  rgba[0] = (colorINT >> 24) & 255;
  rgba[1] = (colorINT >> 16) & 255;
  rgba[2] = (colorINT >> 8) & 255;
  rgba[3] = colorINT & 255;
}

ColorCMY RGBtoCMY(const ColorRGB& colorRGB)
{
  return ColorCMY(255 - colorRGB.r, 255 - colorRGB.g, 255 - colorRGB.b, colorRGB.a);
}

ColorRGB CMYtoRGB(const ColorCMY& colorCMY)
{
  return ColorRGB(255 - colorCMY.c, 255 - colorCMY.m, 255 - colorCMY.y, colorCMY.a);
}

ColorCMYK RGBtoCMYK(const ColorRGB& colorRGB)
{
  ColorCMYK result(255 - colorRGB.r, 255 - colorRGB.g, 255 - colorRGB.b, 255, colorRGB.a);
  if(result.c < result.k) result.k = result.c;
  if(result.m < result.k) result.k = result.m;
  if(result.y < result.k) result.k = result.y;
  if(result.k == 255)
  {
    result.c = result.m = result.y = 0;
  }
  else
  {
    result.c = (int)((result.c - result.k) / (1.0 - (result.k / 255.0)));
    result.m = (int)((result.m - result.k) / (1.0 - (result.k / 255.0)));
    result.y = (int)((result.y - result.k) / (1.0 - (result.k / 255.0)));
  }
  return result;
}

ColorRGB CMYKtoRGB(const ColorCMYK& color)
{
  double k = color.k / 255.0;
  ColorCMY colorCMY((int)(color.c * (1.0 - k) + color.k)
                  , (int)(color.m * (1.0 - k) + color.k)
                  , (int)(color.y * (1.0 - k) + color.k)
                  , color.a);
  return ColorRGB(255 - colorCMY.c, 255 - colorCMY.m, 255 - colorCMY.y, colorCMY.a);
}

ColorXYZ RGBtoXYZ(const ColorRGB& color)
{
  ColorRGBd temp(color.r / 255.0, color.g / 255.0, color.b / 255.0, 255);
  ColorXYZd temp2 = RGBtoXYZ(temp);
  return ColorXYZ((int)(temp2.x * 255.0), (int)(temp2.y * 255.0), (int)(temp2.z * 255.0), color.a);
}

ColorRGB XYZtoRGB(const ColorXYZ& color)
{
  ColorXYZd temp(color.x / 255.0, color.y / 255.0, color.z / 255.0, 255);
  ColorRGBd temp2 = XYZtoRGB(temp);
  return ColorRGB((int)(temp2.r * 255.0), (int)(temp2.g * 255.0), (int)(temp2.b * 255.0), color.alpha);
}

ColorLab RGBtoLab(const ColorRGB& color)
{
  ColorRGBd temp(color.r / 255.0, color.g / 255.0, color.b / 255.0, 255);
  ColorLabd temp2 = RGBtoLab(temp);
  return ColorLab((int)(temp2.l * 255.0), (int)(temp2.a * 255.0), (int)(temp2.b * 255.0), color.a);
}

ColorRGB LabtoRGB(const ColorLab& color)
{
  ColorLabd temp(color.l / 255.0, color.a / 255.0, color.b / 255.0, 255);
  ColorRGBd temp2 = LabtoRGB(temp);
  return ColorRGB((int)(temp2.r * 255.0), (int)(temp2.g * 255.0), (int)(temp2.b * 255.0), color.alpha);
}

ColorYPbPr RGBtoYPbPr(const ColorRGB& color)
{
  ColorYPbPr result;
  result.y = (color.r + color.g + color.b) / 3;
  result.pb = color.b - result.y;
  result.pr = color.r - result.y;
  //clamp just in case
  if(result.y < 256)
  {
    if(result.pb > 127) result.pb = 127;
    if(result.pr > 127) result.pr = 127;
  }
  if(result.y >= 0)
  {
    if(result.pb < -128) result.pb = -128;
    if(result.pr < -128) result.pr = -128;
  }
  //bring in range 0-255
  result.pb += 128;
  result.pr += 128;

  result.alpha = color.a;
  return result;
}

ColorRGB YPbPrtoRGB(const ColorYPbPr& color)
{
  ColorRGB result;
  int y = color.y;
  int pb = color.pb - 128;
  int pr = color.pr - 128;
  result.r = y + pr;
  result.g = y - pr - pb;
  result.b = y + pb;
  result.a = color.alpha;
  return result;
}

ColorYCbCr RGBtoYCbCr(const ColorRGB& color)
{
  ColorYCbCr result;
  static double matrix[9] = {0.299, 0.587, 0.114, -0.1687, -0.3313, 0.5, 0.5, -0.4187, -0.0813};
  triComponentMatrix(result.y, result.cb, result.cr, color.r, color.g, color.b, matrix);
  result.cb += 128;
  result.cr += 128;
  result.alpha = color.a;
  return result;
}

ColorRGB YCbCrtoRGB(const ColorYCbCr& color)
{
  ColorRGB result;
  static double matrix[9] = {1, 0, 1.402, 1, -0.34414, -0.71414, 1, 1.772, 0};
  triComponentMatrix(result.r, result.g, result.b, color.y, color.cb - 128, color.cr - 128, matrix);
  result.a = color.alpha;
  return result;
}

ColorYDbDr RGBtoYDbDr(const ColorRGB& color)
{
  ColorYDbDr result;
  static double matrix[9] = {0.299, 0.587, 0.114, -0.450, -0.883, 1.333, -1.333, 1.116, 0.217};
  double y, db, dr;
  triComponentMatrix(y, db, dr, color.r, color.g, color.b, matrix);
  result.y = (int)y;
  result.setDb(db / 255.0);
  result.setDr(dr / 255.0);
  result.alpha = color.a;
  return result;
}

ColorRGB YDbDrtoRGB(const ColorYDbDr& color)
{
  ColorRGB result;
  static double matrix[9] = {1, 0.000092303716148, -0.525912630661865, 1, -0.129132898890509, 0.267899328207599, 1, 0.664679059978955, -0.000079202543533};
  triComponentMatrix(result.r, result.g, result.b, color.y, color.getDb() * 255.0, color.getDr() * 255.0, matrix);
  result.a = color.alpha;
  return result;
}

ColorYUV RGBtoYUV(const ColorRGB& color)
{
  ColorYUV result;
  static double matrix[9] = {0.299, 0.587, 0.114, -0.14713, -0.28886, 0.436, 0.615, -0.51499, -0.10001};
  double y, u, v;
  triComponentMatrix(y, u, v, color.r, color.g, color.b, matrix);
  result.y = (int)y;
  result.setU(u / 255.0);
  result.setV(v / 255.0);
  result.alpha = color.a;
  return result;
}

ColorRGB YUVtoRGB(const ColorYUV& color)
{
  ColorRGB result;
  static double matrix[9] = {1, 0, 1.13983, 1, -0.39465, -0.58060, 1, 2.03211, 0};
  triComponentMatrix(result.r, result.g, result.b, color.y, color.getU() * 255.0, color.getV() * 255.0, matrix);
  result.a = color.alpha;
  return result;
}

ColorYIQ RGBtoYIQ(const ColorRGB& color)
{
  ColorYIQ result;
  static double matrix[9] = {0.299, 0.587, 0.114, 0.595716, -0.274453, -0.321263, 0.211456, -0.522591, 0.311135};
  double y, i, q;
  triComponentMatrix(y, i, q, color.r, color.g, color.b, matrix);
  result.y = (int)y;
  result.setI(i / 255.0);
  result.setQ(q / 255.0);
  result.alpha = color.a;
  return result;
}

ColorRGB YIQtoRGB(const ColorYIQ& color)
{
  ColorRGB result;
  static double matrix[9] = {1, 0.9563, 0.6210, 1, -0.2721, -0.6474, 1, -1.1070, 1.7046};
  triComponentMatrix(result.r, result.g, result.b, color.y, color.getI() * 255.0, color.getQ() * 255.0, matrix);
  result.a = color.alpha;
  return result;
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////



//Converts an RGB color to HSL color
ColorHSLd RGBtoHSL(const ColorRGBd& colorRGB)
{
  double r, g, b, h, s, l; //this function works with floats between 0 and 1
  r = colorRGB.r;
  g = colorRGB.g;
  b = colorRGB.b;

  double maxColor = std::max(r, std::max(g, b));
  double minColor = std::min(r, std::min(g, b));

  if(minColor == maxColor) //R = G = B, so it's a shade of grey
  {
    h = 0.0; //it doesn't matter what value it has
    s = 0.0;
    l = r; //doesn't matter if you pick r, g, or b
  }
  else
  {
    l = (minColor + maxColor) / 2.0;
    
    double div = l < 0.5 ? (maxColor + minColor) : (2.0 - maxColor - minColor);
    s = div == 0.0 ? 0.0 : (maxColor - minColor) / div;

    h = RGBtoHue(colorRGB);
  }
  
  ColorHSLd colorHSL;
  colorHSL.h = h;
  colorHSL.s = s;
  colorHSL.l = l;
  colorHSL.a = colorRGB.a;
  return colorHSL;
}

//Converts an HSL color to RGB color
ColorRGBd HSLtoRGB(const ColorHSLd& colorHSL)
{
  double r, g, b, h, s, l; //this function works with floats between 0 and 1
  double temp1, temp2, tempr, tempg, tempb;
  h = colorHSL.h;
  s = colorHSL.s;
  l = colorHSL.l;

  //If saturation is 0, the color is a shade of grey
  if(s == 0.0) r = g = b = l;
  //If saturation > 0, more complex calculations are needed
  else
  {
    //set the temporary values
    if(l < 0.5) temp2 = l * (1 + s);
    else temp2 = (l + s) - (l * s);
    temp1 = 2.0 * l - temp2;
    tempr=h + 1.0 / 3.0;
    if(tempr > 1.0) tempr--;
    tempg=h;
    tempb=h-1.0 / 3.0;
    if(tempb < 0.0) tempb++;

    //red
    if(tempr < 1.0 / 6.0) r = temp1 + (temp2 - temp1) * 6.0 * tempr;
    else if(tempr < 0.5) r = temp2;
    else if(tempr < 2.0 / 3.0) r = temp1 + (temp2 - temp1) * ((2.0 / 3.0) - tempr) * 6.0;
    else r = temp1;
    
     //green
    if(tempg < 1.0 / 6.0) g = temp1 + (temp2 - temp1) * 6.0 * tempg;
    else if(tempg < 0.5) g=temp2;
    else if(tempg < 2.0 / 3.0) g = temp1 + (temp2 - temp1) * ((2.0 / 3.0) - tempg) * 6.0;
    else g = temp1;

    //blue
    if(tempb < 1.0 / 6.0) b = temp1 + (temp2 - temp1) * 6.0 * tempb;
    else if(tempb < 0.5) b = temp2;
    else if(tempb < 2.0 / 3.0) b = temp1 + (temp2 - temp1) * ((2.0 / 3.0) - tempb) * 6.0;
    else b = temp1;
  }

  ColorRGBd colorRGB;
  colorRGB.r = r;
  colorRGB.g = g;
  colorRGB.b = b;
  colorRGB.a = colorHSL.a;
  return colorRGB;
}

//Converts an RGB color to HSV color
ColorHSVd RGBtoHSV(const ColorRGBd& colorRGB)
{
  double r, g, b, h, s, v; //this function works with floats between 0 and 1
  r = colorRGB.r;
  g = colorRGB.g;
  b = colorRGB.b;

  double maxColor = std::max(r, std::max(g, b));
  double minColor = std::min(r, std::min(g, b));

  v = maxColor;

  if(maxColor == 0.0) //avoid division by zero when the color is black
  {
    s = 0.0;
  }
  else
  {
    s = (maxColor - minColor) / maxColor;
  }

  h = RGBtoHue(colorRGB);

  ColorHSVd colorHSV;
  colorHSV.h = h;
  colorHSV.s = s;
  colorHSV.v = v;
  colorHSV.a = colorRGB.a;
  return colorHSV;
}

//Converts an HSV color to RGB color
ColorRGBd HSVtoRGB(const ColorHSVd& colorHSV)
{
  double r, g, b, h, s, v; //this function works with floats between 0 and 1
  h = colorHSV.h;
  s = colorHSV.s;
  v = colorHSV.v;

  //if saturation is 0, the color is a shade of grey
  if(s == 0.0) r = g = b = v;
  //if saturation > 0, more complex calculations are needed
  else
  {
    double f, p, q, t;
    int i;
    h *= 6.0; //to bring hue to a number between 0 and 6, better for the calculations
    i = (int)(floor(h)); //e.g. 2.7 becomes 2 and 3.01 becomes 3 or 4.9999 becomes 4
    f = h - floor(h); //the fractional part of h

    p = v * (1.0 - s);
    q = v * (1.0 - (s * f));
    t = v * (1.0 - (s * (1.0 - f)));

    switch(i)
    {
      case 0: r=v; g=t; b=p; break;
      case 1: r=q; g=v; b=p; break;
      case 2: r=p; g=v; b=t; break;
      case 3: r=p; g=q; b=v; break;
      case 4: r=t; g=p; b=v; break;
      default: r=v; g=p; b=q; break; //this be case 5, it's mathematically impossible for i to be something else
    }
  }
  
  ColorRGBd colorRGB;
  colorRGB.r = r;
  colorRGB.g = g;
  colorRGB.b = b;
  colorRGB.a = colorHSV.a;
  return colorRGB;
}

ColorCMYd RGBtoCMY(const ColorRGBd& colorRGB)
{
  return ColorCMYd(1.0 - colorRGB.r, 1.0 - colorRGB.g, 1.0 - colorRGB.b, colorRGB.a);
}

ColorRGBd CMYtoRGB(const ColorCMYd& colorCMY)
{
  return ColorRGBd(1.0 - colorCMY.c, 1.0 - colorCMY.m, 1.0 - colorCMY.y, colorCMY.a);
}

ColorCMYKd RGBtoCMYK(const ColorRGBd& colorRGB)
{
  ColorCMYKd result(1.0 - colorRGB.r, 1.0 - colorRGB.g, 1.0 - colorRGB.b, 1.0, colorRGB.a);
  if(result.c < result.k) result.k = result.c;
  if(result.m < result.k) result.k = result.m;
  if(result.y < result.k) result.k = result.y;
  if(result.k == 1.0)
  {
    result.c = result.m = result.y = 0.0;
  }
  else
  {
    result.c = (result.c - result.k) / (1.0 - result.k);
    result.m = (result.m - result.k) / (1.0 - result.k);
    result.y = (result.y - result.k) / (1.0 - result.k);
  }
  result.a = colorRGB.a;
  return result;
}

ColorRGBd CMYKtoRGB(const ColorCMYKd& colorCMYK)
{
  double k = colorCMYK.k;
  ColorCMYd colorCMY(colorCMYK.c * (1.0 - k) + k, colorCMYK.m * (1.0 - k) + k, colorCMYK.y * (1.0 - k) + k, colorCMYK.a);
  return ColorRGBd(1.0 - colorCMY.c, 1.0 - colorCMY.m, 1.0 - colorCMY.y, colorCMY.a);
}

ColorXYZd RGBtoXYZ_CIERGB_NoGamma(const ColorRGBd& color)
{
  ColorXYZd result;
  static double matrix[9] = {0.49, 0.31, 0.2, 0.17697, 0.81240, 0.01063, 0, 0.01, 0.99};
  triComponentMatrix(result.x, result.y, result.z, color.r, color.g, color.b, matrix);
  result.alpha = color.a;
  return result;
}

ColorRGBd XYZtoRGB_CIERGB_NoGamma(const ColorXYZd& color)
{
  ColorRGBd result;
  static double matrix[9] = {2.3646138, -0.8965406, -0.4680733, -0.5151662, 1.4264081, 0.0887581, 0.0052037, -0.0144082, 1.0092045};
  triComponentMatrix(result.r, result.g, result.b, color.x, color.y, color.z, matrix);
  result.a = color.alpha;
  return result;
}

//helper function for rgb to xyz (non linear sRGB component to linear)
static double nonLinToLinRGB(double c)
{
  if(c <= 0.04045) return c / 12.92;
  else return std::pow((c + 0.055) / (1 + 0.055), 2.4 /*note: gamma is 2.2 despite the 2.4 exponent*/);
}

ColorXYZd RGBtoXYZ(const ColorRGBd& color)
{
  ColorXYZd result;
  //linear
  double rl = nonLinToLinRGB(color.r);
  double gl = nonLinToLinRGB(color.g);
  double bl = nonLinToLinRGB(color.b);
  //matrix for linear sRGB to XYZ
  static double matrix[9] = {0.4124, 0.3576, 0.1805, 0.2126, 0.7152, 0.0722, 0.0193, 0.1192, 0.9505};
  triComponentMatrix(result.x, result.y, result.z, rl, gl, bl, matrix);
  result.alpha = color.a;
  return result;
}

//helper function for xyz to rgb (linear RGB component to sRGB)
static double linToNonLinRGB(double c)
{
  if(c <= 0.0031308) return c * 12.92;
  else return (1 + 0.055) * std::pow(c, 1.0 / 2.4 /*note: gamma is 2.2 despite the 2.4 exponent*/) - 0.055;
}

ColorRGBd XYZtoRGB(const ColorXYZd& color)
{
  ColorRGBd result;
  double rl, gl, bl;
  //matrix for XYZ to linear sRGB
  static double matrix[9] = {3.2406, -1.5372, -0.4986, -0.9689, 1.8758, 0.0415, 0.0557, -0.2040, 1.0570};
  triComponentMatrix(rl, gl, bl, color.x, color.y, color.z, matrix);
  result.r = linToNonLinRGB(rl);
  result.g = linToNonLinRGB(gl);
  result.b = linToNonLinRGB(bl);
  result.a = color.alpha;
  return result;
}

//helper function for CIE Lab color conversion
static double labf(double c)
{
  static const double delta = 6.0 / 29.0;
  static const double v = delta * delta * delta; //(6/29)^3
  static const double f = (1.0 / 3.0) / (delta * delta);
  if(c > v) return std::pow(c, 1.0 / 3.0);
  else return f * c + 16.0 / 116.0;
}

ColorLabd RGBtoLab(const ColorRGBd& color)
{
  ColorXYZd xyz = RGBtoXYZ(color);
  ColorLabd result;
  
  //normalized CIE XYZ tristimulus values of white point: those for sRGB are taken here (for "E", take 1.0 for each)
  //XYZ is "the" absolute color space, but Lab is derived from XYZ + a given white point
  static const double xn = 0.3127 / 0.3290;
  static const double yn = 0.3290 / 0.3290;
  static const double zn = 0.3583 / 0.3290;

  double fx = labf(xyz.x / xn);
  double fy = labf(xyz.y / yn);
  double fz = labf(xyz.z / zn);

  double l = 116 * fy - 16;
  double a = 500 * (fx - fy);
  double b = 200 * (fy - fz);

  result.setL(l);
  result.setA(a);
  result.setB(b);
  
  result.alpha = color.a;
  return result;
}

ColorRGBd LabtoRGB(const ColorLabd& color)
{
  ColorXYZd xyz;

  double l = color.getL();
  double a = color.getA();
  double b = color.getB();
  
  double fy = (l + 16) / 116;
  double fx = fy + a / 500;
  double fz = fy - b / 200;
  
  //normalized CIE XYZ tristimulus values of white point: those for sRGB are taken here (for "E", take 1.0 for each)
  //XYZ is "the" absolute color space, but Lab is derived from XYZ + a given white point
  static const double xn = 0.3127 / 0.3290;
  static const double yn = 0.3290 / 0.3290;
  static const double zn = 0.3583 / 0.3290;

  static const double delta = 6.0 / 29.0;
  static const double sixteens = 16.0 / 116.0;
  static const double tds = 3.0 * delta * delta; //"three delta square"
  
  xyz.x = fx > delta ? xn * fx * fx * fx : (fx - sixteens) * tds * xn;
  xyz.y = fy > delta ? yn * fy * fy * fy : (fy - sixteens) * tds * yn;
  xyz.z = fz > delta ? zn * fz * fz * fz : (fz - sixteens) * tds * zn;
  
  ColorRGBd result = XYZtoRGB(xyz);
  result.a = color.alpha;
  return result;
}

ColorYPbPrd RGBtoYPbPr(const ColorRGBd& colorRGB)
{
  ColorYPbPrd result;
  result.y = (colorRGB.r + colorRGB.g + colorRGB.b) / 3.0;
  result.pb = colorRGB.b - result.y;
  result.pr = colorRGB.r - result.y;
  result.alpha = colorRGB.a;
  //bring in range 0-1
  result.pb += 0.5;
  result.pr += 0.5;
  return result;
}

ColorRGBd YPbPrtoRGB(const ColorYPbPrd& color)
{
  ColorRGBd result;
  double y = color.y;
  double pb = color.pb - 0.5;
  double pr = color.pr - 0.5;
  result.r = y + pr;
  result.g = y - pr - pb;
  result.b = y + pb;
  result.a = color.alpha;
  return result;
}

ColorYCbCrd RGBtoYCbCr(const ColorRGBd& color)
{
  ColorYCbCrd result;
  static double matrix[9] = {0.299, 0.587, 0.114, -0.1687, -0.3313, 0.5, 0.5, -0.4187, -0.0813};
  triComponentMatrix(result.y, result.cb, result.cr, color.r, color.g, color.b, matrix);
  result.cb += 0.5;
  result.cr += 0.5;
  result.alpha = color.a;
  return result;
}

ColorRGBd YCbCrtoRGB(const ColorYCbCrd& color)
{
  ColorRGBd result;
  static double matrix[9] = {1, 0, 1.402, 1, -0.34414, -0.71414, 1, 1.772, 0};
  triComponentMatrix(result.r, result.g, result.b, color.y, color.cb - 0.5, color.cr - 0.5, matrix);
  result.a = color.alpha;
  return result;
}

ColorYDbDrd RGBtoYDbDr(const ColorRGBd& color)
{
  ColorYDbDrd result;
  static double matrix[9] = {0.299, 0.587, 0.114, -0.450, -0.883, 1.333, -1.333, 1.116, 0.217};
  double y, db, dr;
  triComponentMatrix(y, db, dr, color.r, color.g, color.b, matrix);
  result.y = y;
  result.setDb(db);
  result.setDr(dr);
  result.alpha = color.a;
  return result;
}

ColorRGBd YDbDrtoRGB(const ColorYDbDrd& color)
{
  ColorRGBd result;
  static double matrix[9] = {1, 0.000092303716148, -0.525912630661865, 1, -0.129132898890509, 0.267899328207599, 1, 0.664679059978955, -0.000079202543533};
  triComponentMatrix(result.r, result.g, result.b, color.y, color.getDb(), color.getDr(), matrix);
  result.a = color.alpha;
  return result;
}

ColorYUVd RGBtoYUV(const ColorRGBd& color)
{
  ColorYUVd result;
  static double matrix[9] = {0.299, 0.587, 0.114, -0.14713, -0.28886, 0.436, 0.615, -0.51499, -0.10001};
  double y, u, v;
  triComponentMatrix(y, u, v, color.r, color.g, color.b, matrix);
  result.y = y;
  result.setU(u);
  result.setV(v);
  result.alpha = color.a;
  return result;
}

ColorRGBd YUVtoRGB(const ColorYUVd& color)
{
  ColorRGBd result;
  static double matrix[9] = {1, 0, 1.13983, 1, -0.39465, -0.58060, 1, 2.03211, 0};
  triComponentMatrix(result.r, result.g, result.b, color.y, color.getU(), color.getV(), matrix);
  result.a = color.alpha;
  return result;
}

ColorYIQd RGBtoYIQ(const ColorRGBd& color)
{
  ColorYIQd result;
  static double matrix[9] = {0.299, 0.587, 0.114, 0.595716, -0.274453, -0.321263, 0.211456, -0.522591, 0.311135};
  double y, i, q;
  triComponentMatrix(y, i, q, color.r, color.g, color.b, matrix);
  result.y = y;
  result.setI(i);
  result.setQ(q);
  result.alpha = color.a;
  return result;
}

ColorRGBd YIQtoRGB(const ColorYIQd& color)
{
  ColorRGBd result;
  static double matrix[9] = {1, 0.9563, 0.6210, 1, -0.2721, -0.6474, 1, -1.1070, 1.7046};
  triComponentMatrix(result.r, result.g, result.b, color.y, color.getI(), color.getQ(), matrix);
  result.a = color.alpha;
  return result;
}


////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////



//Converts an RGB color to HSL color
ColorHSLf RGBtoHSL(const ColorRGBf& colorRGB)
{
  float r, g, b, h, s, l; //this function works with floats between 0 anf 1
  r = colorRGB.r;
  g = colorRGB.g;
  b = colorRGB.b;

  float maxColor = std::max(r, std::max(g, b));
  float minColor = std::min(r, std::min(g, b));

  if(minColor == maxColor) //R = G = B, so it's a shade of grey
  {
    h = 0.0; //it doesn't matter what value it has
    s = 0.0;
    l = r; //doesn't matter if you pick r, g, or b
  }
  else
  {
    l = (minColor + maxColor) / 2.0;
    
    float fiv = l < 0.5 ? (maxColor + minColor) : (2.0 - maxColor - minColor);
    s = fiv == 0.0 ? 0.0 : (maxColor - minColor) / fiv;

    h = RGBtoHue(colorRGB);
  }
  
  ColorHSLf colorHSL;
  colorHSL.h = h;
  colorHSL.s = s;
  colorHSL.l = l;
  colorHSL.a = colorRGB.a;
  return colorHSL;
}

//Converts an HSL color to RGB color
ColorRGBf HSLtoRGB(const ColorHSLf& colorHSL)
{
  float r, g, b, h, s, l; //this function works with floats between 0 anf 1
  float temp1, temp2, tempr, tempg, tempb;
  h = colorHSL.h;
  s = colorHSL.s;
  l = colorHSL.l;

  //If saturation is 0, the color is a shade of grey
  if(s == 0.0) r = g = b = l;
  //If saturation > 0, more complex calculations are needed
  else
  {
    //set the temporary values
    if(l < 0.5) temp2 = l * (1 + s);
    else temp2 = (l + s) - (l * s);
    temp1 = 2.0 * l - temp2;
    tempr=h + 1.0 / 3.0;
    if(tempr > 1.0) tempr--;
    tempg=h;
    tempb=h-1.0 / 3.0;
    if(tempb < 0.0) tempb++;

    //red
    if(tempr < 1.0 / 6.0) r = temp1 + (temp2 - temp1) * 6.0 * tempr;
    else if(tempr < 0.5) r = temp2;
    else if(tempr < 2.0 / 3.0) r = temp1 + (temp2 - temp1) * ((2.0 / 3.0) - tempr) * 6.0;
    else r = temp1;
    
     //green
    if(tempg < 1.0 / 6.0) g = temp1 + (temp2 - temp1) * 6.0 * tempg;
    else if(tempg < 0.5) g=temp2;
    else if(tempg < 2.0 / 3.0) g = temp1 + (temp2 - temp1) * ((2.0 / 3.0) - tempg) * 6.0;
    else g = temp1;

    //blue
    if(tempb < 1.0 / 6.0) b = temp1 + (temp2 - temp1) * 6.0 * tempb;
    else if(tempb < 0.5) b = temp2;
    else if(tempb < 2.0 / 3.0) b = temp1 + (temp2 - temp1) * ((2.0 / 3.0) - tempb) * 6.0;
    else b = temp1;
  }

  ColorRGBf colorRGB;
  colorRGB.r = r;
  colorRGB.g = g;
  colorRGB.b = b;
  colorRGB.a = colorHSL.a;
  return colorRGB;
}

//Converts an RGB color to HSV color
ColorHSVf RGBtoHSV(const ColorRGBf& colorRGB)
{
  float r, g, b, h, s, v; //this function works with floats between 0 anf 1
  r = colorRGB.r;
  g = colorRGB.g;
  b = colorRGB.b;

  float maxColor = std::max(r, std::max(g, b));
  float minColor = std::min(r, std::min(g, b));

  v = maxColor;

  if(maxColor == 0.0) //avoid division by zero when the color is black
  {
    s = 0.0;
  }
  else
  {
    s = (maxColor - minColor) / maxColor;
  }

  h = RGBtoHue(colorRGB);

  ColorHSVf colorHSV;
  colorHSV.h = h;
  colorHSV.s = s;
  colorHSV.v = v;
  colorHSV.a = colorRGB.a;
  return colorHSV;
}

//Converts an HSV color to RGB color
ColorRGBf HSVtoRGB(const ColorHSVf& colorHSV)
{
  float r, g, b, h, s, v; //this function works with floats between 0 anf 1
  h = colorHSV.h;
  s = colorHSV.s;
  v = colorHSV.v;

  //if saturation is 0, the color is a shade of grey
  if(s == 0.0) r = g = b = v;
  //if saturation > 0, more complex calculations are needed
  else
  {
    float f, p, q, t;
    int i;
    h *= 6.0; //to bring hue to a number between 0 anf 6, better for the calculations
    i = (int)(floor(h)); //e.g. 2.7 becomes 2 anf 3.01 becomes 3 or 4.9999 becomes 4
    f = h - floor(h); //the fractional part of h

    p = v * (1.0 - s);
    q = v * (1.0 - (s * f));
    t = v * (1.0 - (s * (1.0 - f)));

    switch(i)
    {
      case 0: r=v; g=t; b=p; break;
      case 1: r=q; g=v; b=p; break;
      case 2: r=p; g=v; b=t; break;
      case 3: r=p; g=q; b=v; break;
      case 4: r=t; g=p; b=v; break;
      default: r=v; g=p; b=q; break; //this be case 5, it's mathematically impossible for i to be something else
    }
  }
  
  ColorRGBf colorRGB;
  colorRGB.r = r;
  colorRGB.g = g;
  colorRGB.b = b;
  colorRGB.a = colorHSV.a;
  return colorRGB;
}

ColorCMYf RGBtoCMY(const ColorRGBf& colorRGB)
{
  return ColorCMYf(1.0 - colorRGB.r, 1.0 - colorRGB.g, 1.0 - colorRGB.b, colorRGB.a);
}

ColorRGBf CMYtoRGB(const ColorCMYf& colorCMY)
{
  return ColorRGBf(1.0 - colorCMY.c, 1.0 - colorCMY.m, 1.0 - colorCMY.y, colorCMY.a);
}

ColorCMYKf RGBtoCMYK(const ColorRGBf& colorRGB)
{
  ColorCMYKf result(1.0 - colorRGB.r, 1.0 - colorRGB.g, 1.0 - colorRGB.b, 1.0, colorRGB.a);
  if(result.c < result.k) result.k = result.c;
  if(result.m < result.k) result.k = result.m;
  if(result.y < result.k) result.k = result.y;
  if(result.k == 1.0)
  {
    result.c = result.m = result.y = 0.0;
  }
  else
  {
    result.c = (result.c - result.k) / (1.0 - result.k);
    result.m = (result.m - result.k) / (1.0 - result.k);
    result.y = (result.y - result.k) / (1.0 - result.k);
  }
  result.a = colorRGB.a;
  return result;
}

ColorRGBf CMYKtoRGB(const ColorCMYKf& colorCMYK)
{
  float k = colorCMYK.k;
  ColorCMYf colorCMY(colorCMYK.c * (1.0 - k) + k, colorCMYK.m * (1.0 - k) + k, colorCMYK.y * (1.0 - k) + k, colorCMYK.a);
  return ColorRGBf(1.0 - colorCMY.c, 1.0 - colorCMY.m, 1.0 - colorCMY.y, colorCMY.a);
}

ColorXYZf RGBtoXYZ(const ColorRGBf& color)
{
  ColorRGBd temp(color.r, color.g, color.b, 1);
  ColorXYZd temp2 = RGBtoXYZ(temp);
  return ColorXYZf(temp2.x, temp2.y, temp2.z, color.a);
}

ColorRGBf XYZtoRGB(const ColorXYZf& color)
{
  ColorXYZd temp(color.x, color.y, color.z, 1);
  ColorRGBd temp2 = XYZtoRGB(temp);
  return ColorRGBf(temp2.r, temp2.g, temp2.b, color.alpha);
}

ColorLabf RGBtoLab(const ColorRGBf& colorRGB)
{
  ColorRGBd temp(colorRGB.r, colorRGB.g, colorRGB.b, 1);
  ColorLabd temp2 = RGBtoLab(temp);
  return ColorLabf(temp2.l, temp2.a, temp2.b, colorRGB.a);
}

ColorRGBf LabtoRGB(const ColorLabf& colorLab)
{
  ColorLabd temp(colorLab.l, colorLab.a, colorLab.b, 1);
  ColorRGBd temp2 = LabtoRGB(temp);
  return ColorRGBf(temp2.r, temp2.g, temp2.b, colorLab.alpha);
}

ColorYPbPrf RGBtoYPbPr(const ColorRGBf& colorRGB)
{
  ColorYPbPrf result;
  result.y = (colorRGB.r + colorRGB.g + colorRGB.b) / 3.0;
  result.pb = colorRGB.b - result.y;
  result.pr = colorRGB.r - result.y;
  result.alpha = colorRGB.a;
  //bring in range 0-1
  result.pb += 0.5;
  result.pr += 0.5;
  return result;
}

ColorRGBf YPbPrtoRGB(const ColorYPbPrf& color)
{
  ColorRGBf result;
  float y = color.y;
  float pb = color.pb - 0.5;
  float pr = color.pr - 0.5;
  result.r = y + pr;
  result.g = y - pr - pb;
  result.b = y + pb;
  result.a = color.alpha;
  return result;
}
ColorYCbCrf RGBtoYCbCr(const ColorRGBf& color)
{
  ColorYCbCrf result;
  static double matrix[9] = {0.299, 0.587, 0.114, -0.1687, -0.3313, 0.5, 0.5, -0.4187, -0.0813};
  triComponentMatrix(result.y, result.cb, result.cr, color.r, color.g, color.b, matrix);
  result.cb += 0.5;
  result.cr += 0.5;
  result.alpha = color.a;
  return result;
}

ColorRGBf YCbCrtoRGB(const ColorYCbCrf& color)
{
  ColorRGBf result;
  static double matrix[9] = {1, 0, 1.402, 1, -0.34414, -0.71414, 1, 1.772, 0};
  triComponentMatrix(result.r, result.g, result.b, color.y, color.cb - 0.5, color.cr - 0.5, matrix);
  result.a = color.alpha;
  return result;
}

ColorYDbDrf RGBtoYDbDr(const ColorRGBf& color)
{
  ColorYDbDrf result;
  static double matrix[9] = {0.299, 0.587, 0.114, -0.450, -0.883, 1.333, -1.333, 1.116, 0.217};
  double y, db, dr;
  triComponentMatrix(y, db, dr, color.r, color.g, color.b, matrix);
  result.y = y;
  result.setDb(db);
  result.setDr(dr);
  result.alpha = color.a;
  return result;
}

ColorRGBf YDbDrtoRGB(const ColorYDbDrf& color)
{
  ColorRGBf result;
  static double matrix[9] = {1, 0.000092303716148, -0.525912630661865, 1, -0.129132898890509, 0.267899328207599, 1, 0.664679059978955, -0.000079202543533};
  triComponentMatrix(result.r, result.g, result.b, color.y, color.getDb(), color.getDr(), matrix);
  result.a = color.alpha;
  return result;
}

ColorYUVf RGBtoYUV(const ColorRGBf& color)
{
  ColorYUVf result;
  static double matrix[9] = {0.299, 0.587, 0.114, -0.14713, -0.28886, 0.436, 0.615, -0.51499, -0.10001};
  double y, u, v;
  triComponentMatrix(y, u, v, color.r, color.g, color.b, matrix);
  result.y = y;
  result.setU(u);
  result.setV(v);
  result.alpha = color.a;
  return result;
}

ColorRGBf YUVtoRGB(const ColorYUVf& color)
{
  ColorRGBf result;
  static double matrix[9] = {1, 0, 1.13983, 1, -0.39465, -0.58060, 1, 2.03211, 0};
  triComponentMatrix(result.r, result.g, result.b, color.y, color.getU(), color.getV(), matrix);
  result.a = color.alpha;
  return result;
}

ColorYIQf RGBtoYIQ(const ColorRGBf& color)
{
  ColorYIQf result;
  static double matrix[9] = {0.299, 0.587, 0.114, 0.595716, -0.274453, -0.321263, 0.211456, -0.522591, 0.311135};
  double y, i, q;
  triComponentMatrix(y, i, q, color.r, color.g, color.b, matrix);
  result.y = y;
  result.setI(i);
  result.setQ(q);
  result.alpha = color.a;
  return result;
}

ColorRGBf YIQtoRGB(const ColorYIQf& color)
{
  ColorRGBf result;
  static double matrix[9] = {1, 0.9563, 0.6210, 1, -0.2721, -0.6474, 1, -1.1070, 1.7046};
  triComponentMatrix(result.r, result.g, result.b, color.y, color.getI(), color.getQ(), matrix);
  result.a = color.alpha;
  return result;
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

ColorRGB RGBdtoRGB(const ColorRGBd& color)
{
  return ColorRGB((int)(color.r*255 + 0.5),(int)(color.g*255 + 0.5),(int)(color.b*255 + 0.5),(int)(color.a*255 + 0.5));
}

ColorRGBd RGBtoRGBd(const ColorRGB& color)
{
  return ColorRGBd(color.r / 255.0, color.g / 255.0, color.b / 255.0, color.a / 255.0);
}

ColorRGB RGBftoRGB(const ColorRGBf& color)
{
  return ColorRGB((int)(color.r*255 + 0.5f),(int)(color.g*255 + 0.5f),(int)(color.b*255 + 0.5f),(int)(color.a*255 + 0.5f));
}

ColorRGBf RGBtoRGBf(const ColorRGB& color)
{
  return ColorRGBf(color.r / 255.0f, color.g / 255.0f, color.b / 255.0f, color.a / 255.0f);
}

ColorRGBd RGBftoRGBd(const ColorRGBf& color)
{
  return ColorRGBd(color.r, color.g, color.b, color.a);
}

ColorRGBf RGBdtoRGBf(const ColorRGBd& color)
{
  return ColorRGBf(color.r, color.g, color.b, color.a);
}


//Table taken from specrend.c from fourmilab (public domain)
//See also: http://www.fourmilab.ch/documents/specrend/
//has x, y, z values for wavelengths 380-780nm, in steps of 5nm
//it are samples of sensitivity of the 3 color cones of the eye to wavelengths
//TODO: find a table that has at least 256 samples instead of 81
static double cie_colour_match[81][3] = {
    {0.0014,0.0000,0.0065}, {0.0022,0.0001,0.0105}, {0.0042,0.0001,0.0201},
    {0.0076,0.0002,0.0362}, {0.0143,0.0004,0.0679}, {0.0232,0.0006,0.1102},
    {0.0435,0.0012,0.2074}, {0.0776,0.0022,0.3713}, {0.1344,0.0040,0.6456},
    {0.2148,0.0073,1.0391}, {0.2839,0.0116,1.3856}, {0.3285,0.0168,1.6230},
    {0.3483,0.0230,1.7471}, {0.3481,0.0298,1.7826}, {0.3362,0.0380,1.7721},
    {0.3187,0.0480,1.7441}, {0.2908,0.0600,1.6692}, {0.2511,0.0739,1.5281},
    {0.1954,0.0910,1.2876}, {0.1421,0.1126,1.0419}, {0.0956,0.1390,0.8130},
    {0.0580,0.1693,0.6162}, {0.0320,0.2080,0.4652}, {0.0147,0.2586,0.3533},
    {0.0049,0.3230,0.2720}, {0.0024,0.4073,0.2123}, {0.0093,0.5030,0.1582},
    {0.0291,0.6082,0.1117}, {0.0633,0.7100,0.0782}, {0.1096,0.7932,0.0573},
    {0.1655,0.8620,0.0422}, {0.2257,0.9149,0.0298}, {0.2904,0.9540,0.0203},
    {0.3597,0.9803,0.0134}, {0.4334,0.9950,0.0087}, {0.5121,1.0000,0.0057},
    {0.5945,0.9950,0.0039}, {0.6784,0.9786,0.0027}, {0.7621,0.9520,0.0021},
    {0.8425,0.9154,0.0018}, {0.9163,0.8700,0.0017}, {0.9786,0.8163,0.0014},
    {1.0263,0.7570,0.0011}, {1.0567,0.6949,0.0010}, {1.0622,0.6310,0.0008},
    {1.0456,0.5668,0.0006}, {1.0026,0.5030,0.0003}, {0.9384,0.4412,0.0002},
    {0.8544,0.3810,0.0002}, {0.7514,0.3210,0.0001}, {0.6424,0.2650,0.0000},
    {0.5419,0.2170,0.0000}, {0.4479,0.1750,0.0000}, {0.3608,0.1382,0.0000},
    {0.2835,0.1070,0.0000}, {0.2187,0.0816,0.0000}, {0.1649,0.0610,0.0000},
    {0.1212,0.0446,0.0000}, {0.0874,0.0320,0.0000}, {0.0636,0.0232,0.0000},
    {0.0468,0.0170,0.0000}, {0.0329,0.0119,0.0000}, {0.0227,0.0082,0.0000},
    {0.0158,0.0057,0.0000}, {0.0114,0.0041,0.0000}, {0.0081,0.0029,0.0000},
    {0.0058,0.0021,0.0000}, {0.0041,0.0015,0.0000}, {0.0029,0.0010,0.0000},
    {0.0020,0.0007,0.0000}, {0.0014,0.0005,0.0000}, {0.0010,0.0004,0.0000},
    {0.0007,0.0002,0.0000}, {0.0005,0.0002,0.0000}, {0.0003,0.0001,0.0000},
    {0.0002,0.0001,0.0000}, {0.0002,0.0001,0.0000}, {0.0001,0.0000,0.0000},
    {0.0001,0.0000,0.0000}, {0.0001,0.0000,0.0000}, {0.0000,0.0000,0.0000}
};


ColorXYZd spectrumToXYZ(double wavelength)
{
  ColorXYZd result;
  int i = (int)((wavelength - 380) / (780 - 380) * 81 * 4);
  if(i < 0 || i >= 80 * 4)
  {
    result.x = result.y = result.z = 0;
  }
  else
  {
    int j = i / 4;
    double k = (i % 4) / 4.0;
    result.x = cie_colour_match[j][0] * (1.0 - k) + cie_colour_match[j + 1][0] * k;
    result.y = cie_colour_match[j][1] * (1.0 - k) + cie_colour_match[j + 1][1] * k;
    result.z = cie_colour_match[j][2] * (1.0 - k) + cie_colour_match[j + 1][2] * k;
  }
  result.alpha = 1.0;
  return result;
}


ColorRGBd spectrumToRGB(double wavelength)
{
  ColorRGBd result = XYZtoRGB(spectrumToXYZ(wavelength));
  result.clamp();
  return result;
}



} // namespace lpi
