/*
LodePaint

Copyright (c) 2009-2010 Lode Vandevenne
All rights reserved.

This file is part of LodePaint.

LodePaint is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LodePaint is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LodePaint.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

/*
This file contains some texture classes for LodePaint, for the various color modes.
This is based on lpi::TextureGL, similar function names are used. They
all use OpenGL.

MODE_RGBA_8: use TextureRGBA8
MODE_RGBA_32F: TextureRGBA32F
MODE_GREY_8: TextureGrey8
MODE_GREY_32F: TextureGrey32F


*/

#include "lpi/gl/lpi_texture_gl.h"
#include "lpi/gl/lpi_screen_gl.h"

extern lpi::GLContext* contextForPaintTextures;


class TextureGeneric
{
  /*
  Important:
  
  There is only one texture uploaded to the videocard per texture object.
  That means that assigning or copying texture objects should not be done, unless the original one will be destroyed and only the new one will be used.
  */
  
  protected:
  
    //variables for glTexImage2D
    GLint internalFormat; //GL_RGBA, GL_LUMINANCE, ...
    GLenum format; //GL_RGBA, GL_LUMINANCE, ...
    GLenum type; //GL_UNSIGNED_BYTE, GL_FLOAT, ...
    
    size_t bytesPerPixel;
    
    mutable bool size_valid_gl;
  
  public:
  
    struct Part
    {
      int generated_id;
      GLuint texture; //the unique OpenGL texture "name" to identify ourselves
      size_t u;
      size_t v;
      //width and height as powers of two (this will be the actual size of the buffer, because OpenGL only supports such textures)
      size_t u2;
      size_t v2;
      
      lpi::GLContext* context;
      
      int shiftx; //shift compared to topleft of main texture
      int shifty; //shift compared to topleft of main texture
      
      Part(lpi::GLContext* context);
      
      ~Part();
    };
  
  private:
    std::vector<unsigned char> buffer; //the buffer containing the image data in pc memory, can also be altered: just upload() the texture again to make the new result visible
    
    mutable std::vector<unsigned char> ubyte_buffer; //buffer used only if ALWAS_USE_GL_UNSIGNED_BYTE is true
    
    //width and height of the texture
    size_t u;
    size_t v;
    //width and height as powers of two (this will be the actual size of the buffer, because OpenGL only supports such textures)
    size_t u2;
    size_t v2;
    
    size_t partsx; //num parts in x direction
    size_t partsy; //num parts in y direction
    mutable std::vector<Part> parts;
    
    lpi::GLContext* context;
    
    
  public:

    //divide texture in multiple ones if size is larger than this so that all video hardware can support it (also reduces memory usage for non power of two textures)
    static const size_t MAXX = 512;
    static const size_t MAXY = 512;
  
    TextureGeneric(GLint internalFormat, GLenum format, GLenum type, size_t bytesPerPixel);
    virtual ~TextureGeneric();
    
    virtual void setSize(size_t u, size_t v);
    //width and height of the texture
    virtual size_t getU() const {return u;}
    virtual size_t getV() const {return v;}
    //width and height as powers of two (this will be the actual size of the buffer, because OpenGL only supports such textures)
    virtual size_t getU2() const {return u2;}
    virtual size_t getV2() const {return v2;}
    
    virtual void update() { upload(); }
    virtual void updatePartial(int x0, int y0, int x1, int y1) { uploadPartial(x0, y0, x1, y1); }
    
    virtual void* getBufferGeneric()
    {
      return (void*)(&buffer[0]);
    }
    
    virtual const void* getBufferGeneric() const
    {
      return (const void*)(&buffer[0]);
    }
    
    void bind(bool smoothing, size_t index) const; //set this texture for OpenGL
    
    size_t getNumParts() const { return parts.size(); }
    const Part& getPart(size_t i) const { return parts[i]; }
    
    bool updateForNewOpenGLContextIfNeeded() const; //the number can be gotten from the ScreenGL with getOpenGLContextDestroyedNumber()
    
  private:
    
    void makeBuffer(); //creates memory for the buffer
    void makeBufferGL() const; //creates memory for the buffer

    void uploadPartial(int x0, int y0, int x1, int y1); //todo: make use of this for efficiency
    void upload() const; //sets the texture to openGL with correct datatype and such. Everytime something changes in the data in the buffer, upload it again to let the videocard/API know the changes. Also, use upload AFTER a screen is already set! And when the screen changes resolution, everything has to be uploaded again.
    void reupload() const; //call this after you changed the screen (causing the textures to be erased from the video card)
    
    //get/set buffer that has the (possible non power of two) size of the wanted image (u * v RGBA pixels)
    template<typename T>
    void getTextAlignedBuffer(std::vector<T>& out)
    {
      out.clear();
      for(size_t y = 0; y < v; y++)
      {
        out.insert(out.end(), buffer.begin() + bytesPerPixel * y * u2, buffer.begin() + bytesPerPixel * y * u2 + bytesPerPixel * u);
      }
    }
    
    template<typename T>
    void setTextAlignedBuffer(const std::vector<T>& in)
    {
      for(size_t y = 0; y < v; y++)
      {
        std::copy(in.begin() + bytesPerPixel * y * u, in.begin() + bytesPerPixel * y * u + bytesPerPixel * u, buffer.begin() + bytesPerPixel * y * u2);
      }
    }
};

template<typename T>
class TextureGenericT : public TextureGeneric
{
  public:

    TextureGenericT(GLint internalFormat, GLenum format, GLenum type, size_t bytesPerPixel)
    : TextureGeneric(internalFormat, format, type, bytesPerPixel)
    {
    }
  
    virtual T* getBuffer()
    {
      return (T*)(getBufferGeneric());
    }

    virtual const T* getBuffer() const
    {
      return (const T*)(getBufferGeneric());
    }
};

class TextureRGBA8 : public TextureGenericT<unsigned char>
{
  public:
    TextureRGBA8()
    : TextureGenericT<unsigned char>(4, GL_RGBA, GL_UNSIGNED_BYTE, 4)
    {
    }
};

class TextureRGBA32F : public TextureGenericT<float>
{
  public:
    TextureRGBA32F()
    : TextureGenericT<float>(4, GL_RGBA, GL_FLOAT, 16)
    {
    }
};

class TextureGrey8 : public TextureGenericT<unsigned char>
{
  public:
    TextureGrey8()
    : TextureGenericT<unsigned char>(1, GL_LUMINANCE, GL_UNSIGNED_BYTE, 1)
    {
    }
    
    TextureGrey8(GLint internalFormat, GLenum format) //set internalFormat and format to GL_ALPHA for mask
    : TextureGenericT<unsigned char>(internalFormat, format, GL_UNSIGNED_BYTE, 1)
    {
    }
};

class TextureGrey32F : public TextureGenericT<float>
{
  public:
    TextureGrey32F()
    : TextureGenericT<float>(1, GL_LUMINANCE, GL_FLOAT, 4)
    {
    }
    
    TextureGrey32F(GLint internalFormat, GLenum format) //set internalFormat and format to GL_ALPHA for mask
    : TextureGenericT<float>(internalFormat, format, GL_FLOAT, 4)
    {
    }
};


//TODO: fix this to use ColorRGBd/f for floating point texture. Probably don't make it a template anymore.
template<typename T>
void createTextureRGBA(TextureGeneric* texture, size_t w, size_t h, const lpi::ColorRGB& color)
{
  texture->setSize(w, h);
  T* buffer = (T*)texture->getBufferGeneric();
  size_t u2 = texture->getU2();
  for(size_t y = 0; y < h; y++)
  for(size_t x = 0; x < w; x++)
  {
    buffer[y * u2 * 4 + x * 4 + 0] = (T)color.r;
    buffer[y * u2 * 4 + x * 4 + 1] = (T)color.g;
    buffer[y * u2 * 4 + x * 4 + 2] = (T)color.b;
    buffer[y * u2 * 4 + x * 4 + 3] = (T)color.a;
  }
}

template<typename T>
void createTextureGrey(TextureGeneric* texture, size_t w, size_t h, T value)
{
  texture->setSize(w, h);
  T* buffer = (T*)texture->getBufferGeneric();
  size_t u2 = texture->getU2();
  for(size_t y = 0; y < h; y++)
  for(size_t x = 0; x < w; x++)
  {
    buffer[y * u2 + x] = value;
  }
}




void drawTexture(lpi::ScreenGL* screen, const TextureGeneric* texture, int x, int y, const lpi::ColorRGB& colorMod = lpi::RGB_White);
void drawTextureSized(lpi::ScreenGL* screen, const TextureGeneric* texture, int x, int y, size_t sizex, size_t sizey, const lpi::ColorRGB& colorMod = lpi::RGB_White);
void drawTextureSizedCentered(lpi::ScreenGL* screen, const TextureGeneric* texture, int x, int y, size_t sizex, size_t sizey, const lpi::ColorRGB& colorMod = lpi::RGB_White);

void getAlignedBuffer(std::vector<unsigned char>& buffer, const TextureRGBA8* texture);
void setAlignedBuffer(TextureRGBA8* texture, const unsigned char* buffer, bool update = true); //assumes texture has already correct size and buffer contains that many pixels
void getAlignedBuffer(std::vector<float>& buffer, const TextureRGBA32F* texture);
void setAlignedBuffer(TextureRGBA32F* texture, const float* buffer, bool update = true); //assumes texture has already correct size and buffer contains that many pixels
void getAlignedBuffer(std::vector<unsigned char>& buffer, const TextureGrey8* texture);
void setAlignedBuffer(TextureGrey8* texture, const unsigned char* buffer, bool update = true); //assumes texture has already correct size and buffer contains that many pixels
void getAlignedBuffer(std::vector<float>& buffer, const TextureGrey32F* texture);
void setAlignedBuffer(TextureGrey32F* texture, const float* buffer, bool update = true); //assumes texture has already correct size and buffer contains that many pixels

void copyTexture(TextureRGBA8* dest, const TextureRGBA8* source);
void copyTexture(TextureRGBA32F* dest, const TextureRGBA32F* source);
void copyTexture(TextureGrey8* dest, const TextureGrey8* source);
void copyTexture(TextureGrey32F* dest, const TextureGrey32F* source);

void setPixel(TextureRGBA8* texture, int x, int y, const lpi::ColorRGB& color);
lpi::ColorRGB getPixel(const TextureRGBA8* texture, int x, int y);
void setPixel(TextureRGBA32F* texture, int x, int y, const lpi::ColorRGBd& color);
lpi::ColorRGBd getPixel(const TextureRGBA32F* texture, int x, int y);
void setPixel(TextureGrey8* texture, int x, int y, int color);
int getPixel(const TextureGrey8* texture, int x, int y);
void setPixel(TextureGrey32F* texture, int x, int y, float color);
float getPixel(const TextureGrey32F* texture, int x, int y);
