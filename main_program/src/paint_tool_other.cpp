/*
LodePaint

Copyright (c) 2009-2010 Lode Vandevenne
All rights reserved.

This file is part of LodePaint.

LodePaint is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LodePaint is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LodePaint.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "paint_tool_other.h"

#include "lpi/gui/lpi_gui_color.h"
#include "lpi/lpi_math.h"
#include "lpi/lpi_translate.h"

#include "imalg/floodfill.h"

#include "paint_algos.h"
#include "paint_gui.h"

#include <iostream>

////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////

void Tool_Pan::handle(const lpi::IInput& input, Paint& paint)
{
  if(paint.mouseGrabbed(input))
  {
    paint.move(-paint.mouseGetGrabDiffX(input), -paint.mouseGetGrabDiffY(input));
  }
}

////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////

Tool_FloodFill::Tool_FloodFill(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom)
: ITool(settings)
, justdone(false)
, tolerance(0.0)
, tolerance_ignores_alpha(false)
{
  dialog.addControl(_t("Tolerance"), new lpi::gui::DynamicSliderSpinner<double>(&tolerance, 0.0, 1.0, 0.01, geom));
  dialog.addControl(_t("Tol. Ignores A."), new lpi::gui::DynamicCheckbox(&tolerance_ignores_alpha));
}

void Tool_FloodFill::handle(const lpi::IInput& input, Paint& paint)
{
  if(paint.pressed(input, lpi::LMB) || paint.pressed(input, lpi::RMB))
  {
    
    int ux0, uy0, ux1, uy1;
    
    int drawx, drawy;
    paint.screenToPixel(drawx, drawy, input);
    
    justdone = true;
    
    if(paint.getColorMode() == MODE_RGBA_8)
    {
      std::vector<unsigned char> working;
      getAlignedBuffer(working, &paint.canvasRGBA8);

      lpi::ColorRGB color = input.mouseButtonDown(lpi::LMB) ? settings.leftColor255() : settings.rightColor255();
      
      RGBA8* ibuffer = (RGBA8*)(&working[0]);
      RGBA8 newColor = { { (unsigned char)color.r, (unsigned char)color.g, (unsigned char)color.b, (unsigned char)color.a } };


      RGBA8 oldColor = ibuffer[paint.canvasRGBA8.getU() * drawy + drawx];

      floodFillRGBA8(ux0, uy0, ux1, uy1, ibuffer, paint.canvasRGBA8.getU(), paint.canvasRGBA8.getV(), paint.canvasRGBA8.getU(), drawx, drawy, 0, 0, paint.canvasRGBA8.getU(), paint.canvasRGBA8.getV(), newColor, oldColor, (int)(tolerance * 255), tolerance_ignores_alpha);
      storePartialTextureInUndo(tempundo, paint.canvasRGBA8, ux0, uy0, ux1, uy1);
      RGBA8* obuffer = (RGBA8*)(paint.canvasRGBA8.getBuffer());
      for(int y = uy0; y < uy1; y++)
      for(int x = ux0; x < ux1; x++)
      {
        size_t iindex = paint.canvasRGBA8.getU() * y + x;
        size_t oindex = paint.canvasRGBA8.getU2() * y + x;
        obuffer[oindex] = ibuffer[iindex];
      }
    }
    else if(paint.getColorMode() == MODE_RGBA_32F)
    {
      std::vector<float> working;
      getAlignedBuffer(working, &paint.canvasRGBA32F);

      lpi::ColorRGBd colord = input.mouseButtonDown(lpi::LMB) ? settings.leftColor : settings.rightColor;
      
      RGBA32F* ibuffer = (RGBA32F*)(&working[0]);
      RGBA32F newColor = { { (float)colord.r, (float)colord.g, (float)colord.b, (float)colord.a } };

      RGBA32F oldColor = ibuffer[paint.canvasRGBA8.getU() * drawy + drawx];

      floodFillRGBA32F(ux0, uy0, ux1, uy1, ibuffer, paint.canvasRGBA32F.getU(), paint.canvasRGBA32F.getV(), paint.canvasRGBA32F.getU(), drawx, drawy, 0, 0, paint.canvasRGBA32F.getU(), paint.canvasRGBA32F.getV(), newColor, oldColor, tolerance, tolerance_ignores_alpha);
      storePartialTextureInUndo(tempundo, paint.canvasRGBA32F, ux0, uy0, ux1, uy1);
      RGBA32F* obuffer = (RGBA32F*)(paint.canvasRGBA32F.getBuffer());
      for(int y = uy0; y < uy1; y++)
      for(int x = ux0; x < ux1; x++)
      {
        size_t iindex = paint.canvasRGBA32F.getU() * y + x;
        size_t oindex = paint.canvasRGBA32F.getU2() * y + x;
        obuffer[oindex] = ibuffer[iindex];
      }
    }
    
    paint.uploadPartial(ux0, uy0, ux1, uy1);
  }
}

bool Tool_FloodFill::done()
{
  bool result = justdone;
  justdone = false;
  return result;
}

void Tool_FloodFill::getUndoState(UndoState& state)
{
  state.swap(tempundo);
  tempundo.clear();
}

void Tool_FloodFill::undo(Paint& paint, UndoState& state)
{
  UndoStateIndex index;
  UndoState redostate;
  if(paint.getColorMode() == MODE_RGBA_8) readPartialTextureFromUndo(paint.canvasRGBA8, redostate, state, index);
  else if(paint.getColorMode() == MODE_RGBA_32F) readPartialTextureFromUndo(paint.canvasRGBA32F, redostate, state, index);
  state.swap(redostate);
  
}

void Tool_FloodFill::redo(Paint& paint, UndoState& state)
{
  UndoStateIndex index;
  UndoState undostate;
  if(paint.getColorMode() == MODE_RGBA_8) readPartialTextureFromUndo(paint.canvasRGBA8, undostate, state, index);
  else if(paint.getColorMode() == MODE_RGBA_32F) readPartialTextureFromUndo(paint.canvasRGBA32F, undostate, state, index);
  state.swap(undostate);
}

void Tool_FloodFill::draw(lpi::gui::IGUIDrawer& drawer, Paint& paint)
{
  drawCommonCrosshair(drawer, paint, 0, 0);
}

////////////////////////////////////////////////////////////////////////////////

Tool_ColorPicker::Tool_ColorPicker(GlobalToolSettings& settings, ColorWindow& colorWindow)
: ITool(settings)
, colorWindow(colorWindow)
{
}

void Tool_ColorPicker::handle(const lpi::IInput& input, Paint& paint)
{
  int drawx, drawy;
  paint.screenToPixel(drawx, drawy, input);
  
  lpi::ColorRGBd color;
  
  if(paint.mouseDownHere(input, lpi::LMB) || paint.mouseDownHere(input, lpi::RMB))
  if(drawx >= 0 && drawy >= 0 && drawx < (int)paint.getU() && drawy < (int)paint.getV())
  {
    if(paint.getColorMode() == MODE_RGBA_8)
    {
      color = lpi::RGBtoRGBd(getPixel(&paint.canvasRGBA8, drawx, drawy));
    }
    else if(paint.getColorMode() == MODE_RGBA_32F)
    {
      color = getPixel(&paint.canvasRGBA32F, drawx, drawy);
    }
    else if(paint.getColorMode() == MODE_GREY_8)
    {
      int c = getPixel(&paint.canvasGrey8, drawx, drawy);
      color = lpi::ColorRGBd(c/255.0, c/255.0, c/255.0, 1.0);
    }
    else if(paint.getColorMode() == MODE_GREY_32F)
    {
      float c = getPixel(&paint.canvasGrey32F, drawx, drawy);
      color = lpi::ColorRGBd(c, c, c, 1.0);
    }
    
    if(paint.mouseDownHere(input, lpi::LMB)) colorWindow.setLeftColor(color);
    if(paint.mouseDownHere(input, lpi::RMB)) colorWindow.setRightColor(color);
  }
}

void Tool_ColorPicker::draw(lpi::gui::IGUIDrawer& drawer, Paint& paint)
{
  if(paint.mouseOver(drawer.getInput()))
  {
    drawCommonCrosshair(drawer, paint, 0, 0);
    drawOwnIcon(drawer, paint, 0, 16);
  }
}

////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////

Tool_SelRect::Tool_SelRect(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom)
: ITool(settings)
, wasup(true)
, wasdown(false)
, grabbed(false)
, justdone(false)
{
  dialog.addControl(_t("Alpha As Opacity"), new lpi::gui::DynamicCheckbox(&settings.other.alpha_as_opacity));
  
  std::vector<SelectionBGType> shapes; shapes.push_back(BG_BG); shapes.push_back(BG_FG); shapes.push_back(BG_TRANSPARENT); shapes.push_back(BG_AUTO);
  std::vector<std::string> names; names.push_back(_t("BG Color")); names.push_back(_t("FG Color")); names.push_back(_t("Transparent")); names.push_back(_t("Auto"));
  dialog.addControl(_t("Background"), new lpi::gui::DynamicEnum<SelectionBGType>(&settings.other.selection_bg_type, names, shapes, geom));
}

template<typename T>
T getMostCommonValue(const std::vector<T> values)
{
  std::map<T, int> counts;
  for(size_t i = 0; i < values.size(); i++)
  {
    counts[values[i]]++;
  }
  
  typename std::map<T, int>::iterator it;
  int highest = 0;
  T result;
  for(it = counts.begin(); it != counts.end(); it++)
  {
    if(it->second > highest)
    {
      result = it->first;
      highest = it->second;
    }
  }
  
  if(highest == 1) result = values[0]; //use predictable one in this case
  
  return result;
}

lpi::ColorRGB Tool_SelRect::getAutoBGColor(const Paint& paint) const
{
  const unsigned char* buffer = paint.canvasRGBA8.getBuffer();
  size_t u = paint.canvasRGBA8.getU();
  size_t v = paint.canvasRGBA8.getV();
  size_t u2 = paint.canvasRGBA8.getU2();
  
  lpi::ColorRGB result(255, 255, 255, 255);
  
  std::vector<lpi::ColorRGB> colors;
  for(size_t i = 0; i < 4; i++)
  {
    size_t x = (i % 2 == 0) ? paint.sel.x0 - 1 : paint.sel.x1 + 1;
    size_t y = ((i >> 1) % 2 == 0) ? paint.sel.y0 - 1 : paint.sel.y1 + 1;
    
    if(/*x >= 0 && */x < u && /*y >= 0 && */y < v)
    {
      size_t index = y * u2 * 4 + x * 4;
      colors.push_back(lpi::ColorRGB(buffer[index + 0], buffer[index + 1], buffer[index + 2], buffer[index + 3]));
    }
  }
  
  if(colors.size() > 0)
  {
    result = getMostCommonValue(colors);
  }
  
  return result;
}

void Tool_SelRect::addSelInfoToUndo(UndoState& state, const Paint& paint)
{
  state.addInt(paint.sel.x0);
  state.addInt(paint.sel.y0);
  state.addInt(paint.sel.x1);
  state.addInt(paint.sel.y1);
  state.addBool(settings.other.alpha_as_opacity);
}

void Tool_SelRect::handle(const lpi::IInput& input, Paint& paint)
{
  paint.floatsel.treat_alpha_as_opacity = settings.other.alpha_as_opacity; //a bit stupid, later this will probably become an option in a tool dialog instead of two separate selection tools
  
  bool down = paint.mouseGrabbed(input, lpi::LMB);

  int drawx, drawy;
  paint.screenToPixel(drawx, drawy, input);
  
  bool inside = drawx >= paint.sel.x0 && drawx < paint.sel.x1 && drawy >= paint.sel.y0 && drawy < paint.sel.y1;
  
  if(down)
  {
    if((wasup && inside) || grabbed)
    {
      if(wasup)
      {
        grabbed = true;
        if(!paint.floatsel.active)
        {
          lpi::ColorRGB bgcolor = settings.rightColor255();
          switch(settings.other.selection_bg_type)
          {
            case BG_BG: bgcolor = settings.rightColor255(); break;
            case BG_FG: bgcolor = settings.leftColor255(); break;
            case BG_TRANSPARENT: bgcolor = lpi::ColorRGB(0,0,0,0); break;
            case BG_AUTO: bgcolor = getAutoBGColor(paint); break;
          }
          
          bool copy = input.keyDown(SDLK_LCTRL);
          tempundo.addInt((int)(copy ? SELOP_FLOAT_AND_COPY : SELOP_FLOAT));
          addSelInfoToUndo(tempundo, paint);
          if(!copy) storeColorInUndo(tempundo, bgcolor);
          justdone = true;
          paint.makeSelectionFloating(bgcolor, copy);
        }
        else if(input.keyDown(SDLK_LCTRL))
        {
          tempundo.addInt((int)SELOP_COPY);
          addSelInfoToUndo(tempundo, paint);
          storePartialTextureInUndo(tempundo, paint.canvasRGBA8, paint.sel.x0, paint.sel.y0, paint.sel.x1, paint.sel.y1);
          justdone = true;
          paint.copyFloatSelection();
        }
        grabx = drawx;
        graby = drawy;
      }
      else
      {
        int diffx = drawx - grabx;
        int diffy = drawy - graby;
        grabx = drawx;
        graby = drawy;
        paint.sel.x0 += diffx;
        paint.sel.y0 += diffy;
        paint.sel.x1 += diffx;
        paint.sel.y1 += diffy;
      }
    }
    else
    {
      if(wasup)
      {
        if(paint.floatsel.active)
        {
          tempundo.addInt((int)SELOP_UNFLOAT);
          addSelInfoToUndo(tempundo, paint);
          storeCompleteTextureInUndo(tempundo, paint.floatsel.texture);
          storePartialTextureInUndo(tempundo, paint.canvasRGBA8, paint.sel.x0, paint.sel.y0, paint.sel.x1, paint.sel.y1);
          justdone = true;
          paint.unFloatSelection();
        }
        else
        {
          tempundo.addInt((int)(SELOP_CHANGE_COOR_WHILE_NOT_FLOATING));
          addSelInfoToUndo(tempundo, paint);
          justdone = true;
        }
        paint.sel.x0 = drawx;
        paint.sel.y0 = drawy;
      }
      paint.sel.x1 = drawx;
      paint.sel.y1 = drawy;
    }

    wasup = false;
    wasdown = true;
  }
  else
  {
    grabbed = false;
    if(inside)
    {
    }
    else
    {
      if(wasdown)
      {
        //paint.sel.x1 = drawx;
        //paint.sel.y1 = drawy;
        if(paint.sel.x1 < paint.sel.x0) std::swap(paint.sel.x0, paint.sel.x1);
        if(paint.sel.y1 < paint.sel.y0) std::swap(paint.sel.y0, paint.sel.y1);
        
      }
    }

    wasup = true;
    wasdown = false;
  }
}

void Tool_SelRect::undo(Paint& paint, UndoState& state)
{
  
  UndoStateIndex index;
  UndoSubOp subop = (UndoSubOp)(state.getInt(index.iint++));

  UndoState temp;
  temp.addInt((int)subop);
  addSelInfoToUndo(temp, paint);

  int ux0 = state.getInt(index.iint++);
  int uy0 = state.getInt(index.iint++);
  int ux1 = state.getInt(index.iint++);
  int uy1 = state.getInt(index.iint++);
  bool alpha_as_opacity = state.getBool(index.ibool++);
  paint.sel.x0 = ux0;
  paint.sel.y0 = uy0;
  paint.sel.x1 = ux1;
  paint.sel.y1 = uy1;
  paint.floatsel.treat_alpha_as_opacity = alpha_as_opacity;
  switch(subop)
  {
    case SELOP_CHANGE_COOR_WHILE_NOT_FLOATING:
    {
      state.swap(temp);
      break; //everything already done above
    }
    case SELOP_FLOAT:
    {
      readColorFromUndo(state, index); //just read to make index correct, return value not used
      paint.floatsel.treat_alpha_as_opacity = false; //never treat alpha as opacity here, because we reverse the process of how the texture with alpha channel became a floating one
      paint.unFloatSelection();
      paint.floatsel.active = false;
      break;
    }
    case SELOP_FLOAT_AND_COPY:
    {
      paint.floatsel.active = false;
      break;
    }
    case SELOP_UNFLOAT:
    {
      paint.makeFloatingSelection(paint.sel.x1 - paint.sel.x0, paint.sel.y1 - paint.sel.y0, paint.sel.x0, paint.sel.y0);
      readCompleteTextureFromUndo(paint.floatsel.texture, state, index);
      readPartialTextureFromUndo(paint.canvasRGBA8, state, index);
      paint.floatsel.active = true;
      break;
    }
    case SELOP_COPY:
    {
      readPartialTextureFromUndo(paint.canvasRGBA8, state, index);
      break;
    }
  }
}

void Tool_SelRect::redo(Paint& paint, UndoState& state)
{
  UndoStateIndex index;
  UndoSubOp subop = (UndoSubOp)(state.getInt(index.iint++));
  
  UndoState temp;
  temp.addInt((int)subop);
  addSelInfoToUndo(temp, paint);
  
  int ux0 = state.getInt(index.iint++);
  int uy0 = state.getInt(index.iint++);
  int ux1 = state.getInt(index.iint++);
  int uy1 = state.getInt(index.iint++);
  bool alpha_as_opacity = state.getBool(index.ibool++);
  paint.sel.x0 = ux0;
  paint.sel.y0 = uy0;
  paint.sel.x1 = ux1;
  paint.sel.y1 = uy1;
  paint.floatsel.treat_alpha_as_opacity = alpha_as_opacity;
  switch(subop)
  {
    case SELOP_CHANGE_COOR_WHILE_NOT_FLOATING:
    {
      state.swap(temp);
      break; //everything already done above
    }
    case SELOP_FLOAT:
    {
      lpi::ColorRGB color = readColorFromUndo(state, index);
      paint.makeSelectionFloating(color, false);
      break;
    }
    case SELOP_FLOAT_AND_COPY:
    {
      paint.makeSelectionFloating(lpi::RGB_Black, true);
      break;
    }
    case SELOP_UNFLOAT:
    {
      paint.unFloatSelection();
      break;
    }
    case SELOP_COPY:
    {
      paint.copyFloatSelection();
      break;
    }
  }
}

bool Tool_SelRect::done()
{
  bool result = justdone;
  justdone = false;
  return result;
}

void Tool_SelRect::getUndoState(UndoState& state)
{
  state.swap(tempundo);
  tempundo.clear();
}

void Tool_SelRect::draw(lpi::gui::IGUIDrawer& drawer, Paint& paint)
{
  if(paint.mouseOver(drawer.getInput()))
  {
    drawSmallCrosshair(drawer, paint, -1, -1, 10);
    drawCommonCrosshair(drawer, paint, -1, -1);
    drawOwnIcon(drawer, paint, 0, 16);
  }
}

////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////

Tool_MagicWand::Tool_MagicWand(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom)
: ITool(settings)
, justdone(false)
, tolerance(0.125)
, tolerance_ignores_alpha(false)
, alpha_left(0)
, alpha_right(255)
{
  dialog.addControl(_t("Tolerance"), new lpi::gui::DynamicSliderSpinner<double>(&tolerance, 0.0, 1.0, 0.01, geom));
  dialog.addControl(_t("Tol. Ignores A."), new lpi::gui::DynamicCheckbox(&tolerance_ignores_alpha));
  dialog.addControl(_t("Alpha Left"), new lpi::gui::DynamicSliderSpinner<int>(&alpha_left, 0, 255, 1, geom));
  dialog.addControl(_t("Alpha Right"), new lpi::gui::DynamicSliderSpinner<int>(&alpha_right, 0, 255, 1, geom));
}

void Tool_MagicWand::handle(const lpi::IInput& input, Paint& paint)
{
  if(paint.pressed(input, lpi::LMB) || paint.pressed(input, lpi::RMB))
  {
    std::vector<unsigned char> temp;
    getAlignedBuffer(temp, &paint.mask8);

    int ux0, uy0, ux1, uy1;

    int drawx, drawy;
    paint.screenToPixel(drawx, drawy, input);

    justdone = true;

    unsigned char color = input.mouseButtonDown(lpi::LMB) ? alpha_left : alpha_right;

    RGBA8* ibuffer = (RGBA8*)(paint.canvasRGBA8.getBuffer());
    unsigned char* tbuffer = &temp[0];
    unsigned char newColor = color;

    RGBA8 oldColor = ibuffer[paint.canvasRGBA8.getU2() * drawy + drawx];

    floodFillRGBA8Grey8(ux0, uy0, ux1, uy1, tbuffer, ibuffer
                      , paint.canvasRGBA8.getU(), paint.canvasRGBA8.getV(), paint.canvasRGBA8.getU(), paint.canvasRGBA8.getU2()
                      , drawx, drawy
                      , 0, 0, paint.canvasRGBA8.getU(), paint.canvasRGBA8.getV()
                      , newColor, oldColor
                      , (int)(tolerance * 255), tolerance_ignores_alpha);
    storePartialTextureInUndo(tempundo, paint.mask8, ux0, uy0, ux1, uy1);
    unsigned char* obuffer = paint.mask8.getBuffer();
    for(int y = uy0; y < uy1; y++)
    for(int x = ux0; x < ux1; x++)
    {
      size_t iindex = paint.mask8.getU() * y + x;
      size_t oindex = paint.mask8.getU2() * y + x;
      obuffer[oindex] = tbuffer[iindex];
    }
    paint.mask8.updatePartial(ux0, uy0, ux1, uy1);
  }

}

bool Tool_MagicWand::done()
{
  bool result = justdone;
  justdone = false;
  return result;
}

void Tool_MagicWand::getUndoState(UndoState& state)
{
  state.swap(tempundo);
  tempundo.clear();
}

void Tool_MagicWand::undo(Paint& paint, UndoState& state)
{
  UndoStateIndex index;
  UndoState redostate;
  readPartialTextureFromUndo(paint.mask8, redostate, state, index);
  state.swap(redostate);

}

void Tool_MagicWand::redo(Paint& paint, UndoState& state)
{
  UndoStateIndex index;
  UndoState undostate;
  readPartialTextureFromUndo(paint.mask8, undostate, state, index);
  state.swap(undostate);
}

void Tool_MagicWand::draw(lpi::gui::IGUIDrawer& drawer, Paint& paint)
{
  drawCommonCrosshair(drawer, paint, 0, 0);
}

////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////

Tool_Text::Tool_Text(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom)
: ITool(settings)
, text(_t("example"))
, opacity(1.0)
, textDrawer(lpi::TextureFactory<lpi::TextureBuffer>(), &drawer)
, font(PX8)
{
  dialog.addControl(_t("Text"), new lpi::gui::DynamicValue<std::string>(&text));
  dialog.addControl(_t("Opacity"), new lpi::gui::DynamicSliderSpinner<double>(&opacity, 0.0, 1.0, 0.01, geom));
  
  std::vector<Font> enums; enums.push_back(PX8); enums.push_back(PX7); enums.push_back(PX6); enums.push_back(PX4);
  std::vector<std::string> names; names.push_back("8px"); names.push_back("7px"); names.push_back("6px"); names.push_back("4px");
  dialog.addControl(_t("Font"), new lpi::gui::DynamicEnum<Font>(&font, names, enums, geom));
}

void Tool_Text::handle(const lpi::IInput& input, Paint& paint)
{
  if(paint.pressed(input, lpi::LMB) || paint.pressed(input, lpi::RMB))
  {
    int ux0, uy0, ux1, uy1;
    //glyph width and height
    int sw = 8, sh = 8;
    if(font == PX7) { sw = 7; sh = 9; }
    else if(font == PX6) { sw = 6; sh = 6; }
    else if(font == PX4) { sw = 4; sh = 5; }
    
    
    int drawx, drawy;
    paint.screenToPixel(drawx, drawy, input);

    justdone = true;

    lpi::ColorRGB color = input.mouseButtonDown(lpi::LMB) ? settings.leftColor255() : settings.rightColor255();
    
    TextureRGBA8& texture = paint.canvasRGBA8;
    
    ux0 = drawx - 1;
    uy0 = drawy - 1 - sh / 2; //some room for accents
    ux1 = drawx + sw * text.size() + 1;
    uy1 = drawy + sh + 2;
    if(ux0 < 0) ux0 = 0;
    if(uy0 < 0) uy0 = 0;
    if(ux1 > (int)texture.getU()) ux1 = texture.getU();
    if(uy1 > (int)texture.getV()) uy1 = texture.getV();
    
    storePartialTextureInUndo(tempundo, paint.canvasRGBA8, ux0, uy0, ux1, uy1);

    drawer.setTexture(&texture);
    drawer.setTextureAlphaAsOpacity(true);
    drawer.setColorAlphaAsOpacity(false);
    drawer.setExtraOpacity(opacity);
    
    std::string fontname = "lpi8";
    if(font == PX7) fontname = "lpi7";
    else if(font == PX6) fontname = "lpi6";
    else if(font == PX4) fontname = "lpi4";
    
    
    textDrawer.drawText(text, drawx, drawy, lpi::createFont(fontname, color));

    
    paint.uploadPartial(ux0, uy0, ux1, uy1);
  }
}

bool Tool_Text::done()
{
  bool result = justdone;
  justdone = false;
  return result;
}

void Tool_Text::getUndoState(UndoState& state)
{
  state.swap(tempundo);
  tempundo.clear();
}

void Tool_Text::undo(Paint& paint, UndoState& state)
{
  UndoStateIndex index;
  UndoState redostate;
  readPartialTextureFromUndo(paint.canvasRGBA8, redostate, state, index);
  state.swap(redostate);

}

void Tool_Text::redo(Paint& paint, UndoState& state)
{
  UndoStateIndex index;
  UndoState undostate;
  readPartialTextureFromUndo(paint.canvasRGBA8, undostate, state, index);
  state.swap(undostate);
}

void Tool_Text::draw(lpi::gui::IGUIDrawer& drawer, Paint& paint)
{
  if(paint.mouseOver(drawer.getInput()))
  {
    drawCommonCrosshair(drawer, paint, 0, 0);
  }
}

////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////

