/*
LodePaint

Copyright (c) 2009-2010 Lode Vandevenne
All rights reserved.

This file is part of LodePaint.

LodePaint is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LodePaint is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LodePaint.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "paint_filter_mask.h"


#include "lpi/gui/lpi_gui_color.h"
#include "lpi/lpi_math.h"
#include "lpi/lpi_math2d.h"

#include "imalg/blur.h"
#include "imalg/colorop.h"
#include "imalg/hqnx.h"
#include "imalg/transform.h"

#include "paint_algos.h"
#include "paint_clipboard.h"

#include <cfloat>
#include <cmath>
#include <iostream>
#include <limits>

////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////

void OperationClearMask::doit(TextureGrey8& texture, Progress&)
{
  unsigned char* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();
  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  {
    buffer[y * u2 + x] = 255;
  }
}

void OperationClearMask::doit(TextureGrey32F& texture, Progress&)
{
  float* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();
  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  {
    buffer[y * u2 + x] = 1.0;
  }
}

////////////////////////////////////////////////////////////////////////////////

OperationClearMaskToValue::OperationClearMaskToValue(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom)
: AOperationFilterUtil(settings, true, true)
, value(0.5)
{
  params.addDouble("Value", &value, 0.0, 1.0, 0.01);
  
  paramsToDialog(dialog, params, geom);
}

void OperationClearMaskToValue::doit(TextureGrey8& texture, Progress&)
{
  unsigned char* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();
  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  {
    buffer[y * u2 + x] = (int)(value * 255);
  }
}

void OperationClearMaskToValue::doit(TextureGrey32F& texture, Progress&)
{
  float* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();
  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  {
    buffer[y * u2 + x] = value;
  }
}

////////////////////////////////////////////////////////////////////////////////

bool OperationSwapMask::operate(Paint& paint, UndoState& state, Progress& progress)
{
  (void)state;
  (void)paint;
  (void)progress;
  return true;
}

bool OperationSwapMask::operateGL(Paint& paint, UndoState& state)
{
  (void)state; //no undo needed, this operation is perfectly invertible (at least on 32-bit images)
  
  if(paint.getColorMode() != MODE_RGBA_8)
  {
    return false; //it corrupts 128-bit images currently. Don't do it!
  }
  
  //swaps complete image
  /*std::vector<unsigned char> m;
  getAlignedBuffer(m, &paint.mask);
  lpi::copyTexture(&paint.mask, &paint.canvasRGBA8);
  lpi::setAlignedBuffer(&paint.canvasRGBA8, &m[0]);*/

  TextureRGBA8& texture = paint.canvasRGBA8;
  unsigned char* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();
  TextureGrey8& textureb = paint.mask8;
  unsigned char* bufferb = (unsigned char*)textureb.getBuffer();
  size_t ub = textureb.getU();
  size_t vb = textureb.getV();
  size_t u2b = textureb.getU2();
  for(size_t y = 0; y < v && y < vb; y++)
  for(size_t x = 0; x < u && y < ub; x++)
  {
    size_t index = y * u2 * 4 + x * 4;
    size_t indexb = y * u2b + x;
    std::swap(buffer[index + 3], bufferb[indexb]);
    /*float temp = bufferb[indexb];
    bufferb[indexb] = buffer[index + 3] / 255.0f;
    buffer[index + 3] = (unsigned char)(temp * 255.0f);*/
  }
  paint.update();

  return true;
}

void OperationSwapMask::undo(Paint& paint, UndoState& state)
{
  operate(paint, state, dummyProgress);
}

void OperationSwapMask::redo(Paint& paint, UndoState& state)
{
  operate(paint, state, dummyProgress);
}

////////////////////////////////////////////////////////////////////////////////

OperationChannelToMask::OperationChannelToMask(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom)
: AOperationFilterBase(settings)
, channel(C_A)
, hue(0)
, huedist(0.1)
{
  std::vector<Channel> enums;
  enums.push_back(C_A); enums.push_back(C_H); enums.push_back(C_S); enums.push_back(C_L); enums.push_back(C_R); enums.push_back(C_G); enums.push_back(C_B);
  std::vector<std::string> names;
  names.push_back("Alpha"); names.push_back("Hue"); names.push_back("Saturation"); names.push_back("Lightness");
  names.push_back("R"); names.push_back("G"); names.push_back("B");
  params.addEnum("Channel", &channel, names, enums);
  params.addHue("Hue", &hue);
  params.addDouble("Hue Distance", &huedist, 0, 1, 0.01);

  paramsToDialog(dialog, params, geom);
}

bool OperationChannelToMask::operate(Paint& paint, UndoState& state, Progress& progress)
{
  progress.setStatus(S_BUSY);

  if(paint.getColorMode() == MODE_RGBA_8)
  {
    TextureGrey8& mask = paint.mask8;

    storeCompleteTextureInUndo(state, mask);

    TextureRGBA8& texture = paint.canvasRGBA8;
    unsigned char* buffer = texture.getBuffer();
    size_t u = texture.getU();
    size_t v = texture.getV();
    size_t u2 = texture.getU2();
    unsigned char* bufferb = (unsigned char*)mask.getBuffer();
    size_t ub = mask.getU();
    size_t vb = mask.getV();
    size_t u2b = mask.getU2();
    for(size_t y = 0; y < v && y < vb; y++)
    {
      for(size_t x = 0; x < u && x < ub; x++)
      {
        size_t index = y * u2 * 4 + x * 4;
        size_t indexb = y * u2b + x;

        int val = 0;
        switch(channel)
        {
          case C_A: val = buffer[index + 3]; break;
          case C_H:
            {
              lpi::ColorHSL hsl = lpi::RGBtoHSL(lpi::ColorRGB(buffer[index + 0], buffer[index + 1], buffer[index + 2]));
              double h = hsl.h;
              double dist = h / 255.0 - hue;
              if(dist < 0) dist = -dist;
              if(dist > 0.5) dist = 1.0 - dist;
              if(dist < huedist) val = 0;
              else if(dist < huedist * 2) val = (int)(255 * (dist - huedist) / huedist );
              else val = 255;
              
              double satadjust = 0.0;
              if(hsl.s < 32) satadjust = 0.0;
              else if(hsl.s > 64) satadjust = 1.0;
              else satadjust = (hsl.s - 32.0) / 32.0;
              
              val = (int)(255 - ((255 - val) * satadjust));
              
              break;
            }
          case C_S:
            {
              lpi::ColorHSL hsl = lpi::RGBtoHSL(lpi::ColorRGB(buffer[index + 0], buffer[index + 1], buffer[index + 2]));
              val = hsl.s;
              break;
            }
          case C_L: val = ((int)buffer[index + 0] + (int)buffer[index + 1] + (int)buffer[index + 2]) / 3; break;
          case C_R: val = buffer[index + 0]; break;
          case C_G: val = buffer[index + 1]; break;
          case C_B: val = buffer[index + 2]; break;
        }

        bufferb[indexb] = (val * bufferb[indexb]) / 255;
      }

      if(progress.handle(y, v)) return false;
    }
  }

  progress.setStatus(S_DONE);
  return true;
}

void OperationChannelToMask::undo(Paint& paint, UndoState& state)
{
  UndoStateIndex index;
  UndoState temp;
  storeCompleteTextureInUndo(temp, paint.mask8);
  readCompleteTextureFromUndo(paint.mask8, state, index);
  state.swap(temp);
  paint.update();
}

void OperationChannelToMask::redo(Paint& paint, UndoState& state)
{
  UndoStateIndex index;
  UndoState temp;
  storeCompleteTextureInUndo(temp, paint.mask8);
  readCompleteTextureFromUndo(paint.mask8, state, index);
  state.swap(temp);
  paint.update();
}


////////////////////////////////////////////////////////////////////////////////

bool OperationSelectionToMask::operate(Paint& paint, UndoState& state, Progress& progress)
{
  progress.setStatus(S_BUSY);
  TextureGrey8& mask = paint.mask8;
  
  storeCompleteTextureInUndo(state, mask);
  
  unsigned char* buffer = (unsigned char*)mask.getBuffer();
  size_t u = mask.getU();
  size_t v = mask.getV();
  size_t u2 = mask.getU2();
  for(size_t y = 0; y < v; y++)
  {
    for(size_t x = 0; x < u; x++)
    {
      size_t index = y * u2 + x;
      if((int)x >= paint.sel.x0 && (int)x < paint.sel.x1
      && (int)y >= paint.sel.y0 && (int)y < paint.sel.y1)
      {
        buffer[index] = 0;
      }
      else
      {
        //buffer[index] = 1;
      }
    }
    
    progress.handle(y, v);
  }

  progress.setStatus(S_DONE);
  return true;
}

void OperationSelectionToMask::undo(Paint& paint, UndoState& state)
{
  UndoStateIndex index;
  UndoState temp;
  storeCompleteTextureInUndo(temp, paint.mask8);
  readCompleteTextureFromUndo(paint.mask8, state, index);
  state.swap(temp);
  paint.update();
}

void OperationSelectionToMask::redo(Paint& paint, UndoState& state)
{
  UndoStateIndex index;
  UndoState temp;
  storeCompleteTextureInUndo(temp, paint.mask8);
  readCompleteTextureFromUndo(paint.mask8, state, index);
  state.swap(temp);
  paint.update();
}

////////////////////////////////////////////////////////////////////////////////

OperationFeatherMask::OperationFeatherMask(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom)
: AOperationFilterUtil(settings, true, true)
, radius(8.0)
, wrapping(false)
{
  params.addDouble("Radius", &radius, 4.0, 50.0, 1.0);
  params.addBool("Wrapping", &wrapping);
  
  paramsToDialog(dialog, params, geom);
}

void OperationFeatherMask::doit(TextureGrey8& texture, Progress& progress)
{
  unsigned char* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();

  gaussianBlurGrey8(buffer, u, v, u2, radius, wrapping, progress);
}


////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////

bool OperationPasteAsSelection::operate(Paint& paint, UndoState& state, Progress&)
{
  //not thread safe
  (void)paint;
  (void)state;
  return true;
}

bool OperationPasteAsSelection::operateGL(Paint& paint, UndoState& state)
{
  if(paint.getColorMode() != MODE_RGBA_8)
  {
    return false;
  }

  state.addBool(paint.floatsel.active);
  state.addInt(paint.sel.x0);
  state.addInt(paint.sel.y0);
  state.addInt(paint.sel.x1);
  state.addInt(paint.sel.y1);
  if(paint.floatsel.active) storeCompleteTextureInUndo(state, paint.floatsel.texture);
  storePartialTextureInUndo(state, paint.canvasRGBA8, paint.sel.x0, paint.sel.y0, paint.sel.x1, paint.sel.y1);

  if(paint.hasFloatingSelection()) paint.unFloatSelection();


  std::vector<unsigned char> image;
  int w, h;
  if(getClipboardImageRGBA8(image, w, h))
  {
    PaintWindow& paintWindow = *pw;
    int wx = paintWindow.getX0() + 1;
    int wy = paintWindow.getY0() + 1;
    if(paintWindow.getPaint().getX0() > wx) wx = paintWindow.getPaint().getX0();
    if(paintWindow.getPaint().getY0() > wy) wy = paintWindow.getPaint().getY0();
    int x, y;
    paintWindow.getPaint().screenToPixel(x, y, wx, wy);
    paintWindow.getPaint().makeFloatingSelection(&image[0], w, h, x, y);
    paintWindow.setStatus(PaintWindow::S_MODIFIED);
  }

  paint.update();
  return true;
}

void OperationPasteAsSelection::setPaintWindow(PaintWindow* pw)
{
  this->pw = pw;
}

void OperationPasteAsSelection::undo(Paint& paint, UndoState& state)
{
  if(paint.getColorMode() != MODE_RGBA_8) return;

  UndoStateIndex index;
  paint.floatsel.active = state.getBool(index.ibool++);
  paint.sel.x0 = state.getInt(index.iint++);
  paint.sel.y0 = state.getInt(index.iint++);
  paint.sel.x1 = state.getInt(index.iint++);
  paint.sel.y1 = state.getInt(index.iint++);

  UndoState temp;
  temp.addBool(paint.floatsel.active);
  temp.addInt(paint.sel.x0);
  temp.addInt(paint.sel.y0);
  temp.addInt(paint.sel.x1);
  temp.addInt(paint.sel.y1);
  storePartialTextureInUndo(temp, paint.canvasRGBA8, paint.sel.x0, paint.sel.y0, paint.sel.x1, paint.sel.y1);

  if(paint.floatsel.active) readCompleteTextureFromUndo(paint.floatsel.texture, state, index);
  readPartialTextureFromUndo(paint.canvasRGBA8, state, index);

  state.swap(temp);

  paint.update();
}

void OperationPasteAsSelection::redo(Paint& paint, UndoState& state)
{
  if(paint.getColorMode() != MODE_RGBA_8) return;

  UndoStateIndex index;
  paint.floatsel.active = state.getBool(index.ibool++);
  paint.sel.x0 = state.getInt(index.iint++);
  paint.sel.y0 = state.getInt(index.iint++);
  paint.sel.x1 = state.getInt(index.iint++);
  paint.sel.y1 = state.getInt(index.iint++);

  UndoState temp;
  temp.addBool(paint.floatsel.active);
  temp.addInt(paint.sel.x0);
  temp.addInt(paint.sel.y0);
  temp.addInt(paint.sel.x1);
  temp.addInt(paint.sel.y1);
  if(paint.floatsel.active) storeCompleteTextureInUndo(temp, paint.floatsel.texture);
  storePartialTextureInUndo(temp, paint.canvasRGBA8, paint.sel.x0, paint.sel.y0, paint.sel.x1, paint.sel.y1);

  paint.deleteSelection(lpi::ColorRGB(0,0,0,0));
  readPartialTextureFromUndo(paint.canvasRGBA8, state, index);

  state.swap(temp);

  paint.update();
}



////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
