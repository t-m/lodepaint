/*
LodePaint

Copyright (c) 2009 Lode Vandevenne
All rights reserved.

This file is part of LodePaint.

LodePaint is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LodePaint is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LodePaint.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "paint_algos_brush_pixelop.h"

#include "lpi/gio/lpi_draw2d.h"
#include "lpi/lpi_math.h"
#include "lpi/lpi_math2d.h"

#include "paint_algos.h"

#include <algorithm>
#include <iostream>


void pset(unsigned char* buffer, int buffer_w, int buffer_h, int x, int y, int centerx, int centery, const Brush& brush, const IPixelOperation& op)
{
  if(x < 0 || y < 0 || x >= buffer_w || y >= buffer_h) return;

  int dx = x - centerx;
  int dy = y - centery;

  double opacity = brush.opacity;

  if(brush.softness != 0)
  {
    double distance = 0.0;
    if(brush.shape == Brush::SHAPE_SQUARE) //square
    {
      distance = std::max((dx > 0 ? dx : -dx), (dy > 0 ? dy : -dy)) / (brush.size / 2.0);
    }
    else if(brush.shape == Brush::SHAPE_ROUND) //round
    {
      distance = (dx * dx + dy * dy) / ((brush.size / 2.0) * (brush.size / 2.0)); //squared version
      //distance = std::sqrt(dx * dx + dy * dy) / (brush.size / 2); //sqrt version
    }
    else if(brush.shape == Brush::SHAPE_DIAMOND)
    {
      distance = ((dx > 0 ? dx : -dx) + (dy > 0 ? dy : -dy)) / (brush.size / 2.0);
    }

    double antidistance = 1.0 - distance;
    if(antidistance < 0.0) antidistance = 0.0;

    antidistance /= brush.softness;
    if(antidistance < 0.0) antidistance = 0.0;
    if(antidistance > 1.0) antidistance = 1.0;

    opacity = brush.opacity * antidistance;
  }
  
  int bufferPos = 4 * buffer_w * y + 4 * x;
  op.operate(buffer[bufferPos + 0], buffer[bufferPos + 1], buffer[bufferPos + 2], buffer[bufferPos + 3], opacity);
}


void horLineBrush(unsigned char* buffer, int buffer_w, int buffer_h, int y, int x1, int x2, int centerx, int centery, const Brush& brush, const IPixelOperation& op)
{
  if(x2 < x1) {x1 += x2; x2 = x1 - x2; x1 -= x2;} //swap x1 and x2, x1 must be the leftmost endpoint
  if(x2 < 0 || x1 >= buffer_w || y < 0 || y >= buffer_h) return; //no single point of the line is on screen

  if(x1 < 0) x1 = 0;
  if(x2 > buffer_w) x2 = buffer_w;

  for(int x = x1; x < x2; x++)
  {
    pset(buffer, buffer_w, buffer_h, x, y, centerx, centery, brush, op);
  }
}


void drawBrushDisk(unsigned char* buffer, int buffer_w, int buffer_h, int xc, int yc, const Brush& brush, const IPixelOperation& op)
{
  int radius = brush.size / 2;
  if(xc + radius < 0 || xc - radius >= buffer_w || yc + radius < 0 || yc - radius >= buffer_h) return; //every single pixel outside screen, so don't waste time on it
  int x = 0;
  int y = radius;
  int p = 3 - (radius << 1);
  int a, b, c, d, e, f, g, h;
  int pb = xc - radius, pd = xc - radius; //previous values: to avoid drawing horizontal lines multiple times
  while (x <= y)
  {
    // write data
    a = xc + x;
    b = yc + y;
    c = xc - x;
    d = yc - y;
    e = xc + y;
    f = yc + x;
    g = xc - y;
    h = yc - x;

    if(b != pb) horLineBrush(buffer, buffer_w, buffer_h, b, a, c, xc, yc, brush, op);
    if(d != pd) horLineBrush(buffer, buffer_w, buffer_h, d, a, c, xc, yc, brush, op);
    if(f != b)  horLineBrush(buffer, buffer_w, buffer_h, f, e, g, xc, yc, brush, op);
    if(h != d && h != f) horLineBrush(buffer, buffer_w, buffer_h, h, e, g, xc, yc, brush, op);

    pb = b;
    pd = d;
    if(p < 0) p += (x++ << 2) + 6;
    else p += ((x++ - y--) << 2) + 10;
  }
}

void drawBrushDiamond(unsigned char* buffer, int buffer_w, int buffer_h, int xc, int yc, const Brush& brush, const IPixelOperation& op)
{
  int radius = brush.size / 2;

  for(int y = yc - radius; y < yc - radius + brush.size; y++)
  {
    int dist = y - yc;
    if(dist < 0) dist = -dist;
    int size = brush.size - dist * 2;
    int x0 = xc - size / 2;
    int x1 = xc - size / 2 + size;

    horLineBrush(buffer, buffer_w, buffer_h, y, x0, x1, xc, yc, brush, op);
  }
}

void drawBrushSquare(unsigned char* buffer, int buffer_w, int buffer_h, int xc, int yc, const Brush& brush, const IPixelOperation& op)
{
  int radius = brush.size / 2;
  for(int y = yc - radius; y < yc - radius + brush.size; y++) horLineBrush(buffer, buffer_w, buffer_h, y, xc - radius, xc - radius + brush.size, xc, yc, brush, op);
}

void drawBrushShape(unsigned char* buffer, int buffer_w, int buffer_h, int xc, int yc, const Brush& brush, const IPixelOperation& op)
{
  if(brush.shape == Brush::SHAPE_SQUARE) drawBrushSquare(buffer, buffer_w, buffer_h, xc, yc, brush, op);
  else if(brush.shape == Brush::SHAPE_ROUND) drawBrushDisk(buffer, buffer_w, buffer_h, xc, yc, brush, op);
  else if(brush.shape == Brush::SHAPE_DIAMOND) drawBrushDiamond(buffer, buffer_w, buffer_h, xc, yc, brush, op);
}
