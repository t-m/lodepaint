/*
LodePaint

Copyright (c) 2009-2010 Lode Vandevenne
All rights reserved.

This file is part of LodePaint.

LodePaint is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LodePaint is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LodePaint.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "lpi/lpi_texture.h"

#include "paint_algos_brush_pixelop.h"
#include "paint_algos_brush_color.h"
#include "paint_gui_operation.h"
#include "paint_operation.h"
#include "paint_param.h"
#include "paint_window.h"

#include "imalg/progress.h"

extern Progress dummyProgress; //for use when calling a function taking a progress from one that doesn't use one (e.g. operateForPreview or undo)

class IOperationFilter : public IOperation //anything that affects the whole image (or part) in one go (without mouse interaction), e.g. invert colors, blur, ...
{
  protected:
    DynamicParameters params;
    std::string lastError;
    
  public:
    IOperationFilter(GlobalToolSettings& settings);
    virtual bool operate(Paint& paint, UndoState& state, Progress& progress) = 0; //this may NOT use any OpenGL calls or other things that don't support multithreading
    virtual bool canOperate(std::string& reason, const Paint& paint) const { (void)reason; (void)paint; return true; } //reason = message why it can not operate on this paint
    virtual bool operateGL(Paint& paint, UndoState& state) = 0; //this is to do GL calls or other things that don't support multithreading, after operate is done
    
    //for initializing the dialog and such
    virtual void init(PaintWindow* pw) = 0;
    
    /*
    getDialog:
    Return a composite GUI element with your settings.
    Return 0 if no dialog
    Don't add "..." in getLabel if you have a dialog, the program adds it automatically if this doens't return 0.
    Don't add a window or frame around the dialog, the painting program does that.
    Don't add an OK and cancel button, the painting program does that.
    Don't add a preview of the result, it's planned that the painting program will do that
    DO make sure that the filter knows the settings from the dialog at all times, because the painting program can call "operate" at any time (including for generating the preview)
    How the dialog and the filter that returns it communicate is free to the implementation. The painting
    program will run the operate function of the filter if the OK button is pressed and sometimes for the preview, or just close the
    dialog and do nothing if the cancel button was pressed.
    */
    virtual lpi::gui::Element* getDialog() { return 0; }
    

    virtual DynamicParameters* getParams() { return &params; }
    virtual std::string getLastError() { return lastError; }
    
    virtual std::string getHelp() { return ""; }
};

//these implement almost everything themselves
class AOperationFilterBase : public IOperationFilter
{
  public:
    AOperationFilterBase(GlobalToolSettings& settings) : IOperationFilter(settings) {}
    virtual bool operateGL(Paint& paint, UndoState& state);
    virtual void init(PaintWindow* pw);
};

//does NOT work for filters that change the shape or size of the texture
class AOperationFilter : public AOperationFilterBase //works on the floating selection or whole image of the given Paint
{
  protected:
    void blendWithMask(Paint& paint, const unsigned char* old);
    void blendWithMask(Paint& paint, const float* old);
    
    virtual bool worksOnMask() {return false;}
    
    virtual bool supportsColorMode(ColorMode mode) const;
    static bool sup(ColorMode mode, bool rgba8, bool rgba32f, bool grey8, bool grey32f); //utility fo ruse by supportsColorMode implementations. Short name on purpose.
    
    bool operate2(Paint& paint, UndoState& state, Progress& progress);
    bool canOperateThreadSafe(Paint& paint);
    
  public:
    AOperationFilter(GlobalToolSettings& settings) : AOperationFilterBase(settings) {}
    
    virtual bool canOperate(std::string& reason, const Paint& paint) const;
    
    virtual bool operate(Paint& paint, UndoState& state, Progress& progress);
    virtual bool operateGL(Paint& paint, UndoState& state);
    virtual void undo(Paint& paint, UndoState& state);
    virtual void redo(Paint& paint, UndoState& state);
    
    virtual void operate(TextureRGBA8& texture, UndoState& state, Progress& progress, bool not_invertible) = 0;
    virtual void undo(TextureRGBA8& texture, UndoState& state) = 0;
    virtual void redo(TextureRGBA8& texture, UndoState& state) = 0;

    virtual void operate(TextureRGBA32F& texture, UndoState& state, Progress& progress, bool not_invertible) { (void)texture; (void)state; (void)not_invertible; (void)progress;}
    virtual void undo(TextureRGBA32F& texture, UndoState& state) { (void)texture; (void)state; }
    virtual void redo(TextureRGBA32F& texture, UndoState& state) { (void)texture; (void)state; }

    virtual void operate(TextureGrey8& texture, UndoState& state, Progress& progress, bool not_invertible) { (void)texture; (void)state; (void)not_invertible; (void)progress; }
    virtual void undo(TextureGrey8& texture, UndoState& state) { (void)texture; (void)state; }
    virtual void redo(TextureGrey8& texture, UndoState& state) { (void)texture; (void)state; }

    virtual void operate(TextureGrey32F& texture, UndoState& state, Progress& progress, bool not_invertible) { (void)texture; (void)state; (void)not_invertible; (void)progress; }
    virtual void undo(TextureGrey32F& texture, UndoState& state) { (void)texture; (void)state; }
    virtual void redo(TextureGrey32F& texture, UndoState& state) { (void)texture; (void)state; }

    virtual void operateForPreview(TextureRGBA8& texture) = 0;
    virtual void operateForPreview(TextureRGBA32F& texture) = 0;
    virtual void operateForPreview(TextureGrey8& texture) = 0;
    virtual void operateForPreview(TextureGrey32F& texture) = 0;
};

/*
AOperationFilterUtil is a utility class to avoid code duplication in the undo and redo functions. It
supports 3 kinds of operations:
1) those that don't save any undo information at all and are completely invertible by doing the same operation in both directions, such as negate RGB (but NOT "rotate 90 degrees" because that one is invertible but by using a different angle in each direction)
   --> set save_texture_undo to false and save_texture_redo to false
2) those that save exactly the complete texture that they work on for undo, and for redo don't save anything but just repeat the same calculation
   --> set save_texture_undo to true and save_texture_redo to false
3) those that save exactly the complete texture for undo, and also save the result for redo (instead of redoing the calculations)
   --> set save_texture_undo to true and save_texture_redo to true
Operations that are not supported and need to inherit from IOperation instead:
-those that need to save some more info in undo, e.g. if it needs to save a color, ..., to be able to repeat the process. E.G. "clear" which uses the bg color
-those that are invertible, but do a different calculation in forward and reversed direction
-those that simply do something totally different, e.g. crop which changes the size, ...
*/
class AOperationFilterUtil : public AOperationFilter
{
  protected:
    bool save_texture_undo; //save complete texture when operating so that it can be used for undo
    bool save_texture_redo; //for redo, also save result image, instead of "re-doing" the calculations (may only be true if save_texture_undo is also true)

  protected:
    virtual void doit(TextureRGBA8& texture, Progress& progress) { (void)texture; (void)progress; }
    virtual void doit(TextureRGBA32F& texture, Progress& progress) { (void)texture; (void)progress; }
    virtual void doit(TextureGrey8& texture, Progress& progress) { (void)texture; (void)progress; }
    virtual void doit(TextureGrey32F& texture, Progress& progress) { (void)texture; (void)progress; }

  public:
    AOperationFilterUtil(GlobalToolSettings& settings, bool save_texture_undo, bool save_texture_redo) : AOperationFilter(settings), save_texture_undo(save_texture_undo), save_texture_redo(save_texture_redo) {}
    
    virtual void operate(TextureRGBA8& texture, UndoState& state, Progress& progress, bool not_invertible);
    virtual void undo(TextureRGBA8& texture, UndoState& state);
    virtual void redo(TextureRGBA8& texture, UndoState& state);
    
    virtual void operate(TextureRGBA32F& texture, UndoState& state, Progress& progress, bool not_invertible);
    virtual void undo(TextureRGBA32F& texture, UndoState& state);
    virtual void redo(TextureRGBA32F& texture, UndoState& state);

    virtual void operate(TextureGrey8& texture, UndoState& state, Progress& progress, bool not_invertible);
    virtual void undo(TextureGrey8& texture, UndoState& state);
    virtual void redo(TextureGrey8& texture, UndoState& state);

    virtual void operate(TextureGrey32F& texture, UndoState& state, Progress& progress, bool not_invertible);
    virtual void undo(TextureGrey32F& texture, UndoState& state);
    virtual void redo(TextureGrey32F& texture, UndoState& state);

    virtual void operateForPreview(TextureRGBA8& texture);
    virtual void operateForPreview(TextureRGBA32F& texture);
    virtual void operateForPreview(TextureGrey8& texture);
    virtual void operateForPreview(TextureGrey32F& texture);
};

/*
Similar to AOperationFilterUtil, but for filters that use some of the GlobalToolSettings, there were
quite a few of these so this fixes some more code duplication.
Filters with custom user settings specifically only for this filter (e.g. rescale where you choose the size)
aren't supported by this class though!
*/
class AOperationFilterUtilWithSettings : public AOperationFilter
{
  protected:
    bool save_texture_undo; //save complete texture when operating so that it can be used for undo
    bool save_texture_redo; //for redo, also save result image, instead of "re-doing" the calculations (may only be true if save_texture_undo is also true)

  protected:
    virtual void doit(TextureRGBA8& texture, const GlobalToolSettings& settings, Progress& progress) { (void)texture; (void)settings; (void)progress; }
    virtual void doit(TextureRGBA32F& texture, const GlobalToolSettings& settings, Progress& progress) { (void)texture; (void)settings; (void)progress; }
    virtual void doit(TextureGrey8& texture, const GlobalToolSettings& settings, Progress& progress) { (void)texture; (void)settings; (void)progress; }
    virtual void doit(TextureGrey32F& texture, const GlobalToolSettings& settings, Progress& progress) { (void)texture; (void)settings; (void)progress; }

  public:
    AOperationFilterUtilWithSettings(GlobalToolSettings& settings, bool save_texture_undo, bool save_texture_redo);

    virtual void operate(TextureRGBA8& texture, UndoState& state, Progress& progress, bool not_invertible);
    virtual void undo(TextureRGBA8& texture, UndoState& state);
    virtual void redo(TextureRGBA8& texture, UndoState& state);

    virtual void operate(TextureRGBA32F& texture, UndoState& state, Progress& progress, bool not_invertible);
    virtual void undo(TextureRGBA32F& texture, UndoState& state);
    virtual void redo(TextureRGBA32F& texture, UndoState& state);

    virtual void operate(TextureGrey8& texture, UndoState& state, Progress& progress, bool not_invertible);
    virtual void undo(TextureGrey8& texture, UndoState& state);
    virtual void redo(TextureGrey8& texture, UndoState& state);

    virtual void operate(TextureGrey32F& texture, UndoState& state, Progress& progres, bool not_invertibles);
    virtual void undo(TextureGrey32F& texture, UndoState& state);
    virtual void redo(TextureGrey32F& texture, UndoState& state);

    virtual void operateForPreview(TextureRGBA8& texture);
    virtual void operateForPreview(TextureRGBA32F& texture);
    virtual void operateForPreview(TextureGrey8& texture);
    virtual void operateForPreview(TextureGrey32F& texture);
};
