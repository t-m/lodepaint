/*
LodePaint

Copyright (c) 2009-2010 Lode Vandevenne
All rights reserved.

This file is part of LodePaint.

LodePaint is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LodePaint is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LodePaint.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "paint_filter.h"
#include "paint_gui_operation.h"
#include "paint_tool.h"

#include "lpi/gui/lpi_gui_drawer_gl.h"
#include "lpi/lpi_parse.h"
#include "lpi/lpi_pvector.h"
#include "lpi/lpi_translate.h"

#include <iostream>

FilterDialog::FilterDialog(lpi::gui::Element* element, const lpi::gui::IGUIDrawer& geom, const std::string& title)
: element(element)
{
  addTop(geom);
  addTitle(title);
  addResizer(geom);
  addCloseButton(geom);

  resize(0, 0, 500, 500);

  ok.makeTextPanel(0, 0, _t("ok"));
  cancel.makeTextPanel(0, 0, _t("cancel"));
  help.makeTextPanel(0, 0, _t("help"));

  pushTop(&cancel, lpi::gui::Sticky(1.0,-84, 1.0,-24, 1.0,-4, 1.0,-4));
  pushTop(&ok, lpi::gui::Sticky(1.0,-168, 1.0,-24, 1.0,-88, 1.0,-4));
  pushTop(element, lpi::gui::Sticky(0.0,8, 0.0,8, 1.0,-8, 1.0,-32));
  
  setColorMod(lpi::ColorRGB(255, 255, 255));
}

void FilterDialog::addHelp(const std::string& text)
{
  helpText = text;
  pushTop(&help, lpi::gui::Sticky(0.0,4, 1.0,-24, 0.0,64, 1.0,-4));
}

void FilterDialog::handleImpl(const lpi::IInput& input)
{
  lpi::gui::Window::handleImpl(input);

  if(ok.clicked(input) || input.keyDown(SDLK_RETURN) || input.keyDown(SDLK_KP_ENTER))
  {
    setEnabled(false);
    result = OK;
  }
  if(cancel.clicked(input) || closeButtonClicked(input))
  {
    setEnabled(false);
    result = CANCEL;
  }
}

void FilterDialog::drawImpl(lpi::gui::IGUIDrawer& drawer) const
{
  lpi::gui::Window::drawImpl(drawer);
}

////////////////////////////////////////////////////////////////////////////////

void DynamicPageWithAutoUpdate::handleImpl(const lpi::IInput& input)
{
  valueToControl();
  lpi::gui::DynamicPage::handleImpl(input);
  controlToValue();
}

////////////////////////////////////////////////////////////////////////////////

FilterDialogWithPreview::FilterDialogWithPreview(AOperationFilter* filter)
: filter(filter)
, ratherlameautoupdateboolean(false)
, disablepreview(false)
{
  update.makeTextPanel(0, 0, _t("update preview"));
  addSubElement(&update, lpi::gui::Sticky(1.0,-140, 1.0,-40, 1.0,-16, 1.0,-8));
  autoupdate.make(0, 0);
  autoupdate.addText(_t("auto update"));
  addSubElement(&autoupdate, lpi::gui::Sticky(1.0,-140, 1.0,-56, 1.0,-16, 1.0,-40));
  autoupdate.setChecked(true);
}

void FilterDialogWithPreview::generatePreviewSource(TextureRGBA8* texture)
{
  mode = MODE_RGBA_8;
  unsigned char* bufferb = texture->getBuffer();
  size_t ub = texture->getU();
  size_t vb = texture->getV();
  size_t u2b = texture->getU2();
  if(ub == 0 || vb == 0) return;
  if(ub / (double)vb < MAXPREVIEWU / (double)MAXPREVIEWV)
  {
    previewu = (MAXPREVIEWV * ub) / vb;
    previewv = MAXPREVIEWV;
  }
  else
  {
    previewu = MAXPREVIEWU;
    previewv = (MAXPREVIEWU * vb) / ub;
  }
  textureSourceRGBA8.setSize(previewu, previewv);
  unsigned char* buffer = textureSourceRGBA8.getBuffer();
  size_t u = textureSourceRGBA8.getU();
  size_t v = textureSourceRGBA8.getV();
  size_t u2 = textureSourceRGBA8.getU2();
  if(ub == 0 || vb == 0) return;
  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  for(size_t c = 0; c < 4; c++)
  {
    size_t xb = (x * ub) / u;
    size_t yb = (y * vb) / v;
    size_t index = y * u2 * 4 + x * 4 + c;
    size_t indexb = yb * u2b * 4 + xb * 4 + c;
    buffer[index] = bufferb[indexb];
  }
  updatePreview();
}

void FilterDialogWithPreview::generatePreviewSource(TextureRGBA32F* texture)
{
  mode = MODE_RGBA_32F;
  float* bufferb = texture->getBuffer();
  size_t ub = texture->getU();
  size_t vb = texture->getV();
  size_t u2b = texture->getU2();
  if(ub == 0 || vb == 0) return;
  if(ub / (double)vb < MAXPREVIEWU / (double)MAXPREVIEWV)
  {
    previewu = (MAXPREVIEWV * ub) / vb;
    previewv = MAXPREVIEWV;
  }
  else
  {
    previewu = MAXPREVIEWU;
    previewv = (MAXPREVIEWU * vb) / ub;
  }
  textureSourceRGBA32F.setSize(previewu, previewv);
  float* buffer = textureSourceRGBA32F.getBuffer();
  size_t u = textureSourceRGBA32F.getU();
  size_t v = textureSourceRGBA32F.getV();
  size_t u2 = textureSourceRGBA32F.getU2();
  if(ub == 0 || vb == 0) return;
  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  for(size_t c = 0; c < 4; c++)
  {
    size_t xb = (x * ub) / u;
    size_t yb = (y * vb) / v;
    size_t index = y * u2 * 4 + x * 4 + c;
    size_t indexb = yb * u2b * 4 + xb * 4 + c;
    buffer[index] = bufferb[indexb];
  }
  updatePreview();
}

void FilterDialogWithPreview::generatePreviewSource(TextureGrey8* texture)
{
  mode = MODE_GREY_8;
  unsigned char* bufferb = texture->getBuffer();
  size_t ub = texture->getU();
  size_t vb = texture->getV();
  size_t u2b = texture->getU2();
  if(ub == 0 || vb == 0) return;
  if(ub / (double)vb < MAXPREVIEWU / (double)MAXPREVIEWV)
  {
    previewu = (MAXPREVIEWV * ub) / vb;
    previewv = MAXPREVIEWV;
  }
  else
  {
    previewu = MAXPREVIEWU;
    previewv = (MAXPREVIEWU * vb) / ub;
  }
  textureSourceGrey8.setSize(previewu, previewv);
  unsigned char* buffer = textureSourceGrey8.getBuffer();
  size_t u = textureSourceGrey8.getU();
  size_t v = textureSourceGrey8.getV();
  size_t u2 = textureSourceGrey8.getU2();
  if(ub == 0 || vb == 0) return;
  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  {
    size_t xb = (x * ub) / u;
    size_t yb = (y * vb) / v;
    size_t index = y * u2 + x;
    size_t indexb = yb * u2b + xb;
    buffer[index] = bufferb[indexb];
  }
  updatePreview();
}

void FilterDialogWithPreview::generatePreviewSource(TextureGrey32F* texture)
{
  mode = MODE_GREY_32F;
  float* bufferb = texture->getBuffer();
  size_t ub = texture->getU();
  size_t vb = texture->getV();
  size_t u2b = texture->getU2();
  if(ub == 0 || vb == 0) return;
  if(ub / (double)vb < MAXPREVIEWU / (double)MAXPREVIEWV)
  {
    previewu = (MAXPREVIEWV * ub) / vb;
    previewv = MAXPREVIEWV;
  }
  else
  {
    previewu = MAXPREVIEWU;
    previewv = (MAXPREVIEWU * vb) / ub;
  }
  textureSourceGrey32F.setSize(previewu, previewv);
  float* buffer = textureSourceGrey32F.getBuffer();
  size_t u = textureSourceGrey32F.getU();
  size_t v = textureSourceGrey32F.getV();
  size_t u2 = textureSourceGrey32F.getU2();
  if(ub == 0 || vb == 0) return;
  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  {
    size_t xb = (x * ub) / u;
    size_t yb = (y * vb) / v;
    size_t index = y * u2 + x;
    size_t indexb = yb * u2b + xb;
    buffer[index] = bufferb[indexb];
  }
  updatePreview();
}


void FilterDialogWithPreview::drawImpl(lpi::gui::IGUIDrawer& drawer) const
{
  update.draw(drawer);
  autoupdate.draw(drawer);
  int x = x0 + 20;
  
  if(!disablepreview)
  {
    if(mode == MODE_RGBA_8)
    {
      int y = getY1() - texturePreviewRGBA8.getV() - 2;
      
      drawer.drawRectangle(x - 1, y - 1, x + texturePreviewRGBA8.getU() + 1, y + texturePreviewRGBA8.getV() + 1, lpi::RGB_Black, false);
      lpi::gui::drawCheckerBackground(drawer, x, y, x + texturePreviewRGBA8.getU(), y + texturePreviewRGBA8.getV(), 8, 8, lpi::RGB_Grey, lpi::RGB_Gray);

      drawTexture(dynamic_cast<lpi::gui::GUIDrawerGL&>(drawer).getScreen(), &texturePreviewRGBA8, x, y);
    }
    else if(mode == MODE_RGBA_32F)
    {
      int y = getY1() - texturePreviewRGBA32F.getV() - 2;
      
      drawer.drawRectangle(x - 1, y - 1, x + texturePreviewRGBA32F.getU() + 1, y + texturePreviewRGBA32F.getV() + 1, lpi::RGB_Black, false);
      lpi::gui::drawCheckerBackground(drawer, x, y, x + texturePreviewRGBA32F.getU(), y + texturePreviewRGBA32F.getV(), 8, 8, lpi::RGB_Grey, lpi::RGB_Gray);

      drawTexture(dynamic_cast<lpi::gui::GUIDrawerGL&>(drawer).getScreen(), &texturePreviewRGBA32F, x, y);
    }
    else if(mode == MODE_GREY_8)
    {
      int y = getY1() - texturePreviewGrey8.getV() - 2;
      
      drawer.drawRectangle(x - 1, y - 1, x + texturePreviewGrey8.getU() + 1, y + texturePreviewGrey8.getV() + 1, lpi::RGB_Black, false);
      lpi::gui::drawCheckerBackground(drawer, x, y, x + texturePreviewGrey8.getU(), y + texturePreviewGrey8.getV(), 8, 8, lpi::RGB_Grey, lpi::RGB_Gray);

      drawTexture(dynamic_cast<lpi::gui::GUIDrawerGL&>(drawer).getScreen(), &texturePreviewGrey8, x, y);
    }
    else if(mode == MODE_GREY_32F)
    {
      int y = getY1() - texturePreviewGrey32F.getV() - 2;
      
      drawer.drawRectangle(x - 1, y - 1, x + texturePreviewGrey32F.getU() + 1, y + texturePreviewGrey32F.getV() + 1, lpi::RGB_Black, false);
      lpi::gui::drawCheckerBackground(drawer, x, y, x + texturePreviewGrey32F.getU(), y + texturePreviewGrey32F.getV(), 8, 8, lpi::RGB_Grey, lpi::RGB_Gray);

      drawTexture(dynamic_cast<lpi::gui::GUIDrawerGL&>(drawer).getScreen(), &texturePreviewGrey32F, x, y);
    }
  }
}

void FilterDialogWithPreview::updatePreview()
{

  if(mode == MODE_RGBA_8)
  {
    copyTexture(&texturePreviewRGBA8, &textureSourceRGBA8);
    filter->operateForPreview(texturePreviewRGBA8);
  }
  else if(mode == MODE_RGBA_32F)
  {
    copyTexture(&texturePreviewRGBA32F, &textureSourceRGBA32F);
    filter->operateForPreview(texturePreviewRGBA32F);
  }
  else if(mode == MODE_GREY_8)
  {
    copyTexture(&texturePreviewGrey8, &textureSourceGrey8);
    filter->operateForPreview(texturePreviewGrey8);
  }
  else if(mode == MODE_GREY_32F)
  {
    copyTexture(&texturePreviewGrey32F, &textureSourceGrey32F);
    filter->operateForPreview(texturePreviewGrey32F);
  }
}

void FilterDialogWithPreview::handleImpl(const lpi::IInput& input)
{
  update.handle(input);
  autoupdate.handle(input);
  
  if(!disablepreview)
  {
    bool doupdate = false;

    if(autoupdate.isChecked())
    {
      bool mousedown = input.mouseButtonDown(lpi::LMB) /*&& isInside(input.mouseX(), input.mouseY())*/;
      if(!mousedown && ratherlameautoupdateboolean) doupdate = true;
      ratherlameautoupdateboolean = mousedown;
    }

    if(update.clicked(input)) doupdate = true;
    if(doupdate) updatePreview();
  }

  //if it's a resizing filter, disable the preview entirely (with plugin filters, this isn't known beforehand)
  if((mode == MODE_RGBA_8 && (texturePreviewRGBA8.getU() != textureSourceRGBA8.getU()))
  || (mode == MODE_RGBA_32F && (texturePreviewRGBA32F.getU() != textureSourceRGBA32F.getU()))
  || (mode == MODE_GREY_8 && (texturePreviewGrey8.getU() != textureSourceGrey8.getU()))
  || (mode == MODE_GREY_32F && (texturePreviewGrey32F.getU() != textureSourceGrey32F.getU())))
  {
    disablepreview = true;
  }
}

////////////////////////////////////////////////////////////////////////////////

DynamicFilterPageWithPreview::DynamicFilterPageWithPreview(AOperationFilter* filter)
: FilterDialogWithPreview(filter)
{
  addSubElement(&page, lpi::gui::Sticky(0.0,0, 0.0,0, 1.0,0, 1.0,-MAXPREVIEWV));
}

void DynamicFilterPageWithPreview::drawImpl(lpi::gui::IGUIDrawer& drawer) const
{
  page.draw(drawer);
  FilterDialogWithPreview::drawImpl(drawer);
}

void DynamicFilterPageWithPreview::handleImpl(const lpi::IInput& input)
{
  page.handle(input);
  FilterDialogWithPreview::handleImpl(input);
}

////////////////////////////////////////////////////////////////////////////////

Numbers::Numbers(const lpi::gui::IGUIDrawer& geom, int numx, int numy)
: activeNumber(-1)
, changed(true)
, presetlist(geom)
{
  numbers.resize(numx * numy);
  for(int y = 0; y < numy; y++)
  for(int x = 0; x < numx; x++)
  {
    int i = numx * y + x;
    numbers[i] = new lpi::gui::InputLine();
    numbers[i]->make(0, 0, 8);
    addSubElement(numbers[i], lpi::gui::Sticky((1.0/numx)*x,0, 0.15+(0.85/numy)*y,0, (1.0/numx)*(x+1),0, 0.15+(0.85/numy)*(1+y),0));
  }
  presetlist.addItem("---");
  presetlist.addItem("Identity");
  std::vector<double> idpreset(numx * numy);
  for(size_t i = 0; i < idpreset.size(); i++) idpreset[i] = i % numx == i / numx ? 1 : 0;
  presets.push_back(idpreset);
  addSubElement(&presetlist, lpi::gui::Sticky(0.0,0, 0.0,0, 0.5,0, 0.15,0));
}

Numbers::~Numbers()
{
  lpi::vectorDelete(numbers);
}

void Numbers::addPreset(const std::string& name, const std::vector<double>& values)
{
  presetlist.addItem(name);
  presets.push_back(values);
}

void Numbers::drawImpl(lpi::gui::IGUIDrawer& drawer) const
{
  for(size_t i = 0; i < numbers.size(); i++)
  {
    numbers[i]->draw(drawer);
  }
  presetlist.draw(drawer);
}

int Numbers::getKeyboardFocus() const
{
  int active = getActiveNumber();
  if(active >= 0 && numbers[activeNumber]->isControlActive())
  {
    activeNumber = active;
    return lpi::gui::KM_TAB;
  }
  else return 0;
}

int Numbers::getActiveNumber() const
{
  if(activeNumber >= 0 && numbers[activeNumber]->isControlActive())
  {
    return activeNumber;
  }
  else
  {
    for(size_t i = 0; i < numbers.size(); i++)
    {
      if(numbers[i]->isControlActive())
      {
        return i;
      }
    }
  }
  return -1;
}

void Numbers::handleImpl(const lpi::IInput& input)
{
  for(size_t i = 0; i < numbers.size(); i++)
  {
    numbers[i]->handle(input);
  }
  
  if(input.keyPressed(SDLK_TAB))
  {
    int active = getActiveNumber();
    if(active >= 0)
    {
      numbers[active]->activate(false);
      numbers[active]->selectNone();
      int j = 0;
      if(input.keyDown(SDLK_LSHIFT))
      {
        j = active - 1;
        if(j < 0) j = numbers.size() - 1;
      }
      else
      {
        j = active + 1;
        if(j >= (int)numbers.size()) j = 0;
      }
      numbers[j]->activate(true);
      numbers[j]->selectAll();
      activeNumber = j;
    }
  }
  presetlist.handle(input);
  
  if(presetlist.hasChanged() && presetlist.getSelectedItem() != 0)
  {
    int i = presetlist.getSelectedItem();
    const std::vector<double>& p = presets[i - 1];
    valueToControl(&p[0]);
  }
}

void Numbers::controlToValue(double* values) const
{
  for(size_t i = 0; i < numbers.size(); i++)
  {
    values[i] = lpi::strtoval<double>(numbers[i]->getText());
  }
}

void Numbers::valueToControl(const double* values)
{
  for(size_t i = 0; i < numbers.size(); i++)
  {
    numbers[i]->setText(lpi::valtostr<double>(values[i]));
  }
  changed = true;
}

////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////

CurvesDialog::CurvesDialog(const lpi::gui::IGUIDrawer& geom)
: greyscale(false)
, alpha(false)
{
  bullets.make(0, 0, 4, 0, 16);
  bullets.addText("R", 0);
  bullets.addText("G", 1);
  bullets.addText("B", 2);
  bullets.addText("A", 3);
  bullets.set(0);

  //addSubElement(&bullets, lpi::gui::Sticky(0.5,0, 0.0,0, 1.0,0, 1.0,0));
  //addSubElement(&curve, lpi::gui::Sticky(0.5,-256, 0.5,-128, 0.5,0, 0.5,128));
  
  addSubElement(&bullets, lpi::gui::Sticky(0.6,32, 0.0,0, 1.0,0, 0.0,64));
  addSubElement(&curve, lpi::gui::Sticky(0.0,0, 0.0,0, 0.6,0, 1.0,0));
  
  smooth.makeText(0, 0, "smooth", geom);
  reset.makeText(0, 0, "reset", geom);
  
  addSubElement(&smooth, lpi::gui::Sticky(0.6,32, 0.0,64, 0.6,32+smooth.getSizeX(), 0.0,80));
  addSubElement(&reset, lpi::gui::Sticky(0.6,32, 0.0,80, 0.6,32+reset.getSizeX(), 0.0,96));
  
  resetCurve(r);
  resetCurve(g);
  resetCurve(b);
  resetCurve(a);
  resetCurve(l);
}

void CurvesDialog::resetCurve(unsigned char* curve)
{
  for(int i = 0; i < 256; i++) curve[i] = i;
}

void CurvesDialog::smoothCurveOnce(unsigned char* curve)
{
  unsigned char temp[256];
  for(size_t i = 0; i < 256; i++)
  {
    if(i == 0) temp[i] = (curve[i] + curve[i+1]) / 2;
    else if(i == 255) temp[i] = (curve[i] + curve[i-1]) / 2;
    else temp[i] = (curve[i-1]+curve[i] + curve[i+1]) / 3;
  }
  for(size_t i = 0; i < 256; i++) curve[i] = temp[i];
}

void CurvesDialog::smoothCurve(unsigned char* curve)
{
  smoothCurveOnce(curve);
  smoothCurveOnce(curve);
  smoothCurveOnce(curve);
}

void CurvesDialog::drawCurve(const unsigned char* curve, lpi::gui::IGUIDrawer& drawer, int x0, int y0, int x1, int y1, const lpi::ColorRGB& color) const
{
  int dx = x1 - x0;
  int dy = y1 - y0;
  
  if(dx <= 256)
  {
    for(int x = 0; x < dx; x++)
    {
      int i = (x * 256) / dx;
      int ly0 = (curve[i] * dy) / 256;
      int ly1 = (curve[i+1] * dy) / 256;
      drawer.drawLine(x0 + x, y1 - ly0, x0 + x + 1, y1 - ly1, color);
    }
  }
  else
  {
    for(int i = 0; i < 255; i++)
    {
      int lx0 = (i * dx) / 256;
      int ly0 = (curve[i] * dy) / 256;
      int lx1 = ((i+1) * dx) / 256;
      int ly1 = (curve[i+1] * dy) / 256;
      drawer.drawLine(x0 + lx0, y1 - ly0, x0 + lx1, y1 - ly1, color);
    }
  }
}

void CurvesDialog::handleCurve(unsigned char* curve, int x0, int y0, int x1, int y1, int mx0, int my0, int mx1, int my1)
{
  if(mx0 > mx1)
  {
    std::swap(mx0, mx1);
    std::swap(my0, my1);
  }
  
  int dx = x1 - x0;
  int dy = y1 - y0;

  int i0 = (mx0 * 256) / (dx - 1);
  int i1 = ((mx1+1) * 256) / (dx - 1);
  
  //if(i0 < 0 || i1 >= 256) return;
  if(i0 < 0) i0 = 0;
  if(i0 > 256) i0 = 256;
  if(i1 < 0) i1 = 0;
  if(i1 > 256) i1 = 256;
  
  if(i1 == i0)
  {
    if(i0 == 0) i1 = 1;
    else i0 = i1 - 1;
  }
  
  int value0 = ((dy - my0) * 255) / dy;
  int value1 = ((dy - my1) * 255) / dy;
  for(int i = i0; i < i1; i++)
  {
    int value = value0 + ((value1 - value0) * (i - i0)) / (i1 - i0);
    if(value > 255) value = 255;
    if(value < 0) value = 0;
    curve[i] = value;
  }

}


void CurvesDialog::drawImpl(lpi::gui::IGUIDrawer& drawer) const
{
  curve.draw(drawer);
  bullets.draw(drawer);
  smooth.draw(drawer);
  reset.draw(drawer);
  
  drawer.drawRectangle(curve.getX0(), curve.getY0(), curve.getX1(), curve.getY1(), lpi::RGB_Gray, true);
  if(greyscale)
  {
    drawCurve(l, drawer, curve.getX0(), curve.getY0(), curve.getX1(), curve.getY1(), lpi::ColorRGB(0,0,0,128));
  }
  else
  {
    drawCurve(r, drawer, curve.getX0(), curve.getY0(), curve.getX1(), curve.getY1(), lpi::ColorRGB(255,0,0,128));
    drawCurve(g, drawer, curve.getX0(), curve.getY0(), curve.getX1(), curve.getY1(), lpi::ColorRGB(0,255,0,128));
    drawCurve(b, drawer, curve.getX0(), curve.getY0(), curve.getX1(), curve.getY1(), lpi::ColorRGB(0,0,255,128));
  }
  
  drawCurve(a, drawer, curve.getX0(), curve.getY0(), curve.getX1(), curve.getY1(), lpi::ColorRGB(255,255,255,128));
}


void CurvesDialog::handleImpl(const lpi::IInput& input)
{
  curve.handle(input);
  bullets.handle(input);
  smooth.handle(input);
  reset.handle(input);
  
  int x = curve.mouseGetRelPosX(input);
  int y = curve.mouseGetRelPosY(input);
  /*if(x < 0) x = 0;
  if(x >= curve.getSizeX()) x = curve.getSizeX() - 1;
  if(y < 0) y = 0;
  if(y >= curve.getSizeY()) y = curve.getSizeY() - 1;*/

  if(curve.mouseGrabbed(input))
  {
    if(bullets.check() == 3) handleCurve(a, curve.getX0(), curve.getY0(), curve.getX1(), curve.getY1(), prevx, prevy, x, y);
    else if(!greyscale)
    {
      if(bullets.check() == 0) handleCurve(r, curve.getX0(), curve.getY0(), curve.getX1(), curve.getY1(), prevx, prevy, x, y);
      if(bullets.check() == 1) handleCurve(g, curve.getX0(), curve.getY0(), curve.getX1(), curve.getY1(), prevx, prevy, x, y);
      if(bullets.check() == 2) handleCurve(b, curve.getX0(), curve.getY0(), curve.getX1(), curve.getY1(), prevx, prevy, x, y);
    }
    else handleCurve(l, curve.getX0(), curve.getY0(), curve.getX1(), curve.getY1(), prevx, prevy, x, y);
    
  }
  
  if(curve.mouseDoubleClicked(input) || reset.clicked(input))
  {
    if(bullets.check() == 3) resetCurve(a);
    else if(!greyscale)
    {
      if(bullets.check() == 0) resetCurve(r);
      if(bullets.check() == 1) resetCurve(g);
      if(bullets.check() == 2) resetCurve(b);
    }
    else resetCurve(l);
  }
  
  if(curve.clicked(input, lpi::RMB) || smooth.clicked(input))
  {
    if(bullets.check() == 3) smoothCurve(a);
    else if(!greyscale)
    {
      if(bullets.check() == 0) smoothCurve(r);
      if(bullets.check() == 1) smoothCurve(g);
      if(bullets.check() == 2) smoothCurve(b);
    }
    else smoothCurve(l);

  }
  
  prevx = x;
  prevy = y;

}

////////////////////////////////////////////////////////////////////////////////

class MyDynamicEnum : public lpi::gui::DynamicEnum<int>
{
    int myvalue;
    const ParamEnum* dyn;
    
  private:
    
    int* getInitializedValue() //avoids "Conditional jump or move depends on uninitialised value(s)"
    {
      myvalue = 0;
      return &myvalue;
    }

  public:
    MyDynamicEnum(const ParamEnum* dyn, const std::vector<std::string>& names, const std::vector<int>& values, const lpi::gui::IGUIDrawer& geom)
    : lpi::gui::DynamicEnum<int>(getInitializedValue(), names, values, geom)
    , myvalue(dyn->def)
    , dyn(dyn)
    {
    }
    
    
    virtual void controlToValueCustom()
    {
      dyn->setValueAsInt(myvalue);
    }
    virtual void valueToControlCustom()
    {
      myvalue = dyn->getValueAsInt();
    }
};

class DynamicHue : public lpi::gui::TDymamicPageControl<double>
{
  private:
    lpi::gui::InputLine line;
    lpi::gui::ChannelSliderType slider;

    void ctor(const lpi::gui::IGUIDrawer& geom)
    {
      (void)geom;
      static const int TEXTSIZE = 64;
      this->resize(0, 0, TEXTSIZE * 2, 14);

      //slider.makeSmallHorizontal(0, 0, this->getSizeX() - TEXTSIZE, 1.0, geom);
      slider.move(0, (this->getSizeY() - slider.getSizeY()) / 2);
      slider.setAdaptiveColor(lpi::RGBd_Red);
      slider.setType(lpi::gui::CC_HSL_H);
      this->addSubElement(&slider, lpi::gui::Sticky(0.0,0, 0.5,-getSizeY() / 2, 1.0,-TEXTSIZE, 0.5,getSizeY() / 2));

      line.make(0, 0, 256);
      //line.move(0, (this->getSizeY() - line.getSizeY()) / 2);
      line.resize(slider.getX1(), (this->getSizeY() - line.getSizeY()) / 2, slider.getX1() + TEXTSIZE, (this->getSizeY() - line.getSizeY()) / 2 + line.getSizeY());
      this->addSubElement(&line, lpi::gui::Sticky(1.0, -TEXTSIZE, 0.5, -line.getSizeY() / 2, 1.0, 0, 0.5, line.getSizeY() / 2));
    }

  protected:

    double getSliderValue() const
    {
      return slider.getValue();
    }

    void setSliderValue(double val)
    {
      slider.setValue(val);
    }

  public:

    DynamicHue(double* value, const lpi::gui::IGUIDrawer& geom)
    {
      lpi::gui::TDymamicPageControl<double>::bind = value;
      ctor(geom);
      setValue(value);
    }

    virtual void getValue(double* value)
    {
      *value = lpi::strtoval<double>(line.getText());
    }

    virtual void setValue(double* value)
    {
      line.setText(lpi::valtostr<double>(*value));
      setSliderValue(*value);
    }

    virtual void handleImpl(const lpi::IInput& input)
    {
      line.handle(input);
      slider.handle(input);
      if(line.enteringDone())
        setSliderValue(lpi::strtoval<double>(line.getText()));
      if(slider.mouseGrabbed(input))
        line.setText(lpi::valtostr<double>(getSliderValue()));
    }

    virtual void drawImpl(lpi::gui::IGUIDrawer& drawer) const
    {
      line.draw(drawer);
      slider.draw(drawer);
    }

    virtual bool canTab() const { return true; }
    virtual bool tabIsActive() const { return line.isControlActive(); }
    virtual void tabActivate()
    {
      line.activate(true);
      line.selectAll();
    }
    virtual void tabDeActivate()
    {
      line.activate(false);
      line.selectNone();
    }
};

void paramsToDialog(DynamicPageWithAutoUpdate& page, const DynamicParameters& params, const lpi::gui::IGUIDrawer& geom)
{
  for(size_t i = 0; i < params.size(); i++)
  {
    const Param& param = *params.get(i);
    switch(param.getType())
    {
      case P_INT:
      {
        const ParamInt& p = dynamic_cast<const ParamInt&>(param);
        page.addControl(_t(p.name), new lpi::gui::DynamicSliderSpinner<int>(p.bind, p.minl, p.maxl, p.minp, p.maxp, p.step, geom));
        break;
      }
      case P_DOUBLE:
      {
        const ParamDouble& p = dynamic_cast<const ParamDouble&>(param);
        page.addControl(_t(p.name), new lpi::gui::DynamicSliderSpinner<double>(p.bind, p.minl, p.maxl, p.minp, p.maxp, p.step, geom));
        break;
      }
      case PG_HUE:
      {
        const ParamHue& p = dynamic_cast<const ParamHue&>(param);
        page.addControl(_t(p.name), new DynamicHue(p.bind, geom));
        break;
      }
      case P_BOOL:
      {
        const ParamBool& p = dynamic_cast<const ParamBool&>(param);
        page.addControl(_t(p.name), new lpi::gui::DynamicCheckbox(p.bind));
        break;
      }
      case P_NAMED_BOOL:
      {
        const ParamNamedBool& p = dynamic_cast<const ParamNamedBool&>(param);
        page.addControl(_t(p.name), new lpi::gui::DynamicDropDownBool(p.bind, p.name0, p.name1, geom));
        break;
      }
      case PG_MULTIBOOL:
      {
        const ParamMultiBool& p = dynamic_cast<const ParamMultiBool&>(param);
        page.addControl(_t(p.name), new lpi::gui::DynamicCheckboxes(p.bind));
        break;
      }
      case P_STRING:
      {
        const ParamString& p = dynamic_cast<const ParamString&>(param);
        page.addControl(_t(p.name), new lpi::gui::DynamicValue<std::string>(p.bind));
        break;
      }
      case P_COLOR:
      {
        const ParamColor& p = dynamic_cast<const ParamColor&>(param);
        page.addControl(_t(p.name), new lpi::gui::DynamicColord(p.bind, geom));
        break;
      }
      case P_ENUM:
      {
        const ParamEnum& p = dynamic_cast<const ParamEnum&>(param);

        std::vector<std::string> translatednames(p.names.size());
        for(size_t i = 0; i < p.names.size(); i++) translatednames[i] = _t(p.names[i]);

        page.addControl(_t(p.name), new MyDynamicEnum(&p, translatednames, p.valuesInt, geom));
        break;
      }
      case PG_ABS_REL_SIZE:
      {
        const ParamResize& p = dynamic_cast<const ParamResize&>(param);
        page.addControl(_t(p.name), new DynamicControlResize(p.origwidth, p.origheight, p.width, p.height, p.relwidth, p.relheight, p.percentage, geom));
        break;
      }
      case PE_LABEL:
      {
        const ParamLabel& p = dynamic_cast<const ParamLabel&>(param);
        page.addTextRow(_t(p.name));
        break;
      }
      case PE_TOOLTIP:
      {
        const ParamToolTip& p = dynamic_cast<const ParamToolTip&>(param);
        page.addToolTipToLastRow(_t(p.name));
        break;
      }
      default:
      {
        break;
      }
    }
  }
}

////////////////////////////////////////////////////////////////////////////////

DynamicControlResize::DynamicControlResize(const int* origwidth, const int* origheight, int* width, int* height, double* relwidth, double* relheight, bool* usepercent, const lpi::gui::IGUIDrawer& geom)
: link(false)
, origwidth(origwidth)
, origheight(origheight)
, width(width)
, height(height)
, relwidth(relwidth)
, relheight(relheight)
, usepercent(usepercent)
{
  (void)geom;
  
  resize(0, 0, 20, lpi::gui::CONTROLHEIGHT * 3);
  
  inputw.make(this->x0, this->y0, 256);
  inputw.resize(0, 0, 20, lpi::gui::CONTROLHEIGHT - 2);
  addSubElement(&inputw, lpi::gui::Sticky(0.0,0, 0.1667,-inputw.getSizeY()/2, 0.0,56, 0.1667,inputw.getSizeY()/2));
  
  inputh.make(this->x0, this->y0, 256);
  inputh.resize(0, 0, 20, lpi::gui::CONTROLHEIGHT - 2);
  addSubElement(&inputh, lpi::gui::Sticky(0.0,0, 0.5,-inputh.getSizeY()/2, 0.0,56, 0.5,inputh.getSizeY()/2));
  
  inputusepercent.makeSmall(0, 0);
  addSubElement(&inputusepercent, lpi::gui::Sticky(0.0,0, 0.8333, -inputusepercent.getSizeY() / 2, 0.0,inputusepercent.getSizeX(), 0.8333,inputusepercent.getSizeY() / 2));
  
  addSubElement(&linksymbol, lpi::gui::Sticky(0.0,80, 0.333,-7, 0.0,87, 0.333,8));
}

void DynamicControlResize::setControlsAbsolute(int w, int h)
{
  inputw.setText(lpi::valtostr<int>(w));
  inputh.setText(lpi::valtostr<int>(h));
}

void DynamicControlResize::setControlsRelative(double w, double h)
{
  inputw.setText(lpi::valtostr<double>(w));
  inputh.setText(lpi::valtostr<double>(h));
}

void DynamicControlResize::controlToValue()
{
  if(inputusepercent.isChecked())
  {
    *relwidth = lpi::strtoval<double>(inputw.getText());
    *relheight = lpi::strtoval<double>(inputh.getText());
    *width = (int)(*relwidth * *origwidth / 100.0);
    *height = (int)(*relheight * *origheight / 100.0);
  }
  else
  {
    *width = lpi::strtoval<int>(inputw.getText());
    *height = lpi::strtoval<int>(inputh.getText());
    *relwidth = (*width) / (double)(*origwidth) * 100.0;
    *relheight = (*height) / (double)(*origheight) * 100.0;
  }
  
  *usepercent = inputusepercent.isChecked();
  
}

void DynamicControlResize::valueToControl()
{
  if(!inputw.isControlActive() && !inputh.isControlActive())
  {
    if(*usepercent)
    {
      inputw.setText(lpi::valtostr<double>(*relwidth));
      inputh.setText(lpi::valtostr<double>(*relheight));
    }
    else
    {
      inputw.setText(lpi::valtostr<int>(*width));
      inputh.setText(lpi::valtostr<int>(*height));
    }
        
    inputusepercent.setChecked(*usepercent);
    previnputusepercent = *usepercent;
  }
}

void DynamicControlResize::handleImpl(const lpi::IInput& input)
{
  inputw.handle(input);
  inputh.handle(input);
  inputusepercent.handle(input);
  
  if(previnputusepercent != inputusepercent.isChecked())
  {
    previnputusepercent = inputusepercent.isChecked();
    if(previnputusepercent) setControlsRelative(*relwidth, *relheight);
    else setControlsAbsolute(*width, *height);
  }
  
  bool justlinked = false;
  if(linksymbol.clicked(input))
  {
    link = !link;
    justlinked = link;
  }
  
  if(link)
  {
    if((justlinked || inputw.isControlActive()) && !inputw.getText().empty())
    {
      if(*usepercent)
      {
        *relheight = lpi::strtoval<double>(inputw.getText());
        inputh.setText(lpi::valtostr<double>(*relheight));
      }
      else
      {
        int w = lpi::strtoval<int>(inputw.getText());
        *height = (int)(*origheight * (w / (double)(*origwidth)));
        inputh.setText(lpi::valtostr<int>(*height));
      }
    }
    if(inputh.isControlActive() && !inputh.getText().empty())
    {
      if(*usepercent)
      {
        *relwidth = lpi::strtoval<double>(inputh.getText());
        inputw.setText(lpi::valtostr<double>(*relwidth));
      }
      else
      {
        int h = lpi::strtoval<int>(inputh.getText());
        
        int w = lpi::strtoval<int>(inputw.getText());
        if((int)(*origheight * (w / (double)(*origwidth))) != h)
        {
          *width = (int)(*origwidth * (h / (double)(*origheight)));
          inputw.setText(lpi::valtostr<int>(*width));
        }
      }
    }
  }
  
  /*if(inputw.enteringDone())
  {
    if(*usepercent)
    {
      int absw = (int)(lpi::strtoval<double>(inputw.getText()) * *origwidth);
      setControlsAbsolute(absw, *height);
    }
    else
    {
      double relw = lpi::strtoval<int>(inputw.getText()) / (double)(*origwidth);
      setControlsRelative(relw, *relheight);
    }
  }
  
  if(inputh.enteringDone())
  {
    if(*usepercent)
    {
      int absh = (int)(lpi::strtoval<double>(inputh.getText()) * (*origheight));
      setControlsAbsolute(*width, absh);
    }
    else
    {
      double relh = lpi::strtoval<int>(inputh.getText()) / (double)(*origheight);
      setControlsRelative(*relwidth, relh);
    }
  }*/
  
  /*if(!inputw.isControlActive() && !inputh.isControlActive())
  {
    if(*usepercent)
    {
      int absw = (int)(lpi::strtoval<double>(inputw.getText()) * *origwidth);
      int absh = (int)(lpi::strtoval<double>(inputh.getText()) * (*origheight));
      setControlsAbsolute(absw, absh);
    }
    else
    {
      double relw = lpi::strtoval<int>(inputw.getText()) / (double)(*origwidth);
      double relh = lpi::strtoval<int>(inputh.getText()) / (double)(*origheight);
      setControlsRelative(relw, relh);
    }
  }*/
}

//x, y is coordinate of the center
void drawLinkSymbol(lpi::gui::IGUIDrawer& drawer, int x, int y, bool open)
{
  drawer.drawLine(x, y - 5 - 2, x, y - 5 - 6, lpi::RGB_Black);
  drawer.drawLine(x, y - 5 - 6, x - 8, y - 5 - 6, lpi::RGB_Black);
  drawer.drawLine(x, y - 5 + 12, x, y - 5 + 17, lpi::RGB_Black);
  drawer.drawLine(x, y - 5 + 17, x - 8, y - 5 + 17, lpi::RGB_Black);
  
  if(open)
  {
    drawer.drawRectangle(x-3+0, y-7+0, x-3+7, y-7+6, lpi::RGB_Black, false);
    drawer.drawRectangle(x-3+1, y-7+1, x-3+6, y-7+5, lpi::RGB_White, true);
    drawer.drawRectangle(x-3+0, y-7+9, x-3+7, y-7+15, lpi::RGB_Black, false);
    drawer.drawRectangle(x-3+1, y-7+10, x-3+6, y-7+14, lpi::RGB_White, true);
    drawer.drawRectangle(x-3+2, y-7+2, x-3+5, y-7+7, lpi::RGB_Black, false);
    drawer.drawLine(x-3+3, y-7+3, x-3+3, y-7+6, lpi::RGB_White);
    drawer.drawRectangle(x-3+2, y-7+8, x-3+5, y-7+13, lpi::RGB_Black, false);
    drawer.drawLine(x-3+3, y-7+9, x-3+3, y-7+12, lpi::RGB_White);
  }
  else
  {
    drawer.drawRectangle(x-3+0, y-5+0, x-3+7, y-5+11, lpi::RGB_Black, false);
    drawer.drawRectangle(x-3+1, y-5+1, x-3+6, y-5+10, lpi::RGB_White, false);
    drawer.drawLine(x-3+0, y-5+5, x-3+7, y-5+5, lpi::RGB_Black);
    drawer.drawRectangle(x-3+2, y-5+2, x-3+5, y-5+9, lpi::RGB_Black, false);
    drawer.drawLine(x-3+3, y-5+3, x-3+3, y-5+8, lpi::RGB_White);
  }
  
}

void DynamicControlResize::drawImpl(lpi::gui::IGUIDrawer& drawer) const
{
  drawer.drawRectangle(inputw.getX0(), inputw.getY0(), inputw.getX1(), inputw.getY1(), lpi::RGB_White, true);
  drawer.drawRectangle(inputw.getX0(), inputw.getY0(), inputw.getX1(), inputw.getY1(), lpi::RGB_Grey, false);
  drawer.drawRectangle(inputh.getX0(), inputh.getY0(), inputh.getX1(), inputh.getY1(), lpi::RGB_White, true);
  drawer.drawRectangle(inputh.getX0(), inputh.getY0(), inputh.getX1(), inputh.getY1(), lpi::RGB_Grey, false);
  
  //drawLinkSymbol(drawer, x1 - 8, y0 + 8, false);
  //drawLinkSymbol(drawer, x1 - 20, y0 + 8, true);
  drawLinkSymbol(drawer, linksymbol.getCenterX(), linksymbol.getCenterY(), !link);
  
  inputw.draw(drawer);
  inputh.draw(drawer);
  inputusepercent.draw(drawer);
  
  if(previnputusepercent)
  {
    drawer.drawText("%", inputw.getX1() + 2, inputw.getCenterY(), lpi::FONT_Default7, 0, lpi::TextAlign(lpi::HA_LEFT, lpi::VA_CENTER));
    drawer.drawText("%", inputh.getX1() + 2, inputh.getCenterY(), lpi::FONT_Default7, 0, lpi::TextAlign(lpi::HA_LEFT, lpi::VA_CENTER));
  }
  else
  {
    drawer.drawText("px", inputw.getX1() + 2, inputw.getCenterY(), lpi::FONT_Default7, 0, lpi::TextAlign(lpi::HA_LEFT, lpi::VA_CENTER));
    drawer.drawText("px", inputh.getX1() + 2, inputh.getCenterY(), lpi::FONT_Default7, 0, lpi::TextAlign(lpi::HA_LEFT, lpi::VA_CENTER));
  }
  
  drawer.drawText(_t("relative"), inputusepercent.getX1() + 2, inputusepercent.getCenterY(), lpi::FONT_Default7, 0, lpi::TextAlign(lpi::HA_LEFT, lpi::VA_CENTER));
}

////////////////////////////////////////////////////////////////////////////////

