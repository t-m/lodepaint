/*
LodePaint

Copyright (c) 2009-2010 Lode Vandevenne
All rights reserved.

This file is part of LodePaint.

LodePaint is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LodePaint is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LodePaint.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "paint_filter.h"

class OperationRepeat : public AOperationFilterBase
{
  private:
    int numx;
    int numy;
    DynamicPageWithAutoUpdate dialog;
    void doit(Paint& paint, int numx, int numy, Progress& progress);
  public:
    OperationRepeat(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom);
    
    virtual bool operate(Paint& paint, UndoState& state, Progress& progress);
    virtual bool operateGL(Paint& paint, UndoState& state);
    virtual void undo(Paint& paint, UndoState& state);
    virtual void redo(Paint& paint, UndoState& state);

    virtual std::string getLabel() const { return "Repeat"; }
    virtual lpi::gui::Element* getDialog() { return &dialog; }
    virtual std::string getHelp() { return "[OperationRepeat Help]"; }
};

class OperationRepeat2x2 : public AOperationFilterBase
{
  private:
    void doit(Paint& paint, Progress& progress);
  public:
    OperationRepeat2x2(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom)
    : AOperationFilterBase(settings)
    {
      (void)geom;
    }
    virtual bool operate(Paint& paint, UndoState& state, Progress& progress);
    virtual bool operateGL(Paint& paint, UndoState& state);
    virtual void undo(Paint& paint, UndoState& state);
    virtual void redo(Paint& paint, UndoState& state);

    virtual std::string getLabel() const { return "Repeat 2x2"; }
};

/*
MirrorTile turns this:

XYZ
ABC
IJK

into this:

XYZZYX
ABCCBA
IJKKJI
IJKKJI
ABCCBA
XYZZYX

*/
class OperationMirrorTile : public AOperationFilterBase
{
  private:
    void doit(Paint& paint, Progress& progress);
  public:
    OperationMirrorTile(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom)
    : AOperationFilterBase(settings)
    {
      (void)geom;
    }
    virtual bool operate(Paint& paint, UndoState& state, Progress& progress);
    virtual bool operateGL(Paint& paint, UndoState& state);
    virtual void undo(Paint& paint, UndoState& state);
    virtual void redo(Paint& paint, UndoState& state);

    virtual std::string getLabel() const { return "Mirror-Tile"; }
};

class OperationSwapColors : public AOperationFilterUtilWithSettings
{
  private:
  
  protected:
    virtual void doit(TextureRGBA8& texture, const GlobalToolSettings& settings, Progress& progress);
  public:
    OperationSwapColors(GlobalToolSettings& settings)
    : AOperationFilterUtilWithSettings(settings, false, false)
    {
    }
    virtual std::string getLabel() const { return "Swap Colors"; }
};

class OperationOneBitFGBG : public AOperationFilterUtilWithSettings
{
  private:
  
  protected:
    virtual void doit(TextureRGBA8& texture, const GlobalToolSettings& settings, Progress& progress);
  public:
    OperationOneBitFGBG(GlobalToolSettings& settings)
    : AOperationFilterUtilWithSettings(settings, true, true)
    {
    }
    virtual std::string getLabel() const { return "Set 1-bit FG BG"; }
};

//Turns the whole image into the average color
class OperationColorAverage : public AOperationFilterUtil
{
  private:
  
  protected:
    virtual void doit(TextureRGBA8& texture, Progress& progress);
  public:
    OperationColorAverage(GlobalToolSettings& settings) : AOperationFilterUtil(settings, true, true){}
    virtual std::string getLabel() const { return "Average color"; }
};

//sort based on color RGBA integer value (median according to this integer representation will appear in center pixel)
class OperationSortColors : public AOperationFilterUtil
{
  private:
  
  protected:
    virtual void doit(TextureRGBA8& texture, Progress& progress);
  public:
    OperationSortColors(GlobalToolSettings& settings) : AOperationFilterUtil(settings, true, true){}
    virtual std::string getLabel() const { return "Sort colors by #hex value"; }
};

//sort based on color RGBA integer value (median according to this integer representation will appear in center pixel)
class OperationSortColorsHilbert : public AOperationFilterUtil
{
  private:
  
  protected:
    virtual void doit(TextureRGBA8& texture, Progress& progress);
  public:
    OperationSortColorsHilbert(GlobalToolSettings& settings) : AOperationFilterUtil(settings, true, true){}
    virtual std::string getLabel() const { return "Sort colors by Hilbert index"; }
};

//sort based on amount of pixels with these colors (most common first, mode will appear in top left pixel)
class OperationOrderColors : public AOperationFilterUtil
{
  private:
  
  protected:
    virtual void doit(TextureRGBA8& texture, Progress& progress);
  public:
    OperationOrderColors(GlobalToolSettings& settings) : AOperationFilterUtil(settings, true, true){}
    virtual std::string getLabel() const { return "Order colors by use count"; }
};

class OperationNegateRGB : public AOperationFilterUtil
{
  private:
    virtual void doit(TextureRGBA8& texture, Progress& progress);
    virtual void doit(TextureRGBA32F& texture, Progress& progress);
    virtual void doit(TextureGrey8& texture, Progress& progress);
    virtual void doit(TextureGrey32F& texture, Progress& progress);
    virtual bool supportsColorMode(ColorMode mode) const { return sup(mode, true, true, true, true); }
  public:
    OperationNegateRGB(GlobalToolSettings& settings) : AOperationFilterUtil(settings, false, false) {}
    virtual std::string getLabel() const { return "Negate RGB"; }
};

class OperationNegateLightness : public AOperationFilterUtil
{
  private:
    virtual void doit(TextureRGBA8& texture, Progress& progress);
  public:
    OperationNegateLightness(GlobalToolSettings& settings) : AOperationFilterUtil(settings, true, true) {}
    virtual std::string getLabel() const { return "Negate Lightness"; }
};

class OperationNegateAdvanced : public AOperationFilterUtil
{
  private:
    
    bool r;
    bool g;
    bool b;
    bool a;
    bool lightness;
    bool value;
    bool saturation;
    bool labl;
    bool laba;
    bool labb;

    DynamicFilterPageWithPreview dialog;
  
    virtual void doit(TextureRGBA8& texture, Progress& progress);
  public:
    OperationNegateAdvanced(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom);
    
    virtual std::string getLabel() const { return "Negate Advanced"; }
    virtual lpi::gui::Element* getDialog() { return &dialog; }
    virtual std::string getHelp() { return "[OperationNegateAdvanced Help]"; }
};


class OperationNegateRGBA : public AOperationFilterUtil
{
  private:
    virtual void doit(TextureRGBA8& texture, Progress& progress);
    virtual void doit(TextureRGBA32F& texture, Progress& progress);
    virtual bool supportsColorMode(ColorMode mode) const { return sup(mode, true, true, false, false); }
  public:
    OperationNegateRGBA(GlobalToolSettings& settings) : AOperationFilterUtil(settings, false, false) {}
    virtual std::string getLabel() const { return "Negate RGBA"; }
};

class OperationNegateAlpha : public AOperationFilterUtil
{
  private:
    virtual void doit(TextureRGBA8& texture, Progress& progress);
    virtual void doit(TextureRGBA32F& texture, Progress& progress);
    virtual bool supportsColorMode(ColorMode mode) const { return sup(mode, true, true, false, false); }
  public:
    OperationNegateAlpha(GlobalToolSettings& settings) : AOperationFilterUtil(settings, false, false) {}
    virtual std::string getLabel() const { return "Negate Alpha"; }
};

class OperationSolarizeRGB : public AOperationFilterUtil //somewhat like negate, but with V curve (white->black, black->black, 50%grey->white)
{
  private:
    virtual void doit(TextureRGBA8& texture, Progress& progress);
    virtual void doit(TextureRGBA32F& texture, Progress& progress);
    virtual bool supportsColorMode(ColorMode mode) const { return sup(mode, true, true, false, false); }
  public:
    OperationSolarizeRGB(GlobalToolSettings& settings) : AOperationFilterUtil(settings, true, true) {}
    virtual std::string getLabel() const { return "Solarize RGB"; }
};

class OperationExtractChannel : public AOperationFilterUtil
{
  private:
    DynamicFilterPageWithPreview dialog;
   
    bool affect_alpha; //whether or not to affect the alpha of the target image
   
    enum Channel
    {
      C_CHROMA,
      C_HUE,
      C_LIGHTNESS,
      C_SATURATION_L,
      C_VALUE,
      C_SATURATION_V,
      C_INTENSITY,
      C_SATURATION_I,
      C_LUMA,
      C_ALPHA,
      C_RGB_R,
      C_RGB_G,
      C_RGB_B,
      C_CMYK_C,
      C_CMYK_M,
      C_CMYK_Y,
      C_CMYK_K,
      C_Lab_L,
      C_Lab_a,
      C_Lab_b,
      C_XYZ_X,
      C_XYZ_Y,
      C_XYZ_Z,
      C_YPbPr_Y,
      C_YPbPr_Pb,
      C_YPbPr_Pr,
      C_YCbCr_Y,
      C_YCbCr_Cb,
      C_YCbCr_Cr,
      C_Min,
      C_Middle,
      C_Max
    };
   
    Channel channel;

    virtual void doit(TextureRGBA8& texture, Progress& progress);
    virtual bool supportsColorMode(ColorMode mode) const { return sup(mode, true, false, false, false); }

  public:
    OperationExtractChannel(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom);

    virtual std::string getLabel() const { return "Extract Channel"; }
    virtual lpi::gui::Element* getDialog() { return &dialog; }
    virtual std::string getHelp() { return "[OperationExtractChannel Help]"; }
};


/*
absolute value:
on floating point images, this makes negative channels in the pixels positive
on 32-bit RGBA images, this treats value "128" as "0"
but this operation is useful mostly for HDR floating point images, more specifically:
doing the "sharpen" filter on a HDR image can result in negative pixels. This makes them positive.
*/
class OperationAbsoluteValue : public AOperationFilterUtil
{
  private:
    virtual void doit(TextureRGBA8& texture, Progress& progress);
    virtual void doit(TextureRGBA32F& texture, Progress& progress);
    virtual bool supportsColorMode(ColorMode mode) const { return sup(mode, true, true, false, false); }
    bool affect_alpha;
    DynamicFilterPageWithPreview dialog;
  public:
    OperationAbsoluteValue(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom);
    
    virtual std::string getLabel() const { return "Absolute Value"; }
    virtual lpi::gui::Element* getDialog() { return &dialog; }
    virtual std::string getHelp() { return "[OperationAbsoluteValue Help]"; }
};


class OperationFlipX : public AOperationFilterUtil
{
  private:
    virtual void doit(TextureRGBA8& texture, Progress& progress);
  public:
    OperationFlipX(GlobalToolSettings& settings) : AOperationFilterUtil(settings, false, false) {}
    virtual std::string getLabel() const { return "Flip X"; }
};

class OperationFlipY : public AOperationFilterUtil
{
  private:
    virtual void doit(TextureRGBA8& texture, Progress& progress);
  public:
    OperationFlipY(GlobalToolSettings& settings) : AOperationFilterUtil(settings, false, false) {}
    virtual std::string getLabel() const { return "Flip Y"; }
};

class OperationRotate180 : public AOperationFilterUtil
{
  private:
    virtual void doit(TextureRGBA8& texture, Progress& progress);
  public:
    OperationRotate180(GlobalToolSettings& settings) : AOperationFilterUtil(settings, false, false) {}
    virtual std::string getLabel() const { return "Rotate 180"; }
};

class OperationRotate90CW : public AOperationFilterBase
{
  public:
    OperationRotate90CW(GlobalToolSettings& settings) : AOperationFilterBase(settings) {}
    virtual bool operate(Paint& paint, UndoState& state, Progress& progress);
    virtual bool operateGL(Paint& paint, UndoState& state);
    virtual void undo(Paint& paint, UndoState& state);
    virtual void redo(Paint& paint, UndoState& state);
    virtual std::string getLabel() const { return "Rotate 90 CW"; }
};



class OperationRotate90CCW : public AOperationFilterBase
{
  public:
    OperationRotate90CCW(GlobalToolSettings& settings) : AOperationFilterBase(settings) {}
    virtual bool operate(Paint& paint, UndoState& state, Progress& progress);
    virtual bool operateGL(Paint& paint, UndoState& state);
    virtual void undo(Paint& paint, UndoState& state);
    virtual void redo(Paint& paint, UndoState& state);
    virtual std::string getLabel() const { return "Rotate 90 CCW"; }
};

class OperationShift50 : public AOperationFilterUtil
{
  private:
    virtual void doit(TextureRGBA8& texture, Progress& progress);
  public:
    OperationShift50(GlobalToolSettings& settings) : AOperationFilterUtil(settings, true, false) {}
    virtual std::string getLabel() const { return "Wrapping Shift 50%"; }
};

class OperationWrappingShift : public AOperationFilterUtil
{
  private:
    virtual void doit(TextureRGBA8& texture, Progress& progress);
    int x;
    int y;
    DynamicFilterPageWithPreview dialog;
  public:
    OperationWrappingShift(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom);
    virtual std::string getLabel() const { return "Wrapping Shift"; }
    virtual lpi::gui::Element* getDialog() { return &dialog; }
    virtual std::string getHelp() { return "[OperationWrappingShift Help]"; }
};

class OperationGrayscale : public AOperationFilterUtil
{
  private:
    virtual void doit(TextureRGBA8& texture, Progress& progress);
  public:
    OperationGrayscale(GlobalToolSettings& settings) : AOperationFilterUtil(settings, true, false) {}
    virtual std::string getLabel() const { return "Grayscale"; }
};

class OperationGrayscalePhysiological : public AOperationFilterUtil
{
  private:
    virtual void doit(TextureRGBA8& texture, Progress& progress);
  public:
    OperationGrayscalePhysiological(GlobalToolSettings& settings) : AOperationFilterUtil(settings, true, false) {}
    virtual std::string getLabel() const { return "Grayscale (Luma)"; }
};

class OperationGrayscaleAlpha : public AOperationFilterUtil
{
  private:
    virtual void doit(TextureRGBA8& texture, Progress& progress);
  public:
    OperationGrayscaleAlpha(GlobalToolSettings& settings) : AOperationFilterUtil(settings, true, false) {}
    virtual std::string getLabel() const { return "Grayscale (Alpha)"; }
};

class OperationClear : public AOperationFilterUtilWithSettings
{
  private:
    void doit(TextureRGBA8& texture, const GlobalToolSettings& color, Progress& progress);
  public:
    OperationClear(GlobalToolSettings& settings) : AOperationFilterUtilWithSettings(settings, true, false) {}
    virtual std::string getLabel() const { return "Clear"; }
};

class OperationColorize : public AOperationFilterUtilWithSettings
{
  private:
    virtual void doit(TextureRGBA8& texture, const GlobalToolSettings& settings, Progress& progress);
    bool affect_alpha;
    DynamicFilterPageWithPreview dialog;
  public:
    OperationColorize(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom);
    
    virtual lpi::gui::Element* getDialog() { return &dialog; }
    virtual std::string getLabel() const { return "Colorize"; }
    virtual std::string getHelp() { return "[OperationColorize Help]"; }
};

//parameterless normalize. Normalizes colors and alpha independently (keeps alpha at 255 if it was 255)
class OperationNormalize : public AOperationFilterUtil
{
  private:
    virtual void doit(TextureRGBA8& texture, Progress& progress);
    virtual void doit(TextureRGBA32F& texture, Progress& progress);
    virtual void doit(TextureGrey8& texture, Progress& progress);
    virtual void doit(TextureGrey32F& texture, Progress& progress);
    virtual bool supportsColorMode(ColorMode mode) const { return sup(mode, true, true, true, true); }
    
    bool link_channels;
    bool affect_alpha;
    
  public:
    OperationNormalize(GlobalToolSettings& settings)
    : AOperationFilterUtil(settings, true, true)
    , link_channels(true)
    , affect_alpha(false)
    {
    }
    
    virtual std::string getLabel() const { return "Normalize"; }
};

//normalize with some parameters
class OperationNormalize2 : public AOperationFilterUtil
{
  private:
    virtual void doit(TextureRGBA8& texture, Progress& progress);
    virtual void doit(TextureRGBA32F& texture, Progress& progress);
    virtual bool supportsColorMode(ColorMode mode) const { return sup(mode, true, true, false, false); }
    
    bool link_channels;
    bool affect_alpha;
    DynamicFilterPageWithPreview dialog;
  public:
    OperationNormalize2(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom);
    
    virtual lpi::gui::Element* getDialog() { return &dialog; }
    virtual std::string getLabel() const { return "Normalize 2"; }
    virtual std::string getHelp() { return "[OperationNormalize2 Help]"; }
};

class OperationClipColor : public AOperationFilterUtil
{
  private:
    virtual void doit(TextureRGBA8& texture, Progress& progress);
    virtual void doit(TextureRGBA32F& texture, Progress& progress);
    virtual bool supportsColorMode(ColorMode mode) const { return sup(mode, true, true, false, false); }
    
    bool affect_rgb;
    bool affect_alpha;
    double low;
    double high;
    DynamicFilterPageWithPreview dialog;
  public:
    OperationClipColor(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom);
    
    virtual lpi::gui::Element* getDialog() { return &dialog; }
    virtual std::string getLabel() const { return "Clip Color"; }
    virtual std::string getHelp() { return "[OperationClipColor Help]"; }
};

class OperationThreshold : public AOperationFilterUtil
{
  private:
    virtual void doit(TextureRGBA8& texture, Progress& progress);
    virtual void doit(TextureRGBA32F& texture, Progress& progress);
    virtual bool supportsColorMode(ColorMode mode) const { return sup(mode, true, true, false, false); }
    
    double threshold;
    DynamicFilterPageWithPreview dialog;
  public:
    OperationThreshold(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom);
    
    virtual lpi::gui::Element* getDialog() { return &dialog; }
    virtual std::string getLabel() const { return "Threshold"; }
    virtual std::string getHelp() { return "[OperationThreshold Help]"; }
};

class OperationDitherMonochrome : public AOperationFilterUtil
{
  private:
    virtual void doit(TextureRGBA8& texture, Progress& progress);
    
  public:
    OperationDitherMonochrome(GlobalToolSettings& settings)
    : AOperationFilterUtil(settings, true, true)
    {
    }
    
    virtual std::string getLabel() const { return "Monochrome (Dithered)"; }
};

class OperationDitherRGB1 : public AOperationFilterUtil
{
  private:
    virtual void doit(TextureRGBA8& texture, Progress& progress);
    
  public:
    OperationDitherRGB1(GlobalToolSettings& settings)
    : AOperationFilterUtil(settings, true, true)
    {
    }
    
    virtual std::string getLabel() const { return "1-bit RGBA (Dithered)"; }
};

class OperationCrop : public AOperationFilterBase
{
  private:
    virtual void doit(Paint& paint, Progress& progress);

  public:
    OperationCrop(GlobalToolSettings& settings) : AOperationFilterBase(settings) {}
    virtual bool operate(Paint& paint, UndoState& state, Progress& progress);
    virtual bool operateGL(Paint& paint, UndoState& state);
    virtual void undo(Paint& paint, UndoState& state);
    virtual void redo(Paint& paint, UndoState& state);
    virtual std::string getLabel() const { return "Crop"; }
};

//Similar to OperationAddRemoveBorders, but not used in any filter menu, is used for the image draggable corners instead and does not have a dialog, its parameters are set programatically
class OperationAddRemoveBordersInternal : public AOperationFilterUtil
{
  protected:
    
    int left;
    int right;
    int top;
    int bottom;
    
    virtual void doit(TextureRGBA8& texture, Progress& progress);
    virtual void doit(TextureRGBA32F& texture, Progress& progress);
    virtual void doit(TextureGrey8& texture, Progress& progress);
    virtual void doit(TextureGrey32F& texture, Progress& progress);
    virtual bool supportsColorMode(ColorMode mode) const { return sup(mode, true, true, true, true); }

  public:
    OperationAddRemoveBordersInternal(GlobalToolSettings& settings);
    
    virtual bool operate(Paint& paint, UndoState& state, Progress& progress);
    virtual bool operateGL(Paint& paint, UndoState& state);

    void setParameters(int left, int right, int top, int bottom);

    virtual std::string getLabel() const { return "Add/Remove Borders Internal"; }
    virtual lpi::gui::Element* getDialog() { return 0; }
};


class OperationAddRemoveBorders : public OperationAddRemoveBordersInternal
{
  private:
    DynamicPageWithAutoUpdate dialog;
  public:
    OperationAddRemoveBorders(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom);

    virtual std::string getLabel() const { return "Add/Remove Borders"; }
    virtual std::string getHelp() { return "[OperationAddRemoveBorders Help]"; }
    virtual lpi::gui::Element* getDialog() { return &dialog; }
};

class OperationRescale : public AOperationFilterUtil
{
  private:
    virtual bool operate(Paint& paint, UndoState& state, Progress& progress);
    virtual bool operateGL(Paint& paint, UndoState& state);
    virtual void doit(TextureRGBA8& texture, Progress& progress);
    virtual void doit(TextureRGBA32F& texture, Progress& progress);
    virtual void doit(TextureGrey8& texture, Progress& progress);
    virtual void doit(TextureGrey32F& texture, Progress& progress);
    virtual bool supportsColorMode(ColorMode mode) const { return sup(mode, true, true, true, true); }
    DynamicPageWithAutoUpdate dialog;
    
    enum Algo
    {
      ALGO_NEAREST, //bigger: nearest, smaller: no AA
      ALGO_BILINEAR, //bigger: bilinear, smaller: antialiasing
      ALGO_BICUBIC, //bigger: bicubic, smaller: antialiasing
      ALGO_RAREST, //bigger: nearest, smaller: choose least common pixel of region
      ALGO_ADDREMOVEBORDERS,
      ALGO_SKEWED //this keeps the data in the buffer in the same order in 1D, ignoring 2D dimensions. Result is often skewed. This is useful to e.g. fix an image that was loaded with wrong dimensions...
    };
    
    Algo algo;
    int width;
    int height;
    double width_percent;
    double height_percent;
    bool percent;
    bool wrapping;
    int origwidth;
    int origheight;
    
  public:
    OperationRescale(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom);
    
    virtual std::string getLabel() const { return "Rescale"; }
    virtual lpi::gui::Element* getDialog() { return &dialog; }
    virtual std::string getHelp() { return "[OperationRescale Help]"; }
    
    virtual void init(PaintWindow* pw);
};

class OperationPixelArtScaling : public AOperationFilterBase
{
  private:
    virtual void doit(Paint& paint, Progress& progress);
    DynamicPageWithAutoUpdate dialog;
    
    enum Algo
    {
      ALGO_Scale2x,
      ALGO_Scale3x,
      //ALGO_Scale4x //Not in the list because it's doing Scale2x twice in a row.
      ALGO_hq2x,
      ALGO_LAST_DONT_USE
    };
    
    Algo algo;
    bool wrapping;
    
  public:
    OperationPixelArtScaling(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom);
    
    virtual bool operate(Paint& paint, UndoState& state, Progress& progress);
    virtual bool operateGL(Paint& paint, UndoState& state);
    virtual void undo(Paint& paint, UndoState& state);
    virtual void redo(Paint& paint, UndoState& state);

    virtual std::string getLabel() const { return "Pixel Art Scaling"; }
    virtual lpi::gui::Element* getDialog() { return &dialog; }
    virtual std::string getHelp() { return "[OperationPixelArtScaling Help]"; }
};

class OperationRotate : public AOperationFilterBase
{
  private:
    virtual void doit(Paint& paint, Progress& progress);
    DynamicPageWithAutoUpdate dialog;
    
    double angle; //in degrees
    bool ccw; //counter clockwise (negates angle)
    bool smooth;
    bool wrapping;
    bool lock_scale;
    
  public:
    OperationRotate(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom);
    virtual bool operate(Paint& paint, UndoState& state, Progress& progress);
    virtual bool operateGL(Paint& paint, UndoState& state);
    virtual void undo(Paint& paint, UndoState& state);
    virtual void redo(Paint& paint, UndoState& state);

    virtual std::string getLabel() const { return "Rotate"; }
    virtual lpi::gui::Element* getDialog() { return &dialog; }
    virtual std::string getHelp() { return "[OperationRotate Help]"; }
};

class OperationTransform2D;

class Transform2DMatrixDialog : public lpi::gui::ElementComposite
{
  private:

    OperationTransform2D* op;

  public:
    DynamicPageWithAutoUpdate page;
    Numbers numbers;

    Transform2DMatrixDialog(OperationTransform2D* op, const lpi::gui::IGUIDrawer& geom);
    virtual void drawImpl(lpi::gui::IGUIDrawer& drawer) const;
    virtual void handleImpl(const lpi::IInput& input);
};

class OperationTransform2D : public AOperationFilterBase
{
  friend class Transform2DMatrixDialog;
  
  private:
    virtual void doit(Paint& paint, Progress& progress);
    Transform2DMatrixDialog dialog;
    
    double matrix[4]; //2x2 matrix
    bool inverse;
    bool smooth;
    bool wrapping;
    bool lock_scale;

  public:
    OperationTransform2D(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom)
    : AOperationFilterBase(settings)
    , dialog(this, geom)
    , inverse(false)
    , smooth(true)
    , wrapping(false)
    , lock_scale(false)
    {
      for(size_t i = 0; i < 4; i++) matrix[i] = 0;
      matrix[0] = 1;
      matrix[1] = -0.5;
      matrix[3] = 1;
      dialog.numbers.valueToControl(matrix);
    }
    virtual bool operate(Paint& paint, UndoState& state, Progress& progress);
    virtual bool operateGL(Paint& paint, UndoState& state);
    virtual void undo(Paint& paint, UndoState& state);
    virtual void redo(Paint& paint, UndoState& state);

    virtual std::string getLabel() const { return "Transform 2D"; }
    virtual lpi::gui::Element* getDialog() { return &dialog; }
    virtual std::string getHelp() { return "[OperationTransform2D Help]"; }
};

class OperationAdjustBrightnessContrast : public AOperationFilterUtil
{
  private:
    virtual void doit(TextureRGBA8& texture, Progress& progress);
    virtual void doit(TextureRGBA32F& texture, Progress& progress);
    virtual bool supportsColorMode(ColorMode mode) const { return sup(mode, true, true, false, false); }

    DynamicFilterPageWithPreview dialog;
    double brightness; //-1.0 - +1.0
    double contrast; // -0.1- +1.0
    double lgamma; //logarithm of gamma (so that the slider is more uniform)
    bool affect_alpha;
    
  public:
    OperationAdjustBrightnessContrast(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom);

    virtual std::string getLabel() const { return "Brightness/Contrast"; }

    virtual lpi::gui::Element* getDialog() { return &dialog; }
    virtual std::string getHelp() { return "[OperationAdjustBrightnessContrast Help]"; }
};

class OperationAdjustTemperature : public AOperationFilterUtil
{
  private:
    virtual void doit(TextureRGBA8& texture, Progress& progress);

    DynamicFilterPageWithPreview dialog;
    double adjust; //-1.0 - +1.0
    
  public:
    OperationAdjustTemperature(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom);

    virtual std::string getLabel() const { return "Temperature"; }

    virtual lpi::gui::Element* getDialog() { return &dialog; }
    virtual std::string getHelp() { return "[OperationAdjustTemperature Help]"; }
};

class OperationPosterize : public AOperationFilterUtil
{
  private:
    virtual void doit(TextureRGBA8& texture, Progress& progress);

    DynamicFilterPageWithPreview dialog;
    int amount; //number of possible values per channel (2-256) (we don't work with bits here, use powers of two for that, e.g. 64 is 6 bits)
    bool affect_alpha;
    
  public:
    OperationPosterize(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom);

    virtual std::string getLabel() const { return "Posterize"; }
    virtual lpi::gui::Element* getDialog() { return &dialog; }
    virtual std::string getHelp() { return "[OperationPosterize Help]"; }
};

class OperationReduceColors : public AOperationFilterUtil
{
  private:
    virtual void doit(TextureRGBA8& texture, Progress& progress);

    DynamicFilterPageWithPreview dialog;
    int amount; //number of colors to reduce to
    
  public:
    OperationReduceColors(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom);

    virtual std::string getLabel() const { return "Reduce Colors"; }
    virtual lpi::gui::Element* getDialog() { return &dialog; }
    virtual std::string getHelp() { return "[OperationReduceColors Help]"; }
};

class OperationChannelSwitcher : public AOperationFilterUtil
{
  private:
    virtual void doit(TextureRGBA8& texture, Progress& progress);

    DynamicFilterPageWithPreview dialog;
    enum ColorModel
    {
      CM_RGB,
      CM_HSV,
      CM_HSL,
      CM_CMYK,
      CM_XYZ,
      CM_Lab,
      CM_YPbPr,
      CM_YCbCr,
      CM_Count //don't use, it's just for the count
    };

    enum SourceChannel
    {
      SC_Red,
      SC_Green,
      SC_Blue,
      SC_Alpha,
      SC_Lightness,
      SC_Value,
      SC_Intensity,
      SC_Luma,
      SC_Chroma,
      SC_Hue,
      SC_Min,
      SC_Middle,
      SC_Max,
      SC_0,
      SC_64,
      SC_128,
      SC_192,
      SC_255,
      SC_Count //don't use, it's just for the count
    };

    ColorModel cm;
    SourceChannel c1;
    SourceChannel c2;
    SourceChannel c3;
    SourceChannel c4; //only for CMYK (or other color models that use 4 instead of 3 channels)
    SourceChannel ca; //alpha

    int getChannel(SourceChannel c, unsigned char* pixel);
    lpi::ColorRGB getColor(int channel1, int channel2, int channel3, int channel4, int channela,  ColorModel cm);


  public:
    OperationChannelSwitcher(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom);

    virtual std::string getLabel() const { return "Channel Switcher"; }

    virtual lpi::gui::Element* getDialog() { return &dialog; }
    
    virtual std::string getHelp() { return "[OperationChannelSwitcher Help]"; }
};

class OperationChannelSwitcherII : public AOperationFilterUtil
{
  private:
    virtual void doit(TextureRGBA8& texture, Progress& progress);

    DynamicFilterPageWithPreview dialog;
    enum ColorChannel
    {
      CC_RGB_R,
      CC_RGB_G,
      CC_RGB_B,
      CC_HSV_H,
      CC_HSV_S,
      CC_HSV_V,
      CC_HSL_H,
      CC_HSL_S,
      CC_HSL_L,
      CC_CMYK_C,
      CC_CMYK_M,
      CC_CMYK_Y,
      CC_CMYK_K,
      CC_XYZ_X,
      CC_XYZ_Y,
      CC_XYZ_Z,
      CC_Lab_L,
      CC_Lab_a,
      CC_Lab_b,
      CC_YPbPr_Y,
      CC_YPbPr_Pb,
      CC_YPbPr_Pr,
      CC_YCbCr_Y,
      CC_YCbCr_Cb,
      CC_YCbCr_Cr,
      CC_A, //alpha
      CC_0, //set to 0
      CC_Count //don't use, it's just for the count
    };
    
    //which color channel to store in r, g, b and a respectively
    ColorChannel cr;
    ColorChannel cg;
    ColorChannel cb;
    ColorChannel ca;
    
    bool greyscale; //only R is used, G and B are set to same value as R, A is left as it is
    
    double getChannel(const lpi::ColorRGBd& color, ColorChannel type);
    
  public:
    OperationChannelSwitcherII(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom);

    virtual std::string getLabel() const { return "Channel Switcher II"; }

    virtual lpi::gui::Element* getDialog() { return &dialog; }
    
    virtual std::string getHelp() { return "[OperationChannelSwitcherII Help]"; }
};

class OperationAdjustHSL : public AOperationFilterUtil
{
  private:
    virtual void doit(TextureRGBA8& texture, Progress& progress);

    DynamicFilterPageWithPreview dialog;
    int h; //-180 - +180
    int s; //-100 - +100
    int l; // -100 - +100
    int a; //-100 - +100
    
  public:
    OperationAdjustHSL(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom);

    virtual std::string getLabel() const { return "Adjust HSLA"; }
    virtual lpi::gui::Element* getDialog() { return &dialog; }
    virtual std::string getHelp() { return "[OperationAdjustHSL Help]"; }
};

class OperationAdjustHSV : public AOperationFilterUtil
{
  private:
    virtual void doit(TextureRGBA8& texture, Progress& progress);

    DynamicFilterPageWithPreview dialog;
    int h; //-180 - +180
    int s; //-100 - +100
    int v; // -100 - +100
    int a; //-100 - +100

  public:
    OperationAdjustHSV(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom);

    virtual std::string getLabel() const { return "Adjust HSVA"; }
    virtual lpi::gui::Element* getDialog() { return &dialog; }
    virtual std::string getHelp() { return "[OperationAdjustHSV Help]"; }
};

class OperationMultiplySLA : public AOperationFilterUtil
{
  private:
    virtual void doit(TextureRGBA8& texture, Progress& progress);

    DynamicFilterPageWithPreview dialog;
    double s;
    double l;
    double a;
    bool inverted;

  public:
    OperationMultiplySLA(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom);

    virtual std::string getLabel() const { return "Multiply SLA"; }
    virtual lpi::gui::Element* getDialog() { return &dialog; }
    virtual std::string getHelp() { return "[OperationMultiplySLA Help]"; }
};

class OperationColorArithmetic : public AOperationFilterUtil
{
  private:
    virtual void doit(TextureRGBA8& texture, Progress& progress);
    virtual void doit(TextureRGBA32F& texture, Progress& progress);
    virtual bool supportsColorMode(ColorMode mode) const { return sup(mode, true, true, false, false); }
    void calc(lpi::ColorRGBd& rgb);

    DynamicFilterPageWithPreview dialog;
    lpi::ColorRGBd color;
    bool affect_alpha;
    enum Op
    {
      OP_ADD,
      OP_SUBTRACT,
      OP_MULTIPLY,
      OP_INVMULTIPLY,
      OP_DIVIDE,
      OP_LIGHTEST,
      OP_DARKEST,
      //OP_GREYEST,
      OP_AND255,
      OP_OR255,
      OP_XOR255
    };
    Op op;

    enum Overflow
    {
      OF_CLAMP,
      OF_WRAP
    };
    Overflow overflow;

  public:
    OperationColorArithmetic(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom);

    virtual std::string getLabel() const { return "Color Arithmetic"; }
    virtual lpi::gui::Element* getDialog() { return &dialog; }
    virtual std::string getHelp() { return "[OperationColorArithmetic Help]"; }
};

class OperationColorArithmeticII : public AOperationFilterUtil
{
  private:
    virtual void doit(TextureRGBA8& texture, Progress& progress);
    virtual void doit(TextureRGBA32F& texture, Progress& progress);
    virtual bool supportsColorMode(ColorMode mode) const { return sup(mode, true, true, false, false); }
    double calc(double value);
    unsigned char clamp(double value); //clamps in range 0-255
    
    double a;
    double b;
    double c;
    double d;
    double e;
    
    //color channels to affect
    bool affect_r;
    bool affect_g;
    bool affect_b;
    bool affect_a;
    
    enum Formula
    {
      FORMULA1,
      FORMULA2,
      FORMULA3,
      FORMULA4
    };
    Formula formula;

    DynamicFilterPageWithPreview dialog;

  public:
    OperationColorArithmeticII(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom);

    virtual std::string getLabel() const { return "Color Arithmetic II (HDR)"; }
    virtual lpi::gui::Element* getDialog() { return &dialog; }
    virtual std::string getHelp() { return "[OperationColorArithmeticII Help]"; }
};

class OperationToneMapHistogram : public AOperationFilterUtil
{
  private:
    virtual void doit(TextureRGBA8& texture, Progress& progress);
    virtual void doit(TextureGrey8& texture, Progress& progress);
    virtual void doit(TextureRGBA32F& texture, Progress& progress);
    virtual void doit(TextureGrey32F& texture, Progress& progress);
    virtual bool supportsColorMode(ColorMode mode) const { return sup(mode, true, true, true, true); }

    DynamicFilterPageWithPreview dialog;
    
    bool desaturate;

  public:
    OperationToneMapHistogram(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom);

    virtual std::string getLabel() const { return "Tone Map (Histogram Adjustment)"; }
    virtual lpi::gui::Element* getDialog() { return &dialog; }
    virtual std::string getHelp() { return "[OperationToneMapHistogram Help]"; }
};

class OperationRGBAMatrix;

class RGBAMatrixDialog : public FilterDialogWithPreview
{
  private:

    OperationRGBAMatrix* op;

  public:
    DynamicPageWithAutoUpdate page;
    Numbers numbers;

    RGBAMatrixDialog(OperationRGBAMatrix* op, const lpi::gui::IGUIDrawer& geom);
    virtual void drawImpl(lpi::gui::IGUIDrawer& drawer) const;
    virtual void handleImpl(const lpi::IInput& input);
};

class OperationRGBAMatrix : public AOperationFilterUtil
{
  public:
    friend class RGBAMatrixDialog;
  private:
    virtual void doit(TextureRGBA8& texture, Progress& progress);
    virtual void doit(TextureRGBA32F& texture, Progress& progress);
    virtual bool supportsColorMode(ColorMode mode) const { return sup(mode, true, true, false, false); }
    void calc(lpi::ColorRGBd& rgba);

    /*
      r  g  b  a
    r .  .  .  .
    g .  .  .  .
    b .  .  .  .
    a .  .  .  .
    */

    double matrix[16];

    double mul;
    double div;
    double add;
    
    bool affect_alpha; //this is there if you don't want the mul, div and add to affect alpha

    RGBAMatrixDialog dialog;

  public:
    OperationRGBAMatrix(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom)
    : AOperationFilterUtil(settings, true, true)
    , mul(1)
    , div(1)
    , add(0)
    , affect_alpha(true)
    , dialog(this, geom)
    {
      for(size_t i = 0; i < 16; i++) matrix[i] = 0;
      matrix[0] = 1;
      matrix[5] = 1;
      matrix[10] = 1;
      matrix[15] = 1;
      dialog.numbers.valueToControl(matrix);
    }
    virtual lpi::gui::Element* getDialog() { return &dialog; }
    virtual std::string getLabel() const { return "RGBA Matrix"; }
    virtual std::string getHelp() { return "[OperationRGBAMatrix Help]"; }
};

class OperationDeleteSelection : public AOperationFilterBase
{
  public:
    OperationDeleteSelection(GlobalToolSettings& settings) : AOperationFilterBase(settings) {}
    virtual bool operate(Paint& paint, UndoState& state, Progress& progress);
    virtual bool operateGL(Paint& paint, UndoState& state);

    virtual void undo(Paint& paint, UndoState& state);
    virtual void redo(Paint& paint, UndoState& state);

    virtual std::string getLabel() const { return "Delete Selection"; }
};

class OperationSelectAll : public AOperationFilterBase
{
  public:
    OperationSelectAll(GlobalToolSettings& settings) : AOperationFilterBase(settings) {}
    virtual bool operate(Paint& paint, UndoState& state, Progress& progress);
    virtual bool operateGL(Paint& paint, UndoState& state);

    virtual void undo(Paint& paint, UndoState& state);
    virtual void redo(Paint& paint, UndoState& state);

    virtual std::string getLabel() const { return "Select All"; }
};

class OperationSelectNone : public AOperationFilterBase
{
  public:
    OperationSelectNone(GlobalToolSettings& settings) : AOperationFilterBase(settings) {}
    virtual bool operate(Paint& paint, UndoState& state, Progress& progress);
    virtual bool operateGL(Paint& paint, UndoState& state);

    virtual void undo(Paint& paint, UndoState& state);
    virtual void redo(Paint& paint, UndoState& state);

    virtual std::string getLabel() const { return "Select None"; }
};

class OperationBlendSelection : public AOperationFilterBase
{
  private:
   DynamicPageWithAutoUpdate dialog;
   
   double opacity;
   
   bool treat_alpha_as_opacity; //whether or not to treat the alpha channel of the SOURCE (selection) as opacity
   bool affect_alpha; //whether or not to affect the alpha of the TARGET (image)
   
   enum BlendMode
   {
     BM_ALPHA_BLEND, //this one is a bit special compared to the rest (TODO: make separate filter for it?)
     BM_AVERAGE,
     BM_DIFFERENCE,
     BM_ADD,
     BM_ADD_MIN_128,
     BM_SUBTRACT,
     BM_MULTIPLY,
     BM_INVMULTIPLY, //aka "screen"
     BM_OVERLAY,
     BM_HARD_LIGHT,
     BM_SOFT_LIGHT,
     BM_DIVIDE,
     BM_DODGE,
     BM_BURN,
     BM_EXCLUSION,
     BM_LIGHTEST,
     BM_DARKEST,
     BM_AND,
     BM_OR,
     BM_XOR,
     BM_COPY_ALPHA
   };
   
   BlendMode blendMode;


  public:
    OperationBlendSelection(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom);
    virtual bool operate(Paint& paint, UndoState& state, Progress& progress);
    virtual bool operateGL(Paint& paint, UndoState& state);

    virtual void undo(Paint& paint, UndoState& state);
    virtual void redo(Paint& paint, UndoState& state);

    virtual std::string getLabel() const { return "Blend Selection"; }
    virtual lpi::gui::Element* getDialog() { return &dialog; }
    virtual std::string getHelp() { return "[OperationBlendSelection Help]"; }
};

class OperationSetColorMode : public AOperationFilterBase
{
  private:
    ColorMode mode;
  public:
    OperationSetColorMode(GlobalToolSettings& settings, ColorMode mode) : AOperationFilterBase(settings), mode(mode) {}
    virtual bool operate(Paint& paint, UndoState& state, Progress& progress);
    virtual bool operateGL(Paint& paint, UndoState& state);

    virtual void undo(Paint& paint, UndoState& state);
    virtual void redo(Paint& paint, UndoState& state);

    virtual std::string getLabel() const;
};

class OperationCurves;

class CurvesFilterDialog : public FilterDialogWithPreview
{
  private:
    
    OperationCurves* filter;
    
  public:
    friend class OperationCurves;
  
    DynamicPageWithAutoUpdate page;
    CurvesDialog curves;

    CurvesFilterDialog(OperationCurves* filter, const lpi::gui::IGUIDrawer& geom);
    virtual void drawImpl(lpi::gui::IGUIDrawer& drawer) const;
    virtual void handleImpl(const lpi::IInput& input);
};

class OperationCurves : public AOperationFilterUtil
{
  public:
    friend class CurvesFilterDialog;
  private:
    virtual void doit(TextureRGBA8& texture, Progress& progress);

    bool affect_alpha;
    
    enum Type
    {
      TYPE_COLOR,
      TYPE_GREY
    };
    
    Type type;

    CurvesFilterDialog dialog;

  public:
    OperationCurves(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom)
    : AOperationFilterUtil(settings, true, true)
    , affect_alpha(true)
    , type(TYPE_COLOR)
    , dialog(this, geom)
    {
    }
    virtual lpi::gui::Element* getDialog() { return &dialog; }
    virtual std::string getLabel() const { return "Curves"; }
    virtual std::string getHelp() { return "[OperationCurves Help]"; }
};


class OperationLevels : public AOperationFilterUtil
{
  private:
    virtual void doit(TextureRGBA8& texture, Progress& progress);

    DynamicFilterPageWithPreview dialog;
    double input_black; //0.0-1.0 representing color channel value 0-255
    double input_midpoint; //0.0-1.0 representing value in between black and white
    double input_white; //0.0-1.0 representing color channel value 0-255
    
    double output_black; //0.0-1.0 representing color channel value 0-255
    double output_white; //0.0-1.0 representing color channel value 0-255
    
    bool affect_r;
    bool affect_g;
    bool affect_b;
    bool affect_a;
    
    double doFormula(double val) const;
    
    
  public:
    OperationLevels(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom);

    virtual std::string getLabel() const { return "Levels"; }
    virtual lpi::gui::Element* getDialog() { return &dialog; }
    virtual std::string getHelp() { return "[OperationLevels Help]"; }
};

class OperationRGChromaticity : public AOperationFilterUtil
{
  private:
    virtual void doit(TextureRGBA8& texture, Progress& progress);
    
  public:
    OperationRGChromaticity(GlobalToolSettings& settings)
    : AOperationFilterUtil(settings, true, true)
    {
    }
    
    virtual std::string getLabel() const { return "RG Chromaticity"; }
};

//This filter interprets pixel values as wavelength values in range 400-700nm, and generates
//the RGB color for that wavelength. For greyscale images, this describes the whole process.
//For color input images, the R, G and B values are interpreted as 3 separate wavelengths,
//added together, with new RGB color value calculated from that.
class OperationSpectrum : public AOperationFilterUtil
{
  private:
    virtual void doit(TextureRGBA8& texture, Progress& progress);
    
  public:
    OperationSpectrum(GlobalToolSettings& settings)
    : AOperationFilterUtil(settings, true, true)
    {
    }
    
    virtual std::string getLabel() const { return "Spectrum"; }
};
