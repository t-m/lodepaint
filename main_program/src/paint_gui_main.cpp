/*
LodePaint

Copyright (c) 2009-2011 Lode Vandevenne
All rights reserved.

This file is part of LodePaint.

LodePaint is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LodePaint is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LodePaint.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "paint_gui_main.h"

#include "main.h"

#include "lpi/lpi_translate.h"
#include "lpi/gui/lpi_gui_drawer_gl.h"

OptionsWindow::OptionsWindow( const lpi::gui::IGUIDrawer& geom)
{
  resize(0, 0, 500, 300);
  addTop(geom);
  addTitle(_t("Options"));
  addCloseButton(geom);
  addResizer(geom);
  setColorMod(lpi::ColorRGB(255,255,255,224));

  page.addControl(_t("Undo Size (MB)"), new lpi::gui::DynamicSliderSpinner<int>(&options.undoSize, 1, 2000, 1, geom));
  page.addToolTipToLastRow(_t("[Undo Memory Tooltip]"));
  page.addControl(_t("Screen BG Color"), new lpi::gui::DynamicColor(&options.bgcolor, geom));
  page.addControl(_t("Window Color"), new lpi::gui::DynamicColor(&options.drawingWindowColor, geom));
  page.addControl(_t("Window Top Color"), new lpi::gui::DynamicColor(&options.windowTopColor, geom));
  page.addControl(_t("Custom Pixel Grid Width"), new lpi::gui::DynamicSliderSpinner<int>(&options.pixelGridCustomWidth, 4, 128, 1, geom));
  page.addToolTipToLastRow(_t("[Custom Grid Tooltip]"));
  page.addControl(_t("Custom Pixel Grid Height"), new lpi::gui::DynamicSliderSpinner<int>(&options.pixelGridCustomHeight, 4, 128, 1, geom));
  page.addToolTipToLastRow(_t("[Custom Grid Tooltip]"));
  page.addControl(_t("Override Plugin Dir"), new lpi::gui::DynamicValue<std::string>(&options.pathsOverride.plugindir));
  page.addControl(_t("Use Native OS File Dialog"), new lpi::gui::DynamicCheckbox(&options.useOSNativeFileDialog));
  page.addToolTipToLastRow(_t("[Native File Dialog Tooltip]"));

  page.addTextRow(_t("Brush Dynamics (ALT+Scrollwheel)"));
  std::vector<DynamicBrushChange::Change> enums; for(size_t i = 0; i < DynamicBrushChange::C_NUM_ENUM; i++) enums.push_back((DynamicBrushChange::Change)i);
  std::vector<std::string> names; names.push_back(_t("Nothing")); names.push_back(_t("Brush Size")); names.push_back(_t("Brush Opacity")); names.push_back(_t("Brush Softness"));
  names.push_back(_t("FG Hue")); names.push_back(_t("FG Saturation")); names.push_back(_t("FG Brightness")); names.push_back(_t("FG Alpha"));
  names.push_back(_t("BG Hue")); names.push_back(_t("BG Saturation")); names.push_back(_t("BG Brightness")); names.push_back(_t("BG Alpha"));
  page.addControl(_t("Dyn. Change 1"), new lpi::gui::DynamicEnum<DynamicBrushChange::Change>(&options.dyn.change1, names, enums, geom));
  page.addControl(_t("Amount 1"), new lpi::gui::DynamicSliderSpinner<double>(&options.dyn.amount1, 1.0, 20.0, 1.0, geom));
  page.addControl(_t("Dyn. Change 2"), new lpi::gui::DynamicEnum<DynamicBrushChange::Change>(&options.dyn.change2, names, enums, geom));
  page.addControl(_t("Amount 2"), new lpi::gui::DynamicSliderSpinner<double>(&options.dyn.amount2, 1.0, 20.0, 1.0, geom));


  ok.makeTextPanel(0, 0, "ok");
  cancel.makeTextPanel(0, 0, "cancel");

  pushTop(&page, lpi::gui::Sticky(0.0,8, 0.0,8, 1.0,-8, 1.0,-92));
  pushTop(&cancel, lpi::gui::Sticky(1.0,-84, 1.0,-24, 1.0,-4, 1.0,-4));
  pushTop(&ok, lpi::gui::Sticky(1.0,-168, 1.0,-24, 1.0,-88, 1.0,-4));
}

void OptionsWindow::handleImpl(const lpi::IInput& input)
{
  Dialog::handleImpl(input);
  page.controlToValue();
  if(ok.clicked(input)) { result = OK; setEnabled(false); }
  if(cancel.clicked(input) || closeButtonClicked(input)) { result = CANCEL; setEnabled(false); }
}

const Options& OptionsWindow::getOptions() const { return options; }
Options& OptionsWindow::getOptions() { return options; }

void OptionsWindow::valueToControls()
{
  page.valueToControl();
}

ToolSettingWindow::ToolSettingWindow(int x, int y, const lpi::gui::IGUIDrawer& geom)
{
  resize(x, y, x + 300, y + 160);
  addTop(geom);
  addTitle(_t("Tool Settings"));
  //addCloseButton(geom);
  addResizer(geom);
  setColorMod(lpi::ColorRGB(255,255,255,192));
  //addScrollbars(geom);
}

void ToolSettingWindow::clearCustomTab()
{
  clear();
}

void ToolSettingWindow::setCustomTab(lpi::gui::Element* e)
{
  pushTop(e, lpi::gui::Sticky(0.0,8, 0.0,8, 1.0,-8, 1.0,-8));
  //pushTop(e);
}

void ToolSettingWindow::handleImpl(const lpi::IInput& input)
{
  Window::handleImpl(input);
  //updateScroll();
}



static void addFiltersToMenu(lpi::gui::MenuVertical& menu, std::vector<lpi::gui::MenuVertical*>& submenu, Filters& filters, lpi::gui::IGUIDrawer& guidrawer)
{
  for(size_t i = 0; i < filters.getSubFilters().size(); i++)
  {
    submenu.push_back(new lpi::gui::MenuVertical);
    menu.addSubMenu(submenu[i], _t(filters.getSubFilters()[i].name), guidrawer);
    for(size_t j = 0; j < filters.getSubFilters()[i].filters.size(); j++)
    {
      IOperationFilter* filter = filters.getSubFilters()[i].filters[j];
      if(filter == 0)
      {
        submenu[i]->addSeparator(guidrawer);
      }
      else
      {
        std::string label = filter->getDialog() ? _t(filter->getLabel()) + "..." : _t(filter->getLabel());
        submenu[i]->addCommand(label, guidrawer);
      }
    }
  }

  for(size_t j = 0; j < filters.getRootFilters().filters.size(); j++)
  {
    IOperationFilter& filter = *filters.getRootFilters().filters[j];
    std::string label = filter.getDialog() ? _t(filter.getLabel()) + "..." : _t(filter.getLabel());
    menu.addCommand(label, guidrawer);
  }
}

MenuToolbarIndices::MenuToolbarIndices()
: vg_image(submenu_image)
, vg_filters(submenu_filters)
, vg_plugins(submenu_plugins)
{
}

void fillMenusAndToolbars(MenuToolbarIndices& m, MainProgram& p, FiltersCollection& filters)
{
  lpi::gui::GUIDrawerGL& guidrawer = *p.guidrawer;
  m.menuindex_new = m.menu_file.addCommand(_t("New") + "...", guidrawer);
  m.menu_file.addSeparator(guidrawer);
  m.menuindex_open = m.menu_file.addCommand(_t("Open") + "...", guidrawer);
  m.menuindex_reload = m.menu_file.addCommand(_t("Reload"), guidrawer);
  m.menu_file.addSubMenu(p.recentMenu, _t("Recent"), guidrawer);
  m.menu_file.addSeparator(guidrawer);
  m.menuindex_save = m.menu_file.addCommand(_t("Save"), guidrawer);
  m.menuindex_saveas = m.menu_file.addCommand(_t("Save As") + "...", guidrawer);
  m.menuindex_savecopyas = m.menu_file.addCommand(_t("Save Copy As") + "...", guidrawer);
  m.menuindex_saveall = m.menu_file.addCommand(_t("Save All") + "...", guidrawer);
  m.menu_file.addSeparator(guidrawer);
  m.menuindex_close = m.menu_file.addCommand(_t("Close"), guidrawer);
  m.menuindex_closeall = m.menu_file.addCommand(_t("Close All"), guidrawer);
  m.menu_file.addSeparator(guidrawer);
  m.menuindex_exit = m.menu_file.addCommand(_t("Exit"), guidrawer);

  m.menuindex_undo = m.menu_edit.addCommand(_t("Undo"), guidrawer);
  m.menuindex_redo = m.menu_edit.addCommand(_t("Redo"), guidrawer);
  m.menu_edit.addSeparator(guidrawer);
  m.menuindex_cut = m.menu_edit.addCommand(_t("Cut"), guidrawer);
  m.menuindex_copy = m.menu_edit.addCommand(_t("Copy"), guidrawer);
  m.menuindex_paste_image = m.menu_edit.addCommand(_t("Paste As New Image"), guidrawer);
  m.menuindex_paste_sel = m.menu_edit.addCommand(_t("Paste As Selection"), guidrawer);
  //m.menu_edit.addSeparator(guidrawer);
  //m.menuindex_temp_debug = m.menu_edit.addCommand(_t("Temp Debug (Don't Use)"), guidrawer);

  bool hasskin0 = skinExists(p, p.optionsNow, "default");
  bool hasskin1 = skinExists(p, p.optionsNow, "aos4");
  bool hasskin2 = skinExists(p, p.optionsNow, "dark");
  bool hasskin3 = skinExists(p, p.optionsNow, "old");
  if(hasskin0) m.menuindex_skin_default = m.menu_skins.addCommand(_t("Default"), guidrawer);
  else m.menuindex_skin_default = (size_t)(-1);
  if(hasskin1) m.menuindex_skin_aos4 = m.menu_skins.addCommand(_t("Amiga"), guidrawer);
  else m.menuindex_skin_aos4 = (size_t)(-1);
  if(hasskin2) m.menuindex_skin_dark = m.menu_skins.addCommand(_t("Dark"), guidrawer);
  else m.menuindex_skin_dark = (size_t)(-1);
  if(hasskin3) m.menuindex_skin_old = m.menu_skins.addCommand(_t("Old"), guidrawer);
  else m.menuindex_skin_old = (size_t)(-1);
  if(!hasskin0 && !hasskin1 && !hasskin2 && !hasskin3) {
    m.menuindex_skin_none_found_message = m.menu_skins.addCommand(_t("(No Skins)"), guidrawer);
  } else {
    m.menuindex_skin_none_found_message = (size_t)(-1);
  }

  m.menuindex_plugins_none_found_message = m.menu_plugins.addCommand(_t("(Info)"), guidrawer);

  for(size_t i = 0; i < lpi::globalTranslate.getNumLanguages(); i++)
  {
    m.menu_language.addCommand(lpi::globalTranslate.getLanguageKey(i), guidrawer);
  }
  if(lpi::globalTranslate.getNumLanguages() == 0) {
    m.menuindex_lang_none_found_message = m.menu_language.addCommand(_t("(No Languages)"), guidrawer);
  } else {
    m.menuindex_lang_none_found_message = (size_t)(-1);
  }

  m.menuindex_options = m.menu_settings.addCommand(_t("Options") + "...", guidrawer);
  m.menuindex_shortcuts = m.menu_settings.addCommand(_t("Shortcuts") + "...", guidrawer);
  m.menu_settings.addSubMenu(&m.menu_skins, _t("Skins"), guidrawer);
  m.menu_settings.addSubMenu(&m.menu_language, _t("Language"), guidrawer);

  m.menuindex_help = m.menu_help.addCommand(_t("Help") + "...", guidrawer);
  m.menu_help.addSeparator(guidrawer);
  m.menuindex_systeminfo = m.menu_help.addCommand(_t("System Info") + "...", guidrawer);
  m.menu_help.addSeparator(guidrawer);
  m.menuindex_about = m.menu_help.addCommand(_t("About") + "...", guidrawer);

  addFiltersToMenu(m.menu_image, m.submenu_image, filters.getImageMenuFilters(), guidrawer);
  addFiltersToMenu(m.menu_filters, m.submenu_filters, filters.getFiltersMenuFilters(), guidrawer);
  addFiltersToMenu(m.menu_plugins, m.submenu_plugins, filters.getPluginsMenuFilters(), guidrawer);

  m.menuindex_imageinfo = m.menu_image.addCommand(_t("Image Info") + "...", guidrawer);

  m.mainmenu.addSubMenu(&m.menu_file, _t("File"), guidrawer);
  m.mainmenu.addSubMenu(&m.menu_edit, _t("Edit"), guidrawer);
  m.mainmenu.addSubMenu(&m.menu_image, _t("Image"), guidrawer);
  m.mainmenu.addSubMenu(&m.menu_filters, _t("Filters"), guidrawer);
  m.mainmenu.addSubMenu(&m.menu_plugins, _t("Plugins"), guidrawer);
  m.mainmenu.addSubMenu(&m.menu_settings, _t("Settings"), guidrawer);
  m.mainmenu.addSubMenu(&m.menu_help, _t("Help"), guidrawer);

  //////////////////////////////////////////////////////////////////////////////

  m.toolbar_new = p.toolbar.addCommand(&textures_toolbar[0], _t("New")),
  m.toolbar_open = p.toolbar.addCommand(&textures_toolbar[1], _t("Open")),
  m.toolbar_save = p.toolbar.addCommand(&textures_toolbar[2], _t("Save")),
  m.toolbar_save_as = p.toolbar.addCommand(&textures_toolbar[3], _t("Save As")),
  m.toolbar_save_all = p.toolbar.addCommand(&textures_toolbar[4], _t("Save All")),
  p.toolbar.addSeparator();
  m.toolbar_cut = p.toolbar.addCommand(&textures_toolbar[16], _t("Cut")),
  m.toolbar_copy = p.toolbar.addCommand(&textures_toolbar[17], _t("Copy")),
  m.toolbar_paste_image = p.toolbar.addCommand(&textures_toolbar[18], _t("Paste As New Image")),
  m.toolbar_paste_sel = p.toolbar.addCommand(&textures_toolbar[19], _t("Paste As Selection")),
  p.toolbar.addSeparator();
  m.toolbar_undo = p.toolbar.addCommand(&textures_toolbar[32], _t("Undo")),
  m.toolbar_redo = p.toolbar.addCommand(&textures_toolbar[33], _t("Redo")),
  p.toolbar.addSeparator();
  m.toolbar_zoomin = p.toolbar.addCommand(&textures_toolbar[48], _t("Zoom In")),
  m.toolbar_zoomout = p.toolbar.addCommand(&textures_toolbar[49], _t("Zoom Out")),
  m.toolbar_zoom1 = p.toolbar.addCommand(&textures_toolbar[50], _t("Actual Size (& Center)")),
  m.toolbar_fullscreen = p.toolbar.addCommand(&textures_toolbar[51], _t("Show On Entire Screen")),
  p.toolbar.addSeparator();
  m.toolbar_bgpattern = p.toolbar.addCommand(&textures_toolbar[64], _t("Background Pattern (Alpha Channel)")),
  m.toolbar_pixel_raster = p.toolbar.addCommand(&textures_toolbar[65], _t("Pixel Raster")),
  m.toolbar_crosshair = p.toolbar.addCommand(&textures_toolbar[66], _t("Crosshair")),
  //todo: place button to enable/disable and change grid size here
  p.toolbar.addSeparator();
  m.toolbar_mask = p.toolbar.addToggle(&textures_toolbar[67], _t("Toggle Mask"), false),
  m.toolbar_maskcolor = p.toolbar.addCommand(&textures_toolbar[68], _t("Mask Display Color")),
  p.toolbar.addSeparator();
  m.toolbar_filter_on_all = p.toolbar.addToggle(&textures_toolbar[84], _t("Toggle Batch Mode (Do Filters On All)"), false);

  m.toolbar_clear_image = p.toolbar2.addCommand(&textures_filters[0], _t(filters.getClearImageFilter()->getLabel()));
  m.toolbar_crop = p.toolbar2.addCommand(&textures_filters[100], _t(filters.getCropFilter()->getLabel()));
  m.toolbar_flip_x = p.toolbar2.addCommand(&textures_filters[96], _t(filters.getFlipXFilter()->getLabel()));
  m.toolbar_flip_y = p.toolbar2.addCommand(&textures_filters[97], _t(filters.getFlipYFilter()->getLabel()));
  m.toolbar_rescale = p.toolbar2.addCommand(&textures_filters[98], _t(filters.getRescaleFilter()->getLabel()));
  m.toolbar_wrap_50 = p.toolbar2.addCommand(&textures_filters[99], _t(filters.getWrap50Filter()->getLabel()));
  p.toolbar2.addSeparator();
  m.toolbar_negate_rgb = p.toolbar2.addCommand(&textures_filters[1], _t(filters.getNegateRGBFilter()->getLabel()));
  m.toolbar_negate_mask = p.toolbar2.addCommand(&textures_filters[128], _t(filters.getNegateMaskFilter()->getLabel()));
  m.toolbar_normalize = p.toolbar2.addCommand(&textures_filters[2], _t(filters.getNormalizeFilter()->getLabel()));
  m.toolbar_brightness_contrast = p.toolbar2.addCommand(&textures_filters[3], _t(filters.getBrightnessContrastFilter()->getLabel()));
  m.toolbar_hue_saturation = p.toolbar2.addCommand(&textures_filters[4], _t(filters.getHueSaturationFilter()->getLabel()));
  m.toolbar_multiply_sla = p.toolbar2.addCommand(&textures_filters[5], _t(filters.getMultiplySLAFilter()->getLabel()));
  m.toolbar_greyscale = p.toolbar2.addCommand(&textures_filters[6], _t(filters.getGreyscaleFilter()->getLabel()));
  m.toolbar_alpha_opaque = p.toolbar2.addCommand(&textures_filters[80], _t(filters.getAlphaOpaqueFilter()->getLabel()));
  m.toolbar_alpha_apply = p.toolbar2.addCommand(&textures_filters[81], _t(filters.getAlphaApplyFilter()->getLabel()));
  p.toolbar2.addSeparator();
  m.toolbar_gaussian = p.toolbar2.addCommand(&textures_filters[144], _t(filters.getGaussianBlurFilter()->getLabel()));
  m.toolbar_pixel_antialias = p.toolbar2.addCommand(&textures_filters[145], _t(filters.getPixelAntialiasFilter()->getLabel()));
  m.toolbar_blend_sel = p.toolbar2.addCommand(&textures_filters[112], _t(filters.getBlendSelectionFilter()->getLabel()));
  p.toolbar2.addSeparator();
  m.toolbar_test_image = p.toolbar2.addCommand(&textures_filters[160], _t(filters.getTestImageFilter()->getLabel()));
  p.toolbar2.addSeparator();
  m.toolbar_repeat_2 = p.toolbar2.addCommand(&textures_toolbar[80], _t("Repeat Second Last Filter"));
  m.toolbar_repeat = p.toolbar2.addCommand(&textures_toolbar[80], _t("Repeat Last Filter"));
}

////////////////////////////////////////////////////////////////////////////////

ShortcutWindow::ShortcutWindow(ShortCutManager& manager, const lpi::gui::IGUIDrawer& geom)
: list(geom)
, default0(true)
, default1(true)
, manager(manager)
{
  resize(0, 0, 500, 300);
  addTop(geom);
  addTitle(_t("Shortcuts"));
  addCloseButton(geom);
  addResizer(geom);

  for(size_t i = 0; i < manager.getNumCommands(); i++)
  {
    list.addItem(manager.getName((ShortCut)i));
  }

  ok.makeTextPanel(0, 0, _t("ok"));
  cancel.makeTextPanel(0, 0, _t("cancel"));

  pushTop(&list, lpi::gui::Sticky(0.0,8, 0.0,78, 0.0,200, 0.0,94));
  pushTop(&field0, lpi::gui::Sticky(0.0,8, 0.0,102, 0.0,200, 0.0,118));
  pushTop(&field1, lpi::gui::Sticky(0.0,8, 0.0,126, 0.0,200, 0.0,142));
  pushTop(&default0, lpi::gui::Sticky(0.0,208, 0.0,102, 0.0,400, 0.0,118));
  pushTop(&default1, lpi::gui::Sticky(0.0,208, 0.0,126, 0.0,400, 0.0,142));

  pushTop(&cancel, lpi::gui::Sticky(1.0,-84, 1.0,-24, 1.0,-4, 1.0,-4));
  pushTop(&ok, lpi::gui::Sticky(1.0,-168, 1.0,-24, 1.0,-88, 1.0,-4));

  reinit();
}

void ShortcutWindow::reinit()
{
  ShortCut index = (ShortCut)list.getSelectedItem();
  field0.key = manager.getCombo(index, 0);
  field1.key = manager.getCombo(index, 1);
  default0.key = manager.getDefault(index, 0);
  default1.key = manager.getDefault(index, 1);
}

void ShortcutWindow::handleImpl(const lpi::IInput& input)
{
  Dialog::handleImpl(input);

  if(list.hasChanged())
  {
    ShortCut index = (ShortCut)list.getSelectedItem();
    field0.key = manager.getCombo(index, 0);
    field1.key = manager.getCombo(index, 1);
    default0.key = manager.getDefault(index, 0);
    default1.key = manager.getDefault(index, 1);
  }
  else
  {
    ShortCut index = (ShortCut)list.getSelectedItem();
    manager.setCombo(index, field0.key, 0);
    manager.setCombo(index, field1.key, 1);
  }

  if(ok.clicked(input)) { result = OK; setEnabled(false); }
  if(cancel.clicked(input) || closeButtonClicked(input)) { result = CANCEL; setEnabled(false); }
}

void ShortcutWindow::drawImpl(lpi::gui::IGUIDrawer& drawer) const
{
  Dialog::drawImpl(drawer);

  drawer.drawGUIPartText(lpi::gui::GPT_DEFAULT_TEXT00, _t("[Shortcuts Explanation]"), x0 + 4, y0 + 20, x1, y1);

  drawer.drawGUIPartText(lpi::gui::GPT_DEFAULT_TEXT00, _t("defaults:"), default0.getX0(), default0.getY0() - 10, default0.getX1(), default0.getY0() - 1);
}
