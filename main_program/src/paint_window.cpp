/*
LodePaint

Copyright (c) 2009-2010 Lode Vandevenne
All rights reserved.

This file is part of LodePaint.

LodePaint is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LodePaint is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LodePaint.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "paint_window.h"

#include "paint_settings.h"

#include "lpi/sdl/lpi_event.h"
#include "lpi/lpi_file.h"
#include "lpi/gui/lpi_gui_drawer_gl.h"
#include "lpi/lpi_math.h"

#include <cmath>
#include <sstream>
#include <iostream>


////////////////////////////////////////////////////////////////////////////////

FloatingSelection::FloatingSelection()
: texture()
, active(false)
{
}

BackgroundPatternDrawer::BackgroundPatternDrawer(lpi::GLContext* context)
: checker_tex(context)
, bg_color1(lpi::RGB_Black)
, bg_color2(lpi::RGB_White)
, checker_size(12)
, patternindex(-1)
{
  if(context)
  {
    createTexture(&checker_tex, 2, 2, lpi::RGB_Black);
    setPattern(0);
  }
}

void BackgroundPatternDrawer::setPattern(int pattern)
{
  if(pattern == patternindex) return;
  
  patternindex = pattern;
  
  switch(patternindex)
  {
    case 0: bg_color1 = lpi::RGB_Grey; bg_color2 = lpi::RGB_White; break;
    case 1: bg_color1 = lpi::RGB_Grey; bg_color2 = lpi::RGB_Gray; break;
    case 2: bg_color1 = lpi::RGB_Black; bg_color2 = lpi::RGB_White; break;
    case 3: bg_color1 = lpi::RGB_Red; bg_color2 = lpi::RGB_Green; break;
    case 4: bg_color1 = lpi::RGB_Black; bg_color2 = lpi::RGB_Black; break;
    case 5: bg_color1 = lpi::RGB_White; bg_color2 = lpi::RGB_White; break;
  }

  setPixel(&checker_tex, 0, 0, bg_color1);
  setPixel(&checker_tex, 0, 1, bg_color2);
  setPixel(&checker_tex, 1, 0, bg_color2);
  setPixel(&checker_tex, 1, 1, bg_color1);
  checker_tex.update();
}

void BackgroundPatternDrawer::drawBackgroundPattern(lpi::IDrawer2D& drawer, int x0, int y0, int x1, int y1) const
{

  drawer.pushSmallestScissor(x0, y0, x1, y1);

  drawer.drawTextureSizedRepeated(&checker_tex, x0, y0, x1, y1, checker_size, checker_size);

  drawer.popScissor();
}

////////////////////////////////////////////////////////////////////////////////



Paint::RectSel::RectSel()
: x0(0)
, y0(0)
, x1(0)
, y1(0)
{
}

Paint::Paint(Options& options, lpi::GLContext* context)
: bgpattern(context)
, zoom_factor(1.0)
, zoomfactorindex(0)
, colorMode(MODE_RGBA_8)
, mask_enabled(false)
, maskColor(lpi::RGB_Red)
, maskColorIndex(-1)
, pixelGridWidth(1)
, pixelGridHeight(1)
, options(options)
, mask8(GL_ALPHA, GL_ALPHA)
, mask32F(GL_ALPHA, GL_ALPHA)
{
  setMaskColorIndex(0);
}

void Paint::make(int sizex, int sizey, const lpi::ColorRGB& backColor)
{
  createTextureRGBA<unsigned char>(&canvasRGBA8, sizex, sizey, backColor);
  update();
}

size_t Paint::getU() const
{
  if(colorMode == MODE_RGBA_8) return canvasRGBA8.getU();
  else if(colorMode == MODE_RGBA_32F) return canvasRGBA32F.getU();
  else if(colorMode == MODE_GREY_8) return canvasGrey8.getU();
  else if(colorMode == MODE_GREY_32F) return canvasGrey32F.getU();
  else return 0;
}
size_t Paint::getV() const
{
  if(colorMode == MODE_RGBA_8) return canvasRGBA8.getV();
  else if(colorMode == MODE_RGBA_32F) return canvasRGBA32F.getV();
  else if(colorMode == MODE_GREY_8) return canvasGrey8.getV();
  else if(colorMode == MODE_GREY_32F) return canvasGrey32F.getV();
  else return 0;
}

void Paint::setMaskColorIndex(int index)
{
  if(index == maskColorIndex) return;
  
  maskColorIndex = index;
  
  int alpha = 224;
  
  switch(maskColorIndex)
  {
    case 0: maskColor = lpi::ColorRGB(255, 0, 0, alpha); break;
    case 1: maskColor = lpi::ColorRGB(0, 255, 0, alpha); break;
    case 2: maskColor = lpi::ColorRGB(0, 0, 255, alpha); break;
    case 3: maskColor = lpi::ColorRGB(128, 128, 128, alpha); break;
    case 4: maskColor = lpi::ColorRGB(0, 0, 0, alpha); break;
    case 5: maskColor = lpi::ColorRGB(255, 255, 255, alpha); break;
  }
}

void Paint::setPixelGridIndex(int index)
{
  switch(index)
  {
    case 0: pixelGridWidth = pixelGridHeight = 0; break;
    case 1: pixelGridWidth = pixelGridHeight = 1; break;
    case 2:
      pixelGridWidth = options.pixelGridCustomWidth;
      pixelGridHeight = options.pixelGridCustomHeight;
      break;
  }
}

void Paint::updateSize()
{
  if(colorMode == MODE_RGBA_8)
  {
    this->setSizeX((int)(canvasRGBA8.getU() * zoom_factor));
    this->setSizeY((int)(canvasRGBA8.getV() * zoom_factor));

    if(canvasRGBA8.getU() != mask8.getU() || canvasRGBA8.getV() != mask8.getV())
    {
      //mask.setSize(canvasRGBA8.getU(), canvasRGBA8.getV());
      //createTextureGrey<float>(&mask32, canvasRGBA8.getU(), canvasRGBA8.getV(), 1.0f);
      createTextureGrey<unsigned char>(&mask8, canvasRGBA8.getU(), canvasRGBA8.getV(), 255);
    }

  }
  else if(colorMode == MODE_RGBA_32F)
  {
    this->setSizeX((int)(canvasRGBA32F.getU() * zoom_factor));
    this->setSizeY((int)(canvasRGBA32F.getV() * zoom_factor));

    if(canvasRGBA32F.getU() != mask32F.getU() || canvasRGBA32F.getV() != mask32F.getV())
    {
      createTextureGrey<float>(&mask32F, canvasRGBA32F.getU(), canvasRGBA32F.getV(), 1.0f);
    }
  }
  else if(colorMode == MODE_GREY_8)
  {
    this->setSizeX((int)(canvasGrey8.getU() * zoom_factor));
    this->setSizeY((int)(canvasGrey8.getV() * zoom_factor));

    if(canvasGrey8.getU() != mask8.getU() || canvasGrey8.getV() != mask8.getV())
    {
      createTextureGrey<unsigned char>(&mask8, canvasGrey8.getU(), canvasGrey8.getV(), 255);
    }
  }
  else if(colorMode == MODE_GREY_32F)
  {
    this->setSizeX((int)(canvasGrey32F.getU() * zoom_factor));
    this->setSizeY((int)(canvasGrey32F.getV() * zoom_factor));

    if(canvasGrey32F.getU() != mask32F.getU() || canvasGrey32F.getV() != mask32F.getV())
    {
      createTextureGrey<float>(&mask32F, canvasGrey32F.getU(), canvasGrey32F.getV(), 1.0f);
    }
  }
}

void Paint::update()
{
  updateSize();
  upload();
}

void Paint::upload()
{
  if(colorMode == MODE_RGBA_8)
  {
    canvasRGBA8.update();
    mask8.update();
  }
  else if(colorMode == MODE_RGBA_32F)
  {
    canvasRGBA32F.update();
    mask32F.update();
  }
  else if(colorMode == MODE_GREY_8)
  {
    canvasGrey8.update();
    mask8.update();
  }
  else if(colorMode == MODE_GREY_32F)
  {
    canvasGrey32F.update();
    mask32F.update();
  }

}

void Paint::uploadPartial(int x0, int y0, int x1, int y1)
{
  if(colorMode == MODE_RGBA_8)
  {
    canvasRGBA8.updatePartial(x0, y0, x1, y1);
    mask8.updatePartial(x0, y0, x1, y1);
  }
  else if(colorMode == MODE_RGBA_32F)
  {
    canvasRGBA32F.updatePartial(x0, y0, x1, y1);
    mask32F.updatePartial(x0, y0, x1, y1);
  }
  if(colorMode == MODE_GREY_8)
  {
    canvasGrey8.updatePartial(x0, y0, x1, y1);
    mask8.updatePartial(x0, y0, x1, y1);
  }
  else if(colorMode == MODE_GREY_32F)
  {
    canvasGrey32F.updatePartial(x0, y0, x1, y1);
    mask32F.updatePartial(x0, y0, x1, y1);
  }

}

void Paint::clear()
{
  canvasRGBA8.setSize(0, 0);
  canvasRGBA32F.setSize(0, 0);
  canvasGrey8.setSize(0, 0);
  canvasGrey32F.setSize(0, 0);
  mask8.setSize(0, 0);
  mask32F.setSize(0, 0);
  floatsel.active = false;
  floatsel.texture.setSize(0, 0);
  sel.x0 = sel.y0 = sel.x1 = sel.y1 = 0;
}

void Paint::setColorMode(ColorMode mode, bool doupdate)
{
  if(colorMode == mode) return;
  
  ColorMode oldColorMode = colorMode;
  
  size_t u = getU();
  size_t v = getV();
  
  colorMode = mode;
  
  if(oldColorMode == MODE_RGBA_8)
  {
    size_t u2 = canvasRGBA8.getU2();
    unsigned char* buffer = canvasRGBA8.getBuffer();

    if(mode == MODE_RGBA_32F)
    {
      canvasRGBA32F.setSize(u, v);
      float* bufferb = canvasRGBA32F.getBuffer();
      size_t u2b = canvasRGBA32F.getU2();
      for(size_t y = 0; y < v; y++)
      for(size_t x = 0; x < u; x++)
      for(size_t c = 0; c < 4; c++)
      {
        bufferb[y * u2b * 4 + x * 4 + c] = buffer[y * u2 * 4 + x * 4 + c] / 255.0;
      }
    }
    else if(mode == MODE_GREY_8)
    {
      canvasGrey8.setSize(u, v);
      unsigned char* bufferb = canvasGrey8.getBuffer();
      size_t u2b = canvasGrey8.getU2();
      for(size_t y = 0; y < v; y++)
      for(size_t x = 0; x < u; x++)
      {
        size_t i = y * u2 * 4 + x * 4;
        bufferb[y * u2b + x] = ((int)buffer[i+0] + (int)buffer[i+1] + (int)buffer[i+2])/3;
      }
    }
    else if(mode == MODE_GREY_32F)
    {
      canvasGrey32F.setSize(u, v);
      float* bufferb = canvasGrey32F.getBuffer();
      size_t u2b = canvasGrey32F.getU2();
      for(size_t y = 0; y < v; y++)
      for(size_t x = 0; x < u; x++)
      {
        size_t i = y * u2 * 4 + x * 4;
        bufferb[y * u2b + x] = ((int)buffer[i+0] + (int)buffer[i+1] + (int)buffer[i+2])/(255.0*3);
      }
    }
    
    canvasRGBA8.setSize(0,0);
  }
  else if(oldColorMode == MODE_RGBA_32F)
  {
    size_t u2 = canvasRGBA32F.getU2();
    float* buffer = canvasRGBA32F.getBuffer();

    if(mode == MODE_RGBA_8 )
    {
      canvasRGBA8.setSize(u, v);
      unsigned char* bufferb = canvasRGBA8.getBuffer();
      size_t u2b = canvasRGBA8.getU2();
      for(size_t y = 0; y < v; y++)
      for(size_t x = 0; x < u; x++)
      for(size_t c = 0; c < 4; c++)
      {
        int value = lpi::clamp((int)(buffer[y * u2 * 4 + x * 4 + c] * 255.0), 0, 255);
        bufferb[y * u2b * 4 + x * 4 + c] = value;
      }
    }
    else if(mode == MODE_GREY_8)
    {
      canvasGrey8.setSize(u, v);
      unsigned char* bufferb = canvasGrey8.getBuffer();
      size_t u2b = canvasGrey8.getU2();
      for(size_t y = 0; y < v; y++)
      for(size_t x = 0; x < u; x++)
      {
        size_t i = y * u2 * 4 + x * 4;
        bufferb[y * u2b + x] = lpi::clamp((int)(((buffer[i+0]+buffer[i+1]+buffer[i+2]) / 3.0) * 255), 0, 255);
      }
    }
    else if(mode == MODE_GREY_32F)
    {
      canvasGrey32F.setSize(u, v);
      float* bufferb = canvasGrey32F.getBuffer();
      size_t u2b = canvasGrey32F.getU2();
      for(size_t y = 0; y < v; y++)
      for(size_t x = 0; x < u; x++)
      {
        size_t i = y * u2 * 4 + x * 4;
        bufferb[y * u2b + x] = (buffer[i+0]+buffer[i+1]+buffer[i+2]) / 3.0;
      }
    }
    
    canvasRGBA32F.setSize(0,0);
  }
  else if(oldColorMode == MODE_GREY_8)
  {
    size_t u2 = canvasGrey8.getU2();
    unsigned char* buffer = canvasGrey8.getBuffer();

    if(mode == MODE_RGBA_8)
    {
      canvasRGBA8.setSize(u, v);
      unsigned char* bufferb = canvasRGBA8.getBuffer();
      size_t u2b = canvasRGBA8.getU2();
      for(size_t y = 0; y < v; y++)
      for(size_t x = 0; x < u; x++)
      for(size_t c = 0; c < 4; c++)
      {
        if(c == 3) bufferb[y * u2b * 4 + x * 4 + c] = 255;
        else bufferb[y * u2b * 4 + x * 4 + c] = buffer[y * u2 + x];
      }
    }
    else if(mode == MODE_RGBA_32F)
    {
      canvasRGBA32F.setSize(u, v);
      float* bufferb = canvasRGBA32F.getBuffer();
      size_t u2b = canvasRGBA32F.getU2();
      for(size_t y = 0; y < v; y++)
      for(size_t x = 0; x < u; x++)
      for(size_t c = 0; c < 4; c++)
      {
        if(c == 3) bufferb[y * u2b * 4 + x * 4 + c] = 1.0;
        else bufferb[y * u2b * 4 + x * 4 + c] = buffer[y * u2 + x] / 255.0;
      }
    }
    else if(mode == MODE_GREY_32F)
    {
      canvasGrey32F.setSize(u, v);
      float* bufferb = canvasGrey32F.getBuffer();
      size_t u2b = canvasGrey32F.getU2();
      for(size_t y = 0; y < v; y++)
      for(size_t x = 0; x < u; x++)
      {
        size_t i = y * u2 + x;
        bufferb[y * u2b + x] = buffer[i] / 255.0;
      }
    }

    canvasGrey8.setSize(0,0);
  }
  else if(oldColorMode == MODE_GREY_32F)
  {
    size_t u2 = canvasGrey32F.getU2();
    float* buffer = canvasGrey32F.getBuffer();

    if(mode == MODE_RGBA_8)
    {
      canvasRGBA8.setSize(u, v);
      unsigned char* bufferb = canvasRGBA8.getBuffer();
      size_t u2b = canvasRGBA8.getU2();
      for(size_t y = 0; y < v; y++)
      for(size_t x = 0; x < u; x++)
      for(size_t c = 0; c < 4; c++)
      {
        if(c == 3) bufferb[y * u2b * 4 + x * 4 + c] = 255;
        else bufferb[y * u2b * 4 + x * 4 + c] = lpi::clamp((int)(buffer[y * u2 + x] * 255.0), 0, 255);
      }
    }
    else if(mode == MODE_RGBA_32F)
    {
      canvasRGBA32F.setSize(u, v);
      float* bufferb = canvasRGBA32F.getBuffer();
      size_t u2b = canvasRGBA32F.getU2();
      for(size_t y = 0; y < v; y++)
      for(size_t x = 0; x < u; x++)
      for(size_t c = 0; c < 4; c++)
      {
        if(c == 3) bufferb[y * u2b * 4 + x * 4 + c] = 1.0;
        else bufferb[y * u2b * 4 + x * 4 + c] = buffer[y * u2 + x];
      }
    }
    else if(mode == MODE_GREY_8)
    {
      canvasGrey8.setSize(u, v);
      unsigned char* bufferb = canvasGrey8.getBuffer();
      size_t u2b = canvasGrey8.getU2();
      for(size_t y = 0; y < v; y++)
      for(size_t x = 0; x < u; x++)
      {
        size_t i = y * u2 + x;
        bufferb[y * u2b + x] = lpi::clamp((int)(buffer[i] * 255.0), 0, 255);
      }
    }

    canvasGrey32F.setSize(0,0);
  }
  
  if(doupdate) update();
}


void Paint::handleImpl(const lpi::IInput& input)
{
  if(mouseOver(input))
  {
    bool ctrl = input.keyDown(SDLK_LCTRL) || input.keyDown(SDLK_RCTRL);

    if(ctrl)
    {
      double w = 0.5;
      double r = 0.02;

      if(input.keyDown(SDLK_LEFT)) { if(input.keyPressedTime(SDLK_LEFT, w, r)) {move(-1, 0);} }
      if(input.keyDown(SDLK_RIGHT)) { if(input.keyPressedTime(SDLK_RIGHT, w, r)) {move(1, 0);} }
      if(input.keyDown(SDLK_UP)) { if(input.keyPressedTime(SDLK_UP, w, r)) {move(0, -1);} }
      if(input.keyDown(SDLK_DOWN)) { if(input.keyPressedTime(SDLK_DOWN, w, r)) {move(0, 1);} }
    }
    else
    {
      int dx = 0, dy = 0;

      //avoid having two directions at once pressed
      if(input.keyPressedTime(SDLK_LEFT) && !lpi::keyDown(SDLK_UP) && !lpi::keyDown(SDLK_DOWN)) dx--;
      if(input.keyPressedTime(SDLK_RIGHT) && !lpi::keyDown(SDLK_UP) && !lpi::keyDown(SDLK_DOWN)) dx++;
      if(input.keyPressedTime(SDLK_UP)) dy--;
      if(input.keyPressedTime(SDLK_DOWN)) dy++;

      int x, y;
      int ox = input.mouseX(), oy = input.mouseY();

      if(dx != 0 || dy != 0)
      {
        if(zoom_factor >= 1)
        {
          int px, py;
          screenToPixel(px, py, input);

          pixelToScreen(x, y, px + dx, py + dy);

          //fix up annoying calculation errors at some zoom factors
          if(dx == 0) x = ox;
          if(dy == 0) y = oy;
          if(dx != 0 && ox == x) x += dx;
          if(dy != 0 && oy == y) y += dy;
          if(x != ox && y != oy) x = ox;
        }
        else
        {
          x = input.mouseX() + dx;
          y = input.mouseY() + dy;
        }

        if(isInside(x, y)) //avoid moving mouse outside the painting
          input.setMousePos(x, y);
      }
    }
  }
  
  setPattern(options.bgpattern);
  setMaskColorIndex(options.maskColorIndex);
  setPixelGridIndex(options.pixelGridIndex);
}

void Paint::pixelToScreen(int& x, int& y, int px, int py) const
{
  x = x0 + (int)(px * zoom_factor) + (int)(zoom_factor / 2);
  y = y0 + (int)(py * zoom_factor) + (int)(zoom_factor / 2);
}

void Paint::pixelToScreen(int& x, int& y, double px, double py) const
{
  x = x0 + (int)(px * zoom_factor);
  y = y0 + (int)(py * zoom_factor);
}

void Paint::pixelToScreen(int& x0, int& y0, int& x1, int& y1, int px, int py) const
{
  x0 = this->x0 + (int)(px * zoom_factor);
  y0 = this->y0 + (int)(py * zoom_factor);
  x1 = this->x0 + (int)((px + 1) * zoom_factor);
  y1 = this->y0 + (int)((py + 1) * zoom_factor);
}

void Paint::screenToPixel(double& px, double& py, int x, int y) const
{
  px = (x - x0) / zoom_factor;
  py = (y - y0) / zoom_factor;
}

void Paint::screenToPixel(int& px, int& py, const lpi::IInput& input) const
{
  screenToPixel(px, py, input.mouseX(), input.mouseY());
}

void Paint::screenToPixel(int& px, int& py, int x, int y) const
{
  px = (int)((x - x0) / zoom_factor);
  py = (int)((y - y0) / zoom_factor);
}

void Paint::screenToPixel(double& px, double& py, const lpi::IInput& input) const
{
  screenToPixel(px, py, input.mouseX(), input.mouseY());
}

void Paint::screenToNearestCenterOnScreen(int& cx, int& cy, int x, int y) const
{
  screenToPixel(x, y, x, y);
  pixelToScreen(cx, cy, x, y);
}

void Paint::screenToPixelCornersOnScreen(int& x0, int& y0, int& x1, int& y1, int x, int y) const
{
  screenToPixel(x, y, x, y);
  pixelToScreen(x0, y0, x1, y1, x, y);
}

void Paint::drawImpl(lpi::gui::IGUIDrawer& drawer) const
{
  (void)drawer;
  
  bgpattern.drawBackgroundPattern(drawer, x0, y0, x1, y1);
  
  if(colorMode == MODE_RGBA_8)
  {
    int sizex = (int)(canvasRGBA8.getU() * zoom_factor);
    int sizey = (int)(canvasRGBA8.getV() * zoom_factor);
    drawTextureSized(((lpi::gui::GUIDrawerGL&)drawer).getScreen(), &canvasRGBA8, x0, y0, sizex, sizey);
    
    if(mask_enabled)
    {
      drawTextureSized(((lpi::gui::GUIDrawerGL&)drawer).getScreen(), &mask8, x0, y0, sizex, sizey, maskColor);
    }
    
    if(floatsel.active)
    {
      int selsizex = (int)(floatsel.texture.getU() * zoom_factor);
      int selsizey = (int)(floatsel.texture.getV() * zoom_factor);
      if(!floatsel.treat_alpha_as_opacity) bgpattern.drawBackgroundPattern(drawer, x0 + (int)(sel.x0 * zoom_factor), y0 + (int)(sel.y0 * zoom_factor), x0 + (int)(sel.x0 * zoom_factor) + selsizex, y0 + (int)(sel.y0 * zoom_factor) + selsizey);
      drawTextureSized(((lpi::gui::GUIDrawerGL&)drawer).getScreen(), &floatsel.texture, x0 + (int)(sel.x0 * zoom_factor), y0 + (int)(sel.y0 * zoom_factor), selsizex, selsizey);
    }
  }
  else if(colorMode == MODE_RGBA_32F)
  {
    int sizex = (int)(canvasRGBA32F.getU() * zoom_factor);
    int sizey = (int)(canvasRGBA32F.getV() * zoom_factor);
    drawTextureSized(((lpi::gui::GUIDrawerGL&)drawer).getScreen(), &canvasRGBA32F, x0, y0, sizex, sizey);
    
    if(mask_enabled)
    {
      drawTextureSized(((lpi::gui::GUIDrawerGL&)drawer).getScreen(), &mask32F, x0, y0, sizex, sizey, maskColor);
    }
  }
  else if(colorMode == MODE_GREY_8)
  {
    int sizex = (int)(canvasGrey8.getU() * zoom_factor);
    int sizey = (int)(canvasGrey8.getV() * zoom_factor);
    drawTextureSized(((lpi::gui::GUIDrawerGL&)drawer).getScreen(), &canvasGrey8, x0, y0, sizex, sizey);
    
    if(mask_enabled)
    {
      drawTextureSized(((lpi::gui::GUIDrawerGL&)drawer).getScreen(), &mask8, x0, y0, sizex, sizey, maskColor);
    }
  }
  else if(colorMode == MODE_GREY_32F)
  {
    int sizex = (int)(canvasGrey32F.getU() * zoom_factor);
    int sizey = (int)(canvasGrey32F.getV() * zoom_factor);
    drawTextureSized(((lpi::gui::GUIDrawerGL&)drawer).getScreen(), &canvasGrey32F, x0, y0, sizex, sizey);
    
    if(mask_enabled)
    {
      drawTextureSized(((lpi::gui::GUIDrawerGL&)drawer).getScreen(), &mask32F, x0, y0, sizex, sizey, maskColor);
    }
  }
  
  int gridScreenWidth = (int)(zoom_factor * pixelGridWidth);
  int gridScreenHeight = (int)(zoom_factor * pixelGridHeight);
  if(pixelGridWidth > 0 && pixelGridHeight > 0 && gridScreenWidth >= 4)
  {
    int dummy;
    
    int px0, py0, px1, py1;
    screenToPixel(px0, py0, getX0(), getY0());
    screenToPixel(px1, py1, getX1(), getY1());
    
    int px0b = (px0 / pixelGridWidth) * pixelGridWidth;
    int py0b = (py0 / pixelGridHeight) * pixelGridHeight;
    int px1b = (px1 / pixelGridWidth + 1) * pixelGridWidth;
    int py1b = (py1 / pixelGridHeight + 1) * pixelGridHeight;

    if(px0b < 0) px0b = 0;
    if(py0b < 0) py0b = 0;
    if(px1b < 0) px1b = 0;
    if(py1b < 0) py1b = 0;

    int sx0, sy0, sx1, sy1;
    
    pixelToScreen(sx0, sy0, dummy, dummy, px0b, py0b);
    pixelToScreen(sx1, sy1, dummy, dummy, px1b, py1b);

    for(int x = px0b; x < px1b; x += pixelGridWidth)
    {
      int sx;
      pixelToScreen(sx, dummy, dummy, dummy, x, py0b);
      if(gridScreenWidth >= 6) drawer.drawLine(sx - 1, sy0, sx - 1, sy1, lpi::ColorRGB(255,255,255,64));
      drawer.drawLine(sx, sy0, sx, sy1, lpi::ColorRGB(0,0,0,64));
    }

    for(int y = py0b; y < py1b; y += pixelGridHeight)
    {
      int sy;
      pixelToScreen(dummy, sy, dummy, dummy, px0b, y);
      if(gridScreenHeight >= 6) drawer.drawLine(sx0, sy - 1, sx1, sy - 1, lpi::ColorRGB(255,255,255,64));
      drawer.drawLine(sx0, sy, sx1, sy, lpi::ColorRGB(0,0,0,64));
    }
  }
  
  if(sel.x0 != sel.x1 && sel.y0 != sel.y1)
  {
    int rx0 = std::min(sel.x0, sel.x1);
    int ry0 = std::min(sel.y0, sel.y1);
    int rx1 = std::max(sel.x0, sel.x1) - 1;
    int ry1 = std::max(sel.y0, sel.y1) - 1;
    
    int dummy;
    pixelToScreen(rx0, ry0, dummy, dummy, rx0, ry0);
    pixelToScreen(dummy, dummy, rx1, ry1, rx1, ry1);
    
    int x = rx0;
    int STIPPEL = 2;
    bool color = false;
    while(x < rx1)
    {
      drawer.drawLine(x, ry0, x + STIPPEL, ry0, color ? lpi::RGB_Black : lpi::RGB_White);
      drawer.drawLine(x, ry1 - 1, x + STIPPEL, ry1 - 1, color ? lpi::RGB_Black : lpi::RGB_White);
      x += STIPPEL;
      color = !color;
    }
    int y = ry0;
    color = false;
    while(y < ry1)
    {
      drawer.drawLine(rx0, y, rx0, y + STIPPEL, color ? lpi::RGB_Black : lpi::RGB_White);
      drawer.drawLine(rx1 - 1, y, rx1 - 1, y + STIPPEL, color ? lpi::RGB_Black : lpi::RGB_White);
      y += STIPPEL;
      color = !color;
    }
  }
}

void Paint::zoom(int x, int y, bool in)
{
  static const double ZOOMFACTORS[14] = { 1.5, 2.0, 3.0, 4.0, 6.0, 8.0, 12.0, 16.0, 24.0, 32.0, 48.0, 64.0, 96.0, 128.0 };
  int NUMZOOMS = 14;
  
  double oldzoom = zoomfactorindex == 0 ? 1.0 : (zoomfactorindex < 0 ? 1.0 / ZOOMFACTORS[-zoomfactorindex-1] : ZOOMFACTORS[zoomfactorindex-1]);
  
  if(in)
  {
    if(zoomfactorindex >= NUMZOOMS) return;
    else zoomfactorindex++;
  }
  else
  {
    if(zoomfactorindex <= -NUMZOOMS) return;
    else zoomfactorindex--;
  }
  
  double newzoom = zoomfactorindex == 0 ? 1.0 : (zoomfactorindex < 0 ? 1.0 / ZOOMFACTORS[-zoomfactorindex-1] : ZOOMFACTORS[zoomfactorindex-1]);
  
  double factor = newzoom / oldzoom;
  
  zoom_factor *= factor;
  
  x0 += (int)(x - x * factor);
  y0 += (int)(y - y * factor);
  
  updateSize();
}

void Paint::resetZoom()
{
  zoom_factor = 1.0;
  zoomfactorindex = 0;
  updateSize();
}

void Paint::copyFloatSelection()
{
  int x0 = sel.x0; x0 = std::min((int)canvasRGBA8.getU(), std::max(0, x0));
  int y0 = sel.y0; y0 = std::min((int)canvasRGBA8.getV(), std::max(0, y0));
  int x1 = sel.x1; x1 = std::min((int)canvasRGBA8.getU(), std::max(0, x1));
  int y1 = sel.y1; y1 = std::min((int)canvasRGBA8.getV(), std::max(0, y1));
  
  if(x0 == x1 || y0 == y1) return;
  
  unsigned char* buffer = canvasRGBA8.getBuffer();
  unsigned char* buffer2 = floatsel.texture.getBuffer();
  size_t u2 = canvasRGBA8.getU2();
  size_t u2b = floatsel.texture.getU2();
  
  for(int y = y0; y < y1; y++)
  for(int x = x0; x < x1; x++)
  {
    size_t index = 4 * u2 * y + 4 * x;
    size_t index2 = 4 * u2b * (y - sel.y0) + 4 * (x - sel.x0);
    
    if(floatsel.treat_alpha_as_opacity)
    {
      //background
      int r = buffer[index + 0];
      int g = buffer[index + 1];
      int b = buffer[index + 2];
      int a = buffer[index + 3];
      //foreground
      int r2 = buffer2[index2 + 0];
      int g2 = buffer2[index2 + 1];
      int b2 = buffer2[index2 + 2];
      int a2 = buffer2[index2 + 3];
      
      //buffer[index + 0] = (buffer2[index2 + 0] * a2 + buffer[index + 0] * (255 - a2)) / 256;
      //buffer[index + 1] = (buffer2[index2 + 1] * a2 + buffer[index + 1] * (255 - a2)) / 256;
      //buffer[index + 2] = (buffer2[index2 + 2] * a2 + buffer[index + 2] * (255 - a2)) / 256;
      //buffer[index + 3] = a2 > a ? a2 : a; //not really correct but good enough for now.

      //"The" blend formula (for fg and bg with alpha)
      int a3 = 255 - ((255 - a2) * a) / 255;
      if(a2 < a) a3 = std::min(a2, a3); //avoid color of fully transparent foreground leaking through
      buffer[index + 0] = (r2 * a3 + r * (255 - a3)) / 255;
      buffer[index + 1] = (g2 * a3 + g * (255 - a3)) / 255;
      buffer[index + 2] = (b2 * a3 + b * (255 - a3)) / 255;
      buffer[index + 3] = a2 + ((255 - a2) * a) / 255;
    }
    else for(int c = 0; c < 4; c++) buffer[index + c] = buffer2[index2 + c];
  }
  
  canvasRGBA8.updatePartial(x0, y0, x1, y1);
}

void Paint::selectNone()
{
  if(hasFloatingSelection()) unFloatSelection();
  sel.x0 = 0;
  sel.y0 = 0;
  sel.x1 = 0;
  sel.y1 = 0;
}

void Paint::selectAll()
{
  selectNone();
  sel.x0 = 0;
  sel.y0 = 0;
  sel.x1 = getU();
  sel.y1 = getV();
}

void Paint::unFloatSelection()
{
  copyFloatSelection();
  
  floatsel.active = false;
}

void Paint::deleteSelection(const lpi::ColorRGB& color)
{
  if(floatsel.active)
  {
    floatsel.active = false;
  }
  else
  {
    sel.x0 = std::min((int)canvasRGBA8.getU(), std::max(0, sel.x0));
    sel.y0 = std::min((int)canvasRGBA8.getV(), std::max(0, sel.y0));
    sel.x1 = std::min((int)canvasRGBA8.getU(), std::max(0, sel.x1));
    sel.y1 = std::min((int)canvasRGBA8.getV(), std::max(0, sel.y1));
  
    unsigned char* buffer = canvasRGBA8.getBuffer();
    size_t u2 = canvasRGBA8.getU2();
    for(int y = sel.y0; y < sel.y1; y++)
    for(int x = sel.x0; x < sel.x1; x++)
    {
      size_t index = 4 * u2 * y + 4 * x;
      buffer[index + 0] = color.r;
      buffer[index + 1] = color.g;
      buffer[index + 2] = color.b;
      buffer[index + 3] = color.a;
    }
    upload();
  }
  sel.x1 = sel.x0;
  sel.y1 = sel.y0;
}

void Paint::makeFloatingSelection(int w, int h, int x, int y)
{
  sel.x0 = x;
  sel.y0 = y;
  sel.x1 = x + w;
  sel.y1 = y + h;

  floatsel.texture.setSize(sel.x1 - sel.x0, sel.y1 - sel.y0);
  
  floatsel.active = true;
}

void Paint::makeFloatingSelection(const unsigned char* image, int w, int h, int x, int y)
{
  makeFloatingSelection(w, h, x, y);
  
  size_t u2 = floatsel.texture.getU2();

  for(int y = 0; y < h; y++)
  for(int x = 0; x < w; x++)
  for(int c = 0; c < 4; c++)
  {
    size_t index = 4 * w * y + 4 * x + c;
    size_t index2 = 4 * u2 * y + 4 * x + c;
    floatsel.texture.getBuffer()[index2] = image[index];
  }

  canvasRGBA8.updatePartial(sel.x0, sel.y0, sel.x1, sel.y1);
  floatsel.texture.update();
}

/*
makeSelectionFloating:
-creates a floating selection in the selection rectangle
-the texture data is put in a texture in the FloatingSelection struct
-the data where the pixels originally was, is replaced by the color
*/
void Paint::makeSelectionFloating(const lpi::ColorRGB& color, bool copy)
{
  sel.x0 = std::min((int)canvasRGBA8.getU(), std::max(0, sel.x0));
  sel.y0 = std::min((int)canvasRGBA8.getV(), std::max(0, sel.y0));
  sel.x1 = std::min((int)canvasRGBA8.getU(), std::max(0, sel.x1));
  sel.y1 = std::min((int)canvasRGBA8.getV(), std::max(0, sel.y1));
  
  if(sel.x0 == sel.x1 || sel.y0 == sel.y1) return;
  
  floatsel.texture.setSize(sel.x1 - sel.x0, sel.y1 - sel.y0);
  
  unsigned char* buffer = canvasRGBA8.getBuffer();
  size_t u2 = canvasRGBA8.getU2();
  size_t u2b = floatsel.texture.getU2();
  
  for(int y = sel.y0; y < sel.y1; y++)
  for(int x = sel.x0; x < sel.x1; x++)
  {
    size_t index = 4 * u2 * y + 4 * x;
    size_t index2 = 4 * u2b * (y - sel.y0) + 4 * (x - sel.x0);
    for(int c = 0; c < 4; c++)
    {
      floatsel.texture.getBuffer()[index2 + c] = buffer[index + c];
    }
    if(!copy)
    {
      buffer[index + 0] = color.r;
      buffer[index + 1] = color.g;
      buffer[index + 2] = color.b;
      buffer[index + 3] = color.a;
    }
  }
  
  floatsel.active = true;
  
  canvasRGBA8.updatePartial(sel.x0, sel.y0, sel.x1, sel.y1);
  floatsel.texture.update();
}



////////////////////////////////////////////////////////////////////////////////

PaintCorner::PaintCorner()
{
  setSizeXY(8, 8);
}

void PaintCorner::drawImpl(lpi::gui::IGUIDrawer& drawer) const
{
  drawer.drawRectangle(x0, y0, x1, y1, lpi::RGB_White, true);
  drawer.drawRectangle(x0, y0, x1, y1, lpi::RGB_Black, false);
}
void PaintCorner::handleImpl(const lpi::IInput& input)
{
  (void)input;
}



////////////////////////////////////////////////////////////////////////////////

PaintCorners::PaintCorners(Paint& p) : p(p), changed(false), wasgrabbed(false)
{
}

void PaintCorners::drawImpl(lpi::gui::IGUIDrawer& drawer) const
{
  for(size_t i = 0; i < 8; i++) pc[i].draw(drawer);
}

void PaintCorners::handleImpl(const lpi::IInput& input)
{
  bool anygrabbed = false;
  for(size_t i = 0; i < 8; i++)
  {
    int x = left(i) ? p.getX0() - 9 : (right(i) ? p.getX1() + 1 : p.getCenterX() - 4);
    int y = top(i) ? p.getY0() - 9 : (bottom(i) ? p.getY1() + 1 : p.getCenterY() - 4);

    if(pc[i].mouseGrabbed(input))
    {
      anygrabbed = true;
      
      pc[i].moveTo(input.mouseX() - pc[i].mouseGetRelGrabX(), input.mouseY() - pc[i].mouseGetRelGrabY());

      if(left(i)) dx0 = (int)((x - pc[i].getX0()) / p.getZoom());
      else dx0 = 0;
      if(right(i)) dx1 = (int)((pc[i].getX0() - x) / p.getZoom());
      else dx1 = 0;
      if(top(i)) dy0 = (int)((y - pc[i].getY0()) / p.getZoom());
      else dy0 = 0;
      if(bottom(i)) dy1 = (int)((pc[i].getY0() - y) / p.getZoom());
      else dy1 = 0;
    }
    else
    {
      pc[i].moveTo(x, y);
    }
    pc[i].handle(input);
  }

  if(anygrabbed)
  {
    wasgrabbed = true;
  }
  else if(wasgrabbed)
  {
    changed = true;
    wasgrabbed = false;
  }
}

bool PaintCorners::top(int cornerIndex)
{
  return cornerIndex <= 2;
}
bool PaintCorners::right(int cornerIndex)
{
  return cornerIndex >= 2 && cornerIndex <= 4;
}

bool PaintCorners::bottom(int cornerIndex)
{
  return cornerIndex >= 4 && cornerIndex <= 6;
}

bool PaintCorners::left(int cornerIndex)
{
  return cornerIndex >= 6 || cornerIndex == 0;
}

bool PaintCorners::hasChanged()
{
  bool result = changed;
  changed = false;
  return result;
}


////////////////////////////////////////////////////////////////////////////////

PaintWindow::PaintWindow(Options& options, lpi::GLContext* context)
: p(options, context)
, pc(p)
, status(S_NEW)
{
}

void PaintWindow::make(int x, int y, int paintingsizex, int paintingsizey, lpi::gui::IGUIDrawer& geom)
{
  resize(x, y, x + 700, y + 550);
  addTop(geom);
  addTitle("Drawing");
  setColorMod(lpi::RGB_White);

  addCloseButton(geom);
  addMaximizeButton(geom);
  addResizer(geom);
  addStatusBar();
  getStatusBar()->setNumZones(6);
  getStatusBar()->setZoneSize(0, 0.0, 80);
  getStatusBar()->setZoneSize(1, 0.0, 120);
  getStatusBar()->setZoneSize(2, 0.0, 104);
  getStatusBar()->setZoneSize(3, 0.0, 64);
  getStatusBar()->setZoneSize(4, 0.0, 104);

  p.make(paintingsizex, paintingsizey);
  pushTopAt(&p, 20, 20);

  //s.make(0, 0, 660, 460, &p);
  //pushTopAt(&s, 20, 20);

  addScrollbars(geom);
}

void PaintWindow::handleImpl(const lpi::IInput& input)
{
  Window::handleImpl(input);
  
  //bool ctrl = input.keyDown(SDLK_LCTRL) || input.keyDown(SDLK_RCTRL);
  bool alt = input.keyDown(SDLK_LALT) || input.keyDown(SDLK_RALT);
  if(!alt /*because alt+scroll is for dynamic brush change*/)
  {
    if(mouseScrollUp(input))
      p.zoom(p.mouseGetRelPosX(input), p.mouseGetRelPosY(input), true);
    if(mouseScrollDown(input))
      p.zoom(p.mouseGetRelPosX(input), p.mouseGetRelPosY(input), false);
  }
  
  if(p.mouseGrabbed(input, lpi::MMB))
  {
    p.move(-p.mouseGetGrabDiffX(input, lpi::MMB), -p.mouseGetGrabDiffY(input, lpi::MMB));
  }
  
  
  int pixelx, pixely;
  double zoom = p.getZoom();
  p.screenToPixel(pixelx, pixely, input);
  
  /*std::stringstream ss;
  ss << "size:" << p.getU() << "x" << p.getV() << " x:" << pixelx << " y:" << pixely << " zoom:" << zoom << " sel:" << p.sel.x1-p.sel.x0 << "x" << p.sel.y1-p.sel.y0;
  if(p.getColorMode() == MODE_RGBA_8) ss << " [RGBA8, 32 bpp]";
  else if(p.getColorMode() == MODE_RGBA_32F) ss << " [RGBA32F, 128 bpp]";
  else if(p.getColorMode() == MODE_GREY_8) ss << " [Grey8, 8 bpp]";
  else if(p.getColorMode() == MODE_GREY_32F) ss << " [Grey32F, 32 bpp]";
  getStatusBar()->setZoneText(0, ss.str());*/
  
  std::stringstream ss;
  
  ss << p.getU() << "x" << p.getV();
  getStatusBar()->setZoneText(0, ss.str());
  ss.str("");
  
  if(p.getColorMode() == MODE_RGBA_8) ss << "RGBA8, 32 bpp";
  else if(p.getColorMode() == MODE_RGBA_32F) ss << "RGBA32F, 128 bpp";
  else if(p.getColorMode() == MODE_GREY_8) ss << "Grey8, 8 bpp";
  else if(p.getColorMode() == MODE_GREY_32F) ss << "Grey32F, 32 bpp";
  getStatusBar()->setZoneText(1, ss.str());
  ss.str("");

  ss << "x:" << pixelx << " y:" << pixely;
  getStatusBar()->setZoneText(2, ss.str());
  ss.str("");

  int percent = (int)(zoom * 100);
  ss << percent << "%";
  getStatusBar()->setZoneText(3, ss.str());
  ss.str("");

  ss << "sel:" << p.sel.x1-p.sel.x0 << "x" << p.sel.y1-p.sel.y0;
  getStatusBar()->setZoneText(4, ss.str());
  ss.str("");

  int bytepp = 4;
  if(p.getColorMode() == MODE_RGBA_32F) bytepp = 16;
  else if(p.getColorMode() == MODE_GREY_8) bytepp = 1;
  else if(p.getColorMode() == MODE_GREY_32F) bytepp = 4;
  size_t mem = (size_t)p.getU() * p.getV() * bytepp;
  if(mem < (1024*1024*2)) ss << "mem:" << mem / (1024) << "K";
  else ss << "mem:" << mem / (1024*1024) << "." << ((mem % (1024*1024)) / (1024*1024/10)) << "M";
  getStatusBar()->setZoneText(5, ss.str());
  ss.str("");

  updateScroll();
  
  std::string title;
  title = lpi::getFileNameFileExtPart(knownfilename);
  if(status == S_MODIFIED) title += " (*)";
  setTitle(title);

  pc.handle(input);
}


void PaintWindow::drawImpl(lpi::gui::IGUIDrawer& drawer) const
{
  drawWindow(drawer);

  drawer.pushSmallestScissor(scroll.getVisibleX0() + 1, scroll.getVisibleY0(), scroll.getVisibleX1() - 1, scroll.getVisibleY1() - 1);
  drawer.drawRectangle(p.getX0() - 1, p.getY0() - 1, p.getX1() + 1, p.getY1() + 1, lpi::RGB_Black, false);
  drawElements(drawer);
  pc.draw(drawer);
  drawer.popScissor();
}

void PaintWindow::setFilename(const std::string& filename)
{
  //setTitle(filename);
  knownfilename = filename;
}

std::string PaintWindow::getFilename() const
{
  return knownfilename;
}

bool PaintWindow::hasFilename()
{
  return !knownfilename.empty();
}

void PaintWindow::centerPaint()
{
  p.moveCenterTo(getCenterX(), getCenterY());
}

void PaintWindow::zoom(bool in)
{
  p.zoom(p.getSizeX() / 2, p.getSizeY() / 2, in);
}

void PaintWindow::zoom(bool in, const lpi::IInput& input)
{
  /*if(mouseOver(input))*/ p.zoom(p.mouseGetRelPosX(input), p.mouseGetRelPosY(input), in);
  /*else zoom(in);*/
}

Paint& PaintWindow::getPaint()
{
  return p;
}

const Paint& PaintWindow::getPaint() const
{
  return p;
}

void PaintWindow::resizeImpl(const lpi::gui::Pos<int>& newPos)
{
  ElementComposite::resizeImpl(newPos);
  
  int dx = 0, dy = 0;
  
  if(newPos.x0 == x0 && newPos.y0 == y0)
  {
    // This only works correctly for resizing with the bottom right corner, which
    // is the only resizing currently supported.
    if(newPos.x1 < x1 && p.getX0() > x0 + 4 && p.getX1() > newPos.x1 - 4)
    {
      dx = (newPos.x1 - x1);
      if(p.getX0() + dx < x0 + 4) dx = x0 + 4 - p.getX0();
    }
    // The 20 for y is due to top bar and status bar
    if(newPos.y1 < y1 && p.getY0() > y0 + 20 && p.getY1() > newPos.y1 - 20)
    {
      dy = (newPos.y1 - y1);
      if(p.getY0() + dy < y0 + 20) dy = y0 + 20 - p.getY0();
    }
    if(newPos.x1 > x1 && p.getX0() < x0 + 4)
    {
      dx = (newPos.x1 - x1);
      if(p.getX0() + dx > x0 + 4) dx = x0 + 4 - p.getX0();
    }
    if(newPos.y1 > y1 && p.getY0() < y0 + 20)
    {
      dy = (newPos.y1 - y1);
      if(p.getY0() + dy > y0 + 20) dy = y0 + 20 - p.getY0();
    }
  }
  else
  {
    // Fallback for if it's not moving from bottom right corner.
    int cxo = (x0 + x1) / 2;
    int cyo = (y0 + y1) / 2;
    int cxn = (newPos.x0 + newPos.x1) / 2;
    int cyn = (newPos.y0 + newPos.y1) / 2;
    dx = cxn - cxo;
    dy = cyn - cyo;
  }
  
  p.move(dx, dy);
}

void PaintWindow::fitAroundPainting(int ax0, int ay0, int ax1, int ay1)
{
  int newx1 = x1;
  int newy1 = y1;
  if(p.getSizeX() + 8 < (ax1 - ax0)) {
    newx1 = x0 + p.getSizeX() + 8;
  }
  
  if(p.getSizeY() + 8 + 32 < (ay1 - ay0)) {
    newy1 = y0 + p.getSizeY() + 8 + 32;
  }
  
  if(newx1 != x1 || newy1 != y1)
  {
    resize(x0, y0, newx1, newy1);
  }
  
  if(ax0 > x0) moveTo(ax0, y0);
  if(ay0 > y0) moveTo(x0, ay0);
  
  updateScroll();
}

int PaintWindow::getVisibleX0() const
{
  return scroll.getVisibleX0();
}

int PaintWindow::getVisibleY0() const
{
  return scroll.getVisibleY0();
}

int PaintWindow::getVisibleX1() const
{
  return scroll.getVisibleX1();
}

int PaintWindow::getVisibleY1() const
{
  return scroll.getVisibleY1();
}
