/*
LodePaint

Copyright (c) 2009-2010 Lode Vandevenne
All rights reserved.

This file is part of LodePaint.

LodePaint is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LodePaint is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LodePaint.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "lpi/gio/lpi_draw2d_buffer.h"
#include "lpi/gio/lpi_input.h"
#include "lpi/gio/lpi_text_drawer_int.h"
#include "lpi/gui/lpi_gui.h"

#include "paint_gui_operation.h"
#include "paint_operation.h"
#include "paint_tool.h"
#include "paint_window.h"

class Tool_Pan : public ITool //tool that does nothing (on purpose)
{
  private:
    UndoState tempundo; //for swapping in getUndoState
    bool justdone;
    
  public:
  
    Tool_Pan(GlobalToolSettings& settings) : ITool(settings) {}
  
    virtual void handle(const lpi::IInput& input, Paint& paint);

    virtual std::string getLabel() const { return "Pan"; }
    virtual lpi::HTexture* getIcon() const { return &textures_tools[16]; }
};

class Tool_FloodFill : public ITool
{
  private:
    UndoState tempundo; //for swapping in getUndoState
    bool justdone;
    DynamicPageWithAutoUpdate dialog;
    
    double tolerance; //for floodfill and color replacer
    bool tolerance_ignores_alpha; //alpha not used in tolerance calculations
    
  public:
  
    Tool_FloodFill(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom);
  
    virtual void handle(const lpi::IInput& input, Paint& paint);

    virtual bool done();
    virtual void getUndoState(UndoState& state);

    virtual void undo(Paint& paint, UndoState& state);
    virtual void redo(Paint& paint, UndoState& state);
    
    virtual std::string getLabel() const { return "Flood Fill"; }
    virtual lpi::HTexture* getIcon() const { return &textures_tools[64]; }
    
    virtual lpi::gui::Element* getDialog() { return &dialog; }
    
    virtual void draw(lpi::gui::IGUIDrawer& drawer, Paint& paint);
};

class Tool_ColorPicker : public ITool
{
  private:
    ColorWindow& colorWindow;
  
  public:
    Tool_ColorPicker(GlobalToolSettings& settings, ColorWindow& colorWindow);
    
    virtual void handle(const lpi::IInput& input, Paint& paint);
    
    virtual std::string getLabel() const { return "Color Picker"; }
    virtual lpi::HTexture* getIcon() const { return &textures_tools[20]; }
    
    virtual void draw(lpi::gui::IGUIDrawer& drawer, Paint& paint);
};

class Tool_SelRect : public ITool //select rectangles and drag them
{
  private:
    bool wasup;
    bool wasdown;
    int grabx; //in pixels compared to main image
    int graby;
    bool grabbed;
    UndoState tempundo;
    bool justdone; //for undo
    
    DynamicPageWithAutoUpdate dialog;
    
    enum UndoSubOp //sub-operation of selection
    {
      SELOP_CHANGE_COOR_WHILE_NOT_FLOATING, //only used if there wasn't a floating selection already, used if sel coordinates change (e.g. dragged selection box for first time, changed selection while it wasn't floating, or unselected selection while it wasn't floating)
      //SELOP_MOVE,
      SELOP_FLOAT,
      SELOP_UNFLOAT,
      SELOP_COPY,
      SELOP_FLOAT_AND_COPY
    };
    
    void addSelInfoToUndo(UndoState& state, const Paint& paint);
    lpi::ColorRGB getAutoBGColor(const Paint& paint) const;
  
  public:
  
    Tool_SelRect(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom);
  
    virtual void handle(const lpi::IInput& input, Paint& paint);
    
    virtual std::string getLabel() const { return "Select Rectangle"; }
    virtual lpi::HTexture* getIcon() const { return &textures_tools[112]; }

    virtual void undo(Paint& paint, UndoState& state);
    virtual void redo(Paint& paint, UndoState& state);
    virtual bool done();
    virtual void getUndoState(UndoState& state);
    
    virtual lpi::gui::Element* getDialog() { return &dialog; }
    
    virtual void draw(lpi::gui::IGUIDrawer& drawer, Paint& paint);

};

class Tool_MagicWand : public ITool
{
  private:
    UndoState tempundo; //for swapping in getUndoState
    bool justdone;
    DynamicPageWithAutoUpdate dialog;

    double tolerance; //for floodfill and color replacer
    bool tolerance_ignores_alpha; //alpha not used in tolerance calculations
    
    int alpha_left;
    int alpha_right;
    
  protected:

    virtual bool editMask() { return true; }

  public:

    Tool_MagicWand(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom);

    virtual void handle(const lpi::IInput& input, Paint& paint);

    virtual bool done();
    virtual void getUndoState(UndoState& state);

    virtual void undo(Paint& paint, UndoState& state);
    virtual void redo(Paint& paint, UndoState& state);

    virtual std::string getLabel() const { return "Magic Wand (On Mask)"; }
    virtual lpi::HTexture* getIcon() const { return &textures_tools[117]; }

    virtual lpi::gui::Element* getDialog() { return &dialog; }
    
    virtual void draw(lpi::gui::IGUIDrawer& drawer, Paint& paint);
};

class DrawerForTextTool : public lpi::ADrawer2DBuffer
{
  protected:
  
    size_t u;
    size_t v;
  
  public:
    DrawerForTextTool(){}
    DrawerForTextTool(TextureRGBA8* texture){setTexture(texture);}

    
    virtual size_t getWidth() { return u; }
    virtual size_t getHeight() { return v; }
    
    void setTexture(TextureRGBA8* texture)
    {
      setBufferInternal(texture->getBuffer(), texture->getU2(), texture->getV2());
      w = texture->getU2();
      h = texture->getV2();
    }
};

class Tool_Text : public ITool
{
  private:
    UndoState tempundo; //for swapping in getUndoState
    bool justdone;
    DynamicPageWithAutoUpdate dialog;

    std::string text;
    
    double opacity;
    
    DrawerForTextTool drawer;
    lpi::InternalTextDrawer textDrawer;
    
    enum Font
    {
      PX8,
      PX7,
      PX6,
      PX4
    };
    
    Font font;

  public:

    Tool_Text(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom);

    virtual void handle(const lpi::IInput& input, Paint& paint);

    virtual bool done();
    virtual void getUndoState(UndoState& state);

    virtual void undo(Paint& paint, UndoState& state);
    virtual void redo(Paint& paint, UndoState& state);

    virtual std::string getLabel() const { return "Text"; }
    virtual lpi::HTexture* getIcon() const { return &textures_tools[96]; }

    virtual lpi::gui::Element* getDialog() { return &dialog; }
    
    virtual void draw(lpi::gui::IGUIDrawer& drawer, Paint& paint);
};


