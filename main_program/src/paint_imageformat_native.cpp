/*
LodePaint

Copyright (c) 2009-2010 Lode Vandevenne
All rights reserved.

This file is part of LodePaint.

LodePaint is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LodePaint is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LodePaint.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "paint_imageformat_native.h"

#include "lpi/lodepng.h"

////////////////////////////////////////////////////////////////////////////////

bool ImageFormatNative::canDecode(const unsigned char* file, size_t filesize) const
{
  return isFileOfFormat(file, filesize);
}

bool ImageFormatNative::canEncode(const Paint& paint) const
{
  (void)paint;
  return true;
}

bool ImageFormatNative::canDecode() const
{
  return true;
}

bool ImageFormatNative::canEncode() const
{
  return true;
}

static void pushUINT32(std::vector<unsigned char>& file, unsigned i)
{
  file.push_back((unsigned char)((i >> 24) & 0xff));
  file.push_back((unsigned char)((i >> 16) & 0xff));
  file.push_back((unsigned char)((i >>  8) & 0xff));
  file.push_back((unsigned char)((i      ) & 0xff));
}

static unsigned readUINT32(const unsigned char* buffer, size_t& pos)
{
  unsigned result = (buffer[pos + 0] << 24) | (buffer[pos + 1] << 16) | (buffer[pos + 2] << 8) | buffer[pos + 3];
  pos += 4;
  return result;
}

static bool pngToData(std::string& error, std::vector<unsigned char>& data, size_t& pngW, size_t& pngH, const unsigned char* file, size_t& pos, size_t pngSize)
{
  unsigned w, h;
  unsigned pngerror = lodepng::decode(data, w, h, &file[pos], pngSize); //decode the image from PNG
  pos += pngSize;
  if(pngerror)
  {
    std::stringstream ss;
    ss << "Error decoding image because: \nLodePNG error number " << pngerror;
    error = ss.str();
    return false;
  }

  pngW = w;
  pngH = h;
  
  return true;
}

/*
w and h are the expected width and height the texture should get. The PNG's size
may differ depending on the type of data.
*/
static bool pngToTexture(std::string& error, TextureRGBA8& texture, const unsigned char* file, size_t& pos, size_t pngSize, size_t w, size_t h)
{
  std::vector<unsigned char> data;
  size_t pngW, pngH;
  if(!pngToData(error, data, pngW, pngH, file, pos, pngSize)) return false;
  if(pngH != h || pngW != w) { error = "PNG size doesn't match image size"; return false; }

  texture.setSize(w, h);
  unsigned char* buffer = texture.getBuffer();
  size_t u2 = texture.getU2();

  for(int y = 0; y < (int)h; y++)
  for(int x = 0; x < (int)w; x++)
  for(int c = 0; c < 4; c++)
  {
    buffer[y * u2 * 4 + x * 4 + c] = data[y * w * 4 + x * 4 + c];
  }
  
  return true;
}

/*
w and h are the expected width and height the texture should get. The PNG's size
may differ depending on the type of data.
*/
static bool pngToTexture(std::string& error, TextureRGBA32F& texture, const unsigned char* file, size_t& pos, size_t pngSize, size_t w, size_t h)
{
  std::vector<unsigned char> data;
  size_t pngW, pngH;
  if(!pngToData(error, data, pngW, pngH, file, pos, pngSize)) return false;
  if(pngH != h || pngW != w * 4) { error = "PNG size doesn't match image size"; return false; }

  texture.setSize(w, h);
  float* buffer = texture.getBuffer();
  size_t u2 = texture.getU2();

  float* fdata = reinterpret_cast<float*>(&data[0]);
  for(int y = 0; y < (int)h; y++)
  for(int x = 0; x < (int)w; x++)
  for(int c = 0; c < 4; c++)
  {
    buffer[y * u2 * 4 + x * 4 + c] = fdata[y * w * 4 + x * 4 + c];
  }

  return true;
}

/*
w and h are the expected width and height the texture should get. The PNG's size
may differ depending on the type of data.
*/
static bool pngToTexture(std::string& error, TextureGrey8& texture, const unsigned char* file, size_t& pos, size_t pngSize, size_t w, size_t h)
{
  std::vector<unsigned char> data;
  size_t pngW, pngH;
  if(!pngToData(error, data, pngW, pngH, file, pos, pngSize)) return false;
  if(pngH != h || pngW * 4!= w) { error = "PNG size doesn't match image size"; return false; }

  texture.setSize(w, h);
  unsigned char* buffer = texture.getBuffer();
  size_t u2 = texture.getU2();

  for(int y = 0; y < (int)h; y++)
  for(int x = 0; x < (int)w; x++)
  {
    buffer[y * u2 + x] = data[y * w + x];
  }

  return true;
}

/*
w and h are the expected width and height the texture should get. The PNG's size
may differ depending on the type of data.
*/
static bool pngToTexture(std::string& error, TextureGrey32F& texture, const unsigned char* file, size_t& pos, size_t pngSize, size_t w, size_t h)
{
  std::vector<unsigned char> data;
  size_t pngW, pngH;
  if(!pngToData(error, data, pngW, pngH, file, pos, pngSize)) return false;
  if(pngH != h || pngW != w) { error = "PNG size doesn't match image size"; return false; }

  texture.setSize(w, h);
  float* buffer = texture.getBuffer();
  size_t u2 = texture.getU2();

  float* fdata = reinterpret_cast<float*>(&data[0]);
  for(int y = 0; y < (int)h; y++)
  for(int x = 0; x < (int)w; x++)
  {
    buffer[y * u2 + x] = fdata[y * w + x];
  }

  return true;
}

bool ImageFormatNative::decode(std::string& error, Paint& paint, const unsigned char* file, size_t filesize) const
{
  //magic bytes
  if(filesize == 0) { error = "file is empty or doesn't exist"; return false; }
  if(!isFileOfFormat(file, filesize)) { error = "invalid magic bytes"; return false; }

  size_t pos = 8;
  
  pos += 12; //skip chunksize and "head"

  //version
  if(pos + 4 > filesize) { error = "file corrupt"; return false; }
  unsigned version = readUINT32(file, pos);
  if(version != 0) { error = "unknown version"; return false; }

  //width & height
  if(pos + 8 > filesize) { error = "file corrupt"; return false; }
  size_t w = readUINT32(file, pos);
  size_t h = readUINT32(file, pos);
  
  pos += 32; //skip 32 dummy bytes
  
  pos += 12; //skip chunksize and "data"
  
  //type (layer, mask, selection)
  if(pos + 4 > filesize) { error = "file corrupt"; return false; }
  if(readUINT32(file, pos) != 0) { error = "wrong type"; return false; }

  //width and height
  if(pos + 8 > filesize) { error = "file corrupt"; return false; }
  if(readUINT32(file, pos) != w) { error = "wrong image size"; return false; }
  if(readUINT32(file, pos) != h) { error = "wrong image size"; return false; }
  
  pos += 8; //skip x and y offset
  
  if(pos + 12 > filesize) { error = "file corrupt"; return false; }
  int numchan = readUINT32(file, pos);
  int bitsperchan = readUINT32(file, pos);
  int floating = readUINT32(file, pos);
  
  pos += 32; //skip 32 dummy bytes
  
  if(pos + 12 > filesize) { error = "file corrupt"; return false; }
  int pngColorType = readUINT32(file, pos);
  int pngColorDepth = readUINT32(file, pos);
  int pngSize = readUINT32(file, pos);
  if(pngColorType != 6 || pngColorDepth != 8) { error = "unsupported PNG raw data type"; return false; }

  if(pos + pngSize > filesize) { error = "file corrupt"; return false; }

  paint.clear();
  
  if(numchan == 4 && bitsperchan == 8 && floating == 0) //RGBA8
  {
    paint.setColorMode(MODE_RGBA_8);
    if(!pngToTexture(error, paint.canvasRGBA8, file, pos, pngSize, w, h)) return false;
  }
  else if(numchan == 4 && bitsperchan == 32 && floating == 1) //RGBA32F
  {
    paint.setColorMode(MODE_RGBA_32F);
    if(!pngToTexture(error, paint.canvasRGBA32F, file, pos, pngSize, w, h)) return false;
  }
  else if(numchan == 1 && bitsperchan == 8 && floating == 0) //GREY8
  {
    paint.setColorMode(MODE_GREY_8);
    if(!pngToTexture(error, paint.canvasGrey8, file, pos, pngSize, w, h)) return false;
  }
  else if(numchan == 1 && bitsperchan == 32 && floating == 1) //GREY32F
  {
    paint.setColorMode(MODE_GREY_32F);
    if(!pngToTexture(error, paint.canvasGrey32F, file, pos, pngSize, w, h)) return false;
  }
  else
  {
    error = "unsupported color mode";
    return false;
  }
  
  paint.update();
  return true;
}

/*
png_width is the "fake" width, the width the PNG data has, not necessarily the width of the original image
*/
static bool dataToPng(std::string& error, std::vector<unsigned char>& png, const std::vector<unsigned char>& data, size_t png_width, size_t png_height)
{
  /*
  Here, the image data is encoded an an RGBA PNG, completely ignoring the original color
  format of the image. Rationale behind this: PNG doesn't support 32-bit floating point channels.
  We just treat the image data as generic bytes. Encoding as PNG gives a slightly better compression
  ratio than simply using zlib, because there still is image-like regularity in the data.
  Otherwise I'd have just used zlib here.
  */

  unsigned pngerror = lodepng::encode(png, data.empty() ? 0 : &data[0], png_width, png_height);
  if(pngerror)
  {
    std::stringstream ss;
    ss << "Error saving image: LodePNG error " << pngerror;
    error = ss.str();
    return false;
  }
  
  return true;
}

static bool textureToPng(std::string& error, std::vector<unsigned char>& png, const TextureRGBA8& texture)
{
  size_t h = texture.getV();
  std::vector<unsigned char> data;
  getAlignedBuffer(data, &texture);
  
  size_t png_width = (data.size() / (h * 4)); //for encoding as RGBA PNG
  size_t png_height = h;
  
  return dataToPng(error, png, data, png_width, png_height);
}

static bool textureToPng(std::string& error, std::vector<unsigned char>& png, const TextureRGBA32F& texture)
{
  size_t h = texture.getV();
  std::vector<unsigned char> data;
  const float* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();
  data.resize(u * v * 16);
  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  {
    const unsigned char* c = reinterpret_cast<const unsigned char*>(&buffer[4 * u2 * y + 4 * x]);
    for(size_t i = 0; i < 16; i++) data[16 * u * y + 16 * x + i] = c[i];
  }

  size_t png_width = (data.size() / (h * 4)); //for encoding as RGBA PNG
  size_t png_height = h;

  return dataToPng(error, png, data, png_width, png_height);
}

static bool textureToPng(std::string& error, std::vector<unsigned char>& png, const TextureGrey8& texture)
{
  size_t h = texture.getV();
  std::vector<unsigned char> data;
  getAlignedBuffer(data, &texture);

  size_t png_width = (data.size() / (h * 4)); //for encoding as RGBA PNG
  size_t png_height = h;

  return dataToPng(error, png, data, png_width, png_height);
}

static bool textureToPng(std::string& error, std::vector<unsigned char>& png, const TextureGrey32F& texture)
{
  size_t h = texture.getV();
  std::vector<unsigned char> data;
  const float* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();
  data.resize(u * v * 4);
  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  {
    const unsigned char* c = reinterpret_cast<const unsigned char*>(&buffer[u2 * y + x]);
    for(size_t i = 0; i < 4; i++) data[4 * u * y + 4 * x + i] = c[i];
  }

  size_t png_width = (data.size() / (h * 4)); //for encoding as RGBA PNG
  size_t png_height = h;

  return dataToPng(error, png, data, png_width, png_height);
}



bool ImageFormatNative::encode(std::string& error, std::vector<unsigned char>& file, const Paint& paint) const
{
  /*
  The LodePaint Image format is planned to be a format with chunks, where a varying amount of chunks can exist
  to contain various parts of a LodePaint image. However currently only the "head" and "data" chunk are used.
  */
  
  size_t w = paint.getU();
  size_t h = paint.getV();
  
  //magic bytes [8]
  file.push_back(0x89);
  file.push_back('L');
  file.push_back('P');
  file.push_back('I');
  file.push_back(0x0D);
  file.push_back(0x0A);
  file.push_back(0x1A);
  file.push_back(0x0A);
  
  //"head" chunk
  pushUINT32(file, 0); pushUINT32(file, 44);
  file.push_back('h');
  file.push_back('e');
  file.push_back('a');
  file.push_back('d');

  //version
  pushUINT32(file, 0);

  //main width & height
  pushUINT32(file, w);
  pushUINT32(file, h);

  //dummy extra header values for possible future usage
  for(int i = 0; i < 32; i++) file.push_back(0);
  
  //"data" chunk
  
  std::vector<unsigned char> png; //we have to encode to separate vector first, to know the size

  if(paint.getColorMode() == MODE_RGBA_8)
  {
    if(!textureToPng(error, png, paint.canvasRGBA8)) return false;
  }
  else if(paint.getColorMode() == MODE_RGBA_32F)
  {
    if(!textureToPng(error, png, paint.canvasRGBA32F)) return false;
  }
  else if(paint.getColorMode() == MODE_GREY_8)
  {
    if(!textureToPng(error, png, paint.canvasGrey8)) return false;
  }
  else if(paint.getColorMode() == MODE_GREY_32F)
  {
    if(!textureToPng(error, png, paint.canvasGrey32F)) return false;
  }
  
  //chunk size
  pushUINT32(file, 0); pushUINT32(file, png.size() + 72);
  file.push_back('d');
  file.push_back('a');
  file.push_back('t');
  file.push_back('a');
  
  //type: 0 for (background) layer, 1 for mask, 2 for floating selection
  pushUINT32(file, 0);
  
  //width & height
  pushUINT32(file, w);
  pushUINT32(file, h);

  //position offset
  pushUINT32(file, 0);
  pushUINT32(file, 0);

  //number of channels, bits per channel and channel number type
  if(paint.getColorMode() == MODE_RGBA_8)
  {
    pushUINT32(file, 4);
    pushUINT32(file, 8);
    pushUINT32(file, 0);
  }
  else if(paint.getColorMode() == MODE_RGBA_32F)
  {
    pushUINT32(file, 4);
    pushUINT32(file, 32);
    pushUINT32(file, 1);
  }
  else if(paint.getColorMode() == MODE_GREY_8)
  {
    pushUINT32(file, 1);
    pushUINT32(file, 8);
    pushUINT32(file, 0);
  }
  else if(paint.getColorMode() == MODE_GREY_32F)
  {
    pushUINT32(file, 1);
    pushUINT32(file, 32);
    pushUINT32(file, 1);
  }
  
  //dummy extra header values for possible future usage
  for(int i = 0; i < 32; i++) file.push_back(0);
  
  //PNG color type and bits per channel, of the RAW DATA, not the encoded PNG data (that is allowed to be anything, as long as when decoding the raw data is converted back to this format)
  pushUINT32(file, 6);
  pushUINT32(file, 8);
  
  //PNG size
  pushUINT32(file, png.size());
  
  //PNG
  file.insert(file.end(), png.begin(), png.end());
  
  return true;
}

bool ImageFormatNative::isFileOfFormat(const unsigned char* file, size_t filesize) const
{
  return filesize > 8 && file[0] == 0x89
                      && file[1] == 'L' && file[2] == 'P' && file[3] == 'I'
                      && file[4] == 0x0D && file[5] == 0x0A && file[6] == 0x1A && file[7] == 0X0A;
}

size_t ImageFormatNative::getNumExtensions() const
{
  return 1;
}

std::string ImageFormatNative::getExtension(size_t i) const
{
  (void)i;
  return "lpi";
}

std::string ImageFormatNative::getDescription() const
{
  return "LodePaint Image";
}

////////////////////////////////////////////////////////////////////////////////
