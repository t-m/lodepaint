/*
LodePaint

Copyright (c) 2009-2010 Lode Vandevenne
All rights reserved.

This file is part of LodePaint.

LodePaint is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LodePaint is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LodePaint.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "paint_tool.h"

#include "lpi/gui/lpi_gui_color.h"
#include "lpi/lpi_math.h"
#include "lpi/lpi_translate.h"

#include "paint_algos.h"
#include "paint_gui.h"

#include <iostream>

////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////

ITool::ITool(GlobalToolSettings& settings)
: IOperation(settings)
{
}

void ITool::drawPenPixel(lpi::gui::IGUIDrawer& drawer, Paint& paint)
{
  if(paint.getZoom() >= 3.0 && paint.mouseOver(drawer.getInput()))
  {
    int x, y;
    paint.screenToPixel(x, y, drawer.getInput());
    
    int x0, y0, x1, y1;
    
    paint.pixelToScreen(x0, y0, x1, y1, x, y);
    
    drawer.drawRectangle(x0, y0, x1, y1, lpi::RGB_Black, false);
    drawer.drawRectangle(x0 - 1, y0 - 1, x1 + 1, y1 + 1, lpi::RGB_White, false);

  }
}

void ITool::drawSpot(lpi::gui::IGUIDrawer& drawer, Paint& paint, int x, int y)
{
  int x0, y0, x1, y1;
  paint.pixelToScreen(x0, y0, x1, y1, x, y);
  x = (x0 + x1) / 2;
  y = (y0 + y1) / 2;
  
  drawer.drawEllipse(x-2, y-2, x+2, y+2, lpi::RGB_Black, false);
  drawer.drawEllipse(x-3, y-3, x+3, y+3, lpi::RGB_White, false);
}

void ITool::drawCommonCrosshair(lpi::gui::IGUIDrawer& drawer, Paint& paint, int position_x, int position_y)
{
  int x = drawer.getInput().mouseX();
  int y = drawer.getInput().mouseY();
  
  int xc, yc;
  paint.screenToNearestCenterOnScreen(xc, yc, x, y);
  int x0, y0, x1, y1;
  paint.screenToPixelCornersOnScreen(x0, y0, x1, y1, x, y);
    
  if(position_x == -1) x = x0;
  if(position_x == 0) x = xc;
  if(position_x == 1) x = x1;
  if(position_y == -1) y = y0;
  if(position_y == 0) y = yc;
  if(position_y == 1) y = y1;
  
  int px0 = paint.getX0();
  int py0 = paint.getY0();
  int px1 = paint.getX1();
  int py1 = paint.getY1();
  
  if(paint.mouseOver(drawer.getInput()))
  {

    if(settings.crosshairMode == 1)
    {
      static const int C = 12;

      drawer.drawLine(x - C, y + 1, x + C, y + 1, lpi::ColorRGB(255, 255, 255, 255));
      drawer.drawLine(x + 1, y - C, x + 1, y + C, lpi::ColorRGB(255, 255, 255, 255));
      drawer.drawLine(x - C, y - 1, x + C, y - 1, lpi::ColorRGB(255, 255, 255, 255));
      drawer.drawLine(x - 1, y - C, x - 1, y + C, lpi::ColorRGB(255, 255, 255, 255));
      //drawer.drawLine(x - C - 1, y, x + C + 1, y, lpi::ColorRGB(255, 255, 255, 255));
      //drawer.drawLine(x, y - C - 1, x, y + C + 1, lpi::ColorRGB(255, 255, 255, 255));
      drawer.drawLine(x - C - 1, y - 1, x - C - 1, y + 2, lpi::ColorRGB(255, 255, 255, 255));
      drawer.drawLine(x + C, y - 1, x + C, y + 2, lpi::ColorRGB(255, 255, 255, 255));
      drawer.drawLine(x - 1, y - C - 1, x + 2, y - C - 1, lpi::ColorRGB(255, 255, 255, 255));
      drawer.drawLine(x - 1, y + C, x + 2, y + C, lpi::ColorRGB(255, 255, 255, 255));

      drawer.drawLine(x - C, y, x + C, y, lpi::ColorRGB(0, 0, 0, 255));
      drawer.drawLine(x, y - C, x, y + C, lpi::ColorRGB(0, 0, 0, 255));
    }
    else if(settings.crosshairMode == 2)
    {
      drawer.drawLine(px0, y, px1, y, lpi::ColorRGB(128, 128, 128, 192));
      drawer.drawLine(x, py0, x, py1, lpi::ColorRGB(128, 128, 128, 192));
    }
    else if(settings.crosshairMode == 3)
    {
      drawer.drawLine(px0, y + 1, px1, y + 1, lpi::ColorRGB(240, 240, 240, 255));
      drawer.drawLine(x + 1, py0, x + 1, py1, lpi::ColorRGB(240, 240, 240, 255));
      drawer.drawLine(px0, y - 1, px1, y - 1, lpi::ColorRGB(240, 240, 240, 255));
      drawer.drawLine(x - 1, py0, x - 1, py1, lpi::ColorRGB(240, 240, 240, 255));
      drawer.drawLine(px0, y, px1, y, lpi::ColorRGB(0, 0, 0, 255));
      drawer.drawLine(x, py0, x, py1, lpi::ColorRGB(0, 0, 0, 255));
    }
  }
  
}

void ITool::drawSmallCrosshair(lpi::gui::IGUIDrawer& drawer, Paint& paint, int position_x, int position_y, int size)
{
  int x = drawer.getInput().mouseX();
  int y = drawer.getInput().mouseY();
  
  int xc, yc;
  paint.screenToNearestCenterOnScreen(xc, yc, x, y);
  int x0, y0, x1, y1;
  paint.screenToPixelCornersOnScreen(x0, y0, x1, y1, x, y);
    
  if(position_x == -1) x = x0;
  if(position_x == 0) x = xc;
  if(position_x == 1) x = x1;
  if(position_y == -1) y = y0;
  if(position_y == 0) y = yc;
  if(position_y == 1) y = y1;
  
  int px0 = paint.getX0();
  int py0 = paint.getY0();
  int px1 = paint.getX1();
  int py1 = paint.getY1();
  
  if(y >= py0 && y < py1 && x >= px0 && x < px1)
  {
    int C = size;
    
    drawer.drawLine(x - C, y + 1, x + C, y + 1, lpi::ColorRGB(255, 255, 255, 255));
    drawer.drawLine(x + 1, y - C, x + 1, y + C, lpi::ColorRGB(255, 255, 255, 255));
    drawer.drawLine(x - C, y - 1, x + C, y - 1, lpi::ColorRGB(255, 255, 255, 255));
    drawer.drawLine(x - 1, y - C, x - 1, y + C, lpi::ColorRGB(255, 255, 255, 255));
    //drawer.drawLine(x - C - 1, y, x + C + 1, y, lpi::ColorRGB(255, 255, 255, 255));
    //drawer.drawLine(x, y - C - 1, x, y + C + 1, lpi::ColorRGB(255, 255, 255, 255));
    drawer.drawLine(x - C - 1, y - 1, x - C - 1, y + 2, lpi::ColorRGB(255, 255, 255, 255));
    drawer.drawLine(x + C, y - 1, x + C, y + 2, lpi::ColorRGB(255, 255, 255, 255));
    drawer.drawLine(x - 1, y - C - 1, x + 2, y - C - 1, lpi::ColorRGB(255, 255, 255, 255));
    drawer.drawLine(x - 1, y + C, x + 2, y + C, lpi::ColorRGB(255, 255, 255, 255));
    
    drawer.drawLine(x - C, y, x + C, y, lpi::ColorRGB(0, 0, 0, 255));
    drawer.drawLine(x, y - C, x, y + C, lpi::ColorRGB(0, 0, 0, 255));
  }
}

void ITool::drawOwnIcon(lpi::gui::IGUIDrawer& drawer, Paint& paint, int hotspot_x, int hotspot_y)
{
  (void)paint;
  
  int x = drawer.getInput().mouseX();
  int y = drawer.getInput().mouseY();
  
  x -= hotspot_x;
  y -= hotspot_y;
  
  lpi::HTexture* icon = getIcon();
  
  if(icon && icon->texture)
  {
    drawer.convertTextureIfNeeded(icon->texture);
    drawer.drawTexture(icon->texture, x, y);
  }
}

void ITool::addBrushSettingsToPage(lpi::gui::DynamicPage& page, const lpi::gui::IGUIDrawer& geom)
{
  page.addControl(_t("Size"), new lpi::gui::DynamicSliderSpinner<int>(&settings.brush.size, 1, 100, 1, 300, 1, geom));
  page.addControl(_t("Step"), new lpi::gui::DynamicFakeInteger<double>(&settings.brush.step, 100, geom));
  page.addControl(_t("Opacity"), new lpi::gui::DynamicFakeInteger<double>(&settings.brush.opacity, 100, geom));
  page.addControl(_t("Softness"), new lpi::gui::DynamicFakeInteger<double>(&settings.brush.softness, 100, geom));
  std::vector<Brush::Shape> shapes; shapes.push_back(Brush::SHAPE_ROUND); shapes.push_back(Brush::SHAPE_SQUARE); shapes.push_back(Brush::SHAPE_DIAMOND);
  std::vector<std::string> names; names.push_back(_t("Round")); names.push_back(_t("Square")); names.push_back(_t("Diamond"));
  page.addControl(_t("Shape"), new lpi::gui::DynamicEnum<Brush::Shape>(&settings.brush.shape, names, shapes, geom));
}

void ITool::addPenSettingsToPage(lpi::gui::DynamicPage& page, const lpi::gui::IGUIDrawer& geom)
{
  page.addControl(_t("Opacity"), new lpi::gui::DynamicFakeInteger<double>(&settings.brush.opacity, 100, geom));
}

void ITool::addGeomSettingsToPage(lpi::gui::DynamicPage& page, const lpi::gui::IGUIDrawer& geom)
{
  page.addControl(_t("Enable Outline"), new lpi::gui::DynamicCheckbox(&settings.shape.line_enabled));
  page.addControl(_t("Line Thickness"), new lpi::gui::DynamicSliderSpinner<int>(&settings.shape.line_thickness, 1, 32, 1, 100, 1, geom));
  page.addControl(_t("Line Opacity"), new lpi::gui::DynamicFakeInteger<double>(&settings.shape.line_opacity, 100, geom));
  page.addControl(_t("Enable Fill"), new lpi::gui::DynamicCheckbox(&settings.shape.fill_enabled));
  page.addControl(_t("Fill Opacity"), new lpi::gui::DynamicFakeInteger<double>(&settings.shape.fill_opacity, 100, geom));
}

////////////////////////////////////////////////////////////////////////////////

DialogTool::DialogTool(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom)
: ITool(settings)
, params(false)
{
  (void)geom;
  //addBrushSettingsToPage(dialog, geom);
}

////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////

