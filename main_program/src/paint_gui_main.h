/*
LodePaint

Copyright (c) 2009-2011 Lode Vandevenne
All rights reserved.

This file is part of LodePaint.

LodePaint is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LodePaint is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LodePaint.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "lpi/lpi_color.h"
#include "lpi/lpi_pvector.h"
#include "lpi/gui/lpi_gui.h"
#include "lpi/gui/lpi_gui_dynamic.h"
#include "lpi/gui/lpi_gui_file.h"

#include "paint_gui.h"
#include "paint_settings.h"


struct MenuToolbarIndices
{
  MenuToolbarIndices();

  lpi::gui::MenuVertical menu_file;
  size_t menuindex_new;
  size_t menuindex_open;
  size_t menuindex_reload;
  size_t menuindex_save;
  size_t menuindex_saveas;
  size_t menuindex_savecopyas;
  size_t menuindex_saveall;
  size_t menuindex_close;
  size_t menuindex_closeall;
  size_t menuindex_exit;

  lpi::gui::MenuVertical menu_edit;
  size_t menuindex_undo;
  size_t menuindex_redo;
  size_t menuindex_cut;
  size_t menuindex_copy;
  size_t menuindex_paste_image;
  size_t menuindex_paste_sel;
  //size_t menuindex_temp_debug;

  lpi::gui::MenuVertical menu_skins;
  size_t menuindex_skin_default;
  size_t menuindex_skin_aos4;
  size_t menuindex_skin_dark;
  size_t menuindex_skin_old;
  size_t menuindex_skin_none_found_message;

  lpi::gui::MenuVertical menu_language;
  size_t menuindex_lang_none_found_message;

  lpi::gui::MenuVertical menu_settings;
  size_t menuindex_options;
  size_t menuindex_shortcuts;

  lpi::gui::MenuVertical menu_help;
  size_t menuindex_help;
  size_t menuindex_about;
  size_t menuindex_systeminfo;

  lpi::gui::MenuVertical menu_image;
  std::vector<lpi::gui::MenuVertical*> submenu_image;
  lpi::VectorGuard<std::vector<lpi::gui::MenuVertical*> > vg_image;
  size_t menuindex_imageinfo;

  lpi::gui::MenuVertical menu_filters;
  std::vector<lpi::gui::MenuVertical*> submenu_filters;
  lpi::VectorGuard<std::vector<lpi::gui::MenuVertical*> > vg_filters;

  lpi::gui::MenuVertical menu_plugins;
  std::vector<lpi::gui::MenuVertical*> submenu_plugins;
  lpi::VectorGuard<std::vector<lpi::gui::MenuVertical*> > vg_plugins;
  size_t menuindex_plugins_none_found_message;

  lpi::gui::MenuHorizontal mainmenu;

  //////////////////////////////////////////////////////////////////////////////

  size_t toolbar_new;
  size_t toolbar_open;
  size_t toolbar_save;
  size_t toolbar_save_as;
  size_t toolbar_save_all;

  size_t toolbar_cut;
  size_t toolbar_copy;
  size_t toolbar_paste_image;
  size_t toolbar_paste_sel;
  size_t toolbar_undo;
  size_t toolbar_redo;
  size_t toolbar_zoomin;
  size_t toolbar_zoomout;
  size_t toolbar_zoom1;
  size_t toolbar_fullscreen;
  size_t toolbar_bgpattern;
  size_t toolbar_pixel_raster;
  size_t toolbar_crosshair;

  size_t toolbar_mask;
  size_t toolbar_maskcolor;
  size_t toolbar_filter_on_all;

  size_t toolbar_clear_image;
  size_t toolbar_crop;
  size_t toolbar_flip_x;
  size_t toolbar_flip_y;
  size_t toolbar_rescale;
  size_t toolbar_wrap_50;
  size_t toolbar_negate_rgb;
  size_t toolbar_negate_mask;
  size_t toolbar_normalize;
  size_t toolbar_brightness_contrast;
  size_t toolbar_hue_saturation;
  size_t toolbar_multiply_sla;
  size_t toolbar_greyscale;
  size_t toolbar_alpha_opaque;
  size_t toolbar_alpha_apply;
  size_t toolbar_gaussian;
  size_t toolbar_pixel_antialias;
  size_t toolbar_blend_sel;
  size_t toolbar_repeat;
  size_t toolbar_repeat_2;
  size_t toolbar_test_image;
};

class OptionsWindow : public lpi::gui::Dialog
{
  lpi::gui::DynamicPage page;
  lpi::gui::Button ok;
  lpi::gui::Button cancel;
  Options options;

  public:

  OptionsWindow( const lpi::gui::IGUIDrawer& geom);

  virtual void handleImpl(const lpi::IInput& input);

  const Options& getOptions() const;
  Options& getOptions();

  void valueToControls();
};

class ToolSettingWindow : public lpi::gui::Window //for the tool settings
{
  private:

  public:

    ToolSettingWindow(int x, int y, const lpi::gui::IGUIDrawer& geom);
    void clearCustomTab();
    void setCustomTab(lpi::gui::Element* e);
    virtual void handleImpl(const lpi::IInput& input);
};

class ShortcutWindow : public lpi::gui::Dialog
{
  private:
    lpi::gui::DropDownList list;
    ShortCutField field0;
    ShortCutField field1;
    ShortCutField default0;
    ShortCutField default1;
    ShortCutManager& manager;
    lpi::gui::Button ok;
    lpi::gui::Button cancel;
  public:
    ShortcutWindow(ShortCutManager& manager, const lpi::gui::IGUIDrawer& drawer);
    void reinit();
    virtual void handleImpl(const lpi::IInput& input);
    virtual void drawImpl(lpi::gui::IGUIDrawer& drawer) const;
};

void fillMenusAndToolbars(MenuToolbarIndices& m, MainProgram& p, FiltersCollection& filters);


