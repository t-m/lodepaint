/*
LodePaint

Copyright (c) 2009-2010 Lode Vandevenne
All rights reserved.

This file is part of LodePaint.

LodePaint is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LodePaint is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LodePaint.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "lpi/lpi_texture.h"

#include "imalg/generate.h"

#include "paint_filter.h"

class OperationRandomNoise : public AOperationFilterUtil
{
  private:
    bool alpha;
    double opacity;
    bool randomize_opacity;
    double saltpepper;

    enum ColorType
    {
      C_GREY, //black&white
      C_RGB, //random R, G an B
      C_FGBG, //random value between FG and BG color
      C_BRIGHTNESS, //color of underlying pixel, with adjusted brightness
      C_HUE,
      C_SATURATION
    };
    ColorType colorType;

    enum Distribution
    {
      D_UNIFORM,
      D_GAUSSIAN,
      D_BINARY //either fully on or fully off
    };
    Distribution distribution;

    DynamicFilterPageWithPreview dialog;

    virtual void doit(TextureRGBA8& texture, Progress& progress);

    double getRandom(Distribution d);
    double getRandom(double from, double to, Distribution d);
    int getRandom(int from, int to, Distribution d);

  public:
    OperationRandomNoise(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom);

    virtual lpi::gui::Element* getDialog() { return &dialog; }
    virtual std::string getLabel() const { return "Random Noise"; }
    virtual std::string getHelp() { return "[OperationRandomNoise Help]"; }
};

class OperationTurbulence : public AOperationFilterUtil
{
  protected:
    bool alpha;
    bool colored; //uses foreground to background gradient
    double opacity;
    int size;
    bool smooth;

    enum Algo
    {
      ALGO_NEAREST, //pixel
      ALGO_BILINEAR,
      ALGO_BICUBIC
    };

    Algo algo;


    DynamicFilterPageWithPreview dialog;
    virtual void doit(TextureRGBA8& texture, Progress& progress);

    double smoothNoise(double x, double y);
    double turbulence(double x, double y, double size);
    void generateNoise();

    const static int noiseSize = 128;
    std::vector<double> noise;


  public:
    OperationTurbulence(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom);

    virtual lpi::gui::Element* getDialog() { return &dialog; }
    virtual std::string getLabel() const { return "Clouds"; }
    virtual std::string getHelp() { return "[OperationTurbulence Help]"; }
};

class OperationMarble : public OperationTurbulence
{
  private:

   double turbPower;
   double xPeriod;
   double yPeriod;

   enum Pattern
   {
     STRIPES,
     RINGS,
     DOTS
   };

   Pattern pattern;

  public:
    OperationMarble(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom);

    virtual void doit(TextureRGBA8& texture, Progress& progress);
    virtual std::string getLabel() const { return "Marble"; }
    virtual std::string getHelp() { return "[OperationMarble Help]"; }
};

class OperationPlasma : public AOperationFilterUtil
{
  protected:
    PlasmaParam p;
    double opacity;

    DynamicFilterPageWithPreview dialog;
    virtual void doit(TextureRGBA8& texture, Progress& progress);

  public:
    OperationPlasma(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom);

    virtual lpi::gui::Element* getDialog() { return &dialog; }
    virtual std::string getLabel() const { return "Plasma"; }
    virtual std::string getHelp() { return "[OperationPlasma Help]"; }
};

//generates a very simple two-colored tile pattern
class OperationGenerateTiles : public AOperationFilterUtil
{
  protected:
    int sizex;
    int sizey;
    int shiftx;
    int shifty;
    double opacity0;
    double opacity1;
    DynamicFilterPageWithPreview dialog;


  public:
    OperationGenerateTiles(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom);

    virtual void doit(TextureRGBA8& texture, Progress& progress);
    virtual lpi::gui::Element* getDialog() { return &dialog; }
    virtual std::string getLabel() const { return "Tiles"; }
    virtual std::string getHelp() { return "[OperationGenerateTiles Help]"; }
};

//generates a very simple two-colored tile pattern
class OperationGenerateGradient : public AOperationFilterUtil
{
  protected:
    double angle;
    double opacity;
    bool affect_alpha;
    DynamicFilterPageWithPreview dialog;


  public:
    OperationGenerateGradient(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom);

    virtual void doit(TextureRGBA8& texture, Progress& progress);
    virtual lpi::gui::Element* getDialog() { return &dialog; }
    virtual std::string getLabel() const { return "Gradient"; }
    virtual std::string getHelp() { return "[OperationGenerateGradient Help]"; }
};

//generates a very simple two-colored tile pattern
class OperationGenerateGradientQuad : public AOperationFilterUtil
{
  protected:
    lpi::ColorRGBd color00;
    lpi::ColorRGBd color01;
    lpi::ColorRGBd color10;
    lpi::ColorRGBd color11;

    double opacity;
    bool affect_alpha;
    DynamicFilterPageWithPreview dialog;


  public:
    OperationGenerateGradientQuad(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom);

    virtual void doit(TextureRGBA8& texture, Progress& progress);
    virtual lpi::gui::Element* getDialog() { return &dialog; }
    virtual std::string getLabel() const { return "Gradient Quad"; }
    virtual std::string getHelp() { return "[OperationGenerateGradientQuad Help]"; }
};

class OperationGenerateGrid : public AOperationFilterUtil
{
  protected:
    int sizex;
    int sizey;
    int shiftx;
    int shifty;
    double opacity;
    DynamicFilterPageWithPreview dialog;


  public:
    OperationGenerateGrid(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom);

    virtual void doit(TextureRGBA8& texture, Progress& progress);
    virtual lpi::gui::Element* getDialog() { return &dialog; }
    virtual std::string getLabel() const { return "Grid"; }
    virtual std::string getHelp() { return "[OperationGenerateGrid Help]"; }
};

class OperationGenerateXORTexture : public AOperationFilterUtil
{
  protected:
    //there are no parameters and the result is always greyscale. To get special colors, use other filters that change color after this (such as the channel switcher)

    int getMaxValue(int u, int v);

  public:
    OperationGenerateXORTexture(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom)
    : AOperationFilterUtil(settings, true, true)
    {
      (void)geom;
    }

    virtual void doit(TextureRGBA8& texture, Progress& progress);
    virtual void doit(TextureRGBA32F& texture, Progress& progress);
    virtual void doit(TextureGrey8& texture, Progress& progress);
    virtual void doit(TextureGrey32F& texture, Progress& progress);
    virtual bool supportsColorMode(ColorMode mode) const { return sup(mode, true, true, true, true); }

    virtual lpi::gui::Element* getDialog() { return 0; }
    virtual std::string getLabel() const { return "XOR Texture"; }
};

class OperationGenerateTestImage : public AOperationFilterUtil
{
  protected:

  public:
    OperationGenerateTestImage(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom)
    : AOperationFilterUtil(settings, true, true)
    {
      (void)geom;
    }

    virtual void doit(TextureRGBA8& texture, Progress& progress);
    virtual bool supportsColorMode(ColorMode mode) const { return sup(mode, true, false, false, false); }

    virtual lpi::gui::Element* getDialog() { return 0; }
    virtual std::string getLabel() const { return "Test Image"; }
};

class OperationGenerateMandelbrot : public AOperationFilterUtil
{
  protected:
    DynamicFilterPageWithPreview dialog;

    double zoom;
    double moveX;
    double moveY;
    int maxIterations;
    bool normalizedIteration; //smooth coloring (has most visible effect when maxIterations is low)
    bool hue;
    bool tricorn;

  public:
    OperationGenerateMandelbrot(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom);

    virtual void doit(TextureRGBA8& texture, Progress& progress);

    virtual bool supportsColorMode(ColorMode mode) const { return sup(mode, true, false, false, false); }

    virtual lpi::gui::Element* getDialog() { return &dialog; }
    virtual std::string getLabel() const { return "Mandelbrot"; }
    virtual std::string getHelp() { return "[OperationGenerateMandelbrot Help]"; }
};

class OperationGenerateJulia : public AOperationFilterUtil
{
  protected:
    DynamicFilterPageWithPreview dialog;

    double zoom;
    double moveX;
    double moveY;
    double re;
    double im;
    int maxIterations;
    bool normalizedIteration; //smooth coloring (has most visible effect when maxIterations is low)
    bool hue;
    bool tricorn; //terminology from Mandelbrot, but similar math for Julia

  public:
    OperationGenerateJulia(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom);

    virtual void doit(TextureRGBA8& texture, Progress& progress);

    virtual bool supportsColorMode(ColorMode mode) const { return sup(mode, true, false, false, false); }

    virtual lpi::gui::Element* getDialog() { return &dialog; }
    virtual std::string getLabel() const { return "Julia"; }
    virtual std::string getHelp() { return "[OperationGenerateJulia Help]"; }
};

class OperationMedianFilter : public AOperationFilterUtil
{
  private:
    virtual void doit(TextureRGBA8& texture, Progress& progress);
    virtual void doit(TextureRGBA32F& texture, Progress& progress);
    virtual bool supportsColorMode(ColorMode mode) const { return sup(mode, true, true, false, false); }

    int diameterx;
    int diametery;
    bool wrapping;
    bool affect_alpha;

    DynamicFilterPageWithPreview dialog;

  public:
    OperationMedianFilter(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom);

    virtual lpi::gui::Element* getDialog() { return &dialog; }
    virtual std::string getLabel() const { return "Median Filter"; }
    virtual std::string getHelp() { return "[OperationMedianFilter Help]"; }
};

class OperationAdaptiveMedianFilter : public AOperationFilterUtil
{
  private:
    virtual void doit(TextureRGBA8& texture, Progress& progress);

    int maxdiameter;
    bool wrapping;
    bool affect_alpha;

    DynamicFilterPageWithPreview dialog;

  public:
    OperationAdaptiveMedianFilter(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom);
    virtual lpi::gui::Element* getDialog() { return &dialog; }
    virtual std::string getLabel() const { return "Adaptive Median Filter"; }
    virtual std::string getHelp() { return "[OperationAdaptiveMedianFilter Help]"; }
};

class OperationBlur : public AOperationFilterUtil
{
  private:
    virtual void doit(TextureRGBA8& texture, Progress& progress);
    virtual void doit(TextureRGBA32F& texture, Progress& progress);
    virtual bool supportsColorMode(ColorMode mode) const { return sup(mode, true, true, false, false); }

    enum Amount
    {
      LESS,
      NORMAL,
      MORE
    };

    Amount amount;
    bool wrapping;
    bool affect_alpha;

    DynamicFilterPageWithPreview dialog;

  public:
    OperationBlur(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom);

    virtual lpi::gui::Element* getDialog() { return &dialog; }
    virtual std::string getLabel() const { return "Blur"; }
    virtual std::string getHelp() { return "[OperationBlur Help]"; }
};

class OperationSoften : public AOperationFilterUtil
{
  private:
    virtual void doit(TextureRGBA8& texture, Progress& progress);

  public:
    OperationSoften(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom);

    virtual std::string getLabel() const { return "Soften"; }
};

class OperationSoftestBlur : public AOperationFilterUtil
{
  private:
    virtual void doit(TextureRGBA8& texture, Progress& progress);

  public:
    OperationSoftestBlur(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom);

    virtual std::string getLabel() const { return "Softest Possible Blur"; }
};

class OperationSharpen : public AOperationFilterUtilWithSettings
{
  private:
    virtual void doit(TextureRGBA8& texture, const GlobalToolSettings& settings, Progress& progress);
    virtual void doit(TextureRGBA32F& texture, const GlobalToolSettings& settings, Progress& progress);
    virtual bool supportsColorMode(ColorMode mode) const { return sup(mode, true, true, false, false); }

    bool wrapping;
    bool affect_alpha;

    DynamicFilterPageWithPreview dialog;

  public:
    OperationSharpen(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom);

    virtual lpi::gui::Element* getDialog() { return &dialog; }
    virtual std::string getLabel() const { return "Sharpen"; }
    virtual std::string getHelp() { return "[OperationSharpen Help]"; }
};

class OperationGaussianBlur : public AOperationFilterUtil
{
  private:
    double radius;
    bool wrapping;
    bool affect_alpha;

    DynamicFilterPageWithPreview dialog;

    virtual void doit(TextureRGBA8& texture, Progress& progress);
    virtual void doit(TextureRGBA32F& texture, Progress& progress);
    virtual void doit(TextureGrey8& texture, Progress& progress);
    virtual void doit(TextureGrey32F& texture, Progress& progress);
    virtual bool supportsColorMode(ColorMode mode) const { return sup(mode, true, true, true, true); }

  public:

    OperationGaussianBlur(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom);
    virtual lpi::gui::Element* getDialog() { return &dialog; }
    virtual std::string getLabel() const { return "Gaussian Blur"; }
    virtual std::string getHelp() { return "[OperationGaussianBlur Help]"; }
};

class OperationInterpolatingBlur : public AOperationFilterUtil
{
  private:
    int radius;
    bool affect_alpha;

    DynamicFilterPageWithPreview dialog;

    virtual void doit(TextureRGBA8& texture, Progress& progress);
    virtual bool supportsColorMode(ColorMode mode) const { return sup(mode, true, false, false, false); }

  public:

    OperationInterpolatingBlur(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom);
    virtual lpi::gui::Element* getDialog() { return &dialog; }
    virtual std::string getLabel() const { return "Interpolating Blur"; }
    virtual std::string getHelp() { return "[OperationInterpolatingBlur Help]"; }
};

class OperationSelectiveGaussianBlur : public AOperationFilterUtil
{
  protected:
    double radius;
    double threshold;
    bool wrapping;
    bool affect_alpha;

    DynamicFilterPageWithPreview dialog;

    virtual void doit(TextureRGBA8& texture, Progress& progress);
    virtual void doit(TextureRGBA32F& texture, Progress& progress);
    virtual void doit(TextureGrey8& texture, Progress& progress);
    virtual void doit(TextureGrey32F& texture, Progress& progress);
    virtual bool supportsColorMode(ColorMode mode) const { return sup(mode, true, true, true, true); }

  public:

    OperationSelectiveGaussianBlur(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom);
    virtual lpi::gui::Element* getDialog() { return &dialog; }
    virtual std::string getLabel() const { return "Selective Gaussian Blur"; }
    virtual std::string getHelp() { return "[OperationSelectiveGaussianBlur Help]"; }
};

class OperationRemoveJpegArtifacts : public OperationSelectiveGaussianBlur
{
  private:

  public:

    OperationRemoveJpegArtifacts(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom) :
        OperationSelectiveGaussianBlur(settings, geom)
    {
      threshold = 25;
    };
    virtual lpi::gui::Element* getDialog() { return 0; }
    virtual std::string getLabel() const { return "Remove Jpeg Artifacts"; }
};

class OperationUnsharpMask : public AOperationFilterUtil
{
  private:
    double radius;
    double power;
    int threshold;
    bool wrapping;
    bool affect_alpha;

    DynamicFilterPageWithPreview dialog;

    virtual void doit(TextureRGBA8& texture, Progress& progress);
    //virtual void doit(TextureRGBA32F& texture, Progress& progress);

  public:

    OperationUnsharpMask(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom);
    virtual lpi::gui::Element* getDialog() { return &dialog; }
    virtual std::string getLabel() const { return "Unsharp Mask"; }
    virtual std::string getHelp() { return "[OperationUnsharpMask Help]"; }
};

class OperationHighPass : public AOperationFilterUtil
{
  private:
    double radius;
    bool wrapping;
    bool affect_alpha;

    DynamicFilterPageWithPreview dialog;

    virtual void doit(TextureRGBA8& texture, Progress& progress);

  public:

    OperationHighPass(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom);
    virtual lpi::gui::Element* getDialog() { return &dialog; }
    virtual std::string getLabel() const { return "High Pass"; }
    virtual std::string getHelp() { return "[OperationHighPass Help]"; }
};

class OperationEmboss : public AOperationFilterUtilWithSettings
{
  private:
    virtual void doit(TextureRGBA8& texture, const GlobalToolSettings& settings, Progress& progress);

    bool wrapping;
    bool affect_alpha;

    enum Matrix
    {
      MATRIX1,
      MATRIX2,
      MATRIX3
    };

    Matrix matrixType;

    enum MatrixRot
    {
      R0,
      R45,
      R90,
      R135,
      R180,
      R225,
      R270,
      R315
    };

    MatrixRot matrixRot;

    DynamicFilterPageWithPreview dialog;

  public:
    OperationEmboss(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom);
    virtual lpi::gui::Element* getDialog() { return &dialog; }
    virtual std::string getLabel() const { return "Emboss"; }
    virtual std::string getHelp() { return "[OperationEmboss Help]"; }
};

class OperationEdgeDetect : public AOperationFilterUtilWithSettings
{
  private:
    virtual void doit(TextureRGBA8& texture, const GlobalToolSettings& settings, Progress& progress);

    bool wrapping;
    bool affect_alpha;

    enum Matrix
    {
      SOBEL,
      PREWITT,
      PREWITT2,
      SCHARR,
      ROBERT
    };

    Matrix matrixType;

    enum MatrixRot
    {
      R0,
      R45,
      R90,
      R135,
      R180,
      R225,
      R270,
      R315
    };

    MatrixRot matrixRot;

    DynamicFilterPageWithPreview dialog;

  public:
    OperationEdgeDetect(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom);
    virtual lpi::gui::Element* getDialog() { return &dialog; }
    virtual std::string getLabel() const { return "Edge Detect"; }
    virtual std::string getHelp() { return "[OperationEdgeDetect Help]"; }
};

class OperationPixelEdgeDetect : public AOperationFilterUtilWithSettings
{
  private:
    virtual void doit(TextureRGBA8& texture, const GlobalToolSettings& settings, Progress& progress);

  public:
    OperationPixelEdgeDetect(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom);
    virtual lpi::gui::Element* getDialog() { return 0; }
    virtual std::string getLabel() const { return "Pixel Edge Detect"; }
};

class OperationCannyEdgeDetect : public AOperationFilterUtilWithSettings
{
  private:
    virtual void doit(TextureRGBA8& texture, const GlobalToolSettings& settings, Progress& progress);

    bool affect_alpha;
    int steps;
    double radius;
    int lowthreshold;
    int highthreshold;

    DynamicFilterPageWithPreview dialog;

  public:
    OperationCannyEdgeDetect(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom);
    virtual lpi::gui::Element* getDialog() { return &dialog; }
    virtual std::string getLabel() const { return "Canny Edge Detector"; }
    virtual std::string getHelp() { return "[OperationCannyEdgeDetect Help]"; }
};

class OperationScanLines : public AOperationFilterUtil
{
  //TODO: make this effect better and more customizable
  private:
    virtual void doit(TextureRGBA8& texture, Progress& progress);
    bool colored;
    double opacity;
    bool affect_alpha;

    DynamicFilterPageWithPreview dialog;

  public:
    OperationScanLines(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom);
    virtual lpi::gui::Element* getDialog() { return &dialog; }
    virtual std::string getLabel() const { return "Scan Lines"; }
    virtual std::string getHelp() { return "[OperationScanLines Help]"; }
};

class OperationMosaic : public AOperationFilterUtil
{
  private:
    virtual void doit(TextureRGBA8& texture, Progress& progress);

    DynamicFilterPageWithPreview dialog;
    int tilesize;
    bool antialiasing; //if true, the resulting color is the average of all colors inside, instead of just that of one pixel (but it's slower to calculate)
  public:
    OperationMosaic(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom);

    virtual std::string getLabel() const { return "Mosaic"; }
    virtual lpi::gui::Element* getDialog() { return &dialog; }
    virtual std::string getHelp() { return "[OperationMosaic Help]"; }
};

class OperationUserDefinedFilter;

class UserDefinedFilterDialog : public FilterDialogWithPreview
{
  private:

    OperationUserDefinedFilter* udf;

  public:
    DynamicPageWithAutoUpdate page;
    Numbers numbers;

    UserDefinedFilterDialog(OperationUserDefinedFilter* udf, const lpi::gui::IGUIDrawer& geom);
    virtual void drawImpl(lpi::gui::IGUIDrawer& drawer) const;
    virtual void handleImpl(const lpi::IInput& input);
};

class OperationUserDefinedFilter : public AOperationFilterUtil
{
  public:
    friend class UserDefinedFilterDialog;
  private:
    virtual void doit(TextureRGBA8& texture, Progress& progress);
    virtual void doit(TextureRGBA32F& texture, Progress& progress);
    virtual bool supportsColorMode(ColorMode mode) const { return sup(mode, true, true, false, false); }

    bool wrapping;
    bool affect_alpha;

    double matrix[49];

    double mul;
    double div;
    double add;

    UserDefinedFilterDialog dialog;

  public:
    OperationUserDefinedFilter(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom);
    virtual lpi::gui::Element* getDialog() { return &dialog; }
    virtual std::string getLabel() const { return "User Defined"; }
    virtual std::string getHelp() { return "[OperationUserDefinedFilter Help]"; }
};

class OperationPixelShadow : public AOperationFilterUtil
{
  private:
    virtual void doit(TextureRGBA8& texture, Progress& progress);

    DynamicFilterPageWithPreview dialog;
    int xshift;
    int yshift;
    bool not_from_shadow_color;

    enum Mode
    {
      M_SHADOW,
      M_REMOVE
    };

    Mode mode;

  public:
    OperationPixelShadow(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom);

    virtual std::string getLabel() const { return "Pixel Shadow"; }
    virtual lpi::gui::Element* getDialog() { return &dialog; }
    virtual std::string getHelp() { return "[OperationPixelShadow Help]"; }
};

class OperationPixelBorder : public AOperationFilterUtil
{
  private:
    virtual void doit(TextureRGBA8& texture, Progress& progress);

    DynamicFilterPageWithPreview dialog;

    enum Mode
    {
      M_ADD,
      M_REMOVE
    };

    Mode mode;

  public:
    OperationPixelBorder(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom);

    virtual std::string getLabel() const { return "Pixel Border"; }
    virtual lpi::gui::Element* getDialog() { return &dialog; }
    virtual std::string getHelp() { return "[OperationPixelBorder Help]"; }
};

class OperationPixelAntialias : public AOperationFilterUtil
{
  private:
    virtual void doit(TextureRGBA8& texture, Progress& progress);

    //DynamicFilterPageWithPreview dialog;

    //double amount;
    //bool darkest; //to expand the darkest or the lightest shapes?

  public:
    OperationPixelAntialias(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom);

    virtual std::string getLabel() const { return "Pixel Antialias"; }

    //virtual lpi::gui::Element* getDialog() { return &dialog; }
};

class OperationMirrorBits : public AOperationFilterUtil
{
  private:
    virtual void doit(TextureRGBA8& texture, Progress& progress);
  public:
    OperationMirrorBits(GlobalToolSettings& settings) : AOperationFilterUtil(settings, false, false) {}
    virtual std::string getLabel() const { return "Mirror Bits"; }
};

class OperationShiftBits4 : public AOperationFilterUtil
{
  private:
    virtual void doit(TextureRGBA8& texture, Progress& progress);
  public:
    OperationShiftBits4(GlobalToolSettings& settings) : AOperationFilterUtil(settings, false, false) {}
    virtual std::string getLabel() const { return "Shift Bits 4"; }
};

class OperationShiftBits1 : public AOperationFilterUtil
{
  private:
    virtual void doit(TextureRGBA8& texture, Progress& progress);
  public:
    OperationShiftBits1(GlobalToolSettings& settings) : AOperationFilterUtil(settings, true, true) {}
    virtual std::string getLabel() const { return "Shift Bits 1"; }
};

class OperationBitsToGrayCode : public AOperationFilterUtil
{
  private:
    virtual void doit(TextureRGBA8& texture, Progress& progress);
  public:
    OperationBitsToGrayCode(GlobalToolSettings& settings) : AOperationFilterUtil(settings, true, true) {}
    virtual std::string getLabel() const { return "Binary to Gray code"; }
};

class OperationBitsFromGrayCode : public AOperationFilterUtil
{
  private:
    virtual void doit(TextureRGBA8& texture, Progress& progress);
  public:
    OperationBitsFromGrayCode(GlobalToolSettings& settings) : AOperationFilterUtil(settings, true, true) {}
    virtual std::string getLabel() const { return "Gray code to binary"; }
};

class OperationBitsToGrayCodeRGBA : public AOperationFilterUtil
{
  private:
    virtual void doit(TextureRGBA8& texture, Progress& progress);
  public:
    OperationBitsToGrayCodeRGBA(GlobalToolSettings& settings) : AOperationFilterUtil(settings, true, true) {}
    virtual std::string getLabel() const { return "Binary to Gray code RGBA"; }
};

class OperationBitsFromGrayCodeRGBA : public AOperationFilterUtil
{
  private:
    virtual void doit(TextureRGBA8& texture, Progress& progress);
  public:
    OperationBitsFromGrayCodeRGBA(GlobalToolSettings& settings) : AOperationFilterUtil(settings, true, true) {}
    virtual std::string getLabel() const { return "Gray code to binary RGBA"; }
};

class OperationRandomShuffle : public AOperationFilterUtil
{
  private:
    virtual void doit(TextureRGBA8& texture, Progress& progress);
  public:
    OperationRandomShuffle(GlobalToolSettings& settings)
    : AOperationFilterUtil(settings, true, true)
    {
    }

    virtual std::string getLabel() const { return "Random Shuffle"; }
};

class OperationRandomWalk : public AOperationFilterUtil
{
  private:
    virtual void doit(TextureRGBA8& texture, Progress& progress);

    DynamicFilterPageWithPreview dialog;

    double amount;
    int distance; //manhattan
    bool wrapping;

  public:
    OperationRandomWalk(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom);

    virtual std::string getLabel() const { return "Random Walk"; }
    virtual lpi::gui::Element* getDialog() { return &dialog; }
    virtual std::string getHelp() { return "[OperationRandomWalk Help]"; }
};


class OperationSepia : public AOperationFilterUtil
{
  private:
    DynamicFilterPageWithPreview dialog;

    lpi::ColorRGBd color;
    double brightness;


    virtual void doit(TextureRGBA8& texture, Progress& progress);
    virtual void doit(TextureRGBA32F& texture, Progress& progress);
    virtual bool supportsColorMode(ColorMode mode) const { return sup(mode, true, true, false, false); }
  public:
    OperationSepia(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom);
    virtual std::string getLabel() const { return "Sepia"; }
    virtual lpi::gui::Element* getDialog() { return &dialog; }
    virtual std::string getHelp() { return "[OperationSepia Help]"; }
};


class OperationContour : public AOperationFilterUtil
{
  private:
    virtual void doit(TextureRGBA8& texture, Progress& progress);
    virtual void doit(TextureGrey8& texture, Progress& progress);
    virtual bool supportsColorMode(ColorMode mode) const { return sup(mode, true, false, true, false); }

    DynamicFilterPageWithPreview dialog;

    double value;
    bool upper; //other is "lower"

    bool wrapping;
    bool affect_alpha;

  public:
    OperationContour(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom);

    virtual std::string getLabel() const { return "Contour"; }
    virtual lpi::gui::Element* getDialog() { return &dialog; }
    virtual std::string getHelp() { return "[OperationContour Help]"; }
};


class OperationPointillism : public AOperationFilterUtil
{
  private:
    DynamicFilterPageWithPreview dialog;

    int pointSize;
    double opacity;
    double amount;
    bool onCanvas;
    bool regular;


    virtual void doit(TextureRGBA8& texture, Progress& progress);
  public:
    OperationPointillism(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom);
    virtual std::string getLabel() const { return "Pointillism"; }
    virtual lpi::gui::Element* getDialog() { return &dialog; }
    virtual std::string getHelp() { return "[OperationPointillism Help]"; }
};

//gives a somewhat brush-stroke like look by doing mirror bits, then erode, then mirror bits again
class OperationBrushy : public AOperationFilterUtil
{
  protected:

  public:
    OperationBrushy(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom)
    : AOperationFilterUtil(settings, true, true)
    {
      (void)geom;
    }

    virtual void doit(TextureRGBA8& texture, Progress& progress);
    virtual bool supportsColorMode(ColorMode mode) const { return sup(mode, true, false, false, false); }

    virtual lpi::gui::Element* getDialog() { return 0; }
    virtual std::string getLabel() const { return "Brushy"; }
};

//converting channel bits to grey code, then rotate-shifting bits 1 right, then converting back from grey code,
//gives a spooky neon effect
class OperationSpookyNeon : public AOperationFilterUtil
{
  protected:

  public:
    OperationSpookyNeon(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom)
    : AOperationFilterUtil(settings, true, true)
    {
      (void)geom;
    }

    virtual void doit(TextureRGBA8& texture, Progress& progress);
    virtual bool supportsColorMode(ColorMode mode) const { return sup(mode, true, false, false, false); }

    virtual lpi::gui::Element* getDialog() { return 0; }
    virtual std::string getLabel() const { return "Spooky Neon"; }
};

class OperationErode : public AOperationFilterUtil
{
  private:
    virtual void doit(TextureRGBA8& texture, Progress& progress);

    bool wrapping;
    bool affect_alpha;

    DynamicFilterPageWithPreview dialog;

  public:
    OperationErode(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom);
    virtual lpi::gui::Element* getDialog() { return &dialog; }
    virtual std::string getLabel() const { return "Erode"; }
    virtual std::string getHelp() { return "[OperationErode Help]"; }
};

class OperationDilate : public AOperationFilterUtil
{
  private:
    virtual void doit(TextureRGBA8& texture, Progress& progress);

    bool wrapping;
    bool affect_alpha;

    DynamicFilterPageWithPreview dialog;

  public:
    OperationDilate(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom);
    virtual lpi::gui::Element* getDialog() { return &dialog; }
    virtual std::string getLabel() const { return "Dilate"; }
    virtual std::string getHelp() { return "[OperationDilate Help]"; }
};

class OperationMorphologyOpen : public AOperationFilterUtil
{
  private:
    virtual void doit(TextureRGBA8& texture, Progress& progress);

    bool wrapping;
    bool affect_alpha;

    DynamicFilterPageWithPreview dialog;

  public:
    OperationMorphologyOpen(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom);
    virtual lpi::gui::Element* getDialog() { return &dialog; }
    virtual std::string getLabel() const { return "Morph Open"; }
    virtual std::string getHelp() { return "[OperationMorphologyOpen Help]"; }
};

class OperationMorphologyClose : public AOperationFilterUtil
{
  private:
    virtual void doit(TextureRGBA8& texture, Progress& progress);

    bool wrapping;
    bool affect_alpha;

    DynamicFilterPageWithPreview dialog;

  public:
    OperationMorphologyClose(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom);
    virtual lpi::gui::Element* getDialog() { return &dialog; }
    virtual std::string getLabel() const { return "Morph Close"; }
    virtual std::string getHelp() { return "[OperationMorphologyClose Help]"; }
};

class OperationMorphologyGradient : public AOperationFilterUtil
{
  private:
    virtual void doit(TextureRGBA8& texture, Progress& progress);

    bool wrapping;
    bool affect_alpha;

    DynamicFilterPageWithPreview dialog;

  public:
    OperationMorphologyGradient(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom);
    virtual lpi::gui::Element* getDialog() { return &dialog; }
    virtual std::string getLabel() const { return "Morph Gradient"; }
    virtual std::string getHelp() { return "[OperationMorphologyGradient Help]"; }
};

class OperationMorphologyTopHat : public AOperationFilterUtil
{
  private:
    virtual void doit(TextureRGBA8& texture, Progress& progress);

    bool wrapping;
    bool affect_alpha;

    DynamicFilterPageWithPreview dialog;

  public:
    OperationMorphologyTopHat(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom);
    virtual lpi::gui::Element* getDialog() { return &dialog; }
    virtual std::string getLabel() const { return "Top Hat"; }
    virtual std::string getHelp() { return "[OperationMorphologyTopHat Help]"; }
};



class OperationGameOfLife : public AOperationFilterUtil
{
  private:
    virtual void doit(TextureRGBA8& texture, Progress& progress);
  public:
    OperationGameOfLife(GlobalToolSettings& settings)
    : AOperationFilterUtil(settings, true, true)
    {
    }

    virtual std::string getLabel() const { return "Game Of Life"; }
};

class OperationGameOfLifeExtinguish : public AOperationFilterUtil
{
  private:
    virtual void doit(TextureRGBA8& texture, Progress& progress);
  public:
    OperationGameOfLifeExtinguish(GlobalToolSettings& settings)
    : AOperationFilterUtil(settings, true, true)
    {
    }

    virtual std::string getLabel() const { return "Game Of Life Extinguish"; }
};


class OperationMakeSeamless : public AOperationFilterUtil
{
  private:
    virtual void doit(TextureRGBA8& texture, Progress& progress);
  public:
    OperationMakeSeamless(GlobalToolSettings& settings)
    : AOperationFilterUtil(settings, true, true)
    {
    }

    virtual std::string getLabel() const { return "Make Seamless"; }
};

//generates a very simple two-colored tile pattern
class OperationGenerateRandomLines : public AOperationFilterUtil
{
  protected:
    int amount;
    int minwidth;
    int maxwidth;
    int minlength;
    int maxlength;
    double opacity;
    
    DynamicFilterPageWithPreview dialog;


  public:
    OperationGenerateRandomLines(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom);

    virtual void doit(TextureRGBA8& texture, Progress& progress);
    virtual lpi::gui::Element* getDialog() { return &dialog; }
    virtual std::string getLabel() const { return "Random Lines"; }
    virtual std::string getHelp() { return "[OperationGenerateRandomLines Help]"; }
};

