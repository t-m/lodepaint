/*
LodePaint

Copyright (c) 2009-2018 Lode Vandevenne
All rights reserved.

This file is part of LodePaint.

LodePaint is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LodePaint is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LodePaint.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "lpi/lpi_color.h"
#include "lpi/gui/lpi_gui.h"
#include "lpi/gui/lpi_gui_dynamic.h"
#include "lpi/gui/lpi_gui_file.h"

#include "paint_gui.h"
#include "paint_settings.h"

#include <vector>

#define LODEPAINT_VERSION std::string("20180701")

void waitForProgressBar(MainProgram& p, Progress& progress);

struct Painting
{
  UndoStack undoStack;
  PaintWindow window;
  Paint& paint;
  Options& options;

  Painting(Options& options, lpi::GLContext* context)
  : window(options, context)
  , paint(window.getPaint())
  , options(options)
  {
  }
};


struct Paintings
{
  std::vector<Painting*> paintings;

  Painting* operator[](size_t i) { return paintings[i]; }
  Painting* operator[](size_t i) const { return paintings[i]; }

  Painting* activePainting;

  Paintings()
  : activePainting(0)
  {
  }

  ~Paintings()
  {
    for(size_t i = 0; i < paintings.size(); i++) delete paintings[i];
  }
};

void addPaintWindow(MainProgram& p, Paintings& paintings, Painting* w);

//forward declarations
class ImageFormats;
class MyModalFrameHandler;
namespace lpi
{
  class ScreenGL;
  namespace gui
  {
    class GUIDrawerGL;
  }
}

//resources the main program needs, such as the screen
struct MainProgram
{
  lpi::ScreenGL* screen;
  lpi::gui::GUIDrawerGL* guidrawer;
  lpi::FileBrowse* filebrowser;
  lpi::gui::MainContainer* c;
  lpi::gui::RecentFilesMenu* recentMenu;
  MyModalFrameHandler* modalFrameHandler;
  std::string title;
  Options& optionsNow;
  Options& optionsLater;
  ImageFormats* formats;
  lpi::gui::FileDialogPersist fileDialogPersist;

  ToolWindow toolBox;
  lpi::gui::ToolBar toolbar;
  lpi::gui::ToolBar toolbar2;
  ColorWindow* colorWindow;
  OverviewWindow* overviewWindow;

  MainProgram(int width, int height, bool fullscreen, bool resizable, const std::string& title, Options& optionsNow, Options& optionsLater);
  ~MainProgram();

  void initGuiDrawerStyle();

  void destruct();

  void create(int width, int height, bool fullscreen, bool resizable, const std::string& title);
  void recreateScreen(int width, int height, bool fullscreen, bool resizable);
};

void getStandardPaintingArea(int& x0, int& y0, int& x1, int& y1, const MainProgram& p);
void fitWindowAroundPainting(PaintWindow& window, const MainProgram& p);




