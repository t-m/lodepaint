/* THIS CODE CARRIES NO GUARANTEE OF USABILITY OR FITNESS FOR ANY PURPOSE.
 * WHILE THE AUTHORS HAVE TRIED TO ENSURE THE PROGRAM WORKS CORRECTLY,
 * IT IS STRICTLY USE AT YOUR OWN RISK.  */

#include "rgbe.h"
#include <math.h>
#ifndef __AROS__
#include <malloc.h>
#endif
#include <string>
#include <sstream>
#include <ctype.h>
#include <cstdio>
#include <iostream>

/* This file contains code to read and write four byte rgbe file format
 developed by Greg Ward.  It handles the conversions between rgbe and
 pixels consisting of floats.  The data is assumed to be an array of floats.
 By default there are three floats per pixel in the order red, green, blue.
 (RGBE_DATA_??? values control this.)  Only the mimimal header reading and 
 writing is implemented.  Each routine does error checking and will return
 a status value as defined below.  This code is intended as a skeleton so
 feel free to modify it to suit your needs.

 (Place notice here if you modified the code.)
 posted to http://www.graphics.cornell.edu/~bjw/
 written by Bruce Walter  (bjw@graphics.cornell.edu)  5/26/95
 based on code written by Greg Ward
 
 Edited by Lode Vandevenne on 15 January 2010, to convert it to C++ and to be
 able to work in-memory by using std::vector instead of FILE. Also allowed
 things like EXPOSURE to come after FORMAT to be more permissive to various
 files existing on the web.
*/

/* offsets to red, green, and blue components in a data (float) pixel */
#define RGBE_DATA_RED    0
#define RGBE_DATA_GREEN  1
#define RGBE_DATA_BLUE   2
/* number of floats per pixel */
#define RGBE_DATA_SIZE   3

enum rgbe_error_codes {
  rgbe_read_error,
  rgbe_write_error,
  rgbe_format_error,
  rgbe_memory_error
};

/* default error routine.  change this to change error handling */
static int rgbe_error(std::string& error, int rgbe_error_code, const char* msg)
{
  switch (rgbe_error_code) {
  case rgbe_read_error:
    error = "RGBE read error";
    break;
  case rgbe_write_error:
    error = "RGBE write error";
    break;
  case rgbe_format_error:
    error = std::string("RGBE bad file format: ") + msg;
    break;
  default:
  case rgbe_memory_error:
    error = std::string("RGBE error: ") + msg;
  }
  return RGBE_RETURN_FAILURE;
}

/* standard conversion from float pixels to rgbe pixels */
/* note: you can remove the "inline"s if your compiler complains about it */
static inline void 
float2rgbe(unsigned char rgbe[4], float red, float green, float blue)
{
  float v;
  int e;

  v = red;
  if (green > v) v = green;
  if (blue > v) v = blue;
  if (v < 1e-32) {
    rgbe[0] = rgbe[1] = rgbe[2] = rgbe[3] = 0;
  }
  else {
    v = frexp(v,&e) * 256.0/v;
    rgbe[0] = (unsigned char) (red * v);
    rgbe[1] = (unsigned char) (green * v);
    rgbe[2] = (unsigned char) (blue * v);
    rgbe[3] = (unsigned char) (e + 128);
  }
}

/* standard conversion from rgbe to float pixels */
/* note: Ward uses ldexp(col+0.5,exp-(128+8)).  However we wanted pixels */
/*       in the range [0,1] to map back into the range [0,1].            */
static inline void 
rgbe2float(float *red, float *green, float *blue, unsigned char rgbe[4])
{
  float f;

  if (rgbe[3]) {   /*nonzero pixel*/
    f = ldexp(1.0,rgbe[3]-(int)(128+8));
    *red = rgbe[0] * f;
    *green = rgbe[1] * f;
    *blue = rgbe[2] * f;
  }
  else
    *red = *green = *blue = 0.0;
}

static void fprintfToVector(std::vector<unsigned char>& v, const std::string& format)
{
  for(size_t i = 0; i < format.size(); i++) v.push_back(format[i]);
}

template<typename T>
void fprintfToVector(std::vector<unsigned char>& v, const std::string& format, const T& t)
{
  char buffer[80];
  int n = std::sprintf(buffer, format.c_str(), t);
  for(int i = 0; i < n; i++) v.push_back(buffer[i]);
}

template<typename T, typename U>
void fprintfToVector(std::vector<unsigned char>& v, const std::string& format, const T& t, const U& u)
{
  char buffer[80];
  int n = std::sprintf(buffer, format.c_str(), t, u);
  for(int i = 0; i < n; i++) v.push_back(buffer[i]);
}

/* default minimal header. modify if you want more information in header */
int RGBE_WriteHeader(std::string& error, std::vector<unsigned char>& file, int width, int height, const rgbe_header_info& info)
{
  (void)error;
  std::string programtype = "RGBE";

  if (info.valid & RGBE_VALID_PROGRAMTYPE)
    programtype = info.programtype;
  fprintfToVector(file, "#?%s\n",programtype.c_str());
  /* The #? is to identify file type, the programtype is optional. */
  if (info.valid & RGBE_VALID_GAMMA) {
    fprintfToVector(file, "GAMMA=%g\n",info.gamma);
  }
  if (info.valid & RGBE_VALID_EXPOSURE) {
    fprintfToVector(file, "EXPOSURE=%g\n",info.exposure);
  }
  fprintfToVector(file, "FORMAT=32-bit_rle_rgbe\n\n");
  fprintfToVector(file, "-Y %d +X %d\n", height, width);
  return RGBE_RETURN_SUCCESS;
}

/* minimal header reading.  modify if you want to parse more information */
int RGBE_ReadHeader(std::string& error, int& width, int& height, rgbe_header_info& info, size_t& index, const unsigned char* file, const size_t filesize)
{
  char buf[128];
  float tempf;

  info.valid = 0;
  info.programtype = "";
  info.gamma = info.exposure = 1.0;

  for(size_t i = 0; i < sizeof(buf)/sizeof(buf[0]); i++)
  {
    if(index >= filesize) return rgbe_error(error, rgbe_read_error,NULL);
    buf[i] = file[index++];
    if(buf[i] == '\n') { buf[i+1] = 0; break; }
  }
  if ((buf[0] != '#')||(buf[1] != '?')) {
    /* if you want to require the magic token then uncomment the next line */
    return rgbe_error(error, rgbe_format_error,"bad initial token");
  }
  else
  {
    info.valid |= RGBE_VALID_PROGRAMTYPE;
    for(int i=0;i<(int)(sizeof(info.programtype)-1);i++) {
      if ((buf[i+2] == 0) || isspace(buf[i+2]))
        break;
      info.programtype += buf[i+2];
    }
  }

  for(size_t i = 0; i < sizeof(buf)/sizeof(buf[0]); i++)
  {
    if(index >= filesize) return rgbe_error(error, rgbe_read_error,NULL);
    buf[i] = file[index++];
    if(buf[i] == '\n') { buf[i+1] = 0; break; }
  }

  bool format_found = false;
  for(;;) {
    if ((buf[0] == 0)||(buf[0] == '\n'))
      break;
    else if(buf[0] == '\n')
      break; //blank line at end means the header part is over
    else if(std::string(buf) == "FORMAT=32-bit_rle_rgbe\n")
      format_found = true;
    else if ((std::sscanf(buf,"GAMMA=%g",&tempf) == 1)) {
      info.gamma = tempf;
      info.valid |= RGBE_VALID_GAMMA;
    }
    else if ((std::sscanf(buf,"EXPOSURE=%g",&tempf) == 1)) {
      info.exposure = tempf;
      info.valid |= RGBE_VALID_EXPOSURE;
    }
    for(size_t i = 0; i < sizeof(buf)/sizeof(buf[0]); i++)
    {
      if(index >= filesize) return rgbe_error(error, rgbe_read_error,NULL);
      buf[i] = file[index++];
      if(buf[i] == '\n') { buf[i+1] = 0; break; }
    }
  }
  if(!format_found) return rgbe_error(error, rgbe_format_error,"no FORMAT specifier found");
  //for(size_t i = 0; i < sizeof(buf)/sizeof(buf[0]); i++)
  //{
    //if(index >= filesize) return rgbe_error(error, rgbe_read_error,NULL);
    //buf[i] = file[index++];
    //if(buf[i] == '\n') { buf[i+1] = 0; break; }
  //}
  //if(buf[0] != '\n')
  //{
    ///*return rgbe_error(error, rgbe_format_error,
                      //"missing blank line after FORMAT specifier");*/
  //}
  else for(size_t i = 0; i < sizeof(buf)/sizeof(buf[0]); i++)
  {
    if(index >= filesize) return rgbe_error(error, rgbe_read_error,NULL);
    buf[i] = file[index++];
    if(buf[i] == '\n') { buf[i+1] = 0; break; }
  }
  if (std::sscanf(buf,"-Y %d +X %d",&height,&width) < 2)
    return rgbe_error(error, rgbe_format_error,"missing image size specifier");
  return RGBE_RETURN_SUCCESS;
}

/* simple write routine that does not use run length encoding */
/* These routines can be made faster by allocating a larger buffer and
   fread-ing and fwrite-ing the data in larger chunks */
int RGBE_WritePixels(std::string& error, std::vector<unsigned char>& file, const float* data, int numpixels)
{
  (void)error;
  unsigned char rgbe[4];

  while (numpixels-- > 0) {
    float2rgbe(rgbe,data[RGBE_DATA_RED],
               data[RGBE_DATA_GREEN],data[RGBE_DATA_BLUE]);
    data += RGBE_DATA_SIZE;
    file.push_back(rgbe[0]);
    file.push_back(rgbe[1]);
    file.push_back(rgbe[2]);
    file.push_back(rgbe[3]);
  }
  return RGBE_RETURN_SUCCESS;
}

/* simple read routine.  will not correctly handle run length encoding */
int RGBE_ReadPixels(std::string& error, float* data, int numpixels, size_t& index, const unsigned char* file, const size_t filesize)
{
  (void)error;
  unsigned char rgbe[4];

  while(numpixels-- > 0) {
    if(filesize - index < 4)
      return rgbe_error(error, rgbe_read_error,NULL);
    rgbe[0] = file[index++];
    rgbe[1] = file[index++];
    rgbe[2] = file[index++];
    rgbe[3] = file[index++];
    
    rgbe2float(&data[RGBE_DATA_RED],&data[RGBE_DATA_GREEN],
               &data[RGBE_DATA_BLUE],rgbe);
    data += RGBE_DATA_SIZE;
  }
  return RGBE_RETURN_SUCCESS;
}

/* The code below is only needed for the run-length encoded files. */
/* Run length encoding adds considerable complexity but does */
/* save some space.  For each scanline, each channel (r,g,b,e) is */
/* encoded separately for better compression. */

static int RGBE_WriteBytes_RLE(std::string& error, std::vector<unsigned char>& file, const unsigned char* data, int numbytes)
{
  (void)error;
#define MINRUNLENGTH 4
  int cur, beg_run, run_count, old_run_count, nonrun_count;

  cur = 0;
  while(cur < numbytes) {
    beg_run = cur;
    /* find next run of length at least 4 if one exists */
    run_count = old_run_count = 0;
    while((run_count < MINRUNLENGTH) && (beg_run < numbytes)) {
      beg_run += run_count;
      old_run_count = run_count;
      run_count = 1;
      while( (beg_run + run_count < numbytes) && (run_count < 127)
             && (data[beg_run] == data[beg_run + run_count]))
        run_count++;
    }
    /* if data before next big run is a short run then write it as such */
    if ((old_run_count > 1)&&(old_run_count == beg_run - cur)) {
      file.push_back(128 + old_run_count);   /*write short run*/
      file.push_back(data[cur]);
      cur = beg_run;
    }
    /* write out bytes until we reach the start of the next run */
    while(cur < beg_run) {
      nonrun_count = beg_run - cur;
      if (nonrun_count > 128) 
        nonrun_count = 128;
      file.push_back(nonrun_count);
      size_t size = sizeof(data[0])*nonrun_count;
      for(size_t i = 0; i < size; i++) file.push_back(data[cur + i]);
      cur += nonrun_count;
    }
    /* write out next run if one was found */
    if (run_count >= MINRUNLENGTH) {
      file.push_back(128 + run_count);
      file.push_back(data[beg_run]);
      cur += run_count;
    }
  }
  return RGBE_RETURN_SUCCESS;
#undef MINRUNLENGTH
}

int RGBE_WritePixels_RLE(std::string& error, std::vector<unsigned char>& file, const float* data, int scanline_width,
                         int num_scanlines)
{
  unsigned char rgbe[4];
  unsigned char *buffer;
  int i, err;

  if ((scanline_width < 8)||(scanline_width > 0x7fff))
    /* run length encoding is not allowed so write flat*/
    return RGBE_WritePixels(error, file,data,scanline_width*num_scanlines);
  buffer = (unsigned char *)malloc(sizeof(unsigned char)*4*scanline_width);
  if (buffer == NULL) 
    /* no buffer space so write flat */
    return RGBE_WritePixels(error, file,data,scanline_width*num_scanlines);
  while(num_scanlines-- > 0) {
    rgbe[0] = 2;
    rgbe[1] = 2;
    rgbe[2] = scanline_width >> 8;
    rgbe[3] = scanline_width & 0xFF;
    file.push_back(rgbe[0]); file.push_back(rgbe[1]); file.push_back(rgbe[2]); file.push_back(rgbe[3]); 

    for(i=0;i<scanline_width;i++) {
      float2rgbe(rgbe,data[RGBE_DATA_RED],
                 data[RGBE_DATA_GREEN],data[RGBE_DATA_BLUE]);
      buffer[i] = rgbe[0];
      buffer[i+scanline_width] = rgbe[1];
      buffer[i+2*scanline_width] = rgbe[2];
      buffer[i+3*scanline_width] = rgbe[3];
      data += RGBE_DATA_SIZE;
    }
    /* write out each of the four channels separately run length encoded */
    /* first red, then green, then blue, then exponent */
    for(i=0;i<4;i++) {
      if ((err = RGBE_WriteBytes_RLE(error, file,&buffer[i*scanline_width],
                                     scanline_width)) != RGBE_RETURN_SUCCESS) {
        free(buffer);
        return err;
      }
    }
  }
  free(buffer);
  return RGBE_RETURN_SUCCESS;
}
      
int RGBE_ReadPixels_RLE(std::string& error, float* data, int scanline_width,
                        int num_scanlines, size_t& index, const unsigned char* file, const size_t filesize)
{
  unsigned char rgbe[4], *scanline_buffer, *ptr, *ptr_end;
  int i, count;
  unsigned char buf[2];

  if ((scanline_width < 8)||(scanline_width > 0x7fff))
    /* run length encoding is not allowed so read flat*/
    return RGBE_ReadPixels(error, data,scanline_width*num_scanlines, index, file, filesize);
  scanline_buffer = NULL;
  /* read in each successive scanline */
  while(num_scanlines > 0) {
    if(filesize - index < 4) {
      free(scanline_buffer);
      return rgbe_error(error, rgbe_read_error,NULL);
    }
    rgbe[0] = file[index++]; rgbe[1] = file[index++]; rgbe[2] = file[index++]; rgbe[3] = file[index++]; 
    if ((rgbe[0] != 2)||(rgbe[1] != 2)||(rgbe[2] & 0x80)) {
      /* this file is not run length encoded */
      rgbe2float(&data[0],&data[1],&data[2],rgbe);
      data += RGBE_DATA_SIZE;
      free(scanline_buffer);
      return RGBE_ReadPixels(error, data,scanline_width*num_scanlines-1,index, file, filesize);
    }
    if ((((int)rgbe[2])<<8 | rgbe[3]) != scanline_width) {
      free(scanline_buffer);
      return rgbe_error(error, rgbe_format_error,"wrong scanline width");
    }
    if (scanline_buffer == NULL)
      scanline_buffer = (unsigned char *)
        malloc(sizeof(unsigned char)*4*scanline_width);
    if (scanline_buffer == NULL) 
      return rgbe_error(error, rgbe_memory_error,"unable to allocate buffer space");
    
    ptr = &scanline_buffer[0];
    /* read each of the four channels for the scanline into the buffer */
    for(i=0;i<4;i++) {
      ptr_end = &scanline_buffer[(i+1)*scanline_width];
      while(ptr < ptr_end) {
        if(filesize - index < 2) {
          free(scanline_buffer);
          return rgbe_error(error, rgbe_read_error,NULL);
        }
        buf[0] = file[index++]; buf[1] = file[index++]; 
        if (buf[0] > 128) {
          /* a run of the same value */
          count = buf[0]-128;
          if ((count == 0)||(count > ptr_end - ptr)) {
            free(scanline_buffer);
            return rgbe_error(error, rgbe_format_error,"bad scanline data");
          }
          while(count-- > 0)
            *ptr++ = buf[1];
        }
        else {
          /* a non-run */
          count = buf[0];
          if ((count == 0)||(count > ptr_end - ptr)) {
            free(scanline_buffer);
            return rgbe_error(error, rgbe_format_error,"bad scanline data");
          }
          *ptr++ = buf[1];
          if (--count > 0) {
            size_t size = sizeof(*ptr)*count;
            if(filesize - index < size) {
              free(scanline_buffer);
              return rgbe_error(error, rgbe_read_error,NULL);
            }
            for(size_t i = 0; i < size; i++) ptr[i] = file[index++];
            ptr += count;
          }
        }
      }
    }
    /* now convert data from buffer into floats */
    for(i=0;i<scanline_width;i++) {
      rgbe[0] = scanline_buffer[i];
      rgbe[1] = scanline_buffer[i+scanline_width];
      rgbe[2] = scanline_buffer[i+2*scanline_width];
      rgbe[3] = scanline_buffer[i+3*scanline_width];
      rgbe2float(&data[RGBE_DATA_RED],&data[RGBE_DATA_GREEN],
                 &data[RGBE_DATA_BLUE],rgbe);
      data += RGBE_DATA_SIZE;
    }
    num_scanlines--;
  }
  free(scanline_buffer);
  return RGBE_RETURN_SUCCESS;
}

