
// ****************************************************************************
//
// WINIMAGE.H : Generic classes for raster images (MSWindows specialization)
//
//  Content: Class declarations of:
//  - class C_Image             : Storage class for single images
//  - class C_ImageSet          : Storage class for sets of images
//  - class C_AnimationWindow   : Window Class to display animations
//
//  (Includes declarations of routines to Load and Save BMP files and to load
// GIF files into these classes).
//
//  --------------------------------------------------------------------------
//
// Copyright (c) 2000, Juan Soulie <jsoulie@cplusplus.com>
//
// Permission to use, copy, modify, distribute and sell this software or any
// part thereof and/or its documentation for any purpose is granted without fee
// provided that the above copyright notice and this permission notice appear
// in all copies.
//
// This software is provided "as is" without express or implied warranty of
// any kind. The author shall have no liability with respect to the
// infringement of copyrights or patents that any modification to the content
// of this file or this file itself may incur.
//
// ****************************************************************************
//


/*
Modified for LodePaint to remove Win32 dependencies and work for LodePaint.
Only GIF has been used and tested and used, the BMP portion of the code is also
made multiplatform, but has never actually been run.

There were various bugs in the original winimage.cpp and winimage.h GIF loader. These bugs
have been discussed in a forum thread on codeproject.com, and the relevant portions of that
forum thread have been copypasted at the bottom of this header file.

I didn't fix all things mentioned in the thread, I only fixed up to the point where all my test
gifs worked. Some more mentioned fixed actually broke some GIFs.

The fixes done were:

-replacing the NextEntry>=4096 by NextEntry>4096
-replacing the 0x08 by 0x80
-the reading of LocalColorMap with 3 instead of 4 values per color
-replaced the "Transparency" variable by "DisposalMethod", because the Transparency flag didn't give enough
 info to handle some gifs that were both animated and transparent correctly.

In addition to the fixes mentioned in the forum thread at the bottom, I added the following fixes:

-removed all Win32 dependencies and made it compile with g++, so that it's multiplatform.
-made the code compatible with both little endian and big endian machines, so it works for AmigaOS4.
*/



#pragma once

#include <fstream>
#include <string>

// Windows specific types and constants:

struct COLOR {unsigned char b,g,r,x;};  // Windows GDI expects 4bytes per color

struct BitmapInfoHeader {
  unsigned long  biSize;
           long  biWidth;
           long  biHeight;
  unsigned short biPlanes;
  unsigned short biBitCount;
  unsigned long  biCompression;
  unsigned long  biSizeImage;
           long  biXPelsPerMeter;
           long  biYPelsPerMeter;
  unsigned long  biClrUsed;
  unsigned long  biClrImportant;
};


struct BitmapInfo {
  BitmapInfoHeader bmiHeader;
  COLOR bmiColors[1];
};


// ****************************************************************************
// * C_Image                                                                  *
// *    Storage class for single images                                       *
// ****************************************************************************
class C_Image {
public:
  // standard members:
  int Width, Height;      // Dimensions in pixels
  int BPP;          // Bits Per Pixel  
  char * Raster;        // Bits of Raster Data (Byte Aligned)
  COLOR * Palette;      // Color Map
  int BytesPerRow;      // Width (in bytes) including alignment!
  int Transparent;      // Index of Transparent color (-1 for none)
  // Extra members for animations:
  int xPos, yPos;        // Relative Position
  int Delay;          // Delay after image in 1/1000 seconds.
  int DisposalMethod; //What to do with previous frame (when drawing transparent color over it): 0:nothing, 1:leave previous frame, 2:bgcolor, 3:turn into fully transparent pixel (that is, bring back what was on screen before the first gif frame was rendered)
  
  
  // Windows GDI specific:
  BitmapInfo * pbmi;      // BITMAPINFO structure

  // constructor and destructor:
  C_Image() { Raster=0; Palette=0; pbmi=0; }
  ~C_Image() { delete[]pbmi; delete[]Raster; }

  // operator= (object copy)
  C_Image& operator= (C_Image& rhs);

  // Image initializer (allocates space for raster and palette):
  void Init (int iWidth, int iHeight, int iBPP);

  inline char& Pixel (const int& x, const int& y)
    {return Raster[y*BytesPerRow+x];}

  // File Formats:
  int LoadBMP (std::string& error, std::istream& bmpfile);
  int SaveBMP (std::string& error, std::ostream& bmpfile);
};

// ****************************************************************************
// * C_ImageSet                                                               *
// *    Storage class for sets of images                                      *
// ****************************************************************************
class C_ImageSet {
public:
  int FrameWidth, FrameHeight;  // Dimensions of ImageSet in pixels.
  int nLoops;            // Number of Loops (0 = infinite)

  C_Image ** img;        // Images' Vector.
  int nImages;          // Number of images (vector size)

  void AddImage (C_Image*);    // Append new image to vector (push_back)

  // constructor and destructor:
  C_ImageSet() {img=0; nImages=0; nLoops=0;}
  ~C_ImageSet() {for (int n=0;n<nImages;n++) delete img[n]; delete[] img;}

  // File Formats:
  int LoadGIF (std::string& error, std::istream& giffile);
};



/*
*** Excerpts from Codeprojects.com forum thread about fixing some bugs in winimage.cpp ***

hello,

There was a problem with gif files having local color palettes.
some corrections in winimage.cpp :
- line 631 under giffile.read(...
//& 0x80 instead of & 0x08
int LocalColorMap = (gifid.PackedFields & 0x80)? 1 : 0;
- line 651
if (LocalColorMap) // Read Color Map (if descriptor says so)
{
//giffile.read((char*)NextImage->Palette,
// sizeof(COLOR)*(1<<NextImage->BPP));
//local table made of 3 bytes blocks (not 4)
int nb=(1<<NextImage->BPP);
for (int i=0; i
giffile.read((char*)&NextImage->Palette[i], 3);
}
- (unrelated and noted too by dywcp to avoid leaks) line 712 (under giffile.close() for example) add the line :
delete GlobalColorMap;

Hoping this helped,
Raymond



The line 651 correction should be :
{
int nb=(1<<NextImage->BPP);
for (int i=0; i
giffile.read((char*)&NextImage->Palette[i], 3);
}



replace this (again! sorry..) by :
{
   int nb=(1<<NextImage->BPP);
   for (int i=0; i<nb; i++)
   {
      NextImage->Palette[i].r=giffile.get();
      NextImage->Palette[i].g=giffile.get();
      NextImage->Palette[i].b=giffile.get();
   }
}



Well yes I noticed further problems in winimage.cpp :
- first a minor error is (I think) : in   LZWDecoder() under // Prevent Translation table overflow:   replace NextEntry>=4096 by NextEntry>4096
(only one of my gif files affected)

- I'll suppose that you used the code provided with kingdomkao's help here after addition of my last correction :
{
   int nb=(1<<NextImage->BPP);
   for (int i=0; i<nb; i++)
   {
      NextImage->Palette[i].r=giffile.get();
      NextImage->Palette[i].g=giffile.get();
      NextImage->Palette[i].b=giffile.get();
   }
}

The problem I noticed with this correction (sorry kingdomkao...) is that it handled only part of the transparence problems.

I added a [int DisposalMethod] in C_Image and changed the code around line 660 to initialize this field :
if (GraphicExtensionFound)
{
   ...
   NextImage->Delay = gifgce.Delay*10;
   NextImage->DisposalMethod= (gifgce.PackedFields >> 2) & 7;
}
else
   NextImage->DisposalMethod= 0;

DisposalMethod may have values between 0 and 3 (in practice 4..7 unused)
0   //no transparence
1   //leave
2   //restore background
3   //restore previous
This field indicates what must be kept from the previous image (and so on..) before drawing the new one and doesn't seem to be tested by   Kingdomkao's code.
For the 1 (leave image) case one should have to work relatively to the last generated one instead of the first as done in ApplyTransparency.

Concerning the 2 value perhaps that it could be handled by reading the giflsd.Background field after the line     GlobalBPP = (giflsd.PackedFields & 0x07) + 1;
but I didn't need that yet (I think it's not used by html browsers).

My correction to winimage was to initialize the DisposalMethod as shown and not to call the ApplyTransparency but handle the DisposalMethod field in my exterior (and unfortunately much to heavy and dispersed to provide here Frown ) code. The problem is that the colormap may be changed for every bitmap so that we should have to work in a 24 bit buffer as is done by browsers (I use a DDB buffer). I don't know of further problems in winimage.cpp (I mean that the problem is in using it to generate the final animations).

I'll give you only a much simplified (and not at all tested..) version of my code for the 1 case :

pIm is the C_Image *
hDC is the destination DC where the previous bitmap is selected

if (opaque)
   SetDIBitsToDevice(hDC, pIm->xPos, pIm->yPos, pIm->Width,pIm->Height,0,0,0,pIm->Height,pIm->Raster,pIm->pbmi,0);
else //must be linked with msimg32.lib for NT5 and more
{
   //with a SelectPalette RealizePalette in my case
HBITMAP hSrc= CreateDIBitmap (hDC,pIm->pbmi,
   CBM_INIT,pIm->Raster,pIm->pbmi,DIB_RGB_COLORS);
if (hSrc)
{
   HDC hdcSrc;
   if (hdcSrc=CreateCompatibleDC(hDC))
   {
         HBITMAP hOldB = (HBITMAP) ::SelectObject(hdcSrc, hSrc);
         TransparentBlt(hDC,pIm->xPos, pIm->yPos, pIm->Width, pIm->Height, hdcSrc, 0, 0, pIm->Width, pIm->Height, RGBTransLoc);
         if (hOldB)
            ::SelectObject(hdcSrc, hOldB);
         DeleteDC(hdcSrc);
   }
   DeleteObject(hSrc);
}

To much technical details and no simple correcting code... sorry for that.
   Hoping it helped a little anyway at least to make the problem clear,
               Raymond











I corrected some bugs of winimage.cpp earlier and part of these corrections are in the winimage.cpp file contained in AnimationGifEx_src.zip

The missing parts :

- the replacement in line 651 of

{
int nb=(1<<NextImage->BPP);
for (int i=0; i
giffile.read((char*)&NextImage->Palette[i], 3);
}

by

{
   int nb=(1<<NextImage->BPP);
   for (int i=0; i<nb; i++)
   {
      NextImage->Palette[i].r=giffile.get();
      NextImage->Palette[i].g=giffile.get();
      NextImage->Palette[i].b=giffile.get();
   }
}

- in   LZWDecoder() under // Prevent Translation table overflow:   replace   NextEntry>=4096 by NextEntry>4096

this was provided here :
http://www.codeproject.com/staticctrl/gifanimation.asp?df=100&forumid=15674&exp=0&fr=26&select=775384#xx775384xx

I try to explain some of the remaining problems here :
http://www.codeproject.com/staticctrl/gifanimation.asp?df=100&forumid=15674&exp=0&fr=26&select=816543#xx816543xx

Hoping it helped a little,

Raymond







Oups sorry... I should have read the first message!
I looked at the gif file and observed the problem at the right eye in the first frame (2 white instead of black points).

Rage seems to have deformed this kind looking guy to a point...
Well I'll throw an eye on this eye too!!                  Wink

Raymond

-- modified at 20:21 Friday 9th June, 2006      (long hours later...)


Here are some modifications for the int LZWDecoder (supposed in line 767) function :

- in line 784 (under short OutCode) insert the line :     short PrevOutCode;                    // Previous OutCode

- in line 800 (over EndCode = ClearCode + 1;) initialize PrevOutCode and PrevCode :
      PrevOutCode = PrevCode = ClearCode;   //to avoid crashes with GIFs without ClearCode at the beginning

- in line 828 replace   PrevCode=Code;   by :
   PrevOutCode= PrevCode= Code;

- in line 835 replace the faulty line (I think this was the problem for this thread!) :
      OutIndex++;               // Keep "first" character of previous output.
   by (in fact the stack is reversed so that it was the last character!!) :
      OutStack[OutIndex++] = (unsigned char) PrevOutCode; // Keep "first" character of previous output.

- in line 851 replace the line :
      OutStack[OutIndex++] = (unsigned char) OutCode;
   by
      OutStack[OutIndex++] = (unsigned char) (PrevOutCode= OutCode); //save the first character


-- modified at 5:04 Monday 12th June, 2006

I only modified two lines to avoid warnings and the faulty gif's first frame (and winimage.cpp) should be fine now.
Will the animation work without flaw for this gif? Well, not without proper implementation of the 'disposal method' as I explained previously (http://www.codeproject.com/staticctrl/gifanimation.asp?df=100&forumid=15674&exp=0&fr=26&select=816543#xx816543xx
) because this gif uses the '1:leave' method (keep the previous frame and draw over it) as well as the '2:restore background' method that isn't used often (erase the previously drawn image (not the frame!) with the background color and draw over it) (I didn't need the '3:restore previous' yet). With this in place the animation is perfect.

Great implementations!
                              Raymond


*/
