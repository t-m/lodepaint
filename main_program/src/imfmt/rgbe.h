#ifndef _H_RGBE
#define _H_RGBE
/* THIS CODE CARRIES NO GUARANTEE OF USABILITY OR FITNESS FOR ANY PURPOSE.
 * WHILE THE AUTHORS HAVE TRIED TO ENSURE THE PROGRAM WORKS CORRECTLY,
 * IT IS STRICTLY USE AT YOUR OWN RISK.  */

/* utility for reading and writing Ward's rgbe image format.
   See rgbe.txt file for more details.
*/

#include <vector>
#include <string>

struct rgbe_header_info {
  int valid;            /* indicate which fields are valid */
  std::string programtype; /* listed at beginning of file to identify it 
                         * after "#?".  defaults to "RGBE" */ 
  float gamma;          /* image has already been gamma corrected with 
                         * given gamma.  defaults to 1.0 (no correction) */
  float exposure;       /* a value of 1.0 in an image corresponds to
                         * <exposure> watts/steradian/m^2. 
                         * defaults to 1.0 */
  rgbe_header_info()
  : valid(0)
  , programtype("RGBE")
  , gamma(1.0f)
  , exposure(1.0f)
  {
  }
};

/* flags indicating which fields in an rgbe_header_info are valid */
#define RGBE_VALID_PROGRAMTYPE 0x01
#define RGBE_VALID_GAMMA       0x02
#define RGBE_VALID_EXPOSURE    0x04

/* return codes for rgbe routines */
#define RGBE_RETURN_SUCCESS 0
#define RGBE_RETURN_FAILURE -1

/* read or write headers */
/* you may set rgbe_header_info to null if you want to */
int RGBE_WriteHeader(std::string& error, std::vector<unsigned char>& file, int width, int height, const rgbe_header_info& info);
int RGBE_ReadHeader(std::string& error, int& width, int& height, rgbe_header_info& info, size_t& index, const unsigned char* file, const size_t filesize);

/* read or write pixels */
/* can read or write pixels in chunks of any size including single pixels*/
int RGBE_WritePixels(std::string& error, std::vector<unsigned char>& file, const float* data, int numpixels);
int RGBE_ReadPixels(std::string& error, float* data, int numpixels, size_t& index, const unsigned char* file, const size_t filesize);

/* read or write run length encoded files */
/* must be called to read or write whole scanlines */
int RGBE_WritePixels_RLE(std::string& error, std::vector<unsigned char>& file, const float* data, int scanline_width,
                        int num_scanlines);
int RGBE_ReadPixels_RLE(std::string& error, float* data, int scanline_width,
                        int num_scanlines, size_t& index, const unsigned char* file, const size_t filesize);

#endif /* _H_RGBE */



