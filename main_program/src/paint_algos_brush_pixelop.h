/*
LodePaint

Copyright (c) 2009 Lode Vandevenne
All rights reserved.

This file is part of LodePaint.

LodePaint is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LodePaint is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LodePaint.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once


#include "lpi/lpi_color.h"

#include "paint_settings.h"


class IPixelOperation
{
  public:
    virtual ~IPixelOperation(){}

    virtual void operate(unsigned char& r, unsigned char& g, unsigned char& b, unsigned char& a, double opacity) const = 0;
};


void drawBrushShape(unsigned char* buffer, int buffer_w, int buffer_h, int xc, int yc, const Brush& brush, const IPixelOperation& op);
