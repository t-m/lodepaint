/*
    LodePaint save requester/file format selector

    Requirements:
    window.class, layout.gadget, getfile.gadget, chooser.gadget,
    button.gadget, checkbox.gadget, label.image

 */

#define __USE_INLINE__ 1

#include <stdio.h>
#include <string.h>
#include <strings.h>

#include <exec/exec.h>
#include <intuition/intuition.h>
#include <libraries/keymap.h>
#include <libraries/gadtools.h>

#include <classes/window.h>
#include <classes/requester.h>
#include <gadgets/layout.h>
#include <gadgets/getfile.h>
#include <gadgets/chooser.h>
#include <gadgets/button.h>
#include <gadgets/checkbox.h>
#include <images/label.h>

#include <proto/exec.h>
#include <proto/intuition.h>
#include <proto/window.h>
#include <proto/requester.h>
#include <proto/layout.h>
#include <proto/getfile.h>
#include <proto/chooser.h>
#include <proto/button.h>
#include <proto/checkbox.h>
#include <proto/label.h>

#include <proto/asl.h>
#include <proto/dos.h>

#include <reaction/reaction_macros.h>

//Sorry for the C++ instead of C code ;)
#include <vector>
#include <string>

BOOL add_amiga_auto_extenstion = FALSE;





/*  gadget identifiers (for IDCMP event handling))
 */
enum
{
 GID_GETFILE,    // file path + name getfile gadget
 GID_FORMATS,    // file format chooser gadget
 GID_EXTENSION,  // add extension checkbhox gadget
 GID_SAVE,       // save button
 GID_CANCEL,     // cancel button
 GID_LAST        // dummy id (required)
};


/*  global variables
 */
Object               *window_object;
Object               *requester_object;
Object               *gadgets[GID_LAST];
struct List          list_formats;
char  filename_amiga[2048];  // file name with complete path
int32 format;          // file format identifier (see above)
BOOL chancel_pressed = FALSE;


/*  function prototypes
 */
void handle_input(Object *);


/************************************************************************************************/


BOOL make_chooser_list(struct List *list, const std::vector<std::string>& labels)
{
/*
    Creates an exec list of chooser labels.

    Inputs:
    list - a pointer to a global struct List
    labels - a pointer to an array of labels

    Result:
    TRUE or FALSE

 */

 struct Node *node;

 if (list)
  {
   NewList(list);

   for(size_t i = 0; i < labels.size(); i++)
    {
     if (node = AllocChooserNode(
                 CNA_Text, labels[i].c_str(),
                TAG_DONE))
      {
       AddTail(list, node);
      }
     else return FALSE;
    }
   return TRUE;
  }
 else return FALSE;

} /* end of make_lb_list() */





void free_chooser_list(struct List *list)
{
/*
    Frees an exec list of chooser labels.
    It is OK to pass an empty list with no labels.
    The individual list nodes must be Remove()'d
    AND freed by a call to FreeListBrowserNode().

    Input:
    list - a pointer to a global struct List

    Result:
    -

 */
 struct Node *node, *nextnode;

 if (list)
  {
   node = GetHead(list);
   while (node)
    {
     nextnode = GetSucc(node);
     Remove(node);
     FreeChooserNode(node);
     node = nextnode;
    }
   NewList(list);
  }

} /* end of free_chooser_list() */





BOOL do_save_request(CONST_STRPTR def_filename, int32 def_format, BOOL def_ext, const std::vector<std::string>& extensionTitles)
{
/*
    Creates and opens the requester/selector.

    Inputs:
    def_filename - default file name (optional; can be NULL if not needed)
    def_format   - default file format (one of the FMT_??? identifiers)
    def_ext      - add extension automatically? (TRUE or FALSE)

    Result:
    TRUE is all went well
    FALSE if the requester GUI failed to create

    Example call:
    do_save_request(NULL, FMT_JPG, TRUE);

 */

 struct Window *window = NULL;

 /* initialize result values */
 sprintf(filename_amiga, def_filename);
 format = def_format;

 make_chooser_list(&list_formats, extensionTitles);

 /* window object definition */
 if ( window_object = WindowObject,
     WA_Title,           "Save As...",
 	 WA_CloseGadget,     TRUE,
 	 WA_SizeGadget,      FALSE,
 	 WA_DragBar,         TRUE,
     WA_DepthGadget,     TRUE,
     WA_Activate,        TRUE,
     WINDOW_IconifyGadget, FALSE,
     WINDOW_Position,      WPOS_CENTERSCREEN,
	 WINDOW_Layout,        VLayoutObject,
     LAYOUT_SpaceInner,    TRUE,
     LAYOUT_SpaceOuter,    TRUE,
     LAYOUT_DeferLayout,   TRUE,
       LAYOUT_AddChild, gadgets[GID_GETFILE] = GetFileObject,
       GA_ID, GID_GETFILE,
       GA_RelVerify, TRUE,
       GETFILE_TitleText, "Choose a file name:",
       GETFILE_File, filename_amiga,
       GETFILE_DoSaveMode, TRUE,
       GETFILE_RejectIcons, TRUE,
       GETFILE_AllowEmptyFileSelection, TRUE,
       End,
       Label("File:"),
       LAYOUT_AddChild, gadgets[GID_FORMATS] = ChooserObject,
       GA_ID, GID_FORMATS,
       GA_RelVerify, TRUE,
       CHOOSER_Labels, &list_formats,
       CHOOSER_Selected, format,
       End,
       Label("Format:"),
       LAYOUT_AddChild, gadgets[GID_EXTENSION] = CheckBoxObject,
       GA_ID, GID_EXTENSION,
       GA_RelVerify, TRUE,
       GA_Text, "Add file extension automatically?",
       GA_Selected, def_ext,
       CHECKBOX_TextPlace, PLACETEXT_LEFT,
       End,
       LAYOUT_AddChild, HLayoutObject,
       LAYOUT_SpaceOuter,    TRUE,
       LAYOUT_SpaceInner,    TRUE,
       LAYOUT_BevelStyle, BVS_SBAR_VERT,
         LAYOUT_AddChild, gadgets[GID_SAVE] = ButtonObject,
         GA_ID, GID_SAVE,
         GA_RelVerify, TRUE,
         GA_Text, "Save",
         End,
         LAYOUT_AddChild, gadgets[GID_CANCEL] = ButtonObject,
         GA_ID, GID_CANCEL,
         GA_RelVerify, TRUE,
         GA_Text, "Cancel",
         End,
       End, // HLayout
       CHILD_WeightedHeight, 0,
     End, // parent layout
   End) // window object
  {

   /* window object successfully created, now open the requester */
   if (window = RA_OpenWindow(window_object))
    {

     /*  it may be a good idea to set WA_BusyPointer, TRUE
         to the main LodePaint window at this point
      */

     handle_input(window_object);
     IDoMethod(window_object, WM_CLOSE);

     /*  requester closed; set WA_BusyPointer, FALSE
         to the main LodePaint window at this point
      */
    }
   DisposeObject(window_object);
   free_chooser_list(&list_formats);
   if (chancel_pressed == TRUE) { chancel_pressed = FALSE; return FALSE; };
   return TRUE;
  }

 else // error, failed to create window object
  {
   free_chooser_list(&list_formats);
   return FALSE;
  }


} /*end of do_save_request() */







void handle_input(Object *win_obj)
{
/*
    Event loop that handles requester input.
 */

 struct Window   *window = NULL;
 char            *temp_name;
 uint32 attr = 0, sigmask = 0, siggot = 0, result = 0, selection;
 uint16 code = 0;
 BOOL   done = FALSE;

 /* obtain pointers from the window object */
 GetAttrs(win_obj,
                WINDOW_Window,  (uint32 *)&window,
                WINDOW_SigMask, &sigmask,
                TAG_DONE);

 while (!done)
  {
   siggot = Wait(sigmask);

   /* signal: IDCMP */
   while ((result = RA_HandleInput(win_obj, &code)) != WMHI_LASTMSG)
	{
	 switch(result & WMHI_CLASSMASK)
	  {
        case WMHI_CLOSEWINDOW:
          done = TRUE;
        break;

         /*
          *   Keyboard events
          */
        case WMHI_RAWKEY:
         switch (result & WMHI_KEYMASK)
          {
           case RAWKEY_ESC:
            /* do nothing, close the requester */
            chancel_pressed = TRUE;
            done = TRUE;
           break;

          }
        break;


         /*
          *   Gadget events
          */
         case WMHI_GADGETUP:
          switch(result & WMHI_GADGETMASK)
           {

            case GID_GETFILE:
             /* user clicked on the little diskette button = open filerequester */
             IDoMethod(gadgets[GID_GETFILE], GFILE_REQUEST, window);
            break;

            case GID_FORMATS:
             /* obtain selected format from the chooser gadget */
             GetAttr(CHOOSER_Selected, gadgets[GID_FORMATS], (uint32 *)&format);
            break;

            case GID_SAVE:
              /* obtain complete filename from the getfile gadget
                 (will be stored in the global variable "filename" */
              GetAttr(GETFILE_FullFile, gadgets[GID_GETFILE], (uint32 *)&temp_name);
              sprintf(filename_amiga, temp_name);

              /* obtain the current state of the Add Extension checkbox
                 (the local variable "selection" will contain TRUE or FALSE) */
              GetAttr(GA_Selected, gadgets[GID_EXTENSION], (uint32 *)&selection);
              if (selection == TRUE)
               {
                /*  Insert your code that will handle the automatic extension */
                     add_amiga_auto_extenstion = TRUE;

               }
               else {
               add_amiga_auto_extenstion = FALSE;
               }


              /* checks for an empty filename */
              //if (!stricmp(filename_amiga, ""))     
if (!strcmp(filename_amiga, ""))     
               {
                if (requester_object = NewObject(NULL, "requester.class",
                              REQ_Type,  REQTYPE_INFO,
                              REQ_Image, REQIMAGE_ERROR,
                              REQ_TitleText,  "Error",
                              REQ_BodyText,   "You must specify a file name!",
                              REQ_GadgetText, "OK",
                    TAG_END))
                 {
                  SetAttrs(win_obj, WA_BusyPointer, TRUE, TAG_DONE);
                  result = IDoMethod(requester_object, RM_OPENREQ, NULL, window, NULL, TAG_DONE);
                  DisposeObject(requester_object);
                  requester_object = NULL;
                  SetAttrs(win_obj, WA_BusyPointer, FALSE, TAG_DONE);
                  break;
                 }
               }

              /*
                  Make sure that "filename" contains a valid file name.

                 If OK, call your saving routine. You should now have:
                 - full path + filename in the global variable "filename"
                 - format ID in the global variable "format"
                   (you can use this ID to obtain the format label text)
              */
         //    puts(filename_amiga);
        //     puts(extensionTitles[format].c_str());
             done = TRUE;
            break;

            case GID_CANCEL:
             /* do nothing, close the requester */
             done = TRUE;
             chancel_pressed = TRUE;
            break;

           } /* switch */
         break;


      } /* switch */
    } /* while */
  } /* while */


} /* end of handle_input() */


// --------------------------------------------------- for LOAD -------------------------------------------


// --------- Open Library

struct Library                 *AslBase;
struct AslIFace  *IAsl;

BOOL amiga_open_asl_library()
{
   AslBase = OpenLibrary("asl.library", 0);
    if (!AslBase) {
        printf("Error opening asl library\n");
        exit(0);
    };

    IAsl = (struct AslIFace *) GetInterface(AslBase, "main", 1, NULL);

    if (!AslBase && !IAsl) {
        printf("Error opening asl library\n");
        exit(0);
    };

    return TRUE;
}

//---------- Close Library

BOOL amiga_close_asl_library()
{

    if(AslBase && IAsl)  DropInterface((struct Interface *)IAsl);
    if (AslBase)      CloseLibrary(AslBase);
    
   return TRUE;
}


//----------- OpenFile

struct FileRequester *aslfileReq;
char *path = (const char *)("PROGDIR:");
long int file_size;
//const 
FILE *fp;
FILE *fp_open;
char *buffer;
BOOL result = FALSE;




bool amiga_open_file(Paintings& paintings, MainProgram& p)
{

// Get filename
    BOOL result;  


  if ((fp_open = fopen("PROGDIR:config.txt","rb")) == NULL)    
  {
   //  printf ("config.txt file not found, will creates when its need for ASL (for save/load on exit)\n");
     goto skip;
   //  return false;
  }
 fseek (fp_open,0,SEEK_END);
 file_size = ftell(fp_open);
 fseek (fp_open,0,SEEK_SET);
 buffer=(char *)malloc(file_size + 1);
 fread( buffer, 1, file_size, fp_open);

 buffer[file_size] = '\0';


 path= buffer;

 fclose(fp_open);

skip:
p.c->handle(p.guidrawer->getInput());
p.screen->cls();
p.c->draw(*p.guidrawer);
p.screen->redraw();


    // Create the asl file requestor and get a filename
 aslfileReq = (struct FileRequester *) AllocAslRequest (ASL_FileRequest, NULL);

  result = AslRequestTags (aslfileReq,
  ASLFR_InitialDrawer, path, 
  TAG_END);

 free(buffer);

 if (result == FALSE) { FreeAslRequest ( aslfileReq ); return false; };

ULONG len = strlen(aslfileReq->fr_Drawer) + strlen(aslfileReq->fr_File) + 1 + 1;

    char *file = NULL;
    if ( result != FALSE )
    {
        // See if we have both file and drawer
        if ( aslfileReq->fr_Drawer == NULL || aslfileReq->fr_File == NULL )
                     {
			FreeAslRequest ( aslfileReq ); return false; 
		   }

        file = (char *)AllocVec ( len, MEMF_ANY|MEMF_CLEAR );
        if ( ! file )
        {
                  //   { FreeAslRequest ( aslfileReq ); return false; }
        }

       strcpy ( file, aslfileReq->fr_Drawer );
  

        if ( ! AddPart ( file, aslfileReq->fr_File, len) )
        {
            FreeVec ( file );
            file = NULL;
        }
    }

  path = aslfileReq->fr_Drawer;
  //printf("%s", path);

  // --create file------------
  if ((fp = fopen("PROGDIR:config.txt","wb")) == NULL)  
  {
     printf ("can't create trd file\n");
  }

 fwrite(path,1,strlen(path),fp);
 fclose(fp);


  openImageFile(paintings.activePainting->window, file, p);


  return true;

}

