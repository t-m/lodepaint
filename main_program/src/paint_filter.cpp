/*
LodePaint

Copyright (c) 2009-2010 Lode Vandevenne
All rights reserved.

This file is part of LodePaint.

LodePaint is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LodePaint is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LodePaint.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "paint_filter.h" 

#include "lpi/gui/lpi_gui_color.h"

#include <iostream>

////////////////////////////////////////////////////////////////////////////////

Progress dummyProgress;

IOperationFilter::IOperationFilter(GlobalToolSettings& settings)
: IOperation(settings)
, params(false)
{
}

////////////////////////////////////////////////////////////////////////////////

bool AOperationFilterBase::operateGL(Paint& paint, UndoState& state)
{
  (void)state;
  paint.update();
  return true;
}

void AOperationFilterBase::init(PaintWindow* pw)
{
  if(FilterDialogWithPreview* preview = dynamic_cast<FilterDialogWithPreview*>(getDialog()))
  {
    Paint& paint = pw->getPaint();
    if(paint.getColorMode() == MODE_RGBA_8)
    {
      preview->generatePreviewSource(&paint.canvasRGBA8);
    }
    else if(paint.getColorMode() == MODE_RGBA_32F)
    {
      preview->generatePreviewSource(&paint.canvasRGBA32F);
    }
    else if(paint.getColorMode() == MODE_GREY_8)
    {
      preview->generatePreviewSource(&paint.canvasGrey8);
    }
    else if(paint.getColorMode() == MODE_GREY_32F)
    {
      preview->generatePreviewSource(&paint.canvasGrey32F);
    }
  }
}

////////////////////////////////////////////////////////////////////////////////

static const std::string colorModeNotSupportedString = "\
Color mode not (yet) supported by this filter.\n\
Please convert the image to a different\n\
color mode to apply this filter.";

bool AOperationFilter::canOperateThreadSafe(Paint& paint)
{
  if(paint.getColorMode() == MODE_RGBA_8)
  {
    //bool self = paint.hasFloatingSelection();
    bool seln = paint.hasNonFloatingSelection();
    
    if(seln) return false;
  }
  return true;
}

bool AOperationFilter::operate2(Paint& paint, UndoState& state, Progress& progress)
{
  if(!supportsColorMode(paint.getColorMode()))
  {
    lastError = colorModeNotSupportedString;
    return false;
  }
  
  progress.setStatus(S_BUSY);
  
  if(worksOnMask()) //this means the filter edits the mask itself, instead of the image
  {
    if(paint.getColorMode() == MODE_RGBA_8 || paint.getColorMode() == MODE_GREY_8)
    {
      operate(paint.mask8, state, progress, false);
    }
    else
    {
      operate(paint.mask32F, state, progress, false);
    }
  }
  else
  {
    if(paint.getColorMode() == MODE_RGBA_8)
    {
      bool self = paint.hasFloatingSelection();
      bool seln = paint.hasNonFloatingSelection();

      if(seln) paint.makeSelectionFloating(settings.rightColor255(), false);

      if(seln || self)
      {
        operate(paint.floatsel.texture, state, progress, false);

        paint.sel.x1 = paint.sel.x0 + paint.floatsel.texture.getU();
        paint.sel.y1 = paint.sel.y0 + paint.floatsel.texture.getV();
      }
      else
      {
        if(paint.maskEnabled())
        {
          std::vector<unsigned char> old;
          size_t u = paint.canvasRGBA8.getU();
          size_t v = paint.canvasRGBA8.getV();
          getAlignedBuffer(old, &paint.canvasRGBA8);

          operate(paint.canvasRGBA8, state, progress, true);
          if(u == paint.canvasRGBA8.getU() && v == paint.canvasRGBA8.getV())
          {
            blendWithMask(paint, &old[0]);
          }
        }
        else
        {
          operate(paint.canvasRGBA8, state, progress, false);
        }
      }

      if(seln)
      {
        paint.floatsel.treat_alpha_as_opacity = false;
        paint.unFloatSelection();
      }
    }
    else if(paint.getColorMode() == MODE_RGBA_32F)
    {
      operate(paint.canvasRGBA32F, state, progress, false);
    }
    else if(paint.getColorMode() == MODE_GREY_8)
    {
      if(paint.maskEnabled())
      {
        std::vector<unsigned char> old;
        size_t u = paint.canvasGrey8.getU();
        size_t v = paint.canvasGrey8.getV();
        getAlignedBuffer(old, &paint.canvasGrey8);

        operate(paint.canvasGrey8, state, progress, true);
        if(u == paint.canvasGrey8.getU() && v == paint.canvasGrey8.getV())
        {
          blendWithMask(paint, &old[0]);
        }
      }
      else
      {
        operate(paint.canvasGrey8, state, progress, false);
      }
    }
    else if(paint.getColorMode() == MODE_GREY_32F)
    {
      operate(paint.canvasGrey32F, state, progress, false);
    }
  }
  
  progress.setStatus(S_DONE);
  
  return true;
}

bool AOperationFilter::operate(Paint& paint, UndoState& state, Progress& progress)
{
  if(canOperateThreadSafe(paint))
  {
    return operate2(paint, state, progress);
  }
  else return true;
}

bool AOperationFilter::operateGL(Paint& paint, UndoState& state)
{
  if(canOperateThreadSafe(paint))
  {
    paint.update();
    if(paint.hasFloatingSelection()) paint.floatsel.texture.update();
  }
  else
  {
    operate2(paint, state, dummyProgress);
    paint.update();
  }
  
  return true;
}

void AOperationFilter::blendWithMask(Paint& paint, const unsigned char* old)
{
  if(paint.getColorMode() == MODE_RGBA_8)
  {
    size_t u = paint.canvasRGBA8.getU();
    size_t v = paint.canvasRGBA8.getV();
    size_t u2 = paint.canvasRGBA8.getU2();
    unsigned char* buffer = paint.canvasRGBA8.getBuffer();
    unsigned char* mask = paint.mask8.getBuffer();
    for(size_t y = 0; y < v; y++)
    for(size_t x = 0; x < u; x++)
    {
      size_t index = y * u2 * 4 + x * 4;
      size_t indexo = y * u * 4 + x * 4;
      size_t indexm = y * u2 + x;
      int o = mask[indexm];
      buffer[index + 0] = (old[indexo + 0] * o + buffer[index + 0] * (255 - o)) / 255;
      buffer[index + 1] = (old[indexo + 1] * o + buffer[index + 1] * (255 - o)) / 255;
      buffer[index + 2] = (old[indexo + 2] * o + buffer[index + 2] * (255 - o)) / 255;
      buffer[index + 3] = (old[indexo + 3] * o + buffer[index + 3] * (255 - o)) / 255;
      /*buffer[index + 0] = (unsigned char)(lpi::clamp<float>((old[indexo + 0] * o + buffer[index + 0] * (1 - o)), 0, 255));
      buffer[index + 1] = (unsigned char)(lpi::clamp<float>((old[indexo + 1] * o + buffer[index + 1] * (1 - o)), 0, 255));
      buffer[index + 2] = (unsigned char)(lpi::clamp<float>((old[indexo + 2] * o + buffer[index + 2] * (1 - o)), 0, 255));
      buffer[index + 3] = (unsigned char)(lpi::clamp<float>((old[indexo + 3] * o + buffer[index + 3] * (1 - o)), 0, 255));*/
    }
  }
  else if(paint.getColorMode() == MODE_GREY_8)
  {
    size_t u = paint.canvasGrey8.getU();
    size_t v = paint.canvasGrey8.getV();
    size_t u2 = paint.canvasGrey8.getU2();
    unsigned char* buffer = paint.canvasGrey8.getBuffer();
    unsigned char* mask = paint.mask8.getBuffer();
    for(size_t y = 0; y < v; y++)
    for(size_t x = 0; x < u; x++)
    {
      size_t index = y * u2 + x;
      size_t indexo = y * u + x;
      size_t indexm = y * u2 + x;
      int o = mask[indexm];
      buffer[index] = (old[indexo] * o + buffer[index] * (255 - o)) / 255;
     }
  }
}

bool AOperationFilter::canOperate(std::string& reason, const Paint& paint) const
{

  if(!supportsColorMode(paint.getColorMode()))
  {
    reason = colorModeNotSupportedString;
    return false;
  }
  
  return true;
}

bool AOperationFilter::supportsColorMode(ColorMode mode) const
{
  return mode == MODE_RGBA_8;
}

bool AOperationFilter::sup(ColorMode mode, bool rgba8, bool rgba32f, bool grey8, bool grey32f)
{
  return mode == MODE_RGBA_8 ? rgba8 : (
         mode == MODE_RGBA_32F ? rgba32f : (
         mode == MODE_GREY_8 ? grey8 :
         grey32f)); 
}

void AOperationFilter::blendWithMask(Paint& paint, const float* old)
{
  if(paint.getColorMode() == MODE_RGBA_32F)
  {
    size_t u = paint.canvasRGBA32F.getU();
    size_t v = paint.canvasRGBA32F.getV();
    size_t u2 = paint.canvasRGBA32F.getU2();
    float* buffer = paint.canvasRGBA32F.getBuffer();
    float* mask = paint.mask32F.getBuffer();
    for(size_t y = 0; y < v; y++)
    for(size_t x = 0; x < u; x++)
    {
      size_t index = y * u2 * 4 + x * 4;
      size_t indexo = y * u * 4 + x * 4;
      size_t indexm = y * u2 + x;
      float o = mask[indexm];
      buffer[index + 0] = old[indexo + 0] * o + buffer[index + 0] * (1 - o);
      buffer[index + 1] = old[indexo + 1] * o + buffer[index + 1] * (1 - o);
      buffer[index + 2] = old[indexo + 2] * o + buffer[index + 2] * (1 - o);
      buffer[index + 3] = old[indexo + 3] * o + buffer[index + 3] * (1 - o);
   }
  }
  else if(paint.getColorMode() == MODE_GREY_32F)
  {
    size_t u = paint.canvasGrey32F.getU();
    size_t v = paint.canvasGrey32F.getV();
    size_t u2 = paint.canvasGrey32F.getU2();
    float* buffer = paint.canvasGrey32F.getBuffer();
    float* mask = paint.mask32F.getBuffer();
    for(size_t y = 0; y < v; y++)
    for(size_t x = 0; x < u; x++)
    {
      size_t index = y * u2 + x;
      size_t indexo = y * u + x;
      size_t indexm = y * u2 + x;
      float o = mask[indexm];
      buffer[index] = old[indexo] * o + buffer[index] * (1 - o);
     }
  }
  
  
  
  (void)paint;
  (void)old;
  //TODO
  /*size_t u = paint.canvasRGBA8.getU();
  size_t v = paint.canvasRGBA8.getV();
  size_t u2 = paint.canvasRGBA8.getU2();
  float* buffer = paint.canvasRGBA8.getBuffer();
  float* mask = (float*)paint.mask32.getBuffer();
  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  {
    size_t index = y * u2 * 4 + x * 4;
    size_t indexo = y * u * 4 + x * 4;
    size_t indexm = y * u2 + x;
    float o = mask[indexm];
    buffer[index + 0] = old[indexo + 0] * o + buffer[index + 0] * (1 - o);
    buffer[index + 1] = old[indexo + 1] * o + buffer[index + 1] * (1 - o);
    buffer[index + 2] = old[indexo + 2] * o + buffer[index + 2] * (1 - o);
    buffer[index + 3] = old[indexo + 3] * o + buffer[index + 3] * (1 - o);
  }*/
}

void AOperationFilter::undo(Paint& paint, UndoState& state)
{
  if(worksOnMask())
  {
    undo(paint.mask8, state);
    paint.mask8.update();
  }
  else
  {
    if(paint.getColorMode() == MODE_RGBA_8)
    {
      bool self = paint.hasFloatingSelection();
      bool seln = paint.hasNonFloatingSelection();

      if(seln) paint.makeSelectionFloating(settings.rightColor255(), false);

      if(seln || self)
      {
        undo(paint.floatsel.texture, state);

        paint.sel.x1 = paint.sel.x0 + paint.floatsel.texture.getU();
        paint.sel.y1 = paint.sel.y0 + paint.floatsel.texture.getV();
      }
      else
      {
        undo(paint.canvasRGBA8, state);
        paint.update();
      }

      if(seln)
      {
        paint.floatsel.treat_alpha_as_opacity = false;
        paint.unFloatSelection();
      }
    }
    else if(paint.getColorMode() == MODE_RGBA_32F)
    {
      undo(paint.canvasRGBA32F, state);
      paint.update();
    }
    else if(paint.getColorMode() == MODE_GREY_8)
    {
      undo(paint.canvasGrey8, state);
      paint.update();
    }
    else if(paint.getColorMode() == MODE_GREY_32F)
    {
      undo(paint.canvasGrey32F, state);
      paint.update();
    }

  }
}

void AOperationFilter::redo(Paint& paint, UndoState& state)
{
  if(worksOnMask())
  {
    redo(paint.mask8, state);
    paint.mask8.update();
  }
  else
  {
    if(paint.getColorMode() == MODE_RGBA_8)
    {
      bool self = paint.hasFloatingSelection();
      bool seln = paint.hasNonFloatingSelection();

      if(seln) paint.makeSelectionFloating(settings.rightColor255(), false);

      if(seln || self)
      {
        redo(paint.floatsel.texture, state);

        paint.sel.x1 = paint.sel.x0 + paint.floatsel.texture.getU();
        paint.sel.y1 = paint.sel.y0 + paint.floatsel.texture.getV();
      }
      else
      {
        redo(paint.canvasRGBA8, state);
        paint.update();
      }


      if(seln)
      {
        paint.floatsel.treat_alpha_as_opacity = false;
        paint.unFloatSelection();
      }
    }
    else if(paint.getColorMode() == MODE_RGBA_32F)
    {
      redo(paint.canvasRGBA32F, state);
      paint.update();
    }
    else if(paint.getColorMode() == MODE_GREY_8)
    {
      redo(paint.canvasGrey8, state);
      paint.update();
    }
    else if(paint.getColorMode() == MODE_GREY_32F)
    {
      redo(paint.canvasGrey32F, state);
      paint.update();
    }
  }
}

////////////////////////////////////////////////////////////////////////////////

void AOperationFilterUtil::operate(TextureRGBA8& texture, UndoState& state, Progress& progress, bool not_invertible)
{
  state.addBool(not_invertible);
  if(save_texture_undo || not_invertible) storeCompleteTextureInUndo(state, texture);
  doit(texture,progress);
}

void AOperationFilterUtil::undo(TextureRGBA8& texture, UndoState& state)
{
  UndoStateIndex index;
  bool not_invertible = state.getBool(index.ibool++);
  
  if(not_invertible || (save_texture_undo && save_texture_redo))
  {
    UndoState temp;
    temp.addBool(not_invertible);
    storeCompleteTextureInUndo(temp, texture);
    readCompleteTextureFromUndo(texture, state, index);
    state.swap(temp);
  }
  else if(save_texture_undo && !save_texture_redo)
  {
    readCompleteTextureFromUndo(texture, state, index);
  }
  else if(!save_texture_redo)
  {
    doit(texture, dummyProgress);
    texture.update();
  }
  else {}; //ERROR!!! save_texture_undo must be true if save_texture_redo is true.
}

void AOperationFilterUtil::redo(TextureRGBA8& texture, UndoState& state)
{
  UndoStateIndex index;
  bool not_invertible = state.getBool(index.ibool++);
  
  if(not_invertible || (save_texture_undo && save_texture_redo))
  {
    UndoState temp;
    temp.addBool(not_invertible);
    storeCompleteTextureInUndo(temp, texture);
    readCompleteTextureFromUndo(texture, state, index);
    state.swap(temp);
  }
  else
  {
    doit(texture, dummyProgress);
    texture.update();
  }
}

void AOperationFilterUtil::operate(TextureRGBA32F& texture, UndoState& state, Progress& progress, bool not_invertible)
{
  state.addBool(not_invertible);
  if(save_texture_undo || not_invertible) storeCompleteTextureInUndo(state, texture);
  doit(texture, progress);
}

void AOperationFilterUtil::undo(TextureRGBA32F& texture, UndoState& state)
{
  UndoStateIndex index;
  bool not_invertible = state.getBool(index.ibool++);

  if(not_invertible || (save_texture_undo && save_texture_redo))
  {
    UndoState temp;
    temp.addBool(not_invertible);
    storeCompleteTextureInUndo(temp, texture);
    readCompleteTextureFromUndo(texture, state, index);
    state.swap(temp);
  }
  else if(save_texture_undo && !save_texture_redo)
  {
    readCompleteTextureFromUndo(texture, state, index);
  }
  else if(!save_texture_redo)
  {
    doit(texture, dummyProgress);
  }
  else {}; //ERROR!!! save_texture_undo must be true if save_texture_redo is true.
}

void AOperationFilterUtil::redo(TextureRGBA32F& texture, UndoState& state)
{
  UndoStateIndex index;
  bool not_invertible = state.getBool(index.ibool++);
  
  if(not_invertible || (save_texture_undo && save_texture_redo))
  {
    UndoState temp;
    temp.addBool(not_invertible);
    storeCompleteTextureInUndo(temp, texture);
    readCompleteTextureFromUndo(texture, state, index);
    state.swap(temp);
  }
  else
  {
    doit(texture, dummyProgress);
  }
}

void AOperationFilterUtil::operate(TextureGrey8& texture, UndoState& state, Progress& progress, bool not_invertible)
{
  state.addBool(not_invertible);
  if(save_texture_undo || not_invertible) storeCompleteTextureInUndo(state, texture);
  doit(texture,progress);
}

void AOperationFilterUtil::undo(TextureGrey8& texture, UndoState& state)
{
  UndoStateIndex index;
  bool not_invertible = state.getBool(index.ibool++);

  if(not_invertible || (save_texture_undo && save_texture_redo))
  {
    UndoState temp;
    temp.addBool(not_invertible);
    storeCompleteTextureInUndo(temp, texture);
    readCompleteTextureFromUndo(texture, state, index);
    state.swap(temp);
  }
  else if(save_texture_undo && !save_texture_redo)
  {
    readCompleteTextureFromUndo(texture, state, index);
  }
  else if(!save_texture_redo)
  {
    doit(texture, dummyProgress);
    texture.update();
  }
  else {}; //ERROR!!! save_texture_undo must be true if save_texture_redo is true.
}

void AOperationFilterUtil::redo(TextureGrey8& texture, UndoState& state)
{
  UndoStateIndex index;
  bool not_invertible = state.getBool(index.ibool++);

  if(not_invertible || (save_texture_undo && save_texture_redo))
  {
    UndoState temp;
    temp.addBool(not_invertible);
    storeCompleteTextureInUndo(temp, texture);
    readCompleteTextureFromUndo(texture, state, index);
    state.swap(temp);
  }
  else
  {
    doit(texture, dummyProgress);
    texture.update();
  }
}

void AOperationFilterUtil::operate(TextureGrey32F& texture, UndoState& state, Progress& progress, bool not_invertible)
{
  state.addBool(not_invertible);
  if(save_texture_undo || not_invertible) storeCompleteTextureInUndo(state, texture);
  doit(texture, progress);
}

void AOperationFilterUtil::undo(TextureGrey32F& texture, UndoState& state)
{
  UndoStateIndex index;
  bool not_invertible = state.getBool(index.ibool++);

  if(not_invertible || (save_texture_undo && save_texture_redo))
  {
    UndoState temp;
    temp.addBool(not_invertible);
    storeCompleteTextureInUndo(temp, texture);
    readCompleteTextureFromUndo(texture, state, index);
    state.swap(temp);
  }
  else if(save_texture_undo && !save_texture_redo)
  {
    readCompleteTextureFromUndo(texture, state, index);
  }
  else if(!save_texture_redo)
  {
    doit(texture, dummyProgress);
    texture.update();
  }
  else {}; //ERROR!!! save_texture_undo must be true if save_texture_redo is true.
}

void AOperationFilterUtil::redo(TextureGrey32F& texture, UndoState& state)
{
  UndoStateIndex index;
  bool not_invertible = state.getBool(index.ibool++);

  if(not_invertible || (save_texture_undo && save_texture_redo))
  {
    UndoState temp;
    temp.addBool(not_invertible);
    storeCompleteTextureInUndo(temp, texture);
    readCompleteTextureFromUndo(texture, state, index);
    state.swap(temp);
  }
  else
  {
    doit(texture, dummyProgress);
    texture.update();
  }
}

void AOperationFilterUtil::operateForPreview(TextureRGBA8& texture)
{
  doit(texture, dummyProgress);
  texture.update();
}

void AOperationFilterUtil::operateForPreview(TextureRGBA32F& texture)
{
  doit(texture, dummyProgress);
  texture.update();
}

void AOperationFilterUtil::operateForPreview(TextureGrey8& texture)
{
  doit(texture, dummyProgress);
  texture.update();
}

void AOperationFilterUtil::operateForPreview(TextureGrey32F& texture)
{
  doit(texture, dummyProgress);
  texture.update();
}

////////////////////////////////////////////////////////////////////////////////

AOperationFilterUtilWithSettings::AOperationFilterUtilWithSettings(GlobalToolSettings& settings, bool save_texture_undo, bool save_texture_redo)
: AOperationFilter(settings)
, save_texture_undo(save_texture_undo)
, save_texture_redo(save_texture_redo)
{
}

void AOperationFilterUtilWithSettings::operate(TextureRGBA8& texture, UndoState& state, Progress& progress, bool not_invertible)
{
  state.addBool(not_invertible);
  storeSettingsInUndo(state, settings);
  if(save_texture_undo || not_invertible) storeCompleteTextureInUndo(state, texture);
  doit(texture, settings, progress);
}

void AOperationFilterUtilWithSettings::undo(TextureRGBA8& texture, UndoState& state)
{
  UndoStateIndex index;
  bool not_invertible = state.getBool(index.ibool++);
  GlobalToolSettings undoSettings;
  readSettingsFromUndo(undoSettings, state, index);
  if(not_invertible || (save_texture_undo && save_texture_redo))
  {
    UndoState temp;
    temp.addBool(not_invertible);
    storeSettingsInUndo(temp, undoSettings);
    storeCompleteTextureInUndo(temp, texture);
    readCompleteTextureFromUndo(texture, state, index);
    state.swap(temp);
  }
  else if(save_texture_undo && !save_texture_redo)
  {
    readCompleteTextureFromUndo(texture, state, index);
  }

  else if(!save_texture_redo) doit(texture, undoSettings, dummyProgress);
  else {}; //ERROR!!! save_texture_undo must be true if save_texture_redo is true.
}

void AOperationFilterUtilWithSettings::redo(TextureRGBA8& texture, UndoState& state)
{
  UndoStateIndex index;
  bool not_invertible = state.getBool(index.ibool++);
  GlobalToolSettings undoSettings;
  readSettingsFromUndo(undoSettings, state, index);

  if(not_invertible || (save_texture_undo && save_texture_redo))
  {
    UndoState temp;
    temp.addBool(not_invertible);
    storeSettingsInUndo(temp, undoSettings);
    storeCompleteTextureInUndo(temp, texture);
    readCompleteTextureFromUndo(texture, state, index);
    state.swap(temp);
  }
  else doit(texture, undoSettings, dummyProgress);
}


void AOperationFilterUtilWithSettings::operate(TextureRGBA32F& texture, UndoState& state, Progress& progress, bool not_invertible)
{
  state.addBool(not_invertible);
  storeSettingsInUndo(state, settings);
  if(save_texture_undo || not_invertible) storeCompleteTextureInUndo(state, texture);
  doit(texture, settings, progress);
}

void AOperationFilterUtilWithSettings::undo(TextureRGBA32F& texture, UndoState& state)
{
  UndoStateIndex index;
  bool not_invertible = state.getBool(index.ibool++);

  GlobalToolSettings undoSettings;
  readSettingsFromUndo(undoSettings, state, index);

  if(not_invertible || (save_texture_undo && save_texture_redo))
  {
    UndoState temp;
    temp.addBool(not_invertible);
    storeSettingsInUndo(temp, undoSettings);
    storeCompleteTextureInUndo(temp, texture);
    readCompleteTextureFromUndo(texture, state, index);
    state.swap(temp);
  }
  else if(save_texture_undo && !save_texture_redo)
  {
    readCompleteTextureFromUndo(texture, state, index);
  }
  else if(!save_texture_redo) doit(texture, undoSettings, dummyProgress);
  else {}; //ERROR!!! save_texture_undo must be true if save_texture_redo is true.
}

void AOperationFilterUtilWithSettings::redo(TextureRGBA32F& texture, UndoState& state)
{
  UndoStateIndex index;
  bool not_invertible = state.getBool(index.ibool++);

  GlobalToolSettings undoSettings;
  readSettingsFromUndo(undoSettings, state, index);

  if(not_invertible || (save_texture_undo && save_texture_redo))
  {
    UndoState temp;
    temp.addBool(not_invertible);
    storeSettingsInUndo(temp, undoSettings);
    storeCompleteTextureInUndo(temp, texture);
    readCompleteTextureFromUndo(texture, state, index);
    state.swap(temp);
  }
  else doit(texture, undoSettings, dummyProgress);
}


void AOperationFilterUtilWithSettings::operate(TextureGrey8& texture, UndoState& state, Progress& progress, bool not_invertible)
{
  state.addBool(not_invertible);
  storeSettingsInUndo(state, settings);
  if(save_texture_undo || not_invertible) storeCompleteTextureInUndo(state, texture);
  doit(texture, settings, progress);
}

void AOperationFilterUtilWithSettings::undo(TextureGrey8& texture, UndoState& state)
{
  UndoStateIndex index;
  bool not_invertible = state.getBool(index.ibool++);

  GlobalToolSettings undoSettings;
  readSettingsFromUndo(undoSettings, state, index);

  if(not_invertible || (save_texture_undo && save_texture_redo))
  {
    UndoState temp;
    temp.addBool(not_invertible);
    storeSettingsInUndo(temp, undoSettings);
    storeCompleteTextureInUndo(temp, texture);
    readCompleteTextureFromUndo(texture, state, index);
    state.swap(temp);
  }
  else if(save_texture_undo && !save_texture_redo)
  {
    readCompleteTextureFromUndo(texture, state, index);
  }
  else if(!save_texture_redo) doit(texture, undoSettings, dummyProgress);
  else {}; //ERROR!!! save_texture_undo must be true if save_texture_redo is true.
}

void AOperationFilterUtilWithSettings::redo(TextureGrey8& texture, UndoState& state)
{
  UndoStateIndex index;
  bool not_invertible = state.getBool(index.ibool++);

  GlobalToolSettings undoSettings;
  readSettingsFromUndo(undoSettings, state, index);

  if(not_invertible || (save_texture_undo && save_texture_redo))
  {
    UndoState temp;
    temp.addBool(not_invertible);
    storeSettingsInUndo(temp, undoSettings);
    storeCompleteTextureInUndo(temp, texture);
    readCompleteTextureFromUndo(texture, state, index);
    state.swap(temp);
  }
  else doit(texture, undoSettings, dummyProgress);
}


void AOperationFilterUtilWithSettings::operate(TextureGrey32F& texture, UndoState& state, Progress& progress, bool not_invertible)
{
  state.addBool(not_invertible);
  storeSettingsInUndo(state, settings);
  if(save_texture_undo || not_invertible) storeCompleteTextureInUndo(state, texture);
  doit(texture, settings, progress);
}

void AOperationFilterUtilWithSettings::undo(TextureGrey32F& texture, UndoState& state)
{
  UndoStateIndex index;
  bool not_invertible = state.getBool(index.ibool++);

  GlobalToolSettings undoSettings;
  readSettingsFromUndo(undoSettings, state, index);

  if(not_invertible || (save_texture_undo && save_texture_redo))
  {
    UndoState temp;
    temp.addBool(not_invertible);
    storeSettingsInUndo(temp, undoSettings);
    storeCompleteTextureInUndo(temp, texture);
    readCompleteTextureFromUndo(texture, state, index);
    state.swap(temp);
  }
  else if(save_texture_undo && !save_texture_redo)
  {
    readCompleteTextureFromUndo(texture, state, index);
  }
  else if(!save_texture_redo) doit(texture, undoSettings, dummyProgress);
  else {}; //ERROR!!! save_texture_undo must be true if save_texture_redo is true.
}

void AOperationFilterUtilWithSettings::redo(TextureGrey32F& texture, UndoState& state)
{
  UndoStateIndex index;
  bool not_invertible = state.getBool(index.ibool++);

  GlobalToolSettings undoSettings;
  readSettingsFromUndo(undoSettings, state, index);

  if(not_invertible || (save_texture_undo && save_texture_redo))
  {
    UndoState temp;
    temp.addBool(not_invertible);
    storeSettingsInUndo(temp, undoSettings);
    storeCompleteTextureInUndo(temp, texture);
    readCompleteTextureFromUndo(texture, state, index);
    state.swap(temp);
  }
  else doit(texture, undoSettings, dummyProgress);
}

void AOperationFilterUtilWithSettings::operateForPreview(TextureRGBA8& texture)
{
  doit(texture, settings, dummyProgress);
  texture.update();
}

void AOperationFilterUtilWithSettings::operateForPreview(TextureRGBA32F& texture)
{
  doit(texture, settings, dummyProgress);
  texture.update();
}

void AOperationFilterUtilWithSettings::operateForPreview(TextureGrey8& texture)
{
  doit(texture, settings, dummyProgress);
  texture.update();
}

void AOperationFilterUtilWithSettings::operateForPreview(TextureGrey32F& texture)
{
  doit(texture, settings, dummyProgress);
  texture.update();
}

////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
