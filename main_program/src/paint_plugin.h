/*
LodePaint

Copyright (c) 2009-2010 Lode Vandevenne
All rights reserved.

This file is part of LodePaint.

LodePaint is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LodePaint is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LodePaint.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "paint_param.h"

/*
This headerfile shows the exact C signatures the functions in a file format and a filter plugin should have
in the dynamic or shared library.

This is just a first initial idea for the plugin architecture! Major problems with the interface
might be fixed later making it incompatible.
*/

//all LodePaint plugins must have the following function

/*
valid return values:
0: file format
1: filter
*/
int getLodePaintPluginType();
void pluginFreeUnsignedChar(unsigned char* buffer);
void pluginFreeFloat(float* buffer);

///File format plugins

/*
File format plugins work entirely in memory, and the communication between C interface
and std::vectors even involves having the data TWICE in memory. But LodePaint isn't supposed
to be useful for images of gigabytes in size, while PC's do have gigabytes of RAM so that isn't
a problem, on the contrary, it gives a more flexible interface!
NOTE: the "parameters" parameter in the interface of the encode and decode functions is currently not used by LodePaint (but set to NULL), but this might be used later (user chosen parameters like quality, ..., it'd work similar to the custom parameters of filter plugins)
*/

void fileFormatGetDescription(char* name, int maxstrlen); //description for in the file dialog, e.g. "PNG files"
int fileFormatGetNumExt(); //number of extensions to list in the file dialog for this file
void fileFormatGetExt(char* ext, int maxstrlen, int index);
int fileFormatIsOfFormat(unsigned char* buffer, unsigned long size); //return value: 0=false, 1=true

//these are only relevant for file formats that save (and can cause information loss)
int fileFormatSupportsAlpha();
int fileFormatIsLosless();

//NOTE: the functions below create buffers. These must be deallocated with fileFormatFreeUnsighedChar or fileFormatFreeFloat
int fileFormatEncodeRGBA8(unsigned char** buffer, unsigned long* size, unsigned char* image, int width, int height, int* parameters); //returns 0 if ok, otherwise error. Size is output parameter containing filesize.
int fileFormatDecodeRGBA8(unsigned char** image, int* width, int* height, unsigned char* buffer, unsigned long size, int* parameters);
int fileFormatEncodeRGBA32F(unsigned char** buffer, unsigned long* size, float* image, int width, int height, int* parameters); //returns 0 if ok, otherwise error. Size is output parameter containing filesize.
int fileFormatDecodeRGBA32F(float** image, int* width, int* height, unsigned char* buffer, unsigned long size, int* parameters);
int fileFormatEncodeGrey8(unsigned char** buffer, unsigned long* size, unsigned char* image, int width, int height, int* parameters); //returns 0 if ok, otherwise error. Size is output parameter containing filesize.
int fileFormatDecodeGrey8(unsigned char** image, int* width, int* height, unsigned char* buffer, unsigned long size, int* parameters);
int fileFormatEncodeGrey32F(unsigned char** buffer, unsigned long* size, float* image, int width, int height, int* parameters); //returns 0 if ok, otherwise error. Size is output parameter containing filesize.
int fileFormatDecodeGrey32F(float** image, int* width, int* height, unsigned char* buffer, unsigned long size, int* parameters);


///filter plugins
/*
filter: plugins that do an action on the whole image

one or both execute functions, OR one or both resize functions must be implemented to be a filter plugin (and the get name function of course)
it's not allowed to have both an execute and a resize function. It's either the one, or the other.
execute is for filters that don't resize the image, they operate on the given buffer without having to create a new one if not needed
the resize function is for filters that output an image that may be of different size than the input. For that they need to create a new memory buffer with the new size, and output the new size to the ou and ov parameters.
After using a resize function, the user then needs to free the output with pluginFreeUnsignedChar or pluginFreeFloat after using it to free up the memory.
*/

int filterGetNumFilters();

void filterGetName(char* name, int maxstrlen, int filter_index);
void filterGetSubMenu(char* name, int maxstrlen, int filter_index);

/*
The parameters are:
-image: input and output pixels
-u, v: width, height
-u2:  width in memory of pixel rows, u is the displayed width. Each pixel has 4 values (R, G, B and A) so buffer size is u2 * v * 4 and visible size is u * v * 4.
-params: CParams struct with all dynamic parameters
-mainparams: CParams of the main program, currently only contains FG and BG color

If the filter resizes the image, it creates a new buffer. If the size remains the same, it does NOT create a new buffer.
So there are two possibilities after the filter is done:

*) It didn't resize
- *ou will be equal to u
- *ov will be equal to v
- *out will be equal to in
- No deletion of buffer required after filter is done

*) It resized
- *ou isn't equal to u OR *ov isn't equal to v
- *out will be different from in
- the out buffer has to be deleted by the filter with pluginFreeUnsignedChar or pluginFreeFloat

*/
void filterExecuteRGBA8(unsigned char** out, int* ou, int* ov, unsigned char* in, int u, int v, int u2, CParams* params, CParams* mainparams, int filter_index);
void filterExecuteRGBA32F(float** out, int* ou, int* ov, float* in, int u, int v, int u2, CParams* params, CParams* mainparams, int filter_index);
void filterExecuteGrey8(unsigned char** out, int* ou, int* ov, unsigned char* in, int u, int v, int u2, CParams* params, CParams* mainparams, int filter_index);
void filterExecuteGrey32F(float** out, int* ou, int* ov, float* in, int u, int v, int u2, CParams* params, CParams* mainparams, int filter_index);
/*
Predefined parameters. See XML example in paint_param.h.
*/
int filterGetParametersXMLSize(int filter_index); //size of the XML string (including null terminator char)
void filterGetParametersXML(char* xml, int filter_index); //char* xml must be able to hold filterGetParametersXMLSize characters

