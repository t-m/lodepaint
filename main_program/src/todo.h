/*
LodePaint

Copyright (c) 2009-2010 Lode Vandevenne
All rights reserved.

This file is part of LodePaint.

LodePaint is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LodePaint is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LodePaint.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
This document is mostly in Dutch. It is the core design document of LodePaint!
*/

/*
TODO:

[ ] alle filters (ook flip, rotate) ook op floating selectie laten werken ipv hele image
[X] geom tool: line
[X] geom tool: circle by 3 points
[X] geom tool: circle by center and radius
[X] geom tool: ellpise
[X] geom tool: rectangle
[X] geom tool: square --> kan ook setting van de rectangle tool worden...
[ ] geom tool: polyline
[X] geom tool: triangle
[ ] geom tool: bezier
[ ] geom tool: polybezier
[X] geom tool: polygon (filled)
[X] geom tool: rounded rectangle (instelbare roundness)
[X] fill + line settings voor geom vormen
[X] lijnen met dikte
[X] lijnen met opacity (niet hetzelfde als alpha in de kleur!)
[X] fill van geometrische vormen met opacity (niet hetzelfde als alpha in de kleur!)
[X] filters met settings in venster, onder andere resize, noise generatie met opacity, histogram, hue/sat/brightness, enz...
[X] resize met verschillende algo's
[X] bicubic resize
[X] brushes voor lighter en darker, meer/minder saturatin, ... --> "retouch" brush die alles combineert
[ ] brush voor locale blur en sharpen
[X] brushes of andere tools met extra settings in vensterke (bv parameters van spraypaint, ...)
[X] custom filter met matrix die je kan invullen (convolution filter)
[X] plakken vanaf clipboard!!! In Linux (X11).
[X] plakken vanaf clipboard!!! In Windows.
[X] plakken vanaf clipboard: naar selectie ipv naar nieuwe image (met ctrl+E)
[+] undo beter laten werken: resetten wanneer je nieuwe file opent enzo --> deze probleempjes zijn door multiimage support gefixt, andere probleempjes zijn andere items here
[X] die bug fixen met niet exacte kleur op tekening...
[X] ondersteuning voor bmp en misschien tga en enkele andere simpelen
[X] settings dialoog voor bv startup resolutie, undo size, ...
[X] background pattern (voor transparantie) in settings, niet meer in floating vensterke --> tis toolbar knopke geworden
[X] readme in html, met wat prentjes (bv hoe brush softness eruitziet, ...)
[X] in de file dialoog links van de 3 text input fields kort de naam van die tekst input field geven
[X] crop
[X] keuze uit meerdere palettes voor de colors in de color dialoog (en eventueel een custom palette dat wordt onthouden in de settings)
[X] knopje om naar 1x zoom te springen
[X] in interface ook knoppen om zoom in te stellen, ipv enkel met muis
[ ] spraypaint tool, die random wat pixeltjes invult (op mooie manier die image wat ruwer kan maken)
[X] draggen met muis aan zijkanten image voor croppen en vergroten, aan ELKE kant mogelijk
[X] copyright message in elke source file en de readme
[ ] license, en vermelding dat ik niet verantwoordelijk ben voor dataverlies enz...
[X] keyboard shortcuts voor tools in werking krijgen (telkens 1 letter zonder control of shift)
[X] keyboard shortcuts voor undo, redo (ctrlz, ctrly en ook ctrlshiftz)
[+] keyboard shortcuts voor copy en paste
[X] een tekst tool, al is het maar met simpele bitmap fonts. De tekst moet in een floating selectie komen, met alpha transparantie als achtergrond. Als je klikt kan je beginnen typen, met enter is het gedaan.
[X] fullscreen mode (but sort of hidden in settings, this is about true fullscreen like computer games)
[ ] lines with arrow heads
[X] diamond shaped brush
[ ] pen die automatisch hand getekende vormen omzet naar rechte lijnen en circle-delen
[X] color replacer tool
[X] tolerance setting voor floodfill en color replacer
[ ] dropdown menus voor undo en redo met de namen van operaties
[X] knopjes en menu opties van undo / redo grijs als er geen verdere stap is
[ ] DCT operatie
[X] sharpen, emboss, edge detection (H, V, both)
[X] ne simpele parameterloze "blur more" filter die meer dan blur doet, en "soften" die minder dan blur doet
[X] brightness, contract, adjust
[X] "gamma" optie bij de brightness contrast adjust (zie bv. KolourPaint)
[X] hue, saturation, lightness adjust
[ ] enkele voorgedefinieerde brushes (brush shapes), met preview, in de brush settings dialog
[X] onder filters | alpha, een optie voor alpha channel opaak te maken
[ ] gradient & pattern floodfill
[ ] gradient floodfill die als volgt werkt: je trekt lijn tussen 2 punten. Hoek en grootte hiervan bepalen de gradient's hoek en grootte. In centrum tussen die 2 punten (of 3e punt dat je aanduidt), wordt de gradient-floodfill gestart.
[X] gradient line tool: gewoon van voorgrond naar achtergrondkleur
[X] multiple open images
[ ] creeer MultiOperation klasse: operatie die op meerdere images tegelijk werkt (vaak zonder undo, behalve als een bestaande image gewijzigd wordt). De gewone IOperation kan hiervoor niet dienen.
[ ] MultiOperation: split & combine RGBA (and other color models)
[ ] MultiOperation: image arithmetic & blend: add, subtract, xor, ... multiple open images
[ ] MultiOperation: iets om makkelijk 2 prentjes onder elkaar te plakken (of naast)
[ ] MultiOperation: mask to/from image
[ ] MultiOperation: duplicate image
[ ] MultiOperation: prentje opsplitsen in meerdere tiles (bv als alle frames van animated gif naast elkaar staan in 1 prentje, kan je er hiermee meerdere prentjes van maken)
[ ] smudge tool (met brush shapes)
[ ] clone tool (met brush shapes, en zo van klikken op bron locatie, dan herhalen op andere locaties zoveel je wil)
[X] median filter
[X] enkele fitlers niet onder filter menu plaatsen maar onder "image" menu. De clear, alle transforms, resize, ...
[X] toon meer dingen in status bar van image: (R, G, B en A van pixel waarover je hovert in kleine font), totale size van image, totale size van selectie
[X] generate clouds filter
[X] generate andere perlin noise dingen, zoals marmer, hout, ...
[ ] generate sun flare
[X] generate mandelbrot & julia
[ ] measurement tool, dat u de X, Y en carthesische afstand tussen 2 pixels geeft
[ ] measurement tool die afstand van afgelegde weg (onafhankelijk van kropping) terwijl muisknop down is geeft (zoals zoiets met wieltje onder om afstand op kaart te meten)
[ ] measurement tool: ook angle geven
[X] "wraparound" en "affect alpha" voor filters als standaard optie (zoals brush, line en fill ook standaard opties worden)
[ ] "scratch" tool, een pen die effect van scratches op image geeft
[X] tile (repeat) image operatie, die de image 2x2, 3x3 of 4x4 keer herhaalt
[X] color picker activeert vorige tool weer na picken (zoals in kolourpaint, heel handig) (MAW: Color picker moet automatisch naar vorige tool springen na picken van de color) ----> ik heb in de plaats "v" shortcut naar "previous tool" gemaakt
[X] rotate image met eender welke hoek
[+] skew image --> met Transform 2D
[X] multiline text display (met auto wrapping) in lpi voor de volgende puntjes:
[X] help dialoog die eigelijk gewoon zegt dat je manual.html moet lezen
[X] about box --> met acknowledgements voor sing.c, stb_image.c, LodePNG, lpi
[X] "count colors used" dialog, zegt gewoon in messagebox hoeveel unieke kleuren er in de image aanwezig zijn --> algo nodig dat de kleuren kan tellen op efficiente manier (bv sorteren, ...)
[ ] image information dialoog, zegt onder andere de PNG comment fields enzovoort
[X] mooiere toolbar en tool icoontjes
[X] separators in de toolbar
[X] threshold operatie (binaire image als resultaat)
[X] morphology operations: dilate, erode, morph open, morph close, top hat, gradient
[X] knopje in toolbar voor last of recently used filters
[X] iets dat R,G,B allen zelfde kleur maakt, maar alpha channel behoudt (onder filters | alpha)
[ ] themed brushes (like picture tubes in paint shop pro) for bullet holes, rivets and other such game-texture oriented shapes
[ ] make surface muddy/dirty/noisy
[ ] "bloody" brush for blood stains (can have other colors than red, e.g. green for "mossy" brush, brown for "muddy" brush, black for "ashy" brush, ...)
[X] high pass filter THAT DOESNT USE FFT: very useful to create tilable textures by removing low frequencies that make it look ugly when repeated
[X] apart from regular resize, have some "pixel art scaling algorithms", like hq2x etc...
[ ] implement hq3x and hq4x similar to how hq2x is done
[X] There must be a brush that affects ONLY alpha channel. The problem is, with a regular RGBA brush, when using it to erase to alpha channel, if it has smoothess, the hidden RGB components will be mixed with the painting and affect the RGB value - which is not what is intended.
[ ] Show histogram
[ ] undo van verplaatsen en creeeren van selecties
[X] icoontje in toolbar voor crop
[X] apply alpha to background color
[ ] anti-aliased lines
[ ] anti-aliased filled shapes
[ ] non-build-up brush: only paints as much as its opacity, no more as long as you don't release mouse button
[+] Non-rectangular selection --> done with "mask" feature instead
[X] magic wand tool voor selectie --> done voor mask
[X] Progress bars OR simply "working indicators" for long operations like FFT, saving image, ... (==> multithreading)
[X] mogelijkheid om operatie te onderbreken vanuit die progress bar
[ ] progress bars ook voor operaties die resize doen (kan nu niet wegens texture resize die OpenGL doet crashen in andere thread)
[X] progress bars ook voor saven/laden van files
[X] "treat alpha as opacity" meer algemene setting maken, die zowel voor selectie als clone brush als .... werkt
[+] make complete alpha channel equal to some value --> can be done by first making opaque, then subtracting value from it with "adjust HSLA"
[X] multiply complete alpha channel with some value
[X] "colorize" filter, zoals grayscale, maar tussen de FG en BG color ipv tussen zwart en wit
[+] "complex double" color, ipv unsigned char, in speciale "complex double" texture. Voordeel: veel meer precisie, IFFT werkt, speciale effecten dankzij complexe getallen, ... --> Is floating point real geworden (complex is overkill, double is ook overkill)
[X] make FFT work better on non-power-of-two images
[X] band pass filter
[X] gaussian blur filter, en NIET met FFT, met convolutie, 2x 1d convolutie (zie wikipedia), en met of zonder "wrap around"
[X] keuze om FFT wel of niet ook op alpha channel te doen
[X] als FFT niet op alpha channel, maak alles opaak? nu als niet power of 2 image, dan zit er random alpha info op stukken die van kant buiten image komen, da mag ni
[+] los bug op waarbij met pijltjestoetsen de muis buiten tekening kan gaan, maar nimeer erbinnen. --> opgelost aan rand tekening, maar als ingezoomd en andere gui item erboven is er weinig aan te doen momenteel.
[ ] BUG: with small radius (e.g. 5), soft brushes aren't soft enough. And without softness, brush of radius 5 gives strange horizontal rectangle.
[X] kleur naar alpha (via distance tot die kleur, hoe dichter hoe transparanter)
[X] mozaiek filter
[X] generate tiles: afwisselend patroon van 2 kleuren, met eventueel border rond
[X] BUG: FFT crasht als texture size geen macht van 2, en dat is geval bij textures met grootte > 2048
[X] "repeat last filter" toolbar button
[X] jpeg decoding
[X] jpeg decoding of "progressive" JPEG images
[X] jpeg encoding --> met plugin
[X] gif decoding
[ ] gif encoding
[ ] PCX decoding
[ ] PCX encoding
[ ] jpeg2000
[ ] geef menu's die sluiten als muis wegbeweegt een beetje delay of ander mechanisme zodat ze niet sluiten tijdens perongeluk paar pxels wegbewegen van muis (bv enkel als de snelheid van de muis 0 is ofzo)
[X] makkelijke knopjes of shortcuts voor select all en select none (ctrl+a en ctrl+shift+a)
[X] delete selection mogelijkheid
[X] shortcut voor "delete selection" (del)
[X] adjust color curves functie (grafiek waarmee je oude op nieuwe waarde mapt, de grafiek mag je zelf met de muis tekenen)
[X] overweeg FFT library zoals http://www.fftw.org/ ---> 'tis "sing" geworden
[ ] stamp brush: kopieert een prentje overal waar je klikt. Dat is eenvoudigste vorm. Complexer: meerdere prentjes waartussen random gekozen wordt. Custom scale en rotation, eventueel random tussen bepaalde waarden. Opacity. Treat alpha as opacity.
[X] niet meer afsluiten bij drukken "esc"
[X] als prentje veranderd en niet gesaved, bij afsluiten, vragen of je het wil saven
[ ] in menu's, menu waar muis over is is blauw, maar in feite moeten ook deze waarvan submenu open is in blauw getoond worden (vergelijk met meeste GUI's in normale programmas)
[+] wanneer je iets doet op de tekening en muis gaat buiten boord van het venster, moet de tekening mee pannen. Telkens mee pannen als de tekening "grabbed" is zou hier goed werken. Maar soms zijn ook situaties waar muis niet ingedrukt moet zijn, bv bij plaatsen 2e punt van lijn... ---> MISSCHIEN NIMEER NODIG OM TE IMPLEMENTEREN: je kan met middle muis pannen zelfs tijdens andere dingen doen, en ook al door in en uit te zoomen
[X] geef interface van filters mogelijkheid error te geven als parameters niet juist zijn. In dat geval wordt string met error message aan user gegeven, en wordt niets in undo gesaved als nog niets gedaan was.
[X] aparte max value voor spinner en slider in de slider + spinner controls
[X] via slider brush step max tot 1.0 (ipv nu 2), via spinner veel hoger
[+] bij pijltjestoetsen voor pixel verplaatsing: sta 2 pijltjes tegelijk toe voor schuine bewegingen --> NEE NIET, want dan is het heel irritant als je niet schuin wil maar wel eerst op ene dan andere pijltje onmiddelijk drukt
[X] de brush, line, etc... settings onthouden overheen sessies
[X] het "new image" knopje uit toolbar laten werken, en venster tonen waarin je grootte van prentje kan kiezen, en die optie ook in file menu zetten
[ ] keuze tussen al dan niet afgeronde eindpunten van dikke lijnen
[X] outline of mouse drawing around square brush PIXEL-PRECISE and of circle and diamond brushes around center of drawing pixel over which mouse is instead of around mouse itself.
[ ] los bug op waarbij knopjes van spinner (in de dynamic slider spinner control) altijd werken, zelfs als je een ander gui onderdeel (bv slider) gegrabd hebt, terwijl het dan niet zou mogen
[X] opacity moet WEL invloed hebben op de pen tool --> en hoe je ook beweegt, er mogen geen overlappingen zijn als je niet effectief eigen pad kruist (tmoet regelmatig zijn)
[X] BUG: fix bug met crop als je selectie rechtsonder tot buiten image gaat
[X] "show image fullscreen" optie, waarbij prentje hele scherm inneemt (heeft nix te maken met fullscreen gaan van het outer window!). De "any key" brengt u weer terug.
[X] BUG: driehoek (filled), roze kleur, alpha 255, op witte achtergrond, alpha 255, met opacity 0.3 ofzo. Gevolg: alpha channel van resultaat is 253 a 254 ipv 255! Het moet 255 zijn hier, want niemand had iets anders dan 255.
[X] zorg in algemeen dat lijnen en driehoeken enzovoort met opacity mooi aansluiten zonder overlappingen. MAW: driehoeken perfect gerenderd, en lijnen moeten laatste pixel (eindpunt) NIET tekenen (volgende lijn begint met die coordinaten en tekent het wel), behalve natuurlijk bij user line tool waar hij wel verwacht dat die pixel er komt... allez ja kweet het niet zeker
[ ] polygon met fill en lijn dikte van exact 1: lijn sluit niet 100% aan aan de filled polygon. Fix dit.
[ ] "quit" knopje in de toolbar (helemaal rechts) ?
[X] keuze tussen meerdere extensies in SAVE dialoog. Dit is eigelijk keuze van type ipv extensie. De save operatie kijkt naar type ahdv index die je selecteert, niet naar de extensie letters. "all known formats" en "any file" zijn dan ook geen opties.
[X] hernoem "Particle" onder alpha filters naar "extract cloud" ofzo, en laat het uw voorgrond en achtergrond kleur gebruiken. Momenteel gebruikt het zwart als achtergrondkleur en wit als voorgrond. Zo kan je gekleurde voorgrond met antialiasing over gekleurde achtergrond extracten.
[X] extra optie voor de "eraser" tool om alpha niet naar 0 of 255, maar iets tussenin te brengen (bv fg en bg color)
[+] bug: ga fullscreen in flash applicatie in seamonkey. Druk ctrl print screen. Paste in dit programma. Blijft hangen in XNextEvent... En het is omdat het een incr property is.
[ ] in windows: alt+f4 programma laten afsluiten (in linux werkt da al, in windows nog ni)
[X] "thick pen": tool zoals de pen, maar houdt ook rekening met size en shape (maar niet met softness en step). Hij gebruikt step niet, maar gebruikt ander algoritme om te zorgen dat als je 1 strook trekt, hij niet meermaals over zelfde pixel tekent (maar als je lussen maakt gaat hij wel overtekenen, dus tis niet 100% een "niet buildup" brush) --> the pixel brush
[X] fix probleem waarbij circle brush altijd exact 1 pixel op bovenste en onderste rij heeft, wat eigelijk nooit een esthetisch resultaat geeft
[ ] extra opties bij saven naar PNG, BMP, ... (zoals color type, keep alpha channel, ...)
[ ] headerless image import & export (in PSP noemde dat vroeger "raw" maar dat heeft nu betekenis in digitale fotografie gekregen) --> vanalle settings, zoals: width, height, "planar" vs "interleaved", header size, line bit alignment, colortype (1,2,4,8-bit, G+A, RGB, RGB+A), upside down
[X] alignment image resize: waarbij pixels aan einde van rij opschuiven naar volgende rij of omgekeerd (bv als je raw image waarvan je grootte niet kent inlaadt en dan zie je visueel wat het is, kan je dit gebruiken om het te fixen) ---> dit kan ook optie in de resize dialoog worden
[X] if image format supported for decoding but not for encoding, when using "save", propose saving to known format (png), and change filename to have new extension (e.g. simply do the "save as" behaviour)
[X] support command line parameter: voor openen van image vanuit Windows of Linux file manager
[X] voor alpha channel: filter om het makkelijk meer en minder transparant te maken, zowel mutiply als add/subtract versie als inverse multiply (waar opaak blijft opaak)
[X] grootte van rectangular selectie (in pixels) onderaan weergeven
[X] posterize filter: posterization is het aantal mogelijke tinten verminderen (dus: effect van minder bits per color channel)
[ ] convert to X colors filter: met of zonder dithering enz...: hij kiest X beste kleuren uit en converteert alle pixels naar die kleuren (zo goed mogelijke benadering)
[X] combine the "scanlines" filters and add a settings dialog
[X] preview in filter dialoogjes (van filters die preview ondersteunen). Checkboxke "update in realtime" --> "isChanged()" nodig op filter dialogs. Status checkboxke is globaal en gesaved in user settings.
[X] "normalize" filter, die alles (onafhankelijk per channel) uitstrekt tussen min en max value naar 0 en 255 --> Als user: gebruik in combinatie met "image arithmetic"'s "brightest" en "darkest" opties voor uitbijters uit image te halen
[X] undo paste operatie
[X] onderscheid plakken van extern en van intern (voor paste operatie), oftewel de mogelijkheid ook te uploaden naar clipboard
[+] bug: eraser met size 1 werkt niet, en sommige andere 1-pixel brush dingen ook niet...
[X] bug: als selectie halfweg buiten image is gesleept, werken sommige dingen van undo niet meer: sleep selectie half buiten image. Unfloat selectie. druk undo. Probeer de selectie nu te slepen: deel van buiten de image komt niet mee.
[X] ctrl+scrollwheel kan dynamisch setting veranderen ipv zoomen. BV brush size, opacity, color lightness/saturation/hue, ... (instelbaar)
[X] systeem om de vensterkes in programma niet te ver buiten main venster te kunnen draggen, want ze zijn moeilijk terug binnen te brengen dan
[X] BUG: moderne laptops hebben resolutie in hoogte die minder dan 768 is. Pak 700 pixels ofzo als standaard hoogte. En de tool vensterkes bovenaan ipv onderaan, en daaronder de painting
[ ] verwijder de "step" setting van brush, is verwarrend en enkel daar wegens technische tekortkomingen. Beter algoritme nodig om te zorgen dat brush exact 1x over elke pixel gaat (maar niet zoals "non build up" brush, als je lussen maakt overtekent het wel 2x de zelfde plek)
[X] pak IFFT van volledig wit prentje van 501*401 pixels. In het resultaat zit wat data. Dat zou niet mogen, originele image was volledig wit...
[X] pixelrooster tonen op ingezoomde image (optioneel)
[X] variant op pixelrooster: rooster met custom size tonen (bv 16 pixels) --> voor tile pattern tekeningen (zoals icons_tools.png zelf) makkelijker te kunnen maken
[X] check of op Windows CTRL+Z niet CTRL+W wordt. Zoja, maak "azerty" optie in de settings dialoog.
[X] "wrapping" optie voor alle scale algo's, zodat ze voor tiling textures kunnen gebruikt worden
[X] remove "wrapping" and "affect alpha" options from global tool settings. Make them a separate setting in each filter that uses them.
[+] "alpha as opacity" and "tolerance" go to custom tool settings, so the "Other" tab in global settings, becomes a "Custom" tab in non global settings
[ ] save custom user defined filters in txt files and recent file history in the user defined filter dialog (txt format: "name" (string), num rows (7, int), num cols (7, int), the values (49 values, double) with whitespace between each
[X] doe iets aan de lange kommagetallen die je vaak krijgt uit sliders met doubles. Zorg dat de waarde veelvoud van de step van de spinner wordt ofzo...
[X] close knopje van filter dialoog zelfde als cancel
[X] ctrl+t shortcut voor crop
[X] ctrl+e shortcut voor paste as selection
[X] tab toets selecteert volgende item in dynamic page, en selecteert tekst van input boxen
[X] tab toets selecteert volgende item in "user defined" filter dialog en selecteert text van input box
[X] generate raster filter (ietwat gelijkaardig aan tiles, maar met rooster waartussen niets is) --> generate grid
[X] random noise: optie om ook random opacity per noise pixel te hebben
[X] convert to/from floating point texture
[X] FFT on floating point texture
[ ] ALL filters support floating point texture
[ ] ALL tools support floating point texture
[ ] selection on floating point texture
[X] color picker on floating point texture: must be able to show colors > 255 or < 0
[X] file open image reverts back to non floating point texture if the opened image isn't FP
[X] OpenEXR format for floating point texture (als plugin)
[ ] OpenEXR plugin compilen voor windows
[X] RGBE format for floating point texture
[ ] rename "example128" format to "float" format
[ ] de standby moet ook werken als file of filter dialoog open staat
[ ] de standby moet NOG minder cpu gebruiken, en misschien zelfs nix tekenen
[X] preview van filter dialoog op floating point texture: berekening effectief op floating point laten doen, want resultaat is anders (bv bij color arithmetic II, 255 vs 1.0)
[X] tool settings tabs weg, enkel settings voor huidige tool tonen.
[X] help dialoog breder zodat pad van help file leesbaar is --> in de plaats is font smaller :)
[X] een inverse optie voor de Channel Switcher (bv van R of greyscale naar hue omzetten enzo)
[X] enkele seperators in sommige filter menus (bv image->color)
[X] Save naar TGA en herlaadt. De image is negatieve kleur en ondersteboven! Bug!!
[X] "absulute value" filter, die enkel op HDR images werkt. Maakt negatieve pixels positief, per channel. Bv voor HDR image na "sharpen" terug helemaal positief te maken...
[ ] "anticrop": verwijdert selectie uit image en houdt de rest, image wordt kleiner erna (hor, ver en both nodig)
[X] backup file saven als je file oversavet en backup file nog niet bestaat (met ~ in naam ofzo)
[X] inverse optie voor 2D transform
[ ] "hexagon" optie in de mosaic filter
[ ] rechthoeken ipv squares in de mosaic filter
[X] shortcuts en andere input controls customizable maken
[X] shortcut voor wisselen voorgrond en achtergrond kleur (x als in fotoshop)
[ ] ondersteuning voor speciale en gebrekkige muizen (laptop touchpad, muis zonder scrollwiel, ...) --> zoomen met +/- en, pannen met toetsencombinatie+pijltjes, en pan tool + zoom tool, en automatisch bewegen tijdens gebruik van tool en naar rand bewegen, kan helpen. Ook alternatief voor ctrl+scrollwheel (dynamic brushes).
[ ] ondersteuning voor speciale en gebrekkige toetsenborden: bv geen pijltjes, geen numpad
[ ] customizable toolbar buttons voor custom filters
[ ] Undo memory per image information dialoog ipv onderaan, samen met: image en selection memory, filenaam, bestandstype, grootte, color type (32 of 128 bit), ...
[ ] In the shortcut dialog: warn about or disallow duplicate shortcuts for multiple commands
[X] consider shortcuts "ctrl+v" and "ctrl+shift+v", instead of "ctrl+e" and "ctrl+v" for paste as selection and paste as new image
[X] make select all and select none true operations with undo, and add them next to delete selection operation
[ ] iets om kader met rounded corners en dergelijke rond prentje te doen
[ ] iets om reflectie onder prentje te creeeren, zoals reflectie in plastieken tafelblad (die fadeout) enzovoort
[+] bij "new image" moet selectie van vorige verdwijnen (als het in zelfde venster is tenminste, als er multi file support is is dees nimeer relevant)
[X] hang met cirkel van 2x2 (of is het 1x1) pixels (als lijndikte 1)
[X] apart from uniform noise, also gaussian noise
[X] if icons_toolbar.png or icons_tools.png isn't found, it segfaults. It should instead display a proper error and not segfault.
[X] when decoding an image, and searching for one that "canDecode", FIRST try the plugin decoders, then the built-in decoders. That way, plugin decoders can override the built in ones (e.g. for progressive JPEG support, ...)
[X] once there's a fully working polygon tool, the triangle tool can probably be removed.
[ ] creeer verborgen watermark
[X] support voor een textstring voor filter plugins
[X] support voor eender welk aantal parameters voor SimpleFilter plugins (ipv genummerde functies)
[ ] support voor matrix voor in Filter plugins
[X] maak tool window kleiner, bv. maar zo lang als nodig
[ ] voeg inverse optie toe aan perspective crop, waarbij hele image in de zone gaat ipv omgekeerd. Maak ook ergens duidelijk dat het geen lineaire perspectieftransformatie is.
[ ] maak perpsective crop een echte lineaire operatie (eventueel optioneel huidige methode houden)
[X] BUG: memory corruptie probleem EN undo probleem met de perspective crop tool
[ ] perspective crop: de rechtse en onderste pixels zijn niet mee in resultaat, maar moeten er wel mee in. Dus x1 en y1 plus 1 doen voor de berekening (test op icons_toolbar.png, probeer "save" disketje eruit te perspective-croppen). Maar euh, als je 90 graden draait dan is het nog anders...
[ ] 3D transform (voor perspectiefcorrectie, met 3x3 matrix die je kan invullen), met ook inverse optie --> als filter
[X] images van manual in aparte zip file releasen, ipv altijd samen met de rest (om filesize te beperken)
[ ] voor alle filters met matrix in gui: standaard knopjes voor matrix 90 graden te draaien (links of rechts), of te transponeren
[ ] layers
[X] double click voor polygon te eindigen werkt nimeer
[X] optie voor automatische achtergrondkleur van selectie (ipv bg), hij gebruikt kleur van linkerbovenhoek ofzo dan
[X] als je "new image" doet, dan moet de Paint terug een lege filenaam hebben ipv nog de vorige filenaam!
[X] shortcut voor default colors (white, black) terug te zetten
[X] status bar at bottom
[X] bij "paste as selection": de selectie moet in beeld verschijnen, niet in linkerbovenhoek van tekening
[ ] make the "alpha as opacity" setting of the selection tool, persistant
[X] filter die alle pixels een random nieuwe locatie geeft
[X] maak tool window 2 knoppen breed ipv 4, en maar zo lang als nodig (==> meer plek voor image)
[X] Make Plugins folder with capital letter. Folders with capital letter are for release, folders with small letter are for development.
[ ] selection bg color type: works for dragging, but not yet for deleting the selection. Use the custom bg color (auto, ...) there too
[ ] Color picker optie geven om automatisch previous tool te selecteren (zoals kolourpaint. Met v toets is te verwarrend)
[ ] In manual voorbeeld van pixel shadow doen adhv tool icons ofzo
[ ] In de manual de mask examples kleinere prentjes maken
[X] voor lijn, ellips en rectangle tool: ook draggen met mouse ondersteunen, als hij ziet dat hij dragt reageert hij anders
[ ] optie voor HDR prentjes aangaande welke range exact je op scherm zichtbaar maakt (0-1, 0-255, 0-10000, .....)
[ ] "Clear List" mogelijkheid in de recent files list
[ ] Sort Pixels filter. Sorteert pixels op bepaald criterium. Resultaat is gewoon van rechts naar links, van boven naar onder, met size van originele prent.
[X] Voor filters die fg en bg color gebruiken: maak type van dynamic rij voor de dynamic gui die FG en/of BG color van de settings rechtstreeks kan editen
[X] make LodePaint compatible with linux packaging systems (e.g. having the data files next to the executable isn't ideal for those)
[X] have a versioning system (other than the compile date), something starting with 0.1 or so --> the version system is a date system now (separate than compile date)
[X] support for editing images with a single color channel. NOT "indexed color greyscale". Instead a single-channel mode for both 32-bit (then 8-bit) and 128-bit (then 32-bit) images. Purpose: to edit the R, G, B, A and mask channel as separate entity when needed.
[X] "affect alpha" option for all the brushes and geometrical tools. If off, they only edit RGB, the alpha value of the color is ignored. This in preparation for layers where the alpha channel will be used as opacity.
[ ] BUG: it's possible to create images of width or height of 0 pixels (e.g. by using rotate 45 degrees on 1x1 image), and, these 0-pixel images can cause crashes, e.g. when using rescale
[ ] it's possible to open the same file multiple times. Prevent that.
[X] als programma net opgestart, en er is lege tekening waarop je nog niets hebt gedaan, dan moet open image in die tekening openen ipv in nieuw venster
[X] BUG: gaussian blur sets alpha channel to 253. Probably the matrix doesn't add up perfectly to 1.0.
[X] Let the toolbar icons support gui customization by making the toolbar (and status bar) not be drawn with grey. To test: negate "gui.png". Run LodePaint. Everything looks nice except the toolbar.
[X] filters en plugins: geef de parameters die in dynamische GUI komen via XML
[X] filters die bepaalde color mode (32-bit, 128-bit, ...) niet ondersteunen moeten message geven. MAW er moet naast de uitvoerings-functies, ook functie zijn die boolean terug geeft of hij het kan of ni.
[X] hernoem buttons16.png en buttons32.png naar icons_toolbar.png en icons_tools.png
[ ] Voorlopig om meerdere lijnen text te hebben met text tool, geeft 3 of 4 text input veldjes ipv 1, die dan gewoon onder elkaar gezet worden.
[ ] optie in text tool om text met integer waarden te resizen (de bitmap fonts)
[X] rename "32-bit" to "RGBA8" and "128-bit" to "RGBA32F" everywhere in the code
[X] rename the 32-bit and 128-bit methods of plugin filters also to RGBA8 and RGBA32F
[ ] filter similar to pixel shadow, but instead using the alpha channel. It does as if a (colored) light shines through the translucent picture and casts light on a background behind it, making things visible shifted behind the alpha channel image
[ ] optie in "pixel border" filter om enkel de border te behouden
[ ] 3D preview mode for textures, e.g. on a rotating cube and tiled in the background, with adjustable light color to see it in different circumstances
[X] het is momenteel erg vervelend dat prentjes die openen hun venster overheen de tools kan. Maak een soort "zone" in achtergrond die je kan vergroten en verkleinen waarin vensters komen en (als ooit ondersteund) gemaximaliseerd kunnen worden. OF ipv zone, bereken zone zelf uit de open images: mag ni groter worden dan dat of zo
[ ] voor multiple images: opties om vensters te tilen, maximalizeren, etc...
[X] make the menus a bit larger. They're quite hard to click and expand currently.
[ ] menu mnemonics
[ ] show random usage tips somewhere
[X] bij polygon tool: maak de "freehand" mode (met ingedrukt houden muisknop) optioneel met checkboxke
[X] gradient- en/of transparantie-effectje in menu bar. Of iets anders dat het er niet jaren '90 laat uitzien. Ook iets voor toolbar.
[ ] displacement map filter (uses a second image as source data to distort image, two color channels can be used for X and Y) --> using selection and/or MultiOperation
[X] sepia filter (just makes photo sepia, or duotone-like effect)
[ ] polar <--> rectangular coordinates filter
[X] filter die antialiasing doet, zonder te blurren, door scherpe schuine lijnen en boorden te herkennen en enkel daar te AA-en
[X] Antialiasing filter slimmer maken (lijnen met subtiele schuine hoeken, bv 5 graden, op mooiere manier AA-en dan enkel bij overgangspuntjes iets te doen)
[ ] filter op mask die hele mask op bepaalde waarde zet (dus hele mask semi doorlatend maakt), of alternatief, die mask vermenigvuldigt met bepaalde waarde
[X] contour filter: vindt contouren zoals hoogtelijnen op een kaart, ofwel door brightness, of door R,G,B apart
[ ] in the filter preview dialogs, add option to either show scaled image, or part of 1:1 image
[ ] when pasting as selection, automatically enable the rectangle select tool (so you can drag the pasted selection)
[ ] bug: when running the windows version of lodepaint in wine, in Linux, and making a screenshot in linux, then pasting in lodepaint, it crashes. So maybe there's something not so good in the win32 clipboard code.
[X] blend selection should have option about how to treat the alpha channel in the selection (as opacity or not)
[ ] preview in the blend selection dialog
[X] "Contours" moet ook met andere waarden dan lightness kunnen werken, zoals saturation, meer realistische (voor het oog) lightness (XYZ?), etc...
[X] pressing enter in filter dialog same as pressing ok with mouse
[ ] filter to convert selection contents to mask
[X] fix bug with FFT filter on floating point images (something with the shifting-
[X] fix bug met inladen of saven niet-macht-van-2 floating poing images (rgbe en float)
[X] als je probeert te croppen zonder selectie: geef message dat je er selectie voor nodig hebt
[X] swap two colors filter: swapt twee kleuren op exacte waarde. Soms handiger dan manueel te swappen met 2x replacen
[ ] voor uitvoeren van filter, save/load image, en andere zware operaties: berichtje in statusbaar plaatsen (en screen repainten zodat het zichtbaar is)
[X] mirror-tile filter: mirrors the image around x and y axis and both, and places these 4 copies in one new image (twice as large in x and in y direction), the result is tileable (but not in a good looking way)
[ ] copy en paste op andere color modes dan RGBA8
[X] copy en paste van clipboard in text input fields (Linux)
[ ] copy text to the real clipboard in linux
[X] copy en paste van clipboard in text input fields (Windows)
[ ] fix clipboard probleem met INCR van Qt apps
[ ] menu-, tool- en statusbar een beetje lichter grijs van kleur
[ ] kolourpaint selecties kunnen achtergrondkleur als transparant zien. Heel handig. Probeer dat ook te kunnen.
[X] de venters rond een tekening moeten kleiner worden indien mogelijk (als de tekening klein is), zeker als het een nieuw venster met nieuwe tekening (geladen, gecreeerd of gepaste) is
[X] als je venster resized, moet tekening mee bewegen zodat hij zichtbaar blijft als hij dat al was
[ ] niet-rechthoekige (freeform) selecties voor het verplaatsen van dingen --> bv polygonen (implementatie met driehoekige stukken)
[X] bug in linux (misschien ook windows): open image van recent list. Open dan onbekende file van recent list (zodat bericht "unknown file format" komt). Vaak zijn dan sommige letters in menus enzo kapotte texture. Iets corrupteert het geheugen van opengl!
[ ] option to have multiple paintings be as tabs in a single window, instead of as multiple floating windows
[X] deel image->colors menu op in 2: er zijn teveel opties in 1 menu (bv een colors submenu onder filters voor de meer geavanceerde effecten)
[ ] geef plugin filters mogelijkheid om willekeurige submenu naam te hebben
[X] maak % teken van de built in font beter. Het is heel onleesbaar naast nummers nu (zie status bar van zoomed image)
[X] mogelijkheid om operatie te doen op alle open images.
[ ] mogelijkheid om alle open images tegelijk te saven, naar NIEUWE filenaam (prefix en postfix): "save all in directory", met nieuwe dialoog waarin je dir, prefix, postfix kan geven, en file type(s) (bv PNG&LPI, hetwelk PNG gebruikt voor gewone images, LPI voor HDR images)
[X] mogelijkheid om meerdere files tegelijk te openen met de file dialoog
[ ] color dialoog optie om kleuren in range 0.0-1.0 ipv 0-255 te geven (of automatisch zo bij HDR images)
[X] mogelijkheid om ergens volledig pad van de settings.txt file te zien in de GUI onder opties ofzo
[ ] Direct3D version for Windows
[X] een simpele save all button in toolbar en optie in file menu
[ ] submenus in options menu met voorgedefinieerde color themes
[X] speciale graphic rond muis wanneer color picker actief is, om makkelijker te herkennen dat die tool actief is
[X] idem voor selectie
[X] Volgende filters verdienen toolbar iconen (samen met negate in 2e toolbar): gaussian blur, blend with selection, wrapping shift 50%, alpha opaque, apply alpha to bg, color normalize, color brightness contrast, transform flip x en flip y, rescale, clear
[X] Ontvang ook image/bmp images van clipboard, bv openoffice gebruikt dat ipv png
[ ] Openoffice is GEEN qt app maar kan toch INCR images van Qt clipboard verkrijgen! Bestudeer hoe zij dat doen
[X] fix bug: "adjust" tool met square vorm en size 1 en softness 1.0, maakt alpha channel onzichtbaar! En bij hue mode, maakt hij de kleur zwart.
[ ] polygonen met lijndikte 1 errond goed ondersteunen
[X] warn if "OpenGL Generic" driver is detected in Windows. Ask to install video drivers then.
[X] in de program dirs dialoog: in windows is zowiezo lang path voor settings.txt. Doe er iets aan dat lange teksten buiten venster komen. Zort dat er deftige text rendering in kader komt. breek af op einde ofzo.
[X] completely redesign the color selection panel (with a small rectangular HSV chooser etc...)
[ ] make "reload" undoable (or if not warn that changes will be lost when you reload)
[X] selective gaussian blur filter (applies gaussian blur based on contrast threshold)
[ ] driehoekige selectie (en andere polygonvormen via meerdere driehoeken intern)
[X] sometimes the file dialog forgets the last location when opening multiple images in a row. It goes to the parent directory for some reason. Maybe it doesn't save the current dir depending on if you press OK, enter or double click the file?
[ ] oftewel KDE clipboard probleem fixen oftewel import screenshot optie in linux
[ ] positie prentjes is ni goed na croppen en zelfs na openen image. Moet mooi in centrum staan.
[ ] optie om warning over generic OpenGL drivers af te zetten
[ ] als icons_toolbar en icons_tools niet gevonden, gebruik generische iconen (ipv af te sluiten)
[ ] HDR noise filter
[X] don't distribute the dynamic libraries of the example32 and example128 file format plugins anymore with a standard LodePaint release. But do distribute the OpenEXR and jpeglib ones.
[ ] when pasting a selection, sometimes it appears too far in the top right. It should be a bit more towards the center.
[X] bij save as van RGBA8 prentje, moet PNG als default keuze staan. Ook al zijn plugins geladen zo dat ze ingebouwde file extensies overriden, dan nog meot PNG eerste keus zijn (en niet "example32"). Dus volgorde moet zijn: PNG, native (LPI), plugins, andere built-in formaten
[X] er is iets mis met undoen van "ctrl+e"
[X] alle horizontale color sliders zijn geinverteerd!
[ ] maak zichtbaar adhv stijl titelbaar window welke tekening focus heeft
[ ] voeg optie toe dat focus automatisch muis volgt (zelfs zonder clicks)
[X] scrollen (zoomen) moet zowiezo focus naar die tekening brengen
[X] als je 2x achter elkaar ctrl+e drukt, moet vorige selectie niet verdwijnen
[ ] selectie maken en ctrl+c gebruiken, moet image nog NIET als "modified" aanduiden
[X] maak clipboard van AROS zo dat hij tenminste al intern copypasten ondersteunt (maak standaard clipboard klasse die dat kan voor alle OSen, default implementatie)
[ ] De dynamic brush change met andere toets dan CTRL, want: Keuze in opties tussen: scrollwiel zoomt en ctrl+scroll v-scrollt, of, ctrl+scrollwiel zoomt en scroll v-scrollt.
[ ] Floating Point TIFF Support
[ ] Floating Point FITS Support
[ ] apart from the square pixel grid, support an isometric pixel grid
[X] make tool buttons 16x16 instead of 32x32 pixels, and so make the tool window smaller
[+] make the windows on the right (with color selecter and such) also smaller (less wide)
[X] add a window on the right with an overview of the whole painting and rectangle showing where you're zoomed
[X] glDeleteTextures wordt nog steeds teveel opgeroepen. Fix dit, anders crasht het op amiga.
[X] close all menus just by pressing right mouse button on any place of lodepaint window. For now, it closed only if you press by left mouse button on working area, and close not all the menus, but one by one.
[X] als color picker actief en dialoog is boven tekening, zie je pipet symbool nog steeds naast muisaanwijzer
[X] shortcut voor pixel brush
[X] remove hidden "alternateLayout" flag
[ ] remember position of all GUI panels between sessions
[X] generate tiles filter: aparte opacity voor beide kleuren
[X] include manual.html and the css file in the manual_src folder and let the makemanual tool copy it to the destination too
[X] simpele filter voor seamless maken die fadeout versie van zichtzelf in 4 hoeken erover legt
[X] support DNG (digital negative) file format --> plugin
[X] deleten van selectie kan crashen!!!!! (bij deleten selectie die buiten scherm komt in combinatie met undo)
[ ] a flag in filters that decides if the preview in the dialog is zoomed out, or showing a part of the image at per-pixel scale
[ ] iets om ergens makkelijk volledig pad van image te zien (nu toont venster enkel filename zelf)
[ ] je kan geen selectie maken die start in rechterboven pixel van prentje, fix dat
[ ] message box moet op de enter toets reageren
[+] na rescale (kleiner maken, bilinear), verschijnt er transparantie in prentje, ook al was het opaak! bug te fixen!
[ ] ctrl+tab en ctrl+shift+tab voor switchen tussen tekeningen
[ ] "generate test image" filter (parameterloos, genereert testbeeld met grootte van huidige image)
[ ] "reciproke" filter voor HDR (naast de absolute value filter)
[X] Ook de SDL versie outputten (ipv enkel OpenGL versie)
[ ] Indien mogelijk ook Windows en Linux versie strings ergens vandaan halen en outputten
[X] voor de "pointillism" filter: keuze uit 4 soorten achtergronden (ipv 2 zoals nu): -bg color -100% transparent color (en kleurloos, dus beinvloedt brush color niet bij interpolatie) -originele prentje zelf -geblurde versie van originele prentje zelf (hoe hard geblurd is niet instelbaar, is gewoon vaag achtergrondpatroon dat ietwat zelfde kleur heeft als prentje daar, en ik denk dat ik geen gaussian blur, maar mosaic met interpolatie ga gebruiken daar) --> enkel blurred image en canvas color gehouden. Rest is nutteloos en verwarrend.
[ ] probeer warnings (die naar std::err gaan) van DNG formaat af te zetten
[ ] omzetting van of naar Lab kleurenmodel is heel inefficient (probeer maar eens de negate... color model filter en gebruik lab parameter, en vergelijk dat met hsl parameter)
[ ] filter die schuine afwisselende strepen maakt (met instelbare hoek, standaard 45 graden)
[+] bilateral filter --> de selective gaussian blur is al zoiets
[ ] geef alle "grey" in imalg ook een "pixel size" of dergelijke parameter, zodat je die operaties bv ook kan gebruiken op 1 kanaal van een rgba image (door dan "4" te geven, dan skipt hij dus telkens 4 waardes)
[ ] window icoontje met SDL_WM_SetIcon
[X] toolbar button for "adjust hsla" or "adjust hsva"
[ ] toolbar buttons for the 4 color modes
[ ] toolbar buttons for rotate 90 degrees left and right
[ ] choice for hsl instead of rgb sliders in the color panel
[X] option to lock X and Y scale of julia and mandelbrot fractals (instead of independently scaling both depending on image size)
[X] bug waarbij het moelijk is nummerkes te typen in filter parameters, vooral als veld leeg is (maar passop dat de fix geen andere dingen breekt, zoals synchronizatie enz)
[ ] "repeat last filter" knopje: twee versies ervan, eentje met dialoog, eentje met laatste onthouden settings
[ ] indien mogelijk, tileable "generate clouds" filter
[ ] Mac version
[ ] "keep only this color" filter, die alle pixels de achtergrond kleur geeft, behalve deze die al de voorgrondkleur hebben
[X] geef de color chooser dialog (die komt als je dubbelclick op FG of BG color) een cancel button
[ ] MIME types for file formats to make different encoders and decoders recognise they doing the same format as another one. See list further in this document for the MIME types of the currently supported file formats.
[X] Use Windows file dialog in Windows
[X] in "see program dirs": de skins folder toont niet de "/Skins" erbij. Zou wel moeten, anders is het verwarrend.
[X] er is geheugen lek met skins: telkens je skin wisselt, komt er geheugen bij maar gaat er geen weg. Ook afsluiten programma duurt langer als veel skins gewisseld.
[ ] iets in "user defined filter" voor de matrix te normalizeren (zodat som van alles 1 is)
[X] dodge & burn filters --> added to blend selection
[X] in filter controls, the blue selection indication over selected text next to sliders is too soft, make it more visible. Maybe give the text input fields a white background.
[ ] brushes met custom vormen (bruikbaar voor elke brush tool, eventueel soort refactoring van hele brush systeem doen)
[ ] there's a memory leak after loading a DNG file. Close it, LodePaint still has more memory allocated than before. Maybe forgot to delete a certain buffer the DNG framework creates.
[ ] in de hue/sat/lightness dialoog: filter gebaseerd op hue van oorspronkelijke kleur
[X] nieuwe bug: probeer te typen in text input field van de text tool. Typ een "e". De eraser wordt geselecteerd. Open dan de file dialog en plaats cursor in text input field. Oneindig veel e's worden gespamd!
[ ] linux file dialog: via .so file, die .so heeft dan wel rechtstreekse link naar gtk (en andere naar qt)
[ ] tab ook laten werken in de RGBA color sliders om naar volgende input veldje te springen
[ ] In Windows when pressing cancel in the file dialog, then mouse focus of the SDL window is screwed up... Either the mouse is in the window, but the titlebar doesn't work, or vice versa.
[ ] when opening image that is small, make the window around it smaller too
[ ] tooltips of something on the right side, disappear of the screen. This is annoying with the tools panel. Fix this.
[X] make CTRL + left/right arrow work in text controls
[ ] loading the true color gifs from this site makes it crash: http://phil.ipal.org/tc.html. Should at the very least not crash.
[ ] fix bug waar mask brush enkel mask kan wegdoen, maar niet kan toevoegen (altijd waarde 0...)
[ ] fix bug waar bij verkleinen van programmawindow, paintings erin belachelijk klein worden door mee te resizen
[ ] gebruik SDL 1.3/2.0 eens finaal gereleased
[ ] numpad zero as shortcut for 1:1 zoom
[ ] in the extended color picker, show alpha channel to in the preview chip
[ ] make dark theme the default, and also matching bg color
[ ] toon ergens full file path van image ipv enkel filename
[X] maak enkele preset matrices in alle filters met een matrix: RGBA matrix, 2D transform, en user defined
[ ] BUG in de swap mask with alpha filter met undo en redo: probeer maar uit
[ ] bug: als je filter doet terwijl geen enkel image open, en dan open je image, voert hij plots de filter uit op die image
[X] bug: make seamless filter introduceert alpha waarden van 254
[ ] add more possible channels to the contours filter
[ ] allow multi-contour in the contour filter: if enabled, you specify a shift and a distance between two successive contours. If disabled there's one contour at the given value. Or use two separate filters for this: Contour and Multi Contour.
[ ] make ctrl+scrollwheel zoom instead of do brush dynamics (because it's such common key combo), use something else for brush dynamics (shift and alt are both free for this)
[ ] ctrl+tab en ctrl+shift+tab om tussen tekeningen te switchen. De actieve wordt gemaximalizeerd en komt naar voor
[ ] operation to swap selection and image
[ ] operation to set channel from selection into color channel of image
[ ] bug: adjust brush creeert vanalle bizarre kleuren zoals rood in greyscale image
[ ] bug: adjust brush creert alpha 254 waardes in opaque prentje
[ ] add option to choose between tabbed layout or free floating windows for the paintings
[X] bug: wrapping gaussian noise is not wrapping: try it on random noise (and idem with interpolating blur)
[ ] bug: select all moet selectie tool activeren
[ ] bug: select all en deep zoom ==> het wordt traag, waarschijnlijk door alle stippellijntjes van de selectie rand te doorlopen ook al zijn ze niet zichtbaar...
[ ] bug: drag corners werken doorheen window top ==> perongeluke activering ervan
[X] bug in Windows: LodePaint window start ergens in midden met delen buiten uw scherm, als het gemaximalizeerd was voor het afsluiten
[ ] save de "erase content" setting van brush
[X] bug in color picker: alpha slider staat in centrum en zonder getal in advanced tab, terwijl alpha 255 is in de main tab
[ ] crop-draggen van image mag niet in "last used filter" knop komen, nu is dat het geval
[ ] knopje in toolbar voor greyscale (de luma versie)
[ ] window menu met opties zoals cascade e.d. voor open paint windows
[ ] swap selection met image eronder
[ ] bug: lightness brush changet alpha
[ ] filter die alpha voorbereidt voor interpolatie, dat wil zeggen: 100% transparante kleuren  moeten RGB waarde van gekleurde buren krijgen ipv andere

FILE DIALOG TODO:

[X] pressing enter in file dialog same as pressing ok with mouse --> only when editing the file path text
[X] pressing numpad enter ALSO same effect as pressing enter
[X] Als je file open of save as doet, in file dialoog naar folder van momenteel open prentje gaan ipv naar root folder of laatste folder --> gedaan voor save, niet voor open (want bij open gaat het niet meer over de zelfde image, je kan er meerdere open hebben!)
[ ] Indien file dialoog in ander geval opent dan vorige puntje, dan folder onthouden, in de settings file, die je laatst geopend had in vorige sessie...
[ ] Sorteren op datum in file dialoog
[ ] ondersteuning voor file links in linux file browser
[X] favorieten in de file dialoog (zo'n lijstje aan linkerkant, standaard moet er root en home staan (home is my docs in windows), en de rest customizable) --> het is de "sug" dropdown box geworden
[ ] Toon "." nooit in de file dialoog. ".." wel want die kan nuttig zijn, maar "." is dat niet.
[X] Optie in save file dialog voor al dan niet extensie automatisch toe te voegen aan filenaam als hij er nog niet is. (checkboxke "automatically add extension to filename")
[ ] in de save dialoog optie voor bit depth van prentje. Hiervoor algemeen de file dialoog uitbreiden met optionele extra gui component die je er onderaan kan bijpluggen. Deze moet aan een beetje info van de file dialoog aankunnen (bv welk type je hebt geselecteerd, voor aantal ondersteunde bits in lijstje aan te passen)
[ ] als "save as" dialoog geopend wordt, moet in file vakje de filename al in staan van de naam die het nu is (als het prentje al gekende filename heeft). En uiteraard moet het ook in de juist folder staan!!! (bv ook als je image openende vanuit recent files menu). En ook type moet juist staan (de extensie selector combobox)
[X] het scrollen in file dialog: lijkt anders in Windows en Linux. Anyway, in windows hangt scrollsnelheid af van aantal files. Veel te snel als veel files in lijst. Maak scrollen zo dat altijd zelfde aantal files verder gescrolld wordt voor zelfde duur van op scroll knopje duwen.
[X] in save dialog, the text input box must be active whenever it opens
[X] close knopje van file dialoog moet zelfde doen als cancel
[ ] hide hidden files optie (op linux want daar is dat simpel: begint met puntje)
[ ] met scrollwiel scrollen in file dialoog gaat te traag
[ ] soms is file dialoog in begin op rare positie gescrolld, al 5 files naar beneden ofzo
[ ] letter typen in lijst moet naar file die met die letter begint scrollen
[X] image preview in de file dialog
[X] image preview in de Windows file dialog
[ ] met letters in list typen gaat naar file die met die letters begint
[ ] in suggested folters, moet een die er al in stond terug naar boven gaan als je die nu gebruikt ipv onderaan te blijven
[ ] bij save as, vul wel de huidige filenaam al in de input field
[ ] clear mask in menu toolbar
[ ] net zoals de nieuwe --datadir en --plugindir arguments, heb er eentje voor config txt file. maar dat gebruikt lpi_persist dus die eerst flexiebeler maken...
*/


/*
Plugin refactoring: wat mogelijk moet zijn:

*filter plugins*

-meerdere filters in 1 plugin
-submenus in plugins menu
-...

*file plugins*

-support voor parameters bij zowel encoderen als decoderen
-support voor meer keuze tussen rgba32, rgba128, ...
-wat meta info van prentjes kunnen doorgeven (als string pairs bv)
-prentjes met meerdere frames of layers (bv animated gifs) kunnen laden met info over aantal frames
-...

*beide*

-parameters via xml & standaard structje
-acknowledgement string voor in de about box

*/



/*
MIME types:

PNG: image/png
JPEG: image/jpeg
GIF: image/gif
BMP: image/x-bmp (and many others)
TGA: image/tga (and many others)
RGBE: image/vnd.radiance
Digital Negative: image/x-adobe-dng
OpenEXR: image/exr
built-in LPI file format: (here I need to make something up): image/x-lodepaint-lpi
*/
