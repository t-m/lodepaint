/*
LodePaint

Copyright (c) 2009-2010 Lode Vandevenne
All rights reserved.

This file is part of LodePaint.

LodePaint is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LodePaint is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LodePaint.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "paint_gui.h" 

#include "main.h"
#include "paint_gui_items.h"
#include "lpi/gui/lpi_gui_drawer_gl.h"
#include "lpi/lpi_parse.h"
#include "lpi/lpi_translate.h"

////////////////////////////////////////////////////////////////////////////////

void paintDebugMessage(const std::string& message)
{
  std::cout << "PaintDebug Info: " << message << std::endl;
}

////////////////////////////////////////////////////////////////////////////////

WindowInsideKeeper windowInsideKeeper;

void WindowInsideKeeper::handle(lpi::IInput& input, int width, int height)
{
  (void)input; //can be used to make the effect time dependent

  for(size_t i = 0; i < elements.size(); i++)
  {
    lpi::gui::Element& element = *(elements[i]);
    int x = element.getX0();
    int y = element.getY0();

    if(element.getX0() >= width) x = width - 1;
    if(element.getX1() < 0) x = -element.getSizeX();
    if(element.getY0() >= height) y = height - 1;
    if(element.getY0() < 0) y = 0;

    if(element.getX0() > width - DIST) x--;
    if(element.getX1() < DIST) x++;
    if(element.getY0() > height - DIST) y--;
    if(element.getY0() < DISTTOP) y++;

    element.moveTo(x, y);
  }
}

void WindowInsideKeeper::handleMainContainerResize(int oldw, int oldh, int neww, int newh)
{
  (void)oldh;
  (void)newh;
  
  int diffx = neww - oldw;
  //int diffy = newh - oldh;

  for(size_t i = 0; i < elements.size(); i++)
  {
    if(elements[i]->getX0() > oldw / 2) elements[i]->move(diffx, 0);
    //if(elements[i]->getY0() > oldh / 2) elements[i]->move(0, diffy);
  }
}

////////////////////////////////////////////////////////////////////////////////


bool MyModalFrameHandler::doFrame()
{
  if(!lpi::frame(true, true)) return false;

  if(lpi::resizeEventHappened())
  {
    h.recreateScreen(lpi::resizeEventNewSizeX(), lpi::resizeEventNewSizeY(), (bool)h.optionsNow.fullscreen, (bool)h.optionsNow.resizable);
  }
  standby.handle(h.guidrawer->getInput());
  if(standby.isStandBy(h.guidrawer->getInput())) guidrawer.drawText("STANDBY", (bool)h.optionsNow.screenWidth / 2 - 32, (bool)h.optionsNow.screenHeight / 2 - 4);

  screen.redraw();
  screen.cls(lpi::RGB_Black);
  guidrawer.frameEnd();

  guidrawer.frameStart();
  return true;
}

lpi::gui::IGUIDrawer& MyModalFrameHandler::getDrawer()
{
  return guidrawer;
}

void MyModalFrameHandler::getScreenSize(int& x0, int& y0, int& x1, int& y1)
{
  x0 = 0;
  y0 = 0;
  x1 = screen.screenWidth();
  y1 = screen.screenHeight();
}

////////////////////////////////////////////////////////////////////////////////

BulletListWrapping::BulletListWrapping()
{
}

BulletListWrapping::~BulletListWrapping()
{
  clear();
}

void BulletListWrapping::clear()
{
  for(size_t i = 0; i < bullet.size(); i++) delete bullet[i];
  bullet.clear();
}
    
void BulletListWrapping::drawImpl(lpi::gui::IGUIDrawer& drawer) const
{
  drawBullets(drawer);
}

void BulletListWrapping::handleImpl(const lpi::IInput& input)
{
  if(!mouseOver(input)) return; //some speed gain, don't do all those loops if the mouse isn't over this widget

  handleBullets(input);
}

const lpi::gui::Element* BulletListWrapping::hitTest(const lpi::IInput& input) const
{
  const lpi::gui::Element* result = ic.hitTest(input);
  if(result) return result;
  return lpi::gui::Element::hitTest(input);
}

void BulletListWrapping::make(unsigned long amount)
{
  this->setEnabled(true);

  clear();
  for(unsigned long i = 0; i < amount; i++)
  {
    bullet.push_back(new lpi::gui::Checkbox);
    bullet.back()->make(0,0,0,0);
    addSubElement(bullet.back());
  }
}

////////////////////////////////////////////////////////////////////////////////

void ToolWindow::make(int x, int y, const lpi::gui::IGUIDrawer& geom, const ToolsCollection& tools, MainProgram& h)
{
  static const int NUMTOOLBUTTONS = 32;

  addTop(geom);
  addTitle(_t("Toolbox"));
  //addCloseButton(geom);
  addResizer(geom);
  setColorMod(lpi::ColorRGB(255,255,255,192));
  
  
  toolButtons.make(NUMTOOLBUTTONS);
  pushTopAt(&toolButtons, 6, 8, lpi::gui::STICKYFULL);

  for(int i = 0; i < NUMTOOLBUTTONS; i++)
  {
    toolButtons.bullet[i]->setTexturesAndColors(&textures_tools[0], &textures_tools[0], &textures_tools[1], &textures_tools[1], lpi::RGB_White, lpi::RGB_Grey, lpi::RGB_White, lpi::RGB_Grey);
  }

  for(size_t i = 0; i < tools.getNumTools(); i++)
  {
    toolButtons.bullet[i]->addFrontImage(tools.getTool(i)->getIcon());
    toolButtons.bullet[i]->resize(0,0,24,24);
    h.c->getToolTipManager().registerElement(toolButtons.bullet[i], _t(tools.getTool(i)->getToolTip()));
  }
  
  //resize(x, y, x + 140, y + (NUMTOOLBUTTONS / 4) * 32 + 28);
  resize(x, y, x + 140, y + 80);

  //toolButtons.setCorrectSize();

  toolButtons.set(2);
}

////////////////////////////////////////////////////////////////////////////////

ColorWindow::ColorWindow(const lpi::gui::IGUIDrawer& geom, Options& options)
: options(options)
, rgb_fg(true)
, rgb_bg(true)
, colordialog_fgbg(geom)
, colordialog_palette(geom)
, pallist(geom)
{
  rgb_fg.setPlane(lpi::gui::ColorEditor::FG);
  rgb_bg.setPlane(lpi::gui::ColorEditor::BG);
  resize(0, 0, 300, 150);
  addTop(geom);
  addTitle(_t("Color"));

  setColorMod(lpi::ColorRGB(255,255,255,192));

  colorSynchronizer.add(&rgb_fg);
  colorSynchronizer.add(&rgb_bg);
  colorSynchronizer.add(&fgbg);
  
  initPalettes();
  
  for(size_t i = 0; i < palette.size(); i++)
  {
    colorSynchronizer.add(palette[i].palette);
    palette[i].palette->setDontAffectAlpha(true);
    palette[i].palette->setColorChoosingDialog(&colordialog_palette);
    pallist.addItem(palette[i].name);
  }
  
  if(options.selectedColorWindowPalette >= 0 && options.selectedColorWindowPalette < (int)pallist.getNumItems()) pallist.setSelectedItem(options.selectedColorWindowPalette);
  else pallist.setSelectedItem(3); //default

  fgbg.setColorChoosingDialog(&colordialog_fgbg);
  
  for(int i = 0; i < (int)palette.size(); i++)
  {
    pushTop(palette[i].palette, lpi::gui::Sticky(0.0,4, 0.8,0, 1.0,-4, 1.0,-4));
    if(i != 3) palette[i].palette->setEnabled(false);
  }
  pushTop(&fgbg, lpi::gui::Sticky(0.07,4, 0.05,4, 0.32,-4, 0.55,-4));
  pushTop(&rgb_fg, lpi::gui::Sticky(0.5,4, 0.0,4, 1.0,-4, 0.4,-4));
  pushTop(&rgb_bg, lpi::gui::Sticky(0.5,4, 0.4,4, 1.0,-4, 0.8,-4));
  pushTop(&pallist, lpi::gui::Sticky(0.0,4, 0.8,-20, 0.4,-4, 0.8,-4));
  
  opaque.makeText(0, 0, "O", geom);
  pushTop(&opaque, lpi::gui::Sticky(0.4,6, 0.8,-20, 0.4,18, 0.8,-4));

  setLeftColor(lpi::RGB_Black);
  setRightColor(lpi::RGB_White);
}

ColorWindow::~ColorWindow()
{
  for(size_t i = 0; i < palette.size(); i++)
  {
    palette[i].cleanup();
  }
}

void ColorWindow::handleImpl(const lpi::IInput& input)
{
  Window::handleImpl(input);
  
  if(pallist.hasChanged())
  {
    int i = pallist.getSelectedItem();
    options.selectedColorWindowPalette = i;
    if(i >= 0 && i < (int)palette.size())
    {
      for(int j = 0; j < (int)palette.size(); j++) palette[j].palette->setEnabled(false);
      palette[i].palette->setEnabled(true);
    }
  }
  
  if(opaque.pressed(input))
  {
    lpi::ColorRGBd fg = fgbg.getFG();
    fg.a = 1;
    lpi::ColorRGBd bg = fgbg.getBG();
    bg.a = 1;
    
    colorSynchronizer.setMultiColor(lpi::gui::ColorEditor::FG, fg);
    colorSynchronizer.setMultiColor(lpi::gui::ColorEditor::BG, bg);
  }

  colorSynchronizer.handle();
  
  //only set the recent colors while not busy selecting them
  if(!input.mouseButtonDown(lpi::LMB) && !input.mouseButtonDown(lpi::RMB))
  {
    lpi::gui::MultiColorPalette& recent = *palette[2].palette;

    lpi::ColorRGBd left = getLeftColord();
    if(left != recent.getInternalColor(15))
    {
      bool has = false;
      for(size_t i = 0; i < 32; i++)
      {
        if(recent.getInternalColor(i) == left)
        {
          has = true;
          break;
        }
      }
      if(!has)
      {
        for(int i = 0; i < 15; i++) recent.setColor(i, recent.getInternalColor(i + 1));
        recent.setColor(15, left);
      }
    }

    lpi::ColorRGBd right = getRightColord();
    if(right != recent.getInternalColor(31))
    {
      bool has = false;
      for(size_t i = 0; i < 32; i++)
      {
        if(recent.getInternalColor(i) == right)
        {
          has = true;
          break;
        }
      }
      if(!has)
      {
        for(int i = 16; i < 31; i++) recent.setColor(i, recent.getInternalColor(i + 1));
        recent.setColor(31, right);
      }
    }
  }
  
  rgb_fg.getSliders()[3]->getInputLine().setColor(fgbg.getFG255().a == 255 ? lpi::RGB_Black : lpi::RGB_Red);
  rgb_bg.getSliders()[3]->getInputLine().setColor(fgbg.getBG255().a == 255 ? lpi::RGB_Black : lpi::RGB_Red);

}

void ColorWindow::initPalettes()
{
  palette.resize(11);
  
  for(size_t i = 0; i < palette.size(); i++)
  {
    palette[i].init();
  }
  
  for(size_t i = 0; i < 3; i++)
  {
    palette[i].palette->setPaletteSize(16, 2);
    for(size_t j = 0; j < 32; j++) palette[i].palette->setColor(j, lpi::RGBd_White);
  }
  
  palette[0].name = "Custom 1";
  palette[1].name = "Custom 2";
  palette[2].name = "Recent";

  generatePaletteDefault(palette[3]);
  generatePaletteGreyScale(palette[4]);
  generatePalettePixelArt(palette[5]);
  generatePaletteRoyal(palette[6]);
  generatePaletteCGA(palette[7], true);
  generatePaletteCGA(palette[8], false);
  generatePaletteTango(palette[9]);
  generatePaletteCrayons(palette[10]);
}

void ColorWindow::palettesToPersist(lpi::Persist& persist, const std::string& name)
{
  std::string names[3] = { ".custom1", ".custom2", ".recent" };
  for(int i = 0; i < 3; i++)
  {
    for(int j = 0; j < palette[i].palette->getPaletteSize(); j++)
    {
      writeColorToPersist(persist, palette[i].palette->getInternalColor(j), name + names[i] + "." + lpi::valtostr(j));
    }
  }
}

void ColorWindow::palettesFromPersist(const lpi::Persist& persist, const std::string& name)
{
  std::string names[3] = { ".custom1", ".custom2", ".recent" };
  for(int i = 0; i < 3; i++)
  {
    for(int j = 0; j < palette[i].palette->getPaletteSize(); j++)
    {
      lpi::ColorRGBd color;
      readColorFromPersist(color, persist, name + names[i] + "." + lpi::valtostr(j), palette[i].palette->getInternalColor(j));
      palette[i].palette->setColor(j, color);
    }
  }
}

////////////////////////////////////////////////////////////////////////////////

OverviewWindow::OverviewWindow(const lpi::gui::IGUIDrawer& geom)
: w(0)
{
  resize(0, 0, 300, 120);
  addTop(geom);
  addResizer(geom);
  addTitle(_t("Overview"));

  setColorMod(lpi::ColorRGB(255,255,255,192));
  
  setEnabled(true);
}

void OverviewWindow::setPaintWindow(PaintWindow* w)
{
  this->w = w;
}

void OverviewWindow::getTexturePos(int& tx0, int& ty0, int& tx1, int& ty1, const Paint& paint) const
{
  tx0 = x0 + 4;
  ty0 = y0 + 20;
  ty1 = y1 - 4;
  int hh = ty1 - ty0;
  int ww = hh;
  if(paint.getV() != 0) ww = (hh * paint.getU()) / paint.getV();
  
  if(ww < hh / 2) ww = hh / 2;
  if(tx0 + ww > (x1 + x0) / 2) ww = (x1 + x0) / 2 - tx0;
    
  tx1 = tx0 + ww;
}

void OverviewWindow::drawImpl(lpi::gui::IGUIDrawer& drawer) const
{
  lpi::gui::Window::drawImpl(drawer);
  
  if(w != 0)
  {
    ColorMode colorMode = w->getPaint().getColorMode();
    
    Paint& paint = w->getPaint();
    
    int tx0, ty0, tx1, ty1;
    getTexturePos(tx0, ty0, tx1, ty1, paint);
    
    lpi::ScreenGL* screen = ((lpi::gui::GUIDrawerGL&)drawer).getScreen();
    bool hasSmoothing = screen->isSmoothingEnabled();
    screen->enableSmoothing();
    if(colorMode == MODE_RGBA_8) drawTextureSized(screen, &paint.canvasRGBA8, tx0, ty0, tx1 - tx0, ty1 - ty0);
    else if(colorMode == MODE_RGBA_32F) drawTextureSized(screen, &paint.canvasRGBA32F, tx0, ty0, tx1 - tx0, ty1 - ty0);
    else if(colorMode == MODE_GREY_8) drawTextureSized(screen, &paint.canvasGrey8, tx0, ty0, tx1 - tx0, ty1 - ty0);
    else if(colorMode == MODE_GREY_32F) drawTextureSized(screen, &paint.canvasGrey32F, tx0, ty0, tx1 - tx0, ty1 - ty0);
    if(!hasSmoothing) screen->disableSmoothing();

    int wx0 = w->getX0();
    int wy0 = w->getY0();
    if(paint.getX0() > wx0) wx0 = paint.getX0();
    if(paint.getY0() > wy0) wy0 = paint.getY0();
    double px0, py0;
    paint.screenToPixel(px0, py0, wx0, wy0); 

    int wx1 = w->getX1();
    int wy1 = w->getY1();
    if(paint.getX1() < wx1) wx1 = paint.getX1();
    if(paint.getY1() < wy1) wy1 = paint.getY1();
    double px1, py1;
    paint.screenToPixel(px1, py1, wx1, wy1); 
    
    double dx0 = (double)px0 / (double)paint.getU();
    double dy0 = (double)py0 / (double)paint.getV();
    double dx1 = (double)px1 / (double)paint.getU();
    double dy1 = (double)py1 / (double)paint.getV();
    
    int rx0 = tx0 + (int)((tx1 - tx0) * dx0);
    int ry0 = ty0 + (int)((ty1 - ty0) * dy0);
    int rx1 = tx0 + (int)((tx1 - tx0) * dx1);
    int ry1 = ty0 + (int)((ty1 - ty0) * dy1);
    
    if(rx1 > rx0 && ry1 > ry0)
    {
      drawer.drawRectangle(rx0, ry0, rx1, ry1, lpi::RGB_Black, false);
      drawer.drawRectangle(rx0-1, ry0-1, rx1+1, ry1+1, lpi::RGB_White, false);
    }
  }
}

void OverviewWindow::handleImpl(const lpi::IInput& input)
{
  lpi::gui::Window::handleImpl(input);
  
  if(w != 0)
  {
    Paint& paint = w->getPaint();
    
    int tx0, ty0, tx1, ty1;
    getTexturePos(tx0, ty0, tx1, ty1, paint);
    
    if(mouseDownHere(input))
    {
      int x = input.mouseX();
      int y = input.mouseY();
      
      if(x >= tx0 && x <= tx1 && y >= ty0 && y <= ty1)
      {
        double dx = (double)(x - tx0) / (double)(tx1 - tx0) - 0.5;
        double dy = (double)(y - ty0) / (double)(ty1 - ty0) - 0.5;
        
        double px = dx * paint.getU();
        double py = dy * paint.getV();
        
        int sx = (int)(px * paint.getZoom());
        int sy = (int)(py * paint.getZoom());
        
        //paint.pixelToScreen(sx, sy, px, py);
        paint.moveCenterTo(w->getCenterX() - sx, w->getCenterY() - sy);
      }
    }
  }
}

////////////////////////////////////////////////////////////////////////////////

ShortCutField::ShortCutField(bool readonly)
: active(false)
, readonly(readonly)
{
}

void ShortCutField::drawImpl(lpi::gui::IGUIDrawer& drawer) const
{
  std::string s;
  if(key.ascii == 0) s = _t("(no shortcut set)");
  else
  {
    if(key.shift) s += " SHIFT";
    if(key.ctrl) s += " CTRL";
    if(key.alt) s += " ALT";
    if(key.meta) s += " META";
    if(key.super) s += " SUPER";
    s += " ";
    //s += (char)(key.ascii);
    s += SDL_GetKeyName((SDLKey)(key.ascii));
  }
  
  drawer.drawGUIPartText(lpi::gui::GPT_DEFAULT_TEXT01, s, x0 + 4, y0, x1, y1);
  
  drawer.drawRectangle(x0, y0, x1, y1, lpi::RGB_Black, false);
  if(active) drawer.drawRectangle(x0, y0, x1, y1, lpi::ColorRGB(255,0,0,64), true);
  if(readonly) drawer.drawRectangle(x0, y0, x1, y1, lpi::ColorRGB(255,255,255,64), true);
}

void ShortCutField::handleImpl(const lpi::IInput& input)
{
  if(!readonly)
  {
    autoActivate(input, mouseState, active);
    
    if(mouseDoubleClicked(input)) key.ascii = 0;
    
    if(active)
    {
      for(size_t i = 1; i < 300; i++)
      {
        if(input.keyPressed(i))
        {
          key.ctrl = input.keyDown(SDLK_LCTRL) || input.keyDown(SDLK_RCTRL);
          key.shift = input.keyDown(SDLK_LSHIFT) || input.keyDown(SDLK_RSHIFT);
          key.alt = input.keyDown(SDLK_LALT) || input.keyDown(SDLK_RALT);
          key.meta = input.keyDown(SDLK_LMETA) || input.keyDown(SDLK_RMETA);
          key.super = input.keyDown(SDLK_LSUPER) || input.keyDown(SDLK_RSUPER);
          key.ascii = i;
        }
      }
    }
  }
}


////////////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////////////

