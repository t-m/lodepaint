/*
LodePaint

Copyright (c) 2009 Lode Vandevenne
All rights reserved.

This file is part of LodePaint.

LodePaint is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LodePaint is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LodePaint.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <vector>
#include "lpi/lpi_texture.h"

class TextureRGBA32F;
class TextureRGBA8;

//FFT2D does not do any extra multiplication. If you do forward, then inverse FFT2D, then you must multiply it with 1.0 / (n * m) to have original image back.
//The input data is RGBA. So the "double" arrays must have 4 * n * m elements. If affect_alpha is false, the 4th channel isn't affected and less calculations are done.
void FFT2D(double* ore, double* oim, const double* ire, const double* iim, int n, int m, bool inverse, bool affect_alpha);
void FFT2D(float* ore, float* oim, const float* ire, const float* iim, int n, int m, bool inverse, bool affect_alpha);
void FFT2D_greyscale(double* ore, double* oim, const double* ire, const double* iim, int n, int m, bool inverse);
void FFT2D_greyscale(float* ore, float* oim, const float* ire, const float* iim, int n, int m, bool inverse);
void multiplyForFFT(double* iore, double* ioim, int n, int m, double factor, bool affect_alpha);
void multiplyForFFT(float* iore, float* ioim, int n, int m, float factor, bool affect_alpha);
void multiplyForFFT(double* io, int n, int m, double factor, bool affect_alpha);
void multiplyForFFT(float* io, int n, int m, float factor, bool affect_alpha);

/*
FFT2D real works on real input, and stores ALL the data in a single instead of
double array, because the spectrum of real data has some symmetry rules so there
is enough space to store all data. It is not simple to explain exactly what data
goes where though.
*/
void FFT2D_real(double* iore, int n, int m, bool inverse, bool affect_alpha); //RGBA data
void FFT2D_real(float* iore, int n, int m, bool inverse, bool affect_alpha); //RGBA data

//clips values in range 0-255
void convolute2x2(TextureRGBA8& texture, const double matrix[4], bool wrapping, bool alpha, double extra_term);
void convolute3x3(TextureRGBA8& texture, const double matrix[9], bool wrapping, bool alpha, double extra_term);
void convolute5x5(TextureRGBA8& texture, const double matrix[25], bool wrapping, bool alpha, double extra_term);
void convolute7x7(TextureRGBA8& texture, const double matrix[49], bool wrapping, bool alpha, double extra_term, double mul);

//does NOT clip values to any range, it's for HDR
void convolute2x2(TextureRGBA32F& texture, const double matrix[4], bool wrapping, bool alpha, double extra_term);
void convolute3x3(TextureRGBA32F& texture, const double matrix[9], bool wrapping, bool alpha, double extra_term);
void convolute5x5(TextureRGBA32F& texture, const double matrix[25], bool wrapping, bool alpha, double extra_term);
void convolute7x7(TextureRGBA32F& texture, const double matrix[49], bool wrapping, bool alpha, double extra_term, double mul);
