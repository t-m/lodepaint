/*
LodePaint

Copyright (c) 2009-2010 Lode Vandevenne
All rights reserved.

This file is part of LodePaint.

LodePaint is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LodePaint is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LodePaint.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once 

#include "paint_imageformats.h"

#if defined(LPI_OS_WINDOWS)
#include "windows.h"
#endif


class ImageFormatPlugin : public AImageFormat
{
  private:
#if defined(LPI_OS_LINUX) || defined(LPI_OS_AMIGA)
    void* dl; //the pointer to the dynamic or shared library
#elif defined(LPI_OS_WINDOWS)
    HINSTANCE  dl; //the pointer to the dynamic or shared library
#endif
    std::string description;
    bool isvalid; //true if all required functions are in the dll (not ALL functions are required, but some are). For example if 128-bit is supported, the pluginFreeFloat function must be there, the pluginFreeUnsignedChar function must ALWAYS be there.
    
    typedef void (*T_fileFormatGetDescription)(char*, int);
    T_fileFormatGetDescription p_fileFormatGetDescription;
    typedef int (*T_fileFormatGetNumExt)();
    T_fileFormatGetNumExt p_fileFormatGetNumExt;
    typedef void (*T_fileFormatGetExt)(char*, int, int);
    T_fileFormatGetExt p_fileFormatGetExt;
    typedef int (*T_fileFormatIsOfFormat)(unsigned char*, unsigned long);
    T_fileFormatIsOfFormat p_fileFormatIsOfFormat;
    typedef int (*T_fileFormatEncodeRGBA8)(unsigned char**, unsigned long*, unsigned char*, int, int, int*);
    T_fileFormatEncodeRGBA8 p_fileFormatEncodeRGBA8;
    typedef int (*T_fileFormatDecodeRGBA8)(unsigned char**, int*, int*, unsigned char*, unsigned long, int*);
    T_fileFormatDecodeRGBA8 p_fileFormatDecodeRGBA8;
    typedef int (*T_fileFormatEncodeRGBA32F)(unsigned char**, unsigned long*, float*, int, int, int*);
    T_fileFormatEncodeRGBA32F p_fileFormatEncodeRGBA32F;
    typedef int (*T_fileFormatDecodeRGBA32F)(float**, int*, int*, unsigned char*, unsigned long, int*);
    T_fileFormatDecodeRGBA32F p_fileFormatDecodeRGBA32F;
    typedef void (*T_pluginFreeUnsignedChar)(unsigned char* buffer);
    T_pluginFreeUnsignedChar p_pluginFreeUnsignedChar;
    typedef void (*T_pluginFreeFloat)(float* buffer);
    T_pluginFreeFloat p_pluginFreeFloat;
    typedef void (*T_getLastError)(char*, int);
    T_getLastError p_getLastError;
    
  protected:
    
    virtual bool canDecodeRGBA8() const;
    virtual bool canEncodeRGBA8() const;
    
    virtual bool decodeRGBA8(std::string& error, std::vector<unsigned char>& image, int& w, int& h, const unsigned char* file, size_t filesize) const;
    virtual bool encodeRGBA8(std::string& error, std::vector<unsigned char>& file, const unsigned char* image, int w, int h) const;

    virtual bool canDecodeRGBA32F() const;
    virtual bool canEncodeRGBA32F() const;
    
    virtual bool decodeRGBA32F(std::string& error, std::vector<float>& image, int& w, int& h, const unsigned char* file, size_t filesize) const;
    virtual bool encodeRGBA32F(std::string& error, std::vector<unsigned char>& file, const float* image, int w, int h) const;

    virtual bool isFileOfFormat(const unsigned char* file, size_t filesize) const;
    
    void setError(std::string& error, int result) const; //the result parameter is the result from a decode call
    
  public:
    
    ImageFormatPlugin(const std::string& dynamicLib);
    ~ImageFormatPlugin();
    
    virtual size_t getNumExtensions() const;
    virtual std::string getExtension(size_t i) const;
    virtual std::string getDescription() const;
    
    bool isValidFileFormatPlugin() const;
};
