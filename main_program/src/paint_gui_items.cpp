/*
LodePaint

Copyright (c) 2009-2010 Lode Vandevenne
All rights reserved.

This file is part of LodePaint.

LodePaint is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LodePaint is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LodePaint.  If not, see <http://www.gnu.org/licenses/>.
*/ 

#include "paint_gui_items.h"

#include "paint_tool_geom.h"
#include "paint_tool_brushes.h"
#include "paint_tool_other.h"

#include "paint_filter_other.h"
#include "paint_filter_alpha.h"
#include "paint_filter_dsp.h"
#include "paint_filter_image.h"
#include "paint_filter_mask.h"
#include "paint_filter_plugin.h"

#include "lpi/lpi_translate.h"

#include <SDL/SDL.h>

void FilterGroup::destruct()
{
  for(size_t i = 0; i < filters.size(); i++)
  {
    delete filters[i];
  }
  filters.clear();
}

Filters::Filters()
{
}

Filters::~Filters()
{
  for(size_t i = 0; i < filters.size(); i++)
  {
    filters[i].destruct();
  }
  rootFilters.destruct();
  
  filters.clear();
}

void Filters::addSub(const std::string& title)
{
  filters.resize(filters.size() + 1);
  filters.back().name = title;
}

void Filters::addSub(IOperationFilter* filter)
{
  filters.back().filters.push_back(filter);
}

void Filters::addSubSeparator()
{
  filters.back().filters.push_back(0);
}

void Filters::addRoot(IOperationFilter* filter)
{
  rootFilters.filters.push_back(filter);
}

int Filters::findSub(const std::string& name)
{
  for(size_t i = 0; i < filters.size(); i++)
  {
    if(filters[i].name == name) return i;
  }
  return -1;
}

FilterGroup& Filters::getSub(size_t index)
{
  return filters[index];
}

////////////////////////////////////////////////////////////////////////////////

FiltersCollection::FiltersCollection(GlobalToolSettings& globalToolSettings, lpi::gui::IGUIDrawer& guidrawer)
{
  clear_image = new OperationClear(globalToolSettings);
  imageMenuFilters.addRoot(clear_image);
  crop = new OperationCrop(globalToolSettings);
  imageMenuFilters.addRoot(crop);
  
  imageMenuFilters.addSub("Color Adjust");
  negrgb = new OperationNegateRGB(globalToolSettings);
  imageMenuFilters.addSub(negrgb);
  imageMenuFilters.addSub(new OperationNegateLightness(globalToolSettings));
  imageMenuFilters.addSub(new OperationSolarizeRGB(globalToolSettings));
  imageMenuFilters.addSubSeparator();
  normalize = new OperationNormalize(globalToolSettings);
  imageMenuFilters.addSub(normalize);
  imageMenuFilters.addSub(new OperationNormalize2(globalToolSettings, guidrawer));
  imageMenuFilters.addSub(new OperationClipColor(globalToolSettings, guidrawer));
  imageMenuFilters.addSub(new OperationThreshold(globalToolSettings, guidrawer));
  imageMenuFilters.addSubSeparator();
  imageMenuFilters.addSub(new OperationGrayscale(globalToolSettings));
  greyscale = new OperationGrayscalePhysiological(globalToolSettings);
  imageMenuFilters.addSub(greyscale);
  imageMenuFilters.addSub(new OperationColorize(globalToolSettings, guidrawer));
  imageMenuFilters.addSub(new OperationPosterize(globalToolSettings, guidrawer));
  imageMenuFilters.addSub(new OperationReduceColors(globalToolSettings, guidrawer));
  imageMenuFilters.addSub(new OperationDitherMonochrome(globalToolSettings));
  imageMenuFilters.addSub(new OperationDitherRGB1(globalToolSettings));
  imageMenuFilters.addSubSeparator();
  brightness_contrast = new OperationAdjustBrightnessContrast(globalToolSettings, guidrawer);
  imageMenuFilters.addSub(brightness_contrast);
  imageMenuFilters.addSub(new OperationAdjustTemperature(globalToolSettings, guidrawer));
  imageMenuFilters.addSub(new OperationCurves(globalToolSettings, guidrawer));
  imageMenuFilters.addSub(new OperationLevels(globalToolSettings, guidrawer));
  
  imageMenuFilters.addSub("Color Tools");
  imageMenuFilters.addSub(new OperationSwapColors(globalToolSettings));
  imageMenuFilters.addSub(new OperationOneBitFGBG(globalToolSettings));
  imageMenuFilters.addSubSeparator();
  imageMenuFilters.addSub(new OperationColorAverage(globalToolSettings));
  imageMenuFilters.addSub(new OperationSortColors(globalToolSettings));
  imageMenuFilters.addSub(new OperationSortColorsHilbert(globalToolSettings));
  imageMenuFilters.addSub(new OperationOrderColors(globalToolSettings));
  
  imageMenuFilters.addSub("Color Models");
  imageMenuFilters.addSub(new OperationNegateAdvanced(globalToolSettings, guidrawer));
  imageMenuFilters.addSubSeparator();
  hue_saturation = new OperationAdjustHSL(globalToolSettings, guidrawer);
  imageMenuFilters.addSub(hue_saturation);
  imageMenuFilters.addSub(new OperationAdjustHSV(globalToolSettings, guidrawer));
  multiply_sla = new OperationMultiplySLA(globalToolSettings, guidrawer);
  imageMenuFilters.addSub(multiply_sla);
  imageMenuFilters.addSubSeparator();
  imageMenuFilters.addSub(new OperationExtractChannel(globalToolSettings, guidrawer));
  imageMenuFilters.addSub(new OperationChannelSwitcher(globalToolSettings, guidrawer));
  imageMenuFilters.addSub(new OperationChannelSwitcherII(globalToolSettings, guidrawer));
  imageMenuFilters.addSubSeparator();
  imageMenuFilters.addSub(new OperationRGChromaticity(globalToolSettings));
  imageMenuFilters.addSub(new OperationSpectrum(globalToolSettings));
  imageMenuFilters.addSubSeparator();
  imageMenuFilters.addSub(new OperationColorArithmetic(globalToolSettings, guidrawer));
  imageMenuFilters.addSub(new OperationRGBAMatrix(globalToolSettings, guidrawer));
  
  imageMenuFilters.addSub("Color Mode");
  imageMenuFilters.addSub(new OperationSetColorMode(globalToolSettings, MODE_RGBA_8));
  imageMenuFilters.addSub(new OperationSetColorMode(globalToolSettings, MODE_RGBA_32F));
  imageMenuFilters.addSub(new OperationSetColorMode(globalToolSettings, MODE_GREY_8));
  imageMenuFilters.addSub(new OperationSetColorMode(globalToolSettings, MODE_GREY_32F));
  
  imageMenuFilters.addSub("HDR");
  imageMenuFilters.addSub(new OperationColorArithmeticII(globalToolSettings, guidrawer));
  imageMenuFilters.addSub(new OperationToneMapHistogram(globalToolSettings, guidrawer));
  imageMenuFilters.addSub(new OperationAbsoluteValue(globalToolSettings, guidrawer));

  imageMenuFilters.addSub("Transform");
  imageMenuFilters.addSub(new OperationAddRemoveBorders(globalToolSettings, guidrawer));
  rescale = new OperationRescale(globalToolSettings, guidrawer);
  imageMenuFilters.addSub(rescale);
  imageMenuFilters.addSub(new OperationPixelArtScaling(globalToolSettings, guidrawer));
  imageMenuFilters.addSubSeparator();
  flip_x = new OperationFlipX(globalToolSettings);
  imageMenuFilters.addSub(flip_x);
  flip_y = new OperationFlipY(globalToolSettings);
  imageMenuFilters.addSub(flip_y);
  imageMenuFilters.addSubSeparator();
  imageMenuFilters.addSub(new OperationRotate90CW(globalToolSettings));
  imageMenuFilters.addSub(new OperationRotate180(globalToolSettings));
  imageMenuFilters.addSub(new OperationRotate90CCW(globalToolSettings));
  imageMenuFilters.addSub(new OperationRotate(globalToolSettings, guidrawer));
  imageMenuFilters.addSubSeparator();
  imageMenuFilters.addSub(new OperationTransform2D(globalToolSettings, guidrawer));
  
  imageMenuFilters.addSub("Selection");
  delete_selection = new OperationDeleteSelection(globalToolSettings);
  imageMenuFilters.addSub(delete_selection);
  select_all = new OperationSelectAll(globalToolSettings);
  imageMenuFilters.addSub(select_all);
  select_none = new OperationSelectNone(globalToolSettings);
  imageMenuFilters.addSub(select_none);
  blend_sel = new OperationBlendSelection(globalToolSettings, guidrawer);
  imageMenuFilters.addSub(blend_sel);
  
  imageMenuFilters.addSub("Mask");
  imageMenuFilters.addSub(new OperationClearMask(globalToolSettings));
  imageMenuFilters.addSub(new OperationClearMaskToValue(globalToolSettings, guidrawer));
  imageMenuFilters.addSub(new OperationFeatherMask(globalToolSettings, guidrawer));
  negmask = new OperationNegateMask(globalToolSettings);
  imageMenuFilters.addSub(negmask);
  imageMenuFilters.addSub(new OperationSelectionToMask(globalToolSettings));
  imageMenuFilters.addSub(new OperationChannelToMask(globalToolSettings, guidrawer));
  imageMenuFilters.addSub(new OperationSwapMask(globalToolSettings));

  filtersMenuFilters.addSub("Sharpness");
  filtersMenuFilters.addSub(new OperationBlur(globalToolSettings, guidrawer));
  filtersMenuFilters.addSub(new OperationSoften(globalToolSettings, guidrawer));
  filtersMenuFilters.addSub(new OperationSoftestBlur(globalToolSettings, guidrawer));
  gaussian_blur = new OperationGaussianBlur(globalToolSettings, guidrawer);
  filtersMenuFilters.addSub(gaussian_blur);
  filtersMenuFilters.addSub(new OperationInterpolatingBlur(globalToolSettings, guidrawer));
  filtersMenuFilters.addSub(new OperationSelectiveGaussianBlur(globalToolSettings, guidrawer));
  filtersMenuFilters.addSub(new OperationSharpen(globalToolSettings, guidrawer));
  filtersMenuFilters.addSub(new OperationUnsharpMask(globalToolSettings, guidrawer));
  filtersMenuFilters.addSub(new OperationHighPass(globalToolSettings, guidrawer));
  filtersMenuFilters.addSub(new OperationMedianFilter(globalToolSettings, guidrawer));
  pixel_antialias = new OperationPixelAntialias(globalToolSettings, guidrawer);
  filtersMenuFilters.addSub(pixel_antialias);

  filtersMenuFilters.addSub("Edges");
  filtersMenuFilters.addSub(new OperationEdgeDetect(globalToolSettings, guidrawer));
  filtersMenuFilters.addSub(new OperationPixelEdgeDetect(globalToolSettings, guidrawer));
  filtersMenuFilters.addSub(new OperationCannyEdgeDetect(globalToolSettings, guidrawer));
  filtersMenuFilters.addSub(new OperationEmboss(globalToolSettings, guidrawer));
  filtersMenuFilters.addSub(new OperationPixelBorder(globalToolSettings, guidrawer));
  filtersMenuFilters.addSub(new OperationContour(globalToolSettings, guidrawer));

  filtersMenuFilters.addSub("Morphology");
  filtersMenuFilters.addSub(new OperationErode(globalToolSettings, guidrawer));
  filtersMenuFilters.addSub(new OperationDilate(globalToolSettings, guidrawer));
  filtersMenuFilters.addSub(new OperationMorphologyOpen(globalToolSettings, guidrawer));
  filtersMenuFilters.addSub(new OperationMorphologyClose(globalToolSettings, guidrawer));
  filtersMenuFilters.addSub(new OperationMorphologyGradient(globalToolSettings, guidrawer));
  filtersMenuFilters.addSub(new OperationMorphologyTopHat(globalToolSettings, guidrawer));

  filtersMenuFilters.addSub("Alpha");
  alpha_opaque = new OperationOpaque(globalToolSettings);
  filtersMenuFilters.addSub(alpha_opaque);
  filtersMenuFilters.addSub(new OperationSetAlphaTo(globalToolSettings, guidrawer));
  alpha_apply = new OperationApplyAlpha(globalToolSettings, true);
  filtersMenuFilters.addSub(alpha_apply);
  filtersMenuFilters.addSub(new OperationOnlyAlpha(globalToolSettings));
  filtersMenuFilters.addSub(new OperationPremultiplyAlpha(globalToolSettings));
  filtersMenuFilters.addSubSeparator();
  filtersMenuFilters.addSub(new OperationNegateAlpha(globalToolSettings));
  filtersMenuFilters.addSubSeparator();
  filtersMenuFilters.addSub(new OperationTranslucent(globalToolSettings));
  filtersMenuFilters.addSub(new OperationAlphaModulated(globalToolSettings));
  filtersMenuFilters.addSub(new OperationAlphaSaturation(globalToolSettings));
  filtersMenuFilters.addSubSeparator();
  filtersMenuFilters.addSub(new OperationAlphaColorKey(globalToolSettings));
  filtersMenuFilters.addSub(new OperationAlphaSoftKey(globalToolSettings));
  filtersMenuFilters.addSub(new OperationAlphaExtractCloud(globalToolSettings));
  filtersMenuFilters.addSubSeparator();
  filtersMenuFilters.addSub(new OperationAlphaToGray(globalToolSettings));
  filtersMenuFilters.addSub(new OperationThresholdAlpha(globalToolSettings, guidrawer));
  filtersMenuFilters.addSub(new OperationDitherMonochromeAlpha(globalToolSettings));
  filtersMenuFilters.addSubSeparator();
  filtersMenuFilters.addSub(new OperationGrayscaleAlpha(globalToolSettings));
  filtersMenuFilters.addSubSeparator();
  filtersMenuFilters.addSub(new OperationIdentifyAlpha(globalToolSettings));
  
  filtersMenuFilters.addSub("Seamless Tiling");
  filtersMenuFilters.addSub(new OperationMakeSeamless(globalToolSettings));
  filtersMenuFilters.addSub(new OperationRepeat(globalToolSettings, guidrawer));
  filtersMenuFilters.addSub(new OperationRepeat2x2(globalToolSettings, guidrawer));
  wrap_50 = new OperationShift50(globalToolSettings);
  filtersMenuFilters.addSub(wrap_50);
  filtersMenuFilters.addSub(new OperationWrappingShift(globalToolSettings, guidrawer));
  filtersMenuFilters.addSub(new OperationMirrorTile(globalToolSettings, guidrawer));
  
  filtersMenuFilters.addSub("Generate");
  filtersMenuFilters.addSub(new OperationGenerateGrid(globalToolSettings, guidrawer));
  filtersMenuFilters.addSub(new OperationGenerateTiles(globalToolSettings, guidrawer));
  filtersMenuFilters.addSub(new OperationGenerateRandomLines(globalToolSettings, guidrawer));
  filtersMenuFilters.addSub(new OperationScanLines(globalToolSettings, guidrawer));
  test_image = new OperationGenerateTestImage(globalToolSettings, guidrawer);
  filtersMenuFilters.addSub(test_image);
  filtersMenuFilters.addSub(new OperationGenerateGradient(globalToolSettings, guidrawer));
  filtersMenuFilters.addSub(new OperationGenerateGradientQuad(globalToolSettings, guidrawer));
  filtersMenuFilters.addSub(new OperationTurbulence(globalToolSettings, guidrawer));
  filtersMenuFilters.addSub(new OperationMarble(globalToolSettings, guidrawer));
  filtersMenuFilters.addSub(new OperationPlasma(globalToolSettings, guidrawer));
  filtersMenuFilters.addSub(new OperationGenerateXORTexture(globalToolSettings, guidrawer));
  filtersMenuFilters.addSub(new OperationGenerateMandelbrot(globalToolSettings, guidrawer));
  filtersMenuFilters.addSub(new OperationGenerateJulia(globalToolSettings, guidrawer));
  
  filtersMenuFilters.addSub("Noise");
  filtersMenuFilters.addSub(new OperationRandomNoise(globalToolSettings, guidrawer));
  filtersMenuFilters.addSub(new OperationRandomWalk(globalToolSettings, guidrawer));
  filtersMenuFilters.addSub(new OperationRandomShuffle(globalToolSettings));
  
  filtersMenuFilters.addSub("Effects");
  filtersMenuFilters.addSub(new OperationMosaic(globalToolSettings, guidrawer));
  filtersMenuFilters.addSub(new OperationSepia(globalToolSettings, guidrawer));
  filtersMenuFilters.addSub(new OperationPixelShadow(globalToolSettings, guidrawer));
  filtersMenuFilters.addSub(new OperationGameOfLife(globalToolSettings));
  filtersMenuFilters.addSub(new OperationGameOfLifeExtinguish(globalToolSettings));
  filtersMenuFilters.addSub(new OperationSpookyNeon(globalToolSettings, guidrawer));
  
  filtersMenuFilters.addSub("Artistic");
  filtersMenuFilters.addSub(new OperationPointillism(globalToolSettings, guidrawer));
  filtersMenuFilters.addSub(new OperationBrushy(globalToolSettings, guidrawer));
  
  filtersMenuFilters.addSub("Bits");
  filtersMenuFilters.addSub(new OperationMirrorBits(globalToolSettings));
  filtersMenuFilters.addSub(new OperationShiftBits1(globalToolSettings));
  filtersMenuFilters.addSub(new OperationShiftBits4(globalToolSettings));
  filtersMenuFilters.addSub(new OperationBitsToGrayCode(globalToolSettings));
  filtersMenuFilters.addSub(new OperationBitsFromGrayCode(globalToolSettings));
  filtersMenuFilters.addSub(new OperationBitsToGrayCodeRGBA(globalToolSettings));
  filtersMenuFilters.addSub(new OperationBitsFromGrayCodeRGBA(globalToolSettings));

  filtersMenuFilters.addSub("DSP");
  filtersMenuFilters.addSub(new OperationFFT(globalToolSettings, false, guidrawer));
  filtersMenuFilters.addSub(new OperationFFT(globalToolSettings, true, guidrawer));
  filtersMenuFilters.addSub(new OperationFFTMagnitude(globalToolSettings, guidrawer));
  filtersMenuFilters.addSub(new OperationLowPassFilter(globalToolSettings, guidrawer));
  filtersMenuFilters.addSub(new OperationHighPassFilter(globalToolSettings, guidrawer));
  filtersMenuFilters.addSub(new OperationBandPassFilter(globalToolSettings, guidrawer));
  filtersMenuFilters.addSub(new OperationBandStopFilter(globalToolSettings, guidrawer));

  filtersMenuFilters.addSub("Fix");
  filtersMenuFilters.addSub(new OperationRemoveJpegArtifacts(globalToolSettings, guidrawer));
  filtersMenuFilters.addSub(new OperationAdaptiveMedianFilter(globalToolSettings, guidrawer));

  filtersMenuFilters.addRoot(new OperationUserDefinedFilter(globalToolSettings, guidrawer));
}

void FiltersCollection::addPluginFilters(lpi::FileBrowse& filebrowser, const std::string& directory, GlobalToolSettings& globalToolSettings, lpi::gui::IGUIDrawer& guidrawer)
{
  std::vector<std::string> files;
  filebrowser.getFiles(files, directory);
  for(size_t i = 0; i < files.size(); i++)
  {
    FilterPlugin* plugin = new FilterPlugin(directory + files[i], globalToolSettings, guidrawer);
    if(plugin->isValidFilterPlugin())
    {
      if(plugin->getSubMenu().size() > 0)
      {
        int index = pluginsMenuFilters.findSub(plugin->getSubMenu());
        if(index < 0)
        {
          pluginsMenuFilters.addSub(plugin->getSubMenu());
          pluginsMenuFilters.addSub(plugin);
        }
        else pluginsMenuFilters.getSub(index).filters.push_back(plugin);
      }
      else pluginsMenuFilters.addRoot(plugin);
    }
    else delete plugin;
  }
}

ToolsCollection::ToolsCollection(GlobalToolSettings& globalToolSettings, ColorWindow& colorWindow, lpi::gui::IGUIDrawer& guidrawer)
{
  tools.push_back(new Tool_Pan(globalToolSettings));
  tools.push_back(new Tool_ColorPicker(globalToolSettings, colorWindow));
  shortcuts['c'] = tools.size() - 1;
  tools.push_back(new Tool_Pen(globalToolSettings, guidrawer));
  shortcuts['p'] = tools.size() - 1;
  tools.push_back(new Tool_PixelBrush(globalToolSettings, guidrawer));
  shortcuts['i'] = tools.size() - 1;
  tools.push_back(new Tool_Brush(globalToolSettings, guidrawer));
  shortcuts['b'] = tools.size() - 1;
  tools.push_back(new Tool_Eraser(globalToolSettings, guidrawer));
  shortcuts['e'] = tools.size() - 1;
  tools.push_back(new Tool_ColorReplacer(globalToolSettings, guidrawer));
  tools.push_back(new Tool_Adjust(globalToolSettings, guidrawer));
  tools.push_back(new Tool_FloodFill(globalToolSettings, guidrawer));
  shortcuts['f'] = tools.size() - 1;
  tools.push_back(new Tool_Line(globalToolSettings, guidrawer));
  shortcuts['l'] = tools.size() - 1;
  tools.push_back(new Tool_Rectangle(globalToolSettings, guidrawer));
  tools.push_back(new Tool_Polygon(globalToolSettings, guidrawer));
  tools.push_back(new Tool_Ellipse(globalToolSettings, guidrawer));
  tools.push_back(new Tool_Text(globalToolSettings, guidrawer));
  tools.push_back(new Tool_GradientLine(globalToolSettings, guidrawer));
  tools.push_back(new Tool_SierpinskiTriangle(globalToolSettings, guidrawer));
  tools.push_back(new Tool_SelRect(globalToolSettings, guidrawer));
  shortcuts['s'] = tools.size() - 1;
  tools.push_back(new Tool_MaskBrush(globalToolSettings, guidrawer));
  tools.push_back(new Tool_MagicWand(globalToolSettings, guidrawer));
  tools.push_back(new Tool_PerspectiveCrop(globalToolSettings, guidrawer));
}

ToolsCollection::~ToolsCollection()
{
  for(size_t i = 0; i < tools.size(); i++)
  {
    delete tools[i];
  }
}

ITool* ToolsCollection::getTool(size_t index) const
{
  return tools[index];
}

size_t ToolsCollection::getNumTools() const
{
  return tools.size();
}

size_t ToolsCollection::getShortcutTool(char shortcut) const
{
  std::map<char, size_t>::const_iterator it = shortcuts.find(shortcut);
  if(it != shortcuts.end()) return it->second;
  else return tools.size();
}


KeyCombo::KeyCombo()
: shift(false)
, ctrl(false)
, alt(false)
, meta(false)
, super(false)
, ascii(0)
{
}


KeyCombo::KeyCombo(int ascii)
: shift(false)
, ctrl(false)
, alt(false)
, meta(false)
, super(false)
, ascii(ascii)
{
}

KeyCombo::KeyCombo(bool shift, bool ctrl, bool alt, int ascii)
: shift(shift)
, ctrl(ctrl)
, alt(alt)
, meta(false)
, super(false)
, ascii(ascii)
{
}

KeyCombo::KeyCombo(bool shift, bool ctrl, bool alt, bool meta, bool super, int ascii)
: shift(shift)
, ctrl(ctrl)
, alt(alt)
, meta(meta)
, super(super)
, ascii(ascii)
{
}

bool operator==(const KeyCombo& a, const KeyCombo& b)
{
  return a.shift == b.shift && a.ctrl == b.ctrl && a.alt == b.alt && a.meta == b.meta && a.super == b.super && a.ascii == b.ascii;
}

bool operator!=(const KeyCombo& a, const KeyCombo& b)
{
  return !(a == b);
}

void ShortCutManager::makeDefaults()
{
  defaults[0].resize(SC_NONE);
  
  defaults[0][SC_TOOL_PEN] = KeyCombo('p');
  defaults[0][SC_TOOL_BRUSH] = KeyCombo('b');
  defaults[0][SC_TOOL_PIXEL_BRUSH] = KeyCombo('i');
  defaults[0][SC_TOOL_ERASER] = KeyCombo('e');
  defaults[0][SC_TOOL_COLORPICKER] = KeyCombo('c');
  defaults[0][SC_TOOL_FLOODFILL] = KeyCombo('f');
  defaults[0][SC_TOOL_SELRECT] = KeyCombo('s');
  defaults[0][SC_TOOL_LINE] = KeyCombo('l');
  defaults[0][SC_TOOL_PREVIOUS] = KeyCombo('v');
  defaults[0][SC_OPERATION_CROP] = KeyCombo(false, true, false, 't');
  defaults[0][SC_OPERATION_DELETE_SELECTION] = KeyCombo(SDLK_DELETE);
  defaults[0][SC_OPERATION_SELECT_ALL] = KeyCombo(false, true, false, 'a');
  defaults[0][SC_OPERATION_SELECT_NONE] = KeyCombo(false, true, false, 'd');
  defaults[0][SC_SWAP_FG_BG] = KeyCombo('x');
  defaults[0][SC_NEGATE_FG_BG] = KeyCombo('y');
  defaults[0][SC_DEFAULT_FG_BG] = KeyCombo('d');
  defaults[0][SC_CUT] = KeyCombo(false, true, false, 'x');
  defaults[0][SC_COPY] = KeyCombo(false, true, false, 'c');
  defaults[0][SC_PASTE_IMAGE] = KeyCombo(false, true, false, 'v');
  defaults[0][SC_PASTE_SELECTION] = KeyCombo(true, true, false, 'v');
  defaults[0][SC_UNDO] = KeyCombo(false, true, false, 'z');
  defaults[0][SC_REDO] = KeyCombo(false, true, false, 'y');
  defaults[0][SC_NEW] = KeyCombo(false, true, false, 'n');
  defaults[0][SC_OPEN] = KeyCombo(false, true, false, 'o');
  defaults[0][SC_SAVE] = KeyCombo(false, true, false, 's');
  defaults[0][SC_RELOAD] = KeyCombo(false, false, false, SDLK_F5);
  defaults[0][SC_ZOOM_IN] = KeyCombo(true, false, false, '=');
  defaults[0][SC_ZOOM_OUT] = KeyCombo('-');
  defaults[0][SC_DYN_CHANGE_UP] = KeyCombo(SDLK_PAGEUP);
  defaults[0][SC_DYN_CHANGE_DOWN] = KeyCombo(SDLK_PAGEDOWN);

  defaults[1].resize(SC_NONE);
  defaults[1][SC_PASTE_SELECTION] = KeyCombo(false, true, false, 'e');
  defaults[1][SC_REDO] = KeyCombo(true, true, false, 'z');
  defaults[1][SC_ZOOM_IN] = KeyCombo(SDLK_KP_PLUS);
  defaults[1][SC_ZOOM_OUT] = KeyCombo(SDLK_KP_MINUS);

  names[SC_TOOL_PEN] = _t("Pen");
  names[SC_TOOL_BRUSH] = _t("Brush");
  names[SC_TOOL_PIXEL_BRUSH] = _t("Pixel Brush");
  names[SC_TOOL_ERASER] = _t("Eraser");
  names[SC_TOOL_COLORPICKER] = _t("Color Picker");
  names[SC_TOOL_FLOODFILL] = _t("Flood Fill");
  names[SC_TOOL_SELRECT] = _t("Select");
  names[SC_TOOL_LINE] = _t("Line");
  names[SC_TOOL_PREVIOUS] = _t("Previous Tool");
  names[SC_OPERATION_CROP] = _t("Crop");
  names[SC_OPERATION_DELETE_SELECTION] = _t("Delete Selection");
  names[SC_OPERATION_SELECT_ALL] = _t("Select All");
  names[SC_OPERATION_SELECT_NONE] = _t("Select None");
  names[SC_SWAP_FG_BG] = _t("Swap FG BG");
  names[SC_NEGATE_FG_BG] = _t("Negate FG BG");
  names[SC_DEFAULT_FG_BG] = _t("Default FG BG");
  names[SC_CUT] = _t("Cut");
  names[SC_COPY] = _t("Copy");
  names[SC_PASTE_IMAGE] = _t("Paste As Image");
  names[SC_PASTE_SELECTION] = _t("Paste As Selection");
  names[SC_UNDO] = _t("Undo");
  names[SC_REDO] = _t("Redo");
  names[SC_NEW] = _t("New Image");
  names[SC_OPEN] = _t("Open File");
  names[SC_SAVE] = _t("Save File");
  names[SC_RELOAD] = _t("Reload File");
  names[SC_ZOOM_IN] = _t("Zoom In");
  names[SC_ZOOM_OUT] = _t("Zoom Out");
  names[SC_DYN_CHANGE_UP] = _t("Dyn Change Up");
  names[SC_DYN_CHANGE_DOWN] = _t("Dyn Change Down");
  
  for(std::map<ShortCut, std::string>::const_iterator it = names.begin(); it != names.end(); it++)
  {
    namesinv[it->second] = it->first;
  }
}

ShortCutManager::ShortCutManager()
{
  makeDefaults();
  
  shortcuts[0] = defaults[0];
  shortcuts[1] = defaults[1];
}

ShortCut ShortCutManager::getShortCut(const lpi::IInput& input)
{
  bool ctrl = input.keyDown(SDLK_LCTRL) || input.keyDown(SDLK_RCTRL);
  bool shift = input.keyDown(SDLK_LSHIFT) || input.keyDown(SDLK_RSHIFT);
  bool alt = input.keyDown(SDLK_LALT) || input.keyDown(SDLK_RALT);
  bool meta = input.keyDown(SDLK_LMETA) || input.keyDown(SDLK_RMETA);
  bool super = input.keyDown(SDLK_LSUPER) || input.keyDown(SDLK_RSUPER);
  
  for(int i = 0; i < SC_NONE; i++)
  for(int j = 0; j < 2; j++)
  {
    const KeyCombo& key = shortcuts[j][i];
    if(key.ascii != 0)
    {
      if(key.alt == alt && key.ctrl == ctrl && key.shift == shift && key.meta == meta && key.super == super)
      {
        if(input.keyPressed(key.ascii)) return (ShortCut)i;
      }
    }
  }
  
  return SC_NONE;
}

size_t ShortCutManager::getNumCommands()
{
  return SC_NONE;
}

std::string ShortCutManager::getName(ShortCut index) const
{
  std::map<ShortCut, std::string>::const_iterator it = names.find(index);
  if(it != names.end()) return it->second;
  else return "";
}

std::string ShortCutManager::getXMLName(ShortCut index) const
{
  std::string name = getName(index);
  for(size_t i = 0; i < name.size(); i++) if(name[i] == ' ') name[i] = '_';
  return name;
}

ShortCut ShortCutManager::getNameInv(const std::string& name) const
{
  std::map<std::string, ShortCut>::const_iterator it = namesinv.find(name);
  if(it != namesinv.end()) return it->second;
  else return SC_NONE;

}

KeyCombo ShortCutManager::getCombo(ShortCut index, int bank)
{
  return shortcuts[bank][index];
}

KeyCombo ShortCutManager::getDefault(ShortCut index, int bank)
{
  return defaults[bank][index];
}

void ShortCutManager::setCombo(ShortCut index, const KeyCombo& combo, int bank)
{
  shortcuts[bank][index] = combo;
}

void ShortCutManager::writeKeyComboToPersist(lpi::Persist& persist, const KeyCombo& combo, const std::string& name)
{
  persist.setSettingT(name + ".shift", combo.shift);
  persist.setSettingT(name + ".ctrl", combo.ctrl);
  persist.setSettingT(name + ".alt", combo.alt);
  persist.setSettingT(name + ".meta", combo.meta);
  persist.setSettingT(name + ".super", combo.super);
  persist.setSettingT(name + ".ascii", combo.ascii);
}

void ShortCutManager::readKeyComboFromPersist(KeyCombo& combo, const lpi::Persist& persist, const std::string& name)
{
  persist.getSettingT(name + ".shift", combo.shift);
  persist.getSettingT(name + ".ctrl", combo.ctrl);
  persist.getSettingT(name + ".alt", combo.alt);
  persist.getSettingT(name + ".meta", combo.meta);
  persist.getSettingT(name + ".super", combo.super);
  persist.getSettingT(name + ".ascii", combo.ascii);
}

void ShortCutManager::removeKeyComboFromPersist(lpi::Persist& persist, const std::string& name)
{
  persist.removeSetting(name + ".shift");
  persist.removeSetting(name + ".ctrl");
  persist.removeSetting(name + ".alt");
  persist.removeSetting(name + ".meta");
  persist.removeSetting(name + ".super");
  persist.removeSetting(name + ".ascii");
}

void ShortCutManager::writeToPersist(lpi::Persist& persist, const std::string& name) const
{
  for(size_t i = 0; i < shortcuts[0].size(); i++)
  {
    std::string name0 = name + "." + getXMLName((ShortCut)i) + ".0";
    std::string name1 = name + "." + getXMLName((ShortCut)i) + ".1";

    if(shortcuts[0][i] != defaults[0][i]) writeKeyComboToPersist(persist, shortcuts[0][i], name0);
    else removeKeyComboFromPersist(persist, name0);
    if(shortcuts[1][i] != defaults[1][i]) writeKeyComboToPersist(persist, shortcuts[1][i], name1);
    else removeKeyComboFromPersist(persist, name1);
  }
}

void ShortCutManager::readFromPersist(const lpi::Persist& persist, const std::string& name)
{
  for(size_t i = 0; i < shortcuts[0].size(); i++)
  {
    std::string name0 = name + "." + getXMLName((ShortCut)i) + ".0";
    std::string name1 = name + "." + getXMLName((ShortCut)i) + ".1";

    if(persist.hasSetting(name0 + ".ascii"))
    {
      readKeyComboFromPersist(shortcuts[0][i], persist, name0);
    }
    if(persist.hasSetting(name1 + ".ascii"))
    {
      readKeyComboFromPersist(shortcuts[1][i], persist, name1);
    }
  }
}

void ShortCutManager::resetDefaults()
{
  shortcuts[0] = defaults[0];
  shortcuts[1] = defaults[1];
}

void ShortCutManager::getCombos(std::vector<KeyCombo>& combos0, std::vector<KeyCombo>& combos1) const
{
  combos0 = shortcuts[0];
  combos1 = shortcuts[1];
}

void ShortCutManager::setCombos(const std::vector<KeyCombo>& combos0, const std::vector<KeyCombo>& combos1)
{
  shortcuts[0] = combos0;
  shortcuts[1] = combos1;
}
