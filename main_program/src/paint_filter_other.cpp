/*
LodePaint

Copyright (c) 2009-2010 Lode Vandevenne
All rights reserved.

This file is part of LodePaint.

LodePaint is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LodePaint is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LodePaint.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "paint_filter_other.h"

#include "lpi/gui/lpi_gui_color.h"
#include "lpi/lpi_math.h"
#include "lpi/lpi_math2d.h"
#include "lpi/lpi_math3d.h"

#include "imalg/blur.h"
#include "imalg/edges.h"
#include "imalg/morphology.h"
#include "imalg/other.h"
#include "imalg/transform.h"
#include "imalg/util.h"

#include "paint_algos.h"
#include "paint_algos_brush_color.h"
#include "paint_algos_geom.h"
#include "paint_algos_dsp.h"

#include <iostream>
#include <limits>
#include <cmath>

////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////

OperationRandomNoise::OperationRandomNoise(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom)
: AOperationFilterUtil(settings, true, true)
, alpha(false)
, opacity(1.0)
, randomize_opacity(false)
, saltpepper(0.0)
, colorType(C_RGB)
, distribution(D_UNIFORM)
, dialog(this)
{
  std::vector<ColorType> enums2;
  enums2.push_back(C_GREY); enums2.push_back(C_RGB); enums2.push_back(C_FGBG);
  enums2.push_back(C_BRIGHTNESS); enums2.push_back(C_HUE); enums2.push_back(C_SATURATION);
  std::vector<std::string> names2;
  names2.push_back("Grey"); names2.push_back("RGB Colored"); names2.push_back("FG BG Color");
  names2.push_back("Brightness"); names2.push_back("Hue"); names2.push_back("Saturation");
  params.addEnum("Color Type", &colorType, names2, enums2);
  params.addColor("FG Color", &settings.leftColor);
  params.addColor("BG Color", &settings.rightColor);

  params.addBool("Include Alpha", &alpha);
  params.addDouble("Opacity", &opacity, 0.0, 1.0, 0.01);
  params.addBool("Randomize Opacity", &randomize_opacity);
  params.addDouble("Salt & Pepper", &saltpepper, 0.0, 0.99, 0.01);

  std::vector<Distribution> enums; enums.push_back(D_UNIFORM); enums.push_back(D_GAUSSIAN); enums.push_back(D_BINARY);
  std::vector<std::string> names; names.push_back("Uniform"); names.push_back("Gaussian"); names.push_back("Binary");
  params.addEnum("Distribution", &distribution, names, enums);

  paramsToDialog(dialog.page, params, geom);
}

double OperationRandomNoise::getRandom(Distribution d)
{
  switch(d)
  {
    case D_UNIFORM:
    {
      return lpi::getRandom();
    }
    case D_GAUSSIAN:
    {
      //idea gotten from "HAKMEM" (AIM-239.pdf)
      double x = lpi::getRandom();
      double y = lpi::getRandom() * 2 * 3.14159;
      double r = std::sqrt(-std::log(x));
      r = r * std::cos(y); //r * std::cos(y) and r * std::sin(y) are two independent gaussian-distributed variables
      r *= 0.33;
      r += 0.5;
      if(r < 0) r = 0;
      if(r > 1) r = 1;
      return r;
    }
    case D_BINARY:
    {
      return lpi::getRandom() > 0.5 ? 0.0 : 1.0;
    }
    default: return 0;
  }
}

double OperationRandomNoise::getRandom(double from, double to, Distribution d)
{
  return from + getRandom(d) * (to - from);
}

int OperationRandomNoise::getRandom(int from, int to, Distribution d)
{
  return (int)(from + getRandom(d) * (to - from));
}

void OperationRandomNoise::doit(TextureRGBA8& texture, Progress& progress)
{
  unsigned char* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();
  for(size_t y = 0; y < v; y++)
  {
    if(progress.handle(y, v)) return;
    for(size_t x = 0; x < u; x++)
    {
      if(saltpepper > 0 && getRandom(0.0, 1.0, D_UNIFORM) < saltpepper) continue;

      double o = opacity;
      if(randomize_opacity) o = getRandom(distribution) * o;

      size_t index = y * u2 * 4 + x * 4;
      if(colorType == C_RGB)
      {
        int r = getRandom(0, 255, distribution);
        int g = getRandom(0, 255, distribution);
        int b = getRandom(0, 255, distribution);

        buffer[index + 0] = (int)(r * o + buffer[index + 0] * (1.0 - o));
        buffer[index + 1] = (int)(g * o + buffer[index + 1] * (1.0 - o));
        buffer[index + 2] = (int)(b * o + buffer[index + 2] * (1.0 - o));

        if(alpha)
        {
          int a = getRandom(0, 255, distribution);
          buffer[index + 3] = (int)(a * o + buffer[index + 3] * (1.0 - o));
        }
        else buffer[index + 3] = (int)(255 * o + buffer[index + 3] * (1.0 - o));
      }
      else if(colorType == C_GREY)
      {
        int g = getRandom(0, 255, distribution);
        buffer[index + 0] = (int)(g * o + buffer[index + 0] * (1.0 - o));
        buffer[index + 1] = (int)(g * o + buffer[index + 1] * (1.0 - o));
        buffer[index + 2] = (int)(g * o + buffer[index + 2] * (1.0 - o));

        if(alpha)
        {
          int a = getRandom(0, 255, distribution);
          buffer[index + 3] = (int)(a * o + buffer[index + 3] * (1.0 - o));
        }
        else buffer[index + 3] = (int)(255 * o + buffer[index + 3] * (1.0 - o));
      }
      else if(colorType == C_FGBG)
      {
        int random = getRandom(0, 255, distribution);
        int r = (settings.leftColor255().r * random + settings.rightColor255().r * (255 - random)) / 255;
        int g = (settings.leftColor255().g * random + settings.rightColor255().g * (255 - random)) / 255;
        int b = (settings.leftColor255().b * random + settings.rightColor255().b * (255 - random)) / 255;
        int a = (settings.leftColor255().a * random + settings.rightColor255().a * (255 - random)) / 255;
        buffer[index + 0] = (int)(r * o + buffer[index + 0] * (1.0 - o));
        buffer[index + 1] = (int)(g * o + buffer[index + 1] * (1.0 - o));
        buffer[index + 2] = (int)(b * o + buffer[index + 2] * (1.0 - o));

        if(alpha) buffer[index + 3] = (int)(a * o + buffer[index + 3] * (1.0 - o));
        else buffer[index + 3] = (int)(255 * o + buffer[index + 3] * (1.0 - o));
      }
      else if(colorType == C_BRIGHTNESS)
      {
        int random = getRandom(0, 255, distribution);
        int r = lpi::clamp(buffer[index + 0] + 127 - random, 0, 255);
        int g = lpi::clamp(buffer[index + 1] + 127 - random, 0, 255);
        int b = lpi::clamp(buffer[index + 2] + 127 - random, 0, 255);
        int a = lpi::clamp(buffer[index + 3] + 127 - random, 0, 255);
        buffer[index + 0] = (int)(r * o + buffer[index + 0] * (1.0 - o));
        buffer[index + 1] = (int)(g * o + buffer[index + 1] * (1.0 - o));
        buffer[index + 2] = (int)(b * o + buffer[index + 2] * (1.0 - o));

        if(alpha) buffer[index + 3] = (int)(a * o + buffer[index + 3] * (1.0 - o));
        else buffer[index + 3] = (int)(255 * o + buffer[index + 3] * (1.0 - o));
      }
      else if(colorType == C_HUE)
      {
        int random = getRandom(0, 255, distribution);
        lpi::ColorHSV hsv = lpi::RGBtoHSV(lpi::ColorRGB(buffer[index + 0], buffer[index + 1], buffer[index + 2], buffer[index + 3]));
        hsv.h += random;
        if(hsv.h > 255) hsv.h -= 256;
        lpi::ColorRGB rgb = lpi::HSVtoRGB(hsv);

        buffer[index + 0] = (int)(rgb.r * o + buffer[index + 0] * (1.0 - o));
        buffer[index + 1] = (int)(rgb.g * o + buffer[index + 1] * (1.0 - o));
        buffer[index + 2] = (int)(rgb.b * o + buffer[index + 2] * (1.0 - o));
      }
      else if(colorType == C_SATURATION)
      {
        int random = getRandom(0, 255, distribution);
        lpi::ColorHSV hsv = lpi::RGBtoHSV(lpi::ColorRGB(buffer[index + 0], buffer[index + 1], buffer[index + 2], buffer[index + 3]));
        hsv.s = lpi::clamp(hsv.s + random - 127, 0, 255);
        lpi::ColorRGB rgb = lpi::HSVtoRGB(hsv);

        buffer[index + 0] = (int)(rgb.r * o + buffer[index + 0] * (1.0 - o));
        buffer[index + 1] = (int)(rgb.g * o + buffer[index + 1] * (1.0 - o));
        buffer[index + 2] = (int)(rgb.b * o + buffer[index + 2] * (1.0 - o));
      }
    }
  }
}

////////////////////////////////////////////////////////////////////////////////

OperationTurbulence::OperationTurbulence(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom)
: AOperationFilterUtil(settings, true, true)
, alpha(false)
, colored(true)
, opacity(1.0)
, size(64)
, algo(ALGO_BICUBIC)
, dialog(this)
{
  params.addBool("Affect Alpha", &alpha);
  params.addBool("Colored", &colored);
  params.addColor("FG Color", &settings.leftColor);
  params.addColor("BG Color", &settings.rightColor);
  params.addDouble("Opacity", &opacity, 0.0, 1.0, 0.01);
  params.addInt("Size", &size, 2, 128, 1);

  std::vector<Algo> enums; enums.push_back(ALGO_NEAREST); enums.push_back(ALGO_BILINEAR); enums.push_back(ALGO_BICUBIC);
  std::vector<std::string> names; names.push_back("Nearest (Blocky)"); names.push_back("Bilinear"); names.push_back("Bicubic");
  params.addEnum("Interpolation", &algo, names, enums);

  paramsToDialog(dialog.page, params, geom);
}

void OperationTurbulence::generateNoise()
{
  noise.resize(noiseSize * noiseSize);

  for(size_t i = 0; i < noise.size(); i++)
  {
    noise[i] = lpi::getRandom(0.0, 1.0);
  }
}

double OperationTurbulence::smoothNoise(double x, double y)
{

  if(algo == ALGO_NEAREST)
  {
    //wrap around
    int x1 = (int(x) + noiseSize) % noiseSize;
    int y1 = (int(y) + noiseSize) % noiseSize;
    return noise[x1 + y1 * noiseSize];
  }
  else if(algo == ALGO_BILINEAR)
  {
    //get fractional part of x and y
    double fractX = x - int(x);
    double fractY = y - int(y);

    //wrap around
    int x1 = (int(x) + noiseSize) % noiseSize;
    int y1 = (int(y) + noiseSize) % noiseSize;

    //neighbor values
    int x2 = (x1 + noiseSize - 1) % noiseSize;
    int y2 = (y1 + noiseSize - 1) % noiseSize;

    //smooth the noise with bilinear interpolation
    double value = 0.0;
    value += fractX       * fractY       * noise[x1 + y1 * noiseSize];
    value += fractX       * (1 - fractY) * noise[x1 + y2 * noiseSize];
    value += (1 - fractX) * fractY       * noise[x2 + y1 * noiseSize];
    value += (1 - fractX) * (1 - fractY) * noise[x2 + y2 * noiseSize];

    return value;
  }
  else if(algo == ALGO_BICUBIC)
  {
    //get fractional part of x and y
    double fractX = x - int(x);
    double fractY = y - int(y);

    //wrap around
    int x0 = (int(x) + noiseSize) % noiseSize;
    int y0 = (int(y) + noiseSize) % noiseSize;

    //neighbor values
    int xm = (x0 + noiseSize - 1) % noiseSize; //m from "minus"
    int ym = (y0 + noiseSize - 1) % noiseSize;
    int x1 = (x0 + noiseSize + 1) % noiseSize;
    int y1 = (y0 + noiseSize + 1) % noiseSize;
    int x2 = (x0 + noiseSize + 2) % noiseSize;
    int y2 = (y0 + noiseSize + 2) % noiseSize;

    double t0 = cubic(noise[xm + ym * noiseSize], noise[x0+ym * noiseSize], noise[x1+ym * noiseSize], noise[x2+ym * noiseSize], fractX);
    double t1 = cubic(noise[xm + y0 * noiseSize], noise[x0+y0 * noiseSize], noise[x1+y0 * noiseSize], noise[x2+y0 * noiseSize], fractX);
    double t2 = cubic(noise[xm + y1 * noiseSize], noise[x0+y1 * noiseSize], noise[x1+y1 * noiseSize], noise[x2+y1 * noiseSize], fractX);
    double t3 = cubic(noise[xm + y2 * noiseSize], noise[x0+y2 * noiseSize], noise[x1+y2 * noiseSize], noise[x2+y2 * noiseSize], fractX);

    double value = cubic(t0, t1, t2, t3, fractY);
    if(value > 1.0) value = 1.0;
    return value;
  }

  return 0.0;

}

double OperationTurbulence::turbulence(double x, double y, double size)
{
  double value = 0.0, initialSize = size;

  while(size >= 1)
  {
      value += smoothNoise(x / size, y / size) * size;
      size /= 2.0;
  }

  return(0.5 * value / initialSize);
}

void OperationTurbulence::doit(TextureRGBA8& texture, Progress& progress)
{
  generateNoise();

  unsigned char* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();
  for(size_t y = 0; y < v; y++)
  {
    if(progress.handle(y, v)) return;
    for(size_t x = 0; x < u; x++)
    {

      double turb = turbulence(x, y, size);
      if(turb > 1.0) turb = 1.0; if(turb < 0.0) turb = 0.0;
      int turb255 = (int)(turb * 255);

      size_t index = y * u2 * 4 + x * 4;
      if(colored)
      {

        int r, g, b, a;
        r = (int)(settings.leftColor255().r * turb + settings.rightColor255().r * (1.0 - turb));
        g = (int)(settings.leftColor255().g * turb + settings.rightColor255().g * (1.0 - turb));
        b = (int)(settings.leftColor255().b * turb + settings.rightColor255().b * (1.0 - turb));
        a = (int)(settings.leftColor255().a * turb + settings.rightColor255().a * (1.0 - turb));
        if(!alpha) a = 255;

        buffer[index + 0] = (int)(r * opacity + buffer[index + 0] * (1.0 - opacity));
        buffer[index + 1] = (int)(g * opacity + buffer[index + 1] * (1.0 - opacity));
        buffer[index + 2] = (int)(b * opacity + buffer[index + 2] * (1.0 - opacity));
        buffer[index + 3] = (int)(a * opacity + buffer[index + 3] * (1.0 - opacity));
      }
      else
      {
        int g = turb255;
        int a = alpha ? g : 255;
        buffer[index + 0] = (int)(g * opacity + buffer[index + 0] * (1.0 - opacity));
        buffer[index + 1] = (int)(g * opacity + buffer[index + 1] * (1.0 - opacity));
        buffer[index + 2] = (int)(g * opacity + buffer[index + 2] * (1.0 - opacity));
        buffer[index + 3] = (int)(a * opacity + buffer[index + 3] * (1.0 - opacity));
      }
    }
  }
}

////////////////////////////////////////////////////////////////////////////////

OperationMarble::OperationMarble(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom)
: OperationTurbulence(settings, geom)
, turbPower(32.0)
, xPeriod(40.0)
, yPeriod(20.0)
, pattern(STRIPES)
{

  std::vector<Pattern> enums; enums.push_back(STRIPES); enums.push_back(RINGS); enums.push_back(DOTS);
  std::vector<std::string> names; names.push_back("Stripes"); names.push_back("Rings"); names.push_back("Dots");
  params.addEnum("Sine Pattern", &pattern, names, enums);

  params.addDouble("Power", &turbPower, 0.0, 128.0, 1.00);
  params.addDouble("Period X", &xPeriod, 1.0, 100.0, 1.00);
  params.addDouble("Period Y", &yPeriod, 1.0, 100.0, 1.00);

  dialog.page.clear();
  paramsToDialog(dialog.page, params, geom);
}

void OperationMarble::doit(TextureRGBA8& texture, Progress& progress)
{
  generateNoise();

  unsigned char* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();
  for(size_t y = 0; y < v; y++)
  {
    if(progress.handle(y, v)) return;
    for(size_t x = 0; x < u; x++)
    {
      double turb = 0;

      if(pattern == STRIPES)
      {
        double xyValue = lpi::twopi * x / xPeriod + lpi::twopi * y / yPeriod + turbPower * turbulence(x, y, size);
        turb = std::fabs(std::sin(xyValue));
      }
      else if(pattern == RINGS)
      {
        double xValue = (double)x - (double)(u / 2);
        double yValue = (double)y - (double)(v / 2);
        double distValue = std::sqrt(xValue * xValue + yValue * yValue) + turbPower * turbulence(x, y, size);
        turb = std::fabs(std::sin(lpi::twopi * distValue / xPeriod));
      }
      else if(pattern == DOTS)
      {
        double xValue = lpi::twopi * x / xPeriod + turbPower * turbulence(x, y, size);
        double yValue = lpi::twopi * y / yPeriod + turbPower * turbulence(x + noiseSize / 2, y + noiseSize / 2, size);
        turb = 0.5 * std::fabs(std::sin(xValue) + std::sin(yValue));
      }

      int turb255 = (int)(turb * 255);

      size_t index = y * u2 * 4 + x * 4;
      if(colored)
      {

        int r, g, b, a;
        r = (int)(settings.leftColor255().r * turb + settings.rightColor255().r * (1.0 - turb));
        g = (int)(settings.leftColor255().g * turb + settings.rightColor255().g * (1.0 - turb));
        b = (int)(settings.leftColor255().b * turb + settings.rightColor255().b * (1.0 - turb));
        a = (int)(settings.leftColor255().a * turb + settings.rightColor255().a * (1.0 - turb));
        if(!alpha) a = 255;

        buffer[index + 0] = (int)(r * opacity + buffer[index + 0] * (1.0 - opacity));
        buffer[index + 1] = (int)(g * opacity + buffer[index + 1] * (1.0 - opacity));
        buffer[index + 2] = (int)(b * opacity + buffer[index + 2] * (1.0 - opacity));
        buffer[index + 3] = (int)(a * opacity + buffer[index + 3] * (1.0 - opacity));
      }
      else
      {
        int g = turb255;
        int a = alpha ? g : 255;
        buffer[index + 0] = (int)(g * opacity + buffer[index + 0] * (1.0 - opacity));
        buffer[index + 1] = (int)(g * opacity + buffer[index + 1] * (1.0 - opacity));
        buffer[index + 2] = (int)(g * opacity + buffer[index + 2] * (1.0 - opacity));
        buffer[index + 3] = (int)(a * opacity + buffer[index + 3] * (1.0 - opacity));
      }
    }
  }
}

////////////////////////////////////////////////////////////////////////////////

OperationGenerateTiles::OperationGenerateTiles(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom)
: AOperationFilterUtil(settings, true, true)
, sizex(32)
, sizey(32)
, shiftx(0)
, shifty(0)
, opacity0(1.0)
, opacity1(1.0)
, dialog(this)
{
  params.addInt("Size X", &sizex, 2, 256, 1);
  params.addInt("Size Y", &sizey, 2, 256, 1);
  params.addInt("Shift X", &shiftx, 2, 256, 1);
  params.addInt("Shift Y", &shifty, 2, 256, 1);
  params.addDouble("Opacity A", &opacity0, 0.0, 1.0, 0.01);
  params.addDouble("Opacity B", &opacity1, 0.0, 1.0, 0.01);
  params.addColor("FG Color", &settings.leftColor);
  params.addColor("BG Color", &settings.rightColor);

  paramsToDialog(dialog.page, params, geom);
}

void OperationGenerateTiles::doit(TextureRGBA8& texture, Progress&)
{
  unsigned char* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();
  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  {
    size_t index = y * u2 * 4 + x * 4;
    bool b = (((x + shiftx) / sizex) + ((y + shifty) / sizey)) % 2 == 0;
    lpi::ColorRGB color = b ? settings.leftColor255() : settings.rightColor255();
    double opacity = b ? opacity0 : opacity1;
    buffer[index + 0] = (int)(color.r * opacity + buffer[index + 0] * (1.0 - opacity));
    buffer[index + 1] = (int)(color.g * opacity + buffer[index + 1] * (1.0 - opacity));
    buffer[index + 2] = (int)(color.b * opacity + buffer[index + 2] * (1.0 - opacity));
    buffer[index + 3] = (int)(color.a * opacity + buffer[index + 3] * (1.0 - opacity));
  }
}

////////////////////////////////////////////////////////////////////////////////

OperationGenerateGradient::OperationGenerateGradient(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom)
: AOperationFilterUtil(settings, true, true)
, angle(0.0)
, opacity(1.0)
, affect_alpha(false)
, dialog(this)
{
  params.addDouble("Angle", &angle, 0.0, 360.0, 1.0);
  params.addDouble("Opacity", &opacity, 0.0, 1.0, 0.01);
  params.addBool("Affect Alpha", &affect_alpha);
  params.addColor("FG Color", &settings.leftColor);
  params.addColor("BG Color", &settings.rightColor);

  paramsToDialog(dialog.page, params, geom);
}

void OperationGenerateGradient::doit(TextureRGBA8& texture, Progress&)
{
  unsigned char* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();

  lpi::Vector2 a(u / 2.0, v / 2.0);
  double rad = angle * 2 * 3.14159 / 360.0;
  lpi::Vector2 b(a.x + std::cos(rad), a.y + std::sin(rad));

  double d0 = lpi::distancePointLine(lpi::Vector2(u, v), a, b);
  double d1 = lpi::distancePointLine(lpi::Vector2(0, v), a, b);
  double d = std::max(d0, d1);

  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  {
    size_t index = y * u2 * 4 + x * 4;

    lpi::Vector2 p(x, y);
    double val = lpi::distancePointLine(p, a, b) / d;
    if(lpi::sideOfLineGivenByTwoPoints(p, a, b)) val = -val;
    val += 1.0;
    val /= 2;


    lpi::ColorRGB color = (val & settings.leftColor255()) | ((1.0 - val) & settings.rightColor255());
    buffer[index + 0] = (int)(color.r * opacity + buffer[index + 0] * (1.0 - opacity));
    buffer[index + 1] = (int)(color.g * opacity + buffer[index + 1] * (1.0 - opacity));
    buffer[index + 2] = (int)(color.b * opacity + buffer[index + 2] * (1.0 - opacity));
    if(affect_alpha) buffer[index + 3] = (int)(color.a * opacity + buffer[index + 3] * (1.0 - opacity));
  }
}

////////////////////////////////////////////////////////////////////////////////

OperationGenerateGradientQuad::OperationGenerateGradientQuad(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom)
: AOperationFilterUtil(settings, true, true)
, color00(lpi::RGBd_Red)
, color01(lpi::RGBd_Yellow)
, color10(lpi::RGBd_Blue)
, color11(lpi::RGBd_Green)
, opacity(1.0)
, affect_alpha(false)
, dialog(this)
{
  params.addDouble("Opacity", &opacity, 0.0, 1.0, 0.01);
  params.addBool("Affect Alpha", &affect_alpha);
  params.addColor("Color 00", &color00);
  params.addColor("Color 01", &color01);
  params.addColor("Color 10", &color10);
  params.addColor("Color 11", &color11);

  paramsToDialog(dialog.page, params, geom);
}

void OperationGenerateGradientQuad::doit(TextureRGBA8& texture, Progress&)
{
  unsigned char* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();


  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  {
    size_t index = y * u2 * 4 + x * 4;

    double vx1 = (double)x / (double)u;
    double vy1 = (double)y / (double)v;
    double vx0 = 1.0 - vx1;
    double vy0 = 1.0 - vy1;

    double v00 = vx0 * vy0;
    double v01 = vx0 * vy1;
    double v10 = vx1 * vy0;
    double v11 = vx1 * vy1;

    lpi::ColorRGBd colord = (v00 & color00) | (v01 & color01) | (v10 & color10) | (v11 & color11);

    lpi::ColorRGB color = lpi::RGBdtoRGB(colord);
    buffer[index + 0] = (int)(color.r * opacity + buffer[index + 0] * (1.0 - opacity));
    buffer[index + 1] = (int)(color.g * opacity + buffer[index + 1] * (1.0 - opacity));
    buffer[index + 2] = (int)(color.b * opacity + buffer[index + 2] * (1.0 - opacity));
    if(affect_alpha) buffer[index + 3] = (int)(color.a * opacity + buffer[index + 3] * (1.0 - opacity));
  }
}

////////////////////////////////////////////////////////////////////////////////

OperationPlasma::OperationPlasma(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom)
: AOperationFilterUtil(settings, true, true)
, opacity(1.0)
, dialog(this)
{
  p.sins.resize(4);

  p.sins[0].period_x = 50.0;
  p.sins[0].period_y = 74.0;
  //p.sins[0].color = lpi::ColorRGBd(1.0, 0.5, 0.0, 1.0);
  p.sins[1].period_x = 181.0;
  p.sins[1].period_y = 191.0;

  p.sins[2].circular = true;
  p.sins[2].period_x = 50.0;
  p.sins[2].period_y = 175.0;

  p.sins[3].circular = true;
  p.sins[3].period_x = 71.0;
  p.sins[3].shift_x = 1000.0;
  p.sins[3].shift_y = -1000.0;

  params.addDouble("Opacity", &opacity, 0.0, 1.0, 0.01);

  params.addLabel("Sine 1 (Grid)");
  params.addDouble("Period X 1", &p.sins[0].period_x, 1.0, 200.0, 1.0);
  params.addDouble("Period Y 1", &p.sins[0].period_y, 1.0, 200.0, 1.0);
  //params.addDouble("Shift X 1", &p.sins[0].shift_x, -200.0, 200.0, 1.0);
  //params.addDouble("Shift Y 1", &p.sins[0].shift_y, -200.0, 200.0, 1.0);
  params.addDouble("Power 1", &p.sins[0].power, 0.0, 1.0, 0.01);
  //params.addColor("Color 1", &p.sins[0].color);

  params.addLabel("Sine 2 (Grid)");
  params.addDouble("Period X 2", &p.sins[1].period_x, 1.0, 200.0, 1.0);
  params.addDouble("Period Y 2", &p.sins[1].period_y, 1.0, 200.0, 1.0);
  //params.addDouble("Shift X 2", &p.sins[1].shift_x, -200.0, 200.0, 1.0);
  //params.addDouble("Shift Y 2", &p.sins[1].shift_y, -200.0, 200.0, 1.0);
  params.addDouble("Power 2", &p.sins[1].power, 0.0, 1.0, 0.01);
  //params.addColor("Color 2", &p.sins[1].color);

  params.addLabel("Sine 3 (Circular)");
  params.addDouble("Period X 3", &p.sins[2].period_x, 1.0, 200.0, 1.0);
  //params.addDouble("Period Y 3", &p.sins[2].period_y, 1.0, 200.0, 1.0);
  params.addDouble("Shift X 3", &p.sins[2].shift_x, -1000.0, 1000.0, 1.0);
  params.addDouble("Shift Y 3", &p.sins[2].shift_y, -1000.0, 1000.0, 1.0);
  params.addDouble("Power 3", &p.sins[2].power, 0.0, 1.0, 0.01);
  //params.addColor("Color 3", &p.sins[2].color);

  params.addLabel("Sine 4 (Circular)");
  params.addDouble("Period X 4", &p.sins[3].period_x, 1.0, 200.0, 1.0);
  //params.addDouble("Period Y 4", &p.sins[3].period_y, 1.0, 200.0, 1.0);
  params.addDouble("Shift X 4", &p.sins[3].shift_x, -1000.0, 1000.0, 1.0);
  params.addDouble("Shift Y 4", &p.sins[3].shift_y, -1000.0, 1000.0, 1.0);
  params.addDouble("Power 4", &p.sins[3].power, 0.0, 1.0, 0.01);
  //params.addColor("Color 4", &p.sins[3].color);

  paramsToDialog(dialog.page, params, geom);
}

void OperationPlasma::doit(TextureRGBA8& texture, Progress&)
{
  generatePlasmaRGBA8(texture.getBuffer(), texture.getU(), texture.getV(), texture.getU2(), p, opacity);
}

////////////////////////////////////////////////////////////////////////////////

OperationGenerateGrid::OperationGenerateGrid(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom)
: AOperationFilterUtil(settings, true, true)
, sizex(32)
, sizey(32)
, shiftx(0)
, shifty(0)
, opacity(1.0)
, dialog(this)
{
  params.addInt("Size X", &sizex, 2, 256, 1);
  params.addInt("Size Y", &sizey, 2, 256, 1);
  params.addInt("Shift X", &shiftx, 0, 256, 1);
  params.addInt("Shift Y", &shifty, 0, 256, 1);
  params.addDouble("Opacity", &opacity, 0.0, 1.0, 0.01);
  params.addColor("FG Color", &settings.leftColor);

  paramsToDialog(dialog.page, params, geom);
}

void OperationGenerateGrid::doit(TextureRGBA8& texture, Progress&)
{
  unsigned char* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();
  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  {
    if(!((y + shifty) % sizey == 0 || (x + shiftx) % sizex == 0)) continue;
    size_t index = y * u2 * 4 + x * 4;
    lpi::ColorRGB color = settings.leftColor255();
    buffer[index + 0] = (int)(color.r * opacity + buffer[index + 0] * (1.0 - opacity));
    buffer[index + 1] = (int)(color.g * opacity + buffer[index + 1] * (1.0 - opacity));
    buffer[index + 2] = (int)(color.b * opacity + buffer[index + 2] * (1.0 - opacity));
    buffer[index + 3] = (int)(color.a * opacity + buffer[index + 3] * (1.0 - opacity));
  }
}

////////////////////////////////////////////////////////////////////////////////

int OperationGenerateXORTexture::getMaxValue(int u, int v)
{
  if(u == 0 || v == 0) return 1;

  //calculate max value xor can get, to scale colors. The max value is the first power of two equal to or higher than the largest of width and height, minus 1.
  int m = std::max(u, v);
  int i = 1;
  while(i < m) i *= 2;

  return i - 1;
}

void OperationGenerateXORTexture::doit(TextureRGBA8& texture, Progress&)
{
  unsigned char* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();

  int maxValue = getMaxValue(u, v);


  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  {
    size_t index = y * u2 * 4 + x * 4;

    int result = (int)( 255.0 * (double)(x ^ y) / (double)(maxValue));

    buffer[index + 0] = result;
    buffer[index + 1] = result;
    buffer[index + 2] = result;
  }
}

void OperationGenerateXORTexture::doit(TextureRGBA32F& texture, Progress&)
{
  float* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();

  int maxValue = getMaxValue(u, v);

  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  {
    size_t index = y * u2 * 4 + x * 4;

    float result = (float)(x ^ y) / (float)(maxValue);

    buffer[index + 0] = result;
    buffer[index + 1] = result;
    buffer[index + 2] = result;
  }
}

void OperationGenerateXORTexture::doit(TextureGrey8& texture, Progress&)
{
  unsigned char* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();

  int maxValue = getMaxValue(u, v);

  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  {
    size_t index = y * u2 * 4 + x * 4;

    int result = (int)(255.0 * (double)(x ^ y) / (double)(maxValue));

    buffer[index] = result;
  }
}

void OperationGenerateXORTexture::doit(TextureGrey32F& texture, Progress&)
{
  float* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();

  int maxValue = getMaxValue(u, v);

  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  {
    size_t index = y * u2 + x;

    float result = (float)(x ^ y) / (float)(maxValue);

    buffer[index] = result;
  }
}

////////////////////////////////////////////////////////////////////////////////

void OperationGenerateTestImage::doit(TextureRGBA8& texture, Progress&)
{
  unsigned char* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();


  // num squares
  int numx, numy;
  if(u >= v)
  {
    numx = 20;
    numy = (v * 20) / u;
    if(numy % 2 == 0) numy++;
  }
  else
  {
    numy = 21;
    numx = (u * 21) / v;
    if(numx % 2 == 1) numx++;
  }
  // size of squares
  float sx = u / (float)numx;
  float sy = v / (float)numy;

  for(int ny = 0; ny < numy; ny++)
  for(int nx = 0; nx < numx; nx++)
  {
    int startx = (int)(sx * nx);
    int endx = (int)(sx * (nx + 1));
    int starty = (int)(sy * ny);
    int endy = (int)(sy * (ny + 1));

    unsigned char c1 = 128, c2 = 255;
    if(nx == 0 || ny == 0 || nx == numx - 1 || ny == numy - 1)
    {
      c1 = (nx + ny) % 2 == 0 ? 64 : 192;
      c2 = c1;
    }

    for(int y = starty; y < endy; y++)
    for(int x = startx; x < endx; x++)
    {
      size_t index = y * u2 * 4 + x * 4;
      unsigned char c = c1;
      static const int D = 20;
      if(x - startx < sx / D || y - starty < sy / D || x - startx >= sx - sx / D || y - starty >= sy - sy / D) c = c2;

      buffer[index + 0] = c;
      buffer[index + 1] = c;
      buffer[index + 2] = c;
      buffer[index + 3] = 255;
    }
  }

  int x0, y0, x1, y1;

  // Circle
  if(u > v)
  {
    y0 = sy * 3 / 2;
    y1 = v - sy * 3 / 2;
    x0 = u / 2 - (y1 - y0) / 2;
    x1 = u / 2 + (y1 - y0) / 2;
  }
  else
  {
    x0 = sx * 3 / 2;
    x1 = u - sx * 3 / 2;
    y0 = v / 2 - (x1 - x0) / 2;
    y1 = v / 2 + (x1 - x0) / 2;
  }

  drawEllipse(buffer, u2, v
            , x0, y0, x1, y1
            , 0
            , true, lpi::RGB_White, 1
            , true, lpi::RGB_White, 1
            , false);

  // Hue bar
  x0 = sx * 3;
  y0 = sy * 3;
  x1 = (numx - 3) * sx - 1;
  y1 = sy * 4;

  for(int y = y0; y < y1; y++)
  for(int x = x0; x < x1; x++)
  {
    size_t index = y * u2 * 4 + x * 4;
    float v = (float)(x - x0) / (float)(x1 - x0);

    lpi::ColorRGBd rgb = lpi::HSVtoRGB(lpi::ColorHSVd(v, 1, 1));

    buffer[index + 0] = (int)(rgb.r * 255);
    buffer[index + 1] = (int)(rgb.g * 255);
    buffer[index + 2] = (int)(rgb.b * 255);
  }

  // ? bar
  x0 = sx * 4;
  y0 = sy * 5;
  x1 = (numx - 4) * sx - 1;
  y1 = sy * 6;

  for(int y = y0; y < y1; y++)
  for(int x = x0; x < x1; x++)
  {
    size_t index = y * u2 * 4 + x * 4;
    int v = (int)((float)(x - x0) / (float)(x1 - x0) * 255);

    buffer[index + 0] = 255 - v;
    buffer[index + 1] = v;
    buffer[index + 2] = v;
  }

  // Faint hue bar
  x0 = sx * 4;
  y0 = (numy - 6) * sy - 1;
  x1 = (numx - 4) * sx - 1;
  y1 = (numy - 5) * sy - 1;

  for(int y = y0; y < y1; y++)
  for(int x = x0; x < x1; x++)
  {
    size_t index = y * u2 * 4 + x * 4;
    float v = (float)(x - x0) / (float)(x1 - x0);

    lpi::ColorRGBd rgb = lpi::HSVtoRGB(lpi::ColorHSVd(v, 0.5, 0.5));

    buffer[index + 0] = (int)(rgb.r * 255);
    buffer[index + 1] = (int)(rgb.g * 255);
    buffer[index + 2] = (int)(rgb.b * 255);
  }

  // Grey bar
  x0 = sx * 3;
  y0 = (numy - 4) * sy - 1;
  x1 = (numx - 3) * sx - 1;
  y1 = (numy - 3) * sy - 1;

  for(int y = y0; y < y1; y++)
  for(int x = x0; x < x1; x++)
  {
    size_t index = y * u2 * 4 + x * 4;
    int v = (int)((float)(x - x0) / (float)(x1 - x0) * 255);

    buffer[index + 0] = v;
    buffer[index + 1] = v;
    buffer[index + 2] = v;
  }

  // Random bar
  x0 = sx * 2;
  y0 = (numy / 2 - 1) * sy - 1;
  x1 = (numx - 2) * sx - 1;
  y1 = (numy / 2) * sy - 1;

  for(int y = y0; y < y1; y++)
  for(int x = x0; x < x1; x++)
  {
    size_t index = y * u2 * 4 + x * 4;
    float v = (float)(x - x0) / (float)(x1 - x0);

    if(lpi::getRandom() < v)
    {
      buffer[index + 0] = (int)(lpi::getRandom() * 255);
      buffer[index + 1] = (int)(lpi::getRandom() * 255);
      buffer[index + 2] = (int)(lpi::getRandom() * 255);
    }
    else
    {
      int r = (int)(lpi::getRandom() * 255);
      buffer[index + 0] = r;
      buffer[index + 1] = r;
      buffer[index + 2] = r;
    }
  }

  // CGA bar
  x0 = sx * 2;
  y0 = (numy / 2 + 1) * sy - 1;
  x1 = (numx - 2) * sx - 1;
  y1 = (numy / 2 + 2) * sy - 1;

  for(int y = y0; y < y1; y++)
  for(int x = x0; x < x1; x++)
  {
    size_t index = y * u2 * 4 + x * 4;
    float v = (float)(x - x0) / (float)(x1 - x0);
    int c = (int)(v * 16);

    int r = 170 * ((c & 4) / 4) + 85 * ((c & 8) / 8);
    int g = 170 * ((c & 2) / 2) + 85 * ((c & 8) / 8);
    int b = 170 * ((c & 1) / 1) + 85 * ((c & 8) / 8);
    if(c == 6) g /= 2;

    buffer[index + 0] = r;
    buffer[index + 1] = g;
    buffer[index + 2] = b;
  }
}

////////////////////////////////////////////////////////////////////////////////

OperationGenerateMandelbrot::OperationGenerateMandelbrot(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom)
: AOperationFilterUtil(settings, true, true)
, dialog(this)
, zoom(1.0)
, moveX(-0.5)
, moveY(0.0)
, maxIterations(50)
, normalizedIteration(true)
, hue(true)
, tricorn(false)
{
  params.addDouble("Zoom", &zoom, 0.1, 250.0, 0.1);
  params.addDouble("Move X", &moveX, -2.0, 2.0, 0.001);
  params.addDouble("Move Y", &moveY, -2.0, 2.0, 0.001);
  params.addInt("Max Iterations", &maxIterations, 3, 300, 1);
  params.addBool("Normalized Iteration", &normalizedIteration);
  params.addBool("Tricorn", &tricorn);
  params.addBool("Hue Colored", &hue);

  paramsToDialog(dialog.page, params, geom);
}

void OperationGenerateMandelbrot::doit(TextureRGBA8& texture, Progress& progress)
{
  unsigned char* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();

  //each iteration, it calculates: newz = oldz*oldz + p, where p is the current pixel, and oldz stars at the origin
  double newRe, newIm, oldRe, oldIm;   //real and imaginary parts of new and old z


  //loop through every pixel
  for(int y = 0; y < (int)v; y++)
  {
    for(int x = 0; x < (int)u; x++)
    {
      size_t index = y * u2 * 4 + x * 4;

      //calculate the initial real and imaginary part of z, based on the pixel location and zoom and position values
      int w = u < v ? u : v;
      double pr = 1.66 * (x - u / 2.0) / (0.5 * zoom * w) + moveX;
      double pi = 1.66 * (y - v / 2.0) / (0.5 * zoom * w) + moveY;

      newRe = newIm = oldRe = oldIm = 0; //these should start at 0,0
      //"i" will represent the number of iterations
      int i = 0;

      if(!tricorn)
      {
        double q = (pr - 0.25) * (pr - 0.25) + pi * pi;
        //check if point is in the main cardoid and if so don't do the iteration process (optimization)
        if(q * (q + (pr - 0.25)) < 0.25 * pi * pi) i = maxIterations;
        //similar test for the level 2 bulb
        else if((pr + 1) * (pr + 1) + pi * pi < 1.0 / 16.0) i = maxIterations;
      }

      //start the iteration process
      for(; i < maxIterations; i++)
      {
        //remember value of previous iteration
        oldRe = newRe;
        oldIm = newIm;
        //the actual iteration, the real and imaginary part are calculated
        newRe = oldRe * oldRe - oldIm * oldIm + pr;
        newIm = 2 * oldRe * oldIm + pi;
        if(tricorn) newIm *= -1.0;

        //if the point is outside the circle with radius 2: stop (for normalizedIteration, use higher radius to have smoother result)
        if((newRe * newRe + newIm * newIm) > (normalizedIteration ? 16*16 : 2*2)) break;
      }

      double val = 0.0;
      if(normalizedIteration)
      {
        double dz = std::sqrt(newRe*newRe+newIm*newIm);
        static const double log2 = std::log(2.0);
        static const double log16 = std::log(16.0);
        double d = i < maxIterations ? std::log((std::log(dz) / log16)) / log2 : 1;
        val = (i - d + 1) / (double)(maxIterations);
      }
      else val = i / (double)(maxIterations);


      if(hue)
      {
        //lpi::ColorRGB color = lpi::HSVtoRGB(lpi::ColorHSV(val, 255, 255 * (i < maxIterations)));
        lpi::ColorRGB color = lpi::RGBdtoRGB(lpi::HSVtoRGB(lpi::ColorHSVd(val, 1.0, 1.0 * (i < maxIterations))));
        buffer[index + 0] = color.r;
        buffer[index + 1] = color.g;
        buffer[index + 2] = color.b;
      }
      else
      {
        int val255 = (int)(255 * val);
        buffer[index + 0] = val255;
        buffer[index + 1] = val255;
        buffer[index + 2] = val255;
      }
    }
    if(progress.handle(y, v)) return;
  }
}

////////////////////////////////////////////////////////////////////////////////

OperationGenerateJulia::OperationGenerateJulia(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom)
: AOperationFilterUtil(settings, true, true)
, dialog(this)
, zoom(1.0)
, moveX(-0.0)
, moveY(0.0)
, re(-0.7)
, im(0.27015)
, maxIterations(50)
, normalizedIteration(true)
, hue(true)
, tricorn(false)
{
  params.addDouble("Re", &re, -2.0, 2.0, 0.001);
  params.addDouble("Im", &im, -2.0, 2.0, 0.001);
  params.addDouble("Zoom", &zoom, 0.1, 250.0, 0.1);
  params.addDouble("Move X", &moveX, -2.0, 2.0, 0.001);
  params.addDouble("Move Y", &moveY, -2.0, 2.0, 0.001);
  params.addInt("Max Iterations", &maxIterations, 3, 300, 1);
  params.addBool("Normalized Iteration", &normalizedIteration);
  params.addBool("Tricorn", &tricorn);
  params.addBool("Hue Colored", &hue);

  paramsToDialog(dialog.page, params, geom);
}

void OperationGenerateJulia::doit(TextureRGBA8& texture, Progress& progress)
{
  unsigned char* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();

  //each iteration, it calculates: newz = oldz*oldz + p, where p is the current pixel, and oldz stars at the origin
  double newRe, newIm, oldRe, oldIm;   //real and imaginary parts of new and old z

  //loop through every pixel
  for(int y = 0; y < (int)v; y++)
  {
    for(int x = 0; x < (int)u; x++)
    {
      size_t index = y * u2 * 4 + x * 4;

      //calculate the initial real and imaginary part of z, based on the pixel location and zoom and position values
      int w = u < v ? u : v;
      newRe = 1.66 * (x - u / 2.0) / (0.5 * zoom * w) + moveX;
      newIm = 1.66 * (y - v / 2.0) / (0.5 * zoom * w) + moveY;
      //i will represent the number of iterations
      int i;
      //start the iteration process
      for(i = 0; i < maxIterations; i++)
      {
          //remember value of previous iteration
          oldRe = newRe;
          oldIm = newIm;
          //the actual iteration, the real and imaginary part are calculated
          newRe = oldRe * oldRe - oldIm * oldIm + re;
          newIm = 2 * oldRe * oldIm + im;
          if(tricorn) newIm *= -1;
          //if the point is outside the circle with radius 2: stop
          if((newRe * newRe + newIm * newIm) > (normalizedIteration ? 16*16 : 2*2)) break;
      }

      double val = 0.0;
      if(normalizedIteration)
      {
        double dz = std::sqrt(newRe*newRe+newIm*newIm);
        static const double log2 = std::log(2.0);
        static const double log16 = std::log(16.0);
        double d = i < maxIterations ? std::log((std::log(dz) / log16)) / log2 : 1;
        val = (i - d + 1) / (double)(maxIterations);
      }
      else val = i / (double)(maxIterations);

      if(hue)
      {
        //lpi::ColorRGB color = lpi::HSVtoRGB(lpi::ColorHSV(val, 255, 255 * (i < maxIterations)));
        lpi::ColorRGB color = lpi::RGBdtoRGB(lpi::HSVtoRGB(lpi::ColorHSVd(val, 1.0, 1.0 * (i < maxIterations))));
        buffer[index + 0] = color.r;
        buffer[index + 1] = color.g;
        buffer[index + 2] = color.b;
      }
      else
      {
        int val255 = (int)(255 * val);
        buffer[index + 0] = val255;
        buffer[index + 1] = val255;
        buffer[index + 2] = val255;
      }
    }
    if(progress.handle(y, v)) return;
  }

}

////////////////////////////////////////////////////////////////////////////////

OperationMedianFilter::OperationMedianFilter(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom)
: AOperationFilterUtil(settings, true, true)
, diameterx(3)
, diametery(3)
, wrapping(false)
, affect_alpha(true)
, dialog(this)
{
  params.addInt("Diameter X", &diameterx, 2, 9, 1);
  params.addInt("Diameter Y", &diametery, 2, 9, 1);
  params.addBool("Wrapping", &wrapping);
  params.addBool("Affect Alpha", &affect_alpha);

  paramsToDialog(dialog.page, params, geom);
}

void OperationMedianFilter::doit(TextureRGBA8& texture, Progress& progress)
{
  unsigned char* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();

  medianFilterRGBA8(buffer, u, v, u2, diameterx, diametery, wrapping, affect_alpha, progress);
}

void OperationMedianFilter::doit(TextureRGBA32F& texture, Progress& progress)
{
  float* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();

  medianFilterRGBA32F(buffer, u, v, u2, diameterx, diametery, wrapping, affect_alpha, progress);
}

////////////////////////////////////////////////////////////////////////////////

OperationAdaptiveMedianFilter::OperationAdaptiveMedianFilter(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom)
: AOperationFilterUtil(settings, true, true)
, maxdiameter(5)
, wrapping(false)
, affect_alpha(true)
, dialog(this)
{
  params.addInt("Max Diameter", &maxdiameter, 3, 11, 2);
  params.addBool("Wrapping", &wrapping);
  params.addBool("Affect Alpha", &affect_alpha);

  paramsToDialog(dialog.page, params, geom);
}

void OperationAdaptiveMedianFilter::doit(TextureRGBA8& texture, Progress& progress)
{
  unsigned char* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();

  adaptiveMedianFilterRGBA8(buffer, u, v, u2, maxdiameter, wrapping, affect_alpha, progress);
}

////////////////////////////////////////////////////////////////////////////////

OperationBlur::OperationBlur(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom)
: AOperationFilterUtil(settings, true, true)
, amount(NORMAL)
, wrapping(false)
, affect_alpha(true)
, dialog(this)
{
  std::vector<Amount> enums; enums.push_back(LESS); enums.push_back(NORMAL); enums.push_back(MORE);
  std::vector<std::string> names; names.push_back("Blur Less"); names.push_back("Normal (3x3)"); names.push_back("Blur More (5x5)");
  params.addEnum("Amount", &amount, names, enums);
  params.addBool("Wrapping", &wrapping);
  params.addBool("Affect Alpha", &affect_alpha);

  paramsToDialog(dialog.page, params, geom);
}

void OperationBlur::doit(TextureRGBA8& texture, Progress&)
{
  if(amount == LESS)
  {
    static const double DIV = 8.0;
    static const double matrix[9] =
    {
      0/DIV, 1/DIV, 0/DIV,
      1/DIV, 4/DIV, 1/DIV,
      0/DIV, 1/DIV, 0/DIV
    };

    convolute3x3(texture, matrix, wrapping, affect_alpha, 0.0);

  }
  else if(amount == NORMAL)
  {
    //static const double matrix[9] =
    //{
      //0.0,0.2,0.0,
      //0.2,0.2,0.2,
      //0.0,0.2,0.0
    //};

    //static const double matrix[9] =
    //{
      //1.0/9,1.0/9,1.0/9,
      //1.0/9,1.0/9,1.0/9,
      //1.0/9,1.0/9,1.0/9
    //};

    static const double matrix[9] =
    {
      1/16.0, 2/16.0, 1/16.0,
      2/16.0, 4/16.0, 2/16.0,
      1/16.0, 2/16.0, 1/16.0
    };

    convolute3x3(texture, matrix, wrapping, affect_alpha, 0.0);
  }
  else if(amount == MORE)
  {
    static const double matrix[25] =
    {
      1.0/331,  4.0/331,  7.0/331,  4.0/331, 1.0/331,
      4.0/331, 20.0/331, 33.0/331, 20.0/331, 4.0/331,
      7.0/331, 33.0/331, 55.0/331, 33.0/331, 7.0/331,
      4.0/331, 20.0/331, 33.0/331, 20.0/331, 4.0/331,
      1.0/331,  4.0/331,  7.0/331,  4.0/331, 1.0/331
    };

    convolute5x5(texture, matrix, wrapping, affect_alpha, 0.0);
  }

}

void OperationBlur::doit(TextureRGBA32F& texture, Progress&)
{
  if(amount == LESS)
  {
    static const double DIV = 8.0;
    static const double matrix[9] =
    {
      0/DIV, 1/DIV, 0/DIV,
      1/DIV, 4/DIV, 1/DIV,
      0/DIV, 1/DIV, 0/DIV
    };

    convolute3x3(texture, matrix, wrapping, affect_alpha, 0.0);

  }
  else if(amount == NORMAL)
  {
    //static const double matrix[9] =
    //{
      //0.0,0.2,0.0,
      //0.2,0.2,0.2,
      //0.0,0.2,0.0
    //};

    //static const double matrix[9] =
    //{
      //1.0/9,1.0/9,1.0/9,
      //1.0/9,1.0/9,1.0/9,
      //1.0/9,1.0/9,1.0/9
    //};

    static const double matrix[9] =
    {
      1/16.0, 2/16.0, 1/16.0,
      2/16.0, 4/16.0, 2/16.0,
      1/16.0, 2/16.0, 1/16.0
    };

    convolute3x3(texture, matrix, wrapping, affect_alpha, 0.0);
  }
  else if(amount == MORE)
  {
    static const double matrix[25] =
    {
      1.0/331,  4.0/331,  7.0/331,  4.0/331, 1.0/331,
      4.0/331, 20.0/331, 33.0/331, 20.0/331, 4.0/331,
      7.0/331, 33.0/331, 55.0/331, 33.0/331, 7.0/331,
      4.0/331, 20.0/331, 33.0/331, 20.0/331, 4.0/331,
      1.0/331,  4.0/331,  7.0/331,  4.0/331, 1.0/331
    };

    convolute5x5(texture, matrix, wrapping, affect_alpha, 0.0);
  }
}

////////////////////////////////////////////////////////////////////////////////

OperationSoften::OperationSoften(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom)
: AOperationFilterUtil(settings, true, true)
{
  (void)geom;
}

void OperationSoften::doit(TextureRGBA8& texture, Progress& progress)
{
  unsigned char* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();

  softestBlurRGBA8(buffer, u, v, u2, 16, false, true, progress);
}

////////////////////////////////////////////////////////////////////////////////

OperationSoftestBlur::OperationSoftestBlur(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom)
: AOperationFilterUtil(settings, true, true)
{
  (void)geom;
}

void OperationSoftestBlur::doit(TextureRGBA8& texture, Progress& progress)
{
  unsigned char* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();

  softestBlurRGBA8(buffer, u, v, u2, 1, false, true, progress);
}

////////////////////////////////////////////////////////////////////////////////

OperationSharpen::OperationSharpen(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom)
: AOperationFilterUtilWithSettings(settings, true, true)
, wrapping(false)
, affect_alpha(true)
, dialog(this)
{
  (void)geom;
  params.addBool("Wrapping", &wrapping);
  params.addBool("Affect Alpha", &affect_alpha);

  paramsToDialog(dialog.page, params, geom);
}

void OperationSharpen::doit(TextureRGBA8& texture, const GlobalToolSettings& settings, Progress&)
{
  (void)settings;

  static const double matrix[9] =
  {
    0, -1, 0,
   -1, 5, -1,
    0, -1, 0
  };

  convolute3x3(texture, matrix, wrapping, affect_alpha, 0.0);
}

void OperationSharpen::doit(TextureRGBA32F& texture, const GlobalToolSettings& settings, Progress&)
{
  (void)settings;

  static const double matrix[9] =
  {
    0, -1, 0,
   -1, 5, -1,
    0, -1, 0
  };

  convolute3x3(texture, matrix, wrapping, affect_alpha, 0.0);
}

////////////////////////////////////////////////////////////////////////////////

OperationGaussianBlur::OperationGaussianBlur(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom)
: AOperationFilterUtil(settings, true, true)
, radius(8.0)
, wrapping(false)
, affect_alpha(true)
, dialog(this)
{
  params.addDouble("Radius", &radius, 2.0, 50.0, 1.0);
  params.addBool("Wrapping", &wrapping);
  params.addBool("Affect Alpha", &affect_alpha);

  paramsToDialog(dialog.page, params, geom);
}

void OperationGaussianBlur::doit(TextureRGBA8& texture, Progress& progress)
{
  unsigned char* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();

  gaussianBlurRGBA8(buffer, u, v, u2, radius, wrapping, affect_alpha, progress);
}

void OperationGaussianBlur::doit(TextureRGBA32F& texture, Progress& progress)
{
  float* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();

  gaussianBlurRGBA32F(buffer, u, v, u2, radius, wrapping, affect_alpha, progress);
}
void OperationGaussianBlur::doit(TextureGrey8& texture, Progress& progress)
{
  unsigned char* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();

  gaussianBlurGrey8(buffer, u, v, u2, radius, wrapping, progress);
}

void OperationGaussianBlur::doit(TextureGrey32F& texture, Progress& progress)
{
  float* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();

  gaussianBlurGrey32F(buffer, u, v, u2, radius, wrapping, progress);
}

////////////////////////////////////////////////////////////////////////////////

OperationInterpolatingBlur::OperationInterpolatingBlur(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom)
: AOperationFilterUtil(settings, true, true)
, radius(8)
, affect_alpha(true)
, dialog(this)
{
  params.addInt("Radius", &radius, 2, 50, 1);
  params.addBool("Affect Alpha", &affect_alpha);

  paramsToDialog(dialog.page, params, geom);
}

void OperationInterpolatingBlur::doit(TextureRGBA8& texture, Progress& progress)
{
  unsigned char* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();

  interpolatingBlurRGBA8(buffer, u, v, u2, radius, affect_alpha, progress);
}
////////////////////////////////////////////////////////////////////////////////

OperationSelectiveGaussianBlur::OperationSelectiveGaussianBlur(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom)
: AOperationFilterUtil(settings, true, true)
, radius(3.0)
, threshold(50.0)
, wrapping(false)
, affect_alpha(true)
, dialog(this)
{
  params.addDouble("Radius", &radius, 2.0, 50.0, 1.0);
  params.addDouble("Threshold", &threshold, 1.0, 255.0, 1.0);
  params.addBool("Wrapping", &wrapping);
  params.addBool("Affect Alpha", &affect_alpha);

  paramsToDialog(dialog.page, params, geom);
}

void OperationSelectiveGaussianBlur::doit(TextureRGBA8& texture, Progress& progress)
{
  unsigned char* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();

  gaussianBlurSelectiveRGBA8(buffer, u, v, u2, radius, threshold, wrapping, affect_alpha, progress);
}

void OperationSelectiveGaussianBlur::doit(TextureRGBA32F& texture, Progress& progress)
{
  float* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();

  gaussianBlurSelectiveRGBA32F(buffer, u, v, u2, radius, threshold, wrapping, affect_alpha, progress);
}

void OperationSelectiveGaussianBlur::doit(TextureGrey8& texture, Progress& progress)
{
  unsigned char* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();

  gaussianBlurSelectiveGrey8(buffer, u, v, u2, radius, threshold, wrapping, progress);
}

void OperationSelectiveGaussianBlur::doit(TextureGrey32F& texture, Progress& progress)
{
  float* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();

  gaussianBlurSelectiveGrey32F(buffer, u, v, u2, radius, threshold, wrapping, progress);
}

////////////////////////////////////////////////////////////////////////////////

OperationUnsharpMask::OperationUnsharpMask(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom)
: AOperationFilterUtil(settings, true, true)
, radius(8.0)
, power(1.0)
, threshold(0)
, wrapping(false)
, affect_alpha(true)
, dialog(this)
{
  params.addDouble("Radius", &radius, 4.0, 50.0, 1.0);
  params.addDouble("Power", &power, 0.1, 10.0, 0.1);
  params.addInt("Threshold", &threshold, 0, 255, 1);
  params.addBool("Wrapping", &wrapping);
  params.addBool("Affect Alpha", &affect_alpha);

  paramsToDialog(dialog.page, params, geom);
}

void OperationUnsharpMask::doit(TextureRGBA8& texture, Progress& progress)
{
  unsigned char* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();

  std::vector<unsigned char> old;
  getAlignedBuffer(old, &texture);

  gaussianBlurRGBA8(&old[0], u, v, u, radius, wrapping, affect_alpha, progress);

  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  for(size_t c = 0; c < 4; c++)
  {
    if(c == 3 && !affect_alpha) continue;

    size_t index = y * u * 4 + x * 4 + c;
    size_t index2 = y * u2 * 4 + x * 4 + c;

    int a = old[index];
    int b = buffer[index2];

    double result = b + power * (b - a);
    if(result > 255) result = 255;
    if(result < 0) result = 0;

    if(std::abs(b - a) >= threshold)
    {
      buffer[index2] = (unsigned char)(result /*-threshold*/);
    }
  }
}

////////////////////////////////////////////////////////////////////////////////

OperationHighPass::OperationHighPass(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom)
: AOperationFilterUtil(settings, true, true)
, radius(2.0)
, wrapping(false)
, affect_alpha(false)
, dialog(this)
{
  params.addDouble("Radius", &radius, 2.0, 50.0, 0.1);
  params.addBool("Wrapping", &wrapping);
  params.addBool("Affect Alpha", &affect_alpha);

  paramsToDialog(dialog.page, params, geom);
}

void OperationHighPass::doit(TextureRGBA8& texture, Progress& progress)
{
  unsigned char* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();

  std::vector<unsigned char> old;
  getAlignedBuffer(old, &texture);

  gaussianBlurRGBA8(&old[0], u, v, u, radius, wrapping, affect_alpha, progress);

  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  for(size_t c = 0; c < 4; c++)
  {
    if(c == 3 && !affect_alpha) continue;

    size_t index = y * u * 4 + x * 4 + c;
    size_t index2 = y * u2 * 4 + x * 4 + c;

    int a = old[index];
    int b = buffer[index2];

    int result = 128 + b - a;
    if(result > 255) result = 255;
    if(result < 0) result = 0;

      buffer[index2] = result;
  }
}

////////////////////////////////////////////////////////////////////////////////

void rotate3x3matrix45degrees(double* matrix)
{
  double temp = matrix[0];
  matrix[0] = matrix[1];
  matrix[1] = matrix[2];
  matrix[2] = matrix[5];
  matrix[5] = matrix[8];
  matrix[8] = matrix[7];
  matrix[7] = matrix[6];
  matrix[6] = matrix[3];
  matrix[3] = temp;
}

OperationEmboss::OperationEmboss(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom)
: AOperationFilterUtilWithSettings(settings, true, true)
, wrapping(false)
, affect_alpha(false)
, matrixType(MATRIX1)
, matrixRot(R315)
, dialog(this)
{
  (void)geom;
  std::vector<Matrix> enums; enums.push_back(MATRIX1); enums.push_back(MATRIX2); enums.push_back(MATRIX3);
  std::vector<std::string> names; names.push_back("Matrix 1"); names.push_back("Matrix 2"); names.push_back("Matrix 3");
  params.addEnum("Matrix", &matrixType, names, enums);
  std::vector<MatrixRot> enums2; enums2.push_back(R0);enums2.push_back(R45);enums2.push_back(R90);enums2.push_back(R135);enums2.push_back(R180);enums2.push_back(R225);enums2.push_back(R270);enums2.push_back(R315);
  std::vector<std::string> names2; names2.push_back("0");names2.push_back("45");names2.push_back("90");names2.push_back("135");names2.push_back("180");names2.push_back("225");names2.push_back("270");names2.push_back("315");
  params.addEnum("Matrix Rotation", &matrixRot, names2, enums2);
  params.addBool("Wrapping", &wrapping);
  params.addBool("Affect Alpha", &affect_alpha);

  paramsToDialog(dialog.page, params, geom);
}

void OperationEmboss::doit(TextureRGBA8& texture, const GlobalToolSettings& settings, Progress&)
{
  (void)settings;

  static const double matrix1[9] =
  {
    0, 0, 0,
    0, 1,-1,
    0, 0, 0
  };
  static const double matrix2[9] =
  {
    0, 0, 0,
    1, 0,-1,
    0, 0, 0
  };
  static const double matrix3[9] =
  {
    0, 0, 0,
    2,-1,-1,
    0, 0, 0
  };

  const double* m = (matrixType == MATRIX1) ? matrix1 : ((matrixType == MATRIX2) ? matrix2 : matrix3);
  double matrix[9] = { m[0], m[1], m[2], m[3], m[4], m[5], m[6], m[7], m[8] };
  int numrot = (int)matrixRot;
  for(int i = 0; i < numrot; i++) rotate3x3matrix45degrees(matrix);

  convolute3x3(texture, matrix, wrapping, affect_alpha, 128.0);
}

////////////////////////////////////////////////////////////////////////////////

OperationEdgeDetect::OperationEdgeDetect(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom)
: AOperationFilterUtilWithSettings(settings, true, true)
, wrapping(false)
, affect_alpha(false)
, matrixType(SOBEL)
, matrixRot(R315)
, dialog(this)
{
  (void)geom;
  std::vector<Matrix> enums; enums.push_back(SOBEL); enums.push_back(PREWITT); enums.push_back(PREWITT2); enums.push_back(SCHARR); enums.push_back(ROBERT);
  std::vector<std::string> names; names.push_back("Sobel"); names.push_back("Prewitt"); names.push_back("Prewitt 2"); names.push_back("Scharr"); names.push_back("Robert's Cross");
  params.addEnum("Matrix", &matrixType, names, enums);
  std::vector<MatrixRot> enums2; enums2.push_back(R0);enums2.push_back(R45);enums2.push_back(R90);enums2.push_back(R135);enums2.push_back(R180);enums2.push_back(R225);enums2.push_back(R270);enums2.push_back(R315);
  std::vector<std::string> names2; names2.push_back("0");names2.push_back("45");names2.push_back("90");names2.push_back("135");names2.push_back("180");names2.push_back("225");names2.push_back("270");names2.push_back("315");
  params.addEnum("Matrix Rotation", &matrixRot, names2, enums2);
  params.addBool("Wrapping", &wrapping);
  params.addBool("Affect Alpha", &affect_alpha);

  paramsToDialog(dialog.page, params, geom);
}

void OperationEdgeDetect::doit(TextureRGBA8& texture, const GlobalToolSettings& settings, Progress&)
{
  (void)settings;

  if(matrixType == ROBERT)
  {
    unsigned char* buffer = texture.getBuffer();
    size_t u = texture.getU();
    size_t v = texture.getV();
    size_t u2 = texture.getU2();

    std::vector<unsigned char> temp(u * v * 4);

    for(size_t y = 0; y < v; y++)
    for(size_t x = 0; x < u; x++)
    for(size_t c = 0; c < 4; c++)
    {
      if(c == 3 && !affect_alpha) continue;
      double value = 0.0;

      int y2 = y + 1;
      if(y2 < 0) y2 = wrapping ? v - 1 : 0;
      else if(y2 >= (int)v) y2 = wrapping ? 0 : v - 1;
      int x2 = x + 1;
      if(x2 < 0) x2 = wrapping ? u - 1 : 0;
      else if(x2 >= (int)u) x2 = wrapping ? 0 : u - 1;

      value += std::abs(buffer[u2 * y * 4 + x * 4 + c] - buffer[u2 * y2 * 4 + x2 * 4 + c]);
      value += std::abs(buffer[u2 * y * 4 + x2 * 4 + c] - buffer[u2 * y2 * 4 + x * 4 + c]);

      if(value < 0) value = 0;
      if(value > 255) value = 255;
      temp[y * u * 4 + x * 4 + c] = (unsigned char)(value);
    } //for x, y, c

    for(size_t y = 0; y < v; y++)
    for(size_t x = 0; x < u; x++)
    for(size_t c = 0; c < 4; c++)
    {
      if(c == 3 && !affect_alpha) continue;
      size_t index = y * u * 4 + x * 4 + c;
      size_t index2 = y * u2 * 4 + x * 4 + c;
      buffer[index2] = temp[index];
    }
  }
  else
  {
    //Sobel
    static const double matrix1[9] =
    {
      1, 0,-1,
      2, 0,-2,
      1, 0,-1
    };
    //Prewitt
    static const double matrix2[9] =
    {
      1, 0,-1,
      1, 0,-1,
      1, 0,-1
    };
    //Another Prewitt
    static const double matrix3[9] =
    {
      1, 1,-1,
      1,-2,-1,
      1, 1,-1
    };
    //Scharr
    static const double matrix4[9] =
    {
      3, 0,-3,
     10, 0,-10,
      3, 0,-3
    };

    const double* m = (matrixType == SOBEL) ? matrix1 : ((matrixType == PREWITT) ? matrix2 : ((matrixType == PREWITT2) ? matrix3 : matrix4));
    double matrix[9] = { m[0], m[1], m[2], m[3], m[4], m[5], m[6], m[7], m[8] };
    int numrot = (int)matrixRot;
    for(int i = 0; i < numrot; i++) rotate3x3matrix45degrees(matrix);

    convolute3x3(texture, matrix, wrapping, affect_alpha, 0);
  }
}

////////////////////////////////////////////////////////////////////////////////

OperationPixelEdgeDetect::OperationPixelEdgeDetect(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom)
: AOperationFilterUtilWithSettings(settings, true, true)
{
  (void)geom;
}

void OperationPixelEdgeDetect::doit(TextureRGBA8& texture, const GlobalToolSettings&, Progress& progress)
{
  (void)progress;
  unsigned char* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();

  std::vector<unsigned char> buffer2(u2 * v * 4);
  for(size_t i = 0; i < buffer2.size(); i++) buffer2[i] = buffer[i];

  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  {
    size_t i = y * u2 * 4 + x * 4;
    int color = buffer2[i] + 256 * buffer2[i + 1] + 256 * 256 * buffer2[i + 2] + 256 * 256 * 256 * buffer2[i + 3];
    bool edge = false;
    for(int a = 0; a < 8; a++)
    {
      size_t x2 = (a == 0 || a == 3 || a == 5) ? x - 1 : (a == 1 || a == 6 ? x : x + 1);
      size_t y2 = (a == 0 || a == 1 || a == 2) ? y - 1 : (a == 3 || a == 4 ? y : y + 1);
      if(x2 >= u || y2 >= v) continue;
      size_t i2 = y2 * u2 * 4 + x2 * 4;
      int color2 = buffer2[i2] + 256 * buffer2[i2 + 1] + 256 * 256 * buffer2[i2 + 2] + 256 * 256 * 256 * buffer2[i2 + 3];
      if(color > color2) edge = true;
    }

    buffer[i + 3] = 255;
    if(edge) buffer[i] = buffer[i + 1] = buffer[i + 2] = 255;
    else buffer[i] = buffer[i + 1] = buffer[i + 2] = 0;
  }
}

////////////////////////////////////////////////////////////////////////////////

OperationCannyEdgeDetect::OperationCannyEdgeDetect(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom)
: AOperationFilterUtilWithSettings(settings, true, true)
, affect_alpha(false)
, steps(5)
, radius(3)
, lowthreshold(2)
, highthreshold(75)
, dialog(this)
{
  (void)geom;
  params.addBool("Affect Alpha", &affect_alpha);
  params.addDouble("Radius", &radius, 0.0, 20.0, 0.01);
  params.addInt("Low Threshold", &lowthreshold, 1, 255, 1);
  params.addInt("High Threshold", &highthreshold, 1, 255, 1);
  params.addInt("Algorithm Steps", &steps, 0, 5, 1);

  paramsToDialog(dialog.page, params, geom);
}

void OperationCannyEdgeDetect::doit(TextureRGBA8& texture, const GlobalToolSettings&, Progress& progress)
{
  unsigned char* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();

  cannyEdgeDetectRGBA8(buffer, u, v, u2, affect_alpha, progress, radius, lowthreshold, highthreshold, steps);
}

////////////////////////////////////////////////////////////////////////////////

OperationScanLines::OperationScanLines(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom)
: AOperationFilterUtil(settings, true, true)
, colored(false)
, opacity(0.25)
, affect_alpha(false)
, dialog(this)
{
  params.addDouble("Opacity", &opacity, 0.0, 1.0, 0.01);
  params.addBool("Colored", &colored);
  params.addBool("Affect Alpha", &affect_alpha);
  params.addColor("FG Color", &settings.leftColor);

  paramsToDialog(dialog.page, params, geom);
}

void OperationScanLines::doit(TextureRGBA8& texture, Progress&)
{
  unsigned char* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();

  lpi::ColorRGB color = colored ? settings.leftColor255() : lpi::RGB_Black;

  for(size_t y = 1; y < v; y+=2)
  for(size_t x = 0; x < u; x++)
  for(size_t c = 0; c < 4; c++)
  {
    if(affect_alpha || c != 3)
    {
      size_t index = y * u2 * 4 + x * 4 + c;

      buffer[index] = (int)(((&color.r)[c] * opacity) + (1.0 - opacity) * buffer[index]);
    }
  }
}

////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////

OperationMosaic::OperationMosaic(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom)
: AOperationFilterUtil(settings, true, true)
, dialog(this)
, tilesize(8)
, antialiasing(true)
{
  params.addInt("Tile Size", &tilesize, 2, 32, 1);
  params.addBool("Antialias", &antialiasing);

  paramsToDialog(dialog.page, params, geom);
}

void OperationMosaic::doit(TextureRGBA8& texture, Progress&)
{
  unsigned char* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();

  mosaicRGBA8(buffer, u, v, u2, tilesize, tilesize, antialiasing);
}

////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////

static std::vector<double> arrayToVector(double* array, size_t size)
{
  std::vector<double> result(size);
  for(size_t i = 0; i < size; i++) result[i] = array[i];
  return result;
}

static void populateUserDefinedDefaultMatrices(Numbers& numbers)
{
  std::vector<double> base(49, 0);

  std::vector<double> center = base;
  center[24] = 1;
  numbers.addPreset("Center", center);

  std::vector<double> soften = base;
  soften[17] = soften[23] = soften[25] = soften[31] = 1.0 / 8.0;
  soften[24] = 4.0 / 8.0;
  numbers.addPreset("Soften", soften);

  std::vector<double> blur = base;
  blur[16] = blur[18] = blur[30] = blur[32] = 1.0 / 16.0;
  blur[17] = blur[23] = blur[25] = blur[31] = 2.0 / 16.0;
  blur[24] = 4.0 / 16.0;
  numbers.addPreset("Blur", blur);

  double gaussian7[49] = {
      0.00000067, 0.00002292, 0.00019117, 0.00038771, 0.00019117, 0.00002292, 0.00000067,
      0.00002292, 0.00078633, 0.00655965, 0.01330373, 0.00655965, 0.00078633, 0.00002292,
      0.00019117, 0.00655965, 0.05472157, 0.11098164, 0.05472157, 0.00655965, 0.00019117,
      0.00038771, 0.01330373, 0.11098164, 0.22508352, 0.11098164, 0.01330373, 0.00038771,
      0.00019117, 0.00655965, 0.05472157, 0.11098164, 0.05472157, 0.00655965, 0.00019117,
      0.00002292, 0.00078633, 0.00655965, 0.01330373, 0.00655965, 0.00078633, 0.00002292,
      0.00000067, 0.00002292, 0.00019117, 0.00038771, 0.00019117, 0.00002292, 0.00000067};
  numbers.addPreset("Blur 7x7", arrayToVector(gaussian7, 49));

  std::vector<double> edge = base;
  edge[16] = edge[17] = edge[18] = edge[23] = edge[25] = edge[30] = edge[31] = edge[32] = -1.0;
  edge[24] = 8.0;
  numbers.addPreset("Find Edges", edge);
}

UserDefinedFilterDialog::UserDefinedFilterDialog(OperationUserDefinedFilter* udf, const lpi::gui::IGUIDrawer& geom)
: FilterDialogWithPreview(udf)
, udf(udf)
, numbers(geom, 7, 7)
{
  addSubElement(&numbers, lpi::gui::Sticky(0.0,16, 0.0,0, 1.0,-16, 0.66,-MAXPREVIEWV));
  addSubElement(&page, lpi::gui::Sticky(0.0,0, 0.66,-MAXPREVIEWV, 1.0,0, 1.0,-MAXPREVIEWV));

  page.addControl("Wrapping", new lpi::gui::DynamicCheckbox(&udf->wrapping));
  page.addControl("Affect Alpha", new lpi::gui::DynamicCheckbox(&udf->affect_alpha));
  page.addControl("Mul", new lpi::gui::DynamicSliderSpinner<double>(&udf->mul, -256.0, 256.0, 1.0, geom));
  page.addControl("Div", new lpi::gui::DynamicSliderSpinner<double>(&udf->div, -256.0, 256.0, 1.0, geom));
  page.addControl("Add", new lpi::gui::DynamicSliderSpinner<double>(&udf->add, -256.0, 256.0, 1.0, geom));

  populateUserDefinedDefaultMatrices(numbers);
}

void UserDefinedFilterDialog::drawImpl(lpi::gui::IGUIDrawer& drawer) const
{
  numbers.draw(drawer);
  page.draw(drawer);
  FilterDialogWithPreview::drawImpl(drawer);
}

void UserDefinedFilterDialog::handleImpl(const lpi::IInput& input)
{
  numbers.handle(input);
  page.handle(input);
  FilterDialogWithPreview::handleImpl(input);
  numbers.controlToValue(udf->matrix);
  if(numbers.hasChanged()) updatePreview();
}

////////////////////////////////////////////////////////////////////////////////

OperationUserDefinedFilter::OperationUserDefinedFilter(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom)
: AOperationFilterUtil(settings, true, true)
, wrapping(false)
, affect_alpha(false)
, mul(1)
, div(1)
, add(0)
, dialog(this, geom)
{
  for(size_t i = 0; i < 49; i++) matrix[i] = 0;
  matrix[24] = 1;
  dialog.numbers.valueToControl(matrix);
}

void OperationUserDefinedFilter::doit(TextureRGBA8& texture, Progress&)
{
  convolute7x7(texture, matrix, wrapping, affect_alpha, add, mul / div);
}

void OperationUserDefinedFilter::doit(TextureRGBA32F& texture, Progress&)
{
  convolute7x7(texture, matrix, wrapping, affect_alpha, add, mul / div);
}

////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////

OperationPixelShadow::OperationPixelShadow(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom)
: AOperationFilterUtil(settings, true, true)
, dialog(this)
, xshift(1)
, yshift(1)
, not_from_shadow_color(true)
, mode(M_SHADOW)
{
  std::vector<Mode> enums; enums.push_back(M_SHADOW); enums.push_back(M_REMOVE);
  std::vector<std::string> names; names.push_back("Add Shadow"); names.push_back("Remove Shadow");
  params.addEnum("Mode", &mode, names, enums);

  params.addColor("Shadow Color", &settings.leftColor);
  params.addColor("BG Color", &settings.rightColor);

  params.addInt("X Shift", &xshift, -4, 4, 1);
  params.addInt("Y Shift", &yshift, -4, 4, 1);
  params.addBool("Not From Shadow Color", &not_from_shadow_color);

  paramsToDialog(dialog.page, params, geom);
}

void OperationPixelShadow::doit(TextureRGBA8& texture, Progress&)
{
  unsigned char* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();

  std::vector<unsigned char> old;
  if(mode == M_SHADOW) getAlignedBuffer(old, &texture);

  for(size_t y = 1; y < v; y++)
  for(size_t x = 0; x < u; x++)
  {
    if(mode == M_SHADOW)
    {
      int x2 = x - xshift;
      int y2 = y - yshift;
      if(x2 < 0) continue;
      if(y2 < 0) continue;
      if(x2 >= (int)u) continue;
      if(y2 >= (int)v) continue;

      size_t index = y * u2 * 4 + x * 4;
      size_t index2 = y2 * u * 4 + x2 * 4;

      bool oldnotbg = old[index2 + 0] != settings.rightColor255().r
                || old[index2 + 1] != settings.rightColor255().g
                || old[index2 + 2] != settings.rightColor255().b
                || old[index2 + 3] != settings.rightColor255().a;

      bool oldfg = old[index2 + 0] == settings.leftColor255().r
             && old[index2 + 1] == settings.leftColor255().g
             && old[index2 + 2] == settings.leftColor255().b
             && old[index2 + 3] == settings.leftColor255().a;

      bool thisbg = buffer[index + 0] == settings.rightColor255().r
             && buffer[index + 1] == settings.rightColor255().g
             && buffer[index + 2] == settings.rightColor255().b
             && buffer[index + 3] == settings.rightColor255().a;

      if(oldnotbg && thisbg && !(not_from_shadow_color && oldfg))
      {
        buffer[index + 0] = settings.leftColor255().r;
        buffer[index + 1] = settings.leftColor255().g;
        buffer[index + 2] = settings.leftColor255().b;
        buffer[index + 3] = settings.leftColor255().a;
      }
    }
    else if(mode == M_REMOVE)
    {
      size_t index = y * u2 * 4 + x * 4;

      if(buffer[index + 0] == settings.leftColor255().r
      && buffer[index + 1] == settings.leftColor255().g
      && buffer[index + 2] == settings.leftColor255().b
      && buffer[index + 3] == settings.leftColor255().a)
      {
        buffer[index + 0] = settings.rightColor255().r;
        buffer[index + 1] = settings.rightColor255().g;
        buffer[index + 2] = settings.rightColor255().b;
        buffer[index + 3] = settings.rightColor255().a;
      }
    }
  }
}

////////////////////////////////////////////////////////////////////////////////

OperationPixelBorder::OperationPixelBorder(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom)
: AOperationFilterUtil(settings, true, true)
, dialog(this)
, mode(M_ADD)
{
  std::vector<Mode> enums; enums.push_back(M_ADD); enums.push_back(M_REMOVE);
  std::vector<std::string> names; names.push_back("Add Border"); names.push_back("Remove Border");
  params.addEnum("Mode", &mode, names, enums);

  params.addColor("Border Color", &settings.leftColor);
  params.addColor("BG Color", &settings.rightColor);

  paramsToDialog(dialog.page, params, geom);
}


void OperationPixelBorder::doit(TextureRGBA8& texture, Progress&)
{
  unsigned char* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();

  std::vector<unsigned char> old;
  if(mode == M_ADD) getAlignedBuffer(old, &texture);

  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  {
    if(mode == M_ADD)
    {
      size_t oindex = y * u * 4 + x * 4;

      bool oldbg = old[oindex + 0] == settings.rightColor255().r
                && old[oindex + 1] == settings.rightColor255().g
                && old[oindex + 2] == settings.rightColor255().b
                && old[oindex + 3] == settings.rightColor255().a;

      if(!oldbg) continue;

      bool diff = false;

      for(int y3 = -1; y3 < 2; y3++)
      for(int x3 = -1; x3 < 2; x3++)
      {
        int x2 = x + x3;
        int y2 = y + y3;
        if(x2 < 0) continue;
        if(y2 < 0) continue;
        if(x2 >= (int)u) continue;
        if(y2 >= (int)v) continue;

        size_t index2 = y2 * u * 4 + x2 * 4;

        bool oldnotbg = old[index2 + 0] != settings.rightColor255().r
                     || old[index2 + 1] != settings.rightColor255().g
                     || old[index2 + 2] != settings.rightColor255().b
                     || old[index2 + 3] != settings.rightColor255().a;

        bool oldfg = old[index2 + 0] == settings.leftColor255().r
                  && old[index2 + 1] == settings.leftColor255().g
                  && old[index2 + 2] == settings.leftColor255().b
                  && old[index2 + 3] == settings.leftColor255().a;

        if(oldnotbg && !oldfg)
        {
          diff = true;
          break;
        }
      }

      size_t index = y * u2 * 4 + x * 4;

      if(diff)
      {
        buffer[index + 0] = settings.leftColor255().r;
        buffer[index + 1] = settings.leftColor255().g;
        buffer[index + 2] = settings.leftColor255().b;
        buffer[index + 3] = settings.leftColor255().a;
      }
    }
    else if(mode == M_REMOVE)
    {
      size_t index = y * u2 * 4 + x * 4;

      if(buffer[index + 0] == settings.leftColor255().r
      && buffer[index + 1] == settings.leftColor255().g
      && buffer[index + 2] == settings.leftColor255().b
      && buffer[index + 3] == settings.leftColor255().a)
      {
        buffer[index + 0] = settings.rightColor255().r;
        buffer[index + 1] = settings.rightColor255().g;
        buffer[index + 2] = settings.rightColor255().b;
        buffer[index + 3] = settings.rightColor255().a;
      }
    }
  }
}

////////////////////////////////////////////////////////////////////////////////

////is a smaller than b?
//static bool colorSmaller(const lpi::ColorRGB& a, const lpi::ColorRGB& b)
//{
  //int ab = a.r + a.g + a.b;
  //int bb = b.r + b.g + b.b;
  //if(ab < bb) return true;
  //if(bb < ab) return false;
  //if(a.a > b.a) return true; //I consider alpha channel to be a brighter background than the image.
  //if(b.a > a.a) return false;
  //if(a.b < b.b) return true;
  //if(b.b < a.b) return false;
  //if(a.g < b.g) return true;
  //if(b.g < a.g) return false;
  //if(a.r < b.r) return true;
  //if(b.r < a.r) return false;
  //return false; //they're equal
//}

//void OperationPixelAntialias::doit(TextureRGBA8& texture, Progress&)
//{
  //unsigned char* buffer = texture.getBuffer();
  //size_t u = texture.getU();
  //size_t v = texture.getV();
  //size_t u2 = texture.getU2();

  //std::vector<unsigned char> old;
  //getAlignedBuffer(old, &texture);

  //lpi::ColorRGB c[9];

  //for(int y = 0; y < (int)v; y++)
  //for(int x = 0; x < (int)u; x++)
  //{
    //if(x == 0 || y == 0 || x == (int)u - 1 || y == (int)v - 1) continue;

    //for(int yi = 0; yi < 3; yi++)
    //for(int xi = 0; xi < 3; xi++)
    //{
      //size_t i = (y + yi - 1) * u * 4 + (x + xi - 1) * 4;
      //c[yi * 3 + xi] = lpi::ColorRGB(old[i+0], old[i+1], old[i+2], old[i+3]);
    //}

    //size_t index = y * u2 * 4 + x * 4;

    //bool aa = false; //do aa
    //lpi::ColorRGB aaColor; //still has to be mixed with our pixel

    ///*
    //012
    //345
    //678

    //???
    //??X
    //OX?
    //*/

    //if(c[5] == c[7] && (c[2] != c[5] || c[6] != c[5]) && c[5] != c[4] && (darkest == colorSmaller(c[5], c[4])))
    //{
      //aa = true;
      //aaColor = c[5];
    //}
    //if(c[1] == c[3] && (c[2] != c[1] || c[6] != c[1]) && c[1] != c[4] && (darkest == colorSmaller(c[1], c[4])))
    //{
      //aa = true;
      //aaColor = c[1];
    //}
    //if(c[1] == c[5] && (c[0] != c[1] || c[8] != c[1]) && c[1] != c[4] && (darkest == colorSmaller(c[1], c[4])))
    //{
      //aa = true;
      //aaColor = c[1];
    //}
    //if(c[3] == c[7] && (c[0] != c[3] || c[8] != c[3]) && c[3] != c[4] && (darkest == colorSmaller(c[3], c[4])))
    //{
      //aa = true;
      //aaColor = c[3];
    //}
    //if(aa)
    //{
      //lpi::ColorRGB n = (aaColor & amount/2) | (c[4] & (1.0 - amount/2));
      //buffer[index + 0] = n.r;
      //buffer[index + 1] = n.g;
      //buffer[index + 2] = n.b;
      //buffer[index + 3] = n.a;
    //}
  //}
//}

static bool eq(int a, int b)
{
  return std::abs(a - b) < 10;
}

OperationPixelAntialias::OperationPixelAntialias(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom)
: AOperationFilterUtil(settings, true, true)
//, dialog(this)
//, amount(0.5)
//, darkest(true)
{
  (void)geom;
  //params.addDouble("Amount", &amount, 0.0, 1.0, 0.01);
  //params.addNamedBool("Expand", &darkest, "Brightest", "Darkest");

  //paramsToDialog(dialog.page, params, geom);
}


void OperationPixelAntialias::doit(TextureRGBA8& texture, Progress& progress)
{
  unsigned char* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();

  std::vector<unsigned char> old;
  getAlignedBuffer(old, &texture);

  int a[9];
  ///int aAlpha[9]; //alpha also remembered to avoid leak through (sorry for the stupid variable names)

  for(int y = 0; y < (int)v; y++)
  {
    if(progress.handle(y, v)) return;
    for(int x = 0; x < (int)u; x++)
    for(int c = 0; c < 4; c++)
    {
      if(x == 0 || y == 0 || x == (int)u - 1 || y == (int)v - 1) continue;

      for(int yi = 0; yi < 3; yi++)
      for(int xi = 0; xi < 3; xi++)
      {
        size_t i = (y + yi - 1) * u * 4 + (x + xi - 1) * 4 + c;
        a[yi * 3 + xi] = old[i];
        ///aAlpha[yi * 3 + xi] = old[i - c + 3];
      }



      bool aa = false; //do aa
      int aaColor = 0; //still has to be mixed with our pixel
      int aaAlpha = 255;
      int num = 1;
      int dirx = 0, diry = 0; //direction to move for multi-pixel antialiasing
      int aax = 0, aay = 0; //relative position of color to mix with

      /*
      012
      345
      678

      ???
      ??X
      OX?
      */

      bool jag57 = eq(a[5], a[7]) && (a[2] != a[5] || a[6] != a[5]) && a[5] != a[4];
      bool jag13 = eq(a[1], a[3]) && (a[2] != a[1] || a[6] != a[1]) && a[1] != a[4];
      bool jag15 = eq(a[1], a[5]) && (a[0] != a[1] || a[8] != a[1]) && a[1] != a[4];
      bool jag37 = eq(a[3], a[7]) && (a[0] != a[3] || a[8] != a[3]) && a[3] != a[4];

      if(jag57)
      {
        aa = true;

        if(eq(a[2], a[5]))
        {
          dirx = 0, diry = -1;
          aax = 1, aay = 0;
          for(int yb = y - 1; yb >= 0; yb--)
          {
            if(eq(old[4*u*yb+4*x+c], a[4]) && eq(old[4*u*yb+4*(x+1)+c], a[5])) num++;
            else break;
          }
        }
        else
        {
          dirx = -1, diry = 0;
          aax = 0, aay = 1;
          for(int xb = x - 1; xb >= 0; xb--)
          {
            if(eq(old[4*u*y+4*xb+c], a[4]) && eq(old[4*u*(y+1)+4*xb+c], a[7])) num++;
            else break;
          }
        }
      }
      else if(jag13)
      {
        aa = true;

        if(eq(a[6], a[3]))
        {
          dirx = 0, diry = 1;
          aax = -1, aay = 0;
          for(int yb = y + 1; yb < (int)v; yb++)
          {
            if(eq(old[4*u*yb+4*x+c], a[4]) && eq(old[4*u*yb+4*(x-1)+c], a[3])) num++;
            else break;
          }
        }
        else
        {
          dirx = 1, diry = 0;
          aax = 0, aay = -1;
          for(int xb = x + 1; xb < (int)u; xb++)
          {
            if(eq(old[4*u*y+4*xb+c], a[4]) && eq(old[4*u*(y-1)+4*xb+c], a[1])) num++;
            else break;
          }
        }
      }
      else if(jag15)
      {
        aa = true;

        if(eq(a[8], a[5]))
        {
          dirx = 0, diry = 1;
          aax = 1, aay = 0;
          for(int yb = y + 1; yb < (int)v; yb++)
          {
            if(eq(old[4*u*yb+4*x+c], a[4]) && eq(old[4*u*yb+4*(x+1)+c], a[5])) num++;
            else break;
          }
        }
        else
        {
          dirx = -1, diry = 0;
          aax = 0, aay = -1;
          for(int xb = x - 1; xb >= 0; xb--)
          {
            if(eq(old[4*u*y+4*xb+c], a[4]) && eq(old[4*u*(y-1)+4*xb+c], a[1])) num++;
            else break;
          }
        }
      }
      else if(jag37)
      {
        aa = true;

        if(eq(a[0], a[3]))
        {
          dirx = 0, diry = -1;
          aax = -1, aay = 0;
          for(int yb = y - 1; yb >= 0; yb--)
          {
            if(eq(old[4*u*yb+4*x+c], a[4]) && eq(old[4*u*yb+4*(x-1)+c], a[3])) num++;
            else break;
          }
        }
        else
        {
          dirx = 1, diry = 0;
          aax = 0, aay = 1;
          for(int xb = x + 1; xb < (int)u; xb++)
          {
            if(eq(old[4*u*y+4*xb+c], a[4]) && eq(old[4*u*(y+1)+4*xb+c], a[7])) num++;
            else break;
          }
        }
      }

      bool thin = (jag37 && jag15) || (jag57 && jag13);

      if(aa)
      {
        /*int n = (aaColor + a[4] * 3) / 4;
        //int n = (aaColor + a[4] * 2) / 3;
        //int n = (aaColor * 2 + a[4] * 5) / 7;

        //avoid leaking through of color of 100% transparent parts
        if(alpha != 255 && c != 3) n = (n * alpha + aaColor * (255 - alpha)) / 255;
        if(aaAlpha != 255 && c != 3) n = (n * aaAlpha + a[4] * (255 - aaAlpha)) / 255;
        buffer[index2] = n;*/

        int n = (num + 1) / 2;

        for(int i = 0; i < n; i++)
        {
          size_t index2 = (y + i * diry) * u2 * 4 + (x + i * dirx) * 4 + c;
          size_t indexo = (y + i * diry) * u * 4 + (x + i * dirx) * 4;
          size_t indexa = (y + i * diry + aay ) * u * 4 + (x + i * dirx + aax) * 4;
          int oColor = old[indexo + c];
          int oAlpha = old[indexo + 3];
          aaColor = old[indexa + c];
          aaAlpha = old[indexa + 3];

          double amount = 0;
          if(num % 2 == 0) amount = (0.5 * (double)(n - i) / (double)(n) + 0.5 * (double)(n - i - 1) / (double)(n)) / 2;
          else
          {
            /*
            Pixel with part of 45 degree line going through it
            **********
            *        *
            *        *
        0.5 *.       *
            *xx.     *
            *xxxx.   *
            **********
                0.5
            */
            if(i == n - 1) amount = (0.5 / num) / 4; //e.g. if num is 1, this gives 1/8. See ascii art above to see that 1/8th of the area is filled.
            else amount = ((1.0 / num) + ((double)(n - i - 1)) / num + ((double)(n - i - 2)) / num) / 2;
          }
          if(thin) amount *= 2;

          int n = (int)(aaColor * amount + oColor * (1.0 - amount));
          if(oAlpha != 255 && c != 3) n = (n * oAlpha + aaColor * (255 - oAlpha)) / 255;
          if(aaAlpha != 255 && c != 3) n = (n * aaAlpha + oColor * (255 - aaAlpha)) / 255;
          buffer[index2] = n;
        }
      }
    }
  }
}

////////////////////////////////////////////////////////////////////////////////
static int mirrorbits(int i)
{
  int o = 0;
  if(i & 1) o |= 128;
  if(i & 2) o |= 64;
  if(i & 4) o |= 32;
  if(i & 8) o |= 16;
  if(i & 16) o |= 8;
  if(i & 32) o |= 4;
  if(i & 64) o |= 2;
  if(i & 128) o |= 1;
  return o;
}

void OperationMirrorBits::doit(TextureRGBA8& texture, Progress&)
{
  unsigned char* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();
  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  for(size_t c = 0; c < 4; c++)
  {
    size_t index = y * u2 * 4 + x * 4 + c;
    buffer[index] = mirrorbits(buffer[index]);
  }
}

////////////////////////////////////////////////////////////////////////////////

static int shiftbits4(int i)
{
  int o = 0;
  if(i & 1) o |= 16;
  if(i & 2) o |= 32;
  if(i & 4) o |= 64;
  if(i & 8) o |= 128;
  if(i & 16) o |= 1;
  if(i & 32) o |= 2;
  if(i & 64) o |= 4;
  if(i & 128) o |= 8;
  return o;
}

void OperationShiftBits4::doit(TextureRGBA8& texture, Progress&)
{
  unsigned char* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();
  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  for(size_t c = 0; c < 4; c++)
  {
    size_t index = y * u2 * 4 + x * 4 + c;
    buffer[index] = shiftbits4(buffer[index]);
  }
}

////////////////////////////////////////////////////////////////////////////////

static int shiftbits1(int i)
{
  int o = 0;
  if(i & 1) o |= 2;
  if(i & 2) o |= 4;
  if(i & 4) o |= 8;
  if(i & 8) o |= 16;
  if(i & 16) o |= 32;
  if(i & 32) o |= 64;
  if(i & 64) o |= 128;
  if(i & 128) o |= 1;
  return o;
}

void OperationShiftBits1::doit(TextureRGBA8& texture, Progress&)
{
  unsigned char* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();
  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  for(size_t c = 0; c < 4; c++)
  {
    size_t index = y * u2 * 4 + x * 4 + c;
    buffer[index] = shiftbits1(buffer[index]);
  }
}

////////////////////////////////////////////////////////////////////////////////

unsigned char binaryToGrayCode(unsigned char c)
{
  return (c >> 1) ^ c;
}

void OperationBitsToGrayCode::doit(TextureRGBA8& texture, Progress&)
{
  unsigned char* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();
  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  for(size_t c = 0; c < 4; c++)
  {
    size_t index = y * u2 * 4 + x * 4 + c;
    buffer[index] = binaryToGrayCode(buffer[index]);
  }
}

////////////////////////////////////////////////////////////////////////////////

unsigned char grayCodeToBinary(unsigned char c)
{
  c ^= c >> 1;
  c ^= c >> 2;
  c ^= c >> 4;
  return c;
}

void OperationBitsFromGrayCode::doit(TextureRGBA8& texture, Progress&)
{
  unsigned char* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();
  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  for(size_t c = 0; c < 4; c++)
  {
    size_t index = y * u2 * 4 + x * 4 + c;
    buffer[index] = grayCodeToBinary(buffer[index]);
  }
}

////////////////////////////////////////////////////////////////////////////////

unsigned binaryToGrayCode(unsigned c)
{
  return (c >> 1) ^ c;
}

void OperationBitsToGrayCodeRGBA::doit(TextureRGBA8& texture, Progress&)
{
  unsigned char* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();
  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  {
    size_t index = y * u2 * 4 + x * 4;
    unsigned i = lpi::RGBAtoINT(buffer[index], buffer[index + 1], buffer[index + 2], buffer[index + 3]);
    i = binaryToGrayCode(i);
    lpi::INTtoRGBA(&buffer[index], i);
  }
}

////////////////////////////////////////////////////////////////////////////////

unsigned grayCodeToBinary(unsigned c)
{
  for(size_t shift = 1; shift < 32; shift *= 2)
  {
    c ^= c >> shift;
  }
  return c;
}

void OperationBitsFromGrayCodeRGBA::doit(TextureRGBA8& texture, Progress&)
{
  unsigned char* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();
  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  {
    size_t index = y * u2 * 4 + x * 4;
    unsigned i = lpi::RGBAtoINT(buffer[index], buffer[index + 1], buffer[index + 2], buffer[index + 3]);
    i = grayCodeToBinary(i);
    lpi::INTtoRGBA(&buffer[index], i);
  }
}

////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////

void OperationRandomShuffle::doit(TextureRGBA8& texture, Progress&)
{
  unsigned char* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();

  //Fisher-Yates shuffle
  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  {
    size_t index = y * u2 * 4 + x * 4;
    size_t n = y * u + x;
    int r = lpi::getRandom((int)n, (int)(u * v - 1));
    size_t xr = r % u;
    size_t yr = r / u;
    size_t indexr = yr * u2 * 4 + xr * 4;
    for(size_t c = 0; c < 4; c++)
    {
      std::swap(buffer[index + c], buffer[indexr + c]);
    }
  }
}

////////////////////////////////////////////////////////////////////////////////

OperationRandomWalk::OperationRandomWalk(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom)
: AOperationFilterUtil(settings, true, true)
, dialog(this)
, amount(1.0)
, distance(1)
, wrapping(false)
{
  params.addDouble("Amount", &amount, 0.0, 5.0, 0.01);
  params.addInt("Distance", &distance, 1, 32, 1);
  params.addBool("Wrapping", &wrapping);

  paramsToDialog(dialog.page, params, geom);
}

void OperationRandomWalk::doit(TextureRGBA8& texture, Progress&)
{
  unsigned char* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();

  size_t num = (size_t)(u * v * amount) / 2; //divided through 2 because per random pixel, two pixels are involved (and amount of 1.0 represents that on average every pixel is touched once)



  for(size_t i = 0; i < num; i++)
  {
    int x = (int)(lpi::getRandom() * u);
    int y = (int)(lpi::getRandom() * v);
    int x2 = x + (int)((lpi::getRandom() - 0.5) * 2 * (distance + 0.5));
    int y2 = y + (int)((lpi::getRandom() - 0.5) * 2 * (distance + 0.5));

    if(wrapping)
    {
      x2 = lpi::wrap<int>(x2, 0, u);
      y2 = lpi::wrap<int>(y2, 0, v);
    }
    else
    {
      x2 = lpi::clamp<int>(x2, 0, u - 1);
      y2 = lpi::clamp<int>(y2, 0, v - 1);
    }

    size_t index = y * u2 * 4 + x * 4;
    size_t index2 = y2 * u2 * 4 + x2 * 4;

    std::swap(buffer[index + 0], buffer[index2 + 0]);
    std::swap(buffer[index + 1], buffer[index2 + 1]);
    std::swap(buffer[index + 2], buffer[index2 + 2]);
    std::swap(buffer[index + 3], buffer[index2 + 3]);
  }

}

////////////////////////////////////////////////////////////////////////////////

OperationSepia::OperationSepia(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom)
: AOperationFilterUtil(settings, true, false)
, dialog(this)
, color(112/255.0, 66/255.0, 20/255.0) //official "sepia" color
, brightness(0.3)
{
  params.addColor("Sepia Color", &color);
  params.addDouble("Darkness", &brightness, 0.0, 1.0, 0.01);

  paramsToDialog(dialog.page, params, geom);
}

void OperationSepia::doit(TextureRGBA8& texture, Progress&)
{
  lpi::ColorRGBd sepia = color;

  unsigned char* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();
  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  {
    size_t index = y * u2 * 4 + x * 4;

    int v = (buffer[index + 0] + buffer[index + 1] + buffer[index + 2]) / 3;
    int dark = (int)(brightness * 255);

    if(v < dark)
    {
      int w = dark == 0 ? 0 : (v * 255) / dark;
      buffer[index + 0] = (int)(sepia.r * w);
      buffer[index + 1] = (int)(sepia.g * w);
      buffer[index + 2] = (int)(sepia.b * w);
    }
    else
    {
      int w = (dark == 255) ? 0 : ((v-dark)*255) / (255 - dark);
      buffer[index + 0] = (int)(sepia.r * (255 - w) + w);
      buffer[index + 1] = (int)(sepia.g * (255 - w) + w);
      buffer[index + 2] = (int)(sepia.b * (255 - w) + w);
    }

  }
}

void OperationSepia::doit(TextureRGBA32F& texture, Progress&)
{
  lpi::ColorRGBd sepia = color;

  float* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();
  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  {
    size_t index = y * u2 * 4 + x * 4;

    float intensity = (buffer[index + 0] + buffer[index + 1] + buffer[index + 2]) / 3;
    float dark = brightness;

    if(v < dark)
    {
      float w = intensity / dark;
      buffer[index + 0] = (sepia.r * w);
      buffer[index + 1] = (sepia.g * w);
      buffer[index + 2] = (sepia.b * w);
    }
    else
    {
      float w = ((intensity-dark)) / (1.0 - dark);
      buffer[index + 0] = (sepia.r * (1.0 - w) + w);
      buffer[index + 1] = (sepia.g * (1.0 - w) + w);
      buffer[index + 2] = (sepia.b * (1.0 - w) + w);
    }

  }
}

////////////////////////////////////////////////////////////////////////////////

OperationContour::OperationContour(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom)
: AOperationFilterUtil(settings, true, true)
, dialog(this)
, value(128)
, upper(true)
, wrapping(false)
, affect_alpha(false)
{
  params.addDouble("Value", &value, 0.0, 255.0, 1.0);
  params.addNamedBool("Place Edge", &upper, "Lower", "Upper");
  params.addBool("Wrapping", &wrapping);
  params.addBool("Affect Alpha", &affect_alpha);

  paramsToDialog(dialog.page, params, geom);
}

void OperationContour::doit(TextureRGBA8& texture, Progress&)
{
  unsigned char* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();

  std::vector<unsigned char> old;
  getAlignedBuffer(old, &texture);

  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  for(size_t c = 0; c < 4; c++)
  {
    if(!affect_alpha && c == 3) continue;
    size_t oindex = y * u * 4 + x * 4 + c;

    bool higher = old[oindex] > value;

    bool ok = false;

    for(int y3 = -1; y3 < 2; y3++)
    for(int x3 = -1; x3 < 2; x3++)
    {
      int x2 = x + x3;
      int y2 = y + y3;
      if(wrapping)
      {
        x2 = lpi::wrap<int>(x2, 0, u);
        y2 = lpi::wrap<int>(y2, 0, v);
      }
      else
      {
        if(x2 < 0) continue;
        if(y2 < 0) continue;
        if(x2 >= (int)u) continue;
        if(y2 >= (int)v) continue;
      }

      size_t index2 = y2 * u * 4 + x2 * 4 + c;

      bool neighborHigher = old[index2] > value;
      if(neighborHigher != higher)
      {
        ok = true;
        break;
      }
    }

    size_t index = y * u2 * 4 + x * 4;

    if(ok && upper != higher) buffer[index + c] = 255;
    else buffer[index + c] = 0;
  }
}

void OperationContour::doit(TextureGrey8& texture, Progress&)
{
  unsigned char* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();

  std::vector<unsigned char> old;
  getAlignedBuffer(old, &texture);

  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  {
    size_t oindex = y * u + x;

    bool higher = old[oindex + 0] > value;

    bool ok = false;

    for(int y3 = -1; y3 < 2; y3++)
    for(int x3 = -1; x3 < 2; x3++)
    {
      int x2 = x + x3;
      int y2 = y + y3;
      if(x2 < 0) continue;
      if(y2 < 0) continue;
      if(x2 >= (int)u) continue;
      if(y2 >= (int)v) continue;

      size_t index2 = y2 * u + x2;

      bool neighborHigher = old[index2] > value;
      if(neighborHigher != higher)
      {
        ok = true;
        break;
      }
    }

    size_t index = y * u2 + x;

    if(ok && upper != higher) buffer[index] = 255;
    else buffer[index] = 0;
  }
}

////////////////////////////////////////////////////////////////////////////////

OperationPointillism::OperationPointillism(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom)
: AOperationFilterUtil(settings, true, true)
, dialog(this)
, pointSize(7)
, opacity(1.0)
, amount(0.5)
, onCanvas(false)
, regular(false)
{
  params.addInt("Point Size", &pointSize, 3, 64, 1);
  params.addDouble("Opacity", &opacity, 0.0, 1.0, 0.01);
  params.addDouble("Amount", &amount, 0.0, 1.0, 0.01);
  params.addBool("Regular", &regular);
  params.addBool("BG Colored Canvas", &onCanvas);
  params.addColor("BG Color", &settings.rightColor);

  paramsToDialog(dialog.page, params, geom);
}

void OperationPointillism::doit(TextureRGBA8& texture, Progress& progress)
{
  unsigned char* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();

  std::vector<unsigned char> old;
  getAlignedBuffer(old, &texture);

  if(onCanvas)
  {
    for(size_t y = 0; y < v; y++)
    for(size_t x = 0; x < u; x++)
    {
      size_t index = y * u2 * 4 + x * 4;
      buffer[index + 0] = settings.rightColor255().r;
      buffer[index + 1] = settings.rightColor255().g;
      buffer[index + 2] = settings.rightColor255().b;
      buffer[index + 3] = settings.rightColor255().a;
    }
  }
  else
  {
    //when not using canvas color, put a blurred version of the original image behind it (because some spots are "missed")
    //use interpolating blur, not gaussian blur, because it can have a large radius, gaussian would be too slow and it's just a background that is hardly visible anyway
    interpolatingBlurRGBA8(buffer, u, v, u2, pointSize, true, dummyProgress);
  }

  Brush brush;
  brush.size = pointSize;
  brush.softness = 1.0;
  brush.opacity = opacity;
  brush.shape = Brush::SHAPE_ROUND;
  ExtraBrushOptions e;

  if(regular)
  {
    int s = brush.size; if(s < 1) s = 1;
    double stepx = ((brush.size) / 2.0) / amount;
    double stepy = ((brush.size) / 2.0) / amount;

    for(double dy = 0; dy < v; dy += stepy)
    {
      for(double dx = 0; dx < u; dx += stepx)
      {
        int x = (int)(dx + lpi::getRandom() * stepx);
        int y = (int)(dy + lpi::getRandom() * stepy);
        if(x < 0) x = 0; if(x >= (int)u) x = u - 1;
        if(y < 0) y = 0; if(y >= (int)v) y = v - 1;

        size_t index = y * u * 4 + x * 4;
        lpi::ColorRGB color(old[index + 0], old[index + 1], old[index + 2], old[index + 3]);
        drawBrushShape(buffer, u2, v, x, y, color, brush, e);
      }
      if(progress.handle(dy / v)) return;
    }
  }
  else
  {
    int div = (brush.size * brush.size) / 2;
    if(div < 1) div = 1;
    int num = (int)(8 * amount * (u * v) / div);
    for(int i = 0; i < num; i++)
    {
      int x = (int)(lpi::getRandom() * u);
      int y = (int)(lpi::getRandom() * v);

      size_t index = y * u * 4 + x * 4;
      lpi::ColorRGB color(old[index + 0], old[index + 1], old[index + 2], old[index + 3]);
      drawBrushShape(buffer, u2, v, x, y, color, brush, e);

      if(i % 100 == 0 && progress.handle(i, num)) return;
    }
  }
}

////////////////////////////////////////////////////////////////////////////////

void OperationBrushy::doit(TextureRGBA8& texture, Progress& progress)
{
  unsigned char* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();

  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  for(size_t c = 0; c < 4; c++)
  {
    size_t index = y * u2 * 4 + x * 4 + c;
    buffer[index] = mirrorbits(buffer[index]);
  }

  erodeRGBA8(buffer, u, v, u2, false, true, progress);

  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  for(size_t c = 0; c < 4; c++)
  {
    size_t index = y * u2 * 4 + x * 4 + c;
    buffer[index] = mirrorbits(buffer[index]);
  }
}

////////////////////////////////////////////////////////////////////////////////

void OperationSpookyNeon::doit(TextureRGBA8& texture, Progress& progress)
{
  unsigned char* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();

  for(size_t y = 0; y < v; y++)
  {
    if(progress.handle(y, v)) return;
    for(size_t x = 0; x < u; x++)
    for(size_t c = 0; c < 4; c++)
    {
      if(c == 3) continue;
      size_t index = y * u2 * 4 + x * 4 + c;
      buffer[index] =  binaryToGrayCode(buffer[index]);
      buffer[index] = shiftbits1(buffer[index]);
      buffer[index] =  grayCodeToBinary(buffer[index]);
    }
  }
}

////////////////////////////////////////////////////////////////////////////////

OperationErode::OperationErode(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom)
: AOperationFilterUtil(settings, true, true)
, wrapping(false)
, affect_alpha(true)
, dialog(this)
{
  (void)geom;

  params.addBool("Wrapping", &wrapping);
  params.addBool("Affect Alpha", &affect_alpha);

  paramsToDialog(dialog.page, params, geom);
}

void OperationErode::doit(TextureRGBA8& texture, Progress& progress)
{
  unsigned char* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();

  erodeRGBA8(buffer, u, v, u2, wrapping, affect_alpha, progress);
}


////////////////////////////////////////////////////////////////////////////////

OperationDilate::OperationDilate(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom)
: AOperationFilterUtil(settings, true, true)
, wrapping(false)
, affect_alpha(true)
, dialog(this)
{
  (void)geom;

  params.addBool("Wrapping", &wrapping);
  params.addBool("Affect Alpha", &affect_alpha);

  paramsToDialog(dialog.page, params, geom);
}


void OperationDilate::doit(TextureRGBA8& texture, Progress& progress)
{
  unsigned char* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();

  dilateRGBA8(buffer, u, v, u2, wrapping, affect_alpha, progress);
}

////////////////////////////////////////////////////////////////////////////////

OperationMorphologyOpen::OperationMorphologyOpen(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom)
: AOperationFilterUtil(settings, true, true)
, wrapping(false)
, affect_alpha(true)
, dialog(this)
{
  (void)geom;

  params.addBool("Wrapping", &wrapping);
  params.addBool("Affect Alpha", &affect_alpha);

  paramsToDialog(dialog.page, params, geom);
}

void OperationMorphologyOpen::doit(TextureRGBA8& texture, Progress& progress)
{
  unsigned char* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();

  NestedProgress np(&progress, 0.0, 0.5);
  erodeRGBA8(buffer, u, v, u2, wrapping, affect_alpha, np);
  np.setInterval(0.5, 1.0);
  dilateRGBA8(buffer, u, v, u2, wrapping, affect_alpha, np);
}


////////////////////////////////////////////////////////////////////////////////

OperationMorphologyClose::OperationMorphologyClose(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom)
: AOperationFilterUtil(settings, true, true)
, wrapping(false)
, affect_alpha(true)
, dialog(this)
{
  (void)geom;

  params.addBool("Wrapping", &wrapping);
  params.addBool("Affect Alpha", &affect_alpha);

  paramsToDialog(dialog.page, params, geom);
}

void OperationMorphologyClose::doit(TextureRGBA8& texture, Progress& progress)
{
  unsigned char* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();

  NestedProgress np(&progress, 0.0, 0.5);
  dilateRGBA8(buffer, u, v, u2, wrapping, affect_alpha, np);
  np.setInterval(0.5, 1.0);
  erodeRGBA8(buffer, u, v, u2, wrapping, affect_alpha, np);
}

////////////////////////////////////////////////////////////////////////////////

OperationMorphologyGradient::OperationMorphologyGradient(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom)
: AOperationFilterUtil(settings, true, true)
, wrapping(false)
, affect_alpha(false)
, dialog(this)
{
  (void)geom;

  params.addBool("Wrapping", &wrapping);
  params.addBool("Affect Alpha", &affect_alpha);

  paramsToDialog(dialog.page, params, geom);
}

void OperationMorphologyGradient::doit(TextureRGBA8& texture, Progress& progress)
{
  unsigned char* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();

  std::vector<unsigned char> b;
  getAlignedBuffer(b, &texture);

  NestedProgress np(&progress, 0.0, 0.5);
  erodeRGBA8(&b[0], u, v, u, wrapping, affect_alpha, np);
  np.setInterval(0.5, 1.0);
  dilateRGBA8(buffer, u, v, u2, wrapping, affect_alpha, np);

  //dilate minus erode

  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  for(size_t c = 0; c < 4; c++)
  {
    if(c == 3 && !affect_alpha) continue;

    size_t index = y * u * 4 + x * 4 + c;
    size_t index2 = y * u2 * 4 + x * 4 + c;

    buffer[index2] = 128 + (buffer[index2] - b[index]) / 2;
  }
}

////////////////////////////////////////////////////////////////////////////////

OperationMorphologyTopHat::OperationMorphologyTopHat(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom)
: AOperationFilterUtil(settings, true, true)
, wrapping(false)
, affect_alpha(false)
, dialog(this)
{
  (void)geom;

  params.addBool("Wrapping", &wrapping);
  params.addBool("Affect Alpha", &affect_alpha);

  paramsToDialog(dialog.page, params, geom);
}

void OperationMorphologyTopHat::doit(TextureRGBA8& texture, Progress& progress)
{
  unsigned char* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();

  std::vector<unsigned char> b;
  getAlignedBuffer(b, &texture);

  NestedProgress np(&progress, 0.0, 0.5);
  erodeRGBA8(buffer, u, v, u2, wrapping, affect_alpha, np);
  np.setInterval(0.5, 1.0);
  dilateRGBA8(buffer, u, v, u2, wrapping, affect_alpha, np);

  //original minus open

  for(size_t y = 0; y < v; y++)
  for(size_t x = 0; x < u; x++)
  for(size_t c = 0; c < 4; c++)
  {
    if(c == 3 && !affect_alpha) continue;

    size_t index = y * u * 4 + x * 4 + c;
    size_t index2 = y * u2 * 4 + x * 4 + c;

    buffer[index2] = 128 + (b[index] - buffer[index2]) / 2;
  }
}

////////////////////////////////////////////////////////////////////////////////

void OperationGameOfLife::doit(TextureRGBA8& texture, Progress& progress)
{
  unsigned char* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();

  std::vector<unsigned char> old;
  getAlignedBuffer(old, &texture);

  for(size_t y = 0; y < v; y++)
  {
    if(progress.handle(y, v)) return;
    for(size_t x = 0; x < u; x++)
    {
      size_t index = y * u * 4 + x * 4;

      int value = (old[index + 0] + old[index + 1] + old[index + 2]) / 3;

      int lowest = -1;
      int highest = -1;
      int lowestVal = 0;
      int highestVal = 0;
      int count = 0;

      for(int y3 = -1; y3 < 2; y3++)
      for(int x3 = -1; x3 < 2; x3++)
      {
        if(x3 == 0 && y3 == 0) continue;

        int x2 = lpi::wrap<int>(x + x3, 0, u);
        int y2 = lpi::wrap<int>(y + y3, 0, v);

        size_t index2 = y2 * u * 4 + x2 * 4;

        int value2 = (old[index2 + 0] + old[index2 + 1] + old[index2 + 2]) / 3;

        if(lowest < 0)
        {
          lowest = highest = index2;
          lowestVal = highestVal = value2;
        }
        if(value2 > highestVal)
        {
          highestVal = value2;
          highest = index2;
        }
        if(value2 < lowestVal)
        {
          lowestVal = value2;
          lowest = index2;
        }

        if(value2 < 127) count++;
      }

      size_t index2 = y * u2 * 4 + x * 4;
      if(value < 127)
      {
        if(count < 2 || count > 3)
        {
          buffer[index2 + 0] = old[highest + 0];
          buffer[index2 + 1] = old[highest + 1];
          buffer[index2 + 2] = old[highest + 2];
          buffer[index2 + 3] = old[highest + 3];
        }
      }
      else
      {
        if(count == 3)
        {
          buffer[index2 + 0] = old[lowest + 0];
          buffer[index2 + 1] = old[lowest + 1];
          buffer[index2 + 2] = old[lowest + 2];
          buffer[index2 + 3] = old[lowest + 3];
        }
      }
    }
  }
}

////////////////////////////////////////////////////////////////////////////////

void OperationGameOfLifeExtinguish::doit(TextureRGBA8& texture, Progress& progress)
{
  unsigned char* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();

  std::vector<unsigned char> old;
  getAlignedBuffer(old, &texture);

  for(size_t y = 0; y < v; y++)
  {
    if(progress.handle(y, v)) return;
    for(size_t x = 0; x < u; x++)
    {
      size_t index = y * u * 4 + x * 4;

      int value = (old[index + 0] + old[index + 1] + old[index + 2]) / 3;

      int lowest = -1;
      int highest = -1;
      int lowestVal = 0;
      int highestVal = 0;

      for(int y3 = -1; y3 < 2; y3++)
      for(int x3 = -1; x3 < 2; x3++)
      {
        if(x3 == 0 && y3 == 0) continue;

        int x2 = lpi::wrap<int>(x + x3, 0, u);
        int y2 = lpi::wrap<int>(y + y3, 0, v);

        size_t index2 = y2 * u * 4 + x2 * 4;

        int value2 = (old[index2 + 0] + old[index2 + 1] + old[index2 + 2]) / 3;

        if(lowest < 0)
        {
          lowest = highest = index2;
          lowestVal = highestVal = value2;
        }
        if(value2 > highestVal)
        {
          highestVal = value2;
          highest = index2;
        }
        if(value2 < lowestVal)
        {
          lowestVal = value2;
          lowest = index2;
        }
      }

      size_t index2 = y * u2 * 4 + x * 4;
      if(value < 127)
      {
        buffer[index2 + 0] = old[highest + 0];
        buffer[index2 + 1] = old[highest + 1];
        buffer[index2 + 2] = old[highest + 2];
        buffer[index2 + 3] = old[highest + 3];
      }
    }
  }
}

////////////////////////////////////////////////////////////////////////////////


void OperationMakeSeamless::doit(TextureRGBA8& texture, Progress& progress)
{
  unsigned char* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();

  std::vector<unsigned char> old;
  getAlignedBuffer(old, &texture);

  for(size_t y = 0; y < v; y++)
  {
    if(progress.handle(y, v)) return;
    for(size_t x = 0; x <= u / 2; x++)
    {
      int x2 = lpi::wrap(x + u / 2, 0, u);
      int y2 = lpi::wrap(y + v / 2, 0, v);

      size_t index = y * u2 * 4 + x * 4;
      size_t index2 = y2 * u2 * 4 + x2 * 4;

      double xc = (std::abs((int)x - (int)u / 2) )/ (double)(u / 2);
      double yc = (std::abs((int)y - (int)v / 2) ) / (double)(v / 2);

      double f = xc * yc / (xc * yc + (1.0 - xc) * (1.0 - yc)); //interpolation
      if (xc == 0.0 && yc == 1.0) f = 1.0;
      else if (xc == 1.0 && yc == 0.0) f = 0.0;
      f = lpi::clamp(f, 0.0, 1.0);

      int r1 = buffer[index + 0];
      int g1 = buffer[index + 1];
      int b1 = buffer[index + 2];
      int a1 = buffer[index + 3];
      int r2 = buffer[index2 + 0];
      int g2 = buffer[index2 + 1];
      int b2 = buffer[index2 + 2];
      int a2 = buffer[index2 + 3];

      buffer[index + 0] = buffer[index2 + 0] = (int)(f * r2 + (1.0 - f) * r1 + 0.5);
      buffer[index + 1] = buffer[index2 + 1] = (int)(f * g2 + (1.0 - f) * g1 + 0.5);
      buffer[index + 2] = buffer[index2 + 2] = (int)(f * b2 + (1.0 - f) * b1 + 0.5);
      buffer[index + 3] = buffer[index2 + 3] = (int)(f * a2 + (1.0 - f) * a1 + 0.5);
    }
  }
}

////////////////////////////////////////////////////////////////////////////////

OperationGenerateRandomLines::OperationGenerateRandomLines(GlobalToolSettings& settings, const lpi::gui::IGUIDrawer& geom)
: AOperationFilterUtil(settings, true, true)
, amount(50)
, minwidth(8)
, maxwidth(8)
, minlength(200)
, maxlength(500)
, opacity(1.0)
, dialog(this)
{
  params.addInt("Amount", &amount, 1, 200, 1);
  params.addInt("Min width", &minwidth, 1, 20, 1);
  params.addInt("Max width", &maxwidth, 1, 20, 1);
  params.addInt("Min length", &minlength, 1, 1000, 1);
  params.addInt("Max length", &maxlength, 1, 1000, 1);
  params.addColor("FG Color", &settings.leftColor);
  params.addDouble("Opacity", &opacity, 0.0, 1.0, 0.01);

  paramsToDialog(dialog.page, params, geom);
}

void OperationGenerateRandomLines::doit(TextureRGBA8& texture, Progress&)
{
  unsigned char* buffer = texture.getBuffer();
  size_t u = texture.getU();
  size_t v = texture.getV();
  size_t u2 = texture.getU2();

  int actualminlength = lpi::template_min(lpi::template_min(u, v), minlength);
  int actualmaxlength = lpi::template_min(lpi::template_min(u, v), maxlength);

  for(int i = 0; i < amount; i++)
  {
    int x0 = (int)(lpi::getRandom() * u);
    int y0 = (int)(lpi::getRandom() * v);
    double length = actualminlength + lpi::getRandom() * (actualmaxlength - actualminlength);
    double angle = lpi::getRandom() * 3.14159 * 2;
    int x1 = x0 + (int)(std::cos(angle) * length);
    int y1 = y0 + (int)(std::sin(angle) * length);
    int thickness = minwidth + (int)(lpi::getRandom() * (maxwidth - minwidth));
    drawThickLine(buffer, u, v, u2, x0, y0, x1, y1, thickness, lpi::RGBdtoRGB(settings.leftColor), opacity, true, true);
  }
}

////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
