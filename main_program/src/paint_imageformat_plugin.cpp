/*
LodePaint

Copyright (c) 2009-2010 Lode Vandevenne
All rights reserved.

This file is part of LodePaint.

LodePaint is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LodePaint is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LodePaint.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "paint_imageformat_plugin.h"

////////////////////////////////////////////////////////////////////////////////

#include "paint_filter_plugin.h"

#if defined(LPI_OS_LINUX) || defined(LPI_OS_AMIGA)


#include <dlfcn.h>

namespace
{
  template<typename F>
  void do_dlsym(F& functionPointer, void* dl, const char* name) //to avoid warning "dereferencing type-punned pointer will break strict-aliasing rulesdereferencing type-punned pointer will break strict-aliasing rules" when using dlsym
  {
    //*(void **)(&functionPointer) = dlsym(dl, name); //this gives the warning
    
    union { F func; void* obj; } alias; //Wikipedia's way to avoid the warning!
#if defined(LPI_OS_AMIGA)
    alias.obj = dlsym(dl, (char*)name); //Amiga's dlsym takes char* instead of const char*
#else
    alias.obj = dlsym(dl, name);
#endif
    functionPointer = alias.func;

  }
}

ImageFormatPlugin::ImageFormatPlugin(const std::string& dynamicLib)
: isvalid(true)
, p_fileFormatGetDescription(0)
, p_fileFormatGetNumExt(0)
, p_fileFormatGetExt(0)
, p_fileFormatIsOfFormat(0)
, p_fileFormatEncodeRGBA8(0)
, p_fileFormatDecodeRGBA8(0)
, p_fileFormatEncodeRGBA32F(0)
, p_fileFormatDecodeRGBA32F(0)
, p_pluginFreeUnsignedChar(0)
, p_pluginFreeFloat(0)
, p_getLastError(0)
{
  dl = dlopen(dynamicLib.c_str(), RTLD_LOCAL | RTLD_LAZY);

  if(dl)
  {
    int (*p_getLodePaintPluginType)();
    do_dlsym(p_getLodePaintPluginType, dl, "getLodePaintPluginType");
    if(p_getLodePaintPluginType && p_getLodePaintPluginType() == 0)
    {
      //file format plugin identifier found!
    }
    else
    {
      isvalid = false;
      dlclose(dl);
      dl = 0;
    }
  }
  else
  {
    //std::cout << "not valid: " << dynamicLib << std::endl;
  }
  
  if(dl)
  {
    std::cout<<"Loading File Format Plugin: " << dynamicLib << std::endl;
    
    do_dlsym(p_fileFormatGetDescription, dl, "fileFormatGetDescription");
    if(p_fileFormatGetDescription)
    {
      char desc[80];
      p_fileFormatGetDescription(desc, 80);
      description = desc;
    }
    else
    {
      std::cout << "fileFormatGetDescription not found "<< dlerror()<<std::endl;
      isvalid = false;
    }
    
    do_dlsym(p_fileFormatGetNumExt, dl, "fileFormatGetNumExt");
    if(!p_fileFormatGetNumExt)
    {
      std::cout << "fileFormatGetNumExt not found "<< dlerror()<<std::endl;
      isvalid = false;
    }
    
    do_dlsym(p_fileFormatGetExt, dl, "fileFormatGetExt");
    if(!p_fileFormatGetExt)
    {
      std::cout << "fileFormatGetExt not found "<< dlerror()<<std::endl;
      isvalid = false;
    }
    
    do_dlsym(p_fileFormatIsOfFormat, dl, "fileFormatIsOfFormat");
    if(!p_fileFormatIsOfFormat)
    {
      std::cout << "fileFormatIsOfFormat not found "<< dlerror()<<std::endl;
      isvalid = false;
    }
    
    do_dlsym(p_pluginFreeUnsignedChar, dl, "pluginFreeUnsignedChar");
    if(!p_pluginFreeUnsignedChar)
    {
      std::cout << "pluginFreeUnsignedChar not found "<< dlerror()<<std::endl;
      isvalid = false;
    }
    
    do_dlsym(p_fileFormatDecodeRGBA8, dl, "fileFormatDecodeRGBA8");
    do_dlsym(p_fileFormatEncodeRGBA8, dl, "fileFormatEncodeRGBA8");
    do_dlsym(p_fileFormatDecodeRGBA32F, dl, "fileFormatDecodeRGBA32F");
    do_dlsym(p_fileFormatEncodeRGBA32F, dl, "fileFormatEncodeRGBA32F");

    do_dlsym(p_pluginFreeFloat, dl, "pluginFreeFloat");
    if(!p_pluginFreeFloat && p_fileFormatDecodeRGBA32F)
    {
      std::cout << "pluginFreeFloat not found "<< dlerror()<<std::endl;
      isvalid = false;
    }
    
    do_dlsym(p_getLastError, dl, "getLastError");

  }
  else isvalid = false;
  
  if(!isvalid && dl != 0)
  {
    dlclose(dl);
    dl = 0;
  }
}

ImageFormatPlugin::~ImageFormatPlugin()
{
  if(dl) dlclose(dl);
}

#elif defined(LPI_OS_WINDOWS)

ImageFormatPlugin::ImageFormatPlugin(const std::string& dynamicLib)
: isvalid(true)
, p_fileFormatGetDescription(0)
, p_fileFormatGetNumExt(0)
, p_fileFormatGetExt(0)
, p_fileFormatIsOfFormat(0)
, p_fileFormatEncodeRGBA8(0)
, p_fileFormatDecodeRGBA8(0)
, p_fileFormatEncodeRGBA32F(0)
, p_fileFormatDecodeRGBA32F(0)
, p_pluginFreeUnsignedChar(0)
, p_pluginFreeFloat(0)
, p_getLastError(0)
{
  bool isDLLFileName = lpi::extEqualsIgnoreCase(dynamicLib, "dll");
  //LOAD_WITH_ALTERED_SEARCH_PATH ensures it looks in the folder where we load the dll from, for possible other dll dependencies of that dll (instead of searching in the main lodepaint dir)
  if(isDLLFileName) dl = LoadLibraryEx(dynamicLib.c_str(), 0, LOAD_WITH_ALTERED_SEARCH_PATH);
  else dl = 0; //avoid MS Windows dialog boxes for files in the plugin folder that aren't a DLL


  if(dl)
  {
    typedef int (*T_getLodePaintPluginType)();
    T_getLodePaintPluginType p_getLodePaintPluginType;
    p_getLodePaintPluginType = (T_getLodePaintPluginType)GetProcAddress(dl, "getLodePaintPluginType");

    if(p_getLodePaintPluginType && p_getLodePaintPluginType() == 0)
    {
      //file format plugin identifier found!
    }
    else
    {
      isvalid = false;
      FreeLibrary(dl);
      dl = 0;
    }
  }

  if(dl)
  {
    std::cout<<"Loading File Format Plugin: " << dynamicLib << std::endl;


    p_fileFormatGetDescription = (T_fileFormatGetDescription)GetProcAddress(dl, "fileFormatGetDescription");
    if(p_fileFormatGetDescription)
    {
      char desc[80];
      p_fileFormatGetDescription(desc, 80);
      description = desc;
    }
    else
    {
      std::cout << "fileFormatGetDescription not found"<<std::endl;
      isvalid = false;
    }

    p_fileFormatGetNumExt = (T_fileFormatGetNumExt)GetProcAddress(dl, "fileFormatGetNumExt");
    if(!p_fileFormatGetNumExt)
    {
      std::cout << "fileFormatGetNumExt not found"<<std::endl;
      isvalid = false;
    }
    
    p_fileFormatGetExt = (T_fileFormatGetExt)GetProcAddress(dl, "fileFormatGetExt");
    if(!p_fileFormatGetExt)
    {
      std::cout << "fileFormatGetExt not found"<<std::endl;
      isvalid = false;
    }

    p_fileFormatIsOfFormat = (T_fileFormatIsOfFormat)GetProcAddress(dl, "fileFormatIsOfFormat");
    if(!p_fileFormatIsOfFormat)
    {
      std::cout << "fileFormatIsOfFormat not found"<<std::endl;
      isvalid = false;
    }

    p_pluginFreeUnsignedChar = (T_pluginFreeUnsignedChar)GetProcAddress(dl, "pluginFreeUnsignedChar");
    if(!p_pluginFreeUnsignedChar)
    {
      std::cout << "pluginFreeUnsignedChar not found"<<std::endl;
      isvalid = false;
    }

    p_fileFormatDecodeRGBA8 = (T_fileFormatDecodeRGBA8)GetProcAddress(dl, "fileFormatDecodeRGBA8");
    p_fileFormatEncodeRGBA8 = (T_fileFormatEncodeRGBA8)GetProcAddress(dl, "fileFormatEncodeRGBA8");
    p_fileFormatDecodeRGBA32F = (T_fileFormatDecodeRGBA32F)GetProcAddress(dl, "fileFormatDecodeRGBA32F");
    p_fileFormatEncodeRGBA32F = (T_fileFormatEncodeRGBA32F)GetProcAddress(dl, "fileFormatEncodeRGBA32F");

    p_pluginFreeFloat = (T_pluginFreeFloat)GetProcAddress(dl, "pluginFreeFloat");
    if(!p_pluginFreeFloat && p_fileFormatDecodeRGBA32F)
    {
      std::cout << "pluginFreeFloat not found"<<std::endl;
      isvalid = false;
    }

    p_getLastError = (T_getLastError)GetProcAddress(dl, "getLastError");

  }
  else isvalid = false;

  if(!isvalid && dl != 0)
  {
    FreeLibrary(dl);
    dl = 0;
  }
}

ImageFormatPlugin::~ImageFormatPlugin()
{
  if(dl) FreeLibrary(dl);
}

#elif defined(LPI_OS_AROS)

ImageFormatPlugin::ImageFormatPlugin(const std::string& dynamicLib)
: isvalid(true)
, p_fileFormatGetDescription(0)
, p_fileFormatGetNumExt(0)
, p_fileFormatGetExt(0)
, p_fileFormatIsOfFormat(0)
, p_fileFormatEncodeRGBA8(0)
, p_fileFormatDecodeRGBA8(0)
, p_fileFormatEncodeRGBA32F(0)
, p_fileFormatDecodeRGBA32F(0)
, p_pluginFreeUnsignedChar(0)
, p_pluginFreeFloat(0)
, p_getLastError(0)
{
}

ImageFormatPlugin::~ImageFormatPlugin()
{
}

#endif

void ImageFormatPlugin::setError(std::string& error, int result) const
{
  if(!p_getLastError)
  {
    std::stringstream ss;
    ss << "plugin returned error code " << result;
    error = ss.str();
  }
  else
  {
    char err[1024];
    p_getLastError(err, 1024);
    error = "plugin returned error: ";
    error += err;
    if(error.empty())
    {
      std::stringstream ss;
      ss << "plugin returned error code " << result;
      error = ss.str();
    }
  }
}

bool ImageFormatPlugin::isValidFileFormatPlugin() const
{
  return isvalid;
}

bool ImageFormatPlugin::canDecodeRGBA8() const
{
  return p_fileFormatDecodeRGBA8 != 0 && isvalid;
}

bool ImageFormatPlugin::canEncodeRGBA8() const
{
  return p_fileFormatEncodeRGBA8 != 0 && isvalid;
}

bool ImageFormatPlugin::decodeRGBA8(std::string& error, std::vector<unsigned char>& image, int& w, int& h, const unsigned char* file, size_t filesize) const
{
  if(!isvalid)
  {
    error = "plugin not valid";
    return false;
  }
  
  if(p_fileFormatDecodeRGBA8 != 0)
  {
    unsigned char* img;
    int result = p_fileFormatDecodeRGBA8(&img, &w, &h, (unsigned char*)file, (unsigned long)filesize, 0);
    if(result != 0)
    {
      setError(error, result);
      return false;
    }
    size_t size = w * h * 4;
    image.resize(size);
    for(size_t i = 0; i < size; i++) image[i] = img[i];
    p_pluginFreeUnsignedChar(img);
    return true;
  }
  
  error = "plugin can't decode 32 bit";
  return false;
}

bool ImageFormatPlugin::encodeRGBA8(std::string& error, std::vector<unsigned char>& file, const unsigned char* image, int w, int h) const
{
  if(!isvalid)
  {
    error = "plugin not valid";
    return false;
  }
  
  if(p_fileFormatEncodeRGBA8 != 0)
  {
    unsigned char* buffer;
    unsigned long size;
    int result = p_fileFormatEncodeRGBA8(&buffer, &size, (unsigned char*)image, w, h, 0);
    if(result != 0)
    {
      setError(error, result);
      return false;
    }
    file.resize(size);
    for(size_t i = 0; i < size; i++) file[i] = buffer[i];
    p_pluginFreeUnsignedChar(buffer);
    return true;
  }
  
  error = "plugin can't encode 32 bit";
  return false;
}

bool ImageFormatPlugin::canDecodeRGBA32F() const
{
  return p_fileFormatDecodeRGBA32F != 0 && isvalid;
}

bool ImageFormatPlugin::canEncodeRGBA32F() const
{
  return p_fileFormatEncodeRGBA32F != 0 && isvalid;
}

bool ImageFormatPlugin::decodeRGBA32F(std::string& error, std::vector<float>& image, int& w, int& h, const unsigned char* file, size_t filesize) const
{
  if(!isvalid)
  {
    error = "plugin not valid";
    return false;
  }
  
  if(p_fileFormatDecodeRGBA32F != 0)
  {
    float* img;
    int result = p_fileFormatDecodeRGBA32F(&img, &w, &h, (unsigned char*)file, (unsigned long)filesize, 0);
    if(result != 0)
    {
      setError(error, result);
      return false;
    }
    if(w < 0 || h < 0)
    {
      error = "negative size";
      return false;
    }
    size_t size = w * h * 4;
    image.resize(size);
    for(size_t i = 0; i < size; i++) image[i] = img[i];
    p_pluginFreeFloat(img);
    return true;
  }
  
  error = "plugin can't decode 128 bit";
  return false;
}

bool ImageFormatPlugin::encodeRGBA32F(std::string& error, std::vector<unsigned char>& file, const float* image, int w, int h) const
{
  if(!isvalid)
  {
    error = "plugin not valid";
    return false;
  }
  
  if(p_fileFormatEncodeRGBA32F != 0)
  {
    unsigned char* buffer;
    unsigned long size;
    int result = p_fileFormatEncodeRGBA32F(&buffer, &size, (float*)image, w, h, 0);
    if(result != 0)
    {
      setError(error, result);
      return false;
    }
    file.resize(size);
    for(size_t i = 0; i < size; i++) file[i] = buffer[i];
    p_pluginFreeUnsignedChar(buffer);
    return true;
  }
  
  error = "plugin can't encode 128 bit";
  return false;
}

bool ImageFormatPlugin::isFileOfFormat(const unsigned char* file, size_t filesize) const
{
  if(p_fileFormatIsOfFormat != 0)
  {
    return p_fileFormatIsOfFormat((unsigned char*)file, (unsigned long)filesize);
  }
  else return false;
}

size_t ImageFormatPlugin::getNumExtensions() const
{
  if(p_fileFormatGetNumExt != 0)
  {
    return p_fileFormatGetNumExt();
  }
  else return 0;
}

std::string ImageFormatPlugin::getExtension(size_t i) const
{
  if(p_fileFormatGetExt)
  {
    char ext[80];
    p_fileFormatGetExt(ext, 80, i);
    return ext;
  }
  return "";
}

std::string ImageFormatPlugin::getDescription() const
{
  return description;
}



////////////////////////////////////////////////////////////////////////////////
