/*
LodePaint

Copyright (c) 2009-2010 Lode Vandevenne
All rights reserved.

This file is part of LodePaint.

LodePaint is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LodePaint is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LodePaint.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "lpi/lpi_texture.h"

#include "paint_window.h"
#include "paint_undo.h"

class BrushSettingWindow;
class ColorWindow;

/*
Generic interface for both interactive tools, and filters
*/
class IOperation
{
  protected:
    GlobalToolSettings& settings;
  public:
  
    IOperation(GlobalToolSettings& settings);
    virtual ~IOperation();
  
    virtual void undo(Paint& paint, UndoState& state) { (void)paint; (void)state; } //it'll put redo state in the undo state
    virtual void redo(Paint& paint, UndoState& state) { (void)paint; (void)state; } //the given state should contain redo information. It'll put back the undo information in the undo state.

    virtual std::string getLabel() const { return "Unnamed"; } //label is the name how it's displayed in the GUI
    virtual std::string getToolTip() const { return ""; }
    virtual lpi::HTexture* getIcon() const { return 0; }
    
    const GlobalToolSettings& getSettings() const { return settings; }
};
