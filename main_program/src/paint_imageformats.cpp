/*
LodePaint

Copyright (c) 2009-2010 Lode Vandevenne
All rights reserved.

This file is part of LodePaint.

LodePaint is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LodePaint is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LodePaint.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "paint_imageformats.h"

#include "paint_imageformat_plugin.h"
#include "paint_imageformat_native.h"

#include "imfmt/imageformats.h"

#include "imfmt/rgbe.h"
#include "lpi/lodepng.h"

////////////////////////////////////////////////////////////////////////////////



bool AImageFormat::canDecode(const unsigned char* file, size_t filesize) const
{
  return canDecode() && isFileOfFormat(file, filesize);
}

bool AImageFormat::canEncode(const Paint& paint) const
{
  if(paint.getColorMode() == MODE_RGBA_32F)
  {
    return canEncodeRGBA32F();
  }
  else if(paint.getColorMode() == MODE_RGBA_8)
  {
    return canEncodeRGBA8();
  }
  else if(paint.getColorMode() == MODE_GREY_8)
  {
    return canEncodeGrey8();
  }
  else if(paint.getColorMode() == MODE_GREY_32F)
  {
    return canEncodeGrey32F();
  }
  else return false;
}

bool AImageFormat::canDecode() const
{
  return canDecodeRGBA8() || canDecodeRGBA32F() || canDecodeGrey32F() || canDecodeGrey32F();
}

bool AImageFormat::canEncode() const
{
  return canEncodeRGBA8() || canEncodeRGBA32F() || canEncodeGrey32F() || canEncodeGrey32F();
}


bool AImageFormat::decodePaintRGBA8(std::string& error, Paint& paint, const unsigned char* file, size_t filesize) const
{
  std::vector<unsigned char> image;
  int w, h;

  if(!decodeRGBA8(error, image, w, h, file, filesize)) return false;

  if(paint.getColorMode() != MODE_RGBA_8) paint.setColorMode(MODE_RGBA_8, false);
  paint.canvasRGBA8.setSize(w, h);
  setAlignedBuffer(&paint.canvasRGBA8, &image[0], false);
  //paint.update();

  return true;
}

bool AImageFormat::decodePaintRGBA32F(std::string& error, Paint& paint, const unsigned char* file, size_t filesize) const
{
  std::vector<float> image;
  int w, h;

  if(!decodeRGBA32F(error, image, w, h, file, filesize)) return false;

  if(paint.getColorMode() != MODE_RGBA_32F) paint.setColorMode(MODE_RGBA_32F, false);
  paint.canvasRGBA32F.setSize(w, h);
  setAlignedBuffer(&paint.canvasRGBA32F, &image[0], false);
  //paint.update();

  return true;
}

bool AImageFormat::decodePaintGrey8(std::string& error, Paint& paint, const unsigned char* file, size_t filesize) const
{
  std::vector<unsigned char> image;
  int w, h;


  if(!decodeGrey8(error, image, w, h, file, filesize)) return false;

  if(paint.getColorMode() != MODE_GREY_8) paint.setColorMode(MODE_GREY_8, false);
  paint.canvasRGBA8.setSize(w, h);
  setAlignedBuffer(&paint.canvasGrey8, &image[0], false);
  //paint.update();

  return true;
}

bool AImageFormat::decodePaintGrey32F(std::string& error, Paint& paint, const unsigned char* file, size_t filesize) const
{
  std::vector<float> image;
  int w, h;

  if(!decodeGrey32F(error, image, w, h, file, filesize)) return false;

  if(paint.getColorMode() != MODE_GREY_32F) paint.setColorMode(MODE_GREY_32F, false);
  paint.canvasRGBA32F.setSize(w, h);
  setAlignedBuffer(&paint.canvasGrey32F, &image[0], false);
  //paint.update();

  return true;
}

bool AImageFormat::decode(std::string& error, Paint& paint, const unsigned char* file, size_t filesize) const
{
  //first prefer the format the Paint is currently in
  if(paint.getColorMode() == MODE_RGBA_32F && canDecodeRGBA32F())
  {
    return decodePaintRGBA32F(error, paint, file, filesize);
  }
  else if(paint.getColorMode() == MODE_RGBA_8 && canDecodeRGBA8())
  {
    return decodePaintRGBA8(error, paint, file, filesize);
  }
  else if(paint.getColorMode() == MODE_GREY_8 && canDecodeGrey8())
  {
    return decodePaintGrey8(error, paint, file, filesize);
  }
  else if(paint.getColorMode() == MODE_GREY_32F && canDecodeGrey32F())
  {
    return decodePaintGrey8(error, paint, file, filesize);
  }
  //if not possible, convert the Paint to the supported format and then decode
  else if(paint.getColorMode() != MODE_RGBA_8 && canDecodeRGBA8())
  {
    return decodePaintRGBA8(error, paint, file, filesize);
  }
  else if(paint.getColorMode() != MODE_RGBA_32F && canDecodeRGBA32F())
  {
    return decodePaintRGBA32F(error, paint, file, filesize);
  }
  else if(paint.getColorMode() != MODE_GREY_8 && canDecodeGrey8())
  {
    return decodePaintGrey8(error, paint, file, filesize);
  }
  else if(paint.getColorMode() != MODE_GREY_32F && canDecodeGrey32F())
  {
    return decodePaintGrey32F(error, paint, file, filesize);
  }

  error = "can't decode file";
  return false;
}

bool AImageFormat::encode(std::string& error, std::vector<unsigned char>& file, const Paint& paint) const
{
  if(paint.getColorMode() == MODE_RGBA_8)
  {
    std::vector<unsigned char> image;
    getAlignedBuffer(image, &paint.canvasRGBA8);
    return encodeRGBA8(error, file, image.empty() ? 0 : &image[0], (int)paint.canvasRGBA8.getU(), (int)paint.canvasRGBA8.getV());
  }
  else if(paint.getColorMode() == MODE_RGBA_32F)
  {
    std::vector<float> image;
    getAlignedBuffer(image, &paint.canvasRGBA32F);
    return encodeRGBA32F(error, file, image.empty() ? 0 : &image[0], paint.canvasRGBA32F.getU(), paint.canvasRGBA32F.getV());
  }
  else if(paint.getColorMode() == MODE_GREY_8)
  {
    std::vector<unsigned char> image;
    getAlignedBuffer(image, &paint.canvasGrey8);
    return encodeGrey8(error, file, image.empty() ? 0 : &image[0], (int)paint.canvasGrey8.getU(), (int)paint.canvasGrey8.getV());
  }
  else if(paint.getColorMode() == MODE_GREY_32F)
  {
    std::vector<float> image;
    getAlignedBuffer(image, &paint.canvasGrey32F);
    return encodeGrey32F(error, file, image.empty() ? 0 : &image[0], (int)paint.canvasGrey32F.getU(), (int)paint.canvasGrey32F.getV());
  }
  else
  {
    error = "Can't encode images of this Color Mode";
    return false;
  }
}


////////////////////////////////////////////////////////////////////////////////
 
bool ImageFormatPNG::canDecodeRGBA8() const
{
  return true;
}

bool ImageFormatPNG::canEncodeRGBA8() const
{
  return true;
}

bool ImageFormatPNG::canEncodeGrey8() const
{
  return true;
}

bool ImageFormatPNG::decodeRGBA8(std::string& error, std::vector<unsigned char>& image, int& w, int& h, const unsigned char* file, size_t filesize) const
{
  return decodeImageFile(error, image, w, h, file, filesize, IF_PNG);
}

bool ImageFormatPNG::encodeRGBA8(std::string& error, std::vector<unsigned char>& file, const unsigned char* image, int w, int h) const
{
  return encodeImageFile(error, file, image, w, h, IF_PNG);
}

bool ImageFormatPNG::encodeGrey8(std::string& error, std::vector<unsigned char>& file, const unsigned char* image, int w, int h) const
{
  ImageEncodeOptions options;
  options.dataNumChannels = options.formatNumChannels = 1;
  return encodeImageFile(error, file, image, w, h, IF_PNG, &options);
}


bool ImageFormatPNG::isFileOfFormat(const unsigned char* file, size_t filesize) const
{
  return isOfFormat(file, filesize, IF_PNG);
}

size_t ImageFormatPNG::getNumExtensions() const
{
  return 1;
}

std::string ImageFormatPNG::getExtension(size_t i) const
{
  (void)i;
  return "png";
}

std::string ImageFormatPNG::getDescription() const
{
  return "PNG files";
}
////////////////////////////////////////////////////////////////////////////////
 
bool ImageFormatBMP::canDecodeRGBA8() const
{
  return true;
}
bool ImageFormatBMP::canEncodeRGBA8() const
{
  return true;
}

bool ImageFormatBMP::decodeRGBA8(std::string& error, std::vector<unsigned char>& image, int& w, int& h, const unsigned char* file, size_t filesize) const
{
  return decodeImageFile(error, image, w, h, file, filesize, IF_BMP);
}

bool ImageFormatBMP::encodeRGBA8(std::string& error, std::vector<unsigned char>& file, const unsigned char* image, int w, int h) const
{
  return encodeImageFile(error, file, image, w, h, IF_BMP);
}


bool ImageFormatBMP::isFileOfFormat(const unsigned char* file, size_t filesize) const
{
  return isOfFormat(file, filesize, IF_BMP);
}

size_t ImageFormatBMP::getNumExtensions() const
{
  return 1;
}

std::string ImageFormatBMP::getExtension(size_t i) const
{
  (void)i;
  return "bmp";
}

std::string ImageFormatBMP::getDescription() const
{
  return "BMP files";
}
////////////////////////////////////////////////////////////////////////////////
 
bool ImageFormatTGA::canDecodeRGBA8() const
{
  return true;
}
bool ImageFormatTGA::canEncodeRGBA8() const
{
  return true;
}

bool ImageFormatTGA::decodeRGBA8(std::string& error, std::vector<unsigned char>& image, int& w, int& h, const unsigned char* file, size_t filesize) const
{
  return decodeImageFile(error, image, w, h, file, filesize, IF_TGA);
}

bool ImageFormatTGA::encodeRGBA8(std::string& error, std::vector<unsigned char>& file, const unsigned char* image, int w, int h) const
{
  return encodeImageFile(error, file, image, w, h, IF_TGA);
}


bool ImageFormatTGA::isFileOfFormat(const unsigned char* file, size_t filesize) const
{
  return isOfFormat(file, filesize, IF_TGA);
}

size_t ImageFormatTGA::getNumExtensions() const
{
  return 1;
}

std::string ImageFormatTGA::getExtension(size_t i) const
{
  (void)i;
  return "tga";
}

std::string ImageFormatTGA::getDescription() const
{
  return "TGA files";
}

////////////////////////////////////////////////////////////////////////////////

bool ImageFormatJPG::canDecodeRGBA8() const
{
  return true;
}
bool ImageFormatJPG::canEncodeRGBA8() const
{
  return false;
}

bool ImageFormatJPG::decodeRGBA8(std::string& error, std::vector<unsigned char>& image, int& w, int& h, const unsigned char* file, size_t filesize) const
{
  return decodeImageFile(error, image, w, h, file, filesize, IF_JPG);
}

bool ImageFormatJPG::isFileOfFormat(const unsigned char* file, size_t filesize) const
{
  return isOfFormat(file, filesize, IF_JPG);
}

size_t ImageFormatJPG::getNumExtensions() const
{
  return 2;
}

std::string ImageFormatJPG::getExtension(size_t i) const
{
  if(i == 0) return "jpg";
  else return "jpeg";
}

std::string ImageFormatJPG::getDescription() const
{
  return "JPEG files";
}

////////////////////////////////////////////////////////////////////////////////

bool ImageFormatGIF::canDecodeRGBA8() const
{
  return true;
}
bool ImageFormatGIF::canEncodeRGBA8() const
{
  return false;
}

bool ImageFormatGIF::decodeRGBA8(std::string& error, std::vector<unsigned char>& image, int& w, int& h, const unsigned char* file, size_t filesize) const
{
  return decodeImageFile(error, image, w, h, file, filesize, IF_GIF);
}

bool ImageFormatGIF::isFileOfFormat(const unsigned char* file, size_t filesize) const
{
  return isOfFormat(file, filesize, IF_GIF);
}

size_t ImageFormatGIF::getNumExtensions() const
{
  return 1;
}

std::string ImageFormatGIF::getExtension(size_t i) const
{
  (void)i;
  return "gif";
}

std::string ImageFormatGIF::getDescription() const
{
  return "GIF files";
}

////////////////////////////////////////////////////////////////////////////////

bool ImageFormatRGBE::canDecodeRGBA32F() const
{
  return true;
}
bool ImageFormatRGBE::canEncodeRGBA32F() const
{
  return true;
}

bool ImageFormatRGBE::decodeRGBA32F(std::string& error, std::vector<float>& image, int& w, int& h, const unsigned char* file, size_t filesize) const
{
  rgbe_header_info info;
  int err = RGBE_RETURN_SUCCESS;
  size_t index = 0;
  err = RGBE_ReadHeader(error, w,h,info, index, file, filesize);
  if(err != RGBE_RETURN_SUCCESS) return false;
  std::vector<float> image3;
  image3.resize(3*w*h);
  err = RGBE_ReadPixels_RLE(error, &image3[0],w,h, index, file, filesize);
  if(err == RGBE_RETURN_SUCCESS)
  {
    image.resize(4*w*h);
    for(int y = 0; y < h; y++)
    for(int x = 0; x < w; x++)
    {
      size_t iindex = y * w * 3 + x * 3;
      size_t oindex = y * w * 4 + x * 4;
      image[oindex + 0] = image3[iindex + 0];
      image[oindex + 1] = image3[iindex + 1];
      image[oindex + 2] = image3[iindex + 2];
      image[oindex + 3] = 1.0f;
    }
  }
  return err == RGBE_RETURN_SUCCESS;
}

bool ImageFormatRGBE::encodeRGBA32F(std::string& error, std::vector<unsigned char>& file, const float* image, int w, int h) const
{
  rgbe_header_info info;
  int err = RGBE_RETURN_SUCCESS;
  std::vector<float> image3(3*w*h);
  for(int y = 0; y < h; y++)
  for(int x = 0; x < w; x++)
  {
    size_t oindex = y * w * 3 + x * 3;
    size_t iindex = y * w * 4 + x * 4;
    image3[oindex + 0] = image[iindex + 0];
    image3[oindex + 1] = image[iindex + 1];
    image3[oindex + 2] = image[iindex + 2];
  }
  err = RGBE_WriteHeader(error,file,w,h,info);
  if(err != RGBE_RETURN_SUCCESS) return false;
  err = RGBE_WritePixels(error,file,&image3[0],w*h);
  return err == RGBE_RETURN_SUCCESS;
}


bool ImageFormatRGBE::isFileOfFormat(const unsigned char* file, size_t filesize) const
{
  return filesize > 2 && file[0] == '#' && file[1] == '?';
}

size_t ImageFormatRGBE::getNumExtensions() const
{
  return 2;
}

std::string ImageFormatRGBE::getExtension(size_t i) const
{
  if(i == 0) return "hdr";
  else return "rgbe";
}

std::string ImageFormatRGBE::getDescription() const
{
  return "radiance RGBE (HDR)";
}

////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////

ImageFormats::ImageFormats()
{
}

ImageFormats::~ImageFormats()
{
  for(size_t i = 0; i < formats.size(); i++) delete formats[i];
}

void ImageFormats::addPluginFormats(lpi::FileBrowse& filebrowser, const std::string& directory)
{
  std::vector<std::string> files;
  filebrowser.getFiles(files, directory);
  for(size_t i = 0; i < files.size(); i++)
  {
    ImageFormatPlugin* plugin = new ImageFormatPlugin(directory + files[i]);
    if(plugin->isValidFileFormatPlugin())
    {
      formats.push_back(plugin);
    }
    else delete plugin;
  }
}

void ImageFormats::addMainBuiltInFormats()
{
  formats.push_back(new ImageFormatPNG);
  formats.push_back(new ImageFormatNative);
}

void ImageFormats::addOtherBuiltInFormats()
{
  formats.push_back(new ImageFormatBMP);
  formats.push_back(new ImageFormatJPG);
  formats.push_back(new ImageFormatGIF);
  formats.push_back(new ImageFormatTGA);
  formats.push_back(new ImageFormatRGBE);
}

