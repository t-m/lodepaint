/*
LodePaint

Copyright (c) 2009 Lode Vandevenne
All rights reserved.

This file is part of LodePaint.

LodePaint is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LodePaint is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LodePaint.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <vector>

//requires linking -lX11 in case of Linux.

//stores the image as RGBA in image, and its width and height in w and h. So image.size() is w * h * 4. Returns true if there was an image on the clipboard, false if not (in that case the output parameters should not be used)
bool getClipboardImageRGBA8(std::vector<unsigned char>& image, int& w, int& h);

bool setClipboardImageRGBA8(const unsigned char* image, int w, int h, int w2); //RGBA. w2 is memory width of the lines in the buffer (also in pixels)

void processClipboardEvents(); //call once per frame to process events related to clipboard if required by the Operating System.
