/*
LodePaint

Copyright (c) 2009 Lode Vandevenne
All rights reserved.

This file is part of LodePaint.

LodePaint is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LodePaint is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LodePaint.  If not, see <http://www.gnu.org/licenses/>.
*/ 

#pragma once

#include "lpi/os/lpi_filebrowse.h"
#include "lpi/os/lpi_persist.h"
#include "lpi/gio/lpi_input.h"

#include "paint_tool.h"
#include "paint_filter.h"
#include <vector>
#include <string>
#include <map>

struct FilterGroup
{
  std::vector<IOperationFilter*> filters; //NOTE: null means a separator!
  std::string name;
  
  void destruct();
};

class Filters
{
  private:
    std::vector<FilterGroup> filters;
    FilterGroup rootFilters;
  
  public:
    Filters();
    ~Filters();
    
    std::vector<FilterGroup>& getSubFilters() { return filters; }
    void addSub(const std::string& title); //adds a submenu
    void addSub(IOperationFilter* filter); //adds filter to the last sub menu
    void addSubSeparator(); //adds seperator in the last submenu
    
    FilterGroup& getRootFilters() { return rootFilters; }
    void addRoot(IOperationFilter* filter); //adds filter to the root of the menu (instead of in submenu)
    
    int findSub(const std::string& name); //returns -1 if no such submenu is found
    FilterGroup& getSub(size_t index);
    
};

class FiltersCollection
{
  private:
    Filters imageMenuFilters;
    Filters filtersMenuFilters;
    Filters pluginsMenuFilters;
    
    IOperationFilter* negrgb;
    IOperationFilter* crop;
    IOperationFilter* delete_selection;
    IOperationFilter* select_all;
    IOperationFilter* select_none;
    IOperationFilter* negmask;
    IOperationFilter* clear_image;
    IOperationFilter* gaussian_blur;
    IOperationFilter* pixel_antialias;
    IOperationFilter* blend_sel;
    IOperationFilter* wrap_50;
    IOperationFilter* alpha_opaque;
    IOperationFilter* alpha_apply;
    IOperationFilter* normalize;
    IOperationFilter* brightness_contrast;
    IOperationFilter* hue_saturation;
    IOperationFilter* multiply_sla;
    IOperationFilter* greyscale;
    IOperationFilter* flip_x;
    IOperationFilter* flip_y;
    IOperationFilter* rescale;
    IOperationFilter* test_image;

  public:
  
    FiltersCollection(GlobalToolSettings& globalToolSettings, lpi::gui::IGUIDrawer& guidrawer);
  
    Filters& getImageMenuFilters() { return imageMenuFilters; }
    Filters& getFiltersMenuFilters() { return filtersMenuFilters; }
    Filters& getPluginsMenuFilters() { return pluginsMenuFilters; }
    
    void addPluginFilters(lpi::FileBrowse& filebrowser, const std::string& directory, GlobalToolSettings& globalToolSettings, lpi::gui::IGUIDrawer& guidrawer); //directory must have endslash
    
    //the few that have a toolbar icon or keyboard shortcut
    IOperationFilter* getCropFilter() { return crop; }
    IOperationFilter* getNegateRGBFilter() { return negrgb; }
    IOperationFilter* getDeleteSelectionFilter() { return delete_selection; }
    IOperationFilter* getSelectAllFilter() { return select_all; }
    IOperationFilter* getSelectNoneFilter() { return select_none; }
    IOperationFilter* getNegateMaskFilter() { return negmask; }
    IOperationFilter* getClearImageFilter() { return clear_image; }
    IOperationFilter* getGaussianBlurFilter() { return gaussian_blur; }
    IOperationFilter* getPixelAntialiasFilter() { return pixel_antialias; }
    IOperationFilter* getBlendSelectionFilter() { return blend_sel; }
    IOperationFilter* getWrap50Filter() { return wrap_50; }
    IOperationFilter* getAlphaOpaqueFilter() { return alpha_opaque; }
    IOperationFilter* getAlphaApplyFilter() { return alpha_apply; }
    IOperationFilter* getNormalizeFilter() { return normalize; }
    IOperationFilter* getBrightnessContrastFilter() { return brightness_contrast; }
    IOperationFilter* getHueSaturationFilter() { return hue_saturation; }
    IOperationFilter* getMultiplySLAFilter() { return multiply_sla; }
    IOperationFilter* getGreyscaleFilter() { return greyscale; }
    IOperationFilter* getFlipXFilter() { return flip_x; }
    IOperationFilter* getFlipYFilter() { return flip_y; }
    IOperationFilter* getRescaleFilter() { return rescale; }
    IOperationFilter* getTestImageFilter() { return test_image; }
};

class ToolsCollection
{
  private:
    std::map<char, size_t> shortcuts;
    std::vector<ITool*> tools;
  public:

    ToolsCollection(GlobalToolSettings& globalToolSettings, ColorWindow& colorWindow, lpi::gui::IGUIDrawer& guidrawer);
    ~ToolsCollection();
    
    ITool* getTool(size_t index) const;
    size_t getNumTools() const;

    //accepted values: 'c', 'p', 's', 'b', 'e', 'f', 'l'
    //NOTE: these shortcut values have no keyboard-related relevance anymore now that I use ShortCutManager
    size_t getShortcutTool(char shortcut) const;
};


enum ShortCut //the commands
{
  SC_TOOL_PEN, //p
  SC_TOOL_BRUSH, //b
  SC_TOOL_PIXEL_BRUSH, //i
  SC_TOOL_ERASER, //e
  SC_TOOL_COLORPICKER, //c
  SC_TOOL_FLOODFILL, //f
  SC_TOOL_SELRECT, //s
  SC_TOOL_LINE, //l
  
  SC_TOOL_PREVIOUS, //v
  
  SC_OPERATION_CROP, //CTRL+t
  SC_OPERATION_DELETE_SELECTION, //DEL
  SC_OPERATION_SELECT_ALL,
  SC_OPERATION_SELECT_NONE,

  //gap for more operations

  SC_SWAP_FG_BG, //x
  SC_NEGATE_FG_BG, //y
  SC_DEFAULT_FG_BG, //z
  SC_CUT,
  SC_COPY,
  SC_PASTE_IMAGE, //CTRL+v
  SC_PASTE_SELECTION,
  SC_UNDO, //CTRL+z
  SC_REDO, //CTRL+SHIFT+z or CTRR+y
  SC_NEW, //CTRL+n
  SC_OPEN,
  SC_SAVE, //CTRL+S
  SC_RELOAD, //F5
  SC_ZOOM_IN, //+ or numpad +
  SC_ZOOM_OUT, //- or numpad -
  SC_DYN_CHANGE_UP,
  SC_DYN_CHANGE_DOWN,
  
  SC_NONE //used both as amount and to indicate no valid shortcut was pressed this frame. Must always be last in this enum!
};

struct KeyCombo
{
  bool shift;
  bool ctrl;
  bool alt;
  bool meta;
  bool super;
  int ascii; //ascii value of the key. 0 means "nothing" (no shortcut)
  
  KeyCombo();
  KeyCombo(int ascii);
  KeyCombo(bool shift, bool ctrl, bool alt, int ascii);
  KeyCombo(bool shift, bool ctrl, bool alt, bool meta, bool super, int ascii);
};

bool operator==(const KeyCombo& a, const KeyCombo& b);
bool operator!=(const KeyCombo& a, const KeyCombo& b);

class ShortCutManager
{
  private:
    std::vector<KeyCombo> shortcuts[2];
    std::vector<KeyCombo> defaults[2];
    std::map<ShortCut, std::string> names;
    std::map<std::string, ShortCut> namesinv;
    
    void makeDefaults();
    
    static void writeKeyComboToPersist(lpi::Persist& persist, const KeyCombo& combo, const std::string& name);
    static void removeKeyComboFromPersist(lpi::Persist& persist, const std::string& name);
    static void readKeyComboFromPersist(KeyCombo& combo, const lpi::Persist& persist, const std::string& name);

  public:

    ShortCutManager();
  
    ShortCut getShortCut(const lpi::IInput& input); //Get currently pressed shortcut. There can be maximum one shortcut pressed per frame (SC_NONE if no shortcut)
    
    size_t getNumCommands(); //potential number of commands (there are many empty placeholder commands in between)
    std::string getName(ShortCut index) const;
    std::string getXMLName(ShortCut index) const;
    ShortCut getNameInv(const std::string& name) const;
    KeyCombo getCombo(ShortCut index, int bank); //"bank" is the "memory bank", there are two banks: 0 and 1 (two possible shortcuts per command)
    KeyCombo getDefault(ShortCut index, int bank);
    void setCombo(ShortCut index, const KeyCombo& combo, int bank);
    void resetDefaults();
    
    
    void writeToPersist(lpi::Persist& persist, const std::string& name) const;
    void readFromPersist(const lpi::Persist& persist, const std::string& name);
    
    void getCombos(std::vector<KeyCombo>& combos0, std::vector<KeyCombo>& combos1) const;
    void setCombos(const std::vector<KeyCombo>& combos0, const std::vector<KeyCombo>& combos1);

};
