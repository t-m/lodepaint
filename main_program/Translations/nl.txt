"Nederlands"

#general
"Yes" "Ja"
"No" "Nee"
"yes" "ja"
"no" "nee"
"OK" "OK"
"Ok" "Ok"
"ok" "ok"
"Cancel" "Annuleren"
"cancel" "annuleren"

#messages
"Please restart the program for the new language setting to fully take effect."
"De taal is veranderd naar Nederlands.\nHerstart aub het programma om alle teksten te vertalen."

"[Help Text]"
"Zie de bijgevoegde manual.html voor hulp (Engelstalig).\n\
\n\
Dit bestand is in de zip file waarmee dit\n\
programma was geïnstalleerd, in de map Manual.\n\
\n\
Dit bestand kan in een webbrowser bekeken worden.\n\
\n\
De laatste versie is hier:\n\
http://sourceforge.net/projects/lodepaint/"

"Image has unsaved changes. Really close it?" "Er zijn niet opgeslagen veranderingen.\nEcht afsluiten?"
"Some images have unsaved changes. Really close?" "Er zijn niet opgeslagen veranderingen.\nEcht afsluiten?"
"Discard" "Weggooien"
"Keep Open" "Openhouden"

#color dialog
"Color" "Kleur"
"Basic" "Basis"
"Advanced" "Geavanceerd"

#menus
"Edit" "Bewerken"
"File" "Bestand"
"Filters" "Filters"
"Help" "Help"
"Image" "Afbeelding"
"Plugins" "Plugins"
"Settings" "Instellingen"

#submenus
"New" "Nieuw"
"Open" "Openen"
"Reload" "Herladen"
"Recent" "Recent"
"Save" "Opslaan"
"Save As" "Opslaan Als"
"Save Copy As" "Kopie Opslaan Als"
"Save All" "Alles Opslaan"
"Exit" "Afsluiten"

"Undo" "Ongedaan Maken"
"Redo" "Opnieuw Uitvoeren"
"Cut" "Knippen"
"Copy" "Kopiëren"
"Paste As New Image" "Plakken Als Afbeelding"
"Paste As Selection" "Plakken Als Selectie"

"Options" "Opties"
"Shortcuts" "Sneltoetsen"
"Skins" "Skins"
"Language" "Taal"
"See Program Dirs" "Toon Programma Paden"

"About" "Over"

#file dialog
"File exists. Overwrite?" "Bestand bestaat. Overschrijven?"
"Confirm" "Bevestigen"
"Save File" "Bestand Opslaan"
"Open File" "Bestand Openen"
"save" "opslaan"
"open" "openen"
"cancel" "annuleren"
"up" "omhoog"
"Automatically add extension to filename" "Voeg extensie automatisch toe"
"dir:" "map:"
"sug:" "sug:"
"name:" "naam:"
"type:" "type:"
"ext:" "ext:"
"Clear History" "Geschiedenis Wissen"
"Clear recent files list?" "Wis recente geschiedenis?"


#toolbar tooltips
"Zoom In" "Inzoomen" 
"Zoom Out" "Uitzoomen" 
"Actual Size (& Center)" "Ware Grootte (& Centreer)" 
"Show On Entire Screen" "Toon Op Volledig Scherm" 
"Background Pattern (Alpha Channel)" "Achtergrondpatroon (Alphakanaal)" 
"Pixel Raster" "Pixel Rooster" 
"Crosshair" "Richtkruis" 
"Toggle Mask" "Masker Aan/Uit" 
"Mask Display Color" "Masker Kleur" 
"Toggle Batch Mode (Do Filters On All)" "Batch Mode Aan/Uit (doe filters op alles)"
"Repeat Last Filter" "Herhaal Laatste Filter"


#filter sections
"Colors" "Kleuren"
"Color Models" "Kleurmodellen"
"Color Mode" "Kleurmodus"
"HDR" "HDR"
"Transform" "Transformeren"
"Selection" "Selectie"
"Mask" "Masker"
"Sharpness" "Scherpte"
"Edges" "Randen"
"Morphology" "Morphologie"
"Alpha" "Alfa"
"Seamless Tiling" "Herhalingspatronen"
"Generate" "Genereren"
"Noise" "Ruis"
"Effects" "Effecten"
"Artistic" "Artistiek"
"Bits" "Bits"
"DSP" "DSP"

#filters
"Clear" "Wissen"
"Crop" "Bijsnijden"
"Add/Remove Borders" "Canvas Zijden"
"Rescale" "Herschalen"
"Pixel Art Scaling" "Pixel Art Herschalen"
"Negate RGB" "Negatief RGB"
"Negate Lightness" "Negatief Helderheid"
"Solarize RGB" "Solarizeren RGB"
"Normalize" "Normalizeren"
"Normalize 2" "Normalizeren 2"
"Clip Color" "Kleur Afvlakken"
"Threshold" "Drempel"
"Greyscale" "Grijswaarden"
"Colorize" "Verkleur"
"Posterize" "Posterizeren"
"Monochrome (Dithered)" "Monochroom (Gespikkeld)"
"Brightness/Contrast" "Helderheid/Contrast"
"Temperature" "Temperatuur"
"Curves" "Curves"
"Levels" "Niveaus"
"Swap Colors" "Wissel Kleuren"
"Negate" "Negatief"
"Adjust HSLA" "Aanpassen HSLA "
"Adjust HSVA" "Aanpassen HSVA"
"Multiply SLA" "Vermenigvuldig SLA"
"Channel Switcher" "Kanaalwisselaar"
"Channel Switcher II" "Kanaalwisselaar II"
"Color Arithmetic" "Kleurwiskunde"
"RGBA Matrix" "RGBA Matrix"
"RGBA8 (32 bpp)" "RGBA8 (32 bpp)"
"RGBA32F (128 bpp, floating point)" "RGBA32F (128 bpp, hoog bereik)"
"Grey8 (greyscale, 8 bpp)" "Grey8 (grijswaarden, 8 bpp)"
"Grey32F (greyscale, 32 bpp, floating point)" "Grey32F (grijswaarden, 32 bpp, hoog bereik)"
"Color Arithmetic II (HDR)" "Kleurwiskunde II (HDR)"
"Tone Map (Histogram Adjustment)" "Tone Map (Histogram Aanpassen)"
"Absolute Value" "Absolute Waarde"
"Flip X" ""
"Flip Y" ""
"Rotate 90 CW" "Roteer 90° Wijzerzin"
"Rotate 180" "Roteer 180°"
"Rotate 90 CCW" "Roteer 90° Tegenwijzerzin"
"Rotate" "Roteren"
"Transform 2D" "2D Transformatie"
"Delete Selection" "Verwijder Selectie"
"Select All" "Selecteer Alles"
"Select None" "Selecteer Niets"
"Blend Selection" "Meng Selectie"
"Clear Mask" "Leegmaken Masker"
"Clear Mask To Value" "Zet Masker Op Waarde"
"Feather" "Uitbreiden"
"Invert Mask" "Inverteren Masker"
"Selection To Mask" "Selectie Naar Masker"
"Channel To Mask" "Kanaal Naar Masker"
"Swap Mask With Alpha" "Wissel Masker Met Alfa"
"Blur" "Vervagen"
"Gaussian Blur" "Gaussfilter"
"Interpolating Blur" "Interpolerend Vervagen"
"Selective Gaussian Blur" "Selectieve Gaussfilter"
"Sharpen" "Verscherpen"
"Unsharp Mask" "Onscherp Masker"
"High Pass" "Hoogdoorlaatfilter"
"Median Filter" "Mediaanfilter"
"Pixel Antialias" "Pixel Anti-aliasing"
"Edge Detect" "Randdetectie"
"Emboss" "Graveer"
"Pixel Border" "Pixel Rand"
"Contour" "Isolijnen"
"Erode" ""
"Dilate" ""
"Morph Open" ""
"Morph Close" ""
"Morph Gradient" ""
"Top Hat" ""
"Opaque" "Opaak"
"Apply Alpha To BG" "Pas Alfa Toe Op BG"
"Keep Only Alpha" "Houd Enkel Alfa"
"Negate Alpha" "Negatief Alfa"
"Translucent" "Doorzichtig"
"Modulated" "Gemoduleerd"
"Saturation" "Saturatie"
"Color Key" "Chroma Key"
"Soft Key" "Zachte Chroma Key"
"Extract Clouds" "Onttrek Wolken"
"Alpha To Gray" "Alfa Naar Grijswaarden"
"Threshold Alpha" "Drempel Alfa"
"Alpha Monochrome (Dithered)" "Monochrome Alfa (Gespikkeld)"
"Make Seamless" "Maak Herhalingspatroon"
"Repeat" "Herhaal"
"Wrapping Shift 50%" "Zijkant Naar Midden"
"Mirror-Tile" "Herhaal Spiegelend"
"Grid" "Rooster"
"Tiles" "Tegels"
"Scan Lines" "Scanlijnen"
"Gradient" "Gradient"
"Gradient Quad" "Gradient Vierhoek"
"Clouds" "Wolken"
"Marble" "Marmer"
"Plasma" "Plasma"
"XOR Texture" "XOR Textuur"
"Mandelbrot" "Mandelbrot"
"Julia" "Julia"
"Random Noise" "Willekeurige Ruis"
"Random Walk" "Toevalsbeweging"
"Random Shuffle" "Pixels Dooreenschudden"
"Mosaic" "Mosaiek"
"Sepia" "Sepia"
"Pixel Shadow" "Pixelschaduw"
"Game Of Life" "Game Of Life"
"Game Of Life Extinguish" "Game Of Life Uitdoven"
"Pointillism" "Pointillisme"
"Mirror Bits" "Spiegel Bits"
"Shift Bits 1" "Schuif Bits 1"
"Shift Bits 4" "Schuif Bits 4"
"FFT (real)" "FFT (reëel)"
"IFFT (real)" "IFFT (reëel)"
"FFT (magnitude)" "FFT (magnitude)"
"Low Pass Filter (FFT)" "Laagdoorlaatfilter (FFT)"
"Low Cut Filter (FFT)" "Hoogdoorlaatfilter (FFT)"
"Band Pass Filter (FFT)" "Banddoorlaatfilter (FFT)"
"Band Stop Filter (FFT)" "Bandsperfilter (FFT)"
"User Defined" "Gebruikersgedefiniëerd"


#options dialog
"Undo Size (MB)" "Ongedaanmaakgeheugen (MB)"
"Screen BG Color" "Scherm Achtergrondkleur"
"Window Color" "Vensterkleur"
"Window Top Color" "Vensterbalk Kleur"
"Custom Pixel Grid Width" "Aangepast Pixelrooster Breedte"
"Custom Pixel Grid Height" "Aangepast Pixelrooster Hoogte"
"Override Plugin Dir" "Alternatieve Plugin Folder"
"Use Native OS File Dialog" "Inheemse Bestandsdialoog"
"Brush Dynamics (CTRL+Scrollwheel)" "Penseel Dynamics (CTRL+Scrolwiel)"
"Dyn. Change 1" "Dyn. Verandering 1"
"Amount 1" "Hoeveelheid 1"
"Dyn. Change 2" "Dyn. Verandering 2"
"Amount 2" "Hoeveelheid 2"
"Nothing" "Niets"
"Brush Size" "Penseelgrootte"
"Brush Opacity" "Penseelopaciteit"
"Brush Softness" "Penseelzachtheid"
"FG Hue" "Voorgrond Tint"
"FG Saturation" "Voorgrond Verzadiging"
"FG Brightness" "Voorgrond Helderheid"
"FG Alpha" "Voorgrond Alfa"
"BG Hue" "Achtergrond Tint"
"BG Saturation" "Achtergrond Verzadiging"
"BG Brightness" "Achtergrond Helderheid"
"BG Alpha" "Achtergrond Alfa"

"[Undo Memory Tooltip]"
"Maximale hoeveelheid RAM voor onthouden bewerkingen, per afbeelding.\n\
Aangeraden instelling is de helft van uw RAM."

"[Custom Grid Tooltip]"
"Deze instelling bepaalt de grootte van sommige standen van de Pixel Rooster knop."

"[Native File Dialog Tooltip]"
"Niet op alle besturingssystemen ondersteund.\n\
Op Windows gebruikt dit dan de bekende Windows\n\
bestands dialoog in plaats van de in dit programma\n\
ingebouwde OpenGL bestandsdialoog."

"[Shortcuts Explanation]"
"Selecteer actie in de dropdown, klik op\n\
toetscombinatie 1 of 2, druk dan de snelkoppeling\n\
met het toetsenbord. Geldige combinaties zijn een letter of\n\
symbool met optioneel SHIFT, CTRL of ALT. Dubbelklik om\n\
te legen. Op Windows kunnen niet-qwerty toetsenborden\n\
verkeerd werken, geef de snelkoppeling in zoals je het\n\
werkelijk wil."

"defaults:" "standaard:"
"(no shortcut set)" "geen snelkoppeling"

"This lists the paths used on your system by this program." "Dit toont de paden die dit programma op uw systeem geruikt."
"Exe     : " "Exe     : "
"Skins   : " "Skins   : "
"Plugins : " "Plugins : "
"Settings: " "Opties  : "

#filter preview
"update preview" "vers voorbeeld"
"auto update" "auto verversen"

#filter dialog contents
"Wrapping" "Doorheen Randen"
"Affect Alpha" "Alfa beïnvloeden"
"Affect RGB" "RGB beïnvloeden"
"Link Channels" "Veranker kanalen"
"Low" "Laag"
"High" "Hoog"
"FG Color" "Voorgrondkleur"
"BG Color" "Achtergrondkleur"
"Amount" "Hoeveelheid"
"Brightness" "Helderheid"
"Contrast" "Contrast"
"Gamma (log)" "Gamma (log)"
"Adjust" "Aanpassen"
"Include Alpha" "Ook alfa"
"Type" "Type"
"Color" "Kleur"
"smooth" "glad"
"reset" "terugstellen"
"Input Low" "Invoer laag"
"Input Mid (relative)" "Invoer mid (relatief)"
"Input High" "Invoer hoog"
"Output Mid" "Uitvoer mid"
"Output High" "Uitvoer hoog"
"Affect R, G, B, A" "R, G, B, A beïnvloeden"
"Negate R" "Negatief R"
"Negate G" "Negatief G"
"Negate B" "Negatief B"
"Negate A" "Negatief A"
"Negate Saturation" "Negatief verzadiging"
"Negate Lab L" "Negatief Lab L"
"Negate Lab a" "Negatief Lab a"
"Negate Lab b" "Negatief Lab b"
"Hue Adjust" "Tint aanpassen"
"Saturation Adjust" "Verzadiging aanpassen"
"Lightness Adjust" "Helderheid aanpassen"
"Alpha Adjust" "Alfa aanpassen"
"Saturation Factor" "Verzadigingsfactor"
"Lightness Factor" "Helderheidsfactor"
"Alpha Factor" "Alfa factor"
"Inverted" "Geïnverteerd"
"Color Model" "Kleurmodel"
"Channel 1" "Kanaal 1"
"Channel 2" "Kanaal 2"
"Channel 3" "Kanaal 3"
"(Channel 4)" "(Kanaal 4)"
"Channel Alpha" "Kanaal alfa"
"Grey" "Grijs"
"Red" "Rood"
"Green" "Groen"
"Blue" "Blauw"
"Channel for R" "Kanaal voor R"
"Channel for G" "Kanaal voor G"
"Channel for B" "Kanaal voor B"
"Channel for A" "Kanaal voor A"
"Greyscale (uses R)" "Grijswaarden (gebruikt R)"
"Operation" "Operatie"
"Formula 1: d * ((ax + b) ^ c)" "Formule 1: d * ((ax + b) ^ c)"
"Formula 2: d * (e ^ (ax + b))" "Formule 2: d * (e ^ (ax + b))"
"Formula 3: d * (log_e(ax + b))" "Formule 3: d * (log_e(ax + b))"
"Formula 4: d / (ax + b)" "Formule 4: d / (ax + b)"
"Formula 1" "Formule 1"
"Formula 2" "Formule 2"
"Formula 3" "Formule 3"
"Formula 4" "Formule 4"
"Formula" "Formule"
"Affect R" "R beïnvloeden"
"Affect G" "G beïnvloeden"
"Affect B" "B beïnvloeden"
"Affect A" "A beïnvloeden"
"Desaturate" "Desatureren"
"For HDR images with neg. pixels" "Voor HDR afbeeldingen met neg. pixels"
"Angle" "Hoek"
"Counterclockwise" "Tegenwijzerzin"
"Smooth" "Geïnterpoleerd"
"Repeating" "Herhalend"
"Lock Scale" "Vergrendel grootte"
"Opacity" "Opaciteit"
"Treat Source Alpha As Opacity" "Bron alfa als opaciteit"
"Affect Target Alpha" "Beïnvloeden doel alfa"
"Value" "Waarde"
"Radius" "Straal"
"Channel" "Kanaal"
"Hue" "Tint"
"Hue Distance" "Hue Afstand"
"Negative values decrease size" "Negatieve waarden verkleinen"
"Left" "Links"
"Right" "Rechts"
"Top" "Boven"
"Bottom" "Onder"
"Size" "Grootte"
"Interpolation" "Interpolatie"
"Nearest" "Dichtstbijzijnde"
"Bilinear" "Bilineair"
"Bicubic" "Bikubisch"
"Skewed (Ugly!)" "Schuin (lelijk!)"
"Algorithm" "Algoritme"
"Power" "Kracht"
"Diameter X" "Diameter X"
"Diameter Y" "Diameter Y"
"Matrix" "Matrix"
"Matrix Rotation" "Matrix rotatie"
"Mode" "Modus"
"Border Color" "Randkleur"
"Type" "Type"
"Place Edge" "Plaats Rand"
"Num X" "Aantal X"
"Num Y" "Aantal Y"
"Mul" "Vermenigvuldigen"
"Div" "Delen"
"Add" "Optellen"
"Size X" "Grootte X"
"Size Y" "Grootte Y"
"Shift X" "Verplaats X"
"Shift Y" "Verplaats Y"
"Opacity A" "Opaciteit A"
"Opacity B" "Opaciteit B"
"Colored" "Gekleurd"
"Color 00" "Kleur 00"
"Color 01" "Kleur 01"
"Color 10" "Kleur 10"
"Color 11" "Kleur 11"
"Sine Pattern" "Sinuspatroon"
"Period X" "Periode X"
"Period Y" "Periode Y"
"Period X 1" "Periode X 1"
"Period Y 1" "Periode Y 1"
"Period X 2" "Periode X 2"
"Period Y 2" "Periode Y 2"
"Period X 3" "Periode X 3"
"Period X 4" "Periode X 4"
"Power 1" "Kracht 1"
"Power 2" "Kracht 2"
"Power 3" "Kracht 3"
"Power 4" "Kracht 4"
"Shift X 3" "Verplaats X 3"
"Shift Y 3" "Verplaats Y 3"
"Shift X 4" "Verplaats X 4"
"Shift Y 4" "Verplaats Y 4"
"Move X" "Verplaats X"
"Move Y" "Verplaats Y"
"Max Iterations" "Max iteraties"
"Normalized Iteration" "Genormalizeerde iteratie"
"Tricorn" "'Tricorn'"
"Hue colored" "Getint"
"Color Type" "Kleurtype"
"Randomize Opacity" "Willekeurige opaciteit"
"Salt & Pepper" "Peper en zout"
"Distribution" "Verdeling"
"Uniform" "Uniform"
"Gaussian" "Gaussiaans"
"Binary" "Binair"
"Distance" "Afstand"
"Tile Size" "Tegelgrootte"
"Antialias" "Anti-aliasing"
"Sepia Color" "Sepiakleur"
"Darkness" "Donkerte"
"Shadow Color" "Schaduwkleur"
"X Shift" "Verplaats X"
"Y Shift" "Verplaats Y"
"Not From Shadow Color" "Niet van schaduwkleur"
"Point Size" "Puntgrootte"
"Regular" "Regelmatig"
"BG Colored Canvas" "Gekleurde achtergrond"
"Trade-off (noise <-> waves)" "Trade-off (ruis <-> golven)"
"Stop Factor" "Stopfactor"
"Pass Factor" "Doorlaatfactor"
"Grey instead of DC" "Grijs ipv DC"
"Low Radius" "Lage straal"
"High Radius" "Hoge straal"
"relative" "relatief"

#tool settings
"Step" "Stap"
"Softness" "Zachtheid"
"Shape" "Vorm"
"Round" "Rond"
"Square" "Vierkant"
"Diamond" "Ruit"
"Alpha Left" "Alfa links"
"Alpha Right" "Alfa rechts"
"Tolerance" "Tolerantie"
"Tol. Ignores A." "Tol. Negeert α"
"Lightness" "Helderheid"
"Enable Outline" "Omlijnen"
"Line Thickness" "Lijndikte"
"Line Opacity" "Lijn opaciteit"
"Enable Fill" "Vullen"
"Fill Opacity" "Vulopaciteit"
"Allow Freehand" "Kan vrije hand"
"Input" "Invoer"
"Ellipse" "Ellips"
"Circle (2 points)" "Cirkel (2 punten)"
"Circle (3 points)" "Cirkel (3 punten)"
"Text" "Tekst"
"Font" "Lettertype"
"Background" "Achtergrond"
"Transparent" "Transparant"
"8 points" "8 punten"

#panel titles
"Overview" "Overzicht"
"Toolbox" "Gereedschap"
"Tool Settings" "Gereedschapsopties"

#tools
"Pan" "Verplaatsen"
"Color Picker" "Pipet"
"Pen" "Pen"
"Pixel Brush" "Pixel penseel"
"Brush" "Penseel"
"Eraser" "Gom"
"Color Replacer" "Kleurvervanger"
"Flood Fill" "Emmer"
"Line" "Lijn"
"Rectangle" "Rechthoek"
"Polygon" "Veelhoek"
"Gradient Line" "Gradientlijn"
"Sierpinski Triangle" "Driehoek van Sierpinski"
"Select Rectangle" "Selecteer rechthoek"
"Edit Mask Brush" "Masker penseel"
"Magic Wand (On Mask)" "Toverstaf (op masker)"
"Perspective Crop" "Perspectief bijsnijden"

#shortcuts
"Select" "Selecteren"
"Previous Tool" "Vorig gereedschap"
"Swap FG BG" "Wissel kleuren"
"Negate FG BG" "Negatief kleuren"
"Default FG BG" "Standaard kleuren"
"Paste As Image" "Plakken als afbeelding"
"New Image" "Nieuwe afbeelding"
"Reload file" "Herlaad bestand"
"Dyn Change Up" "Dyn. penseel op"
"Dyn Change Down" "Dyn. penseel neer"
"" ""
"" ""




